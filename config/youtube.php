<?php

/*
|--------------------------------------------------------------------------
| Laravel PHP Facade/Wrapper for the Youtube Data API v3
|--------------------------------------------------------------------------
|
| Here is where you can set your key for Youtube API. In case you do not
| have it, it can be acquired from: https://console.developers.google.com
*/

return [
    'key' => 'AIzaSyAxz_MWYVcOt6u1XDDsUxyEPk9Rc_Qdhw0'//env('YOUTUBE_API_KEY')
];
