<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectReferenceTag extends Model
{
    protected $fillable = [
        'tag'
    ];

    protected $connection = 'mysql_insect';

    public function references(){
        return $this->belongsToMany('App\InsectReference', 'insect_Reference_has_tags', 'reference_tag_id', 'reference_id');
    }
}
