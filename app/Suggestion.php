<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class Suggestion extends Model
{
    use FiltersRecords;

    protected $fillable = ['user_id', 'source_id', 'suggestion_type_id', 'suggestion', 'status', 'email'];

    public function source()
    {
        return $this->belongsTo('App\Source');
    }

    public function suggestionType()
    {
        return $this->belongsTo('App\SuggestionType');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
