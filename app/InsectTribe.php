<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectTribe extends Model
{
    use FiltersRecords;

    protected $fillable = [
        'tribe', 'sub_family_id', 'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function subFamily()
    {
        return $this->belongsTo(InsectSubFamily::class, 'sub_family_id');
    }

    public function genus()
    {
        return $this->hasMany(InsectGenus::class);
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
