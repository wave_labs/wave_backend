<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Carbon\Carbon;

class Dive extends Model
{
    use FiltersRecords;

    protected $fillable =
    [
        'max_depth',
        'dive_time',
        'number_diver',
        'diving_spot_id',
        'user_id',
        'date'
    ];

    protected $appends = ['class'];

    public function getClassAttribute()
    {
        return "DIVE_REPORTER";
    }

    public function creatures()
    {
        return $this->belongsToMany('App\Creature', 'dive_pivots');
    }

    public function diving_spot()
    {
        return $this->belongsTo('App\DivingSpot', 'diving_spot_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function dive_photo()
    {
        return $this->hasMany('App\DivePhoto');
    }

    public function survey()
    {
        return $this->hasOne('App\Survey');
    }

    public function dive_pivot()
    {
        return $this->hasMany('App\DivePivot');
    }

    public function litters()
    {
        return $this->belongsToMany('App\Litter', 'dive_has_litters');
    }

    public static function getActivity($user_id)
    {
        return static::where('user_id', $user_id)->with('diving_spot')->with('dive_pivot.creature')->orderBy('date', 'desc')->limit(4)->get();
    }

    public static function reportsPerMonth($users)
    {
        $date = Carbon::now()->firstOfMonth()->subMonths(11);
        $total = [];

        for ($i = 0; $i <= 11; $i++) {
            $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
            $last = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();

            $dives = Dive::whereBetween("date", [$first, $last])->fromUsers($users)->count();
            $total[$date->format('M')] = $dives;
            $date->addMonths(1);
        }

        return $total;
    }

    public static function reportsPerMonthPerDivingSpot($filter, $users)
    {
        $filter ? $date = Carbon::createFromFormat('Y-m', $filter) : $date = Carbon::now()->firstOfMonth()->subMonths(11);
        $total = [];

        for ($i = 0; $i <= 11; $i++) {
            $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
            $last = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();
            $values = [];

            $divingSpots = DivingSpot::all();

            foreach ($divingSpots as $divingSpot) {
                $dives = Dive::whereBetween("date", [$first, $last])->whereDivingSpotId($divingSpot->id)->fromUsers($users)->count();
                $values[$divingSpot->name] = $dives;
            }

            $total[$date->format('M')] = $values;
            if ($filter) break;
            else $date->addMonths(1);
        }

        return $total;
    }

    public function scopeGetPosts($query, $users)
    {
        if ($users)
            return $query->whereIn('user_id', $users);
        else
            return $query;
    }


    public function scopeFromUsers($query, $users)
    {
        if ($users)
            return $query->whereIn('user_id', $users);
        else
            return $query;
    }
}
