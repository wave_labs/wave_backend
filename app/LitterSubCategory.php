<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Spatie\Translatable\HasTranslations;

class LitterSubCategory extends Model
{
    use FiltersRecords, HasTranslations;
    public $translatable = ['name'];
    protected $fillable = ['name', 'litter_category_id', 'OSPAR_code', 'UNEP_code', 'Core', 'Beach', 'Seafloor', 'Floating', 'Biota', 'Micro'];

    public function scopeSource($query, $filter = 'beach')
    {
        return $query->where($filter, 1);
    }

    public function scopeCategory($query, $filter = 1)
    {
        return $query->whereLitterCategoryId($filter);
    }

    public function litter()
    {
        return $this->belongsToMany('App\Litter', 'litter_sub_category_pivot');
    }

    public function litterCategory()
    {
        return $this->belongsTo('App\LitterCategory');
    }
}
