<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotRule extends Model
{
    protected $connection = 'mysql_iot';

    public function fields()
    {
        return $this->belongsToMany(IotDeviceField::class, 'iot_device_field_has_rules', 'rule_id', 'field_id');
    }
}
