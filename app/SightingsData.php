<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
Use Storage;
use Cerbero\QueryFilters\FiltersRecords;

class SightingsData extends Model
{
    use FiltersRecords;

    protected $fillable = ['capsule_link', 'sighting_id', 'underwater','type'];


	/**
	* The table associated with the model.
	*
	* @var string
	*/
    protected $table = 'sightings_data';


    /**
    * Get the sighting that this data belongs to
    */
    public function sighting()
    {
        return $this->belongsTo('App\Sighting');
    }

    //TODO: hasOne
    /**
    * Get all of the acoustic data for the sighting.
    */
    public function acoustic()
    {
        return $this->hasMany('App\Acoustic');
    }
    
  
}
