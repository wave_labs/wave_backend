<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinate extends Model
{
    /**
     * Get average value of a array of latitudes or longitudes
     */
    public static function AverageCoordinate($array)
    {
        $step = 1;
        $result = [];

        for ($i = -89; $i < 89 - $step; $i += $step) {
            $res = array_filter($array, function ($v) use ($i, $step) {
                $c = ($i <= $v) && $v < $i + $step;
                return $c;
            });
            $result[$i . '|' . ($i + $step)] = count($res);
        }
        $range = array_search(max($result), $result);
        $range = explode("|", $range);

        return $range;
    }
}
