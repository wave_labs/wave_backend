<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnmModelPrediction extends Model
{
    public function model()
    {
        return $this->belongsTo('App\EnmModel');
    }

    public function predictions()
    {
        return $this->hasMany('App\EnmPrediction');
    }
}
