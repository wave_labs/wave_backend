<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class FreshwaterPin extends Model
{
    use FiltersRecords;

    protected $fillable = ['creature_id'];

    public function form()
    {
        return $this->hasMany('App\FreshwaterPinForm');
    }

    public function coordinates()
    {
        return $this->hasMany('App\FreshwaterPinCoordinate');
    }

    public function creature()
    {
        return $this->belongsTo('App\Creature');
    }

    public function storePins($pins)
    {
        foreach ($pins as $key => $image) { //Image iterator
            foreach ($image as $index => $timeframe) { //Timeframe iterator
                foreach ($timeframe as $coords) { //Timeframe coordinates
                    FreshwaterPinCoordinate::create([
                        'latitude' => $coords[0],
                        'longitude' => $coords[1],
                        'freshwater_pin_id' => $this->id,
                        'timeframe' => $index,
                        'image' => $key,
                    ]);
                }
            }
        }
    }

    public function storeForm($form)
    {
        foreach ($form as $key => $value) {
            $question = StudyQuestion::whereCode($key)->whereStudyId(4)->first();

            FreshwaterPinForm::create([
                'answer' => $value,
                'freshwater_pin_id' => $this->id,
                'study_question_id' => $question->id,
            ]);
        }
    }
}
