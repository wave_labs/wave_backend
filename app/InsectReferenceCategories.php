<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectReferenceCategories extends Model
{
    protected $fillable = [
        'category'
    ];

    protected $connection = 'mysql_insect';

    public function references(){
        return $this->belongsToMany('App\InsectReference', 'insect_Reference_has_categories', 'reference_category_id', 'reference_id');
    }
}
