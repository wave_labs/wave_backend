<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AiCategory extends Model
{
    public function getLabels()
    {
        $labels = [];
        foreach ($this->models as $model) {
            foreach ($model->labels as $label) {
                $labelArray = ['name' => $label->name, 'id' => $label->id];
                if (!in_array($labelArray, $labels)) {
                    array_push($labels,  $labelArray);
                }
            }
        }
        return $labels;
    }

    //

    public function models()
    {
        return $this->hasMany('App\AiModel', 'ai_category_id');
    }
}
