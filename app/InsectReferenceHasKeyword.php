<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectReferenceHasKeyword extends Model
{
    protected $fillable =
    [
        'reference_id',
        'reference_keyword_id',
    ];

    protected $connection = 'mysql_insect';

    public function keywords()
    {
        return $this->hasMany('App\InsectReferenceKeyword');
    }

    public function references()
    {
        return $this->hasMany('App\InsectReference');
    }
}


