<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LitterCategoryImageType extends Model
{
    public function images()
    {
        return $this->hasMany('App\LitterCategoryImage');
    }
}
