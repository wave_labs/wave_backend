<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnmPrediction extends Model
{
    protected $fillable = ['enm_id', 'path', 'enm_model_prediction_id'];

    public function enm()
    {
        return $this->belongsTo('App\Enm', 'model_id');
    }

    public function predictionType()
    {
        return $this->belongsTo('App\EnmModelPrediction', 'enm_model_prediction_id');
    }
}
