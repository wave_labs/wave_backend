<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;
use App\TroutCoordinate;

class Trout extends Model
{
    use FiltersRecords;
    protected $fillable = ['knowledge', 'nameA', 'nameB', 'nameC'];

    public function coordinates()
    {
        return $this->hasMany('App\TroutCoordinate');
    }

    public function storePins($pins)
    {
        foreach ($pins as $key => $image) { //Image iterator
            foreach ($image as $index => $timeframe) { //Timeframe iterator
                foreach ($timeframe as $coords) { //Timeframe coordinates
                    TroutCoordinate::create([
                        'latitude' => $coords[0],
                        'longitude' => $coords[1],
                        'trout_id' => $this->id,
                        'timeframe' => $index,
                        'image' => $key,
                    ]);
                }
            }
        }
    }
}
