<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DivePhoto extends Model
{
	protected $fillable = ['dive_id', 'url'];

    public function dive()
    {
        return $this->belongsTo('App\Dive');
    }
}
