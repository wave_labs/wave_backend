<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image as IMG;
use App\WhatsNewMessage;

class WhatsNew extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'image'
    ];


    public function whatsNewMessages()
    {
        return $this->belongsToMany('App\WhatsNewMessage', 'whats_new_message_pivot', 'whats_new_id', 'whats_new_message_id');
    }

    public static function storeImage($file)
    {
        if ($file->isValid()) {
            $filename = uniqid() . '.png';

            IMG::make($file)->encode('png', 65)->resize(760, null, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            })->save(public_path('storage/images/news/' . $filename));

            return '/images/news/' . $filename;
        }
    }

    public static function storePivot($number, $update, $array, $type)
    {
        if ($number > 0) {
            for ($i = 0; $i < $number; $i++) {
                $current = $array[$i];

                if (!is_null($current)) {
                    $data = [
                        'message' => $current,
                        'whats_new_type_id' => $type,
                    ];
                    $insert = WhatsNewMessage::firstOrCreate($data);

                    $update->whatsNewMessages()->attach($insert->id);
                }
            }
        }
    }
}
