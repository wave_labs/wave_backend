<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectSuperFamily extends Model
{
    use FiltersRecords;

    protected $fillable = [
        'superFamily', 'infra_order_id', 'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function infraOrder()
    {
        return $this->belongsTo(InsectInfraOrder::class, 'infra_order_id');
    }

    public function family()
    {
        return $this->hasMany(InsectFamily::class);
    }

    public function families(){
        return $this->hasMany(InsectFamily::class);
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
