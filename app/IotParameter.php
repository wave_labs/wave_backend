<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotParameter extends Model
{
    protected $connection = 'mysql_iot';

    public function features()
    {
        return $this->belongsToMany(IotFeature::class, 'iot_feature_has_parameters', 'parameter_id', 'feature_id');
    }

    public function fieldFacts()
    {
        return $this->belongsToMany(IotFieldFact::class, 'iot_field_fact_has_parameters', 'parameter_id', 'field_fact_id')
            ->withPivot('value');
    }
}
