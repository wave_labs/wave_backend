<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Process\Process;

use App\AiModel;
use App\AiLabel;
use App\AiDetection;

class AiDetectionClassification extends Model
{

    protected $fillable = ['ai_detection_image_id', 'ai_label_id', 'confirmation', 'accuracy', 'xmin', 'xmax', 'ymin', 'ymax'];

    public static function createClassifications($detection)
    {
        $model = AiModel::find($detection->ai_model_id);

        foreach ($detection->images as $key => $image) {
            $process = new Process([
                "python3",
                public_path("python/image_inference.py"),
                "--modeldir=/models/" .  $model->path,
                "--image=python/images/" . $image->image
            ]);
            $process->run();

            $output = $process->getOutput();
            $output = AiDetection::fixJSON($output); //Regex ' to " 
            $data = json_decode($output, true);

            if ($data != []) {
                foreach ($data as $classification) {
                    $classification = (object) $classification;
                    $label = AiLabel::whereName($classification->label)->value('id');

                    AiDetectionClassification::create([
                        'ai_detection_image_id' => $image->id,
                        'ai_label_id' => $label,
                        'accuracy' => $classification->accuracy,
                        'xmin' => $classification->xmin,
                        'xmax' => $classification->xmax,
                        'ymin' => $classification->ymin,
                        'ymax' => $classification->ymax,
                    ]);
                }
            } else {
                AiDetectionClassification::create([
                    'ai_detection_image_id' => $image->id,
                    'ai_label_id' => null,
                    'accuracy' => 0,
                    'xmin' => 0,
                    'xmax' => 0,
                    'ymin' => 0,
                    'ymax' => 0,
                ]);
            }
        }
    }
    //

    public function image()
    {
        return $this->belongsTo('App\AiDetectionImage');
    }

    public function label()
    {
        return $this->belongsTo('App\AiLabel', 'ai_label_id');
    }
}
