<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectReferenceHasCategories extends Model
{
    protected $fillable =
    [
        'reference_id',
        'reference_category_id',
    ];

    protected $connection = 'mysql_insect';

    public function categories()
    {
        return $this->hasMany('App\InsectReferenceCategories');
    }

    public function references()
    {
        return $this->hasMany('App\InsectReference');
    }
}
