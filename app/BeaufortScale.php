<?php

namespace App;

use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class BeaufortScale extends Model
{
    use HasTranslations;

    protected $table = 'beaufort_scale';
    protected $fillable = ['desc', 'sea_conditions', 'land_conditions', 'scale'];
    protected $translatable = ['desc'];

    /**
     * Get all sightings with this rating
     */
    public function sightings()
    {
        return $this->hasMany('App\Sighting');
    }
}
