<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyQuestion extends Model
{
    public function answer()
    {
        return $this->hasMany('App\MarkerResult');
    }
}
