<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectSinonimia extends Model
{
    protected $fillable = [
        'specific_id',
        'infra_specific_id',
        'genus',
        'subgenus',
        'specificEpithet',
        'infraSpecificEpithet',
        'reference_id',
        'taxonRank',
        'taxonomicStatus',
        'nomenclatureStatus',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName',
    ];
    protected $connection = 'mysql_insect';

    /**
     * Get the sinonimias that owns the InsectSinonimia
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function specie()
    {
        return $this->belongsTo(InsectSpecific::class, 'specific_id');
    }

    public function subspecie()
    {
        return $this->belongsTo(InsectInfraSpecific::class, 'infra_specific_id');
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
