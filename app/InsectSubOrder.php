<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectSubOrder extends Model
{
    use FiltersRecords;

    protected $fillable = [
        'subOrder', 'order_id', 'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function order()
    {
        return $this->belongsTo(InsectOrder::class, 'order_id');
    }

    public function family()
    {
        return $this->hasMany(InsectFamily::class);
    }

    public function infraOrders()
    {
        return $this->hasMany(InsectInfraOrder::class);
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
