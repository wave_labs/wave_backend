<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class Litter extends Model
{
    use FiltersRecords;

    protected $fillable = [
        'user_id',
        'latitude',
        'longitude',
        'date',
        'quantity',
        'radius',
        'collected',
        'source',
        'multiple'
    ];

    protected $appends = ['class'];

    public function getClassAttribute()
    {
        return "LITTER_REPORTER";
    }
    public static function reportsPerMonth($source, $users)
    {
        $date = Carbon::now()->firstOfMonth()->subMonths(11);
        $total = [];

        for ($i = 0; $i <= 11; $i++) {
            $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
            $last = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();

            $source ?
                $reports = Litter::whereBetween("date", [$first, $last])->fromUsers($users)->whereSource($source)->count() : $reports = Litter::whereBetween("date", [$first, $last])->fromUsers($users)->count();

            $total[Carbon::now()->month($date->month)->year($date->year)->format('M')] = $reports;

            $date->addMonths(1);
        }

        return $total;
    }

    public static function reportsPerMonthPerCategory($filter, $users)
    {
        $filter ? $date = Carbon::createFromFormat('Y-m', $filter) : $date = Carbon::now()->subMonths(11);

        $total = [];

        for ($i = 0; $i <= 11; $i++) {
            $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
            $last = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();

            $values = [];
            $categories = LitterCategory::all();

            foreach ($categories as $category) {
                $reports = Litter::whereBetween("date", [$first, $last])->fromUsers($users)->whereHas('litterCategory', function ($query) use ($category) {
                    $query->whereLitterCategoryId($category->id);
                })->count();

                $values[$category->name] = $reports;
            }

            $values["Not categorized"] = Litter::whereBetween("date", [$first, $last])->fromUsers($users)->doesntHave('litterCategory')->count();

            $total[Carbon::now()->month($date->month)->year($date->year)->format('M')] = $values;

            if ($filter) break;
            else $date->addMonths(1);
        }



        return $total;
    }

    public static function reportsPerMonthPerSpecificCreature($category, $filter)
    {
        $filter ? $date = Carbon::createFromFormat('Y-m', $filter) : $date = Carbon::now()->subMonths(12);
        $total = [];

        for ($i = 0; $i <= 11; $i++) {
            $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
            $last = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();
            $values = [];

            $reports = Litter::whereBetween("date", [$first, $last])->whereHas('litterCategory', function ($query) use ($category) {
                $query->whereLitterCategoryId($category->id);
            })->count();

            $values[$category->name] = $reports;
            $total[Carbon::now()->month($date->month)->year($date->year)->format('M')] = $values;

            if ($filter) break;
            else $date->addMonths(1);
        }

        return $total;
    }

    public function litterSubCategory()
    {
        return $this->belongsToMany('App\LitterSubCategory', 'litter_sub_category_pivot');
    }

    public function litterCategory()
    {
        return $this->belongsToMany('App\LitterCategory', 'litter_category_pivot');
    }

    public function litterPhotos()
    {
        return $this->hasMany('App\LitterPhoto');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pending_points()
    {
        return $this->hasMany('App\PendingPoint');
    }

    public static function getActivity($user_id)
    {
        return static::where('user_id', $user_id)->orderBy('date', 'desc')->with('litterSubCategory')->with('litterPhotos')->limit(4)->get();
    }

    public function dives()
    {
        return $this->belongsToMany('App\Dive', 'dive_has_litters');
    }

    public function scopeGetPosts($query, $users)
    {
        if ($users)
            return $query->whereIn('user_id', $users);
        else
            return $query;
    }

    public function scopeFromUsers($query, $users)
    {
        if ($users)
            return $query->whereIn('user_id', $users);
        else
            return $query;
    }

    public function reports()
    {
        return $this->hasMany('App\LitterReport');
    }
}
