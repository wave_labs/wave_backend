<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class InsectProject extends Model
{
    protected $connection = 'mysql_insect';

    use FiltersRecords;

    protected $fillable = [
        'name',
        'owner_id',
        'description',
        'imagePath',
        'mobile_contact',
        'email_contact',
        'address',
    ];

    public function owner()
    {
        return $this->belongsTo('App\User');
    }

    public function team()
    {
        return $this->hasMany('App\InsectProjectHasUser');
    }

    public function references()
    {
        return $this->hasMany('App\InsectReference');
    }

    public function occurrences()
    {
        return $this->hasMany('App\InsectOccurrence');
    }
}
