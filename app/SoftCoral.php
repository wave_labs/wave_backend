<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;
use App\SoftCoralCoordinate;

class SoftCoral extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'soft_coral';
    protected $connection = 'mysql_studies';

    use FiltersRecords;
    protected $fillable = ['knowledge', 'name',];

    public function coordinates()
    {
        return $this->hasMany('App\SoftCoralCoordinate');
    }

    public function storePins($pins)
    {
        foreach ($pins as $key => $image) { //Image iterator
            foreach ($image as $index => $timeframe) { //Timeframe iterator
                foreach ($timeframe as $coords) { //Timeframe coordinates
                    SoftCoralCoordinate::create([
                        'latitude' => $coords[0],
                        'longitude' => $coords[1],
                        'soft_coral_id' => $this->id,
                        'timeframe' => $index,
                        'image' => $key,
                    ]);
                }
            }
        }
    }
}
