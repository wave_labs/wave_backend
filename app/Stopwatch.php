<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stopwatch extends Model
{
    protected $fillable =
    [
        'video_id',
        'class_id',
        'user_id',
        'time',
    ];

    public function class()
    {
        return $this->belongsTo('App\OceanusClass');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function video()
    {
        return $this->belongsTo('App\User');
    }
}
