<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotFieldFact extends Model
{
    protected $fillable = ['feature_id', 'section_id', 'field_id'];
    protected $table = "iot_field_facts";
    protected $connection = 'mysql_iot';
    
    public function parameters()
    {
        return $this->belongsToMany(IotParameter::class, 'iot_field_fact_has_parameters', 'field_fact_id', 'parameter_id')
            ->withPivot("value");
    }

    public function feature()
    {
        return $this->belongsTo(IotFeature::class, 'feature_id');
    }
}
