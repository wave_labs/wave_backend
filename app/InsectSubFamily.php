<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectSubFamily extends Model
{
    use FiltersRecords;

    protected $fillable = [
        'subFamily', 'family_id', 'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function family()
    {
        return $this->belongsTo(InsectFamily::class, 'family_id');
    }

    public function tribes()
    {
        return $this->hasMany(InsectTribe::class);
    }

    public function genus()
    {
        return $this->hasMany(InsectGenus::class);
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }

}
