<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;


class Survey extends Model
{
    use FiltersRecords;

    protected $guarded = 
    [
        'id',
        'create_at',
        'update_at',
        'deleted_at'
    ];
    protected $fillable =
    [
        'abundance_value',
        'creature_id',
        'dive_id',
        'user_id',
        'date'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
    * Get all the known creatures with this type
    */
    public function creature() 
    {
        return $this->belongsTo('App\Creature');
    }

    public function type()
    {
        return $this->belongsTo('App\CreatureType');
    }


    public function dive() 
    {
        return $this->belongsTo('App\Dive');
    }
}
