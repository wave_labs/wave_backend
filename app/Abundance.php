<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use App\DivingSpot;

class Abundance extends Model
{
    use FiltersRecords;

    protected $fillable =
    [
        'creature_id',
        'level_1',
        'level_2',
        'level_3',
        'level_4'
    ];
    protected $guarded =
    [
        'id',
        'create_at',
        'update_at',
        'deleted_at'
    ];

    /**
     * Get the creature for this abundance
     */
    public function creature()
    {
        return $this->belongsToOne('App\Creature');
    }

    public static function getAverage($divingSpot)
    {
        return $divingSpot;
    }
}
