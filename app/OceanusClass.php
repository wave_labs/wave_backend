<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OceanusClass extends Model
{
    protected $fillable =
    [
        'name',
    ];

    protected $table = 'classes';

    public function object()
    {
        return $this->hasMany('App\OceanusObject');
    }

    public function stopwatch()
    {
        return $this->hasMany('App\Stopwatch');
    }
}
