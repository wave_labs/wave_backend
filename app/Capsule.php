<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class Capsule extends Model
{
    use FiltersRecords;

	protected $guarded = ['id', 'create_at', 'update_at'];


    /**
    * Get all the sightings from this capsule
    */
    public function sightings()
    {
        return $this->hasMany('App\Sightings');
    }

    /**
    * Get the user that owns this capsule
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
