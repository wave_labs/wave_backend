<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreshwaterPinForm extends Model
{
    protected $fillable = [
        'answer',
        'freshwater_pin_id',
        'study_question_id'
    ];

    public function freshwaterPin()
    {
        return $this->belongsTo('App\FreshwaterPin');
    }

    public function question()
    {
        return $this->belongsTo('App\StudyQuestion', 'study_question_id');
    }
}
