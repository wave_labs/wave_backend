<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomeItem extends Model
{
    //

    public function interaction()
    {
        return $this->belongsTo('App\DomeHasInteraction', 'dome_interaction_id');
    }

    public function arReports()
    {
        return $this->belongsToMany('App\ArDome', 'ar_dome_has_items');
    }
}
