<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarkerCoordinates extends Model
{
    protected $fillable = ['latitude', 'longitude', 'marker_activity_id'];
    public function markerActivity()
    {
        return $this->belongsTo('App\MarkerActivity');
    }
}
