<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHasDivingSpot extends Model
{
    protected $fillable =
    [
        'user_id',
        'diving_spot_id',
    ];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function divingSpot()
    {
        return $this->hasMany('App\DivingSpot');
    }

}