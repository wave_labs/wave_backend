<?php

namespace App\Imports;

use App\InsectAuthor;
use App\InsectReference;
use App\InsectReferenceHasAuthor;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

class InsectReferencesImport implements ToModel, WithHeadingRow
{

    public function __construct()
    {

        HeadingRowFormatter::default('none');
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $reference = InsectReference::where([['title', '=', $row["Title"]], ['date', '=', $row["Publication Year"]]])->first();
        if (!$reference) {
            $reference = InsectReference::create([
                'title' => $row['Title'],
                'identifier' => $row["DOI"],
                'description' => $row["Item Type"],
                'date' => new \DateTime($row["Publication Year"] . "-01-01"),
                'journal' => $row["Publication Title"],
                /* 'chapterTitle' => $row["Chapter Title"], */
                'volume' => $row["Volume"],
                'pages' => $row["Pages"],
                'edition' => $row["Edition"],
                'publisher' => $row["Publisher"],
                'language' => $row["Language"],
                'subject' => $row["Extra"],
                'taxonRemarks' => $row["Notes"],
                'type' => $row["Type"],
                'validated' => "No",
                'issue' => $row["Issue"],
                /* 'degree' => $row["Degree"], */
                /* 'university' => $row["University"], */
                'conferenceTittle' => $row["Conference Name"],
                'accessDate' => $row["Access Date"],
            ]);
            $authorList = explode(";", $row["Author"]);

            foreach ($authorList as $authorName) {
                $author = InsectAuthor::where('name', '=', $authorName)->first();
                if (!$author) {
                    $author = InsectAuthor::create([
                        'name' => $authorName
                    ]);
                }
                InsectReferenceHasAuthor::create([
                    'author_id' => $author['id'],
                    'reference_id' => $reference['id']
                ]);
            }
        }
        return;
    }
}
