<?php

namespace App\Imports;

use App\InsectReport;
use App\InsectZone;
use App\InsectFamily;
use App\InsectSpecies;
use App\InsectSinonimia;
use App\InsectReference;
use DateTime;
use Error;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class InsectReportImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //check if zone exists if not create it
        $zone = InsectZone::where([['region', '=', $row[2]], ['province', '=', $row[3]], ['locality', '=', $row[4]]])->first();
        if(!$zone){
            $zone = InsectZone::create([
                'region' => $row[2],
                'province' => $row[3],
                'locality' => $row[4]
            ]); 
        }
        $family = InsectFamily::where('name', '=', $row[0])->first();
        if(!$family)
        {
            $family = InsectFamily::create([
                'name' => $row[0]
            ]);
        }
        $species = InsectSpecies::where('name', '=', $row[1])->first();
        if(!$species)
        {
            $species = InsectSpecies::create([
                'name' => $row[1],
                'family_id' => $family['id']
            ]);
        }
        $sinonimia = InsectSinonimia::where([['name', '=', $row[8]],['species_id', '=', $species['id']]])->first();
        if(!$sinonimia)
        {
            $sinonimia = InsectSinonimia::create([
                'name' => $row[8],
                'species_id' => $species['id']
            ]);
        }
        $reference = InsectReference::where([['title', '=', $row[16]],['year', '=', $row[18]]])->first();
        if(!$reference)
        {
            $reference = InsectReference::create([
                'title' => $row[16],
                'author' => $row[17],
                'year' => $row[18],
                'journal' => $row[19],
                'volume' => $row[20],
                'editor' => $row[21],
                'chapter' => $row[22]
            ]);
        }
        $report = InsectReport::where([['zone_id', '=', $zone['id']], ['species_id', '=', $species['id']], ['sinonimia_id', '=', $sinonimia['id']], ['reference_id', '=', $reference['id']]])->first();
        if(!$report)
        {
            $dates = explode("-", $row[10]);
            $start_date = date("Y/m/d", strtotime(str_replace('/', '-', $dates[0]))); //problem when its a singular date
            count($dates) > 1 ? $end_date = date("Y/m/d", strtotime(str_replace('/', '-', $dates[1]))) : $end_date = $start_date;
            return new InsectReport([
                'latitude' => $this->DMStoDEC($row[7]),
                'longitude' => $this->DMStoDEC($row[6]),
                'coordinates' => $row[5],
                'n_male' => $row[11],
                'n_female' => $row[12],
                'n_children' => $row[13],
                'page' => $row[23],
                'zone_id' => $zone['id'],
                'species_id' => $species['id'],
                'sinonimia_id' => $sinonimia['id'],
                'reference_id' => $reference['id'],
                'collector' => $row[15],
                'date_type' => 'range', //how to check the date_type without doing if's for all
                'date' => $start_date,
                'start' => $start_date,
                'end' => $end_date
            ]);
        }
        return;
    }

    function DMStoDEC($DMS)
    {
        if($DMS)
        {
            $divide = preg_split("/[º\']/",$DMS); //[0] => 41 [1] =>        [2] => 31         [3] => 57.50         [4] =>        [5] => N
            return $divide[0]+((($divide[2]*60)+($divide[3]))/3600);
        }
        return NULL;
    }

    public function startRow(): int
    {
        return 2;
    }
}
