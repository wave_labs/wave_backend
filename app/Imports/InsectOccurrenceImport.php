<?php

namespace App\Imports;

use App\InsectOccurrence;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

class InsectOccurrenceImport implements ToModel, WithHeadingRow, WithStrictNullComparison
{
    private $projectId;

    public function __construct($projectId)
    {
        $this->projectId = $projectId;
        HeadingRowFormatter::default('none');
    }

    public function model(array $row)
    {
        return;
    }
}
