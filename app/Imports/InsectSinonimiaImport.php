<?php

namespace App\Imports;

use App\InsectSinonimia;
use App\InsectSpecies;
use App\InsectFamily;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class InsectSinonimiaImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $sinonimia = InsectSinonimia::where('name', '=', $row[2])->first();
        if(!$sinonimia)
        { 
            $species = InsectSpecies::where('name', '=', $row[1])->first();   
            if(!$species)             
            {
                $family = InsectFamily::where('name', '=', $row[0])->first();
                if(!$family){
                    $family = InsectFamily::create([
                        'name' => $row[0]
                    ]);
                }     
                $species = InsectSpecies::create([
                    'name' => $row[1],
                    'family_id' => $family['id']
                ]);
            }
            return new InsectSinonimia([
                'name' => $row[2],
                'species_id' => $species['id'],
            ]);
        }
        return;
    }

    public function startRow(): int
    {
        return 2;
    }
    
}
