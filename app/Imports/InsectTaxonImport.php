<?php


namespace App\Imports;

ini_set('max_execution_time', 300);

use App\InsectAuthor;
use App\InsectClass;
use App\InsectFamily;
use App\InsectGenus;
use App\InsectInfraOrder;
use App\InsectInfraSpecific;
use App\InsectKingdom;
use App\InsectOrder;
use App\InsectPhylum;
use App\InsectReference;
use App\InsectSinonimia;
use App\InsectSpecific;
use App\InsectSubFamily;
use App\InsectSubGenus;
use App\InsectSubOrder;
use App\InsectSuperFamily;
use App\InsectTribe;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

class InsectTaxonImport implements ToModel, WithHeadingRow, WithStrictNullComparison, WithBatchInserts
{

    public function __construct()
    {

        HeadingRowFormatter::default('none');
    }


    public function model(array $row)
    {
        $kingdomReference = InsectReference::firstOrCreate(['title' => $row['Kingdom'], 'date' => substr($row['Kingdom'], strrpos($row['Kingdom'], ' ') + 1) . "-01-01"]);
        $kingdomAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Kingdom'], " ", false), ", ", true)]);
        $kingdomReference->authors()->sync($kingdomAuthor);
        $kingdom = InsectKingdom::firstOrCreate([
            'kingdom' => stristr($row['Kingdom'], " ", true)
        ], [
            'taxonRank' => 'Kingdom',
            'reference_id' => $kingdomReference->id
        ]);

        $phylumReference = InsectReference::firstOrCreate(['title' => $row['Phylum'], 'date' => substr($row['Phylum'], strrpos($row['Phylum'], ' ') + 1) . "-01-01"]);
        $phylumAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Phylum'], " ", false), ", ", true)]);
        $phylumReference->authors()->sync($phylumAuthor);
        $phylum = InsectPhylum::firstOrCreate([
            'phylum' => stristr($row['Phylum'], " ", true),
            'kingdom_id' => $kingdom->id,
        ], [
            'taxonRank' => 'Phylum',
            'reference_id' => $phylumReference->id
        ]);

        $classReference = InsectReference::firstOrCreate(['title' => $row['Class'], 'date' => substr($row['Class'], strrpos($row['Class'], ' ') + 1) . "-01-01"]);
        $classAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Class'], " ", false), ", ", true)]);
        $classReference->authors()->sync($classAuthor);
        $class = InsectClass::firstOrCreate([
            'class' => stristr($row['Class'], " ", true),
            'phylum_id' => $phylum->id,
        ], [
            'taxonRank' => 'Class',
            'reference_id' => $classReference->id
        ]);

        $orderReference = InsectReference::firstOrCreate(['title' => $row['Order'], 'date' => substr($row['Order'], strrpos($row['Order'], ' ') + 1) . "-01-01"]);
        $orderAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Order'], " ", false), ", ", true)]);
        $orderReference->authors()->sync($orderAuthor);
        $order = InsectOrder::firstOrCreate([
            'order' => stristr($row['Order'], " ", true),
            'class_id' => $class->id,
        ], [
            'taxonRank' => 'Order',
            'reference_id' => $orderReference->id
        ]);

        if ($row['Suborder'] != null) {
            $subOrderReference = InsectReference::firstOrCreate(['title' => $row['Suborder'], 'date' => substr($row['Suborder'], strrpos($row['Suborder'], ' ') + 1) . "-01-01"]);
            $subOrderAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Suborder'], " ", false), ", ", true)]);
            $subOrderReference->authors()->sync($subOrderAuthor);
            $subOrder = InsectSubOrder::firstOrCreate([
                'subOrder' => stristr($row['Suborder'], " ", true),
                'order_id' => $order->id,
            ], [
                'taxonRank' => 'Suborder',
                'reference_id' => $subOrderReference->id
            ]);
        }

        if ($row['Infraorder'] != null) {
            $infraOrderReference = InsectReference::firstOrCreate(['title' => $row['Infraorder'], 'date' => substr($row['Infraorder'], strrpos($row['Infraorder'], ' ') + 1) . "-01-01"]);
            $infraOrderAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Infraorder'], " ", false), ", ", true)]);
            $infraOrderReference->authors()->sync($infraOrderAuthor);
            $infraOrder = InsectInfraOrder::firstOrCreate([
                'infraOrder' => stristr($row['Infraorder'], " ", true),
                'sub_order_id' => isset($subOrder) ? $subOrder->id : null,
            ], [
                'taxonRank' => 'Infraorder',
                'reference_id' => $infraOrderReference->id
            ]);
        }

        if ($row['Superfamily'] != null) {
            $superFamilyReference = InsectReference::firstOrCreate(['title' => $row['Superfamily'], 'date' => substr($row['Superfamily'], strrpos($row['Superfamily'], ' ') + 1) . "-01-01"]);
            $superFamilyAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Superfamily'], " ", false), ", ", true)]);
            $superFamilyReference->authors()->sync($superFamilyAuthor);
            $superFamily = InsectSuperFamily::firstOrCreate([
                'superFamily' => stristr($row['Superfamily'], " ", true),
                'infra_order_id' => isset($infraOrder) ? $infraOrder->id : null,
            ], [
                'taxonRank' => 'Superfamily',
                'reference_id' => $superFamilyReference->id
            ]);
        }
        $familyReference = InsectReference::firstOrCreate(['title' => $row['Family'], 'date' => substr($row['Family'], strrpos($row['Family'], ' ') + 1) . "-01-01"]);
        $familyAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Family'], " ", false), ", ", true)]);
        $familyReference->authors()->sync($familyAuthor);
        $family = InsectFamily::firstOrCreate([
            'family' => stristr($row['Family'], " ", true),
            'order_id' => $order->id,
            'sub_order_id' => isset($subOrder) ? $subOrder->id : null,
            'infra_order_id' => isset($infraOrder) ? $infraOrder->id : null,
            'super_family_id' => isset($superFamily) ? $superFamily->id : null,
        ], [
            'taxonRank' => 'Family',
            'reference_id' => $familyReference->id
        ]);

        if ($row['Subfamily'] != null) {
            $subFamilyReference = InsectReference::firstOrCreate(['title' => $row['Subfamily'], 'date' => substr($row['Subfamily'], strrpos($row['Subfamily'], ' ') + 1) . "-01-01"]);
            $subFamilyAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Subfamily'], " ", false), ", ", true)]);
            $subFamilyReference->authors()->sync($subFamilyAuthor);
            $subFamily = InsectSubFamily::firstOrCreate([
                'subFamily' => stristr($row['Subfamily'], " ", true),
                'family_id' => $family->id,
                'taxonRank' => 'Subfamily',
            ], [
                'reference_id' => $subFamilyReference->id
            ]);
        }

        if ($row['Tribe'] != null) {
            $tribeReference = InsectReference::firstOrCreate(['title' => $row['Tribe'], 'date' => substr($row['Tribe'], strrpos($row['Tribe'], ' ') + 1) . "-01-01"]);
            $tribeAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Tribe'], " ", false), ", ", true)]);
            $tribeReference->authors()->sync($tribeAuthor);
            $tribe = InsectTribe::firstOrCreate([
                'tribe' => stristr($row['Tribe'], " ", true),
                'sub_family_id' => $subFamily->id,
            ], [
                'taxonRank' => 'Tribe',
                'reference_id' => $tribeReference->id
            ]);
        }

        $genusReference = InsectReference::firstOrCreate(['title' => $row['Genus'], 'date' => substr($row['Genus'], strrpos($row['Genus'], ' ') + 1) . "-01-01"]);
        $genusAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Genus'], " ", false), ", ", true)]);
        $genusReference->authors()->sync($genusAuthor);
        $genus = InsectGenus::firstOrCreate([
            'genus' => stristr($row['Genus'], " ", true),
            'family_id' => $family->id,
            'sub_family_id' => isset($subFamily) ? $subFamily->id : null,
            'tribe_id' => isset($tribe) ? $tribe->id : null,
        ], [
            'taxonRank' => 'Genus',
            'reference_id' => $genusReference->id
        ]);

        if ($row['Subgenus'] != null) {
            $subgenusReference = InsectReference::firstOrCreate(['title' => $row['Subgenus'], 'date' => substr($row['Subgenus'], strrpos($row['Subgenus'], ' ') + 1) . "-01-01"]);
            $subgenusAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Subgenus'], " ", false), ", ", true)]);
            $subgenusReference->authors()->sync($subgenusAuthor);
            $subgenus = InsectSubGenus::firstOrCreate([
                'subGenus' => stristr($row['Subgenus'], " ", true),
                'genus_id' => $genus->id,
            ], [
                'taxonRank' => 'Subgenus',
                'reference_id' => $subgenusReference->id
            ]);
        }

        $specieReference = InsectReference::firstOrCreate(['title' => $row['Species'], 'date' => substr($row['Species'], strrpos($row['Species'], ' ') + 1) . "-01-01"]);
        $speciesAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Species'], " ", false), ", ", true)]);
        $specieReference->authors()->sync($speciesAuthor);
        $specie = InsectSpecific::firstOrCreate([
            'specific' => stristr($row['Species'], " ", true),
            'genus_id' => $genus->id,
            'sub_genus_id' => isset($subgenus) ? $subgenus->id : null,
        ], [
            'taxonRank' => 'Species',
            'reference_id' => $specieReference->id
        ]);

        if ($row['Subespecies'] != null) {
            $subspecieReference = InsectReference::firstOrCreate(['title' => $row['Subespecie'], 'date' => substr($row['Subespecie'], strrpos($row['Subespecie'], ' ') + 1) . "-01-01"]);
            $subspecieAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Subespecie'], " ", false), ", ", true)]);
            $subspecieReference->authors()->sync($subspecieAuthor);
            $subspecie = InsectInfraSpecific::firstOrCreate([
                'infraSpecific' => stristr($row['Subspecies'], " ", true),
                'specific_id' => $specie->id,
            ], [
                'taxonRank' => 'Subspecie',
                'reference_id' => $subspecieReference->id
            ]);
        }

        /*   
        if ($row['synonymes'] != null) {
            $synonymeReference = InsectReference::firstOrCreate(['title' => $row['synonymes'], 'date' => substr($row['synonymes'], strrpos($row['synonymes'], ' ') + 1) . "-01-01"]);
            $synonymeAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['synonymes'], " ", false), ", ", true)]);
            $synonymeReference->authors()->sync($synonymeAuthor);
            $synonyme = InsectInfraSpecific::firstOrCreate([
                'infraSpecific' => stristr($row['Subspecies'], " ", true),
                'specific_id' => $specie->id,
            ], [
                'taxonRank' => 'Subspecie',
                'reference_id' => $subspecieReference->id
            ]);
        }

        if ($row['synonymes2'] != null) {
            $subspecieReference = InsectReference::firstOrCreate(['title' => $row['Subespecie'], 'date' => substr($row['Subespecie'], strrpos($row['Subespecie'], ' ') + 1) . "-01-01"]);
            $subspecieAuthor = InsectAuthor::firstOrCreate(['name' => stristr(stristr($row['Subespecie'], " ", false), ", ", true)]);
            $subspecieReference->authors()->sync($subspecieAuthor);
            $subspecie = InsectSinonimia::firstOrCreate([
                'specific_id' => $specie->id,
                'infraSpecific' => stristr($row['Subspecies'], " ", true),
            ], [
                'taxonRank' => 'Subspecie',
                'reference_id' => $subspecieReference->id
            ]);
        }

        */

        return;
    }

    public function batchSize(): int
    {
        return 100;
    }
}
