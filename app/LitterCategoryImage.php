<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image as IMG;
use App\LitterCategoryImageType;
use App\Helper;

class LitterCategoryImage extends Model
{
    protected $fillable = ['url', 'litter_category_id', 'litter_category_image_type_id'];
    public function category()
    {
        return $this->belongsTo('App\LitterCategory', 'litter_category_id');
    }

    public function type()
    {
        return $this->belongsTo('App\LitterCategoryImageType', 'litter_category_image_type_id');
    }

    public static function generateImage($image, $filename, $bg, $category, $path, $offset = true)
    {
        $background = IMG::make(public_path('images/litter-reporter/background/' . $bg));
        $position = [$offset ? 'bottom' : 'center', $offset ? 100 : 0];

        $background->insert($image, $position[0], 0, $position[1])->save(public_path($path . $filename));

        LitterCategoryImage::create([
            'url' => $path . $filename,
            'litter_category_id' => $category,
            'litter_category_image_type_id' => LitterCategoryImageType::whereName($bg)->value('id'),
        ]);
    }

    public static function generateImages($image, $category_id)
    {
        $imageInstance = IMG::make($image)->encode('png', 100)->resize(null, 150, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        })->crop(150, 150);

        $blue = IMG::canvas(150, 150)->fill('rgb(60,67,114)')->mask($imageInstance, true);
        $red = IMG::canvas(150, 150)->fill('rgb(210,81,82)')->mask($imageInstance, true);

        self::generateImage($imageInstance, $category_id . '.png', "ios",  $category_id, '/images/litter-reporter/ios/', false);
        self::generateImage($imageInstance, $category_id . '.png', "menu",  $category_id, '/images/litter-reporter/menu/', false);
        self::generateImage($imageInstance, $category_id . '.png', "android-self",  $category_id, '/images/litter-reporter/android/self/');
        self::generateImage($blue, $category_id . 'p.png', "android-self-picked",  $category_id, '/images/litter-reporter/android/self/');
        self::generateImage($imageInstance, $category_id . '.png', "android-others",  $category_id, '/images/litter-reporter/android/other/');
        self::generateImage($red, $category_id . 'p.png', "android-others-picked",  $category_id, '/images/litter-reporter/android/other/');
    }
}
