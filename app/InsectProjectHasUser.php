<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectProjectHasUser extends Model
{
    protected $connection = 'mysql_insect';

    protected $fillable = [
        'project_id',
        'user_id',
        'experienceLevel'
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function projects()
    {
        return $this->belongsTo('App\InsectProject');
    }
}
