<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectOccurrence extends Model
{
    use FiltersRecords;

    protected $fillable = [
        'basisOfRecord',
        'dateType',
        'eventDateStart',
        'eventDateEnd',
        'endDayOfYear',
        'verbatimEventDate',
        'eventRemarks',
        'specific_id',
        'infra_specific_id',
        'reference_id',
        'dateIdentified',
        'identificationRemarks',
        'previousIdentifications',
        'decimalLatitude',
        'decimalLongitude',
        'geodeticDatum',
        'coordinateUncertaintyInMeters',
        'verbatimCoordinates',
        'verbatimCoordinateSystem',
        'georeferencedBy',
        'georeferencedDate',
        'georeferenceProtocol',
        'georeferenceSources',
        'georeferenceRemarks',
        'georeferenceVerificationStatus',
        'higherGeography',
        'continent',
        'islandGroup',
        'island',
        'biogeographicRealm',
        'country',
        'countryCode',
        'region',
        'province',
        'county',
        'geographicRemarks',
        'locality',
        'verbatimLocality',
        'locationAccordingTo',
        'localityRemarks',
        'elevationMin',
        'elevationMax',
        'elevationMethod',
        'elevationAccuracy',
        'verbatimElevation',
        'type',
        'numbers',
        'machineSpecifics',
        'language',
        'license',
        'institutionId',
        'institutionCode',
        'collectionCode',
        'catalogNumber',
        'individualCount',
        'maleCount',
        'femaleCount',
        'imatureCount',
        'exuvias',
        'organismQuantity',
        'organismQuantityType',
        'establishmentMeans',
        'preparations',
        'habitat',
        'habitatRemarks',
        'microHabitats',
        'biologicalAssociation',
        'biologicalRemarks',
        'samplingMethod',
        'typeOfSampling',
        'tripCode',
        'dataAttributes',
        'materialType',
        'cataloguedBy',
        'dataValidation',
        'identifiedBy',
        'project_id',
        'last_edit_by',
        'occurrenceRemarks',
        'recordNumber',
        'preparationType',
        'preparedBy',
        'preparationDate',
        'preparationRemarks',
        'authorExperience'
    ];
    protected $connection = 'mysql_insect';

    public function specific()
    {
        return $this->hasOne(InsectSpecific::class);
    }

    public function subSpecific()
    {
        return $this->hasOne(InsectInfraSpecific::class);
    }

    public function reference()
    {
        return $this->hasOne(InsectReference::class);
    }

    public function recordedBy()
    {
        return $this->belongsToMany('App\InsectAuthor', 'insect_occurrence_has_recorded_by_authors', 'occurrence_id', 'author_id');
    }

    public function project()
    {
        return $this->belongsTo('App\InsectProject');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User');
    }

    public function lastEditBy()
    {
        return $this->belongsTo('App\User');
    }
}
