<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GpsGatewaySatellite extends Model
{
    protected $fillable = [
        'satID',
        'pseudorange',
        'positionX',
        'positionY',
        'positionZ',
        'velocityX',
        'velocityY',
        'velocityZ',
        'iot_report_id',
        'gpsWeek',
        'gpsTimeWeek_M7',
        'gpsTimeWeek_M41',
        'clockBias_M7',
        'clockBias_M30',
        'gpsTime',
        'gpsSoftwareTime'
    ];
    protected $connection = 'mysql_iot';

    public function reports()
    {
        return $this->belongsTo('App\IotReport', 'iot_report_id');
    }
}
