<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotTemplate extends Model
{
    protected $connection = 'mysql_iot';

    public function devices()
    {
        return $this->hasMany(IotDevice::class, 'template_id');
    }

    public function sections()
    {
        return $this->belongsToMany(IotSection::class, 'iot_template_has_sections', 'template_id', 'section_id');
    }
}
