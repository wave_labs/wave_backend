<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClimarestStakeholdersForm extends Model
{
    protected $fillable = [
        "climarest_id",
        "category",
        "affect",
        "affectRestoration",
        "engage",
        "levelInfluence",
        "levelInterest",
        "levelTrust",
        "name",
        "priority",
        "role",
        "type",
        "typeEngagement",
    ];

    public static function store($validator, $climarest)
    {
        foreach ($validator as $key => $stakeholderCat) {
            foreach ($stakeholderCat as $stakeholder) {
                self::create([
                    'climarest_id' => $climarest,
                    'category' => $key,
                    'affect' => $stakeholder["affect"],
                    'affectRestoration' => $stakeholder["affectRestoration"],
                    'engage' => implode(", ", $stakeholder["engage"]),
                    'levelInfluence' => $stakeholder["levelInfluence"],
                    'levelInterest' => $stakeholder["levelInterest"],
                    'levelTrust' => $stakeholder["levelTrust"],
                    'name' => $stakeholder["name"],
                    'priority' => $stakeholder["priority"],
                    'role' => $stakeholder["role"],
                    'type' => $stakeholder["type"],
                    'typeEngagement' => implode(", ", $stakeholder["typeEngagement"]),
                ]);
            }
        }
    }

    public function climarest()
    {
        return $this->belongsTo('App\Climarest');
    }
}
