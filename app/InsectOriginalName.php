<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectOriginalName extends Model
{
    protected $fillable = [
        'specific_id',
        'infra_specific_id',
        'originalNameGenus',
        'originalNameSubGenus',
        'originalNameSpecificEpithet',
        'originalNameInfraSpecificEpithet',
        'originalNameDate',
        'originalNameReference_id',
        'originalNameTaxonRank',
        'originalNameTaxonRemarks',
        'originalNameLanguage',
        'originalNameRights',
        'originalNameLicense',
        'originalNameInstitutionID',
        'originalNameInstitutionCode',
        'originalNameRightSholder',
        'originalNameAccessRights',
        'originalNameSource',
        'originalNameDatasetID',
        'originalNameDatasetName',
    ];
    protected $connection = 'mysql_insect';

    public function specie()
    {
        return $this->belongsTo(InsectSpecific::class, 'specific_id');
    }

    public function subspecie()
    {
        return $this->belongsTo(InsectInfraSpecific::class, 'infra_specific_id');
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'originalNameReference_id');
    }
}
