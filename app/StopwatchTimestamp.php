<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StopwatchTimestamp extends Model
{
    protected $fillable = ['start_time', 'end_time', 'class_id', 'result_id'];

    public function class()
    {
        return $this->belongsTo('App\StopwatchClass', 'class_id');
    }

    public function result()
    {
        return $this->hasMany('App\StopwatchResult', 'result_id');
    }
}
