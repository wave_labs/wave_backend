<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClimarestAreaCoordinates extends Model
{
    protected $fillable = [
        "climarest_area_form_id",
        "latitude",
        "longitude",
        "label"
    ];
    public static function store($validator, $area)
    {
        foreach ($validator as $key) {
            self::create([
                'climarest_area_form_id' => $area,
                'latitude' => $key["lat"],
                'longitude' => $key["lng"],
                'label' => $key["label"],
            ]);
        }
    }

    public function area()
    {
        return $this->belongsTo('App\ClimarestAreaForm');
    }
}
