<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectClass extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'class', 'phylum_id', 'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function phylum()
    {
        return $this->belongsTo(InsectPhylum::class, 'phylum_id');
    }

    public function orders()
    {
        return $this->hasMany(InsectOrder::class);
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
