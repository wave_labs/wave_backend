<?php

namespace App\Observers;

use App\Capsule;
use Storage;
class CapsuleObserver
{
    /**
     * Handle the capsule "created" event.
     *
     * @param  \App\Capsule  $capsule
     * @return void
     */
    public function created(Capsule $capsule)
    {
        //
    }

    /**
     * Handle the capsule "updated" event.
     *
     * @param  \App\Capsule  $capsule
     * @return void
     */
    public function updated(Capsule $capsule)
    {
        //
    }

    /**
     * Handle the capsule "deleted" event.
     *
     * @param  \App\Capsule  $capsule
     * @return void
     */
    public function deleted(Capsule $capsule)
    {
        Storage::delete('public/' . $capsule->image_link);
    }

    /**
     * Handle the capsule "restored" event.
     *
     * @param  \App\Capsule  $capsule
     * @return void
     */
    public function restored(Capsule $capsule)
    {
        //
    }

    /**
     * Handle the capsule "force deleted" event.
     *
     * @param  \App\Capsule  $capsule
     * @return void
     */
    public function forceDeleted(Capsule $capsule)
    {
        //
    }
}
