<?php

namespace App\Observers;

use App\Sighting;
use Illuminate\Support\Facades\Storage;

class SightingObserver
{
    /**
     * Handle the sighting "created" event.
     *
     * @param  \App\Sighting  $sighting
     * @return void
     */
    public function created(Sighting $sighting)
    {
        //
    }

    /**
     * Handle the sighting "updated" event.
     *
     * @param  \App\Sighting  $sighting
     * @return void
     */
    public function updated(Sighting $sighting)
    {
        //
    }

    /**
     * Handle the sighting "deleted" event.
     *
     * @param  \App\Sighting  $sighting
     * @return void
     */
    public function deleted(Sighting $sighting)
    {
       Storage::deleteDirectory('public/sighting/' . $sighting->id);
    }

    /**
     * Handle the sighting "restored" event.
     *
     * @param  \App\Sighting  $sighting
     * @return void
     */
    public function restored(Sighting $sighting)
    {
        //
    }

    /**
     * Handle the sighting "force deleted" event.
     *
     * @param  \App\Sighting  $sighting
     * @return void
     */
    public function forceDeleted(Sighting $sighting)
    {
        //
    }
}
