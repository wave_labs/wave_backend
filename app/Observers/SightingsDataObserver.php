<?php

namespace App\Observers;

use App\SightingsData;
use Illuminate\Support\Facades\Storage;

class SightingsDataObserver
{
    /**
     * Handle the sightings data "created" event.
     *
     * @param  \App\SightingsData  $sightingsData
     * @return void
     */
    public function created(SightingsData $sightingsData)
    {
        //
    }

    /**
     * Handle the sightings data "updated" event.
     *
     * @param  \App\SightingsData  $sightingsData
     * @return void
     */
    public function updated(SightingsData $sightingsData)
    {
        //
    }

    /**
     * Handle the sightings data "deleted" event.
     *
     * @param  \App\SightingsData  $sightingsData
     * @return void
     */
    public function deleted(SightingsData $sightingsData)
    {
       Storage::delete('public/' . $sightingsData->link);
    }

    /**
     * Handle the sightings data "restored" event.
     *
     * @param  \App\SightingsData  $sightingsData
     * @return void
     */
    public function restored(SightingsData $sightingsData)
    {
        //
    }

    /**
     * Handle the sightings data "force deleted" event.
     *
     * @param  \App\SightingsData  $sightingsData
     * @return void
     */
    public function forceDeleted(SightingsData $sightingsData)
    {
        //
    }
}
