<?php

namespace App\Observers;

use App\UserCertificate;

class UserCertificateObserver
{
    /**
     * Handle the user certificates "created" event.
     *
     * @param  \App\UserCertificate  $userCertificate
     * @return void
     */
    public function created(UserCertificate $userCertificate)
    {
        //
    }

    /**
     * Handle the user certificates "updated" event.
     *
     * @param  \App\UserCertificate  $userCertificate
     * @return void
     */
    public function updated(UserCertificate $userCertificate)
    {
        //
    }

    /**
     * Handle the user certificates "deleted" event.
     *
     * @param  \App\UserCertificate  $userCertificate
     * @return void
     */
    public function deleted(UserCertificate $userCertificate)
    {
       Storage::delete('public/' . $userCertificate->link);
    }

    /**
     * Handle the user certificates "restored" event.
     *
     * @param  \App\UserCertificate  $userCertificate
     * @return void
     */
    public function restored(UserCertificate $userCertificate)
    {
        //
    }

    /**
     * Handle the user certificates "force deleted" event.
     *
     * @param  \App\UserCertificate  $userCertificate
     * @return void
     */
    public function forceDeleted(UserCertificate $userCertificate)
    {
        //
    }
}
