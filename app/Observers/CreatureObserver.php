<?php

namespace App\Observers;

use App\Creature;
use Storage;
class CreatureObserver
{
    /**
     * Handle the creature "created" event.
     *
     * @param  \App\Creature  $creature
     * @return void
     */
    public function created(Creature $creature)
    {
        //
    }

    /**
     * Handle the creature "updated" event.
     *
     * @param  \App\Creature  $creature
     * @return void
     */
    public function updated(Creature $creature)
    {
        //
    }

    /**
     * Handle the creature "deleted" event.
     *
     * @param  \App\Creature  $creature
     * @return void
     */
    public function deleted(Creature $creature)
    {
        Storage::delete('public/' . $creature->photo);
    }

    /**
     * Handle the creature "restored" event.
     *
     * @param  \App\Creature  $creature
     * @return void
     */
    public function restored(Creature $creature)
    {
        //
    }

    /**
     * Handle the creature "force deleted" event.
     *
     * @param  \App\Creature  $creature
     * @return void
     */
    public function forceDeleted(Creature $creature)
    {
        //
    }
}
