<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrainwaveLog extends Model
{
    protected $connection = 'mysql_brainwave';
    protected $fillable = ['message'];
}
