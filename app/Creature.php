<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Spatie\Translatable\HasTranslations;

class Creature extends Model
{
    use FiltersRecords, HasTranslations;

    public $fillable = ['type_id', 'name', 'name_scientific', 'description', 'curiosity', 'conserv', 'size', 'group_size', 'calves', 'nis_status', 'show_order'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public $translatable = ['name', 'description', 'curiosity', 'size'];

    public function activities()
    {
        return $this->belongsToMany('App\Activity', 'creature_activity_pivots');
    }

    public function enms()
    {
        return $this->hasMany('App\Enm');
    }

    public function sightings()
    {
        return $this->hasMany('App\Sighting');
    }

    public function dives()
    {
        return $this->belongsToMany('App\Dive', 'dive_pivots');
    }

    public function abundance()
    {
        return $this->hasOne('App\Abundance');
    }

    public function type()
    {
        return $this->belongsTo('App\CreatureType', 'type_id');
    }

    public function photos()
    {
        return $this->hasMany(CreaturePhoto::class);
    }

    public function sighting_photo()
    {
        return $this->hasManyThrough('App\SightingPhoto', 'App\Sighting');
    }

    public function survey()
    {
        return $this->hasOne('App\Survey');
    }

    public function image()
    {
        return $this->hasOne('App\Image');
    }

    public function audio()
    {
        return $this->hasOne('App\Audio');
    }

    public function video()
    {
        return $this->hasOne('App\Video');
    }

    public function markers()
    {
        return $this->hasMany('App\Marker');
    }

    public function freshwaterPins()
    {
        return $this->hasMany('App\FreshwaterPin');
    }

    public function size()
    {
        return $this->hasOne('App\CreatureSize');
    }

    public function dive_pivot()
    {
        return $this->hasMany('App\DivePivot');
    }

    public function sources()
    {
        return $this->belongsToMany('App\Source', 'creature_has_sources');
    }
}
