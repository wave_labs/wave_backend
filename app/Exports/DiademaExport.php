<?php

namespace App\Exports;

use App\Diadema;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\Http\QueryFilters\DiademaFilters;

class DiademaExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $filters = DiademaFilters::hydrate($this->request->query());
        return Diadema::filterBy($filters)->get();
    }

    public function headings(): array
    {

        return [
            'ID',
            'Date',
            'Percentage of dead sightings',
            'Percentage of sick sightings',
            'Latitude',
            'Longitude',
        ];
    }

    public function map($record): array
    {
        return [
            $record->id,
            $record->created_at,
            $record->dead,
            $record->sick,
            $record->latitude,
            $record->longitude,
        ];
    }

    public function title(): string
    {
        return 'Diadema Study';
    }
}
