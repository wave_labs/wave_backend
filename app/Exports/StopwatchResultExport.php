<?php

namespace App\Exports;

use App\Helper;
use App\Http\QueryFilters\StopwatchResultFilters;
use App\StopwatchClass;
use App\StopwatchResult;
use App\StopwatchTimestamp;
use App\StopwatchVideo;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class StopwatchResultExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    protected $classes = [];

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $filters = StopwatchResultFilters::hydrate($this->request->query());
        return StopwatchResult::filterBy($filters)->orderBy('video_id')->get();
    }

    public function __construct($request)
    {
        $this->request = $request;
        $this->classes = StopwatchClass::whereCategoryId(1)->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Occupation',
            'IUI',
            'Video',
            'Dolphins',
            'Whales',
            'Seals',
            'Seabirds',
            'Sea Turtles',
            'Dolphins',
            'Whales',
            'Seals',
            'Seabirds',
            'Sea Turtles',
        ];
    }

    public function map($record): array
    {
        $video = StopwatchVideo::find($record->video_id);
        $video_path = explode("/", $video->video);
        $video_name_with_extension = end($video_path);
        $video_name = explode(".", $video_name_with_extension);

        $response = [
            $record->id,
            $record->occupation,
            $video->detection,
            $video_name[0] . "(" . $record->video_id . ")",
            0,
            0,
            0,
            0,
            0,
            "",
            "",
            "",
            "",
            ""
        ];

        foreach ($this->classes as $index => $class) {
            $timestamps = StopwatchTimestamp::whereClassId($class->id)->whereResultId($record->id)->get();

            $string = "";
            foreach ($timestamps as $timestamp) {
                if (strlen($string))
                    $init = $string . " , ";
                else
                    $init = $string;

                $string = $init . "[" . $timestamp->start_time . ":" . $timestamp->end_time . "]";
            }

            $response[$index + 4] = $string;
        }

        foreach ($this->classes as $index => $class) {
            $timestamps = StopwatchTimestamp::whereClassId($class->id)->whereResultId($record->id)->get();
            $sum = 0;

            foreach ($timestamps as $timestamp) {
                $diff = $timestamp->end_time - $timestamp->start_time;
                $sum += $diff;
            }

            $response[$index + 9] = $sum;
        }

        return $response;
    }
}
