<?php

namespace App\Exports\Helper;

use App\MarkerCreatureSize;
use App\MarkerResult;
use App\CreatureSize;

class MarkerHelper
{
    public static function mergeCellsLoop($range, $sheet, $mergeColumns)
    {
        $start = 2;
        $end = 2;
        foreach ($range as $value) {
            $end = $start + $value;
            foreach ($mergeColumns as $column) {
                $sheet->mergeCells($column . $start . ':' . $column . $end);
            }
            $start = $end + 1;
        }

        return $end;
    }

    public static function saveActivity($row, $marker_activity)
    {
        array_push($row, $marker_activity->experience);
        array_push($row, $marker_activity->frequency);
        array_push($row, $marker_activity->time);

        return $row;
    }

    public static function saveForm($row, $record, $questions)
    {
        $creatureSizeRange = CreatureSize::whereCreatureId($record->creature->id)->first();
        $formSize = MarkerCreatureSize::whereMarkerId($record->id)->first();

        array_push($row,  'Less than ' . $creatureSizeRange->sm . ' ' . $creatureSizeRange->unit . ': ' . $formSize->sm .
            ', Between ' . $creatureSizeRange->sm . ' and ' . $creatureSizeRange->lg . ' ' . $creatureSizeRange->unit . ': ' . $formSize->md .
            ', Over ' . $creatureSizeRange->lg . ' ' . $creatureSizeRange->unit . ': ' . $formSize->lg);
        foreach ($questions as $question) {
            $answer =  MarkerResult::whereStudyQuestionId($question->id)->whereMarkerId($record->id)->value('answer');
            array_push($row, $answer ? $answer : '-----');
        }

        return $row;
    }
}
