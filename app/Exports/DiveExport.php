<?php

namespace App\Exports;

use App\Dive;
use App\Http\QueryFilters\DiveFilters;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Exports\Helper\MarkerHelper;
use App\Helper;

class DiveExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{

    use Exportable;

    protected $range = [];

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $filters = DiveFilters::hydrate($this->request->query());
        return Dive::filterBy($filters)->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Depth (m)',
            'Dive Time (m)',
            'Number of divers',
            'Date',
            'Diving Spot',
            'User',
            'Taxa',
            'Abundance'
        ];
    }

    public function map($record): array
    {
        $t = [];
        $response = [
            $record->id,
            $record->max_depth,
            $record->dive_time,
            $record->number_diver,
            $record->date,
            $record->diving_spot->name,
            $record->user->userable->name,
        ];

        foreach ($record->dive_pivot as $key => $dive) {
            $diveInfo = $response;
            array_push($diveInfo, $dive->creature->name);
            array_push($diveInfo, $dive->abundance_value);
            array_push($t, $diveInfo);
        }

        array_push($this->range, count($t) - 1);
        return $t;
    }

    public function styles(Worksheet $sheet)
    {
        //
    }

    public function title(): string
    {
        return 'Dive Reporter';
    }
}
