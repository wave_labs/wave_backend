<?php

namespace App\Exports;

use App\InsectReference;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class InsectExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    use Exportable;

    public function __construct($request)
    {
        $this->request = $request;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return InsectReference::all();
    }

    public function headings():array
    {
        return [
            'Reference ID',
            'Reference',
            'Year',
            'Authors',
            'Journal',
            'Volume', 
            'Page',
            'Species',
            'Reffered as',
            'Number of males',
            'Number of females',
            'Number of children',
            'Coordinates (lat, long)',
            'Region',
            'Province',
            'Locality',
            'Date'
        ];
    }
    public function map($record): array
    {
        
        $row = [];
        $response = [
            $record->id,
            $record->title,
            $record->year,
            $record->author,
            $record->journal,
            $record->volume
        ];

        foreach ($record->reports as $report) {
            $reportInfo = $response;
            array_push($reportInfo, $report->page); 
            array_push($reportInfo, $report->species->family->name . ' ' . $report->species->name); 
            array_push($reportInfo, $report->sinonimia->name); 
            array_push($reportInfo, $report->n_male); 
            array_push($reportInfo, $report->n_female);
            array_push($reportInfo, $report->n_children);
            array_push($reportInfo, [$report->latitude, $report->longitude]); 
            array_push($reportInfo, $report->zone->region); 
            array_push($reportInfo, $report->zone->province); 
            array_push($reportInfo, $report->zone->locality); 
            array_push($reportInfo, [$report->start, $report->end]); 
            array_push($row, $reportInfo);
        }
        return $row;

    }

    public function title(): string
    {
        return 'Insect Reports';
    }
}
