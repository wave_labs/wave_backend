<?php

namespace App\Exports;

use App\GpsTagSatellite;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class GpsTagSatelliteExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $from = Carbon::parse($this->request->date[0])->toDateTimeString();
        $to = Carbon::parse($this->request->date[1])->toDateTimeString();

        $device = $this->request->device;

        return GpsTagSatellite::with('reports')->whereHas('reports', function ($query) use ($device) {
            $query->where('id_device', $device);
        })->whereBetween('created_at',  [$from, $to])->get();
    }

    public function headings(): array
    {

        return [
            'ID',
            'satID',
            'pseudorange',
            'gpsWeek',
            'gpsTimeWeek',
        ];
    }

    public function map($record): array
    {
        return [
            $record->id,
            $record->satID,
            $record->pseudorange,
            $record->gpsWeek,
            $record->gpsTimeWeek,
        ];
    }

    public function title(): string
    {
        return 'Tag Data';
    }
}
