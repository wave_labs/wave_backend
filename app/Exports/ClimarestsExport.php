<?php

namespace App\Exports;

use App\ClimarestAreaForm;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use DB;
use Error;

class ClimarestsExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    use Exportable;

    public function __construct($request)
    {
        $this->request = $request;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return ClimarestAreaForm::all();
    }

    public function headings():array
    {
        return [
            'Which restoration area is this?',
            'Is the location inside a protected area? If yes, name area(s) and identify category(ies) of protection (e.g. Natura 2000, national park, nature reserve etc.)',
            'Describe the restoration area’s size (km2)',
            'What is the distance from shore – closest point, furthest point (km)',
            'Who has tenure / ownership / management responsibility of the marine space?',
            'Who has tenure / ownership of the adjacent land?', 
            'Who currently has marine resource access rights (such as permits, quotas, etc.) within the area of intervention?',
            'Will the intervention result in restrictions or changes to access and resource use?',
            'What other activities (e.g. recreational) are currently conducted within the area of intervention?',
            'Will the intervention result in restrictions or changes to these activities? If yes, please explain.',
            'Describe the aspects of the intervention that involve technological solutions.',
            'Describe the aspects of the intervention that involve nature-based solutions.',
            'How will the restoration decrease climate vulnerability?',
            'Have there been any conflicts over resource use, conservation, or restoration at this site before? If yes, please explain.',
            'Are any of the methods that you plan to use controversial? In what way/why? If so, which stakeholders have been involved?',
            'Your name and institution',
            'Category',
            'Type of stakeholder:',
            'Name of stakeholder:',
            'Role of stakeholder in the intervention area:',
            'Type of previous engagement',
            'How will the restoration activity affect this stakeholder (mechanism and extent, negative or positive effect)?',
            'How will this group affect the restoration activity (mechanism and extent, negative or positive effect)?',
            'Rank the level of interest this stakeholder has in the restoration activity',
            'Rank the level of influence this stakeholder has for the success of the restoration activity',
            'Rank the level of trust between your institution and this stakeholder [we are interested in your relationship with this stakeholder]',
            'To what extent would you suggest we prioritise contact with this stakeholder?',
            'How can we best engage with this stakeholder group?',
        ];
    }

    public function map($record): array
    {
        $row = [];
        $response = [
                $record->typeArea,
                $record->protectedArea,
                $record->restorationArea,
                $record->distanceShore,
                $record->ownership,
                $record->adjacentOwnership,
                $record->marineRights,
                $record->resultRestrictions,
                $record->areaActivities,
                $record->activitiesRestrictions,
                $record->technologicSolutions,
                $record->natureSolutions,
                $record->climateVulnerability,
                $record->conflicts,
                $record->contreversialMethods,
                $record->name
        ];
        $stakeholders = DB::table('climarest_stakeholders_forms')->where('climarest_id', '=', $record->climarest_id)->get();
        foreach($stakeholders as $stakeholder){
            $stakeholderInfo = $response;
            array_push($stakeholderInfo, $stakeholder->category); 
            array_push($stakeholderInfo, $stakeholder->type); 
            array_push($stakeholderInfo, $stakeholder->name); 
            array_push($stakeholderInfo, $stakeholder->role); 
            array_push($stakeholderInfo, $stakeholder->typeEngagement); 
            array_push($stakeholderInfo, $stakeholder->affect); 
            array_push($stakeholderInfo, $stakeholder->affectRestoration); 
            array_push($stakeholderInfo, $stakeholder->levelInterest); 
            array_push($stakeholderInfo, $stakeholder->levelInfluence); 
            array_push($stakeholderInfo, $stakeholder->levelTrust); 
            array_push($stakeholderInfo, $stakeholder->priority); 
            array_push($stakeholderInfo, $stakeholder->engage); 
            array_push($row, $stakeholderInfo);
        }
        return $row;
    }

    public function title(): string
    {
        return 'Climarest Stakeholder Reports';
    }
}
