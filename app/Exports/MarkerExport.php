<?php

namespace App\Exports;

use App\Activity;
use App\Helper;
use App\Marker;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\QueryFilters\MarkerFilters;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\MarkerActivity;
use App\MarkerCoordinates;
use App\StudyQuestion;

use App\Exports\Helper\MarkerHelper;

class MarkerExport implements FromCollection, WithHeadings, WithMapping, WithTitle, WithStyles
{
    use Exportable;

    protected $range = [];
    protected $records = [];
    protected $activities, $questions;
    protected $mergeColumns = [
        ['A', 'B', 'C', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q'],
        ['D', 'G', 'H', 'I']
    ];

    public function __construct($request)
    {
        $this->request = $request;
        $this->activities = $request->activity ?  Activity::whereId($request->activity)->get() : Activity::all();
        $this->questions = StudyQuestion::whereStudyId(1)->get();
    }

    public function collection()
    {
        $filters = MarkerFilters::hydrate($this->request->query());
        return Marker::filterBy($filters)->get();
    }

    public function headings(): array
    {

        $columns = [
            'ID',
            'date',
            'creature',
            'activity',
            'lat',
            'lon',
            'experience',
            'frequency',
            'time',
            'Em média, que tamanho tinha(m) o (s) peixe(s) que viu/capturou? *',
        ];

        foreach ($this->questions as $question) {
            array_push($columns, $question->question);
        }

        return $columns;
    }

    public function map($record): array
    {
        $response = [];
        $commonColumns = [
            $record->id,
            $record->created_at,
            $record->creature->name,
        ];

        foreach ($this->activities as $activity) {

            $marker_activity = MarkerActivity::whereActivityId($activity->id)->whereMarkerId($record->id)->first();

            if ($marker_activity) {
                $coords =  MarkerCoordinates::whereMarkerActivityId($marker_activity->id)->get();

                foreach ($coords as $coord) {
                    $row = $commonColumns;
                    array_push($row, $activity->name);
                    array_push($row, $coord->latitude);
                    array_push($row, $coord->longitude);
                    $row = MarkerHelper::saveActivity($row, $marker_activity);
                    $row = MarkerHelper::saveForm($row, $record, $this->questions);

                    array_push($response, $row);
                }
                array_push($this->records, count($coords) - 1);
            }
        };

        array_push($this->range, count($response) - 1);
        return $response;
    }

    public function styles(Worksheet $sheet)
    {
        $numRows = MarkerHelper::mergeCellsLoop($this->range, $sheet, $this->mergeColumns[0]);
        MarkerHelper::mergeCellsLoop($this->records, $sheet, $this->mergeColumns[1]);

        $sheet->getStyle('1:' . $numRows)->getAlignment()->setVertical('center');
    }

    public function title(): string
    {
        return 'Marker Study';
    }
}
