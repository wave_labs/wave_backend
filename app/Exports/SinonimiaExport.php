<?php

namespace App\Exports;

use App\InsectReport;
use Error;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SinonimiaExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    use Exportable;

    public function __construct($request)
    {
        $this->request = $request;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return InsectReport::all();
    }

    public function headings(): array
    {
        return [
            'Species',
            'Reffered as'
        ];
    }

    public function map($record): array
    {
        return [
            $record->species->family->name . " " . $record->species->name,
            $record->sinonimia->name
        ];
    }

    public function title(): string
    {
        return 'Insect Sinonimias';
    }
}
