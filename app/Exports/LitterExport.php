<?php

namespace App\Exports;

use App\Http\QueryFilters\LitterFilters;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Litter;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class LitterExport implements FromCollection, WithHeadings, WithMapping, WithStrictNullComparison
{
    use Exportable;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $filters = LitterFilters::hydrate($this->request->query());
        return Litter::filterBy($filters)->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'User',
            'Coordinates',
            'Date',
            'Quantity',
            'Radius',
            'Source',
            'Collected',
            'Multiple',
            'Categories',
            'SubCategories',
        ];
    }

    public function map($record): array
    {
        $categories = [];
        foreach ($record->litterCategory as $category) {
            array_push($categories, $category->name);
        }

        $subcategories = [];
        foreach ($record->litterSubCategory as $subcategory) {
            array_push($subcategories, $subcategory->name);
        }
        return [
            $record->id,
            $record->user,
            [$record->latitude, $record->longitude],
            $record->date,
            $record->quantity,
            $record->radius,
            $record->source,
            $record->collected,
            $record->multiple,
            $categories,
            $subcategories,
        ];
    }
}
