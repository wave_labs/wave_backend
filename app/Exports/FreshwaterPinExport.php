<?php

namespace App\Exports;

use App\FreshwaterPin;
use App\FreshwaterPinCoordinate;
use App\FreshwaterPinForm;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\QueryFilters\FreshwaterPinFilters;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\StudyQuestion;
use App\Exports\Helper\MarkerHelper;
use App\Helper;

class FreshwaterPinExport implements FromCollection, WithHeadings, WithMapping, WithTitle, WithStyles
{
    use Exportable;
    protected $range = [];
    protected $records = [];
    protected $mergeColumns = [
        ['A', 'B', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U'],
    ];

    public function __construct($request)
    {
        $this->request = $request;
        $this->questions = StudyQuestion::whereStudyId(4)->get();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $filters = FreshwaterPinFilters::hydrate($this->request->query());
        return FreshwaterPin::filterBy($filters)->get();
    }

    public function headings(): array
    {

        $columns = [
            'ID',
            'Date',
            'Image',
            'Timeframe',
            'Latitude',
            'Longitude',
        ];

        foreach ($this->questions as $question) {
            array_push($columns, $question->question);
        }

        return $columns;
    }

    public function map($record): array
    {
        $response = [];
        $commonColumns = [
            $record->id,
            $record->created_at,
        ];

        $coords =  FreshwaterPinCoordinate::whereFreshwaterPinId($record->id)->get();
        foreach ($coords as $coord) {
            $row = $commonColumns;
            array_push($row, $coord->image);
            array_push($row, $coord->timeframe ? "Over 5 years ago" : "In the last 5 years");
            array_push($row, $coord->latitude);
            array_push($row, $coord->longitude);

            foreach ($this->questions as $question) {
                $answer =  FreshwaterPinForm::whereStudyQuestionId($question->id)->whereFreshwaterPinId($record->id)->value('answer');
                array_push($row, $answer ? $answer : '-----');
            }

            array_push($response, $row);
        }
        array_push($this->records, count($coords) - 1);


        array_push($this->range, count($response) - 1);
        return $response;
    }

    public function styles(Worksheet $sheet)
    {
        $numRows = MarkerHelper::mergeCellsLoop($this->range, $sheet, $this->mergeColumns[0]);

        $sheet->getStyle('1:' . $numRows)->getAlignment()->setVertical('center');
    }

    public function title(): string
    {
        return 'Freshwater Pin Study';
    }
}
