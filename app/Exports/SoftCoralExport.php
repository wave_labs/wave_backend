<?php

namespace App\Exports;

use App\Exports\Helper\MarkerHelper;
use App\Http\QueryFilters\TroutFilters;
use App\SoftCoral;
use App\SoftCoralCoordinate;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\StudyQuestion;
use App\Trout;
use App\TroutCoordinate;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class SoftCoralExport  implements FromArray, WithHeadings, WithTitle
{
    use Exportable;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function array() : array
    {
        $withCoordinates = SoftCoralCoordinate::all();
        $withoutCoordinates = SoftCoral::doesntHave('coordinates')->get();

        $arr =  array();

        foreach ($withCoordinates as $key => $value) {

            array_push($arr, [
                'id' => $value->softCoral->id,
                'created_at' => $value->softCoral->created_at,
                'knowledge' => $value->softCoral->knowledge,
                'name' => $value->softCoral->name,
                'year' => $value->year,
                'dimensions' => $value->dimensions,
                'dimension_change' => $value->dimension_change,
                'depth' => $value->depth,
                'latitude' => $value->latitude,
                'longitude' => $value->longitude,
            ]);
        }

        foreach ($withoutCoordinates as $key => $value) {
            array_push($arr, [
                'id' => $value->id,
                'created_at' => $value->created_at,
                'knowledge' => $value->knowledge,
                'name' => $value->name,
                'year' => "",
                'dimensions' => "",
                'dimension_change' => "",
                'depth' => "",
                'latitude' => "",
                'longitude' => "",
            ]);
        }

        $arr = collect($arr)->sortBy('created_at')->reverse()->toArray();

        return $arr;
    }

    public function headings(): array
    {

        return [
            'ID',
            'Date',
            'Have you ever seen this species?',
            'Name of species',
            'Year',
            'Dimensions',
            'Dimension Change',
            'Depth',
            'Latitude',
            'Longitude',
        ];
    }

    // public function map($record): array
    // {
    //     logger($record);
    //     return [
    //         $record->id,
    //         $record->created_at,
    //         $record->knowledge,
    //         $record->name,
    //         $record->year,
    //         $record->dimensions,
    //         $record->dimension_change,
    //         $record->depth,
    //         $record->latitude,
    //         $record->longitude,
    //     ];
    // }

    /* public function styles(Worksheet $sheet)
    {
        // $numRows = MarkerHelper::mergeCellsLoop($this->range, $sheet, $this->mergeColumns[0]);

        // $sheet->getStyle('1:' . $numRows)->getAlignment()->setVertical('center');
    } */

    public function title(): string
    {
        return 'Soft Coral Study';
    }
}
