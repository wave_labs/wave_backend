<?php

namespace App\Exports;

use App\Http\QueryFilters\IotReportFilters;
use App\IotReport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\Helper;

class IotReportExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{

    use Exportable;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $filters = IotReportFilters::hydrate($this->request->query());

        return IotReport::filterBy($filters)->with('device.type')->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Device ID',
            'Device',
            'Collected At',
            'Created At',
            'Fields'
        ];
    }

    public function map($record): array
    {
        $commonColumns = [
            $record->id,
            $record->device->id,
            $record->device->name,
            $record->date,
            $record->created_at ? $record->created_at : "Not specified",
        ];

        foreach ($record->fields as $field) {
            array_push($commonColumns, $field->field->key . ": " . $field->value);
        }

        return $commonColumns;
    }

    public function title(): string
    {
        return 'IoT Reports';
    }
}
