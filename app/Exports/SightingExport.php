<?php

namespace App\Exports;

use App\Sighting;
use App\Http\QueryFilters\SightingFilters;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;


class SightingExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{

    use Exportable;

    protected $range = [];

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $filters = SightingFilters::hydrate($this->request->query());
        return Sighting::filterBy($filters)->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Date',
            'User',
            'Creature',
            'Group Size',
            'Creature Behaviour',
            'Vehicle',
            'Beaufort Scale',
        ];
    }

    public function map($record): array
    {
        return  [
            $record->id,
            $record->date,
            $record->user->userable->name,
            $record->creature->name,
            $record->group_size,
            $record->behaviour->behaviour,
            $record->vehicle->vehicle,
            (string) $record->beaufort_scale->scale,
        ];
    }

    public function title(): string
    {
        return 'Whale Reporter';
    }
}
