<?php

namespace App\Exports\Sheets;

use App\Activity;
use App\Marker;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\QueryFilters\MarkerFilters;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\MarkerActivity;

class MarkerActivitySheet implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    use Exportable;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $filters = MarkerFilters::hydrate($this->request->query());
        return Marker::filterBy($filters)->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'date',
            'creature',
            'activity',
            'experience',
            'frequency',
            'time'
        ];
    }

    public function map($record): array
    {
        $activities = $this->request->activity ? Activity::whereId($this->request->activity)->get() : Activity::all();

        $response = [];
        $commonColumns = [
            $record->id,
            $record->created_at,
            $record->creature->name,
        ];

        foreach ($activities as $activity) {
            $row = $commonColumns;
            $marker_activity = MarkerActivity::whereActivityId($activity->id)->whereMarkerId($record->id)->first();

            if ($marker_activity) {
                array_push($row, $activity->name);
                array_push($row, $marker_activity->experience);
                array_push($row, $marker_activity->frequency);
                array_push($row, $marker_activity->time);


                array_push($response, $row);
            }
        };
        return $response;
    }

    public function title(): string
    {
        return 'Activity';
    }
}
