<?php

namespace App\Exports\Sheets;

use App\CreatureSize;
use App\Marker;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\QueryFilters\MarkerFilters;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\MarkerCreatureSize;
use App\MarkerResult;
use App\StudyQuestion;

class MarkerFormSheet implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    use Exportable;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $filters = MarkerFilters::hydrate($this->request->query());
        return Marker::filterBy($filters)->get();
    }

    public function headings(): array
    {
        $questions = StudyQuestion::whereStudyId(1)->get();
        $columns = [
            'ID',
            'date',
            'creature',
            'Em média, que tamanho tinha(m) o (s) peixe(s) que viu/capturou? *',
        ];

        foreach ($questions as $question) {
            array_push($columns, $question->question);
        }
        return $columns;
    }

    public function map($record): array
    {
        $questions = StudyQuestion::whereStudyId(1)->get();

        $response = [];
        $commonColumns = [
            $record->id,
            $record->created_at,
            $record->creature->name,
        ];
        $row = $commonColumns;

        $creatureSizeRange = CreatureSize::whereCreatureId($record->creature->id)->first();
        $formSize = MarkerCreatureSize::whereMarkerId($record->id)->first();

        array_push($row,  'Less than ' . $creatureSizeRange->sm . ' ' . $creatureSizeRange->unit . ': ' . $formSize->sm .
            ', Between ' . $creatureSizeRange->sm . ' and ' . $creatureSizeRange->lg . ' ' . $creatureSizeRange->unit . ': ' . $formSize->md .
            ', Over ' . $creatureSizeRange->lg . ' ' . $creatureSizeRange->unit . ': ' . $formSize->lg);

        foreach ($questions as $question) {
            $answer =  MarkerResult::whereStudyQuestionId($question->id)->whereMarkerId($record->id)->value('answer');
            array_push($row, $answer ? $answer : '-----');
        }

        array_push($response, $row);

        return $response;
    }

    public function title(): string
    {
        return 'Form';
    }
}
