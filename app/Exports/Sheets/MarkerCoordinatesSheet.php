<?php

namespace App\Exports\Sheets;

use App\Activity;
use App\Helper;
use App\Marker;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\QueryFilters\MarkerFilters;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\MarkerActivity;
use App\MarkerCoordinates;

class MarkerCoordinatesSheet implements FromCollection, WithHeadings, WithMapping, WithTitle
{

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $filters = MarkerFilters::hydrate($this->request->query());
        return Marker::filterBy($filters)->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'date',
            'creature',
            'activity',
            'lat',
            'lon',
        ];
    }

    public function map($record): array
    {

        $activities = $this->request->activity ?  Activity::whereId($this->request->activity)->get() : Activity::all();
        $response = [];
        $commonColumns = [
            $record->id,
            $record->created_at,
            $record->creature->name,
        ];

        foreach ($activities as $activity) {

            $marker_activity_id = MarkerActivity::whereActivityId($activity->id)->whereMarkerId($record->id)->value('id');

            if ($marker_activity_id) {
                $coords =  MarkerCoordinates::whereMarkerActivityId($marker_activity_id)->get();
                foreach ($coords as $coord) {
                    $row = $commonColumns;
                    array_push($row, $activity->name);
                    array_push($row, $coord->latitude);
                    array_push($row, $coord->longitude);

                    array_push($response, $row);
                }
            }
        };
        return $response;
    }

    public function title(): string
    {
        return 'Coordinates';
    }
}
