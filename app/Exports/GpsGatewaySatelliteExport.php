<?php

namespace App\Exports;

use App\GpsGatewaySatellite;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class GpsGatewaySatelliteExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $from = Carbon::parse($this->request->date[0])->toDateTimeString();
        $to = Carbon::parse($this->request->date[1])->toDateTimeString();

        $device = $this->request->device;

        return GpsGatewaySatellite::with('reports')->whereHas('reports', function ($query) use ($device) {
            $query->where('id_device', $device);
        })->whereBetween('created_at',  [$from, $to])->get();
    }

    public function headings(): array
    {

        return [
            'ID',
            'satID',
            'gpsWeek',
            'gpsTimeWeek',
            'clockBias',
            'pseudorange',
            'positionX',
            'positionY',
            'positionZ',
            'velocityX',
            'velocityY',
            'velocityZ',
            'gpsTime',
            'gpsSoftwareTime',
            'device'
        ];
    }

    public function map($record): array
    {
        return [
            $record->id,
            $record->satID,
            $record->gpsWeek,
            $record->gpsTimeWeek_M7,
            $record->clockBias_M7,
            $record->pseudorange,
            $record->positionX,
            $record->positionY,
            $record->positionZ,
            $record->velocityX,
            $record->velocityY,
            $record->velocityZ,
            $record->gpsTime,
            $record->gpsSoftwareTime,
            $record->reports->id_device,
        ];
    }

    public function title(): string
    {
        return 'Gateway Data';
    }
}
