<?php

namespace App\Exports;

use App\Result;
use App\Feedback;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Http\Resources\ResultResource;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Carbon\Carbon;

class ResultExport implements FromCollection, WithHeadings, WithMapping, WithStrictNullComparison
{
	use Exportable;

	public function __construct($Idate, $Fdate)
    {
        $this->Idate = $Idate;
        $this->Fdate = $Fdate;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$from = Carbon::createFromFormat('Y-m-d', $this->Idate)->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $this->Fdate)->endOfDay();

        $results = Result::whereBetween('created_at', [$from, $to])->get();

        return ResultResource::collection($results);
    }

    public function headings(): array
    {
        return [
            'ID',
            'Form',
            'Feedback ID',
            'User ID',
            'User',
            'Question',
            'Rating',
        ];
    }

    /**
    * @var Result $result
    */
    public function map($result): array
    {
        return [
            $result->id,
            $result->question->form->form,
            $result->feedback->id,
            $result->feedback->user->id,
            $result->feedback->user->userable->name,
            $result->question->question,
            $result->rating,
        ];
    }
}
