<?php

namespace App\Exports;

use App\Exports\Helper\MarkerHelper;
use App\Http\QueryFilters\TroutFilters;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\StudyQuestion;
use App\Trout;
use App\TroutCoordinate;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class TroutExport  implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    use Exportable;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return TroutCoordinate::all();
    }

    public function headings(): array
    {

        return [
            'ID',
            'Date',
            'Have you ever seen this animal in Madeira',
            'Name of species A',
            'Name of species B',
            'Name of species C',
            'Species',
            'Year',
            'Stream',
            'Location',
            'Latitude',
            'Longitude',
        ];
    }

    public function map($record): array
    {
        return [
            $record->trout->id,
            $record->trout->created_at,
            $record->trout->knowledge,
            $record->trout->nameA,
            $record->trout->nameB,
            $record->trout->nameC,
            $record->animal,
            $record->year,
            $record->stream,
            $record->location,
            $record->latitude,
            $record->longitude,
        ];
    }

    /* public function styles(Worksheet $sheet)
    {
        // $numRows = MarkerHelper::mergeCellsLoop($this->range, $sheet, $this->mergeColumns[0]);

        // $sheet->getStyle('1:' . $numRows)->getAlignment()->setVertical('center');
    } */

    public function title(): string
    {
        return 'Trout Study';
    }
}
