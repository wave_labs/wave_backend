<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    

    // protected fillable fields
	protected $fillable = array(
        'name',
        'code',
        'continent',
        'active'
    );

    protected $hidden = ['created_at','updated_at'];

// =========
// RELATIONS

    // 1 COUNTRY = n WEESH USERS
    public function weeshUsers() {
    	return $this->hasMany(WeeshUser::class, 'country_id');
    }

    // 1 COUNTRY = n LOCATIONS
    public function locations() {
    	return $this->hasMany(Location::class, 'country_id');
    } 
}
