<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ZipArchive;
use Illuminate\Support\Facades\Storage;
use App\Helper;

class Acoustic extends Model
{

    protected $fillable = ['sighting_data_id', 'type', 'start_time', 'duration_s'];

    /**
     * Get the sighting that this photo belongs to
     */
    public function sighting()
    {
        return $this->belongsTo('App\Sighting');
    }

    public static function getSpectrograms($path)
    {
        return Storage::disk('wave-server')->allFiles($path);
    }

    public static function hardStoreSpectrograms($files, $path)
    {
        foreach ($files as $file) {
            $fileInstance = base64_encode(Storage::disk('wave-server')->get($file));
            Storage::disk('public')->put($path . basename($file), $fileInstance);
        }
    }

    public static function storeSpectrograms($files, $path)
    {
        foreach ($files as $file) {
            //Helper::printToConsole(basename($file));
            if (!file_exists(storage_path() . '/app/public' . $path . basename($file))) {
                //Helper::printToConsole("new");
                $fileInstance = base64_encode(Storage::disk('wave-server')->get($file));
                Storage::disk('public')->put($path . basename($file), $fileInstance);
            } else {
                //Helper::printToConsole("already exists");
            };
        }
    }

    public static function resetSpectrograms()
    { }

    public static function zipSpectrograms($path)
    {
        $validationPath = storage_path($path . 'validation/');
        $trainPath = storage_path($path . 'train/');

        $validationDirectory = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($validationPath));
        $trainDirectory = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($trainPath));

        $zip_file = storage_path('exports/spectrograms.zip');
        $zip = new ZipArchive;

        if ($zip->open($zip_file, ZipArchive::CREATE) === TRUE) {
            $zip = self::resetZipSpectrograms($zip);
            foreach ($validationDirectory as $directoryFile) {
                if (file_exists($directoryFile) && is_file($directoryFile)) {
                    $filePath = $directoryFile->getRealPath();
                    $relativePath = 'validation/' . basename($filePath);
                    $zip->addFile($filePath, $relativePath . ".png");
                }
            }
            foreach ($trainDirectory as $directoryFile) {
                if (file_exists($directoryFile) && is_file($directoryFile)) {
                    $filePath = $directoryFile->getRealPath();
                    $relativePath = 'train/' . basename($filePath);
                    $zip->addFile($filePath, $relativePath . ".png");
                }
            }
        }
        $zip->close();
    }

    public static function resetZipSpectrograms($zip)
    {
        $zip->deleteName('validation/');
        $zip->deleteName('train/');

        return $zip;
    }
}
