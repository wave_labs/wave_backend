<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripPosition extends Model
{
    protected $fillable = [
        'trip_id',
        'latitude',
        'longitude',
    ];

    //Relations
    public function trip()
    {
        return $this->belongsTo('App\Trip');
    }
}
