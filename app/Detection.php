<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detection extends Model
{
    protected $fillable =
    [
        'image_path',
        'image_width',
        'image_weight',
        'lat',
        'lon',
        'user_confirmation',
        'comment',
    ];

    public function object()
    {
        return $this->hasMany('App\OceanusObject');
    }
}
