<?php

namespace App;

use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasTranslations;

    protected $fillable = ['vehicle'];
    public $translatable = ['vehicle'];

    public function sightings()
    {
        return $this->hasMany('App\Sighting');
    }

    public function vehicle_type()
    {
        return $this->belongsTo('App\VehicleType');
    }
}
