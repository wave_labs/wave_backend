<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectReport extends Model
{
    use FiltersRecords;
    protected $casts = [
        'latitude' => 'float',
        'longitude' => 'float',
    ];

    protected $fillable = [
        'latitude', 'longitude', 'coordinates', 'n_male', 'n_female', 'n_children', 'page',
        'start', 'end', 'date', 'date_type',
        'species_id', 'zone_id', 'reference_id', 'sinonimia_id'
    ];
    protected $connection = 'mysql_insect';

    /*     public function user()
    {
        return $this->belongsTo('App\User');
    } */

    public function getDateFormat()
    {
        $dateArray = [
            "month" => "Y-m",
            "year" => "Y",
            "day" => "Y-m-d",
            "range" => "Y-m-d",
        ];

        return $dateArray[$this->date_type];
    }

    public function sinonimia()
    {
        return $this->belongsTo('App\InsectSinonimia', 'sinonimia_id');
    }

    public function species()
    {
        return $this->belongsTo('App\InsectSpecies', 'species_id');
    }

    public function zone()
    {
        return $this->belongsTo('App\InsectZone', 'zone_id');
    }

    public function references()
    {
        return $this->belongsTo('App\InsectReference', 'reference_id');
    }
}
