<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotFieldFactHasParameter extends Model
{
    protected $fillable = ['parameter_id', 'field_fact_id', 'value'];
    protected $connection = 'mysql_iot';
}
