<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectFamily extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'family',
        'order_id',
        'sub_order_id',
        'infra_order_id',
        'super_family_id',
        'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function subFamilies()
    {
        return $this->hasMany(InsectSubFamily::class);
    }

    public function genus()
    {
        return $this->hasMany(InsectGenus::class);
    }

    public function order()
    {
        return $this->belongsTo(InsectClass::class, 'order_id');
    }

    public function subOrder()
    {
        return $this->belongsTo(InsectSubOrder::class, 'sub_order_id');
    }

    public function infraOrder()
    {
        return $this->belongsTo(InsectInfraOrder::class, 'infra_order_id');
    }

    public function superFamily()
    {
        return $this->belongsTo(InsectSuperFamily::class, 'super_family_id');
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
