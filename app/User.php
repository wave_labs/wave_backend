<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\PasswordResetNotification;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Cerbero\QueryFilters\FiltersRecords;
use Spatie\Permission\Traits\HasRoles;
use App\UserPerson;
use App\UserDiveCenter;
use App\BelongsToMorph;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use FiltersRecords;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userable_type', 'email', 'password', 'note', 'active', 'is_verified', 'userable_id', 'user_occupation_id', 'token'
    ];

    protected $connection = 'mysql';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guard_name = 'api';


    /**
     *  Get the types of users
     *
     */
    public static function types()
    {
        return [
            'App\UserPerson' => UserPerson::class,
            'App\UserDiveCenter' => UserDiveCenter::class,
        ];
    }

    /**
     *  Create user and user type
     *
     * @var array
     */
    public static function create(array $attributes = [])
    {

        $userTypes = User::types();
        DB::beginTransaction();

        $newUserType = $userTypes[$attributes['userable_type']]::create($attributes);

        //$attributes['user_type'] = $newUserType->getMorphClass();
        $attributes['userable_id'] = $newUserType->id;
        $attributes['token'] = str_random(16);

        $model = static::query()->create($attributes);

        DB::commit();

        return $model;
    }

    public function occupation()
    {
        return $this->belongsTo('App\UserOccupation', 'user_occupation_id');
    }

    public function sightings()
    {
        return $this->hasMany('App\Sighting');
    }

    public function suggestions()
    {
        return $this->hasMany('App\Suggestions');
    }

    public function arDome()
    {
        return $this->hasMany('App\ArDome');
    }

    public function pending_points()
    {
        return $this->hasMany('App\PendingPoint');
    }

    public function feedback()
    {
        return $this->hasMany('App\Feedback');
    }

    public function certificates()
    {
        return $this->hasMany('App\UserCertificate');
    }

    public function report()
    {
        return $this->hasMany('App\Report');
    }

    public function aiDetection()
    {
        return $this->hasMany('App\AiDetections');
    }

    public function capsules()
    {
        return $this->hasMany('App\Capsule');
    }

    public function userable()
    {
        return $this->morphTo();
    }

    public function user_person()
    {
        return BelongsToMorph::build($this, UserPerson::class, 'userable');
    }

    public function user_dive_Center()
    {
        return BelongsToMorph::build($this, UserDiveCenter::class, 'userable');
    }

    public function dives()
    {
        return $this->hasMany('App\Dives');
    }

    public function stopwatch()
    {
        return $this->hasMany('App\Stopwatch');
    }

    public function iotDevices()
    {
        return $this->belongsToMany('App\IotDevice', 'wave_iot.iot_device_has_users');
    }

    public function insectReports()
    {
        return $this->hasMany('App\InsectReport', 'user_id');
    }

    public function divingSpots()
    {
        return $this->belongsToMany('App\UserHasDivingSpots', 'user_has_diving_spot');
    }

    public function insectProjectsOwned()
    {
        return $this->hasMany('App\InsectProject', 'owner_id');
    }

    public function insectProjectsPartOf()
    {
        return $this->hasMany('App\InsectProjectHasUser');
    }

    public function insectOccurrences()
    {
        return $this->hasMany('App\InsectOccurrence');
    }

    //user has many UserHasDivingSpot with user_id as key

    /**
     * Gets the rest of the user information depending on the type
     */
    // public function userType()
    // {
    //     return $this->morphTo('user_type', 'user_type_id');
    // }

    /**
     * Send a new  Password Reset Notification
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function updatePoints($points, $user_id)
    {
        $user = User::find($user_id);

        $user->points = $user->points + $points;

        if ($user->points <= 1000) {
            $user->level = 1;
        } else if ($user->points > 1000 && $user->points <= 10000) {
            $user->level = 2;
        } else if ($user->points > 10000 && $user->points <= 100000) {
            $user->level = 3;
        } else if ($user->points > 100000 && $user->points <= 500000) {
            $user->level = 4;
        } else if ($user->points > 500000) {
            $user->level = 5;
        }

        $user->save();

        return response()->json(['status' => 'success'], 200);
    }

    public static function isUserAdmin($token)
    {
        if ($token)
            $user = JWTAuth::setToken($token)->user();

        return $user->hasRole('admin') && true;
    }

    public static function getCurrentUser()
    {
        $user = auth()->user();
        //auth()->user() ? $user = auth()->user() : $user = User::getGuest();
        return  $user ? $user->id : null;
    }

    public static function getGuest()
    {
        return User::where('email', "guest@guest.wave")->firstOrFail()->id;
    }

    public static function getUserType($user)
    {
        if ($user->userable_type == "App\\UserPerson") {
            $user = UserPerson::find($user->userable_id);
        } else $user = UserDiveCenter::find($user->userable_id);

        return $user;
    }
}
