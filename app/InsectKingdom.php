<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectKingdom extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'kingdom',
        'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];

    protected $connection = 'mysql_insect';

    public function phylums()
    {
        return $this->hasMany(InsectPhylum::class);
    }

    public function reference()
    {
        return $this->belongsTo('App\InsectReference', 'reference_id');
    }
}
