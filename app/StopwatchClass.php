<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class StopwatchClass extends Model
{
    use FiltersRecords;

    protected $fillable = ['name', 'image', 'category_id'];

    protected $appends = ['order'];

    public function getOrderAttribute()
    {
        if ($this->category_id == 1) {
            return $this->id - 1;
        } else if ($this->category_id == 2) {
            return $this->id - 6;
        }
    }

    public function category()
    {
        return $this->belongsTo('App\StopwatchCategory', 'category_id');
    }

    public function timestamps()
    {
        return $this->hasMany('App\StopwatchTimestamp', 'class_id');
    }
}
