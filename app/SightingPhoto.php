<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SightingPhoto extends Model
{
	protected $fillable = ['sighting_id', 'url'];

    /**
    * Get the sighting where the photo were taken
    */
    public function sighting()
    {
        return $this->belongsTo('App\Sighting');
    }
}
