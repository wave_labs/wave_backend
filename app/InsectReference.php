<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectReference extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'title',
        'identifier',
        'description',
        'bibliographicCitation',
        'editorList',
        'date',
        'journal',
        'chapterTitle',
        'editor',
        'volume',
        'pages',
        'pageStart',
        'pageEnd',
        'edition',
        'publisher',
        'language',
        'subject',
        'taxonRemarks',
        'type',
        'validated',
        'datasetName',
        'datasetID',
        'modified',
        'institutionID',
        'collectionID',
        'institutionCode',
        'collectionCode',
        'ownerInstitutionCode',
        'issue',
        'degree',
        'university',
        'conferenceTittle',
        'accessDate',
        'project_id'
    ];
    protected $connection = 'mysql_insect';

    public function reports()
    {
        return $this->hasMany('App\InsectReport', 'reference_id');
    }

    public function keywords()
    {
        return $this->belongsToMany(InsectReferenceKeyword::class, 'insect_reference_has_keywords', 'reference_id', 'reference_keyword_id');
    }

    public function tags()
    {
        return $this->belongsToMany(InsectReferenceTag::class, 'insect_reference_has_tags', 'reference_id', 'reference_tag_id');
    }

    public function categories()
    {
        return $this->belongsToMany(InsectReferenceCategories::class, 'insect_reference_has_categories', 'reference_id', 'reference_category_id');
    }

    public function authors()
    {
        return $this->belongsToMany('App\InsectAuthor', 'insect_reference_has_authors', 'reference_id', 'author_id');
    }

    public function sinonimia()
    {
        return $this->hasMany(InsectSinonimia::class, 'reference_id');
    }

    public function project()
    {
        return $this->belongsTo('App\InsectProject');
    }
}
