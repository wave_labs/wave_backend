<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deserta extends Model
{
    protected $fillable = [
        'img_airport',
        'img_sky',
        'x_acc',
        'y_acc',
        'z_acc',
        'x_gyro',
        'y_gyro',
        'z_gyro',
        'temperature',
        'humidity',
        'timestamp'
    ];

    protected $table = "desertas";
}
