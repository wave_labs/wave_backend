<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
	protected $fillable = ['form', 'desc'];

	/**
	* The table associated with the model.
	*
	* @var string
	*/
    protected $table = 'forms';

    /**
    * Get all answers this user gave
    */
    public function question()
    {
        return $this->hasMany('App\Question');
    }

    /**
    * Get all answers this user gave
    */
    public function feedback()
    {
        return $this->hasMany('App\Feedback');
    }
}
