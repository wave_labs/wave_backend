<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

	/**
	* The table associated with the model.
	*
	* @var string
	*/
    protected $table = 'results';

    /**
    * Get feedback
    */
    public function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }

    /**
    * Get feedback forms
    */
    public function feedback()
    {
        return $this->belongsTo('App\Feedback', 'feedback_id');
    }
}
