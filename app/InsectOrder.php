<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectOrder extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'order', 'class_id', 'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function class()
    {
        return $this->belongsTo(InsectClass::class, 'class_id');
    }

    public function family()
    {
        return $this->hasMany(InsectFamily::class);
    }

    public function subOrders()
    {
        return $this->hasMany(InsectSubOrder::class);
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
