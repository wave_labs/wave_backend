<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class BoundingBoxClass extends Model
{
    use FiltersRecords;

    protected $fillable = ['name', 'image', 'category_id'];

    public function category()
    {
        return $this->belongsTo('App\StopwatchCategory', 'category_id');
    }

    public function coordinates()
    {
        return $this->hasMany('App\BoundingBoxCoordinate', 'class_id');
    }
}
