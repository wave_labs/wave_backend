<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectOccurrenceIdentifications extends Model
{
    protected $fillable = [
        'occurrence_id',
        'author_id',
        'authorExperience',
        'species_id',
        'sub_species_id',
        'dateIdentified',
        'creator_id'
    ];
    protected $connection = 'mysql_insect';
}
