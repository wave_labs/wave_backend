<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotDeviceCategory extends Model
{
    protected $connection = 'mysql_iot';

    public function devices()
    {
        return $this->belongsToMany(IotDevice::class, 'iot_device_has_categories')->withPivot('latitude', "longitude");
    }
}
