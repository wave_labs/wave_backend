<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Spatie\Translatable\HasTranslations;

class CreatureType extends Model
{
    use FiltersRecords;
    use HasTranslations;

    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $translatable = ['taxa_category'];
    protected $fillable =
    [
        'taxa_category',
        'acoustic_category',
        'acoustic_data',
        'image'
    ];

    protected $appends = ['imagePath'];

    public function getImagePathAttribute()
    {

        return '/storage' . $this->image;
    }

    /*
    Get all the known creatures with this type
    */
    public function creatures()
    {
        return $this->hasOne('App\Creatures');
    }
}
