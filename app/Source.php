<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class Source extends Model
{
    use FiltersRecords;

    protected $fillable = ['name'];

    public function feedbacks()
    {
        return $this->hasMany('App\Feedback');
    }

    public function creatures()
    {
        return $this->belongsToMany('App\Creature', 'creature_has_sources');
    }

    public function suggestions()
    {
        return $this->hasMany('App\Suggestion');
    }

    public static function getSourceFromName($name)
    {
        return Source::where('name', $name)->value('id');
    }
}
