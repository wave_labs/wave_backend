<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TroutCoordinate extends Model
{
    protected $fillable = [
        'latitude',
        'longitude',
        'trout_id',
        'animal',
        'stream',
        'year',
        'location'
    ];

    public function trout()
    {
        return $this->belongsTo('App\Trout');
    }
}
