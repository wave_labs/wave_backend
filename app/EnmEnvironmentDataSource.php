<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnmEnvironmentDataSource extends Model
{
    //

    public function enm()
    {
        return $this->hasMany('App\Enm', 'enm_environment_data_source_id');
    }
}
