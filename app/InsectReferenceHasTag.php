<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectReferenceHasTag extends Model
{
    protected $fillable =
    [
        'reference_id',
        'reference_tag_id',
    ];

    protected $connection = 'mysql_insect';

    public function tags()
    {
        return $this->hasMany('App\InsectReferenceTag');
    }

    public function references()
    {
        return $this->hasMany('App\InsectReference');
    }
}
