<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class IotDeviceState extends Model
{
    use FiltersRecords;
    /**
	* The table associated with the model.
	*
	* @var string
	*/
    protected $table = 'iot_device_state';
    protected $connection = 'mysql_iot';
    protected $fillable = [
        'name'
    ];
}
