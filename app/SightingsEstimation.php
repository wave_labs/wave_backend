<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SightingsEstimation extends Model
{

	/**
	* The table associated with the model.
	*
	* @var string
	*/
    protected $table = 'sighting_estimation';


    /**
    * Get the sighting that is being used to estimate
    */
    public function sighting()
    {
        return $this->belongsTo('App\Sighting');
    }
}
