<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreatureSize extends Model
{
    protected $fillable = ['unit', 'sm', 'lg', 'creature_id'];

    public function creature()
    {
        return $this->belongsTo('App\Creature');
    }
}
