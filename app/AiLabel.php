<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class AiLabel extends Model
{
    use FiltersRecords;

    protected $fillable = ['name'];

    public static function createOrGet($aLabel)
    {
        $label = AiLabel::whereName($aLabel)->first();
        if (!$label) {
            $label = AiLabel::create(['name' => $aLabel]);
        }
        return $label;
    }

    // RELATIONSHIPS

    public function models()
    {
        return $this->belongsToMany('App\AiModel', 'ai_model_has_labels');
    }

    public function detections()
    {
        return $this->hasMany('App\AiDetection');
    }

    public function classifications()
    {
        return $this->hasMany('App\AiDetectionClassification', 'ai_label_id');
    }
}
