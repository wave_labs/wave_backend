<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PendingPoint extends Model
{
	protected $fillable =
    [
        'user_id',
        'points',
        'litter_id',
    ];

    /**
	* The table associated with the model.
	*
	* @var string
	*/
    protected $table = 'pending_points';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function litter()
    {
        return $this->belongsTo('App\Litter');
    }

    public static function calculate($request, $type, $source_id)
    {

    	if ($type == 'litter') 
    	{
    		$points = [
			  'latitude' => 5,
			  'longitude' => 5,
			  'materials' => 50,
			  'name' => 100,
			  'collected' => 1000,
			  // litter points attribution 
			];
    	}
    	else if ($type == 'sighting') 
    	{
    		$points = [
              'latitude' => 5,
              'longitude' => 5,
			  'beaufort_scale_id' => 10,
			  'date' => 10,
			  'vehicle_id' => 50,
			  'notes' => 50,
			  'is_group_mixed' => 50,
			  'group_size' => 100,
			  'behaviour' => 100,
              'creature_id' => 100,
			  'calves' => 100,
			  // sighting points attribution 
			];
    	}
    	else if ($type == 'dive') 
    	{
    		$points = [
			  'diving_spot_id' => 150,
			  'max_depth' => 150,
			  'dive_time' => 110,
			  'number_diver' => 110,
			  'abundance_value' => 200,
			  // dive points attribution 
			];
    	}

		$score = 0;
		foreach ($points as $key => $value) 
		{
			if (!is_null($request->$key) && $request->$key) 
			{
				$score += $value;
			}
		}

		if ($type == 'litter' && $request->collected) 
		{
			$pending = new Request([
	            'user_id' => $request->user_id,
	            'points' => $score,
	            'litter_id' => $source_id,
	        ]);

	        PendingPoint::create($pending->toArray());

			return 0;
		}

		return $score;
    }

    public static function approval($pending_point_id)
    {
    	$model = self::query()->find($pending_point_id);

    	$model->archived = true;

    	$model->save();

    	return $model->points;
    }
}
