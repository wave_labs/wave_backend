<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Response;

class Audio extends Model
{
    protected $fillable =
    [
        'type',
        'file',
        'url',
        'creature_id'
    ];

    public function creature()
    {
        return $this->belongsTo('App\Creature');
    }

    public static function getAudio($index, $class)
    {
        $file = storage_path('/audio/' . $class . '/' . $index . '.wav');

        $response = Response::make($file, 200);
        $response->header('Content-Type', "audio/wav");
        return $response;
    }
}
