<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectReferenceKeyword extends Model
{
    protected $fillable = [
        'keyword'
    ];

    protected $connection = 'mysql_insect';

    public function references(){
        return $this->belongsToMany('App\InsectReference', 'insect_Reference_has_keywords');
    }
}
