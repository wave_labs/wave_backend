<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomeHasInteraction extends Model
{
    //

    public function items()
    {
        return $this->hasMany('App\DomeItem', 'dome_interaction_id');
    }

    public static function incrementCounter($dome, $interaction)
    {

        $counter = self::findFromParams($dome, $interaction)->first();
        $counter->increment('counter');

        return $counter;
    }

    public static function decrementCounter($dome, $interaction)
    {
        $counter = self::findFromParams($dome, $interaction)->first();

        if ($counter->counter > 0) {
            $counter->decrement('counter');
        }

        return $counter;
    }

    public static function findFromParams($dome, $interaction)
    {
        return self::whereDomeId($dome)->whereDomeInteractionId($interaction);
    }
}
