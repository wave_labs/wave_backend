<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Carbon\Carbon;
use App\Source;
use App\Creature;


class DivingSpot extends Model
{
    use FiltersRecords;

    protected $guarded =
    [
        'id',
        'create_at',
        'update_at',
        'deleted_at'
    ];
    protected $fillable =
    [
        'longitude',
        'latitude',
        'range',
        'name',
        'description',
        'protection',
        'validated',
        'old'
    ];

    public function divingSpot()
    {
        return $this->hasOne('App\DivingSpot');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_has_diving_spots');
    }

    public function substracts()
    {
        return $this->belongsToMany(DivingSpotSubstract::class, 'diving_spot_has_substracts');
    }

    public function creaturesAbundance()
    {
        $date = Carbon::now()->subMonths(12);
        $total = [];

        for ($i = 0; $i <= 11; $i++) {
            $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
            $last = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();

            $abundance_values = [];
            $values = [];

            $source_id = Source::whereName('DIVE_REPORTER')->value('id');
            $creatures = Creature::whereHas('sources', function ($query) use ($source_id) {
                $query->where('source_id', $source_id);
            })->get();

            foreach ($creatures as $creature) {
                $dives = Dive::whereBetween("date", [$first, $last])
                    ->whereHas('dive_pivot', function ($query) use ($creature) {
                        $query->whereCreatureId($creature->id);
                    })->whereDivingSpotId($this->id)->pluck('id');

                foreach ($dives as $dive_id) {
                    $abundance = DivePivot::whereCreatureId($creature->id)->whereDiveId($dive_id)->value('abundance_value');
                    !is_null($abundance) && array_push($abundance_values, $abundance);
                }

                if (!is_null($abundance_values)) {
                    $array = array_count_values($abundance_values);
                    arsort($array);
                    $element = key($array);

                    $values[$creature->name] = $element;
                }

                $abundance_values = [];
            }

            $total[Carbon::now()->month($date->month)->year($date->year)->format('M')] = $values;

            $date->addMonths(1);
        }

        return $total;
    }
}
