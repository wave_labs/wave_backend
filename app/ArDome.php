<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArDome extends Model
{
    protected $fillable = ['uuid', 'user_id', 'dome_id', 'has_dome', 'latitude', 'longitude']; //, 'has_dome'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function dome()
    {
        return $this->belongsTo('App\Dome');
    }

    public function items()
    {
        return $this->belongsToMany('App\DomeItem', 'ar_dome_has_items');
    }
}
