<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotReportField extends Model
{
    //
    protected $fillable = [
        'value',
        'report_id',
        'device_field_id'
    ];
    protected $connection = 'mysql_iot';
    public function report()
    {
        return $this->belongsTo('App\IotReport', 'report_id');
    }

    public function field()
    {
        return $this->belongsTo('App\IotDeviceField', 'device_field_id');
    }
}
