<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectInfraSpecific extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'infraSpecific',
        'specific_id',
        'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function originalName()
    {
        return $this->hasOne(InsectOriginalName::class, 'infra_specific_id', 'id');
    }

    public function sinonimia()
    {
        return $this->hasMany(InsectSinonimia::class, 'infra_specific_id', 'id');
    }

    public function specific()
    {
        return $this->belongsTo(InsectSpecific::class, 'specific_id');
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
