<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DivingSpotHasSubstract extends Model
{
    protected $fillable =
    [
        'diving_spot_id',
        'diving_spot_substract_id',
    ];
}
