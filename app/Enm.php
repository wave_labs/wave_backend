<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Process\Process;
use Intervention\Image\Facades\Image as IMG;
use JWTAuth;

class Enm extends Model
{
    protected $fillable = ['referenceId'];

    public static function getImage($path)
    {
        return IMG::make(storage_path($path))->response();
    }

    public static function runProcess($file, $args)
    {
        $command = array_merge([
            "Rscript",
            storage_path("enm/" . $file)
        ], $args);

        $process = new Process($command);
        $process->setTimeout(240);
        $process->setIdleTimeout(240);
        $process->run();

        return $process;
    }

    public static function exeCommand($command)
    {
        $output = shell_exec($command);
        $dataWithoutId = json_decode($output, true);

        $data = Enm::addIdToJson($dataWithoutId);

        return $data;
    }

    public static function decodeErrors($message)
    {
        $response = [];
        if (str_contains($message, 'undefined columns selected')) {
            array_push($response, "Coordinate columns and/or csv separator do not mmatch with the file uploaded");
        }
        if (str_contains($message, 'extents do not overlap')) {
            array_push($response, "Environmental data points to not overlap with ocurrence data points.");
        }
        if (str_contains($message, 'different extent')) {
            array_push($response, "Environmental data points uploaded have extents for different areas");
        }
        if (str_contains($message, 'more than half of the presence points have NA predictor values')) {
            array_push($response, "Many of your points do not overlap with the environmental data, or if they do, they have no value");
        }
        if (str_contains($message, 'Columns `latitude` and `longitude` don\'t exist.')) {
            array_push($response, "Selected repository does not have data points for this creature");
        }
        if (str_contains($message, 'resolution should be one of: 0.5, 2.5, 5, 10')) {
            array_push($response, "Something unexpected happenned with environmental resolution selection");
        }
        if (str_contains($message, 'was removed because it has no variation for the training points')) {
            array_push($response, "Environmental data had no variation for the training points");
        }


        return count($response) ? $response : ["Unexpected error happenned"];
    }

    public static function prepareJson($command)
    {
        $dataWithoutId = json_decode($command, true);

        $data = Enm::addIdToJson($dataWithoutId);

        return $data;
    }

    public static function createEnm($filename, $token)
    {
        $user = JWTAuth::setToken($token)->user();

        $enm = self::create([
            'referenceId' => $filename,
            'user_id' => $user->id
        ]);

        return $enm;
    }

    public static function getResponse($enm, $aData, $getCenter = false)
    {
        $response =  [
            'enm' =>  $enm,
            'center' => $getCenter ? Enm::getAvgCoordinates($aData) : $getCenter,
            'data' => $aData,
            'predictions' => $enm->predictions,
        ];

        return response()->json($response, 200);
    }

    public static function addIdToJson($json)
    {
        $newJson = [];
        foreach ($json as $key => $row) {
            $row['id'] = $key;
            array_push($newJson, $row);
        }

        return $newJson;
    }

    public static function getAvgCoordinates($json)
    {
        $lat = [];
        $lng = [];

        foreach ($json as $row) {
            array_push($lng, $row['longitude']);
            array_push($lat, $row['latitude']);
        }

        $latAvg = array_sum($lat) / count($lat);
        $lngAvg = array_sum($lng) / count($lng);

        return [$latAvg, $lngAvg];
    }

    public function predictions()
    {
        return $this->hasMany('App\EnmPrediction');
    }

    public function models()
    {
        return $this->belongsToMany('App\EnmModel', 'enm_has_models');
    }

    public function creature()
    {
        return $this->belongsTo('App\Creature');
    }

    public function envDataSource()
    {
        return $this->belongsTo('App\EnmEnvironmentDataSource', 'enm_environment_data_source_id');
    }

    public function ocuDataSource()
    {
        return $this->belongsTo('App\EnmOcurrenceDataSource', 'enm_ocurrence_data_source_id');
    }

    public function scopeFromUsers($query, $users)
    {
        if ($users)
            return $query->whereIn('user_id', $users);
        else
            return $query;
    }
}
