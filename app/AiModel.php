<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class AiModel extends Model
{
    use FiltersRecords;

    protected $fillable = ['name', 'path'];

    // RELATIONSHIPS

    public function category()
    {
        return $this->belongsTo('App\AiCategory', 'ai_category_id');
    }

    public function labels()
    {
        return $this->belongsToMany('App\AiLabel', 'ai_model_has_labels');
    }

    public function detections()
    {
        return $this->hasMany('App\AiDetection', 'ai_model_id');
    }
}
