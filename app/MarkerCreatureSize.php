<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarkerCreatureSize extends Model
{
    protected $fillable = ['sm', 'md', 'lg', 'marker_id'];

    public function marker()
    {
        return $this->belongsTo('App\Marker');
    }
}
