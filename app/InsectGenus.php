<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectGenus extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'genus',
        'family_id',
        'sub_family_id',
        'tribe_id',
        'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function subGenus()
    {
        return $this->hasMany(InsectSubGenus::class);
    }

    public function family()
    {
        return $this->belongsTo(InsectFamily::class, 'family_id');
    }

    public function subFamily()
    {
        return $this->belongsTo(InsectSubFamily::class, 'sub_family_id');
    }

    public function tribe()
    {
        return $this->belongsTo(InsectTribe::class, 'tribe_id');
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
