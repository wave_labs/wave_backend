<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectSubGenus extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'subGenus',
        'genus_id',
        'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function specific()
    {
        return $this->hasMany(InsectSpecific::class);
    }

    public function genus()
    {
        return $this->belongsTo(InsectGenus::class, 'genus_id');
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
