<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = [
        'start',
        'end',
    ];

    //Relations
    public function sightings()
    {
        return $this->hasMany('App\Sighting');
    }

    public function positions()
    {
        return $this->hasMany('App\TripPosition');
    }
}
