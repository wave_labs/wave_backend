<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotDeviceFieldHasRule extends Model
{
    protected $connection = 'mysql_iot';
    protected $fillable = ['field_id', 'rule_id'];
}
