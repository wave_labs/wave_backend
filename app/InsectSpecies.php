<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectSpecies extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'kingdom',
        'phylum',
        'class',
        'order',
        'subOrder',
        'infraOrder',
        'superFamily',
        'family',
        'subFamily',
        'tribe',
        'genus',
        'subgenus',
        'specificEpithet',
        'infraSpecificEpithet',
        'author',
        'year',
        'reference',
        'taxonRank',
        'taxonomicStatus',
        'nomenclatureStatus',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function reports()
    {
        return $this->hasMany('App\InsectReport', 'species_id');
    }

    public function sinonimias()
    {
        return $this->hasMany('App\InsectSinonimia', 'species_id');
    }

    public function originalName()
    {
        return $this->hasOne(InsectOriginalName::class, 'species_id');
    }
}
