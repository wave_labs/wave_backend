<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use File;

class BoundingBoxResult extends Model
{
    use FiltersRecords;

    protected $fillable = ['image_id', 'user_id', 'file'];

    public static function createFile($validator)
    {
        $coordinatesArray = [];
        foreach ($validator['classifications'] as $key => $classification) {

            $coordinates = explode(",", $classification);

            $class = BoundingBoxClass::find($coordinates[4]);
            $coordinates = '
                { 
                    "description":"",
                    "tags":[],
                    "bitmap":null,
                    "classTitle":"' . $class->name . '",
                    "points":{
                        "exterior": [ 
                            [' . $coordinates[0] . ', ' . $coordinates[1] . '], 
                            [' . $coordinates[2] . ', ' . $coordinates[3] . ']
                        ],
                        "interior": []
                    }
                }
            ';

            array_push($coordinatesArray, $coordinates);
        }

        $image = BoundingBoxImage::find($validator['image_id']);

        $data = '
        {
            "description":"",
            "name":"' . $image->filename . '",
            "size":{"width":' . $image->width . ', "height":' . $image->height . ' },
            "tags":[""],
            "objects":  [
                ' . implode(",", $coordinatesArray) . '
            ]  
        }
        ';

        $file = uniqid();
        File::put(storage_path('/bounding-box/' . $file . '.json'), $data);

        return $file;
    }



    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function image()
    {
        return $this->belongsTo('App\BoundingBoxImage', 'image_id');
    }

    public function coordinate()
    {
        return $this->hasMany('App\BoundingBoxCoordinate', 'result_id');
    }
}
