<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomePosition extends Model
{
    public function dome()
    {
        return $this->belongsTo('App\Dome');
    }
}
