<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Response;

class Video extends Model
{
    protected $fillable =
    [
        'type',
        'file',
        'url',
        'creature_id'
    ];

    public function creature()
    {
        return $this->belongsTo('App\Creature');
    }

    public static function getVideo($index, $class)
    {
        $file = storage_path('/video/' . $class . '/' . $index . '.mp4');
        return $file;

        

        $response = Response::make($file, 200);
        $response->header('Content-Type', "video/mp4");
        
    }
}
