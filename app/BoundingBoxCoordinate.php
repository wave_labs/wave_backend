<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoundingBoxCoordinate extends Model
{
    protected $fillable = ['x_min', 'x_max', 'y_min', 'y_max', 'class_id', 'result_id'];

    public function class()
    {
        return $this->belongsTo('App\BoundingBoxClass', 'class_id');
    }

    public function result()
    {
        return $this->hasMany('App\BoundingBoxResult', 'result_id');
    }
}
