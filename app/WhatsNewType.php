<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsNewType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type'
    ];

    public function whatsNewMessage()
    {
        return $this->hasMany('App\WhatsNewMessage');
    }
}
