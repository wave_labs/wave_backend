<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use File;
use Spatie\Translatable\HasTranslations;

class LitterCategory extends Model
{
    use FiltersRecords, HasTranslations;
    public $translatable = ['name'];
    protected $fillable = ['name'];

    public function filteredSource()
    {
        return $this->hasMany('\App\LitterSubCategory')->where('beach', 1);
    }

    public function litterSubCategory()
    {
        return $this->hasMany('App\LitterSubCategory');
    }

    public function images()
    {
        return $this->hasMany('App\LitterCategoryImage');
    }

    public function litter()
    {
        return $this->belongsToMany('App\Litter', 'litter_category_pivot');
    }

    public function deleteImages()
    {
        $images = $this->images;

        foreach ($images as $image) {
            File::delete(public_path($image->url));
        }
    }
}
