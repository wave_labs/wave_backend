<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoStudy extends Model
{
    protected $fillable =
    [
        'name',
        'url',
        'lenght',
    ];

    public function stopwatchs()
    {
        return $this->hasMany('App\Stopwatch');
    }
}
