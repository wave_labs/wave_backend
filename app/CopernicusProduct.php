<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CopernicusProduct extends Model
{
    public function parameters()
    {
        return $this->belongsToMany('App\CopernicusParameter', 'copernicus_has_parameters');
    }
}
