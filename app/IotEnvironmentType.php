<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class IotEnvironmentType extends Model
{
    use FiltersRecords;
    /**
	* The table associated with the model.
	*
	* @var string
	*/
    protected $table = 'iot_environment_type';
    protected $connection = 'mysql_iot';
    protected $fillable = [
        'name', 
        'description'
    ];
}
