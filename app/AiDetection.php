<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Intervention\Image\Facades\Image as IMG;

class AiDetection extends Model
{
    use FiltersRecords;

    protected $fillable = ['user_id', 'ai_model_id'];

    public static function storeImage($file)
    {
        if ($file->isValid()) {
            $filename = uniqid() . '.jpg';

            IMG::make($file)->encode('jpg', 65)->resize(760, null, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            })->save(public_path("python/images/" . $filename));

            $image = AiDetectionImage::create([
                'path' => $filename
            ]);

            return [$image->id, $image->path];
        }
    }

    public static function fixJSON($json)
    {
        $regex = <<<'REGEX'
    ~
        "[^"\\]*(?:\\.|[^"\\]*)*"
        (*SKIP)(*F)
    | '([^'\\]*(?:\\.|[^'\\]*)*)'
    ~x
    REGEX;

        return preg_replace_callback($regex, function ($matches) {
            return '"' . preg_replace('~\\\\.(*SKIP)(*F)|"~', '\\"', $matches[1]) . '"';
        }, $json);
    }

    public function scopeFromUser($query, $users)
    {
        if ($users)
            return $query->whereIn('user_id', $users);
        else
            return $query;
    }

    // RELATIONSHIPS

    public function model()
    {
        return $this->belongsTo('App\AiModel', 'ai_model_id');
    }

    public function images()
    {
        return $this->hasMany('App\AiDetectionImage', 'ai_detection_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
