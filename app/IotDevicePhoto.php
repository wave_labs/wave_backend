<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image as IMG;

use App\IotDevice;

class IotDevicePhoto extends Model
{
    protected $table = 'iot_device_image';
    protected $fillable = ['path', 'id_device'];
    protected $connection = 'mysql_iot';
    public static function store($photo, $deviceId)
    {
        $filename = uniqid() . '.jpg';

        IMG::make($photo)->encode('jpg', 65)->resize(760, null, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        })->save(storage_path('/uploaded/iot_device_photo/' . $filename));

        IotDevicePhoto::create([
            'id_device' => $deviceId,
            'path' => $filename
        ]);
    }
}
