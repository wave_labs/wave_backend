<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StopwatchCategory extends Model
{
    protected $fillable = ['name'];

    public function classes()
    {
        return $this->hasMany('App\StopwatchClass', 'class_id');
    }

    public function videos()
    {
        return $this->hasMany('App\StopwatchVideo', 'category_id');
    }
}
