<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class BoundingBoxImage extends Model
{
    use FiltersRecords;
    protected $fillable =
    [
        'image',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo('App\BoundingBoxCategory', 'category_id');
    }
}
