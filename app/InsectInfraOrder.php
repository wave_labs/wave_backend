<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectInfraOrder extends Model
{
    use FiltersRecords;

    protected $fillable = [
        'infraOrder', 'sub_order_id', 'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function subOrder()
    {
        return $this->belongsTo(InsectSubOrder::class, 'sub_order_id');
    }

    public function family()
    {
        return $this->hasMany(InsectFamily::class);
    }

    public function superFamilies()
    {
        return $this->hasMany(InsectSuperFamily::class);
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
