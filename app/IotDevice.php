<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Intervention\Image\Facades\Image as IMG;
use App\IotReport;
use App\IotDeviceState;
use App\IotDeviceType;
use App\IotEnvironmentType;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use stdClass;

class IotDevice extends Model
{
    use FiltersRecords;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'iot_device';
    protected $connection = 'mysql_iot';

    protected $fillable = [
        'name',
        'serial_number',
        'id_type',
        'id_environment',
        'id_state',
        'image',
        'device_category_id',
        'template_id'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'wave_iot.iot_device_has_users', 'id_device');
    }

    public function gateways()
    {
        return $this->belongsToMany('App\IotDevice', 'iot_device_has_iot_devices', 'iot_device_id', 'gateway_id');
    }

    public function state()
    {
        return $this->hasOne(IotDeviceState::class, 'id', 'id_state');
    }

    public function reports()
    {
        return $this->hasMany(IotReport::class, 'id_device', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(IotDeviceCategory::class, 'iot_device_has_categories')->withPivot('latitude', "longitude");
    }

    public function type()
    {
        return $this->hasOne(IotDeviceType::class, 'id', 'id_type');
    }

    public function environment()
    {
        return $this->hasOne(IotEnvironmentType::class, 'id', 'id_environment');
    }

    public function fields()
    {
        return $this->hasMany('App\IotDeviceField', 'device_id');
    }

    public function features()
    {
        $fields = $this->fields()->pluck('id');

        return IotDeviceField::whereHas('fieldFact', function ($query) use ($fields) {
            $query->whereIn('field_id', $fields)->whereHas('feature', function ($q) {
                $q->where('has_timeline', 1);
            });
        })->pluck('key');
    }


    public function template()
    {
        return $this->belongsTo(IotTemplate::class, 'template_id');
    }

    public function sections()
    {
        return $this->belongsToMany(IotSection::class, 'iot_device_has_sections', 'device_id', 'section_id')->withPivot('title');
    }

    public function featureParameters()
    {
        $sections = $this->template->sections->pluck('id');
        $fieldIds = $this->fields->pluck('id');
        $fields = IotFieldFact::whereIn('section_id', $sections)
            ->whereIn('field_id', $fieldIds)
            ->get()->groupBy('section_id');

        $response = new stdClass();

        foreach ($fields as $index => $field) {
            $response->{$index} = new stdClass(); //initializing object with key
            $response->{$index}->content = $field;
        }

        return $response;
    }

    public function sectionFeatures()
    {
        $sections = $this->template->sections->pluck('id');
        $fieldIds = $this->fields->pluck('id');
        $fields = IotFieldFact::whereIn('section_id', $sections)
            ->whereIn('field_id', $fieldIds)
            ->get()->groupBy('section_id');
        $titles = IotDeviceHasSection::whereIn('section_id', $sections)->where('device_id', $this->id)->get()->groupBy('section_id');

        $response = new stdClass();

        foreach ($fields as $index => $fieldFacts) {
            $response->{$index} = new stdClass(); //initializing object with key
            $response->{$index}->content = [];
            foreach ($fieldFacts as $fieldFact) {

                $currentField = IotDeviceField::find($fieldFact->field_id);
                $newElement = [
                    "id" => $fieldFact->id,
                    "feature_id" => $fieldFact->feature_id,
                    "has_timeline" => $fieldFact->feature->has_timeline,
                    "field_id" => $fieldFact->field_id,
                    "field_key" => $currentField->key,
                    "parameters" => $fieldFact->parameters,
                ];
                array_push($response->{$index}->content, $newElement);
            }
        }

        foreach ($titles as $index => $title) {
            $response->{$index}->title = $title[0]['title'];
        }
        return $response;
    }

    public function handleCategory($validator)
    {
        if ($validator['category'] == 1 || $validator['category'] == 4) {
            $this->categories()->sync([$validator['category'] => ['latitude' => $validator['category_latitude'], 'longitude' => $validator['category_longitude']]]);
        } else {
            $this->categories()->sync($validator['category']);
        }
    }

    public function verifyDuplicatedEntry($field)
    {
        $epoch = Carbon::createFromTimestamp($field)
            ->subHour()
            ->toDateTimeString();

        return IotReport::where('device_id', $this->id)
            ->where('collected_at', $epoch)
            ->count();
    }

    public function savePhoto($photo)
    {
        $path = 'storage/uploaded/photo/iot_device/' . uniqid() . '.jpg';

        IMG::make($photo)->encode('jpg', 65)->resize(760, null, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        })->save(public_path($path));

        $this->image = $path;
        $this->save();
    }

    public function hasCategory($category)
    {
        return optional($this->categories[0])->id == $category  && true;
    }

    public function updatePhoto($photo)
    {
        $path = 'storage/uploaded/photo/iot_device/' . uniqid() . '.jpg';
        IMG::make($photo)->encode('jpg', 65)->resize(760, null, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        })->save(public_path($path));

        $this->image !== "storage/uploaded/photo/iot_device/placeholder.png" && File::delete(public_path($this->image));

        $this->image = $path;
        $this->save();
    }

    public function scopeGetRecordsFromUser($query, $users)
    {
        if ($users)
            return $query->whereHas('users', function ($query) use ($users) {
                $query->whereIn('user_id', $users);
            });
        else
            return $query;
    }
}
