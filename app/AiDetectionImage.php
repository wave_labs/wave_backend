<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Intervention\Image\Facades\Image as IMG;

class AiDetectionImage extends Model
{
    use FiltersRecords;

    protected $fillable = ['ai_detection_id', 'image'];

    public static function saveToDirectory($detection, $files, $directory)
    {
        foreach ($files as $key => $file) {
            if ($file->isValid()) {
                $filename = $directory . "/" .  $key . ".jpg";
                IMG::make($file)->encode('jpg', 65)->resize(760, null, function ($c) {
                    $c->aspectRatio();
                    $c->upsize();
                })->save(public_path("python/images/" . $filename));

                AiDetectionImage::create([
                    'ai_detection_id' => $detection->id,
                    'image' => $filename
                ]);
            }
        }
    }

    // RELATIONSHIPS

    public function classifications()
    {
        return $this->hasMany('App\AiDetectionClassification');
    }

    public function detection()
    {
        return $this->belongsTo('App\AiDetection', 'ai_detection_id');
    }
}
