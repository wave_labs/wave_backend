<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image as IMG;
use File;

class LitterPhoto extends Model
{
    protected $fillable = ['litter_id', 'url'];

    public function litter()
    {
        return $this->belongsTo('App\Litter');
    }

    public static function store($photo, $id)
    {
        $filename = uniqid() . '.jpg';

        self::savePhotoLocally($photo, $filename);

        LitterPhoto::create([
            'litter_id' => $id,
            'url' => $filename
        ]);

        return $filename;
    }

    public static function savePhoto($photo, $id)
    {
        $filename = Helper::storePhoto($photo, 'data/images/apps/litter_reporter');

        LitterPhoto::create([
            'litter_id' => $id,
            'url' => $filename
        ]);

        return $filename;
    }

    public static function savePhotoLocally($photo, $filename)
    {
        IMG::make($photo)->encode('jpg', 70)->resize(760, null, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        })->save(public_path('storage/uploaded/photo/litter/' . $filename));
    }

    public static function updatePhoto($photo, $id)
    {
        $filename = uniqid() . '.jpg';
        self::savePhotoLocally($photo, $filename);
        $previousPhoto = LitterPhoto::whereLitterId($id)->first();

        File::delete(public_path('storage/uploaded/photo/litter/' . $previousPhoto->url));

        $previousPhoto->litter_id = $id;
        $previousPhoto->url = $filename;
        $previousPhoto->save();
    }
}
