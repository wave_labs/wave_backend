<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MarkerCoordinates;

class MarkerActivity extends Model
{
    protected $fillable = ['experience', 'frequency', 'time', 'activity_id', 'marker_id'];


    

    public function marker()
    {
        return $this->belongsTo('App\Marker');
    }

    public function activity()
    {
        return $this->belongsTo('App\Activity');
    }

    public function coordinates()
    {
        return $this->hasMany('App\MarkerCoordinates');
    }
}
