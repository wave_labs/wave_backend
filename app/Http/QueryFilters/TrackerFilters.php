<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class TrackerFilters extends QueryFilters
{
    /**
     * Filter records based on the query parameter "specie"
     * 
     * @param mixed $string
     * @return void
     */
    public function specie($string)
    {
        $this->query->whereSpecie($string);
    }
}
