<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

class UserDiveCenterFilters extends QueryFilters
{
    use OrderFilter; 

	public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
                $query->where('name', 'like', '%' .$string . '%')
                ->orWhere('latitude', 'like', '%' .$string . '%')
                ->orWhere('longitude', 'like', '%' .$string . '%')
                ->orWhere('address', 'like', '%' .$string . '%');
                
                      
                         
        });
    }
   
}
