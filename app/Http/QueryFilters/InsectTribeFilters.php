<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectTribeFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('tribe', 'like',  '%' . $string . '%');
    }

    public function subFamily($string)
    {
        $this->query->whereHas('subFamily', function ($query) use ($string) {
            $query->where('id', '=', $string);
        });
    }
}
