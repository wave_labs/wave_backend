<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class AiLabelFilters extends QueryFilters
{
    /**
     * Filter records based on the query parameter "model"
     * 
     * @param mixed $string
     * @return void
     */
    public function model($string)
    {
        $this->query->whereHas('models', function ($query) use ($string) {
            $query->where('name', 'like', '%' . $string . '%');
        });
    }
}
