<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;
use App\User;
use Error;

class DivingSpotFilters extends QueryFilters
{
    use OrderFilter;

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            for ($i = 0; $i < count($string); $i++) {
                $query->where('name', 'like',  '%' . $string[$i] . '%');
            }
        });
    }

    public function validated($status)
    {
        $this->query->where('validated', $status);
    }
}
