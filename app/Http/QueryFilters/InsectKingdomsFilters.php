<?php

namespace App\Http\QueryFilters;

use Error;
use Illuminate\Support\Facades\DB;
use App\InsectKingdom;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectKingdomsFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('kingdom', 'like',  '%' . $string . '%');
    }
}
