<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectInfraSpecificFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('infraSpecific', 'like',  '%' . $string . '%');
    }
}
