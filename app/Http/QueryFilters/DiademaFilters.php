<?php

namespace App\Http\QueryFilters;

use Carbon\Carbon;
use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class DiademaFilters extends QueryFilters
{
    public function date($date)
    {
        $from = Carbon::createFromFormat('Y-m-d', $date[0])->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $date[1])->endOfDay();

        $this->query->whereBetween('created_at', [$from, $to]);
    }
}
