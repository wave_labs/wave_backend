<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

class SurveyFilters extends QueryFilters
{
	use OrderFilter; 

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
                $query->where('abundance_value', 'like', '%' .$string . '%')
                ->orWhere('date', 'like', '%' .$string . '%')
                ->orWhereHas('user', function ($query) use ($string) {
                    $query->WhereHas('user_person', function ($query) use ($string) {
                        $query->where('name', 'like', '%' .$string . '%');
                    })
                    ->orWhereHas('user_dive_center', function ($query) use ($string) {
                        $query->where('name', 'like', '%' .$string . '%');
                    });
                })
                ->orWhereHas('dive', function ($query) use ($string) {
                    $query->where('id', 'like', '%' .$string . '%');
                })
                ->orWhereHas('creature', function ($query) use ($string) {
                    $query->where('name', 'like', '%' .$string . '%');
                });
         });
       
    }

}
