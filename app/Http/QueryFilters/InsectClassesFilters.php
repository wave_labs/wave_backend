<?php

namespace App\Http\QueryFilters;

use App\InsectClass;
use Cerbero\QueryFilters\QueryFilters;
use Error;
use Illuminate\Support\Facades\DB;

/**
 * Filter records based on query parameters.
 *
 */
class InsectClassesFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('class', 'like',  '%' . $string . '%');
    }
}
