<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;
use App\Source;
use Carbon\Carbon;

class SightingFilters extends QueryFilters
{

    use OrderFilter;

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('date', 'like', '%' . $string . '%')
                ->orWhere('notes', 'like', '%' . $string . '%')
                ->orWhere('report_source', 'like', '%' . $string . '%')
                ->orWhereHas('user', function ($query) use ($string) {
                    $query->WhereHas('user_person', function ($query) use ($string) {
                        $query->where('name', 'like', '%' . $string . '%');
                    })
                        ->orWhereHas('user_dive_center', function ($query) use ($string) {
                            $query->where('name', 'like', '%' . $string . '%');
                        });
                })
                ->orWhereHas('behaviour', function ($query) use ($string) {
                    $query->where('behaviour', 'like', '%' . $string . '%');
                })
                ->orWhereHas('vehicle', function ($query) use ($string) {
                    $query->where('vehicle', 'like', '%' . $string . '%');
                })
                ->orWhereHas('beaufort_scale', function ($query) use ($string) {
                    $query->where('desc', 'like', '%' . $string . '%');
                })
                ->orWhereHas('creature', function ($query) use ($string) {
                    $query->where('name', 'like', '%' . $string . '%');
                });
        });
    }

    public function isGroupMixed($int)
    {
        $this->query->where('is_group_mixed', $int);
    }

    public function creature($id)
    {
        $this->query->whereCreatureId($id);
    }

    public function creatures($array)
    {
        $this->query->whereHas('creature', function ($query) use ($array) {
            $query->whereIn('id', $array);
        });
    }

    public function date($array)
    {
        $from = Carbon::createFromFormat('Y-m-d', $array[0])->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $array[1])->endOfDay();

        $this->query->whereBetween('date', [$from, $to]);
    }

    public function sighting()
    {
        $whaleReporter = Source::where('name', 'WHALE_REPORTER')->value('id');

        $this->query->whereHas('creature', function ($query) use ($whaleReporter) {
            $query->whereHas('sources', function ($query) use ($whaleReporter) {
                $query->where('source_id', $whaleReporter);
            });
        });
    }
}
