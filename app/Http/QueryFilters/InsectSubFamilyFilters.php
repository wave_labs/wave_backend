<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectSubFamilyFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('subFamily', 'like',  '%' . $string . '%');
    }

    public function family($string)
    {
        $this->query->whereHas('family', function ($query) use ($string) {
            $query->where('id', '=', $string);
        });
    }
}

