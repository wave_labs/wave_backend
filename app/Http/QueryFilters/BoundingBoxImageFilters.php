<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

/**
 * Filter records based on query parameters.
 *
 */
class BoundingBoxImageFilters extends QueryFilters
{
    use OrderFilter;

    public function random()
    {
        $this->query->inRandomOrder()->limit(1);
    }
}
