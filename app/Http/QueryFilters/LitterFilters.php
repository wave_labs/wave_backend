<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;
use Carbon\Carbon;

class LitterFilters extends QueryFilters
{

    use OrderFilter;

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('date', 'like', '%' . $string . '%')
                ->orWhere('radius', 'like', '%' . $string . '%')
                ->orWhere('quantity', 'like', '%' . $string . '%')
                ->orWhereHas('user', function ($query) use ($string) {
                    $query->WhereHas('user_person', function ($query) use ($string) {
                        $query->where('name', 'like', '%' . $string . '%');
                    })
                        ->orWhereHas('user_dive_center', function ($query) use ($string) {
                            $query->where('name', 'like', '%' . $string . '%');
                        });
                })
                ->orWhereHas('litterCategory', function ($query) use ($string) {
                    $query->where('name', 'like', '%' . $string . '%');
                });
        });
    }

    public function type($array)
    {
        $this->query->whereHas('litter_sub_category', function ($query) use ($array) {
            $query->whereIn('id', $array);
        });
    }

    public function source($string)
    {
        $this->query->whereSource($string);
    }

    public function date($array)
    {
        $from = Carbon::createFromFormat('Y-m-d', $array[0])->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $array[1])->endOfDay();

        $this->query->whereBetween('date', [$from, $to]);
    }

    public function core()
    {
        $this->query->where('source', 'core');
    }

    public function beach()
    {
        $this->query->where('source', 'beach');
    }

    public function seafloor()
    {
        $this->query->where('source', 'seafloor');
    }

    public function floating()
    {
        $this->query->where('source', 'floating');
    }

    public function biota()
    {
        $this->query->where('source', 'biota');
    }

    public function micro()
    {
        $this->query->where('source', 'micro');
    }

    public function dive()
    {
        $this->query->where('source', 'dive');
    }
}
