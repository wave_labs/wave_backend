<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectSubOrderFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('subOrder', 'like',  '%' . $string . '%');
    }

    public function order($string)
    {
        $this->query->whereHas('order', function ($query) use ($string) {
            $query->where('id', '=', $string);
        });
    }
}
