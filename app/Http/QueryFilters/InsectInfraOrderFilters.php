<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectInfraOrderFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('infraOrder', 'like',  '%' . $string . '%');
    }

    public function subOrder($string)
    {
        $this->query->whereHas('subOrder', function ($query) use ($string) {
            $query->where('id', '=', $string);
        });
    }
}
