<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class StopwatchVideoFilters extends QueryFilters
{
    //
    public function category($id)
    {
        $this->query->whereCategoryId($id);
    }

    public function detection($bool)
    {
        $this->query->whereDetection($bool);
    }
}
