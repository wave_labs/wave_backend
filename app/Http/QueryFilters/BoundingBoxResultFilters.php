<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;
use Carbon\Carbon;

/**
 * Filter records based on query parameters.
 *
 */
class BoundingBoxResultFilters extends QueryFilters
{
    use OrderFilter;

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('image_id', 'like', '%' . $string . '%')
                ->orWhere('id', 'like', '%' . $string . '%')
                ->orWhereHas('coordinate', function ($query) use ($string) {
                    $query->WhereHas('class', function ($query) use ($string) {
                        $query->where('name', 'like', '%' . $string . '%');
                    });
                })
                ->orWhereHas('user', function ($query) use ($string) {
                    $query->WhereHas('user_person', function ($query) use ($string) {
                        $query->where('name', 'like', '%' . $string . '%');
                    })
                        ->orWhereHas('user_dive_center', function ($query) use ($string) {
                            $query->where('name', 'like', '%' . $string . '%');
                        });
                });
        });
    }

    public function category($id)
    {
        $this->query->whereHas('video', function ($query) use ($id) {
            $query->WhereHas('category', function ($query) use ($id) {
                $query->where('id', $id);
            });
        });
    }

    public function date($date)
    {
        $from = Carbon::createFromFormat('Y-m-d', $date[0])->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $date[1])->endOfDay();

        $this->query->whereBetween('created_at', [$from, $to]);
    }
}
