<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class AiDetectionImageFilters extends QueryFilters
{
    public function category($id)
    {
        $this->query->whereHas('detection', function ($query) use ($id) {
            $query->whereHas('model', function ($query) use ($id) {
                $query->whereHas('category', function ($query) use ($id) {
                    $query->whereId($id);
                });
            });
        });
    }

    public function detection($id)
    {
        $this->query->whereHas('detection', function ($query) use ($id) {
            $query->whereId($id);
        });
    }

    public function specie($string)
    {
        $this->query->whereHas('aiLabel', function ($query) use ($string) {
            $query->whereName($string);
        });
    }
}
