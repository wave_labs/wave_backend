<?php

namespace App\Http\QueryFilters;

use App\Traits\OrderFilter;
use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectProjectFilters extends QueryFilters
{
    use OrderFilter;

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('name', 'like', '%' .$string . '%');
    });
    }
}
