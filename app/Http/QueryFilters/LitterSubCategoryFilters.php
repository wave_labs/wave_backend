<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

/**
 * Filter records based on query parameters.
 *
 */
class LitterSubCategoryFilters extends QueryFilters
{
    use OrderFilter;

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('name', 'like', '%' . $string . '%')
                ->orWhere('OSPAR_code', 'like', '%' . $string . '%')
                ->orWhere('UNEP_code', 'like', '%' . $string . '%')
                ->orWhereHas('litterCategory', function ($query) use ($string) {
                    $query->where('name', 'like', '%' . $string . '%');
                });
        });
    }
    public function category($id)
    {
        $this->query->whereLitterCategoryId($id);
    }

    public function name($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('name', 'like', '%' . $string . '%');
        });
    }

    public function source($string)
    {
        $this->query->where($string, true);
    }

    public function Core($array)
    {
        $this->query->where('Core', filter_var($array[0], FILTER_VALIDATE_BOOLEAN));
    }

    public function Beach($array)
    {
        $this->query->where('Beach', filter_var($array[0], FILTER_VALIDATE_BOOLEAN));
    }

    public function Seafloor($array)
    {
        $this->query->where('Seafloor', filter_var($array[0], FILTER_VALIDATE_BOOLEAN));
    }

    public function Floating($array)
    {
        $this->query->where('Floating', filter_var($array[0], FILTER_VALIDATE_BOOLEAN));
    }

    public function Biota($array)
    {
        $this->query->where('Biota', filter_var($array[0], FILTER_VALIDATE_BOOLEAN));
    }

    public function Micro($array)
    {
        $this->query->where('Micro', filter_var($array[0], FILTER_VALIDATE_BOOLEAN));
    }
}
