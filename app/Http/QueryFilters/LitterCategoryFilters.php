<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class LitterCategoryFilters extends QueryFilters
{
    /**
     * Filter records based on the query parameter "search"
     * 
     * @param mixed $string
     * @return void
     */
    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('name', 'like', '%' . $string . '%');
        });
    }

    public function source($string)
    {
        $this->query->whereHas('litterSubCategory', function ($query) use ($string) {
            $query->where($string, true);
        });
    }
}
