<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

class QuestionFilters extends QueryFilters
{
    use OrderFilter; 

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
                $query->where('text', 'like', '%' .$string . '%')
                    ->orWhere('type', 'like', '%' .$string . '%')
                    ->orWhere('section', 'like', '%' .$string . '%');
         });
       
    }
}
