<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectFamilyFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('family', 'like',  '%' . $string . '%');
    }
}
