<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

class CreatureFilters extends QueryFilters
{
    use OrderFilter;

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('name', 'like', '%' . $string . '%')
                ->orWhere('name_scientific', 'like', '%' . $string . '%')
                ->orWhere('description', 'like', '%' . $string . '%')
                ->orWhere('curiosity', 'like', '%' . $string . '%')
                ->orWhere('conserv', 'like', '%' . $string . '%')
                ->orWhere('size', 'like', '%' . $string . '%')
                ->orWhere('group_size', 'like', '%' . $string . '%')
                ->orWhere('calves', 'like', '%' . $string . '%')
                ->orWhere('nis_status', 'like', '%' . $string . '%')
                ->orWhereHas('type', function ($query) use ($string) {
                    $query->where('taxa_category', 'like', '%' . $string . '%');
                });
        });
    }

    public function creatureType($id)
    {
        $this->query->whereTypeId($id);
    }

    public function source($source)
    {
        $this->query->whereHas('sources', function ($query) use ($source) {
            $query->where('name', $source);
        });
    }
}
