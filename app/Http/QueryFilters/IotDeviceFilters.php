<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

class IotDeviceFilters extends QueryFilters
{
    use OrderFilter;

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('name', 'like', '%' . $string . '%')
                ->orWhere('serial_number', 'like', '%' . $string . '%');
        });
    }

    public function deviceType($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->orwhereHas('type', function ($query) use ($string) {
                $query->where('name', 'like', '%' . $string . '%');
            });
        });
    }
}
