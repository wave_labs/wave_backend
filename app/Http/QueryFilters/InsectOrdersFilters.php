<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use Error;
use Illuminate\Support\Facades\DB;
use App\InsectOrder;

/**
 * Filter records based on query parameters.
 *
 */
class InsectOrdersFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('order', 'like',  '%' . $string . '%');
    }
}
