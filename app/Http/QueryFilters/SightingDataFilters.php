<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

class SightingDataFilters extends QueryFilters
{
	use OrderFilter; 

	public function underwater($int)
    {
        $this->query->where('underwater',$int);
    }
    
    public function type($array)
    {
        $this->query->whereIn('type',$array);
    }
    
}
