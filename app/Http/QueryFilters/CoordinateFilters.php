<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;
use App\Litter;
use App\Http\Resources\LitterCoordResource;
use App\Http\Resources\SightingCoordResource;

use App\Sighting;

class CoordinateFilters extends QueryFilters
{

    use OrderFilter; 

    public function litter($bool)
    {
        if ($bool) {
            LitterCoordResource::collection(Litter::all());
        }
    }

    public function sighting($bool)
    {
        if ($bool) {
            SightingCoordResource::collection(Sighting::all());
        }
    }
    
}
