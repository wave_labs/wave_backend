<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectSpeciesFilters extends QueryFilters
{

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('genus', 'like', '%' . $string . '%')
                ->orWhere('subgenus', 'like', '%' . $string . '%')
                ->orWhere('specificEpithet', 'like', '%' . $string . '%')
                ->orWhere('infraSpecificEpithet', 'like', '%' . $string . '%')
                ->orWhereHas('sinonimias', function ($q) use ($string) {
                    $q->where('genus', 'like', '%' . $string . '%')
                        ->orWhere('subgenus', 'like', '%' . $string . '%')
                        ->orWhere('specificEpithet', 'like', '%' . $string . '%')
                        ->orWhere('infraSpecificEpithet', 'like', '%' . $string . '%');
                });;
        });
    }

    public function taxonRank($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('taxonRank', '=', $string);
        });
    }
}
