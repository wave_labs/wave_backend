<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

class FeedbackFilters extends QueryFilters
{

    use OrderFilter; 
    
    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
                $query->where('rating', 'like', '%' .$string . '%')
                        ->orWhereHas('user', function ($query) use ($string) {
                            $query->where('first_name', 'like', '%' .$string . '%')
                                ->orWhere('last_name', 'like', '%' .$string . '%');
                        })
                        ->orWhereHas('question', function ($query) use ($string) {
                            $query->where('text', 'like', '%' .$string . '%')
                                ->orWhere('type', 'like', '%' .$string . '%')
                                ->orWhere('section', 'like', '%' .$string . '%');
                        });
        });
    }
}
