<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;
use Carbon\Carbon;

class IotReportFilters extends QueryFilters
{


    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('id', 'like', '%' . $string . '%')
                ->orWhere('id_device', 'like', '%' . $string . '%');
        });
    }

    public function date($array)
    {
        $from = Carbon::createFromFormat('Y-m-d', $array[0])->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $array[1])->endOfDay();

        $this->query->whereBetween('created_at', [$from, $to]);
    }

    public function device($int)
    {
        $this->query->where('id_device', $int);
    }

    public function field($string)
    {
        $this->query->whereHas('fields', function ($query) use ($string) {
            $query->whereHas('field', function ($query) use ($string) {
                $query->where('key', 'like', '%' . $string . '%');
            });
        });
    }

    public function value($string)
    {
        $this->query->whereHas('fields', function ($query) use ($string) {
            $query->where('value', 'like', '%' . $string . '%');
        });
    }
    /*
    public function type($array)
    {
        $this->query->whereHas('litter_sub_category', function ($query) use ($array) {
            $query->whereIn('id', $array);
        });
    }

    public function core()
    {
        $this->query->where('source', 'core');
    }

    public function beach()
    {
        $this->query->where('source', 'beach');
    }

    public function seafloor()
    {
        $this->query->where('source', 'seafloor');
    }

    public function floating()
    {
        $this->query->where('source', 'floating');
    }

    public function biota()
    {
        $this->query->where('source', 'biota');
    }

    public function micro()
    {
        $this->query->where('source', 'micro');
    }

    public function dive()
    {
        $this->query->where('source', 'dive');
    }*/
}
