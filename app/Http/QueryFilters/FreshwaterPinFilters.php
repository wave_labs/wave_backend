<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use Carbon\Carbon;

/**
 * Filter records based on query parameters.
 *
 */
class FreshwaterPinFilters extends QueryFilters
{
    public function image($string)
    {
        $this->query->whereHas('coordinates', function ($query) use ($string) {
            $query->whereImage($string);
        });
    }

    public function timeframe($string)
    {
        $this->query->whereHas('coordinates', function ($query) use ($string) {
            $query->whereTimeframe($string);
        });
    }

    public function date($date)
    {
        $from = Carbon::createFromFormat('Y-m-d', $date[0])->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $date[1])->endOfDay();

        $this->query->whereBetween('created_at', [$from, $to]);
    }
}
