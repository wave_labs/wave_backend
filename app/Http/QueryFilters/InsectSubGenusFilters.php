<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectSubGenusFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('subGenus', 'like',  '%' . $string . '%');
    }

    public function genus($string)
    {
        $this->query->whereHas('genus', function ($query) use ($string) {
            $query->where('id', '=', $string);
        });
    }
}
