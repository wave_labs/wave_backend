<?php

namespace App\Http\QueryFilters;

use Carbon\Carbon;
use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class AiDetectionFilters extends QueryFilters
{
    public function label($string)
    {
        $this->query->whereHas('label', function ($query) use ($string) {
            $query->whereName($string);
        });
    }

    public function model($id)
    {
        $this->query->where('ai_model_id', $id);
    }

    public function date($array)
    {
        $from = Carbon::createFromFormat('Y-m-d', $array[0])->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $array[1])->endOfDay();

        $this->query->whereBetween('created_at', [$from, $to]);
    }

    public function category($id)
    {
        $this->query->whereHas('model', function ($query) use ($id) {
            $query->whereHas('category', function ($query) use ($id) {
                $query->whereId($id);
            });
        });
    }
}
