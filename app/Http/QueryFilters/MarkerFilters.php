<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use Carbon\Carbon;

/**
 * Filter records based on query parameters.
 *
 */
class MarkerFilters extends QueryFilters
{
    public function creature($id)
    {
        $this->query->whereCreatureId($id);
    }

    public function activity($id)
    {
        $this->query->whereHas('activities', function ($query) use ($id) {
            $query->whereActivityId($id);
        });
    }

    public function date($date)
    {
        $from = Carbon::createFromFormat('Y-m-d', $date[0])->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $date[1])->endOfDay();

        $this->query->whereBetween('created_at', [$from, $to]);
    }
}
