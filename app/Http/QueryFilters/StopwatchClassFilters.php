<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

/**
 * Filter records based on query parameters.
 *
 */
class StopwatchClassFilters extends QueryFilters
{
    use OrderFilter;

    public function category($int)
    {
        $this->query->whereCategoryId($int);
    }
}
