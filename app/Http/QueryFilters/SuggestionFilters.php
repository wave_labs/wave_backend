<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

/**
 * Filter records based on query parameters.
 *
 */
class SuggestionFilters extends QueryFilters
{
    use OrderFilter;

    /**
     * Filter records based on the query parameter "source"
     * 
     * @param mixed $string
     * @return void
     */
    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('suggestion', 'like', '%' . $string . '%');
        });
    }

    public function source($array)
    {
        $this->query->whereHas('source', function ($query) use ($array) {
            $query->where('name', 'like', '%' . $array[0] . '%');
        });
    }

    public function suggestionType($array)
    {
        $this->query->whereHas('suggestionType', function ($query) use ($array) {
            $query->where('name', 'like', '%' . $array[0] . '%');
        });
    }

    public function status($array)
    {
        $this->query->where(function ($query) use ($array) {
            $query->where('status', 'like', '%' . $array[0] . '%');
        });
    }
}
