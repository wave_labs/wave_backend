<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;
use Carbon\Carbon;

/**
 * Filter records based on query parameters.
 *
 */
class StopwatchResultFilters extends QueryFilters
{
    use OrderFilter;

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('video_id', 'like', '%' . $string . '%')
                ->orWhere('occupation', 'like', '%' . $string . '%')
                ->orWhere('id', 'like', '%' . $string . '%')
                ->orWhereHas('stopwatchTimestamps', function ($query) use ($string) {
                    $query->WhereHas('class', function ($query) use ($string) {
                        $query->where('name', 'like', '%' . $string . '%');
                    });
                })
                ->orWhereHas('video', function ($query) use ($string) {
                    $query->WhereHas('category', function ($query) use ($string) {
                        $query->where('name', 'like', '%' . $string . '%');
                    });
                });
        });
    }

    public function category($id)
    {
        $this->query->whereHas('video', function ($query) use ($id) {
            $query->WhereHas('category', function ($query) use ($id) {
                $query->where('id', $id);
            });
        });
    }

    public function date($date)
    {
        $from = Carbon::createFromFormat('Y-m-d', $date[0])->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $date[1])->endOfDay();

        $this->query->whereBetween('created_at', [$from, $to]);
    }

    public function hasAi($boolean)
    {
        $this->query->whereHas('video', function ($query) use ($boolean) {
            $query->where('detection', $boolean);
        });
    }

    public function hasExpert($boolean)
    {
        $this->query->where('occupation', $boolean ? "=" : "!=", "marine biologist");
    }
}
