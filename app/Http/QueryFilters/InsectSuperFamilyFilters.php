<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectSuperFamilyFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('superFamily', 'like',  '%' . $string . '%');
    }

    public function infraOrder($string)
    {
        $this->query->whereHas('infraOrder', function ($query) use ($string) {
            $query->where('id', '=', $string);
        });
    }
}
