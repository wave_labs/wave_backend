<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

class CreatureTypeFilters extends QueryFilters
{
    use OrderFilter; 

	public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
                $query->where('taxa_category', 'like', '%' .$string . '%');
        });
    }
    public function acoustic_category($int)
    {
        $this->query->where('acoustic_category',$int);
    }

    public function acoustic_data($int)
    {
        $this->query->where('acoustic_data',$int);
    }
}
