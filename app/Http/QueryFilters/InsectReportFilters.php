<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectReportFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->whereHas('species', function ($query) use ($string) {
            $query->where('name', $string);
        })->orWhereHas('zone', function ($query) use ($string) {
            $query->where('region', 'like', '%' . $string . '%')
                ->orWhere('province', 'like', '%' . $string . '%')
                ->orWhere('locality', 'like', '%' . $string . '%');
        });
    }
}
