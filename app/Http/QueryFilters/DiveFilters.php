<?php

namespace App\Http\QueryFilters;

use App\Creature;
use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;
use Carbon\Carbon;
use Error;

class DiveFilters extends QueryFilters
{
    use OrderFilter;

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->Where(function ($query) use ($string) {
                for ($i = 0; $i < count($string); $i++) {
                    $stringed = $string[$i];
                    $query->where('number_diver', 'like', '%' . $stringed . '%')
                        ->orWhere('dive_time', 'like', '%' . $stringed . '%')
                        ->orWhere('max_depth', 'like', '%' . $stringed . '%')
                        ->orWhereHas('user', function ($query) use ($stringed) {
                            $query->WhereHas('user_person', function ($query) use ($stringed) {
                                $query->where('name', 'like', '%' . $stringed . '%');
                            })
                                ->orWhereHas('user_dive_center', function ($query) use ($stringed) {
                                    $query->where('name', 'like', '%' . $stringed . '%');
                                });
                        })
                        ->orWhereHas('diving_spot', function ($query) use ($stringed) {
                            $query->where('name', 'like', '%' . $stringed . '%');
                        })
                        ->orWhereHas('creatures', function ($query) use ($stringed) {
                            $query->where('name', 'like', '%' . $stringed . '%');
                        });
                }
            });
        });
    }

    public function date($array)
    {
        $from = Carbon::createFromFormat('Y-m-d', $array[0])->startOfDay();
        $to = Carbon::createFromFormat('Y-m-d', $array[1])->endOfDay();

        $this->query->whereBetween('created_at', [$from, $to]);
    }

    public function divingSpot($id)
    {
        $this->query->whereDivingSpotId($id);
    }

    public function creature($name)
    {
        $this->query->whereHas('dive_pivot', function ($query) use ($name) {
            $query->where("creature_id", "=", $name);
        });
    }
}
