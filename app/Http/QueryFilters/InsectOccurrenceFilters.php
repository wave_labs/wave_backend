<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectOccurrenceFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('higherGeography', 'like', '%' . $string . '%')
                ->orWhere('continent', 'like', '%' . $string . '%')
                ->orWhere('islandGroup', 'like', '%' . $string . '%')
                ->orWhere('island', 'like', '%' . $string . '%')
                ->orWhere('country', 'like', '%' . $string . '%')
                ->orWhere('region', 'like', '%' . $string . '%')
                ->orWhere('province', 'like', '%' . $string . '%')
                ->orWhere('county', 'like', '%' . $string . '%')
                ->orWhere('locality', 'like', '%' . $string . '%')
                ->orWhere('decimalLatitude', 'like', '%' . $string . '%')
                ->orWhere('decimalLongitude', 'like', '%' . $string . '%')
                ->orWhere('basisOfRecord', 'like', '%' . $string . '%');
        });
    }

    public function project($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('project_id', '=', $string)
                ->orWhere('project_id', '=', null);
        });
    }
}
