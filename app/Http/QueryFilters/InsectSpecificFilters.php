<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class InsectSpecificFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('specific', 'like',  '%' . $string . '%');
    }

    public function genus($string)
    {
        $this->query->whereHas('genus', function ($query) use ($string) {
            $query->where('id', '=', $string);
        });
    }
    
    public function subgenus($string)
    {
        $this->query->whereHas('subGenus', function ($query) use ($string) {
            $query->where('id', '=', $string);
        });
    }
}
