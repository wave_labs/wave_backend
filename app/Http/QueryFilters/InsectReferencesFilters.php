<?php

namespace App\Http\QueryFilters;

use App\Helper;
use App\InsectReference;
use App\InsectReferenceCategories;
use App\InsectReferenceHasTag;
use App\InsectReferenceKeyword;
use App\InsectReferenceTag;
use Cerbero\QueryFilters\QueryFilters;
use Error;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Filter records based on query parameters.
 *
 */
class InsectReferencesFilters extends QueryFilters
{

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('title', 'like', '%' . $string . '%')
                ->orWhere('date', 'like', '%' . $string . '%')
                ->orWhere('journal', 'like', '%' . $string . '%')
                ->orWhere('editor', 'like', '%' . $string . '%');
        });
    }

    public function project($string)
    {
        $this->query->where(function ($query) use ($string) {
            $query->where('project_id', '=', $string)
                ->orWhere('project_id', '=', null);
        });
    }

    /* public function sinonimia($string)
    {
        $this->query->whereHas('reports', function ($query) use ($string) {
            $query->whereHas('sinonimia', function ($q) use ($string) {
                $q->where('name', 'like', '%' . $string . '%');
            });
        });
    } */

    public function author($string)
    {
        $this->query->whereHas('authors', function ($query) use ($string) {
            $query->where('name', 'like', '%' . $string . '%');
        });
    }

    public function tag($string)
    {
        // $tags = InsectReferenceTag::where('tag', '=',  $string)->pluck('id');
        $this->query->whereHas('tags', function ($query) use ($string) {
            $query->where('tag', 'like', '%' . $string . '%');
        });
    }

    public function keyword($string)
    {
        // $keywords = InsectReferenceKeyword::where('keyword', '=',  $string)->pluck('id');
        $this->query->whereHas('keywords', function ($query) use ($string) {
            $query->where('keyword', 'like', '%' . $string . '%');
        });
    }

    public function category($string)
    {
        // $categories = InsectReferenceCategories::where('category', '=',  $string)->pluck('id');
        $this->query->whereHas('categories', function ($query) use ($string) {
            $query->where('category', 'like', '%' . $string . '%');
        });
    }
}
