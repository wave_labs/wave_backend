<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;

/**
 * Filter records based on query parameters.
 *
 */
class ActivityFilters extends QueryFilters
{
    /**
     * Filter records based on the query parameter "creature"
     * 
     * @param mixed $id
     * @return void
     */
    public function creature($id)
    {
        $this->query->whereHas('creatures', function ($query) use ($id) {
            $query->whereCreatureId($id);
        });
    }
}
