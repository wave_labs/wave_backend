<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

class CapsuleFilters extends QueryFilters
{
    use OrderFilter; 

	public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
                $query->where('description', 'like', '%' .$string . '%')
                        ->orWhereHas('user', function ($query) use ($string) {
                            $query->where('first_name', 'like', '%' .$string . '%')
                                ->orWhere('last_name', 'like', '%' .$string . '%');
                        });
        });
    }

    public function assigned($int)
    {
        $this->query->where('assigned',$int);
    }
}
