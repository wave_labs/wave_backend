<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use App\Traits\OrderFilter;

class AbundanceFilters extends QueryFilters
{
	use OrderFilter; 

    public function search($string)
    {
        $this->query->where(function ($query) use ($string) {
                $query->where('level_1', 'like', '%' .$string . '%')
                    ->orWhere('level_2', 'like', '%' .$string . '%')
                    ->orWhere('level_3', 'like', '%' .$string . '%')
                    ->orWhere('level_4', 'like', '%' .$string . '%')
                    ->orWhereHas('creature_types_id', function ($query) use ($string) {
                        $query->where('id', 'like', '%' .$string . '%');
                    });
         });
       
    }
    public function type($array){
        $this->query->whereHas('creature_type', function ($query) use ($array) {
            $query->whereIn('id', $array);
        });
    } 
}
