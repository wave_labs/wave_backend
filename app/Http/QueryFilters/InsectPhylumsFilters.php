<?php

namespace App\Http\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use Error;
use Illuminate\Support\Facades\DB;
use App\InsectPhylum;

/**
 * Filter records based on query parameters.
 *
 */
class InsectPhylumsFilters extends QueryFilters
{
    public function search($string)
    {
        $this->query->where('phylum', 'like',  '%' . $string . '%');
    }
}
