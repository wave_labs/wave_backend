<?php

namespace App\Http\Controllers;

use App\InsectOrder;
use Illuminate\Http\Request;

class InsectOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectOrder  $insectOrder
     * @return \Illuminate\Http\Response
     */
    public function show(InsectOrder $insectOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectOrder  $insectOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectOrder $insectOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectOrder  $insectOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectOrder $insectOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectOrder  $insectOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectOrder $insectOrder)
    {
        //
    }
}
