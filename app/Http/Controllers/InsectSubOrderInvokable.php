<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectSubOrderFilters;
use App\Http\Resources\InsectSubOrderResource;
use App\InsectSubOrder;
use Illuminate\Http\Request;

class InsectSubOrderInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectSubOrderFilters $filters)
    {
        return response()->json([
            'data' =>  InsectSubOrder::filterBy($filters)->select('subOrder', 'id')->get()
        ]);
    }
}
