<?php

namespace App\Http\Controllers;

use Artisan;
use Response;
use File;
use App\Version;
use DB;

class ExportController extends Controller
{
    public function create($type)
    {
        if ($type == 'litter' || $type == 'dive' || $type == 'sighting') {
            $old = (int) Version::get($type);

            DB::beginTransaction();

            Version::updateVersion($type);
            File::delete(public_path('sqlite/' . $type . '/' . $old . '.db'));
            $new = $old + 1;

            Artisan::call('sql:dump', array('--' . $type . '' => 'true'));
            Artisan::call('sql:convert', array('--' . $type . '' => 'true', '--name' => $new));

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Created version ' . $new . ' on ' . $type
            ], 200);
        }
        return response()->json(
            [
                'success' => false,
                'message' => 'Invalid parameter ' . $type
            ],
            400
        );
    }

    public function download($type)
    {
        if ($type == 'litter' || $type == 'dive' || $type == 'sighting') {
            $version = (int) Version::get($type);
            $db = public_path() . "/sqlite/" . $type . "/" . $version . ".db";

            if (file_exists($db)) {
                return Response::download($db);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Database version does not match, update server file.'
                ], 400);
            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Invalid parameter ' . $type
        ], 400);
    }

    public function index($type)
    {
        if ($type == 'litter' || $type == 'dive' || $type == 'sighting') {

            $version = (int) Version::get($type);

            return response()->json(
                [
                    'success' => true,
                    'version' => $version
                ],
                200
            );
        }

        return response()->json(
            [
                'success' => false,
                'message' => 'Invalid paramater ' . $type
            ],
            400
        );
    }
}
