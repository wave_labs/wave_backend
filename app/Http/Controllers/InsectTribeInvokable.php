<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectTribeFilters;
use App\Http\Resources\InsectTribeResource;
use App\InsectTribe;
use Illuminate\Http\Request;

class InsectTribeInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectTribeFilters $filters)
    {
        return response()->json([
            'data' =>  InsectTribe::filterBy($filters)->select('tribe', 'id')->get()
        ]);
    }
}
