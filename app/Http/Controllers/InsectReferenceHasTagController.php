<?php

namespace App\Http\Controllers;

use App\InsectReferenceHasTag;
use Illuminate\Http\Request;

class InsectReferenceHasTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectReferenceHasTag  $insectReferenceHasTag
     * @return \Illuminate\Http\Response
     */
    public function show(InsectReferenceHasTag $insectReferenceHasTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectReferenceHasTag  $insectReferenceHasTag
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectReferenceHasTag $insectReferenceHasTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectReferenceHasTag  $insectReferenceHasTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectReferenceHasTag $insectReferenceHasTag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectReferenceHasTag  $insectReferenceHasTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectReferenceHasTag $insectReferenceHasTag)
    {
        //
    }
}
