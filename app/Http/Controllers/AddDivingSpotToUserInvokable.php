<?php

namespace App\Http\Controllers;

use App\UserHasDivingSpot;
use Illuminate\Http\Request;

class AddDivingSpotToUserInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($divingSpot)
    {
        UserHasDivingSpot::create([
            'user_id' => auth()->user()->id,
            'diving_spot_id' => $divingSpot
        ]);

        return response()->json(null, 201);
    }
}
