<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Feedback;
use App\Form;
use Illuminate\Http\Request;
use App\Http\Resources\FeedbackResource;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($form)
    {
        $id = Form::where('form', $form)->value('id');

        return FeedbackResource::collection(Feedback::where('form_id', $id)->paginate(20));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feedback  $feedbackForm
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $feedbackForm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feedback  $feedbackForm
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedbackForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feedback  $feedbackForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedback $feedbackForm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feedback  $feedbackForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $feedbackForm)
    {
        //
    }
}
