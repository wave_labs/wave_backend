<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSuggestionRequest;
use App\Http\Resources\SuggestionResource;
use App\Http\QueryFilters\SuggestionFilters;
use App\Http\Requests\UpdateSuggestionRequest;
use App\Suggestion;
use Illuminate\Http\Request;

class SuggestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = SuggestionFilters::hydrate($request->query());
        return SuggestionResource::collection(Suggestion::filterBy($filters)->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSuggestionRequest $request)
    {
        $validator = $request->validated();

        $suggestion = Suggestion::create($validator);
        $suggestion = $suggestion->refresh();

        return new SuggestionResource($suggestion);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function show(Suggestion $suggestion)
    {
        return new SuggestionResource($suggestion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSuggestionRequest $request, Suggestion $suggestion)
    {
        $validator = $request->validated();

        $suggestion->update($validator);

        return new SuggestionResource($suggestion);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suggestion $suggestion)
    {
        $suggestion->delete();

        return response()->json(null, 204);
    }
}
