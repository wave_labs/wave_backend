<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\QueryFilters\StopwatchClassFilters;
use App\Http\Resources\StopwatchClassResource;
use App\StopwatchClass;
use Illuminate\Http\Request;

class StopwatchClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = StopwatchClassFilters::hydrate($request->query());

        return StopwatchClassResource::collection(StopwatchClass::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StopwatchClass  $stopwatchClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(StopwatchClass $stopwatchClass)
    {
        //
    }
}
