<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\SightingEstimation;
use App\Http\Resources\SightingEstimationResource;

class SightingEstimationController extends Controller
{
    /**
     * Display a listing of the sighting estimation.
     *
     * @return App\Http\Resources\SightingEstimationResource
     */
    public function index()
    {
        return SightingEstimationResource::collection(SightingEstimation::paginate(10));    
    }

    /**
     * Store a newly created sighting estimation in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\SightingEstimationResource
     */
    public function store(Request $request)
    {
        $sightingEstimation = SightingEstimation::create($request->all());

        return new SightingEstimationResource($sightingEstimation);
    }

    /**
     * Display the specified sighting estimation.
     *
     * @param  App\SightingEstimation
     * @return App\Http\Resources\SightingEstimationResource
     */
    public function show(SightingEstimation $sightingEstimation)
    {
        return new SightingEstimationResource($sightingEstimation);
    }

    /**
     * Update the specified sighting estimation in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\SightingEstimation
     * @return App\Http\Resources\SightingEstimationResource
     */
    public function update(Request $request, SightingEstimation $sightingEstimation)
    {
        $sightingEstimation->update($request->all());

        return new SightingEstimationResource($sightingEstimation);
    }

    /**
     * Remove the specified sighting estimation from storage.
     *
     * @param  App\SightingEstimation
     * @return \Illuminate\Http\Response
     */
    public function destroy(SightingEstimation $sightingEstimation)
    {
        $sightingEstimation->delete();

        return response()->json(null, 204);
    }
}
