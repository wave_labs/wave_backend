<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserPersonResource;
use App\UserPerson;
use Illuminate\Http\Request;

class UserPersonController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user-list', ['only' => ['index']]);
        $this->middleware('permission:user-create', ['only' => ['store']]);
        $this->middleware('permission:user-update', ['only' => ['update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return UserPersonResource::collection(UserPerson::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\Response
     */
    public function store(Request $request)
    {
        $user_person = UserPerson::create($request->all());

        return new UserPersonResource($user_person);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\user_person  $user_person
     * @return \Illuminate\Http\UserPersonResource
     */
    public function show(UserPerson $UserPerson)
    {
        return new UserPersonResource($UserPerson);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserPerson 
     * @return App\Http\Resources\UserPersonResource
     */
    public function update(Request $request, UserPerson $user_person)
    {
        $user_person->update($request->all());
        return new UserPersonResource($user_person);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserPerson  $user_person
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPerson $user_person)
    {
        $user_person->delete();
        $user_person->user->delete();

        return response()->json(null, 204);
    }
}
