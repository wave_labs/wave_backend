<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Resources\CreatureTypeResource;
use App\Http\QueryFilters\CreatureTypeFilters;
use App\Http\Requests\StoreCreatureTypeRequest;
use App\CreatureType;
use File;
use Intervention\Image\Facades\Image as IMG;

class CreatureTypeController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:creature-list', ['only' => ['index, show']]);
        $this->middleware('permission:creature-create', ['only' => ['store']]);
        $this->middleware('permission:creature-update', ['only' => ['update']]);
        $this->middleware('permission:creature-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the creature type.
     *
     * @return App\Http\Resources\CreatureTypeResource
     */
    public function index(Request $request)
    {
        $filters = CreatureTypeFilters::hydrate($request->query());
        return CreatureTypeResource::collection(CreatureType::filterBy($filters)->paginate(20));
    }

    /**
     * Display a listing of the creature for selection.
     *
     * @return App\Http\Resources\CreatureResource
     */
    public function selector(Request $request)
    {
        $filters = CreatureTypeFilters::hydrate($request->query());
        return CreatureTypeResource::collection(CreatureType::filterBy($filters)->get());
    }

    /**
     * Store a newly created creature type in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\CreatureTypeResource
     */
    public function store(StoreCreatureTypeRequest $request)
    {
        $validator = $request->validated();
        $creatureType = CreatureType::create($validator);

        IMG::make($validator['image'])->encode('png', 100)->resize(null, 300, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        })->save(public_path('storage/images/creature-type/' . $creatureType->id));

        $creatureType->image = '/images/creature-type/' . $creatureType->id;
        $creatureType->save();

        return new CreatureTypeResource($creatureType);
    }

    /**
     * Display the specified creature type.
     *
     * @param  App\CreatureType
     * @return App\Http\Resources\CreatureTypeResource
     */
    public function show(CreatureType $creatureType)
    {
        return new CreatureTypeResource($creatureType);
    }

    /**
     * Update the specified creature type in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\CreatureType
     * @return App\Http\Resources\CreatureTypeResource
     */
    public function update(StoreCreatureTypeRequest $request, CreatureType $creatureType)
    {
        $validator = $request->validated();

        $creatureType->update($validator);

        return new CreatureTypeResource($creatureType);
    }

    /**
     * Remove the specified creature type from storage.
     *
     * @param  App\CreatureType
     * @return \Illuminate\Http\Response
     */
    public function destroy(CreatureType $creatureType)
    {
        File::delete(public_path('storage' . $creatureType->image));
        $creatureType->delete();

        return response()->json(null, 204);
    }
}
