<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\MarkerMapResource;
use App\Marker;
use Illuminate\Http\Request;
use App\Http\QueryFilters\MarkerFilters;

class MarkerCoordinatesController extends Controller
{
    public function __invoke(Request $request)
    {
        $filters = MarkerFilters::hydrate($request->query());
        return MarkerMapResource::collection(Marker::filterBy($filters)->get());
    }
}
