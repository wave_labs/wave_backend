<?php

namespace App\Http\Controllers\API;

use App\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTripRequest;
use App\Http\Resources\TripResource;
use Illuminate\Support\Facades\Validator;

use App\Trip;
use App\TripPosition;
use Illuminate\Http\Request;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return TripResource::collection(Trip::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTripRequest $request)
    {
        $validator = $request->validated();

        $trip = Trip::create($validator);

        if (array_key_exists('positions', $validator)) {
            foreach ($validator["positions"] as $position) {
                TripPosition::create([
                    'trip_id' => $trip->id,
                    'latitude' => $position["latitude"],
                    'longitude' => $position["longitude"]
                ]);
            }
        }

        return response()->json([
            'success' => true,
            'message' => 'Trip created with success',
            'data' => new TripResource($trip),
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function show(Trip $trip)
    {
        return new TripResource($trip);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trip $trip)
    {
        $validator = Validator::make($request->all(), [
            'end' => 'required|date',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $trip->end = $request->end;
        $trip->save();

        return response()->json([
            'success' => true,
            'message' => 'Trip ended with success',
            'data' => new TripResource($trip),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trip $trip)
    {
        $trip->delete();

        return response()->json(null, 204);
    }
}
