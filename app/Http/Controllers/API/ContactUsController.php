<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ContactUs;
use Mail;

class ContactUsController extends Controller
{
    public function index(Request $request)
    {
        return $request;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'section' => 'required',
            'subject' => 'required'
        ]);

        ContactUs::create($request->all());

        Mail::send(
            'mail.contactus',
            array(
                'name' => $request->name,
                'email' => $request->email,
                'message' => $request->message,
                'section' => $request->section,
                'subject' => $request->subject
            ),
            function ($message) {
                $message->from('saquib.gt@gmail.com');
                $message->to('joseruben98@hotmail.com', 'Admin')->subject('Cloudways Feedback');
            }
        );

        return response()->json([
            'success' => true,
            'message' => 'Thanks for getting in contact. We will get back to you.',
        ], 201);
    }

    public function show(Creature $creature)
    {
    }

    public function update(Request $request, Creature $creature)
    {
    }

    public function destroy(Creature $creature)
    {
    }
}
