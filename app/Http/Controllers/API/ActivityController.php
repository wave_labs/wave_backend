<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Activity;
use App\Http\QueryFilters\ActivityFilters;
use App\Http\Resources\ActivityResource;
use Illuminate\Http\Request;
use File;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = ActivityFilters::hydrate($request->query());
        return ActivityResource::collection(Activity::filterBy($filters)->paginate(10));
    }

    public function selector(Request $request)
    {
        $filters = ActivityFilters::hydrate($request->query());
        return ActivityResource::collection(Activity::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        return new ActivityResource($activity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        File::delete(public_path($activity->image));
        $activity->delete();

        return response()->json(null, 204);
    }
}
