<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Survey;
use Illuminate\Http\Request;
use App\Http\QueryFilters\SurveyFilters;
use App\Http\Resources\SurveyResource;
use Illuminate\Support\Facades\Validator;
use App\Dive;
use App\PendingPoint;
use App\User;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = SurveyFilters::hydrate($request->query());
        return SurveyResource::collection(Survey::filterBy($filters)->paginate(20));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validation = Validator::make($request->all(), [
            'user_id' => 'required',
            'creature_id' => 'required',
            'abundance_value' => 'required',
            'date' => 'required'
        ]);

        if ($validation->fails()) {
            return response()->json([
                'status' => 'Required fields missing',
                'errors' => $validation->errors()
            ], 422);
        }

        if (is_null($request->dive_id)) {
            $validation = Validator::make($request->all(), [
                'diving_spot_id' => 'required',
                'max_depth'  => 'required',
                'dive_time' => 'required',
                'number_diver' => 'required',
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'status' => 'Required fields missing',
                    'errors' => $validation->errors()
                ], 422);
            }

            $dives = new Request([
                'diving_spot_id' => $request->diving_spot_id,
                'user_id' => $request->user_id,
                'max_depth' => $request->max_depth,
                'dive_time' => $request->dive_time,
                'number_diver' => $request->number_diver,
            ]);
            $diveInsert = Dive::create($dives->toArray());

            $diveid = $diveInsert->id;
        } else {
            $diveid = $request->dive_id;
        }

        $survey = new Request([
            'creature_id' => $request->creature_id,
            'user_id' => $request->user_id,
            'dive_id' => $diveid,
            'date' => $request->date,
            'abundance_value' => $request->abundance_value,
        ]);

        $surveyInsert = Survey::create($survey->toArray());

        //Calculate and update points
        $points = PendingPoint::calculate($request, 'dive', $surveyInsert->id);
        //User::updatePoints($points, $request->user_id);

        if (is_null($request->dive_id)) {
            return response()->json([
                'status' => 'success',
                'dive' => $diveInsert,
                'survey' => $surveyInsert
            ], 200);
        } else {
            $diveInsert = Dive::find($diveid);
            return response()->json([
                'status' => 'success',
                'dive' => $diveInsert,
                'survey' => $surveyInsert
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function show(Survey $survey)
    {
        return new SurveyResource($survey);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survey $survey)
    {
        $survey->update($request->all());

        return new SurveyResource($survey);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey)
    {
        $survey->delete();

        return response()->json([
            'message' => 'Successfully deleted survey!'
        ], 200);
    }
}
