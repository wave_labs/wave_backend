<?php

namespace App\Http\Controllers\API;

use App\Exports\IotDeviceExport;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\IotDevice;
use App\Helper;
use App\Http\Resources\IotDeviceResource;
use App\Http\QueryFilters\IotDeviceFilters;
use App\Http\Requests\StoreIotDeviceRequest;
use App\Http\Requests\UpdateIotDeviceRequest;
use App\IotDeviceField;
use App\IotDeviceFieldHasRule;
use App\IotFeature;
use App\IotFieldFact;
use App\IotFieldFactHasParameter;
use App\IotSection;
use App\IotTemplate;
use App\User;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class IotDeviceController extends Controller
{

    function __construct()
    {
        /*$this->middleware('permission:iotDevice-list', ['only' => ['index, show']]);
        $this->middleware('permission:iotDevice-create', ['only' => ['store']]);
        $this->middleware('permission:iotDevice-update', ['only' => ['update']]);
        $this->middleware('permission:iotDevice-delete', ['only' => ['destroy']]);*/ }

    /**
     * Display a listing of the iotDevice.
     *
     * @return App\Http\Resources\IotDeviceResource
     */
    public function index(Request $request)
    {
        $filters = IotDeviceFilters::hydrate($request->query());
        // $users = Helper::getUserFromRequest($request);
        if ($request->header('Authorization')) {
            $user = JWTAuth::setToken($request->header('Authorization'))->user()->id;
            return IotDeviceResource::collection(IotDevice::filterBy($filters)->get());
        }
    }

    public function csvExport(Request $request)
    {
        return (new IotDeviceExport($request))->download('dives.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    /**
     * Display a listing of the iotDevice for selection.
     *
     * @return App\Http\Resources\IotDeviceResource
     */
    public function selector(IotDeviceFilters $filters)
    {
        return IotDeviceResource::collection(IotDevice::filterBy($filters)->get());
    }

    /**
     * Store a newly created iotDevice in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\IotDeviceResource
     */
    public function store(StoreIotDeviceRequest $request)
    {
        $validator = $request->validated();
        DB::beginTransaction();
        $iotDevice = IotDevice::create($validator);

        $iotDevice->handleCategory($validator);
        if (array_key_exists('fields', $validator)) {
            foreach ($validator['fields'] as $value) {
                if (isset($value['key']) && $value['key'] != "") {

                    $newField = IotDeviceField::create([
                        'device_id' => $iotDevice->id,
                        'key' => $value['key']
                    ]);

                    if (isset($value['rule']) && $value['rule'] != "") {
                        IotDeviceFieldHasRule::create([
                            'field_id' => $newField->id,
                            'rule_id' => $value['rule']
                        ]);
                    }
                }
            }
        }

        if (array_key_exists('gateways', $validator)) {
            $iotDevice->gateways()->sync($validator['gateways']);
        }

        if (array_key_exists('users', $validator)) {
            $iotDevice->users()->sync($validator['users']);
            $admin = User::whereEmail("admin@admin.wave")->value('id');
            $exists = $iotDevice->users()->whereUserId($admin)->exists();
            if (!$exists) {
                $iotDevice->users()->attach($admin);
            }
        }

        if (array_key_exists('sections', $validator) && $iotDevice->fields->count()) {
            $template = IotTemplate::find($validator['template_id']);
            foreach ($validator['sections'] as $sectionName => $data) {
                $section = IotSection::where('name', $sectionName)->whereHas('template', function ($query) use ($template) {
                    $query->where('template_id', $template->id);
                })->first();

                if ($section) {
                    if (array_key_exists('section_title', $validator)) {
                        if (array_key_exists($sectionName, $validator['section_title'])) {
                            $iotDevice->sections()->attach($section->id, ['title' => $validator['section_title'][$sectionName]]);
                        }
                    }


                    foreach ($data as $value) {
                        $field = IotDeviceField::where('device_id', $iotDevice->id)->where('key', $value['field'])->value('id');
                        $feature = IotFeature::find($value['feature']);
                        $parameters = $feature->parameters;

                        $field_fact = IotFieldFact::create([
                            'feature_id' => $value['feature'],
                            'field_id' => $field,
                            'section_id' => $section->id,
                        ]);

                        foreach ($parameters as $parameter) {
                            if (array_key_exists($parameter->name, $value)) {
                                IotFieldFactHasParameter::create([
                                    'field_fact_id' => $field_fact->id,
                                    'parameter_id' => $parameter->id,
                                    'value' => $value[$parameter->name],
                                ]);
                            }
                        }
                    }
                }
            }
        }

        if ($request->hasFile('photo')) {
            $iotDevice->savePhoto($validator['photo']);
        }

        DB::commit();
        return new IotDeviceResource($iotDevice);
    }

    /**
     * Display the specified iotDevice.
     *
     * @param  App\IotDevice
     * @return App\Http\Resources\IotDeviceResource
     */
    public function show($iotDevice)
    {
        $iotDevice = IotDevice::find($iotDevice);

        return new IotDeviceResource($iotDevice);
    }

    public function nextAndPrior($device)
    {
        $device = IotDevice::find($device);

        $previous = IotDevice::where('id', '<', $device->id)->orderByDesc('id')->first();
        $next = IotDevice::where('id', '>', $device->id)->orderBy('id')->first();

        return response()->json([
            'success' => true,
            'previous' => $previous ? new IotDeviceResource($previous) : null,
            'next' => $next ? new IotDeviceResource($next) : null,
        ], 200);
    }

    /**
     * Update the specified iotDevice in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\IotDevice
     * @return App\Http\Resources\IotDeviceResource
     */
    public function update(UpdateIotDeviceRequest $request, IotDevice $iotDevice)
    {
        $validator = $request->validated();
        $iotDevice->update($validator);

        $iotDevice->handleCategory($validator);
        $remaingKeys = $validator['fields'];
        if (array_key_exists('fields', $validator)) {
            $iotDeviceFields = $iotDevice->fields;

            foreach ($iotDeviceFields as $iotDeviceField) {
                $hasUpdated = false;
                foreach ($validator['fields'] as $aFieldKey => $aField) {
                    if ($aField['key'] == $iotDeviceField['key']) {
                        Helper::printToConsole("key exists: " . $aField['key']);
                        if (isset($aField['rule']) && $aField['rule'] != "") {
                            $existingRule = IotDeviceFieldHasRule::where('field_id', $iotDeviceField['id'])->first();

                            if ($existingRule) {
                                $existingRule->update([
                                    'rule_id' => $aField['rule']
                                ]);
                            }
                        }

                        unset($remaingKeys[$aFieldKey]);
                    } else $hasUpdated = true;
                }
                if (!$hasUpdated) {
                    $iotDeviceField->delete();
                }
            }

            foreach ($remaingKeys as $value) {
                if (isset($value['key']) && $value['key'] != "") {
                    $newField = IotDeviceField::create([
                        'device_id' => $iotDevice->id,
                        'key' => $value['key']
                    ]);
                    if (isset($value['rule']) && $value['rule'] != "") {
                        IotDeviceFieldHasRule::create([
                            'field_id' => $newField->id,
                            'rule_id' => $value['rule']
                        ]);
                    }
                }
            }
        }

        if (array_key_exists('gateways', $validator)) {
            $iotDevice->gateways()->sync($validator['gateways']);
        }

        if (array_key_exists('users', $validator)) {
            $iotDevice->users()->sync($validator['users']);
            $admin = User::whereEmail("admin@admin.wave")->value('id');
            $exists = $iotDevice->users()->whereUserId($admin)->exists();
            if (!$exists) {
                $iotDevice->users()->attach($admin);
            }
        }

        if (array_key_exists('sections', $validator)) {

            $template = IotTemplate::find($validator['template_id']);

            $iotDevice->sections()->detach();
            foreach ($validator['sections'] as $sectionName => $data) {

                $section = IotSection::where('name', $sectionName)->whereHas('template', function ($query) use ($template) {
                    $query->where('template_id', $template->id);
                })->first();

                if ($section) {

                    $iotDevice->sections()->attach($section->id);


                    foreach ($data as $value) {

                        $field = IotDeviceField::where('device_id', $iotDevice->id)->where('key', $value['field'])->value('id');
                        $feature = IotFeature::find($value['feature']);
                        $parameters = $feature->parameters;

                        $field_fact = IotFieldFact::create([
                            'feature_id' => $value['feature'],
                            'field_id' => $field,
                            'section_id' => $section->id,
                        ]);

                        foreach ($parameters as $parameter) {
                            if (array_key_exists($parameter->name, $value)) {
                                IotFieldFactHasParameter::create([
                                    'field_fact_id' => $field_fact->id,
                                    'parameter_id' => $parameter->id,
                                    'value' => $value[$parameter->name],
                                ]);
                            }
                        }
                    }
                }
            }
        }

        if ($request->hasFile('photo')) {
            $iotDevice->updatePhoto($validator['photo']);
        }


        return new IotDeviceResource($iotDevice);
    }

    /**
     * Remove the specified iotDevice from storage.
     *
     * @param  App\IotDevice
     * @return \Illuminate\Http\Response
     */
    public function destroy(IotDevice $iotDevice)
    {
        $iotDevice->images()->delete();
        $iotDevice->delete();

        return response()->json(null, 204);
    }
}
