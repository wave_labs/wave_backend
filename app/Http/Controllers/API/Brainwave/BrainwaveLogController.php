<?php

namespace App\Http\Controllers\API\Brainwave;

use App\Http\Controllers\Controller;

use App\BrainwaveLog;
use Illuminate\Http\Request;

class BrainwaveLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BrainwaveLog  $brainwaveLog
     * @return \Illuminate\Http\Response
     */
    public function show(BrainwaveLog $brainwaveLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BrainwaveLog  $brainwaveLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BrainwaveLog $brainwaveLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BrainwaveLog  $brainwaveLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(BrainwaveLog $brainwaveLog)
    {
        //
    }
}
