<?php

namespace App\Http\Controllers\API\Brainwave;

use App\BrainwaveReport;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FetchBrainwaveAnnotationsInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(BrainwaveReport $brainwaveReport)
    {
        $folderName = explode(".", $brainwaveReport->file);
        $sep = "";
        if ($brainwaveReport->annotations == "file_all_subtypes.csv") {
            $sep = "\r\n";
        } else {
            $sep = "\n";
        }
        $annotations  = Storage::disk('wave-server')->get("/datasets/eeg/" . $folderName[0] . "/" . $brainwaveReport->annotations);
        $ann_arr = array_map('intval', explode($sep, $annotations));
        // $ann_arr =  explode("\r\n", $annotations);
        $values  = Storage::disk('wave-server')->get("/datasets/eeg/" . $folderName[0] . "/" . $brainwaveReport->og_values);
        $val_arr = array_map('floatval', explode(",", $values));
        #$lines = preg_split('/\n|\r\n?/', $file);
        #return $lines;
        #return explode(" ", (string) $file);
        return response()->json([
            'success' => true,
            'annotations' => $ann_arr,
            'values' => $val_arr,
        ], 200);
    }
}
