<?php

namespace App\Http\Controllers\API\Brainwave;

use App\Http\Controllers\Controller;

use App\BrainwaveReport;
use App\Http\Requests\BrainwaveReportRequest;
use App\Http\Resources\BrainwaveReportResource;
use App\Jobs\HandleBrainwaveJob;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Spatie\Ssh\Ssh;

class BrainwaveReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BrainwaveReportResource::collection(BrainwaveReport::paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrainwaveReportRequest $request)
    {
        $validator = $request->validated();
        $filename = uniqid();
        $request->file('file')->storeAs('/datasets/eeg/' . $filename, $filename . ".edf", 'wave-server');
        $validator["file"] = $request->file('file')->storeAs('brainwave', $filename);
        $validator["file"] = $filename . ".edf";



        $brainwave = BrainwaveReport::create($validator);


        #->usePrivateKey('/home/ruben/.ssh/id_ecdsa')->disablePasswordAuthentication()
        #$process = Ssh::create('ruben', '10.20.4.1', 10022)->execute('python3 /datasets/eeg/eeg.py ' . $filename);
        #$connection = ssh2_connect('10.20.4.1', 10022);
        #$con = ssh2_connect($hostAddr, $hostPort, ['hostkey' => 'ecdsa-sha2-nistp256,ssh-rsa']);
        #ssh2_auth_password($connection, 'ruben', 'PVn3fKn5s3!jA8H+');

        #$stream = ssh2_exec($connection, 'python3 /datasets/eeg/eeg.py ' . $filename);
        HandleBrainwaveJob::dispatch($filename, $brainwave->id);
        return new BrainwaveReportResource($brainwave);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BrainwaveReport  $brainwaveReport
     * @return \Illuminate\Http\Response
     */
    public function show(BrainwaveReport $report)
    {
        return new BrainwaveReportResource($report);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BrainwaveReport  $brainwaveReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BrainwaveReport $report)
    {

        $csvPath = storage_path('testeeeg.csv');

        $fd = fopen($csvPath, 'w+');

        fputcsv($fd, $request->annotations, PHP_EOL);
        fclose($fd);

        $folderName = explode(".", $report->file);
        $filename = 'file_all_subtypes_' . uniqid();
        $report->annotations = $filename . '.csv';
        $report->save();
        Storage::disk('wave-server')->putFileAs('/datasets/eeg/' . $folderName[0] . '/', new File($csvPath), $filename . ".csv");
        // Storage::disk('wave-server')->putFileAs($fd, '/datasets/eeg/' . $brainwaveReport->file . '/' . $filename . ".csv");

        #blha
        return new BrainwaveReportResource($report);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BrainwaveReport  $brainwaveReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(BrainwaveReport $report)
    {
        //
    }
}
