<?php

namespace App\Http\Controllers\API\Brainwave;

use App\Http\Controllers\Controller;

use App\BrainwaveAnnotation;
use Illuminate\Http\Request;

class BrainwaveAnnotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BrainwaveAnnotation  $brainwaveAnnotation
     * @return \Illuminate\Http\Response
     */
    public function show(BrainwaveAnnotation $brainwaveAnnotation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BrainwaveAnnotation  $brainwaveAnnotation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BrainwaveAnnotation $brainwaveAnnotation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BrainwaveAnnotation  $brainwaveAnnotation
     * @return \Illuminate\Http\Response
     */
    public function destroy(BrainwaveAnnotation $brainwaveAnnotation)
    {
        //
    }
}
