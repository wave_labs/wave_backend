<?php

namespace App\Http\Controllers\API\Brainwave;

use App\BrainwaveReport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ZipBrainwaveInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(BrainwaveReport $brainwaveReport)
    {
        $folderName = explode(".", $brainwaveReport->file);
        $exists = Storage::disk('wave-server')->exists("/" . $folderName[0] . ".zip");

        if ($exists) {
            return Storage::disk('wave-server')->download("/" . $folderName[0] . ".zip");
        } else {
            return "file does not exist";
        }




        // return Response::download($file, "template.zip");


        // $zip = new ZipArchive;
        // $zip_path = storage_path("app/brainwave/template.zip");
        // // $zip_path = storage_path("app/brainwave/" . $folderName[0] . ".zip");

        // if ($zip->open($zip_path, ZipArchive::CREATE) === true) {
        //     foreach ($files as $path) {
        //         $extension = explode('.', $path);
        //         if ($extension[1] == "csv") {

        //             $currentFile = Storage::disk('wave-server')->get($path);
        //             return $currentFile;
        //             if (file_exists($path)) {
        //                 $zip->addFile($path);
        //             }
        //         }


        //         // $filePath = $directoryFile->getRealPath();
        //         // $relativePath = 'validation/' . basename($filePath);
        //         // $zip->addFile($filePath, $relativePath . ".png");

        //     }
        //     $zip->close();
        // }



        // return response()->download($zip);
        // $validationPath = storage_path($path . 'validation/');
        // $trainPath = storage_path($path . 'train/');

        // $validationDirectory = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($validationPath));
        // $trainDirectory = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($trainPath));

        // $zip_file = storage_path('exports/spectrograms.zip');
        // $zip = new ZipArchive;

        // if ($zip->open($zip_file, ZipArchive::CREATE) === TRUE) {
        //     $zip = self::resetZipSpectrograms($zip);
        //     foreach ($validationDirectory as $directoryFile) {
        //         if (file_exists($directoryFile) && is_file($directoryFile)) {
        //             $filePath = $directoryFile->getRealPath();
        //             $relativePath = 'validation/' . basename($filePath);
        //             $zip->addFile($filePath, $relativePath . ".png");
        //         }
        //     }
        //     foreach ($trainDirectory as $directoryFile) {
        //         if (file_exists($directoryFile) && is_file($directoryFile)) {
        //             $filePath = $directoryFile->getRealPath();
        //             $relativePath = 'train/' . basename($filePath);
        //             $zip->addFile($filePath, $relativePath . ".png");
        //         }
        //     }
        // }
        // $zip->close();
    }
}
