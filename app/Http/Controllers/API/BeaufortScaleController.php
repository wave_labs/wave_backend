<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\BeaufortScale;
use App\Http\Resources\BeaufortScaleResource;

class BeaufortScaleController extends Controller
{
    /**
     * Display a listing of the beaufort scale.
     *
     * @return App\Http\Resources\BeaufortScaleResource
     */
    public function index()
    {
        return BeaufortScaleResource::collection(BeaufortScale::paginate(25));
    }

    /**
     * Display a listing of the beaufort scale.
     *
     * @return App\Http\Resources\BeaufortScaleResource
     */
    public function selector()
    {
        return BeaufortScaleResource::collection(BeaufortScale::all());
    }

    /**
     * Store a newly created beaufort scale in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\BeaufortScaleResource
     */
    public function store(Request $request)
    {
        $sighting = BeaufortScale::create($request->all());

        return new BeaufortScaleResource($sighting);
    }

    /**
     * Display the specified beaufort scale.
     *
     * @param  App\BeaufortScale
     * @return App\Http\Resources\BeaufortScaleResource
     */
    public function show(BeaufortScale $beaufortScale)
    {
        return new BeaufortScaleResource($beaufortScale);
    }

    /**
     * Update the specified beaufort scale in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\BeaufortScale
     * @return App\Http\Resources\BeaufortScaleResource
     */
    public function update(Request $request, BeaufortScale $beaufortScale)
    {
        $beaufortScale->update($request->all());

        return new BeaufortScaleResource($beaufortScale);
    }

    /**
     * Remove the specified beaufort scale from storage.
     *
     * @param  App\BeaufortScale
     * @return \Illuminate\Http\Response
     */
    public function destroy(BeaufortScale $beaufortScale)
    {
        $beaufortScale->delete();

        return response()->json(null, 204);
    }
}
