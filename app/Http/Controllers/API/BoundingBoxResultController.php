<?php

namespace App\Http\Controllers\API;

use App\BoundingBoxCoordinate;
use App\Http\Controllers\Controller;

use App\BoundingBoxResult;
use App\Http\QueryFilters\BoundingBoxResultFilters;
use App\Http\Requests\BoundingBoxResultRequest;
use App\Http\Resources\BoundingBoxResultResource;
use Illuminate\Http\Request;

class BoundingBoxResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = BoundingBoxResultFilters::hydrate($request->query());
        return BoundingBoxResultResource::collection(BoundingBoxResult::filterBy($filters)->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BoundingBoxResultRequest $request)
    {
        $validator = $request->validated();
        //Helper::printToConsole($validator);
        $validator['file'] = BoundingBoxResult::createFile($validator);

        $result = BoundingBoxResult::create($validator);
        foreach ($validator['classifications'] as $key => $classification) {

            $coordinates = explode(",", $classification);
            BoundingBoxCoordinate::create([
                'result_id' => $result->id,
                'class_id' => $coordinates[4],
                'x_min' => $coordinates[0],
                'y_min' => $coordinates[1],
                'x_max' => $coordinates[2],
                'y_max' => $coordinates[3],
            ]);
        }


        return new $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BoundingBoxResult  $boundingBoxResult
     * @return \Illuminate\Http\Response
     */
    public function show(BoundingBoxResult $boundingBoxResult)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BoundingBoxResult  $boundingBoxResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BoundingBoxResult $boundingBoxResult)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BoundingBoxResult  $boundingBoxResult
     * @return \Illuminate\Http\Response
     */
    public function destroy($boundingBoxResult)
    {
        BoundingBoxResult::find($boundingBoxResult)->delete();

        return response()->json(null, 204);
    }
}
