<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\StopwatchCategoryResource;
use App\StopwatchCategory;
use Illuminate\Http\Request;

class StopwatchCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return StopwatchCategoryResource::collection(StopwatchCategory::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StopwatchCategory  $stopwatchCategory
     * @return \Illuminate\Http\Response
     */
    public function show(StopwatchCategory $stopwatchCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StopwatchCategory  $stopwatchCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(StopwatchCategory $stopwatchCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StopwatchCategory  $stopwatchCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StopwatchCategory $stopwatchCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StopwatchCategory  $stopwatchCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(StopwatchCategory $stopwatchCategory)
    {
        //
    }
}
