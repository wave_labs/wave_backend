<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\VehicleTypeResource;
use App\VehicleType;
use Illuminate\Http\Request;

class VehicleTypeController extends Controller
{
    public function selector(Request $request)
    {
        return VehicleTypeResource::collection(VehicleType::has('vehicles')->get());
    }
}
