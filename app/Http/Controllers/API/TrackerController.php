<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Tracker;
use App\Http\Resources\TrackerResource;
use App\Http\Controllers\Controller;
use App\Http\QueryFilters\TrackerFilters;
use Illuminate\Support\Facades\File;
use DateTime;
use Carbon\Carbon;

function fixJSON($json)
{
    $regex = <<<'REGEX'
~
    "[^"\\]*(?:\\.|[^"\\]*)*"
    (*SKIP)(*F)
  | '([^'\\]*(?:\\.|[^'\\]*)*)'
~x
REGEX;

    return preg_replace_callback($regex, function ($matches) {
        return '"' . preg_replace('~\\\\.(*SKIP)(*F)|"~', '\\"', $matches[1]) . '"';
    }, $json);
}

class TrackerController extends Controller
{

    public function index(Request $request)
    {
        $filters = TrackerFilters::hydrate($request->query());
        //return TrackerResource::collection(Tracker::latest()->get());
        return TrackerResource::collection(Tracker::filterBy($filters)->latest()->paginate(20));
    }

    public function show(Tracker $AiMegafauna)
    {
        return new TrackerResource($AiMegafauna);
    }

    public function store(Request $request)
    {
        $path = Tracker::storeImage($request->predictPic);
        $path = substr($path, 1);

        $output = shell_exec("python3 " . public_path('python/image_detection.py') . " --modeldir=" . public_path('python/models/OceanusNet_100k') . " --image=" . $path);
        //$output = shell_exec("python3 python/image_detection.py --modeldir=models/OceanusNet_100k --image=" . $path);
        $output = fixJSON($output); //Regex ' to " 
        $data = json_decode($output, true);

        if ($data != []) {
            $newPredict = new Tracker();
            $filename = substr($path, 15, -4);
            $newPredict->path = $filename;
            $newPredict->specie = $data[0]['label'];
            $avgAcc = 0;
            foreach ($data as $value) {
                $avgAcc += $value['accuracy'];
            }
            $avgAcc = $avgAcc / sizeof($data);
            $newPredict->predict = $avgAcc;
            $newPredict->date = date("Y-m-d", time());
            $newPredict->save();
            return new TrackerResource($newPredict);
        } else {
            File::delete($path);


            return [
                'path' => 'path',
                'specie' => 'specie',
                'predict' => '0.0',
                'id' => '-1',
            ];
        }
    }

    public function deleteDetection(Request $request)
    {
        $filename = $request['filepath'];
        $path = public_path() . "/images/tracker/" . $filename . ".png";
        $detection = Tracker::where('id', $request['id']);
        if ($detection) {
            $detection->delete();
            File::delete($path);
        }

        return response()->json(null, 204);
    }

    public function filterGraph($type)
    {
        switch ($type) {
            case 'specie':
                $odo = Tracker::where('specie', 'Odontoceti')->get();
                $mys = Tracker::where('specie', 'Mysticeti')->get();
                $pin = Tracker::where('specie', 'Pinniped')->get();
                $seaB = Tracker::where('specie', 'Bird')->get();
                $seaT = Tracker::where('specie', 'Turtle')->get();
                //Averages = SUM / #
                $avgOdo = 0;
                foreach ($odo as $item) {
                    $avgOdo += $item->predict;
                }
                $avgMys = 0;
                foreach ($mys as $item) {
                    $avgMys += $item->predict;
                }
                $avgPin = 0;
                foreach ($pin as $item) {
                    $avgPin += $item->predict;
                }
                $avgSeaB = 0;
                foreach ($seaB as $item) {
                    $avgSeaB += $item->predict;
                }
                $avgSeaT = 0;
                foreach ($seaT as $item) {
                    $avgSeaT += $item->predict;
                }
                return [
                    "Odontoceti" => round($avgOdo / max(sizeof($odo), 1), 2),
                    "Mysticeti" => round($avgMys / max(sizeof($mys), 1), 2),
                    "Pinniped" => round($avgPin / max(sizeof($pin), 1), 2),
                    "Bird" => round($avgSeaB / max(sizeof($seaB), 1), 2),
                    "Turtle" => round($avgSeaT / max(sizeof($seaT), 1), 2),
                ];
                break;
            case 'time':
                $tracks = Tracker::select('id', 'created_at')
                    ->get()
                    ->groupBy(function ($date) {
                        return Carbon::parse($date->created_at)->format('m'); // grouping by months
                    });

                $trackCount = [];
                $trackArr = [];

                foreach ($tracks as $key => $value) {
                    $trackCount[(int) $key] = count($value);
                }

                $currentMonth = Carbon::now()->month;

                for ($i = $currentMonth; $i >= $currentMonth - 5; $i--) {
                    if (!empty($trackCount[$i])) {
                        $trackArr[$i] = $trackCount[$i];
                    } else {
                        $trackArr[$i] = 0;
                    }
                }

                return $trackArr;

                break;
            default:
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tracker  $tracker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tracker $AiMegafauna)
    {
        $AiMegafauna->delete();
        File::delete(public_path() . "/images/tracker/" .  $AiMegafauna->path . ".png");
        return response()->json(null, 204);
    }
}
