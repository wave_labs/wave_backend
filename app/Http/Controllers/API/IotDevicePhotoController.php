<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as IMG;
use App\IotDevicePhoto;

class IotDevicePhotoController extends Controller
{
    public function index(Request $request)
    {
        $urls = IotDevicePhoto::where('id_device', $request->query()['id'])->get();
        $count = count($urls);

        return response()->json(
            [
                'success' => true,
                'data' => $urls,
                'metadata' => $count,
            ],
            200
        );
    }

    public function show($index) //show a specific photo
    {
        $storagePath = storage_path('/uploaded/iot_device_photo/' . $index);

        return IMG::make($storagePath)->response();
    }
}
