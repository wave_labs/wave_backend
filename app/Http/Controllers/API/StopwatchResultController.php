<?php

namespace App\Http\Controllers\API;

use App\Exports\StopwatchResultExport;
use App\Http\Controllers\Controller;
use App\Http\QueryFilters\StopwatchResultFilters;
use App\Http\Requests\StopwatchResultRequest;
use App\Http\Resources\StopwatchResultResource;
use App\StopwatchResult;
use App\StopwatchTimestamp;
use Illuminate\Http\Request;

class StopwatchResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = StopwatchResultFilters::hydrate($request->query());
        return StopwatchResultResource::collection(StopwatchResult::filterBy($filters)->paginate(10));
    }

    public function csvExport(Request $request)
    {
        return (new StopwatchResultExport($request))->download('export.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StopwatchResultRequest $request)
    {
        $validator = $request->validated();

        $result = StopwatchResult::create($validator);

        foreach ($validator['timestamps'] as $timestamp) {
            $pieces = explode(",", $timestamp);

            StopwatchTimestamp::create([
                'result_id' => $result->id,
                'class_id' => $pieces[0],
                'start_time' => $pieces[1],
                'end_time' => $pieces[2],
            ]);
        }
        return new StopwatchResultResource($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StopwatchResult  $stopwatchResult
     * @return \Illuminate\Http\Response
     */
    public function show($stopwatchResult)
    {
        return new StopwatchResultResource(StopwatchResult::find($stopwatchResult));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StopwatchResult  $stopwatchResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StopwatchResult $stopwatchResult)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StopwatchResult  $stopwatchResult
     * @return \Illuminate\Http\Response
     */
    public function destroy($stopwatchResult)
    {
        StopwatchResult::find($stopwatchResult)->delete();

        return response()->json(null, 204);
    }
}
