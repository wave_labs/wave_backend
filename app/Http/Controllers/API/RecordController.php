<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use function App\getUniqueString;

class RecordController extends Controller
{
    public function ardomeImage(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'image_id' => 'required|integer',
            'datetime' => 'required|date',
        ]);

        if ($validation->fails()) {
            return response()->json([
                'errors' => $validation->errors()
            ], 422);
        } else {
            Record::create([
                'reference' => "ar_dome",
                'reference_id' => $request->image_id,
                'datetime' => $request->datetime,
            ]);

            return response()->json([
                'uniqid' => getUniqueString(20)
            ], 201);
        }
    }

    public function increment(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'reference' => 'required',
            'datetime' => 'nullable|date',
            'reference_id' => 'nullable|integer',
        ]);

        if ($validation->fails()) {
            return response()->json([
                'errors' => $validation->errors()
            ], 422);
        } else {
            $counter =  Record::where('reference', $request->reference)->count();

            Record::create([
                'reference' => $request->reference,
                'reference_id' => $request->reference_id,
                'datetime' => $request->datetime,
            ]);

            return response()->json([
                'counter' => $counter + 1
            ], 201);
        }
    }
}
