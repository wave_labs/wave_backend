<?php

namespace App\Http\Controllers\API;

use App\CopernicusProduct;
use App\Http\Controllers\Controller;
use App\Http\Resources\CopernicusProductResource;
use Illuminate\Http\Request;

class CopernicusProductController extends Controller
{
    //
    public function index(Request $request)
    {
        return CopernicusProductResource::collection(CopernicusProduct::all());
    }
}
