<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;

use App\Http\Resources\WhatsNewResource;

use App\WhatsNew;
use App\WhatsNewType;

class WhatsNewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return WhatsNewResource::collection(WhatsNew::latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required',
        ]);

        if ($validation->fails()) {
            return response()->json([
                'success' => false,
                'status' => 'Required fields missing',
                'errors' => $validation->errors()
            ], 422);
        }

        $request->event = explode(",", $request->event);
        $request->research = explode(",", $request->research);
        $request->update = explode(",", $request->update);

        if ($request->event[0] || $request->research[0] || $request->update[0]) {

            $path = WhatsNew::storeImage($request->file('image'));

            $update = new Request([
                'title' => $request->title,
                'description' => $request->description,
                'image' => $path,
            ]);

            DB::beginTransaction();
            $updateInsert = WhatsNew::create($update->toArray());

            if ($request->event[0]) {
                $nmr_events = count($request->event);
                $type = WhatsNewType::where('type', 'event')->value('id');
                WhatsNew::storePivot($nmr_events, $updateInsert, $request->event, $type);
            }

            if ($request->research[0]) {
                $nmr_researchs = count($request->research);
                $type = WhatsNewType::where('type', 'research')->value('id');
                WhatsNew::storePivot($nmr_researchs, $updateInsert, $request->research, $type);
            }

            if ($request->update[0]) {
                $nmr_updates = count($request->update);
                $type = WhatsNewType::where('type', 'update')->value('id');
                WhatsNew::storePivot($nmr_updates, $updateInsert, $request->update, $type);
            }

            DB::commit();

            return new WhatsNewResource($updateInsert);
        }


        return response()->json([
            'success' => false,
            'message' => 'At least an update, event or research is needed'
        ], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WhatsNew  $whatsNew
     * @return \Illuminate\Http\Response
     */
    public function show(WhatsNew $whatsNew)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WhatsNew  $whatsNew
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WhatsNew $whatsNew)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WhatsNew  $whatsNew
     * @return \Illuminate\Http\Response
     */
    public function destroy(WhatsNew $whatsNew)
    {
        //
    }
}
