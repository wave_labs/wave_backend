<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Deserta;
use App\Http\Requests\DesertaRequest;
use App\Http\Resources\DesertaResource;
use Illuminate\Http\Request;

class DesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DesertaResource::collection(Deserta::latest()->paginate(10));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DesertaRequest $request)
    {
        $validator = $request->validated();

        $filename = uniqid();

        $request->file('img_airport')->storeAs('/datasets/desertas/airport/', $filename . ".png", 'wave-server');
        $request->file('img_sky')->storeAs('/datasets/desertas/sky/', $filename . ".jpg", 'wave-server');

        $record = Deserta::create([
            "img_airport" => '/datasets/desertas/airport/', $filename . ".png",
            "img_sky" => '/datasets/desertas/sky/', $filename . ".jpg",
            'x_acc' => $validator["x_acc"],
            'y_acc' => $validator["y_acc"],
            'z_acc' => $validator["z_acc"],
            'x_gyro' => $validator["x_gyro"],
            'y_gyro' => $validator["y_gyro"],
            'z_gyro' => $validator["z_gyro"],
            'temperature' => $validator["temperature"],
            'humidity' => $validator["humidity"],
            'timestamp' => $validator["timestamp"],

        ]);

        return new DesertaResource($record);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deserta  $deserta
     * @return \Illuminate\Http\Response
     */
    public function show(Deserta $deserta)
    {
        return new DesertaResource($deserta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deserta  $deserta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deserta $deserta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Deserta  $deserta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deserta $deserta)
    {
        //
    }
}
