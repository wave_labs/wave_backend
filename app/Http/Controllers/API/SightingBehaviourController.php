<?php


namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\SightingBehaviour;
use App\Http\Resources\SightingBehaviourResource;

class SightingBehaviourController extends Controller
{
    /**
     * @return App\Http\Resources\SightingBehaviourResource
     */
    public function selector()
    {
        return SightingBehaviourResource::collection(SightingBehaviour::all());
    }   
}
