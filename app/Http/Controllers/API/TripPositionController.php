<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\TripPositionResource;
use Illuminate\Support\Facades\Validator;

use App\TripPosition;
use Illuminate\Http\Request;
use DB;

class TripPositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return TripPositionResource::collection(TripPosition::where('trip_id', $request->trip)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'trip_id' => 'required|integer|exists:trips,id',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $data = json_decode($request->data, true);
        $positions = count($data);

        if ($positions <= 0) {
            return response()->json([
                'success' => false,
                'message' => 'At least one position is required'
            ], 401);
        }

        DB::beginTransaction();
        for ($i = 0; $i < $positions; $i++) {
            $position = $data[$i];

            if (!is_null($position)) {
                TripPosition::create([
                    'trip_id' => $request->trip_id,
                    'latitude' => $position['latitude'],
                    'longitude' => $position['longitude'],
                ]);
            }
        }
        DB::commit();

        return response()->json([
            'success' => true,
            'message' => 'Trip successfully ended',
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TripPosition  $tripPosition
     * @return \Illuminate\Http\Response
     */
    public function show(TripPosition $tripPosition)
    {
        return new TripPositionResource($tripPosition);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TripPosition  $tripPosition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TripPosition $tripPosition)
    {
        $tripPosition->update($request->all());

        return new ReportResource($tripPosition);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TripPosition  $tripPosition
     * @return \Illuminate\Http\Response
     */
    public function destroy(TripPosition $tripPosition)
    {
        $tripPosition->delete();

        return response()->json(null, 204);
    }
}
