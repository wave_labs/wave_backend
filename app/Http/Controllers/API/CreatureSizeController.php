<?php

namespace App\Http\Controllers\API;

use App\Creature;
use App\Http\Controllers\Controller;
use App\CreatureSize;

class CreatureSizeController extends Controller
{
    public function __invoke(Creature $creature)
    {
        return CreatureSize::whereCreatureId($creature->id)->get();
    }
}
