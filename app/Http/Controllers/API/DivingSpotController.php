<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use App\DivingSpot;
use App\DivingSpotSubstract;
use App\Http\QueryFilters\DivingSpotFilters;
use App\Http\Requests\StoreDivingSpotRequest;
use App\Http\Resources\DivingSpotResource;
use App\Http\Resources\DivingSpotDataResource;
use App\UserHasDivingSpot;
use Illuminate\Support\Facades\DB;
use Auth;
use Error;
use Google\Service\CloudSearch\UserId;

class DivingSpotController extends Controller
{


    function __construct()
    {
        $this->middleware('permission:dive-list', ['only' => ['index, show']]);
        //$this->middleware('permission:dive-create', ['only' => ['store']]);
        $this->middleware('permission:dive-update', ['only' => ['update']]);
        $this->middleware('permission:dive-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return App\Http\Resources\DivingSpotResource
     */
    public function index(DivingSpotFilters $filters)
    {
        return DivingSpotResource::collection(DivingSpot::filterBy($filters)->where('old', '=', '0')->orderBy('name')->paginate(10));
    }

    /**
     * Display the specified diving spot with statistics data.
     *
     * @param  App\DivingSpot
     * @return App\Http\Resources\DivingSpotDataResource
     */
    public function data(DivingSpot $divingSpot)
    {
        return new DivingSpotDataResource($divingSpot);
    }

    /**
     * Display a listing of all diving spots.
     *
     * @return App\Http\Resources\DivingSpotResource
     */
    public function selector(Request $request)
    {
        $filters = DivingSpotFilters::hydrate($request->query());
        return DivingSpotResource::collection(DivingSpot::filterBy($filters)->where('old', '=', '0')->orderBy('name')->get());
    }

    /* Display a listing of all diving spots tied to a specific user */
    public function selectorUser(Request $request, $userID)
    {
        $filters = DivingSpotFilters::hydrate($request->query());
        $user = DB::table('user_has_diving_spots')->select('diving_spot_id')->where('user_id', $userID)->get()->pluck('diving_spot_id');

        return DivingSpotResource::collection(DivingSpot::whereIn('id', $user)->where('validated', '=', 1)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\DivingSpotResource
     */
    public function store(StoreDivingSpotRequest $request)
    {
        $validator = $request->validated();

        $divingSpot = DivingSpot::create($validator);

        $divingSpot->substracts()->sync($validator['substract']);

        if (!$validator['validated']) {
            UserHasDivingSpot::create([
                'user_id' => $validator['user_id'],
                'diving_spot_id' => $divingSpot->id
            ]);
        }

        return new DivingSpotResource($divingSpot);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\DivingSpot
     * @return App\Http\Resources\DivingSpot
     */
    public function show(DivingSpot $divingSpot)
    {
        return new DivingSpotResource($divingSpot);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DivingSpot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DivingSpot $divingSpot)
    {
        $divingSpot->update($request->all());

        return new DivingSpotResource($divingSpot);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\DivingSpot
     * @return \Illuminate\Http\Response
     */
    public function destroy(DivingSpot $divingSpot)
    {
        $divingSpot->delete();

        return response()->json([
            'message' => 'Successfully deleted task!',
        ], 200);
    }
}
