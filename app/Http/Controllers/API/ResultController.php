<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Result;
use App\Feedback;
use App\Question;
use Validator, DB;
use App\Http\Resources\ResultResource;
use App\Http\Resources\FeedbackResource;
use App\Http\Requests\StoreFormResultRequest;


use App\Exports\ResultExport;


class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        return ResultResource::collection(Result::where('feedback_id', $id)->paginate(10));
    }

    /**
     * Export results resource form a specific date.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeExcel($Idate, $Fdate)
    {
        return (new ResultExport($Idate, $Fdate))->download('invoices.csv', \Maatwebsite\Excel\Excel::CSV);

        //Excel::store(new ResultExport($Idate, $Fdate), $Idate.'_'.$Fdate.'.csv', 'export', \Maatwebsite\Excel\Excel::CSV);

        //Excel::download(new ResultExport($Idate, $Fdate), 'invoices.csv', \Maatwebsite\Excel\Excel::CSV);

        return response()->json([
            'success' => true,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFormResultRequest $request)
    {
        $validator = $request->validated();

        DB::beginTransaction();

        $nmr_questions = Question::countQuestions($validator['form_id']);
        $validator['score'] = Feedback::getAverageScore($validator['form_id'], $validator['ratings']);
        $form = Feedback::create($validator);

        for ($i = 0; $i <= $nmr_questions - 1; $i++) {
            $data = [
                'feedback_id' => $form->id,
                'question_id' => $validator['questions'][$i],
                'rating' => $validator['ratings'][$i],
            ];

            Result::firstOrCreate($data);
        }

        DB::commit();

        return new FeedbackResource($form);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Result  $feedbackResult
     * @return \Illuminate\Http\Response
     */
    public function show(Result $feedbackResult)
    {
        return new FeedbackResultResource($feedbackResult);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Result  $feedbackResult
     * @return \Illuminate\Http\Response
     */
    public function edit(Result $feedbackResult)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Result  $feedbackResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Result $feedbackResult)
    {
        $feedbackResult->update($request->all());

        return new ResultResource($feedbackResult);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Result  $feedbackResult
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $feedbackResult)
    {
        $feedbackResult->delete();

        return response()->json(
            [
                'success' => true,
            ],
            200
        );
    }
}
