<?php

namespace App\Http\Controllers\API;

use App\DomeHasInteraction;
use App\Http\Controllers\Controller;
use App\Record;
use Illuminate\Http\Request;

class DomeHasInteractionController extends Controller
{

    public function increment(Request $request)
    {
        $record = DomeHasInteraction::findFromParams($request->dome, $request->interaction)->first();

        Record::create([
            'reference' => "dome_has_interactions",
            'reference_id' => $record->id
        ]);

        return DomeHasInteraction::incrementCounter($request->dome, $request->interaction);
    }

    public function index(Request $request)
    {
        return  DomeHasInteraction::findFromParams($request->dome, $request->interaction)->first();
    }
}
