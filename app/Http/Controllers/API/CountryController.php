<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Country;

class CountryController extends Controller
{
    // ==============
    // READ COUNTRIES

    public function index()
    {
    	$countriesSQL = Country::orderby("active", "desc")->orderby("name")->get();
        //return json_decode($countriesSQL);
        return response()->json(
        [
            'countries' => $countriesSQL,
        ], 200);
    }


    // ==============
    // CREATE COUNTRY

    public function create(Request $request) {

        //Validate the request, all is required, language code is max 2 chars long
        $this->validate($request, [
            'code' => 'required|alpha|size:2|unique:country',
            'name' => 'required'
        ]);

        //Create the language and store in DB
        $country = new Country; 
        $country->name = $request->name;
        $country->code = $request->code;
        $country->active = 0;
        $country->save();

        // Crate a log
        AdminLogsController::store("Created a Country: " . $request->name);

        //Return back to the same page
        return back();
    }


    // ===================
    // UPDATE COUNTRY NAME

    public function updateName(Request $request, Country $country) {
		
		$this->validate($request, [
            'name' => 'required',
        ]);
    	
        $country->update($request->all());

        // Create a log
        AdminLogsController::store("Updated Country name: " . $request->name);

        return "OK";
    }


    // ===================
    // UPDATE COUNTRY CODE

    public function updateCode(Request $request, Country $country) {
        
        $country->update($request->all());

        // Create a log
        AdminLogsController::store("Updated Country code for " . $country->name . " to " . $country->code );

        return "OK";
    }

    // ================
    // UPDATE CONTINENT

    public function updateContinent(Request $request, Country $country) {
        
        $country->update($request->all());

        // Create a log
        AdminLogsController::store("Updated Continent " . $country->continent );

        return "OK";
    }


    // ==============================
    // UPDATE ACTIVE STATE OF COUNTRY

    public function updateActive(Request $request, Country $country) {
        
        //return $country->id;
        $country->update($request->all());

        // Create a log
        AdminLogsController::store("Updated Country active state: " . $country->name . " to " . $country->active);

        return "OK";
    }




    // ==============
    // DELETE COUNTRY

    public function delete(Request $request, Country $country) {

        $name = $country->name;

        // Delete the country
        $country->delete();

        // Create a log
        AdminLogsController::store("Deleted a Country: " .  $name);
        
        // Return to the previous page
        return "OK";
    }

}