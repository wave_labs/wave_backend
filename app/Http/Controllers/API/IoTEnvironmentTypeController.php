<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\IotEnvironmentType;
use App\Abundance;
use App\Http\Resources\IotEnvironmentTypeResource;
//use App\Http\Resources\IotEnvironmentTypeDataResource;
use App\Http\QueryFilters\IotEnvironmentTypeFilters;
use App\Http\Resources\AbundanceResource;
use Storage;

class IoTEnvironmentTypeController extends Controller
{

    function __construct()
    {
        /*$this->middleware('permission:iotDeviceType-list', ['only' => ['index, show']]);
        $this->middleware('permission:iotDeviceType-create', ['only' => ['store']]);
        $this->middleware('permission:iotDeviceType-update', ['only' => ['update']]);
        $this->middleware('permission:iotDeviceType-delete', ['only' => ['destroy']]);*/ }

    /**
     * Display a listing of the iotDeviceType for selection.
     *
     * @return App\Http\Resources\IotEnvironmentTypeResource
     */
    public function selector(Request $request)
    {
        $filters = IotEnvironmentTypeFilters::hydrate($request->query());
        return IotEnvironmentTypeResource::collection(IotEnvironmentType::filterBy($filters)->get());
    }
}
