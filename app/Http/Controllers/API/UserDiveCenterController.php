<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserDiveCenterResource;
use App\UserDiveCenter;
use Illuminate\Http\Request;

class UserDiveCenterController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user-list', ['only' => ['index']]);
        $this->middleware('permission:user-create', ['only' => ['store']]);
        $this->middleware('permission:user-update', ['only' => ['update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return UserDiveCenterResource::collection(UserDiveCenter::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\Response
     */
    public function store(Request $request)
    {
        $userDiveCenter = UserDiveCenter::create($request->all());

        return new UserDiveCenterResource($userDiveCenter);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserDiveCenter  $userDiveCenter
     * @return \Illuminate\Http\UserDiveCenterResource
     */
    public function show(UserDiveCenter $userDiveCenter)
    {
        return new UserDiveCenterResource($userDiveCenter);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user_dive_center  $user_dive_center
     * @return \Illuminate\Http\Response
     */
    public function edit(UserDiveCenter $user_dive_center)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserDiveCenter 
     * @return App\Http\Resources\UserDiveCenterResource
     */
    public function update(Request $request, UserDiveCenter $userDiveCenter)
    {
        $userDiveCenter->update($request->all());
        return new UserDiveCenterResource($userDiveCenter);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserDiveCenter  $user_dive_center
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDiveCenter $user_dive_center)
    {
        $user_dive_center->delete();
        $user_dive_center->user->delete();

        return response()->json(null, 204);
    }
}
