<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use App\PendingPoint;
use App\Http\Resources\UserResource;
use App\Http\QueryFilters\UserFilters;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Mail\PasswordReset;
use Intervention\Image\Facades\Image as IMG;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
use JWTAuth;

class UserController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:user-list', ['only' => ['index']]);
        $this->middleware('permission:user-create', ['only' => ['store']]);
        $this->middleware('permission:user-update', ['only' => ['update']]);
        // $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the user.
     *
     * @return App\Http\Resources\UserResource
     */
    public function index(Request $request)
    {
        $filters = UserFilters::hydrate($request->query());
        return UserResource::collection(User::filterBy($filters)->paginate(10));
    }

    /**
     * Display a listing of all user.
     *
     * @return App\Http\Resources\UserResource
     */
    public function selector(Request $request)
    {
        $filters = UserFilters::hydrate($request->query());
        return UserResource::collection(User::filterBy($filters)->get());
    }


    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\UserResource
     */
    public function store(StoreUserRequest $request)
    {
        $validator = $request->validated();

        DB::beginTransaction();

        $validator['password'] = bcrypt($validator['password']);

        $user = User::create($validator);
        $user->syncRoles([$validator['role']]);

        DB::commit();

        return new UserResource($user);
    }

    /**
     * Display the specified user.
     *
     * @param  App\User
     * @return App\Http\Resources\UserResource
     */
    public function show(User $user)
    {
        if (Auth::id() == $user->id || Auth::user()->can("user-list")) {
            return new UserResource($user);
        }
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\User
     * @return App\Http\Resources\UserResource
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $validator = $request->validated();

        $user->update($validator);
        $user->syncRoles($validator['role']);

        if ($request->image) {
            $path = self::savePhotoLocally($request->image);
            $request['imagePath'] = $path;
            $user->photo = $request['imagePath'];
        }

        return new UserResource($user);
    }

    public function changePicture(Request $request, User $user)
    {
        if ($request->image) {
            $path = self::savePhotoLocally($request->image);
            $request['imagePath'] = $path;
            $user->photo = $request['imagePath'];
            $user->save();
        }

        return new UserResource($user);
    }

    public static function savePhotoLocally($photo)
    {
        $filename = uniqid() . '.png';
        IMG::make($photo)->encode('png', 70)->resize(760, null, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        })->save(public_path('/storage/uploaded/photo/profilePicture/' . $filename));

        return ($filename);
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  App\User
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $userCreatingRequest = JWTAuth::setToken($request->header('Authorization'))->user();
        if ($userCreatingRequest) {
            if ($userCreatingRequest->hasRole('admin') || $userCreatingRequest->id == $user->id) {
                $userType = User::getUserType($user);

                $user->delete();
                $userType->delete();

                return response()->json(["message" => "Account deleted"], 200);
            } else {
                return response()->json(["message" => "You don't have permissions"], 203);
            }
        } else {
            return response()->json(["message" => "Invalid token"], 422);
        }
    }

    /**
     * Validate pending points
     *
     * @param  App\PendingPoint
     * @return \Illuminate\Http\Response
     */
    public function validation($pending_points_id)
    {
        $pending_points = PendingPoint::query()->find($pending_points_id);

        $points = PendingPoint::approval($pending_points_id);

        //User::updatePoints($points, $pending_points->user_id);

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Send Reset Password Email
     *
     * @param  App\PendingPoint
     * @return \Illuminate\Http\Response
     */
    public function sendResetPasswordEmail($id)
    {
        $user = User::findOrFail($id);

        Mail::to($user->email)->send(new PasswordReset($user));

        return response()->json([
            'success' => true, 'data' => ['message' => 'A reset email has been sent! Please check your email.']
        ]);
    }
}
