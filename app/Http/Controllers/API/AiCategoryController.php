<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\AiCategory;
use App\Http\Resources\AiCategoryResource;
use Illuminate\Http\Request;

class AiCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AiCategoryResource::collection(AiCategory::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AiCategory  $aiCategory
     * @return \Illuminate\Http\Response
     */
    public function show(AiCategory $aiCategory)
    {
        return new AiCategoryResource($aiCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AiCategory  $aiCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AiCategory $aiCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AiCategory  $aiCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AiCategory $aiCategory)
    {
        //
    }
}
