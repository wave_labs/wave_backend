<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\DivingSpotSubstract;
use App\Http\Resources\DivingSpotSubstractResource;
use Illuminate\Http\Request;

class DivingSpotSubstractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    //create selector
    public function selector(Request $request)
    {
        return DivingSpotSubstractResource::collection(DivingSpotSubstract::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DivingSpotSubstract  $divingSpotSubstract
     * @return \Illuminate\Http\Response
     */
    public function show(DivingSpotSubstract $divingSpotSubstract)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DivingSpotSubstract  $divingSpotSubstract
     * @return \Illuminate\Http\Response
     */
    public function edit(DivingSpotSubstract $divingSpotSubstract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DivingSpotSubstract  $divingSpotSubstract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DivingSpotSubstract $divingSpotSubstract)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DivingSpotSubstract  $divingSpotSubstract
     * @return \Illuminate\Http\Response
     */
    public function destroy(DivingSpotSubstract $divingSpotSubstract)
    {
        //
    }
}
