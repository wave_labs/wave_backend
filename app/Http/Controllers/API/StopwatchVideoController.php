<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\QueryFilters\StopwatchVideoFilters;
use App\Http\Resources\StopwatchCategoryResource;
use App\Http\Resources\StopwatchVideoResource;
use App\StopwatchCategory;
use App\StopwatchVideo;
use Illuminate\Http\Request;

class StopwatchVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = StopwatchVideoFilters::hydrate($request->query());
        return StopwatchVideoResource::collection(StopwatchVideo::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function next(StopwatchVideo $stopwatchVideo, Request $request)
    {
        $filters = StopwatchVideoFilters::hydrate($request->query());
        $query = StopwatchVideo::filterBy($filters)->where('id', '>', $stopwatchVideo->id)->orderBy('id');
        return count($query->get()) ? new StopwatchVideoResource($query->first()) : response()->json(null, 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StopwatchVideo  $stopwatchVideo
     * @return \Illuminate\Http\Response
     */
    public function show(StopwatchVideo $stopwatchVideo)
    {
        return new StopwatchVideoResource($stopwatchVideo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StopwatchVideo  $stopwatchVideo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StopwatchVideo $stopwatchVideo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StopwatchVideo  $stopwatchVideo
     * @return \Illuminate\Http\Response
     */
    public function destroy(StopwatchVideo $stopwatchVideo)
    {
        //
    }
}
