<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use App\Abundance;
use Illuminate\Http\Request;
use App\Http\QueryFilters\AbundanceFilters;
use App\Http\Resources\AbundanceResource;
use Illuminate\Support\Facades\Validator;
use App\Creature;

class AbundanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = AbundanceFilters::hydrate($request->query());
        return AbundanceResource::collection(Abundance::paginate(10)); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {/*
        $validation = Validator::make($request->all(), [
            'creature_id' => 'required',
            'level_1' => 'required',
            'level_2' => 'required',
            'level_3' => 'required',
            'level_4' => 'required',
            ]);
     
            if ($validation->fails())
            {
                return response()->json([
                    'status' => 'Required fields missing',
                    'errors' => $validation->errors()
                ], 422);
            }
    
            $abundance = new Request([
                'creature_id' => $request->creature_id,
                'level_1' => $request->level_1,
                'level_2' => $request->level_2,
                'level_3' => $request->level_3,
                'level_4' => $request->level_4,
            ]); 
            $abundanceInsert = Dive::create($abundance->toArray());
    
            
                  
            return response()->json([
                'status' => 'success',
                'abundance' => $abundance                
        ], 200);*/
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Abundance  $abundance
     * @return \Illuminate\Http\Response
     */
    public function show(Abundance $abundance)
    {
        return new AbundanceResource ($abundance);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Abundance  $abbundance
     * @return \Illuminate\Http\Response
     */
    public function edit(Abundance $abundance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Abundance  $abbundance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Abundance $abundance)
    {
        $abundance->update($request->all());
        return new AbundanceResource($abundance);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Abbundance  $abbundance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Abundance $abundance)
    {
        $abundance->delete();

        return response()->json([
            'message' => 'Successfully deleted abundance!'
        ],200);
    }
}
