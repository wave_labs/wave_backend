<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Response;

use App\Audio;

class AudioController extends Controller
{
    public function indexSighting()
    {
        $urls = Audio::where('type', "sighting")->select('file', 'url', 'creature_id')->get();
        $count = count($urls);

        return response()->json(
            [
                'success' => true,
                'data' => $urls,
                'metadata' => $count,
            ],
            200
        );
    }

    public function show($class, $index)
    {
        if ($class == "litter" || $class == "sighting" || $class == "dive") {
            $file = storage_path('/audio/' . $class . '/' . $index . '.wav');
            return Response::download($file);
        }

        return response()->json(
            [
                'success' => false,
                'message' => 'Invalid image path ' . $class,
            ],
            422
        );
    }
}
