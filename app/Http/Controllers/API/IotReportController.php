<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App;

use App\Http\Resources\IotReportResource;
use App\Http\QueryFilters\IotReportFilters;
use App\Http\Requests\StoreIotReportRequest;
use App\Http\Requests\UpdateLitterRequest;

use App\IotReport;
use App\Litter;

use App\Exports\IotReportExport;
use App\Helper;
use App\Http\Resources\IotReportWaveMeasurementsResource;
use App\IotDevice;
use App\Jobs\StoreIotReport;
use stdClass;

class IotReportController extends Controller
{
    function __construct()
    {
        /*
        $this->middleware('permission:litter-list', ['only' => ['index, show']]);
        $this->middleware('permission:litter-create', ['only' => ['store']]);
        $this->middleware('permission:litter-update', ['only' => ['update']]);
        $this->middleware('permission:litter-delete', ['only' => ['destroy']]);*/
        $this->middleware('permission:user-list', ['only' => ['destroyMultiple']]);
    }

    public function latestReport(Request $request)
    {
        $filters = IotReportFilters::hydrate($request->query());

        return new IotReportResource(IotReport::filterBy($filters)->latest()->first());
    }

    public function getWaveMeasurements(Request $request)
    {
        $filters = IotReportFilters::hydrate($request->query());
        return new IotReportWaveMeasurementsResource(IotReport::filterBy($filters)->get(), $request->device);
    }

    public function getFieldsTimeline(Request $request)
    {
        if ($request->fields && $request->device) {
            return response()->json([
                "data" => IotReport::getFieldsTimeline($request->device, $request->fields)
            ]);
        }
    }

    public function generateCustomGraph(Request $request)
    {
        return IotReport::generateCustomGraph($request->device, $request->date, $request->graph);
    }

    /**
     * Display a listing of the litter.
     *
     * @return App\Http\Resources\IotReportResource
     */
    public function index(IotReportFilters $filters)
    {
        $reports = IotReport::filterBy($filters)->latest()->paginate(10);

        return IotReportResource::collection($reports);
    }

    public function csvExport(Request $request)
    {
        return (new IotReportExport($request))->download('iotReport.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function indexAllCoordinatesForLineDraw(Request $request)
    {
        $filters = IotReportFilters::hydrate($request->query());
        $reports = IotReport::filterBy($filters)->latest()->take($request->limit ? $request->limit : 300)->orderBy('collected_at')->get();

        $result = [];

        foreach ($reports as $report) {

            $report_fields = $report->fields;
            $coordinates = new stdClass;
            foreach ($report_fields as $report_field) {
                $features = $report_field->field->features;

                foreach ($features as $feature) {

                    if ($feature->name == 'latitude' || $feature->name == 'longitude') {
                        $feature->name == 'longitude' && Helper::printToConsole($feature);
                        $coordinates->{$feature->name} = (float) $report_field->value;
                    }
                }
            }

            if (property_exists($coordinates, 'latitude') && property_exists($coordinates, 'longitude')) {

                $coordinates->report = $report->id;
                $coordinates->date = $report->collected_at ? $report->collected_at : 'Not specified';
                array_push($result, $coordinates);
            }
        }
        $result = array_values($result);

        return (array) ($result);
    }

    /**
     * Store a newly created litter in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\IotReportResource
     */
    public function store(StoreIotReportRequest $request)
    {
        
        $validator = $request->validated();
        // $device = IotDevice::find(3);
        // return $device->fields;
        StoreIotReport::dispatch($validator)->delay(now()->addSeconds(1));

        return response()->json(['success' => true], 201);
    }

    /**
     * Display the specified litter.
     *
     * @param  App\IotReport
     * @return App\Http\Resources\IotReportResource
     */
    public function show(IotReport $iotReport)
    {
        return new IotReportResource($iotReport);
    }

    public function nextAndPrior($report)
    {
        $report = IotReport::find($report);

        $previous = IotReport::where('id', '<', $report->id)->orderByDesc('id')->first();
        $next = IotReport::where('id', '>', $report->id)->orderBy('id')->first();

        return response()->json([
            'success' => true,
            'previous' => $previous ? new IotReportResource($previous) : null,
            'next' => $next ? new IotReportResource($next) : null,
        ], 200);
    }

    /**
     * Update the specified litter in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Litter
     * @return App\Http\Resources\IotReportResource
     */
    public function update(UpdateLitterRequest $request, Litter $litter)
    {
        //
    }

    /**
     * Remove the specified litter from storage.
     *
     * @param  App\IotReport
     * @return \Illuminate\Http\Response
     */
    public function destroy($report)
    {
        $report = IotReport::find($report);

        //Delete also the reported fields
        $report->fields()->delete();

        $report->delete();

        return response()->json([
            'status' => 'success',
        ], 200);
    }

    public function destroyMultiple(Request $request)
    {
        if (count($request->date) > 1 && $request->device) {
            $filters = IotReportFilters::hydrate($request->query());
            $reports = IotReport::filterBy($filters)->get();

            foreach ($reports as $report) {
                $report->fields()->delete();
                $report->delete();
            }

            return response()->json([
                'status' => 'success',
            ], 200);
        }

        return response()->json([
            'status' => 'false',
            'message' => 'Date range and device are required',
        ], 422);
    }
}
