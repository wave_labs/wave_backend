<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\IotRuleResource;
use App\IotRule;
use Illuminate\Http\Request;

class IotRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function selector(Request $request)
    {
        return IotRuleResource::collection(IotRule::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IotRule  $iotRule
     * @return \Illuminate\Http\Response
     */
    public function show(IotRule $iotRule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IotRule  $iotRule
     * @return \Illuminate\Http\Response
     */
    public function edit(IotRule $iotRule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IotRule  $iotRule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IotRule $iotRule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IotRule  $iotRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(IotRule $iotRule)
    {
        //
    }
}
