<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\BoundingBoxImage;
use App\Http\QueryFilters\BoundingBoxImageFilters;
use App\Http\Resources\BoundingBoxImageResource;
use Illuminate\Http\Request;

class BoundingBoxImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = BoundingBoxImageFilters::hydrate($request->query());

        return new BoundingBoxImageResource(BoundingBoxImage::filterBy($filters)->first());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BoundingBoxImage  $boundingBoxImage
     * @return \Illuminate\Http\Response
     */
    public function show(BoundingBoxImage $boundingBoxImage)
    {
        return $boundingBoxImage;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BoundingBoxImage  $boundingBoxImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BoundingBoxImage $boundingBoxImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BoundingBoxImage  $boundingBoxImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(BoundingBoxImage $boundingBoxImage)
    {
        //
    }
}
