<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Form;
use App\Question;
use Illuminate\Http\Request;
use Validator, DB;
use App\Http\Resources\FormResource;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return FormResource::collection(Form::/*filterBy($filters)->*/paginate(25));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'form_name' => 'required',
            'description' => 'required',
            //'questions' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => 'false',
                'message' => $validator->errors()
            ], 401);
        }

        DB::beginTransaction();

        //CREATE FORM
        $form = new Form;
        $form->form = $request->form_name;
        $form->desc = $request->description;
        $form->save();

        //GET NUMBER OF QUESTIONS AND INITIALIZE ARRAYS
        $nmr_questions = count($request->questions);
        $form_questions = [];

        for ($i = 0; $i < $nmr_questions; $i++) {
            $current_question = $request->questions[$i];

            if (!is_null($current_question)) {
                //INSERT INTO DATABASE
                $data = [
                    'question' => $current_question,
                    'form_id' => $form->id,
                    'scale' => 5,
                ];
                $question = Question::firstOrCreate($data);

                //FILL ARRAY WITH INSERT
                array_push($form_questions, $question);
            }
        }
        DB::commit();

        return response()->json([
            'success' => 'true',
            'form' => $form,
            'questions' => $form_questions,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Form  $feedbackType
     * @return \Illuminate\Http\Response
     */
    public function show(Form $feedbackType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Form  $feedbackType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $feedbackType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Form  $feedbackType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $feedbackType)
    {
        //
    }
}
