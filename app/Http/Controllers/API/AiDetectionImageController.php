<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\AiDetectionImage;
use App\Http\QueryFilters\AiDetectionImageFilters;
use App\Http\Resources\AiDetectionImageResource;
use Illuminate\Http\Request;

class AiDetectionImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = AiDetectionImageFilters::hydrate($request->query());
        return AiDetectionImageResource::collection(AiDetectionImage::filterBy($filters)->latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AiDetectionImage  $aiDetectionImage
     * @return \Illuminate\Http\Response
     */
    public function show(AiDetectionImage $aiDetectionImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AiDetectionImage  $aiDetectionImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AiDetectionImage $aiDetectionImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AiDetectionImage  $aiDetectionImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(AiDetectionImage $aiDetectionImage)
    {
        //
    }
}
