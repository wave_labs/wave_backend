<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\BoundingBoxClass;
use Illuminate\Http\Request;

class BoundingBoxClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BoundingBoxClass  $boundingBoxClass
     * @return \Illuminate\Http\Response
     */
    public function show(BoundingBoxClass $boundingBoxClass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BoundingBoxClass  $boundingBoxClass
     * @return \Illuminate\Http\Response
     */
    public function edit(BoundingBoxClass $boundingBoxClass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BoundingBoxClass  $boundingBoxClass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BoundingBoxClass $boundingBoxClass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BoundingBoxClass  $boundingBoxClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(BoundingBoxClass $boundingBoxClass)
    {
        //
    }
}
