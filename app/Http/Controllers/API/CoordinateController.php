<?php

namespace App\Http\Controllers\API;

use App\Dive;
use App\Http\Controllers\Controller;
use App\Sighting;
use App\Litter;
use App\UserDiveCenter;
use App\DivingSpot;
use App\Http\Resources\DiveCoordResource;
use App\Http\Resources\UserDiveCenterCoordResource;
use App\Http\Resources\DivingSpotCoordResource;
use App\Http\Resources\LitterCoordResource;
use App\Http\Resources\SightingCoordResource;


use Illuminate\Http\Request;

class CoordinateController extends Controller
{
    public function coordinatesAverage()
    {
        $coordinates = Sighting::getAverage();

        if (!$coordinates[0] || !$coordinates[1]) {
            $coordinates[0] = 32.6;
            $coordinates[1] = -16.9;
        }

        return $coordinates;

        return response()->json([
            'success' => true,
            'data' => [
                'latitude' => $coordinates[0],
                'longitude' => $coordinates[1]
            ],
        ], 200);
    }

    public function index(Request $request)
    {

        $litter = LitterCoordResource::collection(Litter::latest()->take(100)->get());
        $sighting = SightingCoordResource::collection(Sighting::latest()->take(100)->get());
        $dive = DiveCoordResource::collection(Dive::latest()->take(100)->get());

        return response()->json([
            'success' => true,
            'data' => [
                'litter' => $request->litter == 'true' ? $litter : null,
                'sighting' =>  $request->sighting == 'true' ? $sighting : null,
                'dive' => $request->dive == 'true' ? $dive : null,
            ]
        ]);
    }
}
