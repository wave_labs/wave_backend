<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use File;

use App\AiDetection;
use App\AiDetectionClassification;
use App\AiDetectionImage;
use App\Helper;
use App\Http\QueryFilters\AiDetectionFilters;
use App\Http\Requests\StoreAiDetectionRequest;
use App\Http\Resources\AiDetectionResource;
use Illuminate\Http\Request;

class AiDetectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = AiDetectionFilters::hydrate($request->query());
        $users = Helper::getUserFromRequest($request);
        return AiDetectionResource::collection(AiDetection::filterBy($filters)->fromUser($users)->latest()->paginate(20));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAiDetectionRequest $request)
    {
        $validator = $request->validated();
        $directory = uniqid();
        File::makeDirectory(public_path('python/images/' . $directory));
        $detection = AiDetection::create([
            'ai_model_id' => $validator['model_id'],
            'user_id' => $validator['user_id'],
        ]);

        AiDetectionImage::saveToDirectory($detection, $request->file('directory'), $directory);
        AiDetectionClassification::createClassifications($detection);

        return new AiDetectionResource($detection);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePreview(StoreAiDetectionRequest $request)
    {
        $validator = $request->validated();

        $detection = AiDetection::create($validator);

        return new AiDetectionResource($detection);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AiDetection  $aiDetection
     * @return \Illuminate\Http\Response
     */
    public function show(AiDetection $aiDetection)
    {
        return new AiDetectionResource($aiDetection);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AiDetection  $aiDetection
     * @return \Illuminate\Http\Response
     */
    public function destroy(AiDetection $aiDetection)
    {
        $image = AiDetectionImage::whereAiDetectionId($aiDetection->id)->first();
        $directory = explode('/', $image->image);
        File::deleteDirectory(public_path('python/images/' . $directory[0]));
        $aiDetection->delete();

        return response()->json(null, 204);
    }
}
