<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\IotTemplateResource;
use App\IotTemplate;
use Illuminate\Http\Request;

class IotTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return IotTemplateResource::collection(IotTemplate::paginate(10));
    }

    public function selector(Request $request)
    {
        return IotTemplateResource::collection(IotTemplate::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IotTemplate  $iotTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(IotTemplate $iotTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IotTemplate  $iotTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(IotTemplate $iotTemplate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IotTemplate  $iotTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IotTemplate $iotTemplate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IotTemplate  $iotTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(IotTemplate $iotTemplate)
    {
        //
    }
}
