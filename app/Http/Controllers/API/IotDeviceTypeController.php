<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\IotDeviceType;
use App\Http\Resources\IotDeviceTypeResource;
use App\Http\QueryFilters\IotDeviceTypeFilters;
use App\Http\Requests\StoreIotDeviceTypeRequest;


class IotDeviceTypeController extends Controller
{
    function __construct()
    {
        /*$this->middleware('permission:iotDeviceType-list', ['only' => ['index, show']]);
        $this->middleware('permission:iotDeviceType-create', ['only' => ['store']]);
        $this->middleware('permission:iotDeviceType-update', ['only' => ['update']]);
        $this->middleware('permission:iotDeviceType-delete', ['only' => ['destroy']]);*/ }

    /**
     * Display a listing of the iotDeviceType.
     *
     * @return App\Http\Resources\IotDeviceTypeResource
     */
    public function index(IotDeviceTypeFilters $filters)
    {
        return IotDeviceTypeResource::collection(IotDeviceType::filterBy($filters)->paginate(10));
    }

    /**
     * Display the specified iotDeviceType with statistics data.
     *
     * @param  App\IotDeviceType
     * @return App\Http\Resources\IotDeviceTypeResource
     */
    /*public function data(IotDeviceType $iotDeviceType)
    {
        return new IotDeviceTypeDataResource($iotDeviceType);
    }*/

    /**
     * Display a listing of the iotDeviceType for selection.
     *
     * @return App\Http\Resources\IotDeviceTypeResource
     */
    public function selector(IotDeviceTypeFilters $filters)
    {
        return IotDeviceTypeResource::collection(IotDeviceType::filterBy($filters)->get());
    }

    /**
     * Store a newly created iotDeviceType in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\IotDeviceTypeResource
     */
    public function store(StoreIotDeviceTypeRequest $request)
    {
        $validator = $request->validated();

        $iotDeviceType = IotDeviceType::create($validator);


        if (array_key_exists('defaultFields', $validator)) {
            $iotDeviceType->defaultFields();

            foreach ($validator['defaultFields'] as $fieldTypeId) {
                $iotDeviceType->defaultFields()->attach($fieldTypeId);
            }
        }

        /*$file = $validated->file('file');

        if ($file) {
            $link = 'iotDeviceType/' . $iotDeviceType->id . '.' . $file->getClientOriginalExtension();
            Storage::put('public/' . $link, file_get_contents($file->getRealPath()));
            $iotDeviceType->photo = $link;
            $iotDeviceType->save();
        }*/

        return new IotDeviceTypeResource($iotDeviceType);
    }

    /**
     * Display the specified iotDeviceType.
     *
     * @param  App\IotDeviceType
     * @return App\Http\Resources\IotDeviceTypeResource
     */
    public function show(IotDeviceType $iotDeviceType)
    {
        return new IotDeviceTypeResource($iotDeviceType);
    }

    /**
     * Update the specified iotDeviceType in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\IotDeviceType
     * @return App\Http\Resources\IotDeviceTypeResource
     */
    public function update(StoreIotDeviceTypeRequest $request, IotDeviceType $iotDeviceType)
    {
        $validator = $request->validated();

        $iotDeviceType->update($validator);

        if (array_key_exists('defaultFields', $validator)) {
            $iotDeviceType->defaultFields()->detach();

            foreach ($validator['defaultFields'] as $fieldTypeId) {
                $iotDeviceType->defaultFields()->attach($fieldTypeId);
            }
        }

        return new IotDeviceTypeResource($iotDeviceType);

        /*$file = $request->file('file');
        $iotDeviceType->update($request->except(['file']));

        if ($file) {
            Storage::delete('public/' . $iotDeviceType->link);
            $link = 'iotDeviceType/' . $iotDeviceType->id . '.' . $file->getClientOriginalExtension();
            Storage::put('public/' . $link, file_get_contents($file->getRealPath()));
            $iotDeviceType->photo = $link;
            $iotDeviceType->save();
        }*/
    }

    /**
     * Remove the specified iotDeviceType from storage.
     *
     * @param  App\IotDeviceType
     * @return \Illuminate\Http\Response
     */
    public function destroy(IotDeviceType $iotDeviceType)
    {
        $iotDeviceType->delete();

        return response()->json(null, 204);
    }
}
