<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Dome;
use Illuminate\Http\Request;

class DomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dome  $dome
     * @return \Illuminate\Http\Response
     */
    public function show(Dome $dome)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dome  $dome
     * @return \Illuminate\Http\Response
     */
    public function edit(Dome $dome)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dome  $dome
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dome $dome)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dome  $dome
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dome $dome)
    {
        //
    }
}
