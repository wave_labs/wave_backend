<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\ArDome;
use App\Dome;
use App\DomeHasInteraction;
use App\DomeItem;
use Illuminate\Http\Request;
use App\Jobs\FinalizeDomeAr;
use App\Http\Requests\StoreArDomeRequest;
use App\Http\Requests\UpdateArDomeRequest;
use App\Http\Resources\ArDomeResource;
use Carbon\Carbon;

class ArDomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return ArDomeResource::collection(ArDome::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArDomeRequest $request)
    {
        $validator = $request->validated();
        $dome = Dome::getClosest($validator['latitude'], $validator['longitude']);

        if ($dome > 0) {
            $validator['dome_id'] = $dome;
            $arDome = ArDome::create($validator);

            DomeHasInteraction::incrementCounter($validator['dome_id'], 1);
            // FinalizeDomeAr::dispatch($arDome->id, 1)->delay(now()->addMinutes(20));

            return new ArDomeResource($arDome);
        }

        return response()->json([
            'message' => "Currently there are no active domes in your area.",
        ], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ArDome  $arDome
     * @return \Illuminate\Http\Response
     */
    public function show(ArDome $arDome)
    {
        return new ArDomeResource($arDome);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ArDome  $arDome
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateArDomeRequest $request, ArDome $arDome)
    {
        $validator = $request->validated();

        $item = DomeItem::where('item', $validator['item'])->whereHas('interaction', function ($query) use ($arDome) {
            $query->where('dome_id', $arDome->dome_id)->where('dome_interaction_id', 1);
        })->value('id');
        $arDome->items()->syncWithoutDetaching($item);


        DomeHasInteraction::decrementCounter($arDome->dome_id, 1);
        $arDome->score = $arDome->items()->count();
        $arDome->timespent = Carbon::parse($arDome->created_at)->diffInSeconds(Carbon::now());
        $arDome->save();


        return new ArDomeResource($arDome);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ArDome  $arDome
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArDome $arDome)
    {
        $arDome->delete();

        return response()->json([
            'status' => 'success',
        ], 200);
    }
}
