<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\LitterCategoryImageTypeResource;
use App\Helper;


use App\Image;
use App\LitterCategoryImageType;

class ImageController extends Controller
{
    public function indexSighting()
    {
        $urls = Image::where('type', "sighting")->select('photo_number', 'url', 'creature_id')->get();
        $count = count($urls);

        return response()->json(
            [
                'success' => true,
                'data' => $urls,
                'metadata' => $count,
            ],
            200
        );
    }

    public function indexDive()
    {
        $urls = Image::where('type', "dive")->select('photo_number', 'url')->get();
        $count = count($urls);

        return response()->json(
            [
                'success' => true,
                'images' => $urls,
                'metadata' => $count,
            ],
            200
        );
    }

    public function indexLitter()
    {
        $urls = Image::where('type', "litter")->select('photo_number', 'url', 'material')->get();
        $count = count($urls);

        return response()->json(
            [
                'success' => true,
                'images' => $urls,
                'metadata' => $count,
            ],
            200
        );
    }

    public function show($class, $index)
    {
        if ($class == "litter" || $class == "sighting" || $class == "dive") {
            return Image::getImage($index, $class);
        }

        return response()->json(
            [
                'success' => false,
                'message' => 'Invalid image path ' . $class,
            ],
            422
        );
    }

    public function showCreature($index)
    {
        $image = Image::getCreatureImage($index);
        return $image ? $image->response() : response()->json(['message' => 'Invalid image path'], 422);
    }

    public function showAi($index)
    {
        return Helper::getPhoto('backend_python/images/' . $index);
    }

    public function indexLitterCategory()
    {
        $urls = LitterCategoryImageTypeResource::collection(LitterCategoryImageType::all());
        $count = count($urls);

        return response()->json(
            [
                'success' => true,
                'images' => $urls,
                'metadata' => $count,
            ],
            200
        );
    }
}
