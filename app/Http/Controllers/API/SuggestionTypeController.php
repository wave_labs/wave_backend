<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\QueryFilters\SuggestionTypeFilters;
use App\Http\Resources\SuggestionTypeResource;
use App\SuggestionType;
use Illuminate\Http\Request;

class SuggestionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SuggestionType::all();
    }

    public function selector(Request $request)
    {
        $filters = SuggestionTypeFilters::hydrate($request->query());
        return SuggestionTypeResource::collection(SuggestionType::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SuggestionType  $suggestionType
     * @return \Illuminate\Http\Response
     */
    public function show(SuggestionType $suggestionType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SuggestionType  $suggestionType
     * @return \Illuminate\Http\Response
     */
    public function edit(SuggestionType $suggestionType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SuggestionType  $suggestionType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SuggestionType $suggestionType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SuggestionType  $suggestionType
     * @return \Illuminate\Http\Response
     */
    public function destroy(SuggestionType $suggestionType)
    {
        //
    }
}
