<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Creature;
use App\Abundance;
use App\Http\Resources\CreatureResource;
use App\Http\Resources\CreatureDataResource;
use App\Http\QueryFilters\CreatureFilters;
use App\Http\Resources\AbundanceResource;
use App\Http\Requests\StoreCreatureRequest;
use App\Http\Requests\UpdateCreatureRequest;
use Storage;

class CreatureController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:creature-list', ['only' => ['index, show']]);
        $this->middleware('permission:creature-create', ['only' => ['store']]);
        $this->middleware('permission:creature-update', ['only' => ['update']]);
        $this->middleware('permission:creature-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the creature.
     *
     * @return App\Http\Resources\CreatureResource
     */
    public function index(CreatureFilters $filters)
    {
        return CreatureResource::collection(Creature::filterBy($filters)->paginate(10));
    }

    /**
     * Display the specified creature with statistics data.
     *
     * @param  App\Creature
     * @return App\Http\Resources\CreatureResource
     */
    public function data(Creature $creature)
    {
        return new CreatureDataResource($creature);
    }

    /**
     * Display a listing of the creature for selection.
     *
     * @return App\Http\Resources\CreatureResource
     */
    public function selector(CreatureFilters $filters)
    {
        return CreatureResource::collection(Creature::filterBy($filters)->orderBy('name')->get());
    }

    /**
     * Store a newly created creature in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\CreatureResource
     */
    public function store(StoreCreatureRequest $request)
    {
        $validator = $request->validated();

        $creature = Creature::create($validator);


        if (array_key_exists('sources', $validator)) {
            $creature->sources()->attach($validator['sources']);
        }

        Abundance::create([
            'creature_id' => $creature->id,
            'level_1' => $validator['level_1'],
            'level_2' => $validator['level_2'],
            'level_3' => $validator['level_3'],
            'level_4' => $validator['level_4'],
        ]);


        /*$file = $validated->file('file');

        if ($file) {
            $link = 'creature/' . $creature->id . '.' . $file->getClientOriginalExtension();
            Storage::put('public/' . $link, file_get_contents($file->getRealPath()));
            $creature->photo = $link;
            $creature->save();
        }*/

        return new CreatureResource($creature);
    }

    /**
     * Display the specified creature.
     *
     * @param  App\Creature
     * @return App\Http\Resources\CreatureResource
     */
    public function show(Creature $creature)
    {
        return new CreatureResource($creature);
    }

    /**
     * Update the specified creature in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Creature
     * @return App\Http\Resources\CreatureResource
     */
    public function update(UpdateCreatureRequest $request, Creature $creature)
    {
        $validator = $request->validated();

        $creature->update($validator);

        if (array_key_exists('sources', $validator)) {
            $creature->sources()->sync($validator['sources']);
        }

        return new CreatureResource($creature);

        /*$file = $request->file('file');
        $creature->update($request->except(['file']));

        if ($file) {
            Storage::delete('public/' . $creature->link);
            $link = 'creature/' . $creature->id . '.' . $file->getClientOriginalExtension();
            Storage::put('public/' . $link, file_get_contents($file->getRealPath()));
            $creature->photo = $link;
            $creature->save();
        }*/
    }

    /**
     * Remove the specified creature from storage.
     *
     * @param  App\Creature
     * @return \Illuminate\Http\Response
     */
    public function destroy(Creature $creature)
    {
        $creature->delete();

        return response()->json(null, 204);
    }
}
