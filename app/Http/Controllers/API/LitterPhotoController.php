<?php

namespace App\Http\Controllers\API;

use App\Helper;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\LitterPhoto;

class LitterPhotoController extends Controller
{
    public function index(Request $request)
    {
        $urls = LitterPhoto::where('litter_id', $request->query()['id'])->get();
        $count = count($urls);

        return response()->json(
            [
                'success' => true,
                'data' => $urls,
                'metadata' => $count,
            ],
            200
        );
    }

    public function show($index) //show a specific photo
    {
        return Helper::getPhoto('data/images/apps/litter_reporter/' . $index);
    }
}
