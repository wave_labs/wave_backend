<?php

namespace App\Http\Controllers\API;

use App\Creature;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Tag;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Http\Requests\StoreDiveRequest;
use App\Http\Requests\UpdateDiveRequest;

use App\Http\QueryFilters\DiveFilters;

use App\Http\Resources\DiveResource;
use App\Http\Resources\DiveDataResource;

use App\Dive;
use App\DivePivot;
use App\Exports\DiveExport;
use App\Helper;
use App\Http\Resources\CreatureResource;
use App\Http\Resources\DiveCoordResource;
use App\Http\Resources\DivePivotResource;
use App\User;
use App\UserDiveCenter;
use Error;
use Illuminate\Support\Facades\DB as FacadesDB;

class DiveController extends Controller
{

    function __construct()
    {
        // $this->middleware('permission:dive-list', ['only' => ['index, show']]);
        // $this->middleware('permission:dive-create', ['only' => ['store']]);
        // $this->middleware('permission:dive-update', ['only' => ['update']]);
        // $this->middleware('permission:dive-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = DiveFilters::hydrate($request->query());
        $users = Helper::getUserFromRequest($request);
        return DiveResource::collection(Dive::filterBy($filters)->getPosts($users)->latest()->paginate(5));
    }

    public function csvExport(Request $request)
    {
        return (new DiveExport($request))->download('dives.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDivingSpot($diving_spot)
    {
        $dives = Dive::where('diving_spot_id', $diving_spot)->get();
        return DiveDataResource::collection($dives);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDiveRequest $request)
    {
        $validator = $request->validated();

        //dd($validator);
        DB::beginTransaction();
        $dive = Dive::create($validator);

        //GET NUMBER OF CREATURES AND INITIALIZE ARRAYS
        if (array_key_exists('creatures', $validator) && array_key_exists('abundance', $validator)) {
            $nmr_creatures = count($validator['creatures']);
            $nmr_abundance = count($validator['abundance']);
            if ($nmr_creatures && $nmr_abundance) {
                if ($nmr_creatures != $nmr_abundance) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Every creature needs an abundance value'
                    ], 422);
                }

                foreach ($validator['creatures'] as $i => $creature) {
                    $current_abundance = $validator['abundance'][$i];
                    $dive->creatures()->attach([$creature => ['abundance_value' => $current_abundance]]);
                }
            }
        }

        DB::commit();

        return new DiveResource($dive);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Dive
     * @return \Illuminate\Http\Response
     */
    public function show(Dive $dive)
    {
        return new DiveResource($dive);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDiveRequest $request, Dive $dive)
    {
        $validator = $request->validated();

        $dive->update($validator);

        //GET NUMBER OF CREATURES AND INITIALIZE ARRAYS
        $nmr_creatures = count($validator['creatures']);
        $nmr_abundance = count($validator['abundance']);
        if ($nmr_creatures &&  $nmr_abundance) {
            if ($nmr_creatures != $nmr_abundance) {
                return response()->json([
                    'success' => false,
                    'message' => 'Every creature needs an abundance value'
                ], 422);
            }
            $dive->creatures()->detach();
            foreach ($validator['creatures'] as $i => $creature) {
                $current_abundance = $validator['abundance'][$i];
                $dive->creatures()->attach([$creature => ['abundance_value' => $current_abundance]]);
            }
        } else {
            $dive->creatures()->detach();
        }


        return new DiveResource($dive);
    }

    /**
     * Display a listing of all litter coordinates and id
     *
     * @return App\Http\Resources\LitterCoordResource
     */
    public function indexAllCoordinates(Request $request)
    {
        $filters = DiveFilters::hydrate($request->query());
        $users = Helper::getUserFromRequest($request);

        return DiveCoordResource::collection(Dive::filterBy($filters)->getPosts($users)->get());
    }

    public function monthlyReport(Request $request)
    {
        $date = Carbon::now()->firstOfMonth()->subMonths(11);
        $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
        $last = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();
        $total = [];
        $filters = DiveFilters::hydrate($request->query());
        if ($request->self == 'true') {
            $users = Helper::getUserFromRequest($request, true);
            for ($i = 0; $i <= 11; $i++) {
                $dives = Dive::filterBy($filters)->getPosts($users)->whereBetween("date", [$first, $last])->count();
                array_push($total, $dives);
                $first->addMonths(1);
                $last->addMonths(1);
            }
        } else {
            for ($i = 0; $i <= 11; $i++) {
                $dives = Dive::filterBy($filters)->whereBetween("date", [$first, $last])->count();
                array_push($total, $dives);
                $first->addMonths(1);
                $last->addMonths(1);
            }
        }

        return $total;
    }

    public function mostReportedSpecies(Request $request)
    {
        $users = Helper::getUserFromRequest($request, true);

        if ($request->date) {
            $creatures = Creature::whereHas('dives')->withCount([
                'dives' => function ($query) use ($users, $request) {
                    $query->where('user_id', $users)->whereBetween('date', [$request->date[0], $request->date[1]]);
                }
            ])->orderBy('dives_count', 'desc')->take(5)->get();
        } else {
            $creatures = Creature::whereHas('dives')->withCount([
                'dives' => function ($query) use ($users) {
                    $query->where('user_id', $users);
                }
            ])->orderBy('dives_count', 'desc')->take(5)->get();
        }

        return $creatures;
    }

    public function speciesReports(Request $request)
    {
        $date = Carbon::now()->firstOfMonth()->subMonths(11);

        return $date;
    }

    public function rankingReports(Request $request)
    {
        $usersDiveCenters = User::where('userable_type', '=', 'App\UserDiveCenter')->pluck('id');

        $first = Carbon::now()->subMonths(1);
        $last = Carbon::now();

        $rankingCount = DB::table('dives')->whereBetween("date", [$first, $last])->whereIn('user_id', $usersDiveCenters)
            ->selectRaw('user_id, COUNT(*) as count')
            ->groupBy('user_id')
            ->orderBy('count', 'desc')
            ->get();

        foreach ($rankingCount as $rank) {
            $rank->user = UserDiveCenter::where('id', '=', User::where('id', '=', $rank->user_id)->pluck('userable_id'))->pluck('name')->implode(',');
        }

        return $rankingCount;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dive $dive)
    {
        $dive->delete();

        return response()->json([
            'message' => 'Successfully deleted dive!',
        ], 200);
    }
}
