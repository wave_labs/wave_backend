<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Jobs\HandleArquivoJob;
use Spatie\Ssh\Ssh;
use Illuminate\Http\Request;

class GetArquivoPredictionInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if ($request->type == "example") {
            $dictionary = [
                "sofa" => "{'sofa': 99.51382279396057, 'cadeira': 0.21312315948307514, 'cama': 0.13521377695724368, 'mesa': 0.07624842692166567, 'armario': 0.03227450361009687, 'cama_animais': 0.029330802499316633}",
                "arvore" => "{'arvore': 99.90399479866028, 'flor': 0.09600354242138565}",
                "barco" => "{'barco': 99.17458891868591, 'autocarro': 0.43905386701226234, 'camiao': 0.1632321043871343, 'motociclo': 0.11610311921685934, 'carro': 0.10703062871471047}",
                "cao" => "{'cao': 99.58629608154297, 'gato': 0.35012459848076105, 'vaca': 0.03043956239707768, 'peixe': 0.020246570056769997, 'passaro': 0.006934205157449469, 'ovelha': 0.005955473170615733}",
                "carro" => "{'carro': 98.94380569458008, 'camiao': 0.7794985547661781, 'motociclo': 0.14240158488973975, 'autocarro': 0.09010993526317179, 'barco': 0.04418090975377709}",
                "casa" => "{'casa': 90.80884456634521, 'historico': 3.8841813802719116, 'misc': 2.289266884326935, 'apartamento': 1.7728852108120918, 'hotel': 1.2448174878954887}",
                "crianca" => "{'criancas': 85.93232035636902, 'bebe': 10.726133733987808, 'pessoas': 3.3415503799915314}",
                "flor" => "{'flor': 99.98457431793213, 'arvore': 0.015428951883222908}",
                "hotel" => "{'hotel': 92.57471561431885, 'apartamento': 6.49447962641716, 'misc': 0.8174306713044643, 'historico': 0.05890046595595777, 'casa': 0.05447531584650278}",
                "martelo" => "{'martelo': 97.00170755386353, 'chave_de_fendas': 1.5002541244029999, 'serra': 0.5115840118378401, 'parafuso': 0.5089234095066786, 'chave_de_caixa': 0.2693160669878125, 'outros': 0.20821546204388142}",
                "parafuso" => "{'parafuso': 99.25935864448547, 'chave_de_fendas': 0.2489695092663169, 'martelo': 0.21734607871621847, 'chave_de_caixa': 0.18423981964588165, 'serra': 0.061566359363496304, 'outros': 0.028531497810035944} ",
                "passaro" => "{'passaro': 99.98486042022705, 'gato': 0.0075365933298598975, 'peixe': 0.006068993025110103, 'cao': 0.0006643966571573401, 'vaca': 0.0005187202987144701, 'ovelha': 0.00034750185022858204}",
                "pessoa" => "{'pessoas': 97.15823531150818, 'bebe': 1.5657521784305573, 'criancas': 1.2760189361870289}",
                "sobremesa" => "{'sobremesa': 99.49766397476196, 'outros': 0.4024322610348463, 'fruta': 0.0715678499545902, 'carne': 0.01402618654537946, 'verdura': 0.009660340583650395, 'peixe': 0.004640584666049108}",
                "televisao" => "{'televisao': 99.83274340629578, 'forno': 0.07648986647836864, 'telemovel': 0.05789665156044066, 'frigorifico': 0.02018463856074959, 'maquina_de_lavar': 0.01268547639483586}",
                "vegetais" => "{'verdura': 91.49680137634277, 'fruta': 6.762609630823135, 'outros': 1.2282286770641804, 'sobremesa': 0.229769479483366, 'peixe': 0.21797548979520798, 'carne': 0.0646186585072428}",
            ];
            sleep(rand(2, 5));
            return $dictionary[strval($request->file)];
        } else {
            $filename = uniqid();
            $request->file('file')->storeAs('/datasets/images/', $filename . ".jpg", 'wave-server');
            $process = Ssh::create('eeg', '10.20.4.1', 10022)->disablePasswordAuthentication()->execute('python3 modelos_arquivo/predict.py ' . $filename . '.jpg');
            if ($process->isSuccessful()) {
                $response = $process->getOutput();
                $predictions = explode("_____________________________________________________________", $response);
                if (count($predictions) == 2) {
                    return $predictions[1];
                } else {
                    return "erro";
                }
            } else {
                return $process->getErrorOutput();
            }
        }
    }
}
