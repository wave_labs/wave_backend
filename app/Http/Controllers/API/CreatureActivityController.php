<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Activity;
use App\Creature;
use App\Http\Resources\CreatureActivityResource;

class CreatureActivityController extends Controller
{
    public function __invoke(Creature $creature)
    {
        return CreatureActivityResource::collection(Activity::whereHas('creatures', function ($q) use ($creature) {
            $q->whereCreatureId($creature->id);
        })->get());
    }
}
