<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use DB, Storage;
use Illuminate\Http\Request;

use App\SightingsData;
use App\Acoustic;
use App\Http\Resources\SightingDataResource;
use App\Traits\CreateSightingData;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use wapmorgan\Mp3Info\Mp3Info;
use App\Http\QueryFilters\SightingDataFilters;

class SightingsDataController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:sighting-list',['only' => ['index, show']]);
        $this->middleware('permission:sighting-create',['only' => ['store']]);
        $this->middleware('permission:sighting-update',['only' => ['update']]);
        $this->middleware('permission:sighting-delete',['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the sighting data
     *
     * @return App\Http\Resources\SightingDataResource
     */
    public function index(Request $request)
    {
        $filters = SightingDataFilters::hydrate($request->query());
        return SightingDataResource::collection(SightingsData::filterBy($filters)->paginate(20));  
    }

    /**
     * Store a newly created sighting data in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\SightingDataResource
     */
    public function store(Request $request)
    {   
        $type =  $request->input('type');
        $download = $request->input('download');

        if($type == 'Acoustic'){
            DB::beginTransaction();
            
            $sightingData = $this->CreateSightingData($request, $type, $download);          
            
            if ($sightingData->exists) { //if it already exists in the database
                DB::rollback();        
                return new SightingDataResource($sightingData);
            }

            $sightingData->save();

            $sightingData->acoustic()->create([
                'acoustic_type' => $request->input('acoustic_type'),
                'start_time' => $request->input('start_time'),
                'duration_s' => (new Mp3Info($request->file('file')))->duration,
            ]);

            DB::commit();
        }
        else{
            $sightingData = $this->CreateSightingData($request, $type, $download);          
            $sightingData->save();
        }

        return new SightingDataResource($sightingData);
    }

    /**
     * Display the specified sighting data.
     *
     * @param  id
     * @return App\Http\Resources\SightingDataResource
     */
    public function show($id)
    {
        return new SightingDataResource(SightingsData::find($id));
    }

    /**
     * Update the specified sighting data in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  id
     * @return App\Http\Resources\SightingDataResource
     */
    public function update(Request $request, $id)
    {
        $sightingData = SightingsData::find($id);
        $type =  $request->input('type');
        $file = $request->file('file');

        if($type == 'Acoustic'){
            DB::beginTransaction();
            
            $sightingData->update($request->all());
            $sightingData->acoustic()->update([
                'acoustic_type' => $request->input('acoustic_type'),
                'start_time' => $request->input('start_time'),
            ]);

            if($file){
                $sightingData->acoustic()->update([
                 'duration_s' => (new Mp3Info($request->file('file')))->duration
                ]);
            }

            DB::commit();
        }
        else{
            $sightingData->update($request->all());
        }

        //upload new file and delete old one
        if($file){
            Storage::delete('public/' . $sightingData->link);
            $link = $this->createLink($sightingData->sighting_id, $type, $file->getClientOriginalExtension());
            Storage::put('public/' . $link, file_get_contents($file->getRealPath()));
            $sightingData->link = $link;
            $sightingData->save();
        }
        return new SightingDataResource($sightingData);
    }

    /**
     * Remove the specified sighting data from storage.
     *
     * @param  id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $sightingData = SightingsData::find($id)->delete();

        return response()->json(null, 204);
    }

    public function downloadFile($url, $link){
        $guzzle = new Client();
        $response = $guzzle->get($url);                                 //TODO: ERRORS, ROLLBACK

        Storage::put('public/' . $link, $response->getBody());
    }

    public function createLink($sightingID, $type, $extension){
        $name = md5($sightingID . microtime());
        return  $this->generatePath($sightingID, $type) . $name . '.' . $extension;
    }

    public function generatePath($sightingID, $type){
        return 'sighting/' . $sightingID . '/' . $type . '/';
    }

    public function createSightingData(Request $request, $type, $download){
        $dowloadUrl = $request->input('url');
        $sightingID = $request->input('sighting_id');
        $extension = $request->input('extension');


        if($download){
            $sightingData = SightingsData::firstOrNew(
                [
                 'capsule_link' => $dowloadUrl,
                 'sighting_id' => $sightingID,
                 'underwater' => $request->input('underwater'),
                 'type' => $type
                ]
            );
            
            if(!$sightingData->exists){
                $link = $this->createLink($sightingID, $type, $extension);
                $this->downloadFile($dowloadUrl, $link);
                $sightingData->link = $link;
            }
        } else {
            $sightingData = SightingsData::make(
                [
                 'sighting_id' => $sightingID,
                 'underwater' => $request->input('underwater'),
                 'type' => $type
                ]
            );

            $file = $request->file('file');
            $link = $this->createLink($sightingID, $type, $file->getClientOriginalExtension());
            Storage::put('public/' . $link, file_get_contents($file->getRealPath()));
            $sightingData->link = $link;
        }

        return $sightingData;
    }

}
