<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\QueryFilters\LitterSubCategoryFilters;
use App\Http\Requests\StoreLitterSubCategoryRequest;
use App\Http\Resources\LitterSubCategoryResource;
use App\LitterSubCategory;
use Illuminate\Http\Request;

class LitterSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $language = $request->language ? $request->language : 'en';

        $filters = LitterSubCategoryFilters::hydrate($request->query());
        return LitterSubCategoryResource::collection(LitterSubCategory::filterBy($filters)->paginate(10), $language);
    }

    /**
     * Display a listing of the litter.
     *
     * @return App\Http\Resources\LitterSubCategoryResource
     */
    public function selector(Request $request)
    {
        $filters = LitterSubCategoryFilters::hydrate($request->query());
        return LitterSubCategoryResource::collection(LitterSubCategory::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLitterSubCategoryRequest $request)
    {
        $validator = $request->validated();

        $litterSubCategory = LitterSubCategory::create($validator);

        return new LitterSubCategoryResource($litterSubCategory);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LitterSubCategory  $litterSubCategory
     * @return \Illuminate\Http\Response
     */
    public function show(LitterSubCategory $litterSubCategory)
    {
        return new LitterSubCategoryResource($litterSubCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LitterSubCategory  $litterSubCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LitterSubCategory $litterSubCategory)
    {
        $litterSubCategory->update($request->all());

        return new LitterSubCategoryResource($litterSubCategory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LitterSubCategory  $litterSubCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(LitterSubCategory $litterSubCategory)
    {
        $litterSubCategory->delete();

        return response()->json(
            [
                'status' => 'success',
            ],
            200
        );
    }
}
