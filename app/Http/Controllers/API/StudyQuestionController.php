<?php

namespace App\Http\Controllers\API;


use App\StudyQuestion;
use Illuminate\Http\Request;

class StudyQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudyQuestion  $studyQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(StudyQuestion $studyQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudyQuestion  $studyQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(StudyQuestion $studyQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudyQuestion  $studyQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudyQuestion $studyQuestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudyQuestion  $studyQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudyQuestion $studyQuestion)
    {
        //
    }
}
