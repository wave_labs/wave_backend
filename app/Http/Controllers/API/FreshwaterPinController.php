<?php


namespace App\Http\Controllers\API;

use App\Exports\FreshwaterPinExport;
use App\Http\Controllers\Controller;

use App\FreshwaterPin;
use App\Http\QueryFilters\FreshwaterPinFilters;
use App\Http\Requests\StoreFreshwaterPinRequest;
use App\Http\Resources\FreshwaterPinResource;
use Illuminate\Http\Request;

use DB;

class FreshwaterPinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = FreshwaterPinFilters::hydrate($request->query());
        return FreshwaterPinResource::collection(FreshwaterPin::filterBy($filters)->latest()->paginate(10));
    }

    public function selector(Request $request)
    {
        $filters = FreshwaterPinFilters::hydrate($request->query());
        return FreshwaterPinResource::collection(FreshwaterPin::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFreshwaterPinRequest $request)
    {
        $validator = $request->validated();

        DB::beginTransaction();
        $record = FreshwaterPin::create(['creature_id' => 54]);
        $record->storeForm($validator['form']);
        $record->storePins($validator['positions']);
        DB::commit();

        return new FreshwaterPinResource($record);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FreshwaterPin  $freshwaterPin
     * @return \Illuminate\Http\Response
     */
    public function show(FreshwaterPin $freshwaterPin)
    {
        return new FreshwaterPinResource($freshwaterPin);
    }

    public function csvExport(Request $request)
    {
        return (new FreshwaterPinExport($request))->download('export.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FreshwaterPin  $freshwaterPin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FreshwaterPin $freshwaterPin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FreshwaterPin  $freshwaterPin
     * @return \Illuminate\Http\Response
     */
    public function destroy(FreshwaterPin $freshwaterPin)
    {
        $freshwaterPin->delete();

        return response()->json(null, 204);
    }
}
