<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as IMG;
use App\SightingPhoto;

class SightingPhotoController extends Controller
{
    public function index() //json response for all photos
    {
        $urls = SightingPhoto::select('sighting_id', 'url')->with('sighting')->get();

        return response()->json(
        [
            'status' => 'success',
            'photo' => $urls,
        ], 200);
    }

    public function store(Request $request) //receive photo and save it on folder
    {
    	$v = Validator::make($request->all(), [
            'sighting_id' => 'required',
        ]);

        if ($request->file('image')->isValid())
        {
        	if ($request->file('image')->extension() == 'png' || $request->file('image')->extension() == 'jpeg' || $request->file('image')->extension() == 'jpg') 
	        {
	        	$image = $request->file('image');
			    $filename = $request->sighting_id. '-' . uniqid() . '.png';

			    IMG::make($image)->encode('png', 65)->resize(760, null, function ($c) {
			        $c->aspectRatio();
			        $c->upsize();
			        })->save(storage_path('/uploaded/sighting_photo/' . $filename));

			    $table_entry = new Request([
	            'sighting_id' => $request->sighting_id,
	            'url' => 'api/uploaded/sighting/'.$filename,
	        	]);

	        	SightingPhoto::create($table_entry->toArray());

			    return response()->json([
	                'status' => 'success',
	            ], 200);
	        }
	        else
	        {
	        	return response()->json([
                'status' => 'unsupported format',
            ], 422);
	        }
        }
        else
        {
        	return response()->json([
                'status' => 'upload failed',
            ], 422);
        }
    }

    public function show($index) //show a specific photo
    {
    	$storagePath = storage_path('/uploaded/sighting_photo/'.$index.'.png');

        return IMG::make($storagePath)->response();
    }
}
