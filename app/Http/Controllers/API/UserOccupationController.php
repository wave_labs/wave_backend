<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Http\Resources\UserOccupationResource;
use App\UserOccupation;
use Illuminate\Http\Request;


class UserOccupationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of vehicles.
     *
     * @return App\Http\Resources\VehicleResource
     */
    public function selector()
    {
        return UserOccupationResource::collection(UserOccupation::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $occupation = UserOccupation::create($request->all());

        return new UserOccupationResource($occupation);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserOccupation  $userOccupation
     * @return \Illuminate\Http\Response
     */
    public function show(UserOccupation $userOccupation)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserOccupation  $userOccupation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserOccupation $userOccupation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserOccupation  $userOccupation
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserOccupation $userOccupation)
    {
        //
    }
}
