<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\QueryFilters\LitterCategoryFilters;
use App\Http\Requests\StoreLitterCategoryRequest;
use App\Http\Resources\LitterCategoryResource;
use App\LitterCategory;
use App\LitterCategoryImage;
use Illuminate\Http\Request;
use DB;
use Intervention\Image\Facades\Image as IMG;

class LitterCategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = LitterCategoryFilters::hydrate($request->query());

        return LitterCategoryResource::collection(LitterCategory::filterBy($filters)->paginate(20));
    }

    /**
     * Display a listing of the litter.
     *
     * @return App\Http\Resources\LitterCategoryResource
     */
    public function selector(Request $request)
    {
        $filters = LitterCategoryFilters::hydrate($request->query());
        return LitterCategoryResource::collection(LitterCategory::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLitterCategoryRequest $request)
    {
        $validator = $request->validated();

        DB::beginTransaction();

        $litterCategory = LitterCategory::create($validator);

        LitterCategoryImage::generateImages($validator['image'], $litterCategory->id);
        // LitterCategoryImage::generateImageIos($validator['image'], $filename . '.png', "ios",  $filename);
        // LitterCategoryImage::generateImage($validator['image'], $filename . '.png', "menu",  $filename, 'images/litter-reporter/menu/');
        // LitterCategoryImage::generateImage($validator['image'], $filename . '.png', "android-self",  $filename, 'images/litter-reporter/android/self/');
        // LitterCategoryImage::generateImage($validator['image'], $filename . 'p.png', "android-self-picked",  $filename, 'images/litter-reporter/android/self/');
        // LitterCategoryImage::generateImage($validator['image'], $filename . '.png', "android-others",  $filename, 'images/litter-reporter/android/other/');
        // LitterCategoryImage::generateImage($validator['image'], $filename . 'p.png', "android-others-picked",  $filename, 'images/litter-reporter/android/other/');


        DB::commit();

        return new LitterCategoryResource($litterCategory);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LitterCategory  $litterCategory
     * @return \Illuminate\Http\Response
     */
    public function show(LitterCategory $litterCategory)
    {
        return new LitterCategoryResource($litterCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LitterCategory  $litterCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LitterCategory $litterCategory)
    {
        $litterCategory->update($request->all());

        return new LitterCategoryResource($litterCategory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LitterCategory  $litterCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(LitterCategory $litterCategory)
    {
        $litterCategory->deleteImages();
        $litterCategory->delete();

        return response()->json(
            [
                'status' => 'success',
            ],
            200
        );
    }
}
