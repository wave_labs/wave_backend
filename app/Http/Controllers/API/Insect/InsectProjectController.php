<?php

namespace App\Http\Controllers\API\Insect;

use App\Http\Requests\InsectProjectRequest;
use App\Http\Resources\InsectProjectResource;
use App\InsectProject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\InsectProjectHasUser;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as IMG;
use File;

use App\Helper;
use App\Http\QueryFilters\InsectProjectFilters;
use App\Http\Resources\UserResource;
use App\Image;
use App\User;

class InsectProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = InsectProjectFilters::hydrate($request->query());
        $user = auth()->user();
        $userProjects = InsectProjectHasUser::where('user_id', '=', $user->id)->pluck('project_id');
        return InsectProjectResource::collection(InsectProject::filterBy($filters)->whereIn('id', $userProjects)->get());
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsectProjectRequest $request)
    {
        if ($request->image) {
            $path = self::savePhotoLocally($request->image);
            $request['imagePath'] = $path;
        } else {
            $request['imagePath'] = null;
        }

        $validator = $request->validated();

        $record = InsectProject::create($validator);

        $record->imagePath = $request['imagePath'];
        $record->save();

        $owner = InsectProjectHasUser::create([
            'project_id' => $record->id,
            'user_id' => $record->owner_id
        ]);

        return new InsectProjectResource($record);
    }

    public static function savePhotoLocally($photo)
    {
        $filename = uniqid() . '.png';
        IMG::make($photo)->encode('png', 70)->resize(760, null, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        })->save(public_path('/storage/uploaded/photo/insectProject/' . $filename));

        return ('/storage/uploaded/photo/insectProject/' . $filename);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectProject  $insectProject
     * @return \Illuminate\Http\Response
     */
    public function show(InsectProject $insectProject)
    {
        $user = auth()->user();
        $userProjects = InsectProjectHasUser::where('user_id', '=', $user->id)->pluck('project_id')->toArray();
        $project = new InsectProjectResource(InsectProject::where('id', '=', $insectProject->id)->firstOrFail());
        if (in_array($project->id, $userProjects)) {
            return $project;
        } else {
            return response()->json('No permissions', 403);
        }
    }

    public function addMember($projectId, Request $request)
    {
        $user = auth()->user();
        $project = new InsectProjectResource(InsectProject::where('id', '=', $projectId)->firstOrFail());

        if ($user->id == $project->owner->id) { //only the owner can add members
            $newMember = User::where('email', '=', $request->email)->first();
            if ($newMember) {
                $partOfTeam = InsectProjectHasUser::where([['user_id', '=', $newMember->id], ['project_id', '=', $projectId]])->first();

                if ($partOfTeam) {
                    return response()->json([
                        'message' => 'This user is already part of this project',
                    ], 404);
                } else {
                    InsectProjectHasUser::create([
                        'project_id' => $projectId,
                        'user_id' => $newMember->id,
                        'experienceLevel' => $request->experienceLevel
                    ]);
                    return $newMember;
                }
            } else {
                return response()->json([
                    'message' => 'This email does not have an account on Wave Labs website',
                ], 404);
            }
        } else {
            return response()->json('No permissions', 403);
        }
    }

    public function removeMember($userId, $projectId)
    {
        $user = auth()->user();
        $project = new InsectProjectResource(InsectProject::where('id', '=', $projectId)->firstOrFail());
        if ($user->id == $project->owner->id) { //user who tried to delete is the owner
            if ($userId == $project->owner->id) {
                return response()->json('Cannot delete the owner', 400);
            } else {
                $userDeleted = InsectProjectHasUser::where([
                    ['user_id', '=', $userId],
                    ['project_id', '=', $projectId],
                ])->first()->delete();
                /* $project = new InsectProjectResource(InsectProject::where('id', '=', $projectId)->firstOrFail()); */
                return response()->json(null, 204);
            }
        } else { //user who tried to delete is not the owner, cannot delete users
            return response()->json('No permissions', 403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectProject  $insectProject
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectProject $insectProject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectProject  $insectProject
     * @return \Illuminate\Http\Response
     */
    public function update(InsectProjectRequest $request, InsectProject $insectProject)
    {
        /* if ($request->image) {
            $path = self::savePhotoLocally($request->imagePath);
            $request['imagePath'] = $path;
        } else {
            $request['imagePath'] = null;
        }

        $validator = $request->validated();
        $insectProject->update($validator);

        $insectProject->imagePath = $request['imagePath'];
        $insectProject->save();

        return new InsectProjectResource($insectProject); */

        if ($request->image) {
            $path = self::savePhotoLocally($request->image);
            $request['imagePath'] = $path;
        } else {
            $request['imagePath'] = null;
        }

        $validator = $request->validated();

        $insectProject->update($validator);

        $insectProject->imagePath = $request['imagePath'];
        $insectProject->save();

        return new InsectProjectResource($insectProject);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectProject  $insectProject
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectProject $insectProject)
    {
        //
    }
}
