<?php

namespace App\Http\Controllers;

use App\InsectProjectHasUser;
use Illuminate\Http\Request;

class InsectProjectHasUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectProjectHasUser  $insectProjectHasUser
     * @return \Illuminate\Http\Response
     */
    public function show(InsectProjectHasUser $insectProjectHasUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectProjectHasUser  $insectProjectHasUser
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectProjectHasUser $insectProjectHasUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectProjectHasUser  $insectProjectHasUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectProjectHasUser $insectProjectHasUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectProjectHasUser  $insectProjectHasUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectProjectHasUser $insectProjectHasUser)
    {
        //
    }
}
