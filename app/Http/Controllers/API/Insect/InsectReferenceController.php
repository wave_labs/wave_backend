<?php

namespace App\Http\Controllers\API\Insect;

use App\Http\Controllers\Controller;
use App\Http\QueryFilters\InsectReferencesFilters;
use App\Http\Requests\InsectReferenceRequest;
use App\Http\Resources\InsectReferenceResource;
use App\InsectAuthor;
use App\InsectReference;
use App\Exports\InsectExport;
use App\Exports\SinonimiaExport;
use App\Http\QueryFilters\InsectAuthorFilters;
use App\Http\Resources\InsectAuthorResource;
use App\Http\Resources\InsectReferenceSelectorResource;
use App\Imports\InsectReferencesImport;
use App\InsectReferenceCategories;
use App\InsectReferenceHasCategories;
use App\InsectReferenceHasKeyword;
use App\InsectReferenceHasTag;
use App\InsectReferenceKeyword;
use App\InsectReferenceTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class InsectReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(InsectReferencesFilters $filters)
    {
        return InsectReferenceResource::collection(InsectReference::filterBy($filters)->paginate(10));
    }

    public function csvExport(Request $request)
    {
        return (new InsectExport($request))->download('export.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function exportSinonimia(Request $request)
    {
        return (new SinonimiaExport($request))->download('sinonimias_export.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function importReferences(Request $request)
    {
        Excel::import(new InsectReferencesImport, $request->file('excel_file'));
        return redirect('/')->with('success', 'All good');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selector(InsectReferencesFilters $filters)
    {
        return InsectReferenceSelectorResource::collection(InsectReference::filterBy($filters)->orderBy('created_at', 'desc')->get());
    }

    public function authorsSelector(InsectAuthorFilters $filters)
    {
        return InsectAuthorResource::collection(InsectAuthor::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsectReferenceRequest $request)
    {
        $validator = $request->validated();
        $record = InsectReference::create($validator);

        $request_array = $request->all();

        if (array_key_exists("authorList", $request_array)) {
            $ids = [];
            foreach ($request_array["authorList"] as $aAuthor) {
                $author = InsectAuthor::where('name', $aAuthor)->first();
                if (!$author) {
                    $author = InsectAuthor::create([
                        'name' => $aAuthor
                    ]);
                }
                array_push($ids, $author->id);
            }

            $record->authors()->attach($ids);
        }
        if (array_key_exists("tags", $request_array)) {
            foreach ($request_array["tags"] as $aTag) {
                $tag = InsectReferenceTag::where('tag', $aTag)->first();
                $tagAssociation = InsectReferenceHasTag::create([
                    'reference_id' => $record->id,
                    'reference_tag_id' => $tag->id
                ]);
            }
        }
        if (array_key_exists("keywords", $request_array)) {
            foreach ($request_array["keywords"] as $aKeyword) {
                $keyword = InsectReferenceKeyword::where('keyword', $aKeyword)->first();
                $keywordAssociation = InsectReferenceHasKeyword::create([
                    'reference_id' => $record->id,
                    'reference_keyword_id' => $keyword->id
                ]);
            }
        }
        if (array_key_exists("categories", $request_array)) {
            foreach ($request_array["categories"] as $aCategory) {
                $category = InsectReferenceCategories::where('category', $aCategory)->first();
                $categoryAssociation = InsectReferenceHasCategories::create([
                    'reference_id' => $record->id,
                    'reference_category_id' => $category->id
                ]);
            }
        }


        return new InsectReferenceResource($record);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectReference  $insectReference
     * @return \Illuminate\Http\Response
     */
    public function show(InsectReference $insectReference)
    {
        return new InsectReferenceResource($insectReference);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectReference  $insectReference
     * @return \Illuminate\Http\Response
     */
    public function update(InsectReferenceRequest $request, InsectReference $insectReference)
    {
        $validator = $request->validated();

        $insectReference->update($validator);

        $request_array = $request->all();

        if (array_key_exists("authorList", $request_array)) {
            $ids = [];
            foreach ($request_array["authorList"] as $aAuthor) {
                $author = InsectAuthor::where('name', $aAuthor)->first();
                if (!$author) {
                    $author = InsectAuthor::create([
                        'name' => $aAuthor
                    ]);
                }
                array_push($ids, $author->id);
            }

            $insectReference->authors()->sync($ids);
        }
        InsectReferenceHasTag::where('reference_id', '=', $insectReference->id)->delete();
        if (array_key_exists("tags", $request_array)) {
            foreach ($request_array["tags"] as $aTag) {
                $tag = InsectReferenceTag::where('tag', $aTag)->first();
                $tagAssociation = InsectReferenceHasTag::create([
                    'reference_id' => $insectReference->id,
                    'reference_tag_id' => $tag->id
                ]);
            }
        }
        InsectReferenceHasKeyword::where('reference_id', '=', $insectReference->id)->delete();
        if (array_key_exists("keywords", $request_array)) {
            foreach ($request_array["keywords"] as $aKeyword) {
                $keyword = InsectReferenceKeyword::where('keyword', $aKeyword)->first();
                $keywordAssociation = InsectReferenceHasKeyword::create([
                    'reference_id' => $insectReference->id,
                    'reference_keyword_id' => $keyword->id
                ]);
            }
        }
        InsectReferenceHasCategories::where('reference_id', '=', $insectReference->id)->delete();
        if (array_key_exists("categories", $request_array)) {
            foreach ($request_array["categories"] as $aCategory) {
                $category = InsectReferenceCategories::where('category', $aCategory)->first();
                $categoryAssociation = InsectReferenceHasCategories::create([
                    'reference_id' => $insectReference->id,
                    'reference_category_id' => $category->id
                ]);
            }
        }

        return new InsectReferenceResource($insectReference);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectReference  $insectReference
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectReference $insectReference)
    {
        $insectReference->delete();

        return response()->json(null, 204);
    }
}
