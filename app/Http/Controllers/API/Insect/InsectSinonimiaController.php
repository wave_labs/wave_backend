<?php

namespace App\Http\Controllers\API\Insect;

use App\Http\Controllers\Controller;
use App\Http\Resources\InsectSinonimiaResource;
use App\Imports\InsectSinonimiaImport;
use App\InsectSinonimia;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InsectSinonimiaController extends Controller
{
    public function import_sinonimias(Request $request)
    {
        Excel::import(new InsectSinonimiaImport, $request->file('excel_file'));
        return redirect('/')->with('success', 'All good');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $taxonomicStatus = "";
        $nomenclatureStatus = "";
        if ($request->taxonomicStatus) {
            $taxonomicStatus = implode(", ", $request->taxonomicStatus);
        }
        if ($request->nomenclatureStatus) {
            $nomenclatureStatus = implode(", ", $request->nomenclatureStatus);
        }
        if ($request->specieRank == "Specie") {
            return InsectSinonimia::create([
                'specific_id' => $request->specieID,
                'genus' => $request->genus,
                'subgenus' => $request->subgenus,
                'specificEpithet' => $request->specificEpithet,
                'infraSpecificEpithet' => $request->infraSpecificEpithet,
                'reference_id' => $request->reference,
                'taxonRank' => $request->taxonRank,
                'taxonRemarks' => $request->taxonRemarks,
                'language' => $request->language,
                'rights' => $request->rights,
                'license' => $request->license,
                'institutionID' => $request->institutionID,
                'institutionCode' => $request->institutionCode,
                'rightSholder' => $request->rightSholder,
                'accessRights' => $request->accessRights,
                'source' => $request->source,
                'datasetID' => $request->datasetID,
                'datasetName' => $request->datasetName,
                'taxonomicStatus' => $taxonomicStatus,
                'nomenclatureStatus' => $nomenclatureStatus
            ]);
        } else {
            return InsectSinonimia::create([
                'infra_specific_id' => $request->specieID,
                'genus' => $request->genus,
                'subgenus' => $request->subgenus,
                'specificEpithet' => $request->specificEpithet,
                'infraSpecificEpithet' => $request->infraSpecificEpithet,
                'reference_id' => $request->reference,
                'taxonRank' => $request->taxonRank,
                'taxonRemarks' => $request->taxonRemarks,
                'language' => $request->language,
                'rights' => $request->rights,
                'license' => $request->license,
                'institutionID' => $request->institutionID,
                'institutionCode' => $request->institutionCode,
                'rightSholder' => $request->rightSholder,
                'accessRights' => $request->accessRights,
                'source' => $request->source,
                'datasetID' => $request->datasetID,
                'datasetName' => $request->datasetName,
                'taxonomicStatus' => $taxonomicStatus,
                'nomenclatureStatus' => $nomenclatureStatus
            ]);
        }
        return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectSinonimia  $insectSinonimia
     * @return \Illuminate\Http\Response
     */
    public function show(InsectSinonimia $insectSinonimia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectSinonimia  $insectSinonimia
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectSinonimia $insectSinonimia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectSinonimia  $insectSinonimia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectSinonimia $insect_sinonimium)
    {

        $taxonomicStatus = "";
        $nomenclatureStatus = "";
        if ($request->taxonomicStatus) {
            $taxonomicStatus = implode(", ", $request->taxonomicStatus);
        }
        if ($request->nomenclatureStatus) {
            $nomenclatureStatus = implode(", ", $request->nomenclatureStatus);
        }

        $request["taxonomicStatus"] = $taxonomicStatus;
        $request["nomenclatureStatus"] = $nomenclatureStatus;
        $insect_sinonimium->update($request->all());


        return new InsectSinonimiaResource($insect_sinonimium);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectSinonimia  $insectSinonimia
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectSinonimia $insect_sinonimium)
    {
        $insect_sinonimium->delete();

        return response()->json(null, 204);
    }
}
