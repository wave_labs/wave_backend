<?php

namespace App\Http\Controllers\API\Insect;

use App\Http\Controllers\Controller;
use App\Http\Requests\InsectFamilyRequest;
use App\Http\Resources\InsectFamilyResource;
use App\InsectFamily;
use App\InsectSpecies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InsectFamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return InsectFamilyResource::collection(InsectFamily::with('species')->paginate(10));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selector()
    {
        return InsectFamilyResource::collection(InsectFamily::with('species')->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsectFamilyRequest $request)
    {
        $validator = $request->validated();

        DB::beginTransaction();
        $record = InsectFamily::create($validator);

        foreach ($validator['species'] as $species) {
            InsectSpecies::create([
                'name' => $species['name'],
                'family_id' => $record->id,
            ]);
        }
        DB::commit();
        return new InsectFamilyResource($record);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectFamily  $insectFamily
     * @return \Illuminate\Http\Response
     */
    public function show(InsectFamily $insectFamily)
    {
        return new InsectFamilyResource($insectFamily);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectFamily  $insectFamily
     * @return \Illuminate\Http\Response
     */
    public function update(InsectFamilyRequest $request, InsectFamily $insectFamily)
    {
        $validator = $request->validated();

        $insectFamily->update($validator);

        foreach ($validator['species'] as $species) {
            if (array_key_exists('id', $species)) {
                $currentSpecies = InsectSpecies::find($species['id']);

                $currentSpecies->update([
                    'name' => $species['name'],
                ]);
            } else {
                InsectSpecies::create([
                    'name' => $species['name'],
                    'family_id' => $insectFamily->id,
                ]);
            }
        }

        return new InsectFamilyResource($insectFamily);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectFamily  $insectFamily
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectFamily $insectFamily)
    {
        $insectFamily->delete();

        return response()->json(null, 204);
    }
}
