<?php

namespace App\Http\Controllers\API\Insect;

use App\Http\Controllers\Controller;

use App\InsectZone;
use Illuminate\Http\Request;

class InsectZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectZone  $insectZone
     * @return \Illuminate\Http\Response
     */
    public function show(InsectZone $insectZone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectZone  $insectZone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectZone $insectZone)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectZone  $insectZone
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectZone $insectSpecies)
    {
        $insectSpecies->delete();

        return response()->json(null, 204);
    }
}
