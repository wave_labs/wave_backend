<?php

namespace App\Http\Controllers\API\Insect;

ini_set('max_execution_time', 300);

use App\InsectOccurrence;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\QueryFilters\InsectInfraSpecificFilters;
use App\Http\QueryFilters\InsectOccurrenceFilters;
use App\Http\QueryFilters\InsectSpecificFilters;
use App\Http\Requests\InsectOccurrenceRequest;
use App\Http\Resources\InsectInfraSpecificResource;
use App\Http\Resources\InsectInfraSpecificSelectorResource;
use App\Http\Resources\InsectOccurrenceResource;
use App\Http\Resources\InsectSpecificResource;
use App\Http\Resources\InsectSpecificSelectorResource;
use App\Imports\InsectOccurrenceImport;
use App\InsectAuthor;
use App\InsectInfraSpecific;
use App\InsectOccurrenceIdentifications;
use App\InsectSpecific;
use Maatwebsite\Excel\Facades\Excel;

class InsectOccurrenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(InsectOccurrenceFilters $filters)
    {
        return InsectOccurrenceResource::collection(InsectOccurrence::filterBy($filters)->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsectOccurrenceRequest $request)
    {
        $validator = $request->validated();
        $record = InsectOccurrence::create($validator);

        $request_array = $request->all();

        if (array_key_exists("identifiedBy", $request_array)) {
            $author = InsectAuthor::firstOrCreate(['name' => $request_array["identifiedBy"]]);
            $record->identifiedBy = $author->id;
        }
        if (array_key_exists("recordedBy", $request_array)) {
            $ids = [];
            foreach ($request_array["recordedBy"] as $aAuthor) {
                $author = InsectAuthor::firstOrCreate(['name' => $aAuthor]);
                array_push($ids, $author->id);
            }
            $record->recordedBy()->attach($ids);
        }

        if (is_array($request_array["eventDate"])) {
            $record->eventDateStart = $request_array["eventDate"][0];
            $record->eventDateEnd = $request_array["eventDate"][1];
        } else {
            $record->eventDateStart = $request_array["eventDate"];
        }
        $record->save();

        return new InsectOccurrenceResource($record);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectOccurrence  $insectOccurrence
     * @return \Illuminate\Http\Response
     */
    public function show(InsectOccurrence $insectOccurrence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectOccurrence  $insectOccurrence
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectOccurrence $insectOccurrence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectOccurrence  $insectOccurrence
     * @return \Illuminate\Http\Response
     */
    public function update(InsectOccurrenceRequest $request, InsectOccurrence $insectOccurrence)
    {
        $validator = $request->validated();

        $request_array = $request->all();

        if (array_key_exists("identifiedBy", $request_array) || array_key_exists("specific_id", $request_array) || array_key_exists("infra_specific_id", $request_array)) {
            $author = InsectAuthor::firstOrCreate(['name' => $request_array["identifiedBy"]]);

            //create identification to save it as previous identification            
            InsectOccurrenceIdentifications::create([
                'occurrence_id' => $insectOccurrence->id,
                'author_id' =>  $insectOccurrence->identifiedBy,
                'authorExperience' => $insectOccurrence->authorExperience,
                'species_id' => $insectOccurrence->specific_id,
                'sub_species_id' => $insectOccurrence->infra_specific_id,
                'dateIdentified' => $insectOccurrence->dateIdentified,
                'creator_id' => $insectOccurrence->last_edit_by,
            ]);

            $insectOccurrence->specific_id = null;
            $insectOccurrence->infra_specific_id = null;

            //new identification
            $insectOccurrence->identifiedBy = $author->id;
        }

        if (array_key_exists("recordedBy", $request_array)) {
            $ids = [];
            foreach ($request_array["recordedBy"] as $aAuthor) {
                $author = InsectAuthor::firstOrCreate(['name' => $aAuthor]);
                array_push($ids, $author->id);
            }
            $insectOccurrence->recordedBy()->sync($ids);
        }

        if (is_array($request_array["eventDate"]) && $request["dateType"] == "range") {
            $insectOccurrence->eventDateStart = $request_array["eventDate"][0];
            $insectOccurrence->eventDateEnd = $request_array["eventDate"][1];
        } else {
            $insectOccurrence->eventDateStart = $request_array["eventDate"];
            $insectOccurrence->eventDateEnd = null;
        }

        //has to update all the other items before doing the ones in the validator
        $insectOccurrence->save();
        $insectOccurrence->update($validator);

        return new InsectOccurrenceResource($insectOccurrence);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectOccurrence  $insectOccurrence
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectOccurrence $insectOccurrence)
    {
        //
    }

    public function speciesSelector(InsectSpecificFilters $filters)
    {
        return InsectSpecificSelectorResource::collection(InsectSpecific::filterBy($filters)->get());
    }

    public function subspeciesSelector(InsectInfraSpecificFilters $filters)
    {
        return InsectInfraSpecificSelectorResource::collection(InsectInfraSpecific::filterBy($filters)->get());
    }

    public function importOccurrence(Request $request)
    {
        Excel::import(new InsectOccurrenceImport($request->projectId), $request->file('excel_file'));
        return response()->json("Import completed", 201);
    }
}
