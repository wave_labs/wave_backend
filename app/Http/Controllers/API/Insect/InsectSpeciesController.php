<?php

namespace App\Http\Controllers\API\Insect;

ini_set('max_execution_time', 300);

use App\Helper;
use App\Http\Controllers\Controller;
use App\Http\QueryFilters\InsectSpeciesFilters;
use App\Http\Requests\InsectSpeciesRequest;
use App\Http\Resources\InsectClassResource;
use App\Http\Resources\InsectFamilyResource;
use App\Http\Resources\InsectGenusResource;
use App\Http\Resources\InsectInfraOrderResource;
use App\Http\Resources\InsectInfraSpecificResource;
use App\Http\Resources\InsectKingdomResource;
use App\Http\Resources\InsectOrderResource;
use App\Http\Resources\InsectOriginalNameResource;
use App\Http\Resources\InsectPhylumResource;
use App\Http\Resources\InsectSpeciesResource;
use App\Http\Resources\InsectSpecificResource;
use App\Http\Resources\InsectSubFamilyResource;
use App\Http\Resources\InsectSubGenusResource;
use App\Http\Resources\InsectSubOrderResource;
use App\Http\Resources\InsectSuperFamilyResource;
use App\Http\Resources\InsectTribeResource;
use App\Imports\InsectTaxonImport;
use App\InsectClass;
use App\InsectFamily;
use App\InsectGenus;
use App\InsectInfraOrder;
use App\InsectInfraSpecific;
use App\InsectKingdom;
use App\InsectOrder;
use App\InsectOriginalName;
use App\InsectPhylum;
use App\InsectSpecies;
use App\InsectSpecific;
use App\InsectSubFamily;
use App\InsectSubGenus;
use App\InsectSubOrder;
use App\InsectSuperFamily;
use App\InsectTribe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class InsectSpeciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return InsectKingdomResource::collection(InsectKingdom::latest()->paginate(10));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsectSpeciesRequest $request)
    {
        if ($request->taxonRank == "Kingdom") {
            $validator = $request->validated();
            return new InsectKingdomResource(InsectKingdom::create($validator));
        } elseif ($request->taxonRank == "Phylum") {
            $validator = $request->validated();
            return new InsectPhylumResource(InsectPhylum::create($validator));
        } elseif ($request->taxonRank == "Class") {
            $validator = $request->validated();
            return new InsectClassResource(InsectClass::create($validator));
        } elseif ($request->taxonRank == "Order") {
            $validator = $request->validated();
            return new InsectOrderResource(InsectOrder::create($validator));
        } elseif ($request->taxonRank == "Suborder") {
            $validator = $request->validated();
            return new InsectSubOrderResource(InsectSubOrder::create($validator));
        } elseif ($request->taxonRank == "Infraorder") {
            $validator = $request->validated();
            return new InsectInfraOrderResource(InsectInfraOrder::create($validator));
        } elseif ($request->taxonRank == "Superfamily") {
            $validator = $request->validated();
            return new InsectSuperFamilyResource(InsectSuperFamily::create($validator));
        } elseif ($request->taxonRank == "Family") {
            $validator = $request->validated();
            return new InsectFamilyResource(InsectFamily::create($validator));
        } elseif ($request->taxonRank == "Subfamily") {
            $validator = $request->validated();
            return new InsectSubFamilyResource(InsectSubFamily::create($validator));
        } elseif ($request->taxonRank == "Tribe") {
            $validator = $request->validated();
            return new InsectTribeResource(InsectTribe::create($validator));
        } elseif ($request->taxonRank == "Genus") {
            $validator = $request->validated();
            return new InsectGenusResource(InsectGenus::create($validator));
        } elseif ($request->taxonRank == "Subgenus") {
            $validator = $request->validated();
            return new InsectSubGenusResource(InsectSubGenus::create($validator));
        } elseif ($request->taxonRank == "Species") {
            $validator = $request->validated();
            $specie = new InsectSpecificResource(InsectSpecific::create($validator));
            $specie->originalName()->save(new InsectOriginalName($validator));
            return $specie;
        } elseif ($request->taxonRank == "Subspecies") {
            $validator = $request->validated();
            $subspecie = new InsectInfraSpecificResource(InsectInfraSpecific::create($validator));
            $subspecie->originalName()->save(new InsectOriginalName($validator));
            return $subspecie;
        }

        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectSpecies  $insectSpecies
     * @return \Illuminate\Http\Response
     */
    public function show(InsectSpecies $insect_specy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectSpecies  $insectSpecies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectSpecies $insect_specy)
    {
        return $request;
    }

    public function updateTaxon($taxonRank, $id, InsectSpeciesRequest $data)
    {
        if ($taxonRank == "Kingdom") {
            $taxon = InsectKingdom::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Phylum") {
            $taxon = InsectPhylum::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Class") {
            $taxon = InsectClass::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Order") {
            $taxon = InsectOrder::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Suborder") {
            $taxon = InsectSubOrder::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Infraorder") {
            $taxon = InsectInfraOrder::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Superfamily") {
            $taxon = InsectSuperFamily::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Family") {
            $taxon = InsectFamily::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Subfamily") {
            $taxon = InsectSubFamily::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Tribe") {
            $taxon = InsectTribe::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Genus") {
            $taxon = InsectGenus::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Subgenus") {
            $taxon = InsectSubGenus::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
        } elseif ($taxonRank == "Species") {
            $taxon = InsectSpecific::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
            $taxon->originalName->where([['infra_specific_id', '=', null], ['specific_id', '=', $taxon->id]])->update(
                [
                    'originalNameGenus' => $validator['originalNameGenus'],
                    'originalNameSubGenus' => $validator['originalNameSubGenus'],
                    'originalNameSpecificEpithet' => $validator['originalNameSpecificEpithet'],
                    'originalNameInfraSpecificEpithet' => $validator['originalNameInfraSpecificEpithet'],
                    'originalNameDate' => $validator['originalNameDate'],
                    'originalNameReference_id' => $validator['originalNameReference_id'],
                    'originalNameTaxonRank' => $validator['originalNameTaxonRank'],
                    'originalNameTaxonRemarks' => $validator['originalNameTaxonRemarks'],
                    'originalNameLanguage' => $validator['originalNameLanguage'],
                    'originalNameRights' => $validator['originalNameRights'],
                    'originalNameLicense' => $validator['originalNameLicense'],
                    'originalNameInstitutionID' => $validator['originalNameInstitutionID'],
                    'originalNameInstitutionCode' => $validator['originalNameInstitutionCode'],
                    'originalNameRightSholder' => $validator['originalNameRightSholder'],
                    'originalNameAccessRights' => $validator['originalNameAccessRights'],
                    'originalNameSource' => $validator['originalNameSource'],
                    'originalNameDatasetID' => $validator['originalNameDatasetID'],
                    'originalNameDatasetName' => $validator['originalNameDatasetName']
                ]
            );
        } elseif ($taxonRank == "Subspecies") {
            $taxon = InsectInfraSpecific::findOrFail($id);
            $validator = $data->validated();
            $taxon->update($validator);
            $taxon->originalName->where('infra_specific_id', '=', $taxon->id)->update(
                [
                    'originalNameGenus' => $validator['originalNameGenus'],
                    'originalNameSubGenus' => $validator['originalNameSubGenus'],
                    'originalNameSpecificEpithet' => $validator['originalNameSpecificEpithet'],
                    'originalNameInfraSpecificEpithet' => $validator['originalNameInfraSpecificEpithet'],
                    'originalNameDate' => $validator['originalNameDate'],
                    'originalNameReference_id' => $validator['originalNameReference_id'],
                    'originalNameTaxonRank' => $validator['originalNameTaxonRank'],
                    'originalNameTaxonRemarks' => $validator['originalNameTaxonRemarks'],
                    'originalNameLanguage' => $validator['originalNameLanguage'],
                    'originalNameRights' => $validator['originalNameRights'],
                    'originalNameLicense' => $validator['originalNameLicense'],
                    'originalNameInstitutionID' => $validator['originalNameInstitutionID'],
                    'originalNameInstitutionCode' => $validator['originalNameInstitutionCode'],
                    'originalNameRightSholder' => $validator['originalNameRightSholder'],
                    'originalNameAccessRights' => $validator['originalNameAccessRights'],
                    'originalNameSource' => $validator['originalNameSource'],
                    'originalNameDatasetID' => $validator['originalNameDatasetID'],
                    'originalNameDatasetName' => $validator['originalNameDatasetName']
                ]
            );
        }
        return response()->json(null, 200);
    }

    public function deleteTaxon(Request $request)
    {
        if ($request->taxonRank == "Kingdom") {
            InsectKingdom::destroy($request->id);
        } elseif ($request->taxonRank == "Phylum") {
            InsectPhylum::destroy($request->id);
        } elseif ($request->taxonRank == "Class") {
            InsectClass::destroy($request->id);
        } elseif ($request->taxonRank == "Order") {
            InsectOrder::destroy($request->id);
        } elseif ($request->taxonRank == "Suborder") {
            InsectSubOrder::destroy($request->id);
        } elseif ($request->taxonRank == "Infraorder") {
            InsectInfraOrder::destroy($request->id);
        } elseif ($request->taxonRank == "Superfamily") {
            InsectSuperFamily::destroy($request->id);
        } elseif ($request->taxonRank == "Family") {
            InsectFamily::destroy($request->id);
        } elseif ($request->taxonRank == "Subfamily") {
            InsectSubFamily::destroy($request->id);
        } elseif ($request->taxonRank == "Tribe") {
            InsectTribe::destroy($request->id);
        } elseif ($request->taxonRank == "Genus") {
            InsectGenus::destroy($request->id);
        } elseif ($request->taxonRank == "Subgenus") {
            InsectSubGenus::destroy($request->id);
        } elseif ($request->taxonRank == "Species") {
            InsectSpecific::destroy($request->id);
        } elseif ($request->taxonRank == "Subspecies") {
            InsectInfraSpecific::destroy($request->id);
        }
        return response()->json(null, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectSpecies  $insectSpecies
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectSpecies $insect_specy)
    {
        $insect_specy->delete();

        return response()->json(null, 204);
    }

    public function importTaxon(Request $request)
    {
        Excel::import(new InsectTaxonImport, $request->file('excel_file'));
        return response()->json("All good", 201);
    }

    public function success()
    {
        return back()->with('success', 'Thanks for contacting us!');
    }
}
