<?php

namespace App\Http\Controllers\API\Insect;

use App\Http\Controllers\Controller;
use App\Http\QueryFilters\InsectReportFilters;
use App\Http\Requests\InsectReportRequest;
use App\Http\Resources\InsectReportCoordinateResource;
use App\Http\Resources\InsectReportResource;
use App\InsectReport;
use App\InsectZone;
use Illuminate\Http\Request;
use App\Imports\InsectReportImport;
use Maatwebsite\Excel\Facades\Excel;

class InsectReportController extends Controller
{
    public function import_reports(Request $request)
    {
        Excel::import(new InsectReportImport, $request->file('excel_file'));
        return redirect('/')->with('success', 'All good');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(InsectReportFilters $filters)
    {
        return InsectReportResource::collection(InsectReport::filterBy($filters)->with('zone')->with('species')->paginate(10));
    }

    public function selector(InsectReportFilters $filters)
    {
        return InsectReportCoordinateResource::collection(InsectReport::filterBy($filters)->whereNotNull('latitude')->whereNotNull('longitude')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsectReportRequest $request)
    {
        $validator = $request->validated();

        $zone = InsectZone::create($validator);
        $validator['zone_id'] = $zone->id;
        $record = InsectReport::create($validator);
        return new InsectReportResource($record);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectReport  $insectReport
     * @return \Illuminate\Http\Response
     */
    public function show(InsectReport $insectReport)
    {
        return new InsectReportResource($insectReport);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectReport  $insectReport
     * @return \Illuminate\Http\Response
     */
    public function update(InsectReportRequest $request, InsectReport $insectReport)
    {
        $validator = $request->validated();
        $zone = InsectZone::where('region', $validator['region'])
            ->where('province', $validator['province'])
            ->where('locality', $validator['locality'])->first();

        if (!$zone) {
            $zone = InsectZone::create($validator);
        }

        $validator['zone_id'] = $zone->id;
        $insectReport->update($validator);
        return new InsectReportResource($insectReport);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectReport  $insectReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectReport $insectReport)
    {
        $insectReport->delete();

        return response()->json(null, 204);
    }
}
