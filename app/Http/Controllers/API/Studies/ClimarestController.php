<?php

namespace App\Http\Controllers\API\Studies;

use App\Http\Controllers\Controller;
use App\Exports\ClimarestsExport;
use App\Climarest;
use App\ClimarestAreaCoordinates;
use App\ClimarestAreaForm;
use App\ClimarestStakeholdersForm;
use App\Http\Requests\ClimarestRequest;
use App\Http\Resources\ClimarestResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ClimarestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ClimarestResource::collection(Climarest::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClimarestRequest $request)
    {
        $validator = $request->validated();

        DB::beginTransaction();

        try {
            $record = Climarest::create([]);
            $validator['climarest_id'] = $record->id;
            $area = ClimarestAreaForm::store($validator);
            ClimarestAreaCoordinates::store($validator["area"]["protectedArea"], $area->id);
            ClimarestStakeholdersForm::store($validator['stakeholder'], $record->id);
            DB::commit();

            return new ClimarestResource($record);
        } catch (\Throwable $th) {
            DB::rollBack();

            return $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Climarest  $climarest
     * @return \Illuminate\Http\Response
     */
    public function show(Climarest $climarest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Climarest  $climarest
     * @return \Illuminate\Http\Response
     */
    public function edit(Climarest $climarest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Climarest  $climarest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Climarest $climarest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Climarest  $climarest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Climarest $climarest)
    {
        //
    }

    public function indexArea(Climarest $climarest)
    {
        return ClimarestResource::collection(ClimarestAreaForm::paginate(10));
    }

    public function indexAreaStakeholders($index)
    {
        error_log($index);
        return ClimarestResource::collection(ClimarestStakeholdersForm::all());
    }

    public function csvExport(Request $request)
    {
        return (new ClimarestsExport($request))->download('export.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }
}
