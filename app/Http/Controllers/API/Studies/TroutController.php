<?php

namespace App\Http\Controllers\API\Studies;

use App\Exports\TroutExport;
use App\Helper;
use App\Http\Controllers\Controller;
use App\Http\QueryFilters\TroutFilters;
use App\Http\Requests\TroutRequest;
use App\Http\Resources\TroutResource;
use App\Trout;
use App\TroutCoordinate;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class TroutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TroutFilters $filters)
    {
        return TroutResource::collection(Trout::filterBy($filters)->latest()->paginate(10));
    }

    public function selector(TroutFilters $filters)
    {
        return TroutResource::collection(Trout::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TroutRequest $request)
    {
        $validator = $request->validated();


        DB::beginTransaction();
        $record = Trout::create([
            'knowledge' => $validator['knowledge'],
            'nameA' => Arr::get($validator, 'nameA'),
            'nameB' => Arr::get($validator, 'nameB'),
            'nameC' => Arr::get($validator, 'nameC'),
        ]);

        if (array_key_exists('sightings', $validator)) {
            foreach ($validator['sightings'] as $sighting) {
                TroutCoordinate::create([
                    'latitude' => $sighting['latitude'],
                    'longitude' => $sighting['longitude'],
                    'trout_id' => $record->id,
                    'animal' => $sighting['animal'],
                    'stream' => $sighting['stream'],
                    'location' => $sighting['location'],
                    'year' => $sighting['year'],
                ]);
            }
        }



        DB::commit();

        return new TroutResource($record);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FreshwaterPin  $freshwaterPin
     * @return \Illuminate\Http\Response
     */
    public function show(Trout $trout)
    {
        return new TroutResource($trout);
    }

    public function csvExport(Request $request)
    {
        return (new TroutExport($request))->download('export.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FreshwaterPin  $freshwaterPin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trout $trout)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FreshwaterPin  $freshwaterPin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trout $trout)
    {
        $trout->delete();

        return response()->json(null, 204);
    }
}
