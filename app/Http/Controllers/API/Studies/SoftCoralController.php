<?php

namespace App\Http\Controllers\API\Studies;

use App\Exports\SoftCoralExport;
use App\Http\Controllers\Controller;
use App\Http\QueryFilters\SoftCoralFilters;
use App\Http\Requests\SoftCoralRequest;
use App\Http\Resources\SoftCoralResource;
use App\Http\Resources\TroutResource;
use App\SoftCoral;
use App\SoftCoralCoordinate;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class SoftCoralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SoftCoralFilters $filters)
    {
        return TroutResource::collection(SoftCoral::filterBy($filters)->latest()->paginate(10));
    }

    public function selector(SoftCoralFilters $filters)
    {
        return TroutResource::collection(SoftCoral::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SoftCoralRequest $request)
    {
        $validator = $request->validated();


        DB::beginTransaction();
        $record = SoftCoral::create([
            'knowledge' => $validator['knowledge'],
            'name' => Arr::get($validator, 'name'),
        ]);

        if (array_key_exists('coordinates', $validator)) {
            foreach ($validator['coordinates'] as $sighting) {
                SoftCoralCoordinate::create([
                    'latitude' => $sighting['latitude'],
                    'longitude' => $sighting['longitude'],
                    'soft_coral_id' => $record->id,
                    'year' => $sighting['year'],
                    'dimensions' => $sighting['dimensions'],
                    'dimension_change' => $sighting['dimension_change'],
                    'depth' => $sighting['depth'],
                ]);
            }
        }



        DB::commit();

        return new SoftCoralResource($record);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SoftCoral  $softCoral
     * @return \Illuminate\Http\Response
     */
    public function show(SoftCoral $softCoral)
    {
        return new SoftCoralResource($softCoral);
    }

    public function csvExport(Request $request)
    {
        return (new SoftCoralExport($request))->download('export.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SoftCoral  $softCoral
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SoftCoral $softCoral)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SoftCoral  $softCoral
     * @return \Illuminate\Http\Response
     */
    public function destroy(SoftCoral $softCoral)
    {
        $softCoral->delete();

        return response()->json(null, 204);
    }
}
