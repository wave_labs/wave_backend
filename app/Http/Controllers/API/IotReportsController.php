<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\IotReport;
use App\IotDevice;
use App\IotDeviceField;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class IotReportsController extends Controller
{
    public function store(Request $requests)
    {
        //Get the id of the device
        $idDevice = $requests->header('id-device');

        if (!isset($idDevice) || is_null($idDevice)) {
            return response()->json(
                [
                    'msg' => 'Device identifier is required.'
                ],
                400
            );
        }

        try {
            //Get the device
            $device = IotDevice::findOrFail($idDevice);
        } catch (ModelNotFoundException $mnfe) {
            return response()->json(
                [
                    'msg' => 'Invalid device identifier.'
                ],
                400
            );
        }

        $reportedData = json_decode($requests->getContent(), true);

        foreach ($reportedData as $request) {
            //Get the collected date
            $collectedTime = $request['collected_at'] ?? null;

            if ((is_numeric($collectedTime) && (int) $collectedTime == $collectedTime)) {
                //convert the integer into a date
                $collectedTime = date('Y-m-d H:i:s', $collectedTime);
            } else {
                $collectedTime = date('Y-m-d H:i:s');
            }

            //Create the new report
            $report = IotReport::create(
                [
                    'id_device' => $idDevice,
                    'collected_at' => $collectedTime
                ]
            );

            //For each field associeted with the device, there will be created one insertion on DB
            foreach ($device->fields as $field) {

                //Verify if the key of the field is set on the request
                if (isset($request[$field->pivot->key])) {
                    //save the field value on DB
                    IotDeviceField::create(
                        [
                            'value' => $request[$field->pivot->key],
                            'id_report' => $report->id,
                            'id_type' => $field->id
                        ]
                    );
                }
            }
        }

        return response()->json(
            [
                'msg' => 'Reports inserted with success.'
            ],
            201
        );
    }


    public function index()
    {
        $report = IotReport::orderBy('id', 'desc')->first();

        $currentReport = [
            "id" => $report->id,
            "id_device" => $report->id_device,
            "name_device" => $report->device->name,
            "collected_at" => $report->collected_at,
        ];

        $fields = [];
        foreach ($report->fields as $field) {
            $currentField = [
                'type' => $field->type->name,
                'value' => $field->value
            ];

            array_push($fields, $currentField);
        }

        $currentReport['fields'] = $fields;

        return $currentReport;
    }
}
