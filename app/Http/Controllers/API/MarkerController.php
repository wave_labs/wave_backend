<?php

namespace App\Http\Controllers\API;

use App\Exports\MarkerCoordinatesExport;
use App\Exports\MarkerExport;
use App\Http\Controllers\Controller;
use App\Http\QueryFilters\MarkerFilters;
use App\Http\Requests\StoreMarkerRequest;
use App\Http\Resources\MarkerResource;
use App\Marker;

use Illuminate\Http\Request;
use DB;

class MarkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = MarkerFilters::hydrate($request->query());
        return MarkerResource::collection(Marker::filterBy($filters)->latest()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMarkerRequest $request)
    {
        $validator = $request->validated();

        DB::beginTransaction();
        $marker = Marker::create($validator);
        $marker->storeCreatureSize($validator['size']);
        $marker->storeResults($validator['form']);
        $marker->storeActivity($validator['activities'], $validator['coordinates']);
        DB::commit();

        return new MarkerResource($marker);
    }

    public function csvExport(Request $request)
    {
        return (new MarkerExport($request))->download('markers.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Marker  $marker
     * @return \Illuminate\Http\Response
     */
    public function show(Marker $marker)
    {
        return new MarkerResource($marker);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Marker  $marker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marker $marker)
    {
        $marker->delete();

        return response()->json(null, 204);
    }
}
