<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\IotDeviceState;
use App\Abundance;
use App\Http\Resources\IotDeviceStateResource;
//use App\Http\Resources\IotDeviceStateDataResource;
use App\Http\QueryFilters\IotDeviceStateFilters;
use App\Http\Resources\AbundanceResource;
use Storage;

class IoTDeviceStateController extends Controller
{

    function __construct()
    {
        /*$this->middleware('permission:iotDeviceType-list', ['only' => ['index, show']]);
        $this->middleware('permission:iotDeviceType-create', ['only' => ['store']]);
        $this->middleware('permission:iotDeviceType-update', ['only' => ['update']]);
        $this->middleware('permission:iotDeviceType-delete', ['only' => ['destroy']]);*/ }

    /**
     * Display a listing of the iotDeviceType for selection.
     *
     * @return App\Http\Resources\IotDeviceStateResource
     */
    public function selector(Request $request)
    {
        $filters = IotDeviceStateFilters::hydrate($request->query());
        return IotDeviceStateResource::collection(IotDeviceState::filterBy($filters)->get());
    }
}
