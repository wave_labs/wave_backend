<?php

namespace App\Http\Controllers\API;

use App\AiDetection;
use App\AiLabel;
use App\Creature;
use App\Dive;
use App\LitterCategory;
use App\DivingSpot;
use App\Http\Controllers\Controller;
use App\Http\Resources\AnalyticDive;
use App\Http\Resources\AnalyticDiveDivingSpot;
use App\Http\Resources\AnalyticLitterCategoryResource;
use App\Http\Resources\AnalyticLitterResource;
use App\Http\Resources\AnalyticLitterSpecificCategoryResource;
use App\Http\Resources\AnalyticSightingCreatureResource;
use App\Http\Resources\AnalyticSightingResource;
use App\Http\Resources\AnalyticSightingSpecificCreatureResource;
use App\Http\Resources\CreatureDataResource;
use App\Http\Resources\DivingSpotDataResource;
use App\Litter;
use App\Sighting;
use Illuminate\Http\Request;
use App\Helper;
use Carbon\Carbon;

class AnalyticsController extends Controller
{

    // DIVE REPORTER ANALYTICS
    public function diveCreature(Creature $creature)
    {
        return new CreatureDataResource($creature);
    }

    public function dive(Request $request)
    {
        $users = Helper::getUserFromRequest($request);
        return new AnalyticDive(Dive::all(), $users);
    }

    public function diveDivingSpot(Request $request)
    {
        $user = Helper::getUserFromRequest($request);
        return new AnalyticDiveDivingSpot(DivingSpot::all(), $request->date,  $request->self ? $user : null);
    }

    public function diveSpecificDivingSpot(DivingSpot $divingSpot)
    {
        return new DivingSpotDataResource($divingSpot);
    }

    // WHALE REPORTER ANALYTICS
    public function sighting(Request $request)
    {
        $users = Helper::getUserFromRequest($request);
        return new AnalyticSightingResource(Sighting::all(), $users);
    }

    public function sightingCreature(Request $request)
    {
        $users = Helper::getUserFromRequest($request);
        return new AnalyticSightingCreatureResource(Sighting::all(), $request->date, $users);
    }

    public function sightingSpecificCreature(Request $request, Creature $creature)
    {
        return new AnalyticSightingSpecificCreatureResource($creature, $request->date);
    }

    // LITTER REPORTER ANALYTICS
    public function litter(Request $request)
    {
        $users = Helper::getUserFromRequest($request);
        return new AnalyticLitterResource(Litter::all(), $request->source, $users);
    }

    public function litterCategory(Request $request)
    {
        $users = Helper::getUserFromRequest($request);
        return new AnalyticLitterCategoryResource(Litter::all(), $request->date, $users);
    }

    public function litterSpecificCategory(Request $request, LitterCategory $category)
    {
        return new AnalyticLitterSpecificCategoryResource($category, $request->date);
    }

    // AI NET STATISTICS
    public function aiAccuracy(Request $request)
    {
        $labels =  AiLabel::whereHas('models', function ($query) {
            $query->whereName('MegafaunaNet');
        })->get();
        $response = [];

        foreach ($labels as $label) {
            $detections = AiDetection::whereAiLabelId($label->id)->get();
            $sum = $detections->sum('accuracy');
            $response[$label->name] = round($sum / max(sizeof($detections), 1), 2);
        }

        return $response;
    }

    public function aiFrequency(Request $request)
    {
        $date = Carbon::now()->firstOfMonth()->subMonths(5);
        $response = [];

        for ($i = 0; $i <= 5; $i++) {
            $startOfMonth = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
            $lastOfMonth = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();

            $detections = AiDetection::whereBetween('created_at', [$startOfMonth, $lastOfMonth])->count();
            $response[(int) $date->month] = $detections;
            $date->addMonth();
        }

        return $response;
    }
}
