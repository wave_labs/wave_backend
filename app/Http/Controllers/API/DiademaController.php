<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Diadema;
use App\Exports\DiademaExport;
use App\Http\QueryFilters\DiademaFilters;
use App\Http\Requests\DiademaRequest;
use App\Http\Resources\DiademaResource;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class DiademaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = DiademaFilters::hydrate($request->query());
        return DiademaResource::collection(Diadema::filterBy($filters)->paginate(10));
    }

    public function selector(Request $request)
    {
        $filters = DiademaFilters::hydrate($request->query());
        return DiademaResource::collection(Diadema::filterBy($filters)->get());
    }

    public function csvExport(Request $request)
    {
        return Excel::download(new DiademaExport($request), 'export.xlsx');
        // return (new DiademaExport($request))->download('export.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DiademaRequest $request)
    {
        $validator = $request->validated();

        $record = Diadema::create($validator);

        return new DiademaResource($record);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Diadema  $diadema
     * @return \Illuminate\Http\Response
     */
    public function show(Diadema $diadema)
    {
        return new DiademaResource($diadema);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Diadema  $diadema
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Diadema $diadema)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Diadema  $diadema
     * @return \Illuminate\Http\Response
     */
    public function destroy(Diadema $diadema)
    {
        //
    }
}
