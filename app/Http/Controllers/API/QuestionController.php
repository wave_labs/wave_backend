<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Question;
use App\Form;
use Illuminate\Http\Request;
use App\Http\Resources\QuestionResource;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($form)
    {
        $id = Form::where('form', $form)->value('id');

        return QuestionResource::collection(Question::where('form_id', $id)->paginate(10));
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Dive
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        return new QuestionResource($question);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $question->update($request->all());    

        return new QuestionResource($question);
    }
}
