<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Capsule;
use App\Http\Resources\CapsuleResource;
use App\Http\QueryFilters\CapsuleFilters;
use Auth;
use Storage;

class CapsuleController extends Controller
{


    function __construct()
    {
        $this->middleware('permission:capsule-list', ['only' => ['index, show']]);
        $this->middleware('permission:capsule-create', ['only' => ['store']]);
        $this->middleware('permission:capsule-update', ['only' => ['update']]);
        $this->middleware('permission:capsule-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the capsule.
     *
     * @return App\Http\Resources\CapuseResource
     */
    public function index(Request $request)
    {
        $filters = CapsuleFilters::hydrate($request->query());
        return CapsuleResource::collection(Capsule::filterBy($filters)->paginate(10));
    }

    /**
     * Store a newly created capsule in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\CapuseResource
     */
    public function store(Request $request)
    {
        $file = $request->file('file');

        $capsule = Capsule::create($request->except(['file']));

        if ($file) {
            $link = 'capsule/' . $capsule->id . '.' . $file->getClientOriginalExtension();
            Storage::put('public/' . $link, file_get_contents($file->getRealPath()));
            $capsule->image_link = $link;
            $capsule->save();
        }

        return new CapsuleResource($capsule);
    }

    /**
     * Display the specified capsule.
     *
     * @param  Capsule  $capsule
     * @return App\Http\Resources\CapuseResource
     */
    public function show(Capsule $capsule)
    {
        return new CapsuleResource($capsule);
    }

    /**
     * Update the specified capsule in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Capsule  $capsule
     * @return App\Http\Resources\CapuseResource
     */
    public function update(Request $request, Capsule $capsule)
    {
        $file = $request->file('file');
        $capsule->update($request->except(['file']));

        if ($file) {
            Storage::delete('public/' . $capsule->link);
            $link = 'capsule/' . $capsule->id . '.' . $file->getClientOriginalExtension();
            Storage::put('public/' . $link, file_get_contents($file->getRealPath()));
            $capsule->image_link = $link;
            $capsule->save();
        }

        return new CapsuleResource($capsule);
    }

    /**
     * Remove the specified capsule from storage.
     *
     * @param  Capsule  $capsule
     * @return App\Http\Resources\CapuseResource
     */
    public function destroy(Capsule $capsule)
    {
        $capsule->delete();

        return response()->json(null, 204);
    }
}
