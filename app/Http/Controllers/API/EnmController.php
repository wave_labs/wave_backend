<?php

namespace App\Http\Controllers\API;

use App\CopernicusProduct;
use App\Creature;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;
use Illuminate\Support\Facades\Log;

use App\Enm;
use App\EnmEnvironmentDataSource;
use App\EnmModel;
use App\EnmOcurrenceDataSource;
use App\EnmPrediction;
use App\Exports\EnmSightingExport;
use App\Helper;
use App\Http\QueryFilters\SightingFilters;
use App\Http\Resources\EnmOcurrenceResource;
use App\Http\Resources\EnmResource;
use App\Sighting;
use Carbon\Carbon;

class EnmController extends Controller
{

    public function uploadOcurrenceData(Request $request)
    {
        $referenceId = uniqid();
        $request->file('file')->storeAs(
            'enm/data',
            $referenceId . '.csv',
            'storage'
        );

        $command = Enm::runProcess(
            "importData.r",
            [$referenceId, $request->separator, $request->latColumn, $request->lonColumn]
        );

        if ($command->isSuccessful()) {
            if ($request->enm) {
                $enm = Enm::find($request->enm);
                Storage::disk('storage')->delete('enm/data/' . $enm->referenceId . ".csv");
                $data = Enm::prepareJson($command->getOutput());
                $enm->creature_id = null;
                $enm->referenceId = $referenceId;
            } else {
                $data = Enm::prepareJson($command->getOutput());
                $enm = Enm::createEnm($referenceId, $request->header('Authorization'));
                $enm->creature_id = null;
            }
            $source = EnmOcurrenceDataSource::whereName('upload')->value('id');
            $enm->enm_ocurrence_data_source_id  = $source;
            $enm->save();
            return Enm::getResponse($enm, $data, true);
        } else {
            //return response()->json(['errors' => $command->getErrorOutput()], 422);
            return response()->json(['errors' => Enm::decodeErrors($command->getErrorOutput())], 422);
        }
    }

    public function getOcurrenceDataFromPipeline(Request $request)
    {
        if ($request->enm) {
            $enm = Enm::find($request->enm);
        } else {
            $enm = Enm::createEnm(uniqid(), $request->header('Authorization'));
        }

        $creature = Creature::find($request->creature);

        $command = Enm::runProcess(
            "getOcurrenceDataFromPipeline.r",
            [$enm->referenceId, $request->source, $creature->name_scientific]
        );

        if ($command->isSuccessful()) {
            Helper::printToConsole($command->getOutput());
            $data = Enm::prepareJson($command->getOutput());
            $source = EnmOcurrenceDataSource::whereName('gbif')->value('id');
            $enm->enm_ocurrence_data_source_id  = $source;
            $enm->creature_id = $request->creature;
            $enm->save();

            return Enm::getResponse($enm, $data, true);
        } else {
            Log::error($command->getErrorOutput());
            $decodedError = Enm::decodeErrors($command->getErrorOutput());

            return response()->json(['errors' => $decodedError], 422);
        }
    }

    public function uploadEnvironmentalData(Enm $Enm, Request $request)
    {
        File::makeDirectory(storage_path('enm/data/envs/' .  $Enm->referenceId));

        foreach ($request->file('files') as $key => $file) {
            $extension = $file->getClientOriginalExtension();
            $file->storeAs(
                'enm/data/envs/' . $Enm->referenceId,
                $key . "." . $extension,
                'storage'
            );

            $command = Enm::runProcess(
                "uploadEnvData.r",
                [$Enm->referenceId, $extension, $key]
            );
        }

        $source = EnmEnvironmentDataSource::whereName('upload')->value('id');
        $Enm->enm_environment_data_source_id  = $source;
        $Enm->save();

        return "true";
    }

    public function getCopernicusData(Enm $Enm, Request $request)
    {

        $date = Carbon::parse($request->date);
        $month = (floor(log10($date->month) + 1) == 1) ? ("0" . $date->month) : $date->month; //format month to 2 digits (eg. 01)
        $copernicusProduct = CopernicusProduct::find($request->copernicusProduct);
        $basePath = storage_path("copernicus/" . $copernicusProduct->identifier);
        if (file_exists($basePath . "/" . $date->year . "/" .  $month)) {
            $source = EnmEnvironmentDataSource::whereName('copernicus')->value('id');
            $Enm->enm_environment_data_source_id  = $source;
            $Enm->environmentalDataPath = "copernicus/" . $copernicusProduct->identifier . "/" . $date->year . "/" .  $month;
            $Enm->save();
            return response()->json(null, 204);
        } else return response()->json(['errors' => ["Currently Wave does not have data for that product and date"]], 422);
    }

    public function deleteDuplicates(Enm $Enm)
    {
        $command = "Rscript ../storage/enm/removeDups.r " . $Enm->referenceId;

        $data = Enm::exeCommand($command);
        return Enm::getResponse($Enm, $data);
    }

    public function deleteRecord(Enm $Enm, $record)
    {
        $command = "Rscript ../storage/enm/removeRecord.r " . $Enm->referenceId . ' ' . $record;

        $data = Enm::exeCommand($command);
        return Enm::getResponse($Enm, $data);
    }

    public function limitOcurrenceNumber(Enm $Enm, Request $request)
    {
        $command = "Rscript ../storage/enm/limitOcurrenceNumber.r " . $Enm->referenceId . ' ' . $request->limit;

        $data = Enm::exeCommand($command);
        return Enm::getResponse($Enm, $data);
    }

    public function cropGeographicExtent(Enm $Enm, Request $request)
    {
        $errors = [];
        $command = Enm::runProcess(
            "cropGeographicExtent.r",
            [$Enm->referenceId, $request->latMin, $request->lonMin,  $request->latMax, $request->lonMax]
        );

        if ($command->isSuccessful()) {
            $data = Enm::prepareJson($command->getOutput());
            return Enm::getResponse($Enm, $data);
        } else {
            Log::error($command->getErrorOutput());
            $decodedError  = Enm::decodeErrors($command->getErrorOutput());
            if (!in_array($decodedError, $errors)) {
                array_push($errors,  $decodedError);
            }
            return response()->json(['errors' => $errors], 422);
        }
    }

    public function queryOcurrenceData(Request $request)
    {
        $filters = SightingFilters::hydrate($request->query());
        $data = EnmOcurrenceResource::collection(Sighting::filterBy($filters)->whereCreatureId($request->creature)->take(1000)->get());

        if (count($data) == 0) {
            return response()->json(['message' => 'There are no records for that query.'], 422);
        }
        $filename = uniqid();
        (new EnmSightingExport($data))->store(
            'enm/data/' . $filename . '.csv',
            'storage'
        );
        if ($request->enm) {
            $enm = Enm::find($request->enm);
            Storage::disk('storage')->delete('enm/data/' . $enm->referenceId . ".csv");
            $enm->referenceId = $filename;
            $enm->save();
        } else {
            $enm = Enm::createEnm($filename, $request->header('Authorization'));
        }
        $source = EnmOcurrenceDataSource::whereName('wave')->value('id');
        $enm->enm_ocurrence_data_source_id  = $source;
        $enm->creature_id = $request->creature;
        $enm->save();

        $dataWithId = Enm::addIdToJson($data);
        return Enm::getResponse($enm, $dataWithId, true);
    }

    public function getEnvImage(Request $request)
    {
        return Enm::getImage('enm/data/images/' . $request->image . ".png");
    }

    public function getPredictionImage(Request $request)
    {
        return Enm::getImage('enm/output/' . $request->image);
    }

    public function createPrediction(Enm $Enm, Request $request)
    {

        if (!$Enm->enm_environment_data_source_id) {
            $source = EnmEnvironmentDataSource::whereName('bioclim')->value('id');
            $Enm->enm_environment_data_source_id  = $source;
            $Enm->save();
        }

        $variables = array($request->bioclimVars);
        $errors = [];

        foreach ($request->models as $model) {
            $modelObj = EnmModel::find($model);

            $command = Enm::runProcess(
                $modelObj->file,
                [$Enm->referenceId, implode(",", $variables), $request->resolution,  $Enm->enm_environment_data_source_id, $request->kfold, $Enm->environmentalDataPath]
            );

            if ($command->isSuccessful()) {
                $Enm->models()->attach($modelObj->id);
                foreach ($modelObj->predictionTypes as $prediction) {
                    EnmPrediction::create([
                        'enm_id' => $Enm->id,
                        'enm_model_prediction_id' => $prediction->id,
                        'path' => $Enm->referenceId . '-' . $modelObj->model . '-' . $prediction->code . '.png',
                    ]);
                }
            } else {
                Log::error($command->getErrorOutput());
                $decodedError  = Enm::decodeErrors($command->getErrorOutput());
                if (!in_array($decodedError, $errors)) {
                    array_push($errors,  $decodedError);
                }
            }
        };
        $Enm->status = count($errors) ? "failed" : "success";
        $Enm->save();
        return count($errors) ?
            response()->json(['errors' => $errors], 422) : new EnmResource($Enm);
    }

    public function setStep(Enm $Enm, Request $request)
    {
        $Enm->step = $request->step;
        $Enm->save();
    }

    public function index(Request $request)
    {
        $user = Helper::getUserFromRequest($request);
        return EnmResource::collection(Enm::fromUsers($user)->latest()->paginate(3));
    }

    public function destroy(Enm $Enm)
    {
        File::delete(storage_path("enm/data/" . $Enm->referenceId . ".csv"));
        foreach ($Enm->predictions as $prediction) {
            File::delete(storage_path("enm/output/" . $prediction->path));
        }

        $Enm->delete();

        return response()->json(null, 204);
    }
}
