<?php

namespace App\Http\Controllers\API;

use App\Acoustic;
use App\Helper;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use ZipArchive;
use Illuminate\Http\Request;

class AcousticController extends Controller
{
    public function softUpdate()
    {
        set_time_limit(0);
        $trainFiles = Acoustic::getSpectrograms('/data/acoustics/spectrograms/train_original');
        $validationFiles = Acoustic::getSpectrograms('/data/acoustics/spectrograms/validation_original');

        Acoustic::storeSpectrograms($trainFiles, '/temp/train/');
        Acoustic::storeSpectrograms($validationFiles, '/temp/validation/');

        Acoustic::zipSpectrograms('app/public/temp/');

        return response()->json([
            'success' => true,
            'message' => 'file spectrograms generated succesfully'
        ], 201);
    }

    public function hardUpdate()
    {
        set_time_limit(0);
        $trainFiles = Acoustic::getSpectrograms('/data/acoustics/spectrograms/train_original');
        $validationFiles = Acoustic::getSpectrograms('/data/acoustics/spectrograms/validation_original');

        Acoustic::hardStoreSpectrograms($trainFiles, '/temp/train/');
        Acoustic::hardStoreSpectrograms($validationFiles, '/temp/validation/');

        Acoustic::zipSpectrograms('app/public/temp/');

        return response()->json([
            'success' => true,
            'message' => 'file spectrograms generated succesfully'
        ], 201);
    }

    public function download()
    {
        try {
            $zip = storage_path('exports/spectrograms.zip');
            return response()->download($zip);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'zip file spectrograms does not exist, generate him first.'
            ], 404);
        }
    }

    public function downloadFromServer()
    {
        set_time_limit(0);
        return Storage::disk('wave-server')->download('/data/acoustics/spectrograms/export.zip');
    }
}
