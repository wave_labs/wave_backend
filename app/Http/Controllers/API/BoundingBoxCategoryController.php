<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\BoundingBoxCategory;
use Illuminate\Http\Request;

class BoundingBoxCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BoundingBoxCategory  $boundingBoxCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BoundingBoxCategory $boundingBoxCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BoundingBoxCategory  $boundingBoxCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(BoundingBoxCategory $boundingBoxCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BoundingBoxCategory  $boundingBoxCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BoundingBoxCategory $boundingBoxCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BoundingBoxCategory  $boundingBoxCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BoundingBoxCategory $boundingBoxCategory)
    {
        //
    }
}
