<?php


namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Http\Requests\StoreLitterReportRequest;
use App\Http\Resources\LitterReportResource;
use App\LitterReport;
use Illuminate\Http\Request;

class LitterReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return LitterReportResource::collection(LitterReport::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLitterReportRequest $request)
    {
        $validator = $request->validated();

        $litterReport = LitterReport::create($validator);

        return new LitterReportResource($litterReport);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LitterReport  $litterReport
     * @return \Illuminate\Http\Response
     */
    public function show(LitterReport $litterReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LitterReport  $litterReport
     * @return \Illuminate\Http\Response
     */
    public function edit(LitterReport $litterReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LitterReport  $litterReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LitterReport $litterReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LitterReport  $litterReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(LitterReport $litterReport)
    {
        //
    }
}
