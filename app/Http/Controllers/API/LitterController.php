<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;
use DB;
use App;
use App\Exports\LitterExport;
use App\Http\Resources\LitterResource;
use App\Http\Resources\LitterCoordResource;
use App\Http\QueryFilters\LitterFilters;
use App\Http\Requests\StoreLitterRequest;
use App\Http\Requests\UpdateLitterRequest;

use App\Litter;
use App\PendingPoint;
use App\LitterPhoto;
use App\Helper;

class LitterController extends Controller
{
    function __construct()
    {
        /*
        $this->middleware('permission:litter-list', ['only' => ['index, show']]);
        $this->middleware('permission:litter-create', ['only' => ['store']]);
        $this->middleware('permission:litter-update', ['only' => ['update']]);
        $this->middleware('permission:litter-delete', ['only' => ['destroy']]);*/ }

    /**
     * Display a listing of the litter.
     *
     * @return App\Http\Resources\LitterResource
     */
    public function index(Request $request)
    {
        $filters = LitterFilters::hydrate($request->query());
        $users = Helper::getUserFromRequest($request);

        return LitterResource::collection(Litter::filterBy($filters)->getPosts($users)->latest()->paginate(10));
    }

    /**
     * Display a listing of all litter coordinates and id
     *
     * @return App\Http\Resources\LitterCoordResource
     */
    public function indexAllCoordinates(Request $request)
    {
        $filters = LitterFilters::hydrate($request->query());
        $user = Helper::getUserFromRequest($request);

        return LitterCoordResource::collection(Litter::filterBy($filters)->getPosts($user)->get());
    }

    public function csvExport(Request $request)
    {
        return (new LitterExport($request))->download('litter.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }


    /**
     * Store a newly created litter in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\LitterResource
     */
    public function store(StoreLitterRequest $request)
    {
        $validator = $request->validated();

        DB::beginTransaction();

        $litter = Litter::create($validator);

        if (array_key_exists('litter_sub_category', $validator)) {
            $litter->litterSubCategory()->attach($validator['litter_sub_category']);
        }


        if (array_key_exists('litter_category', $validator)) {
            $litter->litterCategory()->attach($validator['litter_category']);
        }

        if ($validator['source'] == "dive") {
            $litter->dives()->attach($validator['dive_id']);
        }

        DB::commit();

        //Calculate and update points
        $points = PendingPoint::calculate($request, 'litter', $litter->id);
        //User::updatePoints($points, $request->user_id);

        if (array_key_exists('photo', $validator)) {
            LitterPhoto::store($validator['photo'], $litter->id);
        }

        return new LitterResource($litter);
    }

    /**
     * Display the specified litter.
     *
     * @param  App\Litter
     * @return App\Http\Resources\LitterResource
     */
    public function show(Litter $litter)
    {
        return new LitterResource($litter);
    }

    public function nextAndPrior(Litter $litter)
    {
        $previous = Litter::where('id', '<', $litter->id)->orderByDesc('id')->first();
        $next = Litter::where('id', '>', $litter->id)->orderBy('id')->first();

        return response()->json([
            'success' => true,
            'previous' => $previous ? new LitterResource($previous) : null,
            'next' => $next ? new LitterResource($next) : null,
        ], 200);
    }

    /**
     * Display the specified litter.
     *
     * @param  App\Litter
     * @return App\Http\Resources\LitterResource
     */
    public function confirm(Request $request, Litter $litter)
    {
        $validator = Validator::make($request->all(), [
            'photo' => 'required|image',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 400);
        }

        $litter->collected = 1;
        $litter->save();

        LitterPhoto::store($request->photo, $litter->id);

        return new LitterResource($litter);
    }

    /**
     * Update the specified litter in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Litter
     * @return App\Http\Resources\LitterResource
     */
    public function update(UpdateLitterRequest $request, Litter $litter)
    {
        $validator = $request->validated();

        DB::beginTransaction();

        $litter->update($validator);

        if (array_key_exists('litter_sub_category', $validator)) {
            $litter->litterSubCategory()->detach();
            $litter->litterSubCategory()->attach($validator['litter_sub_category']);
        }

        if (array_key_exists('litter_category', $validator)) {
            $litter->litterCategory()->detach();
            $litter->litterCategory()->attach($validator['litter_category']);
        }

        if ($request->resetCategory) {
            $litter->litterSubCategory()->detach();
            $litter->litterCategory()->detach();
        }

        if ($request->hasFile('photo')) {
            LitterPhoto::updatePhoto($validator['photo'], $litter->id);
        }

        DB::commit();

        return response()->json([
            'success' => true,
            'data' => new LitterResource($litter),
            'message' => Lang::get('messages.litter.updateSuccess'),
        ], 200);
    }

    /**
     * Remove the specified litter from storage.
     *
     * @param  App\Litter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Litter $litter)
    {
        $litter->delete();

        return response()->json([
            'status' => 'success',
        ], 200);
    }
}
