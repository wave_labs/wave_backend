<?php

namespace App\Http\Controllers\API;

use App\Exports\SightingExport;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;

use App\Http\Requests\StoreSightingRequest;
use App\Http\Resources\SightingResource;
use App\Http\Resources\SightingWithDataResource;
use App\Http\Resources\SightingCoordResource;
use App\Http\QueryFilters\SightingFilters;
use App\Http\Requests\UpdateSightingRequest;
use App\Http\Resources\TripResource;

use App\Sighting;
use App\PendingPoint;
use App\Helper;
use App\Trip;

class SightingController extends Controller
{
    function __construct()
    { }

    /**
     * Display a listing of the sighting.
     *
     * @return App\Http\Resources\SightingResource
     */
    public function index(Request $request)
    {
        $filters = SightingFilters::hydrate($request->query());
        $users = Helper::getUserFromRequest($request);

        return SightingResource::collection(Sighting::filterBy($filters)->getPosts($users)->paginate(10));
    }

    public function csvExport(Request $request)
    {
        return (new SightingExport($request))->download('file.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    /**
     * Display a listing of all sightings coordinates and id
     *
     * @return App\Http\Resources\SightingCoordResource
     */
    public function indexAllCoordinates(Request $request)
    {
        $filters = SightingFilters::hydrate($request->query());

        $users = Helper::getUserFromRequest($request);

        if (Auth::user() && Auth::user()->can("user-list")) {
            return SightingCoordResource::collection(Sighting::filterBy($filters)->getPosts($users)->get());
        } else {

            return SightingCoordResource::collection(Sighting::filterBy($filters)->getPosts($users)->get());
            //TODO: REMOVE THIS UPPER LINE

            $from = Carbon::now()->subDays(14);
            $to = Carbon::now();

            return SightingCoordResource::collection(Sighting::filterBy($filters)
                ->whereBetween('date', array($from, $to)) //unautherized users can only access sightings from two weeks before
                ->whereNotNull('latitude')
                ->whereNotNull('longitude')
                ->get());
        }
    }

    /**
     * Store a newly created sighting in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\SightingResource
     */
    public function store(StoreSightingRequest $request)
    {
        $validator = $request->validated();

        $sighting = Sighting::create($validator);

        //Calculate and update points
        $points = PendingPoint::calculate($request, 'sighting', $sighting->id);
        //User::updatePoints($points, $request->user_id);

        return new SightingResource($sighting);
    }

    /**
     * Display the specified sighting.
     *
     * @param  App\Sighting
     * @return App\Http\Resources\SightingResource
     */
    public function show(Request $request, Sighting $sighting)
    {
        return new SightingWithDataResource($sighting);
    }

    /**
     * Display the specified sighting.
     *
     * @param  App\Sighting
     * @return App\Http\Resources\SightingResource
     */
    public function nextAndPrior(Request $request, Sighting $sighting)
    {
        $user = null;
        //$user = User::getCurrentUser($request);
        $previous = Sighting::where('id', '<', $sighting->id)->fromUser($user)->orderByDesc('id')->first();
        $next = Sighting::where('id', '>', $sighting->id)->fromUser($user)->orderBy('id')->first();

        return response()->json([
            'success' => true,
            'previous' => $previous ? new SightingResource($previous) : null,
            'next' => $next ? new SightingResource($next) : null,
        ], 200);
    }

    /**
     * Display the specified sighting.
     *
     * @param  App\Sighting
     * @return App\Http\Resources\TripResource
     */
    public function indexTrip(Sighting $sighting)
    {
        $trip = Trip::find($sighting->trip_id);
        if ($trip) {
            if (count($trip->positions) > 0) {
                return response()->json([
                    'success' => true,
                    'data' => new TripResource($trip)
                ], 200);
            }
        }

        return response()->json([
            'success' => false,
        ], 400);
    }

    /**
     * Update the specified sighting in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Sighting
     * @return App\Http\Resources\SightingResource
     */
    public function update(UpdateSightingRequest $request, Sighting $sighting)
    {
        $validator = $request->validated();

        $sighting->update($validator);

        return new SightingResource($sighting);
    }

    /**
     * Remove the specified sighting from storage.
     *
     * @param  App\Sighting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sighting $sighting)
    {
        $sighting->delete();

        return response()->json(null, 204);
    }
}
