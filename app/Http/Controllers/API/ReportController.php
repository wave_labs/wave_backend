<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Report;
use App\Http\Resources\ReportResource;


class ReportController extends Controller
{
    /**
     * Display a listing of the question.
     *
     * @return App\Http\Resources\QuestionResource
     */
    public function index(Request $request)
    {
        return ReportResource::collection(Report::paginate(10));
    }

    /**
     * Store a newly created question in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\QuestionResource
     */
    public function store(Request $request)
    {
        $question = Report::create($request->all());

        return new ReportResource($question);
    }

    /**
     * Display the specified question.
     *
     * @param  App\Report
     * @return App\Http\Resources\QuestionResource
     */
    public function show(Report $question)
    {
        return new ReportResource($question);
    }

    /**
     * Update the specified question in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Report
     * @return App\Http\Resources\QuestionResource
     */
    public function update(Request $request, Report $question)
    {
        $question->update($request->all());

        return new ReportResource($question);
    }

    /**
     * Remove the specified question from storage.
     *
     * @param  App\Report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $question)
    {
        $question->delete();

        return response()->json(null, 204);
    }
}
