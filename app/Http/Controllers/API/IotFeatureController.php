<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\IotFeatureResource;
use App\IotFeature;
use Illuminate\Http\Request;

class IotFeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return IotFeatureResource::collection(IotFeature::paginate(10));
    }

    public function selector(Request $request)
    {
        return IotFeatureResource::collection(IotFeature::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IotFeature  $iotFeature
     * @return \Illuminate\Http\Response
     */
    public function show(IotFeature $iotFeature)
    {
        return new IotFeatureResource($iotFeature);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IotFeature  $iotFeature
     * @return \Illuminate\Http\Response
     */
    public function edit(IotFeature $iotFeature)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IotFeature  $iotFeature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IotFeature $iotFeature)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IotFeature  $iotFeature
     * @return \Illuminate\Http\Response
     */
    public function destroy(IotFeature $iotFeature)
    {
        //
    }
}
