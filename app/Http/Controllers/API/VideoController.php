<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Video;
use Response;

class VideoController extends Controller
{
    public function indexSighting()
    {
        $urls = Video::where('type', "sighting")->select('file', 'url', 'creature_id')->get();
        $count = count($urls);

        return response()->json(
            [
                'success' => true,
                'data' => $urls,
                'metadata' => $count,
            ],
            200
        );
    }

    public function show($class, $index)
    {

        if ($class == "litter" || $class == "sighting" || $class == "dive" || $class == "promo") {
            $file = storage_path('/video/' . $class . '/' . $index . '.mp4');
            return Response::download($file);
        }

        return response()->json(
            [
                'success' => false,
                'message' => 'Invalid image path ' . $class,
            ],
            422
        );
    }
}
