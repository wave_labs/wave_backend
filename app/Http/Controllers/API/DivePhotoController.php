<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image as IMG;

class DivePhotoController extends Controller
{
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        if ($request->file('image')->isValid()) {
            $image = $request->file('image');
            $filename = $request->dive_id . '-' . uniqid() . '.png';

            IMG::make($image)->encode('png', 65)->resize(760, null, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            })->save(storage_path('/uploaded/dive_photo/' . $filename));

            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'File format nor suported'
            ], 422);
        }
    }

    public function show($index) //show a specific photo
    {
        $storagePath = storage_path('/uploaded/dive_photo/index.jpeg');

        return IMG::make($storagePath)->response();
    }
}
