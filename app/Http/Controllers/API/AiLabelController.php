<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\AiLabel;
use App\Http\QueryFilters\AiLabelFilters;
use App\Http\Resources\AiLabelResource;
use Illuminate\Http\Request;

class AiLabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = AiLabelFilters::hydrate($request->query());
        return AiLabelResource::collection(AiLabel::filterBy($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AiLabel  $aiLabel
     * @return \Illuminate\Http\Response
     */
    public function show(AiLabel $aiLabel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AiLabel  $aiLabel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AiLabel $aiLabel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AiLabel  $aiLabel
     * @return \Illuminate\Http\Response
     */
    public function destroy(AiLabel $aiLabel)
    {
        //
    }
}
