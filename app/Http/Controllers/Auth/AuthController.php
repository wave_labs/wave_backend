<?php

namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;

use App\User;
use App\Litter;
use App\Sighting;
use App\Dive;
use App\Helper;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB;
use Illuminate\Support\Facades\Password;
use App\Notifications\UserActivationNotification;
use App\Notifications\UserVerifiedEmailNotification;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResource;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UpdateMeRequest;
use App\Http\Resources\DiveHistoryResource;
use App\Http\Resources\LitterHistoryResource;
use App\Http\Resources\SightingHistoryResource;
use App\Mail\ConfirmEmail;
use App\Mail\PasswordReset;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $validator = $request->validated();

        DB::beginTransaction();

        $validator['password'] = bcrypt($validator['password']);

        $user = User::create($validator);

        //TODO: TAKE OUT ACTIVE AND IS_VERIFIED FROM HERE
        $user->active = 1;
        //$user->is_verified = 1;
        $user->save();

        $user->syncRoles([$validator['role']]);

        DB::commit();

        Mail::to($user->email)->send(new ConfirmEmail($user, $validator['name'])); //change mail

        /*
        //Create Certificates
        if ($validator['userable_type'] == 'UserPerson') {
            if ($request->hasfile('photos')) {
                UserCertificate::store($request, $UserType->id);
            }
        }
        //TODO: SEND EMAIL

        $verification_code = str_random(30); //Generate verification code
        DB::table('user_verifications')->insert(['user_id'=>$user->id,'token'=>$verification_code]);
        $user->notify(new ConfirmEmailNotification($verification_code));
        
        $user->syncRoles([$request['role']]);
        return response()->json([
            'success' => true,
            'message' => 'Thanks for signing up! Please check your email to complete your registration.'
        ]);*/

        return response()->json([
            'success' => true,
            'message' => 'Please verify your email to get access to Wave Labs dashboard'
        ]);
    }


    /**
     * API Verify User
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyUser($token)
    {
        $user = User::where('token', '=', $token)->firstOrFail();

        if($user){
            $user->is_verified = 1;
            $user->save();
            return redirect('/email/verify/'.$token);
        }
        return response()->json([
            'success' => false,
            'message' => 'Was not able to find such account.'
        ]);

        /* $check = DB::table('user_verifications')->where('token', $verification_code)->first();

        if (is_null($check)) {
            //TODO: Send another email
            return response()->json(['success' => false, 'error' => "Verification code is invalid."]);
        }


        $user = User::find($check->user_id);

        if ($user->is_verified == 1) {

            return response()->json([
                'success' => true,
                'message' => 'Account already verified.'
            ]);
        }

        $user->update(['is_verified' => 1]);
        DB::table('user_verifications')->where('token', $verification_code)->delete();

        //notify admins that a user has verfied his email
        $admins = User::role('admin')->get();
        foreach ($admins as $admin) {
            $admin->notify(new UserVerifiedEmailNotification($user));
        }

        return response()->json([
            'success' => true,
            'message' => 'You have successfully verified your email address.'
        ]); */
    }



    /**
     * API Login, on success return JWT Auth token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');


        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make($credentials, $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()], 401);
        }

        $user = User::where('email', $request->email)->first();


        try {
            // attempt to verify the credentials and create a token for the user
            if (!$user->is_verified) {
                return response()->json(['success' => false, 'message' => 'The email is not verified.'], 401);
            }
    
            if (!$token = auth()->attempt($credentials)) {
                return response()->json(['success' => false, 'message' => 'We cant find an account with this credentials. Please make sure you entered the right information and you have verified your email address.'], 404);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'message' => 'Failed to login, please try again.'], 500);
        }

        if (!$user->is_verified) {
            return response()->json(['success' => false, 'message' => 'The email is not verified.'], 401);
        }

        if (!$user->active) {
            return response()->json(['success' => false, 'message' => 'You must wait for our admin to accept your account.'], 401);
        }

        return $this->respondWithToken($token);
    }



    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout()
    {
        try {
            auth()->logout();

            return response()->json(['success' => true, 'message' => "You have successfully logged out."]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }


    /**
     * API Recover Password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recoverPassword(Request $request)
    {
        $user = User::where('email', $request->only('email'))->first();

        if (!$user) {
            $error_message = "Your email address was not found.";
            return response()->json(['success' => false, 'error' => ['email' => $error_message]]);
        }

        try {
            if($user->token){
                Mail::to($user->email)->send(new PasswordReset($user)); 
            }
            else{
                $user->token = str_random(16);
                $user->save();
                Mail::to($user->email)->send(new PasswordReset($user));
            }
        } catch (\Exception $e) {
            //Return with error
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 401);
        }

        return response()->json([
            'success' => true, 'data' => ['message' => 'A reset email has been sent! Please check your email.']
        ]);
    }

    public function showResetForm($token){
        return redirect('/password/reset/'.$token);
    }

    public function resetPassword(Request $request){
        $user = User::where('email', '=', $request->email)->first();
        if($user->token == $request->token){
            $user->password = bcrypt($request->password);
            $user->token = str_random(16);
            $user->save();
            return response()->json([
                'success' => true, 'data' => ['message' => 'Your password has been updated']
            ]);
        }
        //change so it doesnt redirect if it fails
        return response()->json(['success' => false, 'error' => ['You dont have permissions to change this account password']]);
    }

    /**
     * Redirect to social login page 
     *
     * @param string
     * @return response
     */

    public function socialLogin($social)
    {
        return Socialite::driver($social)->redirect();
    }


    /**
     * Handle Social login request
     * @param string
     * @return response
     */
    public function handleProviderCallback($social)
    {
        $userSocial = Socialite::driver($social)->user();
        $user = User::where(['email' => $userSocial->getEmail()])->first();

        if ($user) {
            Auth::login($user);
            //TODO: return to after log in view
        } else {
            $data = [
                'email' => $userSocial->getEmail(),
                'name' => $userSocial->getName(),
            ];
            //TODO: return to register view with data
        }
    }

    /**
     * Activates and notifies a user
     *
     * @param  App\User
     * @return \Illuminate\Http\Response
     */
    public function activateUser(User $user)
    {
        $user->active = !$user->active;
        $user->save();

        if ($user->active) {
            $user->notify(new UserActivationNotification($user));
        }

        if ($user->active) {
            $return  = [
                'success' => true, 'data' => ['message' => 'User successfully activated']
            ];
        } else {
            $return  = [
                'success' => true, 'data' => ['message' => 'User successfully deactivated']
            ];
        }

        return response()->json($return, 200);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth()->user();

        return new UserResource($user);
    }

    public function updateMe(UpdateMeRequest $request)
    {
        $validator = $request->validated();
        $user = User::find($validator['user_id']);

        $user->update($validator);
        $user->userable->update($validator);

        if (array_key_exists('members', $validator) && count($validator['members']) > 0) {
            $syncData = [];
            foreach ($validator['members'] as $el) {
                $newUser = User::where('email', $el)->first();

                $newUser->userable_type = "App\\UserPerson" && array_push($syncData, $newUser->userable_id);
            }

            $user->userable->persons()->sync($syncData);
        }

        $user = User::find($validator['user_id']);

        return new UserResource($user);
    }

    /**
     * Get the account activity.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function history(Request $request)
    {
        $user = auth()->user();

        $response = [];

        if ($request->litter == 'true') {
            $response = LitterHistoryResource::collection(Litter::where('user_id', $user->id)->limit(10)->get());
        } else if ($request->sighting == 'true') {
            $response = SightingHistoryResource::collection(Sighting::where('user_id', $user->id)->limit(10)->get());
        } else if ($request->dive == 'true') {
            $response = DiveHistoryResource::collection(Dive::where('user_id', $user->id)->limit(10)->get());
        }

        return $response;
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'success' => true,
            'user' => new UserResource(auth()->user()),
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer'
            ]
        ]);
    }

    /**
     * Assign Role to user
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    /*
    public function assignRole(User $user, Request $request)
    {   
       $user->syncRoles([$request->input('role')]); 
    }
    */
}
