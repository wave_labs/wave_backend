<?php

namespace App\Http\Controllers;

use App\ClimarestAreaCoordinates;
use Illuminate\Http\Request;

class ClimarestAreaCoordinatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClimarestAreaCoordinates  $climarestAreaCoordinates
     * @return \Illuminate\Http\Response
     */
    public function show(ClimarestAreaCoordinates $climarestAreaCoordinates)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClimarestAreaCoordinates  $climarestAreaCoordinates
     * @return \Illuminate\Http\Response
     */
    public function edit(ClimarestAreaCoordinates $climarestAreaCoordinates)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClimarestAreaCoordinates  $climarestAreaCoordinates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClimarestAreaCoordinates $climarestAreaCoordinates)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClimarestAreaCoordinates  $climarestAreaCoordinates
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClimarestAreaCoordinates $climarestAreaCoordinates)
    {
        //
    }
}
