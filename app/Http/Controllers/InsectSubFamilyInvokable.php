<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectSubFamilyFilters;
use App\Http\Resources\InsectSubFamilyResource;
use App\InsectSubFamily;
use Illuminate\Http\Request;

class InsectSubFamilyInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectSubFamilyFilters $filters)
    {
        return response()->json([
            'data' =>  InsectSubFamily::filterBy($filters)->select('subFamily', 'id')->get()
        ]);
    }
}
