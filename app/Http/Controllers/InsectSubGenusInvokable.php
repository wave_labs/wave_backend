<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectSubGenusFilters;
use App\Http\Resources\InsectSubGenusResource;
use App\InsectSubGenus;
use Illuminate\Http\Request;

class InsectSubGenusInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectSubGenusFilters $filters)
    {
        return response()->json([
            'data' =>  InsectSubGenus::filterBy($filters)->select('subGenus', 'id')->get()
        ]);
    }
}
