<?php

namespace App\Http\Controllers;

use App\InsectSpecific;
use Illuminate\Http\Request;

class InsectSpecificController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectSpecific  $insectSpecific
     * @return \Illuminate\Http\Response
     */
    public function show(InsectSpecific $insectSpecific)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectSpecific  $insectSpecific
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectSpecific $insectSpecific)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectSpecific  $insectSpecific
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectSpecific $insectSpecific)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectSpecific  $insectSpecific
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectSpecific $insectSpecific)
    {
        //
    }
}
