<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Facades\Artisan;

class LaravelController extends Controller
{
 	    
	/**
	* Update server from git
	*
	* @param  Request  $request
	* @return 
	*/
    public function update()
    {   

        $process = new Process('git init');
        $process -> setWorkingDirectory(base_path());
        $process -> run(function ($type, $buffer) {
                echo $buffer . "<br>";
                echo $type . "<br>";
        });


        $process1 = new Process('git remote add origin https://bitbucket.org/aquaticui/server.git');
        $process1 -> setWorkingDirectory(base_path());
        $process1 -> run(function ($type, $buffer) {
                echo $buffer . "<br>";
                echo $type . "<br>";
        });

        // TODO
        // $git_fetch = env('PROXY_SERVER') ? 'git -c "http.proxy=' . env('PROXY_SERVER') . '" fetch --all' : 'git fetch --all';
        // $process2 = new Process( $git_fetch );
        // $process2 -> setWorkingDirectory(base_path());
        // $process2 -> setTimeout(3000);
        // $process2 -> run(function ($type, $buffer) {
        //         echo $buffer . "<br>";
        //         echo $type . "<br>";
        // });

        // $process3 = new Process( 'git reset --hard origin/master');
        // $process3 -> setWorkingDirectory(base_path());
        // $process3 -> run(function ($type, $buffer) {
        //         echo $buffer . "<br>";
        //         echo $type . "<br>";
        // });

        // $this->migrate();
    }

    public function migrate()
    {
        echo "Migration: " . Artisan::call('migrate', ['--force'=> true]);
    }

}
