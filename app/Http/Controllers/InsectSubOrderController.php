<?php

namespace App\Http\Controllers;

use App\InsectSubOrder;
use Illuminate\Http\Request;

class InsectSubOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectSubOrder  $insectSubOrder
     * @return \Illuminate\Http\Response
     */
    public function show(InsectSubOrder $insectSubOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectSubOrder  $insectSubOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectSubOrder $insectSubOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectSubOrder  $insectSubOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectSubOrder $insectSubOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectSubOrder  $insectSubOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectSubOrder $insectSubOrder)
    {
        //
    }
}
