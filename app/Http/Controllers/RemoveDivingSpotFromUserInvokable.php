<?php

namespace App\Http\Controllers;

use App\UserHasDivingSpot;
use Illuminate\Http\Request;

class RemoveDivingSpotFromUserInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $records = UserHasDivingSpot::where('diving_spot_id', $request->divingSpot)->where('user_id', auth()->user()->id)->get();

        foreach ($records as $record) {
            $record->delete();
        }

        return response()->json(null, 204);
    }
}
