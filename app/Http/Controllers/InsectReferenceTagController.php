<?php

namespace App\Http\Controllers;

use App\InsectReferenceTag;
use Illuminate\Http\Request;

class InsectReferenceTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectReferenceTag  $insectReferenceTag
     * @return \Illuminate\Http\Response
     */
    public function show(InsectReferenceTag $insectReferenceTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectReferenceTag  $insectReferenceTag
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectReferenceTag $insectReferenceTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectReferenceTag  $insectReferenceTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectReferenceTag $insectReferenceTag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectReferenceTag  $insectReferenceTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectReferenceTag $insectReferenceTag)
    {
        //
    }
}
