<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectFamilyFilters;
use App\Http\Resources\InsectFamilyResource;
use App\InsectFamily;
use Illuminate\Http\Request;

class InsectFamilyInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectFamilyFilters $filters)
    {
        return response()->json([
            'data' =>  InsectFamily::filterBy($filters)->select('family', 'id')->get()
        ]);
    }
}
