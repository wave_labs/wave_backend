<?php

namespace App\Http\Controllers;

use App\InsectOriginalName;
use Illuminate\Http\Request;

class InsectOriginalNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectOriginalName  $insectOriginalName
     * @return \Illuminate\Http\Response
     */
    public function show(InsectOriginalName $insectOriginalName)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectOriginalName  $insectOriginalName
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectOriginalName $insectOriginalName)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectOriginalName  $insectOriginalName
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectOriginalName $insectOriginalName)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectOriginalName  $insectOriginalName
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectOriginalName $insectOriginalName)
    {
        //
    }
}
