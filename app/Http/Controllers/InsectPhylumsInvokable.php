<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectPhylumsFilters;
use App\Http\Resources\InsectPhylumResource;
use App\InsectPhylum;
use Illuminate\Http\Request;

class InsectPhylumsInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectPhylumsFilters $filters)
    {
        return response()->json([
            'data' =>  InsectPhylum::filterBy($filters)->select('phylum', 'id')->get()
        ]);
    }
}
