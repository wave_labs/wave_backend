<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectClassesFilters;
use App\Http\Resources\InsectClassResource;
use App\InsectClass;
use Illuminate\Http\Request;

class InsectClassesInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectClassesFilters $filters)
    {
        return response()->json([
            'data' =>  InsectClass::filterBy($filters)->select('class', 'id')->get()
        ]);
    }
}
