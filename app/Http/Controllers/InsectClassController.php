<?php

namespace App\Http\Controllers;

use App\InsectClass;
use Illuminate\Http\Request;

class InsectClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectClass  $insectClass
     * @return \Illuminate\Http\Response
     */
    public function show(InsectClass $insectClass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectClass  $insectClass
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectClass $insectClass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectClass  $insectClass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectClass $insectClass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectClass  $insectClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectClass $insectClass)
    {
        //
    }
}
