<?php

namespace App\Http\Controllers;

use App\InsectReferenceHasKeyword;
use Illuminate\Http\Request;

class InsectReferenceHasKeywordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectReferenceHasKeyword  $insectReferenceHasKeyword
     * @return \Illuminate\Http\Response
     */
    public function show(InsectReferenceHasKeyword $insectReferenceHasKeyword)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectReferenceHasKeyword  $insectReferenceHasKeyword
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectReferenceHasKeyword $insectReferenceHasKeyword)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectReferenceHasKeyword  $insectReferenceHasKeyword
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectReferenceHasKeyword $insectReferenceHasKeyword)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectReferenceHasKeyword  $insectReferenceHasKeyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectReferenceHasKeyword $insectReferenceHasKeyword)
    {
        //
    }
}
