<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectInfraOrderFilters;
use App\Http\Resources\InsectInfraOrderResource;
use App\InsectInfraOrder;
use Illuminate\Http\Request;

class InsectInfraOrderInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectInfraOrderFilters $filters)
    {
        return response()->json([
            'data' =>  InsectInfraOrder::filterBy($filters)->select('infraOrder', 'id')->get()
        ]);
    }
}
