<?php

namespace App\Http\Controllers;

use App\Exports\GpsGatewaySatelliteExport;
use App\GpsGatewaySatellite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class GpsGatewaySatelliteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function download(Request $request)
    {
        return Excel::download(new GpsGatewaySatelliteExport($request), 'export.xlsx');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return GpsGatewaySatellite::where('iot_report_id', $request->report_id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GpsGatewaySatellite  $gpsGatewaySatellite
     * @return \Illuminate\Http\Response
     */
    public function show(GpsGatewaySatellite $gpsGatewaySatellite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GpsGatewaySatellite  $gpsGatewaySatellite
     * @return \Illuminate\Http\Response
     */
    public function edit(GpsGatewaySatellite $gpsGatewaySatellite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GpsGatewaySatellite  $gpsGatewaySatellite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GpsGatewaySatellite $gpsGatewaySatellite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GpsGatewaySatellite  $gpsGatewaySatellite
     * @return \Illuminate\Http\Response
     */
    public function destroy(GpsGatewaySatellite $gpsGatewaySatellite)
    {
        //
    }
}
