<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectSpecificFilters;
use App\Http\Resources\InsectSpecificResource;
use App\InsectSpecific;
use Illuminate\Http\Request;

class InsectSpecificInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectSpecificFilters $filters)
    {
        return response()->json([
            'data' =>  InsectSpecific::filterBy($filters)->select('specific', 'id')->get()
        ]);
    }
}
