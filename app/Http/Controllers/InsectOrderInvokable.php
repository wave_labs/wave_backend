<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectOrdersFilters;
use App\Http\Resources\InsectOrderResource;
use App\InsectOrder;
use Illuminate\Http\Request;

class InsectOrderInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectOrdersFilters $filters)
    {
        return response()->json([
            'data' =>  InsectOrder::filterBy($filters)->select('order', 'id')->get()
        ]);
    }
}
