<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Mail\ConfirmEmail;
use App\Mail\PasswordReset;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use JWTAuth;

class MailController extends Controller
{
    public function passwordRecover(Request $request){
        $user = auth()->user();
        return Mail::to('fake@mail.com')->send(new PasswordReset($user));
    }

    public function confirmEmail(Request $request){
        $user = auth()->user();
        return Mail::to('fake@mail.com')->send(new ConfirmEmail($user));
    }
}
