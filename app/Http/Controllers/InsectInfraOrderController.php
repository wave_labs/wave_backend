<?php

namespace App\Http\Controllers;

use App\InsectInfraOrder;
use Illuminate\Http\Request;

class InsectInfraOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectInfraOrder  $insectInfraOrder
     * @return \Illuminate\Http\Response
     */
    public function show(InsectInfraOrder $insectInfraOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectInfraOrder  $insectInfraOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectInfraOrder $insectInfraOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectInfraOrder  $insectInfraOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectInfraOrder $insectInfraOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectInfraOrder  $insectInfraOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectInfraOrder $insectInfraOrder)
    {
        //
    }
}
