<?php

namespace App\Http\Controllers;

use App\IotSection;
use Illuminate\Http\Request;

class IotSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IotSection  $iotSection
     * @return \Illuminate\Http\Response
     */
    public function show(IotSection $iotSection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IotSection  $iotSection
     * @return \Illuminate\Http\Response
     */
    public function edit(IotSection $iotSection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IotSection  $iotSection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IotSection $iotSection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IotSection  $iotSection
     * @return \Illuminate\Http\Response
     */
    public function destroy(IotSection $iotSection)
    {
        //
    }
}
