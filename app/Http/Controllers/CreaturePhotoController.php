<?php

namespace App\Http\Controllers;

use App\CreaturePhoto;
use Illuminate\Http\Request;

class CreaturePhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CreaturePhoto  $creaturePhoto
     * @return \Illuminate\Http\Response
     */
    public function show(CreaturePhoto $creaturePhoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CreaturePhoto  $creaturePhoto
     * @return \Illuminate\Http\Response
     */
    public function edit(CreaturePhoto $creaturePhoto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CreaturePhoto  $creaturePhoto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CreaturePhoto $creaturePhoto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CreaturePhoto  $creaturePhoto
     * @return \Illuminate\Http\Response
     */
    public function destroy(CreaturePhoto $creaturePhoto)
    {
        //
    }
}
