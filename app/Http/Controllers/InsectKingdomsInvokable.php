<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 300);

use App\Http\QueryFilters\InsectKingdomsFilters;
use App\Http\Resources\InsectKingdomResource;
use Illuminate\Support\Facades\DB;
use App\InsectKingdom;
use Illuminate\Http\Request;

class InsectKingdomsInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectKingdomsFilters $filters)
    {
        return response()->json([
            'data' =>  InsectKingdom::filterBy($filters)->select('kingdom', 'id')->get()
        ]);
    }
}
