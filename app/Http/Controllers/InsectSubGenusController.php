<?php

namespace App\Http\Controllers;

use App\InsectSubGenus;
use Illuminate\Http\Request;

class InsectSubGenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectSubGenus  $insectSubGenus
     * @return \Illuminate\Http\Response
     */
    public function show(InsectSubGenus $insectSubGenus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectSubGenus  $insectSubGenus
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectSubGenus $insectSubGenus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectSubGenus  $insectSubGenus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectSubGenus $insectSubGenus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectSubGenus  $insectSubGenus
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectSubGenus $insectSubGenus)
    {
        //
    }
}
