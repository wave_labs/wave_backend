<?php

namespace App\Http\Controllers;

use App\InsectOccurrenceHasRecordedByAuthor;
use Illuminate\Http\Request;

class InsectOccurrenceHasRecordedByAuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectOccurrenceHasRecordedByAuthor  $insectOccurrenceHasRecordedByAuthor
     * @return \Illuminate\Http\Response
     */
    public function show(InsectOccurrenceHasRecordedByAuthor $insectOccurrenceHasRecordedByAuthor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectOccurrenceHasRecordedByAuthor  $insectOccurrenceHasRecordedByAuthor
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectOccurrenceHasRecordedByAuthor $insectOccurrenceHasRecordedByAuthor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectOccurrenceHasRecordedByAuthor  $insectOccurrenceHasRecordedByAuthor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectOccurrenceHasRecordedByAuthor $insectOccurrenceHasRecordedByAuthor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectOccurrenceHasRecordedByAuthor  $insectOccurrenceHasRecordedByAuthor
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectOccurrenceHasRecordedByAuthor $insectOccurrenceHasRecordedByAuthor)
    {
        //
    }
}
