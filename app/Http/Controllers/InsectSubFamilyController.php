<?php

namespace App\Http\Controllers;

use App\InsectSubFamily;
use Illuminate\Http\Request;

class InsectSubFamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectSubFamily  $insectSubFamily
     * @return \Illuminate\Http\Response
     */
    public function show(InsectSubFamily $insectSubFamily)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectSubFamily  $insectSubFamily
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectSubFamily $insectSubFamily)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectSubFamily  $insectSubFamily
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectSubFamily $insectSubFamily)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectSubFamily  $insectSubFamily
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectSubFamily $insectSubFamily)
    {
        //
    }
}
