<?php

namespace App\Http\Controllers;

use App\InsectReferenceCategories;
use Illuminate\Http\Request;

class InsectReferenceCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectReferenceCategories  $insectReferenceCategories
     * @return \Illuminate\Http\Response
     */
    public function show(InsectReferenceCategories $insectReferenceCategories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectReferenceCategories  $insectReferenceCategories
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectReferenceCategories $insectReferenceCategories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectReferenceCategories  $insectReferenceCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectReferenceCategories $insectReferenceCategories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectReferenceCategories  $insectReferenceCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectReferenceCategories $insectReferenceCategories)
    {
        //
    }
}
