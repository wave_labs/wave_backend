<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectInfraSpecificFilters;
use App\Http\Resources\InsectInfraSpecificResource;
use App\InsectInfraSpecific;
use Illuminate\Http\Request;

class InsectInfraSpecificInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectInfraSpecificFilters $filters)
    {
        return response()->json([
            'data' =>  InsectInfraSpecific::filterBy($filters)->select('infraSpecific', 'id')->get()
        ]);
    }
}
