<?php

namespace App\Http\Controllers;

use App\Exports\GpsTagSatelliteExport;
use App\GpsTagSatellite;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class GpsTagSatelliteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function download(Request $request)
    {
        return Excel::download(new GpsTagSatelliteExport($request), 'export.xlsx');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return GpsTagSatellite::where('iot_report_id', $request->report_id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GpsTagSatellite  $gpsTagSatellite
     * @return \Illuminate\Http\Response
     */
    public function show(GpsTagSatellite $gpsTagSatellite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GpsTagSatellite  $gpsTagSatellite
     * @return \Illuminate\Http\Response
     */
    public function edit(GpsTagSatellite $gpsTagSatellite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GpsTagSatellite  $gpsTagSatellite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GpsTagSatellite $gpsTagSatellite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GpsTagSatellite  $gpsTagSatellite
     * @return \Illuminate\Http\Response
     */
    public function destroy(GpsTagSatellite $gpsTagSatellite)
    {
        //
    }
}
