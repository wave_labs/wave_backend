<?php

namespace App\Http\Controllers;

use App\InsectGenus;
use Illuminate\Http\Request;

class InsectGenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectGenus  $insectGenus
     * @return \Illuminate\Http\Response
     */
    public function show(InsectGenus $insectGenus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectGenus  $insectGenus
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectGenus $insectGenus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectGenus  $insectGenus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectGenus $insectGenus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectGenus  $insectGenus
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectGenus $insectGenus)
    {
        //
    }
}
