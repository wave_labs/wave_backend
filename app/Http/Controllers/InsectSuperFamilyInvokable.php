<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectSuperFamilyFilters;
use App\Http\Resources\InsectSuperFamilyResource;
use App\InsectSuperFamily;
use Illuminate\Http\Request;

class InsectSuperFamilyInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectSuperFamilyFilters $filters)
    {
        return response()->json([
            'data' =>  InsectSuperFamily::filterBy($filters)->select('superFamily', 'id')->get()
        ]);
    }
}
