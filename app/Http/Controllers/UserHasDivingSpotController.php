<?php

namespace App\Http\Controllers;

use App\UserHasDivingSpot;
use Illuminate\Http\Request;

class UserHasDivingSpotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserHasDivingSpot  $userHasDivingSpot
     * @return \Illuminate\Http\Response
     */
    public function show(UserHasDivingSpot $userHasDivingSpot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserHasDivingSpot  $userHasDivingSpot
     * @return \Illuminate\Http\Response
     */
    public function edit(UserHasDivingSpot $userHasDivingSpot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserHasDivingSpot  $userHasDivingSpot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserHasDivingSpot $userHasDivingSpot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserHasDivingSpot  $userHasDivingSpot
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserHasDivingSpot $userHasDivingSpot)
    {
        //
    }
}
