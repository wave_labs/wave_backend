<?php

namespace App\Http\Controllers;

use App\Http\QueryFilters\InsectGenusFilters;
use App\Http\Resources\InsectGenusResource;
use App\InsectGenus;
use Illuminate\Http\Request;

class InsectGenusInvokable extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(InsectGenusFilters $filters)
    {
        return response()->json([
            'data' =>  InsectGenus::filterBy($filters)->select('genus', 'id')->get()
        ]);
    }
}
