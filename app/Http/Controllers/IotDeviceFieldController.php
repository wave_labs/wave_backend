<?php

namespace App\Http\Controllers;

use App\IotDeviceField;
use Illuminate\Http\Request;

class IotDeviceFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IotDeviceField  $iotDeviceField
     * @return \Illuminate\Http\Response
     */
    public function show(IotDeviceField $iotDeviceField)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IotDeviceField  $iotDeviceField
     * @return \Illuminate\Http\Response
     */
    public function edit(IotDeviceField $iotDeviceField)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IotDeviceField  $iotDeviceField
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IotDeviceField $iotDeviceField)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IotDeviceField  $iotDeviceField
     * @return \Illuminate\Http\Response
     */
    public function destroy(IotDeviceField $iotDeviceField)
    {
        //
    }
}
