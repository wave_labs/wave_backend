<?php

namespace App\Http\Controllers;

use App\InsectTribe;
use Illuminate\Http\Request;

class InsectTribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectTribe  $insectTribe
     * @return \Illuminate\Http\Response
     */
    public function show(InsectTribe $insectTribe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectTribe  $insectTribe
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectTribe $insectTribe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectTribe  $insectTribe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectTribe $insectTribe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectTribe  $insectTribe
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectTribe $insectTribe)
    {
        //
    }
}
