<?php

namespace App\Http\Controllers;

use App\InsectFamily;
use Illuminate\Http\Request;

class InsectFamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectFamily  $insectFamily
     * @return \Illuminate\Http\Response
     */
    public function show(InsectFamily $insectFamily)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectFamily  $insectFamily
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectFamily $insectFamily)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectFamily  $insectFamily
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectFamily $insectFamily)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectFamily  $insectFamily
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectFamily $insectFamily)
    {
        //
    }
}
