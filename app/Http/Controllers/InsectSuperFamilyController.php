<?php

namespace App\Http\Controllers;

use App\InsectSuperFamily;
use Illuminate\Http\Request;

class InsectSuperFamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectSuperFamily  $insectSuperFamily
     * @return \Illuminate\Http\Response
     */
    public function show(InsectSuperFamily $insectSuperFamily)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectSuperFamily  $insectSuperFamily
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectSuperFamily $insectSuperFamily)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectSuperFamily  $insectSuperFamily
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectSuperFamily $insectSuperFamily)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectSuperFamily  $insectSuperFamily
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectSuperFamily $insectSuperFamily)
    {
        //
    }
}
