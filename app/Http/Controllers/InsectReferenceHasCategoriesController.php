<?php

namespace App\Http\Controllers;

use App\InsectReferenceHasCategories;
use Illuminate\Http\Request;

class InsectReferenceHasCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsectReferenceHasCategories  $insectReferenceHasCategories
     * @return \Illuminate\Http\Response
     */
    public function show(InsectReferenceHasCategories $insectReferenceHasCategories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsectReferenceHasCategories  $insectReferenceHasCategories
     * @return \Illuminate\Http\Response
     */
    public function edit(InsectReferenceHasCategories $insectReferenceHasCategories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsectReferenceHasCategories  $insectReferenceHasCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsectReferenceHasCategories $insectReferenceHasCategories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsectReferenceHasCategories  $insectReferenceHasCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsectReferenceHasCategories $insectReferenceHasCategories)
    {
        //
    }
}
