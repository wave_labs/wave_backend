<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SightingCoordResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'creature' => [ 
              'id' => $this->creature->id,
              'name' => $this->creature->name,
            ], 
            'date' => (string) $this->date,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'image' => $this->data->where('type','Image')->first() 
          ];
    }
}
