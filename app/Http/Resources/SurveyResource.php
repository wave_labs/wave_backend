<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SurveyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'userable' => [
                'id' => $this->user->id,
                'type_name' => $this->user->userable_type,
                'user' => $this->user->userable,
               ],
               'creature' => [ 
                 'id' => $this->creature->id,
                 'name' => $this->creature->name,
               ],
               'dive' => [ 
                 'id' => $this->dive->id,
                 'max_depth' => $this->dive->max_depth,
                 'number_diver' => $this->dive->number_diver,
                 'dive_time' => $this->dive->dive_time,
                 'diving_spot' => $this->dive->diving_spot,

               ],
            'abundance_value' =>  $this->abundance_value,
            'date' =>  $this->date,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            'deleted_at' => (string) $this->created_at,
          ];
    }
}
