<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LitterCoordResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'date' => (string) $this->date,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'photo' => $this->litterPhotos,
            'litter_category' => LitterCategoryWithoutSubCatResource::collection($this->litterCategory),
        ];
    }
}
