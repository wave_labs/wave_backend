<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SightingHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category' => $this->creature->name,
            'quantity' => $this->group_size,
            'date' => $this->date,
            'source' => $this->vehicle->vehicle,
            'score' => 100,
        ];
    }
}
