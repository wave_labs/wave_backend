<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MarkerMapResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => (string) $this->created_at,
            'creature' => [
                "id" => $this->creature->id,
                "name" => $this->creature->name,
            ],
            'activities' => MarkerActivityResource::collection($request->activity ? $this->activities->where('activity_id', $request->activity) : $this->activities),
        ];
    }
}
