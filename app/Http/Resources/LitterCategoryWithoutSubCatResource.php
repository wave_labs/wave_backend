<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LitterCategoryWithoutSubCatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->getTranslation('name', $request->language ? $request->language : 'en'),
        ];
    }
}
