<?php

namespace App\Http\Resources;

use App\InsectClass;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectPhylumResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'name' => $this->phylum,
            /* 'kingdom' => $this->kingdom, */
            'uuid' => uniqid(),

            'reference' => new InsectTaxonReferenceResource($this->reference),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'kingdom' => $this->kingdom,

            'children' => InsectClassResource::collection(InsectClass::where('phylum_id', '=', $this->id)->get()),
        ];
    }
}
