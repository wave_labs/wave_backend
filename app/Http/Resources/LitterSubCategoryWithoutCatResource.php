<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LitterSubCategoryWithoutCatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category' => $this->litterCategory->id,
            'OSPAR_code' => $this->OSPAR_code,
            'UNEP_code' => $this->UNEP_code,
        ];
    }
}
