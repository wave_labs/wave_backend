<?php

namespace App\Http\Resources;

use App\InsectFamily;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectSuperFamilyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->superFamily,
            'uuid' => uniqid(),
            /* 'infraOrder' => $this->infraOrder, */
            'reference' => new InsectTaxonReferenceResource($this->reference),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'infraOrder' => $this->infraOrder,
            'children' => InsectFamilyResource::collection(InsectFamily::where('super_family_id', '=', $this->id)->get()),
        ];
    }
}
