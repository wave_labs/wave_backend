<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vehicle' => $this->getTranslation('vehicle', $request->language ? $request->language : 'en'),
            'vehicle_type' => [
                'id' => $this->vehicle_type->id,
                'type' => $this->vehicle_type->getTranslation('type', $request->language ? $request->language : 'en'),
            ],
        ];
    }
}
