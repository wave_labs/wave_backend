<?php

namespace App\Http\Resources;

use App\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class ResultResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'feedback' => [
                'id' => $this->feedback->id,
                'userable' => [
                    'id' => $this->feedback->user->id,
                    'type_name' => $this->feedback->user->userable_type,
                    'user' => $this->feedback->user->userable
                ],
            ],
            'question' => Helper::getFieldTranslation($request, 'question', $this->question),
            'rating' => $this->rating,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
