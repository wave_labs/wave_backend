<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StopwatchResultResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'userable' => [
                'id' => optional($this->user)->id,
                'type_name' => optional($this->user)->userable_type,
                'user' => optional($this->user)->userable
            ],
            'created_at' => (string) $this->created_at,
            'video' => [
                'id' => $this->video->id,
                'category' => $this->video->category->name,
            ],
            'occupation' => $this->occupation,
            'timestamps' => StopwatchTimespampResource::collection($this->stopwatchTimestamps)
        ];
    }
}
