<?php

namespace App\Http\Resources;

use App\InsectSpecific;
use App\InsectSubGenus;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectGenusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->genus,
            'uuid' => uniqid(),
            'reference' => new InsectTaxonReferenceResource($this->reference),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'family' => $this->family,
            'subFamily' => $this->subFamily,
            'tribe' => $this->tribe,
            'children' => InsectSubGenusResource::collection(InsectSubGenus::where('genus_id', '=', $this->id)->get())->concat(InsectSpecificResource::collection(InsectSpecific::where([['genus_id', '=', $this->id], ['sub_genus_id', '=', null]])->get())),
        ];
    }
}
