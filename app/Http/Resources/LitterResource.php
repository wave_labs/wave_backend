<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LitterResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'date' => (string) $this->date,
            'quantity' => $this->quantity,
            'radius' => $this->radius,
            'collected' => $this->collected,
            'multiple' => $this->multiple,
            'photo' => $this->litterPhotos,
            'source' => $this->source,
            'userable' => $this->user ? [
                'id' => $this->user->id,
                'type_name' => $this->user->userable_type,
                'user' => $this->user->userable
            ] : null,
            'litter_sub_category' => LitterSubCategoryWithoutCatResource::collection($this->litterSubCategory),
            'litter_category' => LitterCategoryWithoutSubCatResource::collection($this->litterCategory),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
