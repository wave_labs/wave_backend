<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\DivingSpot;

class DivingSpotDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' =>  $this->name,
            'yAxis' => [
                'level_1' => "1",
                'level_2' => "1 - 6",
                'level_3' => "7 - 10",
                'level_4' => "11 - 15",
                'level_5' => "16 - 30",
                'level_6' => "31 - 50",
                'level_7' => "> 50"
            ],
            'xAxis' => $this->creaturesAbundance(),
        ];
    }
}
