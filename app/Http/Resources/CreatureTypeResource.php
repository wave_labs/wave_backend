<?php

namespace App\Http\Resources;

use App\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class CreatureTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'taxa_category' => Helper::getFieldTranslation($request, 'taxa_category', $this),
            'acoustic_data' =>  $this->acoustic_data,
            'image' =>  $this->imagePath,
            'acoustic_category' =>  $this->acoustic_category,
            'created_at' => (string) $this->created_at,
        ];
    }
}
