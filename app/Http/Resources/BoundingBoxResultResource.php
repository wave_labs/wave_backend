<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BoundingBoxResultResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'userable' => [
                'id' => $this->user->id,
                'photo' => $this->user->photo,
                'type_name' => $this->user->userable_type,
                'user' => $this->user->userable
            ],
            'img' => $this->image->image,
            'boxes' => BoundingBoxCoordinateResource::collection($this->coordinate),
            'file' => $this->file,

            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
