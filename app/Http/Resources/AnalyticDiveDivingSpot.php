<?php

namespace App\Http\Resources;

use App\Dive;
use Illuminate\Http\Resources\Json\JsonResource;

class AnalyticDiveDivingSpot extends JsonResource
{
    private $user_id, $date;

    public function __construct($resource, $date, $user_id)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->date = $date;
        $this->user_id = $user_id;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'yAxis' => Dive::reportsPerMonthPerDivingSpot($this->date, $this->user_id),
        ];
    }
}
