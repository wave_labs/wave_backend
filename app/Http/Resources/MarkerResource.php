<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MarkerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => (string) $this->created_at,
            'creature' => [
                "id" => $this->creature->id,
                "name" => $this->creature->name,
                "size" => [
                    "sm" => $this->creatureSize->sm,
                    "md" => $this->creatureSize->md,
                    "lg" => $this->creatureSize->lg
                ],
            ],
            'activities' => MarkerActivityResource::collection($this->activities),
            'form' =>  MarkerResultResource::collection($this->results),
        ];
    }
}
