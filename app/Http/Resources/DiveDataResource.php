<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DiveDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => (string) $this->date,
            'userable' => [
                'id' => $this->user->id,
                'userable_type' => $this->user->userable_type,
                'user' => $this->user->userable
            ],
            'diving_spot' => [
                'id' => $this->diving_spot->id,
                'name' => $this->diving_spot->name
            ],
            'creatures' => DivePivotResource::collection($this->dive_pivot),
        ];
    }
}
