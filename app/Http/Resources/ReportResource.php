<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'userable' => [
                'id' => $this->user->id,
                'userable_type' => $this->user->userable_type,
                'user' => $this->user->userable,
            ],
            'question' => $this->question,
            'response' => $this->response,
            'subject' => $this->subject,
            'section' => $this->section,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
          ];
    }
}
