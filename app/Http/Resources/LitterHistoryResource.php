<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LitterHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category' => $this->litterSubCategory,
            'quantity' => $this->quantity . ' meters',
            'date' => $this->date,
            'source' => $this->source,
            'score' => 100,
        ];
    }
}
