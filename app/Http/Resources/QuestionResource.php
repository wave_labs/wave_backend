<?php

namespace App\Http\Resources;

use App\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'question' => $this->getTranslation('question', $request->language ? $request->language : 'en'),
            'scale' => $this->scale,
            'form' => [
                'id' => $this->form->id,
                'form' => $this->form->form
            ],
        ];
    }
}
