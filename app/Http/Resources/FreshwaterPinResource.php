<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FreshwaterPinResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => (string) $this->created_at,
            'coordinates' => FreshwaterPinCoordinateResource::collection($this->coordinates->sortBy('image')),
            'form' => FreshwaterPinResultResource::collection($this->form),
        ];
    }
}
