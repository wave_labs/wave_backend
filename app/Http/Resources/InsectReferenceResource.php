<?php

namespace App\Http\Resources;

use App\InsectAuthor;
use App\InsectReferenceCategories;
use App\InsectReferenceHasAuthor;
use App\InsectReferenceHasCategories;
use App\InsectReferenceHasKeyword;
use App\InsectReferenceHasTag;
use App\InsectReferenceKeyword;
use App\InsectReferenceTag;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectReferenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'identifier' => $this->identifier,
            'description' => $this->description,
            'bibliographicCitation' => $this->bibliographicCitation,
            'editorList' => $this->editorList,
            'date' => $this->date,
            'journal' => $this->journal,
            'chapterTitle' => $this->chapterTitle,
            'editor' => $this->editor,
            'volume' => $this->volume,
            'pages' => $this->pages,
            'pageStart' => $this->pageStart,
            'pageEnd' => $this->pageEnd,
            'edition' => $this->edition,
            'publisher' => $this->publisher,
            'language' => $this->language,
            'subject' => $this->subject,
            'taxonRemarks' => $this->taxonRemarks,
            'type' => $this->type,
            'validated' => $this->validated,
            'datasetName' => $this->datasetName,
            'datasetID' => $this->datasetID,
            'modified' => $this->modified,
            'institutionID' => $this->institutionID,
            'collectionID' => $this->collectionID,
            'institutionCode' => $this->institutionCode,
            'collectionCode' => $this->collectionCode,
            'ownerInstitutionCode' => $this->ownerInstitutionCode,
            'issue' => $this->issue,
            'degree' => $this->degree,
            'university' => $this->university,
            'conferenceTittle' => $this->conferenceTittle,
            'accessDate' => $this->accessDate,
            'reports' => InsectReportResource::collection($this->reports),
            'authors' => InsectAuthor::whereIn('id', InsectReferenceHasAuthor::where('reference_id', '=', $this->id)->pluck('author_id'))->pluck('name'),
            'tags' => InsectReferenceTag::whereIn('id', InsectReferenceHasTag::where('reference_id', '=', $this->id)->pluck('reference_tag_id'))->pluck('tag'),
            'keywords' => InsectReferenceKeyword::whereIn('id', InsectReferenceHasKeyword::where('reference_id', '=', $this->id)->pluck('reference_keyword_id'))->pluck('keyword'),
            'categories' => InsectReferenceCategories::whereIn('id', InsectReferenceHasCategories::where('reference_id', '=', $this->id)->pluck('reference_category_id'))->pluck('category'),
            'project_id' => $this->project_id
        ];
    }
}
