<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SightingDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'id' => $this->id,
            'sighting_id' => $this->sighting_id,
            'link' => $this->link,
            'capsule_link' => $this->capsule_link,
            'underwater' => $this->underwater,
            'type' => $this->type,
            'acoustic' => $this->when($this->type == 'Acoustic', $this->acoustic),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
          ];
    }
}
