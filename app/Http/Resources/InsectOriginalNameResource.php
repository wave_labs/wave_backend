<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InsectOriginalNameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference' => new InsectReferenceResource($this->reference),
            'originalNameGenus' => $this->originalNameGenus,
            'originalNameSubGenus' => $this->originalNameSubGenus,
            'originalNameSpecificEpithet' => $this->originalNameSpecificEpithet,
            'originalNameInfraSpecificEpithet' => $this->originalNameInfraSpecificEpithet,
            'originalNameDate' => $this->originalNameDate,
            'originalNameReference_id' => $this->originalNameReference_id,
            'originalNameTaxonRank' => $this->originalNameTaxonRank,
            'originalNameTaxonRemarks' => $this->originalNameTaxonRemarks,
            'originalNameLanguage' => $this->originalNameLanguage,
            'originalNameRights' => $this->originalNameRights,
            'originalNameLicense' => $this->originalNameLicense,
            'originalNameInstitutionID' => $this->originalNameInstitutionID,
            'originalNameInstitutionCode' => $this->originalNameInstitutionCode,
            'originalNameRightSholder' => $this->originalNameRightSholder,
            'originalNameAccessRights' => $this->originalNameAccessRights,
            'originalNameSource' => $this->originalNameSource,
            'originalNameDatasetID' => $this->originalNameDatasetID,
            'originalNameDatasetName' => $this->originalNameDatasetName
        ];
    }
}
