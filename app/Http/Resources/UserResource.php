<?php

namespace App\Http\Resources;

use App\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'note' => $this->note,
            'active' => $this->active,
            'admin' => $this->hasRole('admin') ? 1 : 0,
            'rating' => Helper::getHelperNextLevel($this->points),
            'note' => $this->note,
            'photo' => $this->photo,
            'is_verified' => $this->is_verified,
            'occupation' => $this->occupation,
            'userable' => [
                'type_name' => $this->userable_type,
                'certificates' => $this->certificates,
                'user' => $this->userable_type == 'App\UserPerson' ? (new UserPersonResource($this->userable)) : (new UserDiveCenterResource($this->userable)),
            ],
            'roles' =>  $this->when($this->roles->count() !== 0, $this->roles),
        ];
    }
}
