<?php

namespace App\Http\Resources;

use App\InsectOriginalName;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectInfraSpecificResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->infraSpecific,
            'uuid' => uniqid(),
            'reference' => new InsectTaxonReferenceResource($this->reference),
            'originalName' => new InsectOriginalNameResource(InsectOriginalName::where('infra_specific_id', '=', $this->id)->first()),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'scientificName' => $this->specific->genus->genus . " " . $this->specific->specific . " " . $this->infraSpecific,
            'specific' => $this->specific,
            'sinonimias' => InsectSinonimiaResource::collection($this->sinonimia)
        ];
    }
}
