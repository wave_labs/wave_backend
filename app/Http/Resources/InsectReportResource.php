<?php

namespace App\Http\Resources;

use App\InsectReport;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'coordinates' => (string) $this->coordinates,
            'year' => $this->year,
            'page' => $this->page,
            'n_male' => $this->n_male,
            'n_female' => $this->n_female,
            'n_children' => $this->n_children,
            'date_type' => $this->date_type,
            'date' => $this->date ? Carbon::parse($this->date)->format($this->getDateFormat()) : null,
            'start' => Carbon::parse($this->start)->format('Y-m-d'),
            'end' => Carbon::parse($this->end)->format('Y-m-d'),
            'species' => $this->species,
            'sinonimia' => $this->sinonimia,
            'zone' => $this->zone,
            'userable' => $this->user ? [
                'id' => $this->user->id,
                'type_name' => $this->user->userable_type,
                'user' => $this->user->userable
            ] : null,
        ];
    }
}
