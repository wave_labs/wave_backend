<?php

namespace App\Http\Resources;

use App\Helper;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Source;

class CreatureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $diveReporter = Source::where('name', 'DIVE_REPORTER')->value('id');
        $whaleReporter = Source::where('name', 'WHALE_REPORTER')->value('id');

        return [
            'id' => $this->id,
            'abundance' => $this->abundance,
            'type' => [
                'id' => $this->type->id,
                'taxa_category' => $this->type->taxa_category,
                'acoustic_data' =>  $this->type->acoustic_data,
                'acoustic_category' =>  $this->type->acoustic_category,
            ],
            'photos' => $this->photos,
            'name' => Helper::getFieldTranslation($request, 'name', $this),
            'name_scientific' =>  $this->name_scientific,
            'description' =>  Helper::getFieldTranslation($request, 'description', $this),
            'curiosity' => Helper::getFieldTranslation($request, 'curiosity', $this),
            'notes' =>  $this->notes,
            'conserv' =>  $this->conserv,
            'size' =>  Helper::getFieldTranslation($request, 'size', $this),
            'group_size' =>  $this->group_size,
            'calves' =>  $this->calves,
            'nis_status' =>  $this->nis_status,
            'show_order' =>  $this->show_order,
            'dive_center_app' =>  $this->sources->contains($diveReporter) ? true : false,
            'whale_reporter_app' =>  $this->sources->contains($whaleReporter) ? true : false,
        ];
    }
}
