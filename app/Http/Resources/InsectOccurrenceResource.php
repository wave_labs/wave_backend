<?php

namespace App\Http\Resources;

use App\InsectAuthor;
use App\InsectInfraSpecific;
use App\InsectOccurrenceHasIdentifiedAuthor;
use App\InsectOccurrenceHasRecordedByAuthor;
use App\InsectOccurrenceIdentifications;
use App\InsectSpecific;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectOccurrenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'basisOfRecord' => $this->basisOfRecord,

            'dateType' => $this->dateType,
            'eventDate' => $this->eventDateEnd ? [$this->eventDateStart, $this->eventDateEnd] : [$this->eventDateStart, null],
            'endDayOfYear' => $this->endDayOfYear,
            'verbatimEventDate' => $this->verbatimEventDate,
            'eventRemarks' => $this->eventRemarks,
            'specie' => new InsectSpecificResource(InsectSpecific::where('id', '=', $this->specific_id)->first()),
            'subspecie' => new InsectInfraSpecificResource(InsectInfraSpecific::where('id', '=', $this->infra_specific_id)->first()),
            'reference_id' => $this->reference_id,
            'dateIdentified' => $this->dateIdentified,
            'identificationRemarks' => $this->identificationRemarks,
            'previousIdentifications' => InsectOccurrenceIdentificationsResource::collection(InsectOccurrenceIdentifications::where('occurrence_id', '=', $this->id)->get()),
            'decimalLatitude' => $this->decimalLatitude,
            'decimalLongitude' => $this->decimalLongitude,
            'geodeticDatum' => $this->geodeticDatum,
            'coordinateUncertaintyInMeters' => $this->coordinateUncertaintyInMeters,
            'verbatimCoordinates' => $this->verbatimCoordinates,
            'verbatimCoordinateSystem' => $this->verbatimCoordinateSystem,
            'georeferencedBy' => $this->georeferencedBy,
            'georeferencedDate' => $this->georeferencedDate,
            'georeferenceProtocol' => $this->georeferenceProtocol,
            'georeferenceSources' => $this->georeferenceSources,
            'georeferenceRemarks' => $this->georeferenceRemarks,
            'georeferenceVerificationStatus' => $this->georeferenceVerificationStatus,
            'higherGeography' => $this->higherGeography,
            'continent' => $this->continent,
            'islandGroup' => $this->islandGroup,
            'island' => $this->island,
            'biogeographicRealm' => $this->biogeographicRealm,
            'country' => $this->country,
            'countryCode' => $this->countryCode,
            'region' => $this->region,
            'province' => $this->province,
            'county' => $this->county,
            'geographicRemarks' => $this->geographicRemarks,
            'locality' => $this->locality,
            'verbatimLocality' => $this->verbatimLocality,
            'locationAccordingTo' => $this->locationAccordingTo,
            'localityRemarks' => $this->localityRemarks,
            'elevationMin' => $this->elevationMin,
            'elevationMax' => $this->elevationMax,
            'elevationMethod' => $this->elevationMethod,
            'elevationAccuracy' => $this->elevationAccuracy,
            'verbatimElevation' => $this->verbatimElevation,
            'type' => $this->type,
            'numbers' => $this->numbers,
            'machineSpecifics' => $this->machineSpecifics,
            'language' => $this->language,
            'license' => $this->license,
            'institutionId' => $this->institutionId,
            'institutionCode' => $this->institutionCode,
            'collectionCode' => $this->collectionCode,
            'catalogNumber' => $this->catalogNumber,
            'individualCount' => $this->individualCount,
            'maleCount' => $this->maleCount,
            'femaleCount' => $this->femaleCount,
            'imatureCount' => $this->imatureCount,
            'exuvias' => $this->exuvias,
            'organismQuantity' => $this->organismQuantity,
            'organismQuantityType' => $this->organismQuantityType,
            'establishmentMeans' => $this->establishmentMeans,
            'preparations' => $this->preparations,
            'habitat' => $this->habitat,
            'habitatRemarks' => $this->habitatRemarks,
            'microHabitats' => $this->microHabitats,
            'biologicalAssociation' => $this->biologicalAssociation,
            'biologicalRemarks' => $this->biologicalRemarks,
            'samplingMethod' => $this->samplingMethod,
            'typeOfSampling' => $this->typeOfSampling,
            'tripCode' => $this->tripCode,
            'dataAttributes' => $this->dataAttributes,
            'materialType' => $this->materialType,
            'cataloguedBy' => $this->cataloguedBy,
            'dataValidation' => $this->dataValidation,
            'identifiedBy' => InsectAuthor::where('id', '=', $this->identifiedBy)->pluck('name'),
            'recordedBy' => InsectAuthor::whereIn('id', InsectOccurrenceHasRecordedByAuthor::where('occurrence_id', '=', $this->id)->pluck('author_id'))->pluck('name'),
            'project_id' => $this->project_id,
            'occurrenceRemarks' => $this->occurrenceRemarks,
            'recordNumber' => $this->recordNumber,
            'preparationType' => $this->preparationType,
            'preparedBy' => $this->preparedBy,
            'preparationDate' => $this->preparationDate,
            'preparationRemarks' => $this->preparationRemarks,
            'authorExperience' => $this->authorExperience
        ];
    }
}
