<?php

namespace App\Http\Resources;

use App\Helper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SightingResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'userable' => $this->user ? [
                'id' => $this->user->id,
                'type_name' => $this->user->userable_type,
                'user' => $this->user->userable
            ] : null,
            'creature' => [
                'id' =>  $this->creature->id,
                'name' => $this->creature->getTranslation('name', $request->language ? $request->language : 'en'),
                'dive_center_app' =>  $this->creature->sources->contains(2) ? true : false,
                'whale_reporter_app' =>  $this->creature->sources->contains(1) ? true : false,
            ],
            'capsule_id' => $this->capsule_id,
            'time' => $this->time,
            'report_source' => $this->report_source,
            'trip_id' => $this->trip_id,
            'beaufort_scale' => $this->beaufort_scale,
            'date' => $this->date,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'is_group_mixed' => $this->is_group_mixed,
            'group_size' => $this->group_size,
            'behaviour' => $this->behaviour,
            'vehicle' => $this->vehicle,
            'calves' => $this->calves,
            'notes' => $this->notes,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
