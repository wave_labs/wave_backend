<?php

namespace App\Http\Resources;

use App\InsectFamily;
use App\InsectSubOrder;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->order,
            'uuid' => uniqid(),
            /* 'class' => $this->class, */
            'reference' => new InsectTaxonReferenceResource($this->reference),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'class' => $this->class,
            'children' => InsectSubOrderResource::collection(InsectSubOrder::where('order_id', '=', $this->id)->get())->concat(InsectFamilyResource::collection(InsectFamily::where([['order_id', '=', $this->id], ['sub_order_id', '=', null], ['infra_order_id', '=', null], ['super_family_id', '=', null]])->get()))
        ];
    }
}
