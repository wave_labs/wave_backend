<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DiveHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category' => DivePivotHistoryResource::collection($this->dive_pivot),
            'quantity' => $this->group_size,
            'date' => $this->date,
            'source' => $this->diving_spot->name,
            'score' => 100,
        ];
    }
}
