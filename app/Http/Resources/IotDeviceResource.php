<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class IotDeviceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'timeline_fields' => $this->features(),
            'name' =>  $this->name,
            'template' =>  $this->template,
            'serial_number' =>  $this->serial_number,
            'sections' => $this->sectionFeatures(),
            'parameters' => $this->featureParameters(),
            'fields' => IotDeviceFieldResource::collection($this->fields),
            'type' => [
                'id' => $this->type->id,
                'name' => $this->type->name,
            ],
            'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name,
            ],
            'environment' => [
                'id' => $this->environment->id,
                'name' => $this->environment->name,
            ],
            'gateways' => $this->gateways,
            'category' => [
                'id' => $this->categories[0]->id,
                'name' => $this->categories[0]->name,
                'latitude' => $this->categories[0]->pivot->latitude,
                'longitude' => $this->categories[0]->pivot->longitude,
            ],
            'users' => UserMinimalistResource::collection($this->users),
            'img' => $this->image,
            'created_at' => (string) $this->created_at,
        ];
    }
}
