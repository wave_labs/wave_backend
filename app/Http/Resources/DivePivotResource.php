<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DivePivotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'creature' => [
                'id' => $this->creature->id,
                'name' => $this->creature->name
            ],
            'abundance_value' =>  $this->abundance_value,
        ];
    }
}
