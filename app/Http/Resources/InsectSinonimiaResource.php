<?php

namespace App\Http\Resources;

use App\InsectReference;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectSinonimiaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $scientificName = "";
        if ($this->genus) {
            $scientificName = $scientificName . $this->genus;
        }
        if ($this->subgenus) {
            $scientificName = $scientificName . ' ' . $this->subgenus;
        }
        if ($this->specificEpithet) {
            $scientificName = $scientificName . ' ' . $this->specificEpithet;
        }
        if ($this->infraSpecificEpithet) {
            $scientificName = $scientificName . ' ' . $this->infraSpecificEpithet;
        }

        return [
            'id' => $this->id,
            'genus' => $this->genus,
            'subgenus' => $this->subgenus,
            'specificEpithet' => $this->specificEpithet,
            'infraSpecificEpithet' => $this->infraSpecificEpithet,
            'name' => $this->infraSpecificEpithet,
            'scientificName' => $scientificName,
            'reference' => new InsectReferenceResource($this->reference),
            'taxonRank' => $this->taxonRank,
            'taxonomicStatus' => explode(", ", $this->taxonomicStatus),
            'nomenclatureStatus' => explode(", ", $this->nomenclatureStatus),
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
