<?php

namespace App\Http\Resources;

use App\InsectGenus;
use App\InsectTribe;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectSubFamilyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->subFamily,
            'uuid' => uniqid(),
            'reference' => new InsectTaxonReferenceResource($this->reference),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'family' => $this->family,
            'children' => InsectTribeResource::collection(InsectTribe::where('sub_family_id', '=', $this->id)->get())->concat(InsectGenusResource::collection(InsectGenus::where([['sub_family_id', '=', $this->id], ['tribe_id', '=', null]])->get())),
        ];
    }
}
