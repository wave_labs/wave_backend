<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FreshwaterPinCoordinateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => $this->image,
            'timeframe' => $this->timeframe ? "Over 5 years ago" : "In the last 5 years",
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ];
    }
}
