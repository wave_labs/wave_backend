<?php

namespace App\Http\Resources;

use App\Litter;
use App\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class AnalyticLitterResource extends JsonResource
{
    private $source, $users;

    public function __construct($resource, $source, $users)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->source = $source;
        $this->users = $users;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'yAxis' => Helper::getHelperRangeValues(),
            'xAxis' => Litter::reportsPerMonth($this->source, $this->users),
        ];
    }
}
