<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FeedbackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'source' => [
                'id' => $this->source->id,
                'form' => $this->source->name,
            ],
            'userable' => [
                'id' => $this->user->id,
                'type_name' => $this->user->userable_type,
                'user' => $this->user->userable
            ],
            'form' => [
                'id' => $this->form->id,
                'form' => $this->form->form,
            ],
            'results' => $this->result,
            'score' => $this->score,
            'uniqid' => $this->uniqid,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
