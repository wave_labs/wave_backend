<?php

namespace App\Http\Resources;

use App\IotReport;
use Illuminate\Http\Resources\Json\JsonResource;

class IotReportWaveMeasurementsResource extends JsonResource
{
    private $dataset;

    public function __construct($resource, $device)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->dataset = IotReport::getTimeline($device);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'labels' => $this->dataset[4],
            'period' => $this->dataset[0],
            'significant' => $this->dataset[3],
            'average' => $this->dataset[1],
            'max' => $this->dataset[2],
        ];
    }
}
