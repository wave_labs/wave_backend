<?php

namespace App\Http\Resources;

use App\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class BeaufortScaleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'desc' => Helper::getFieldTranslation($request, 'desc', $this->type),
            'scale' => $this->scale,
            'sea_conditions' => $this->sea_conditions,
            'land_conditions' => $this->land_conditions,
        ];
    }
}
