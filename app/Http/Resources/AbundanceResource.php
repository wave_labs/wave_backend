<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AbundanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->creature_id,
            'level_1'=> $this->level_1,
            'level_2'=> $this->level_2, 
            'level_3'=> $this->level_3,
            'level_4' => $this->level_4,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            ];
    }
}
