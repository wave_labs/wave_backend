<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LitterSubCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->getTranslation('name', $request->language ? $request->language : 'en'),
            'category' => [
                'id' => $this->litterCategory->id,
                'name' => $this->litterCategory->getTranslation('name', $request->language ? $request->language : 'en'),
            ],
            'OSPAR_code' => $this->OSPAR_code,
            'UNEP_code' => $this->UNEP_code,
            'Core' => (string) $this->Core,
            'Beach' => (string) $this->Beach,
            'Seafloor' => (string) $this->Seafloor,
            'Floating' => (string) $this->Floating,
            'Biota' => (string) $this->Biota,
            'Micro' => (string) $this->Micro,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
