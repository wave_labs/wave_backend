<?php

namespace App\Http\Resources;

use App\Helper;
use App\Sighting;
use Illuminate\Http\Resources\Json\JsonResource;

class AnalyticSightingCreatureResource extends JsonResource
{
    private $date, $users;

    public function __construct($resource, $date, $users)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->date = $date;
        $this->users = $users;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'yAxis' => Helper::getHelperRangeValues(),
            'xAxis' => Sighting::reportsPerMonthPerCreature($this->date, $this->users),
        ];
    }
}
