<?php

namespace App\Http\Resources;

use App\Dive;
use Illuminate\Http\Resources\Json\JsonResource;

class AnalyticDive extends JsonResource
{
    private $users;

    public function __construct($resource, $users)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->users = $users;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'yAxis' => Dive::reportsPerMonth($this->users),
        ];
    }
}
