<?php

namespace App\Http\Resources;

use App\InsectAuthor;
use App\InsectReferenceHasAuthor;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectReferenceSelectorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'date' => $this->date,
            'journal' => $this->journal,
            'chapterTitle' => $this->chapterTitle,
            'conferenceTittle' => $this->conferenceTittle,
            'authors' => InsectAuthor::whereIn('id', InsectReferenceHasAuthor::where('reference_id', '=', $this->id)->pluck('author_id'))->pluck('name'),
        ];
    }
}
