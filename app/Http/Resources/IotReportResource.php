<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IotReportResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'device' => $this->id_device,
            'fields' => IotReportFieldResource::collection($this->fields),
            'latitude' => $this->coordinates[0],
            'longitude' => $this->coordinates[1],
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            'collected_at' => (string) $this->collected_at,
        ];
    }
}
