<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EnmResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'ocurrenceData' => optional($this->ocuDataSource)->name,
            'environmentalData' => optional($this->envDataSource)->name,
            'creature' => optional($this->creature)->name,
            'created_at' => (string) $this->created_at,
            'models' => $this->models,
            'predictions' => EnmPredictionResource::collection($this->predictions),
        ];
    }
}
