<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MarkerActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'activity' => $this->activity->name,
            'color' => $this->activity->color,
            'experience' => $this->experience,
            'frequency' => $this->frequency,
            'time' => $this->time,
            'coordinates' => MarkerCoordinatesResource::collection($this->coordinates),
        ];
    }
}
