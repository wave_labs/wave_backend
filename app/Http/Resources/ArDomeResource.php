<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArDomeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'dome' => $this->dome,
            'timespent' => $this->timespent,
            'score' => $this->score,
            'finalized' => $this->finalized,
            'has_dome' => $this->has_dome,
            'userable' => [
                'id' => optional($this->user)->id,
                'type' =>  optional($this->user)->userable_type,
                'user' =>  optional($this->user)->userable
            ],
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
