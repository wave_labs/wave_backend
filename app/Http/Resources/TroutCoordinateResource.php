<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TroutCoordinateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'animal' => $this->animal,
            'stream' => $this->stream,
            'year' => $this->year,
            'location' => $this->location,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ];
    }
}
