<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SoftCoralCoordinateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'year' => $this->year,
            'dimensions' => $this->dimensions,
            'dimension_change' => $this->dimension_change,
            'depth' => $this->depth,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ];
    }
}
