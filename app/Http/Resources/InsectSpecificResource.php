<?php

namespace App\Http\Resources;

use App\InsectInfraSpecific;
use App\InsectOriginalName;
use App\InsectSinonimia;
use App\InsectSpecific;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectSpecificResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->specific,
            'uuid' => uniqid(),
            'reference' => new InsectTaxonReferenceResource($this->reference),
            'originalName' => new InsectOriginalNameResource(InsectOriginalName::where([
                ['specific_id', '=', $this->id],
                ['infra_specific_id', '=', null],
            ])->first()),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'genus' => $this->genus,
            'subGenus' => $this->subGenus,
            'scientificName' => $this->genus->genus . " " . $this->specific,
            'children' => InsectInfraSpecificResource::collection(InsectInfraSpecific::where('specific_id', '=', $this->id)->get()),
            'sinonimias' =>InsectSinonimiaResource::collection(InsectSinonimia::where('specific_id', '=', $this->id)->get())
        ];
    }
}
