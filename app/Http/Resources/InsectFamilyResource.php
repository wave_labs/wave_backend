<?php

namespace App\Http\Resources;

use App\InsectGenus;
use App\InsectSubFamily;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectFamilyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->family,
            'uuid' => uniqid(),
            'reference' => new InsectTaxonReferenceResource($this->reference),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'order' => $this->order,
            'subOrder' => $this->subOrder,
            'infraOrder' => $this->infraOrder,
            'superFamily' => $this->superFamily,
            'children' => InsectSubFamilyResource::collection(InsectSubFamily::where('family_id', '=', $this->id)->get())->concat(InsectGenusResource::collection(InsectGenus::where([['family_id', '=', $this->id], ['sub_family_id', '=', null]])->get())),
        ];
    }
}
