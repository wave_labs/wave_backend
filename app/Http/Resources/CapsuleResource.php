<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CapsuleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'userable' => [
                'id' => $this->user->id,
                'type_name' => $this->user->userable_type,
                'user' => $this->user->userable
            ],
            'temperature_c' => $this->temperature_c,
            'ram_mb' => $this->ram_mb,
            'disk_space_gb' => $this->disk_space_gb,
            'volts' => $this->volts,
            'current' => $this->current,
            'power' => $this->power,
            'image_link' => $this->image_link,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'assigned' => $this->assigned,
            'description' => $this->description,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
      ];
    }
}
