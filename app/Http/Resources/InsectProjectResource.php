<?php

namespace App\Http\Resources;

use App\InsectProjectHasUser;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectProjectResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'owner' => $this->owner,
            'description' => $this->description,
            'imagePath' => $this->imagePath,
            'mobile_contact' => $this->mobile_contact,
            'email_contact' => $this->email_contact,
            'address' => $this->address,
            'team' => User::whereIn('id', InsectProjectHasUser::where('project_id', '=', $this->id)->pluck('user_id'))->get()
        ];
    }
}
