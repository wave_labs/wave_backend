<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\WhatsNewMessage;

class WhatsNewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->image,
            'date' => (string) $this->created_at,
            'update' => $this->whatsNewMessages->where('whats_new_type_id', 1),
            'event' => $this->whatsNewMessages->where('whats_new_type_id', 2),
            'research' => $this->whatsNewMessages->where('whats_new_type_id', 3),

        ];
    }
}
