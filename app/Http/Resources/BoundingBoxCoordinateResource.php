<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BoundingBoxCoordinateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'class' => $this->class->name,
            'coordinates' => [
                'y_min' => $this->y_min,
                'y_max' => $this->y_max,
                'x_min' => $this->x_min,
                'x_max' => $this->x_max,
            ],
        ];
    }
}
