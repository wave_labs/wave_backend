<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StopwatchTimespampResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'end_time' => $this->end_time,
            'start_time' => $this->start_time,
            'class' =>  $this->class,
        ];
    }
}
