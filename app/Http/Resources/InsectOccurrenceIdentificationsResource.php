<?php

namespace App\Http\Resources;

use App\InsectAuthor;
use App\InsectInfraSpecific;
use App\InsectSpecific;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectOccurrenceIdentificationsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'author' => InsectAuthor::where('id', '=', $this->author_id)->pluck('name'),
            'species' => new InsectSpecieIdentificationResource(InsectSpecific::where('id', '=', $this->species_id)->first()),
            'subspecies' => new InsectSubSpecieIdentificationResource(InsectInfraSpecific::where('id', '=', $this->sub_species_id)->first()),
            'dateIdentified' => $this->dateIdentified,
            'authorExperience' => $this->authorExperience
        ];
    }
}
