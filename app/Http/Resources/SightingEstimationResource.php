<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SightingEstimationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'creature_id' => $this->creature_id,
            'sighting_id' => $this->sighting_id,
            'confidence' => $this->confidence,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
          ];
    }
}
