<?php

namespace App\Http\Resources;

use App\InsectReference;
use App\InsectSubFamily;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectSpeciesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $scientificName = "";
        if($this->genus){
            $scientificName = $scientificName . $this->genus;
        }
        if($this->subgenus){
            $scientificName = $scientificName . ' ' . $this->subgenus;
        }
        if($this->specificEpithet){
            $scientificName = $scientificName . ' ' . $this->specificEpithet;
        }
        if($this->infraSpecificEpithet){
            $scientificName = $scientificName . ' ' . $this->infraSpecificEpithet;
        }

        return [
            'id' => $this->id,
            'kingdom' => $this->kingdom,
            'phylum' => $this->phylum,
            'class' => $this->class,
            'order' => $this->order,
            'subOrder' => $this->subOrder,
            'infraOrder' => $this->infraOrder,
            'superFamily' => $this->superFamily,
            'family' => $this->family,
            'subFamily' => $this->subFamily,
            'tribe' => $this->tribe,
            'genus' => $this->genus,
            'subgenus' => $this->subgenus,
            'specificEpithet' => $this->specificEpithet,
            'infraSpecificEpithet' => $this->infraSpecificEpithet,
            'scientificName' => $scientificName,
            'author' => $this->author,
            'year' => $this->year,
            'reference' => InsectReference::where('id', '=', $this->reference)->first(),
            'taxonRank' => $this->taxonRank,
            'taxonomicStatus' => $this->taxonomicStatus,
            'nomenclatureStatus' => $this->nomenclatureStatus,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'sinonimias' => InsectSinonimiaResource::collection($this->sinonimias),
            'originalName' => $this->originalName,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
