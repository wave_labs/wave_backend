<?php

namespace App\Http\Resources;

use App\InsectGenus;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectTribeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->tribe,
            'uuid' => uniqid(),
            /* 'subFamily' => $this->subFamily, */
            'reference' => new InsectTaxonReferenceResource($this->reference),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'subFamily' => $this->subFamily,
            'children' => InsectGenusResource::collection(InsectGenus::where('tribe_id', '=', $this->id)->get()),
        ];
    }
}
