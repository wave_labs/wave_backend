<?php

namespace App\Http\Resources;

use App\InsectFamily;
use App\InsectSuperFamily;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectInfraOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->infraOrder,
            /* 'subOrder' => $this->subOrder, */
            'uuid' => uniqid(),
            'reference' => new InsectTaxonReferenceResource($this->reference),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'subOrder' => $this->subOrder,
            'children' => InsectSuperFamilyResource::collection(InsectSuperFamily::where('infra_order_id', '=', $this->id)->get())->concat(InsectFamilyResource::collection(InsectFamily::where([['infra_order_id', '=', $this->id], ['super_family_id', '=', null]])->get())),
        ];
    }
}
