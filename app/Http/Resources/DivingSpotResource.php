<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DivingSpotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'longitude' =>  $this->longitude,
            'latitude' =>  $this->latitude,
            'substracts' =>  $this->substracts,
            'range' =>  $this->range,
            'name' =>  $this->name,
            'description' =>  $this->description,
            'substracts' =>  $this->substracts,
            'protection' =>  $this->protection,
            'validated' =>  $this->validated,
            'users' =>  $this->users,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            'deleted_at' => (string) $this->created_at,
          ];
    }
}
