<?php

namespace App\Http\Resources;

use App\LitterSubCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class LitterCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->getTranslation('name', $request->language ? $request->language : 'en'),
            'images' => LitterCategoryImageResource::collection($this->images),
            'menu' => $this->images->where('litter_category_image_type_id', 6)->where('litter_category_id', $this->id)->first(),
            'sub_category' => $request->query('source') ?
                LitterSubCategoryWithoutCatResource::collection(LitterSubCategory::source($request->query('source'))->category($this->id)->get()) : LitterSubCategoryWithoutCatResource::collection($this->litterSubCategory),
        ];
    }
}
