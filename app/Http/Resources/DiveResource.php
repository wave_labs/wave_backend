<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DiveResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'max_depth' =>  $this->max_depth,
            'dive_time' =>  $this->dive_time,
            'number_diver' =>  $this->number_diver,
            'date' => (string) $this->date,
            'diving_spot' => [
                'id' => $this->diving_spot->id,
                'name' => $this->diving_spot->name
            ],
            'userable' => $this->user ? [
                'id' => $this->user->id,
                'type_name' => $this->user->userable_type,
                'user' => $this->user->userable
            ] : null,
            'litter' => LitterResource::collection($this->litters),
            'creatures' => DivePivotResource::collection($this->dive_pivot),
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
