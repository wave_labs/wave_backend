<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TroutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'knowledge' => $this->knowledge,
            'nameA' => $this->nameA,
            'nameB' => $this->nameB,
            'nameC' => $this->nameC,
            'date' => (string) $this->created_at,
            'coordinates' => TroutCoordinateResource::collection($this->coordinates),
        ];
    }
}
