<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SoftCoralResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'knowledge' => $this->knowledge,
            'name' => $this->name,
            'date' => (string) $this->created_at,
            'coordinates' => SoftCoralCoordinateResource::collection($this->coordinates),
        ];
    }
}
