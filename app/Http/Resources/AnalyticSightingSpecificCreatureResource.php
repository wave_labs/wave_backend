<?php

namespace App\Http\Resources;

use App\Helper;
use App\Sighting;
use Illuminate\Http\Resources\Json\JsonResource;

class AnalyticSightingSpecificCreatureResource extends JsonResource
{
    private $date;

    public function __construct($resource, $date)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->date = $date;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'yAxis' => Helper::getHelperRangeValues(),
            'xAxis' => Sighting::reportsPerMonthPerSpecificCreature($this, $this->date),
        ];
    }
}
