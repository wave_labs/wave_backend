<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SuggestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'email' => $this->email,
            'suggestion' => $this->suggestion,
            'source' => [
                'id' => $this->source->id,
                'name' => $this->source->name
            ],
            'suggestionType' => [
                'id' => $this->suggestionType->id,
                'name' => $this->suggestionType->getTranslation('name', $request->language ? $request->language : 'en'),
            ],
            'userable' => [
                'id' => $this->user->id,
                'type_name' => $this->user->userable_type,
                'user' => $this->user->userable
            ],
            'created_at' => (string) $this->created_at
        ];
    }
}
