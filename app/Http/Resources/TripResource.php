<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TripResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'start' => (string) $this->start,
            'end' => (string) $this->end,
            'sightings' => SightingCoordResource::collection($this->sightings),
            'positions' => TripPositionResource::collection($this->positions),
        ];
    }
}
