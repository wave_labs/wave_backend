<?php

namespace App\Http\Resources;

use App\InsectFamily;
use App\InsectInfraOrder;
use Illuminate\Http\Resources\Json\JsonResource;

class InsectSubOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->subOrder,
            'reference' => new InsectTaxonReferenceResource($this->reference),
            'uuid' => uniqid(),
            'taxonRank' => $this->taxonRank,
            'taxonRemarks' => $this->taxonRemarks,
            'language' => $this->language,
            'rights' => $this->rights,
            'license' => $this->license,
            'institutionID' => $this->institutionID,
            'institutionCode' => $this->institutionCode,
            'rightSholder' => $this->rightSholder,
            'accessRights' => $this->accessRights,
            'source' => $this->source,
            'datasetID' => $this->datasetID,
            'datasetName' => $this->datasetName,
            'order' => $this->order,
            'children' => InsectInfraOrderResource::collection(InsectInfraOrder::where('sub_order_id', '=', $this->id)->get())->concat(InsectFamilyResource::collection(InsectFamily::where([['sub_order_id', '=', $this->id], ['infra_order_id', '=', null], ['super_family_id', '=', null]])->get())),
        ];
    }
}
