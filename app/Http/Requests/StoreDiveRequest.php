<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\User;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWTAuth;

class StoreDiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->header('Authorization') && Auth::id())
            $user = Auth::id();
        else if ($this->user_id == null || $this->user_id == "undefined" || !$this->user_id)
            $user = User::getGuest();
        else $user = $this->user_id;

        $this->max_depth ? $newDepth = $this->max_depth : $newDepth = 0;


        $this->merge([
            'user_id' => $user,
            'max_depth' => $newDepth
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer|exists:users,id',
            'diving_spot_id' => 'required|integer|exists:diving_spots,id',
            'max_depth' => 'required|integer',
            'dive_time' => 'required|integer',
            'number_diver' => 'required|integer',
            'creatures' => 'nullable',
            'creatures.*' => 'integer|distinct|exists:creatures,id',
            'date' => 'required|date',
            'abundance' => 'nullable',
            'abundance.*' => 'string',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'creatures.*.exists' => 'The selected creature :input is invalid.',
            'creatures.*.integer' => 'The selected creatures :input is invalid.',
            'user_id.exists' => 'The selected user is invalid.',
            'diving_spot_id.exists' => 'The selected diving spot is invalid.',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
