<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

class InsectOccurrenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->header('Authorization'))
            $user = JWTAuth::setToken($this->header('Authorization'))->user()->id;
        else if (!$this->user_id || $this->user_id == null || $this->user_id == "undefined")
            $user = User::getGuest();
        else $user = $this->user_id;

        $this->merge([
            'last_edit_by' => $user,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'basisOfRecord' => 'nullable',
            'dateType' => 'nullable',
            'endDayOfYear' => 'nullable',
            'verbatimEventDate' => 'nullable',
            'eventRemarks' => 'nullable',
            'specific_id' => 'nullable',
            'infra_specific_id' => 'nullable',
            'reference_id' => 'nullable',
            'dateIdentified' => 'nullable',
            'identificationRemarks' => 'nullable',
            'previousIdentifications' => 'nullable',
            'decimalLatitude' => 'nullable',
            'decimalLongitude' => 'nullable',
            'geodeticDatum' => 'nullable',
            'coordinateUncertaintyInMeters' => 'nullable',
            'verbatimCoordinates' => 'nullable',
            'verbatimCoordinateSystem' => 'nullable',
            'georeferencedBy' => 'nullable',
            'georeferencedDate' => 'nullable',
            'georeferenceProtocol' => 'nullable',
            'georeferenceSources' => 'nullable',
            'georeferenceRemarks' => 'nullable',
            'georeferenceVerificationStatus' => 'nullable',
            'higherGeography' => 'nullable',
            'continent' => 'nullable',
            'islandGroup' => 'nullable',
            'island' => 'nullable',
            'biogeographicRealm' => 'nullable',
            'country' => 'nullable',
            'countryCode' => 'nullable',
            'region' => 'nullable',
            'province' => 'nullable',
            'county' => 'nullable',
            'geographicRemarks' => 'nullable',
            'locality' => 'nullable',
            'verbatimLocality' => 'nullable',
            'locationAccordingTo' => 'nullable',
            'localityRemarks' => 'nullable',
            'elevationMin' => 'nullable',
            'elevationMax' => 'nullable',
            'elevationMethod' => 'nullable',
            'elevationAccuracy' => 'nullable',
            'verbatimElevation' => 'nullable',
            'type' => 'nullable',
            'numbers' => 'nullable',
            'machineSpecifics' => 'nullable',
            'language' => 'nullable',
            'license' => 'nullable',
            'institutionId' => 'nullable',
            'institutionCode' => 'nullable',
            'collectionCode' => 'nullable',
            'catalogNumber' => 'nullable',
            'individualCount' => 'nullable',
            'maleCount' => 'nullable',
            'femaleCount' => 'nullable',
            'imatureCount' => 'nullable',
            'exuvias' => 'nullable',
            'organismQuantity' => 'nullable',
            'organismQuantityType' => 'nullable',
            'establishmentMeans' => 'nullable',
            'preparations' => 'nullable',
            'habitat' => 'nullable',
            'habitatRemarks' => 'nullable',
            'microHabitats' => 'nullable',
            'biologicalAssociation' => 'nullable',
            'biologicalRemarks' => 'nullable',
            'samplingMethod' => 'nullable',
            'typeOfSampling' => 'nullable',
            'tripCode' => 'nullable',
            'dataAttributes' => 'nullable',
            'materialType' => 'nullable',
            'cataloguedBy' => 'nullable',
            'dataValidation' => 'nullable',
            'project_id' => 'nullable|integer|exists:mysql_insect.insect_projects,id',
            'created_by' =>  'nullable|integer|exists:users,id',
            'last_edit_by' => 'nullable|integer|exists:users,id',
            'occurrenceRemarks' => 'nullable',
            'recordNumber' => 'nullable',
            'preparationType' => 'nullable',
            'preparedBy' => 'nullable',
            'preparationDate' => 'nullable',
            'preparationRemarks' => 'nullable',
            'authorExperience' => 'nullable'
        ];
    }
}
