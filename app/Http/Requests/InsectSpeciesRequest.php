<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use JWTAuth;

class InsectSpeciesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->taxonRank == "Kingdom") {
            return $this->kingdomRules();
        } elseif ($this->taxonRank == "Phylum") {
            return $this->phylumRules();
        } elseif ($this->taxonRank == "Class") {
            return $this->classRules();
        } elseif ($this->taxonRank == "Order") {
            return $this->orderRules();
        } elseif ($this->taxonRank == "Suborder") {
            return $this->subOrderRules();
        } elseif ($this->taxonRank == "Infraorder") {
            return $this->infraOrderRules();
        } elseif ($this->taxonRank == "Superfamily") {
            return $this->superFamilyRules();
        } elseif ($this->taxonRank == "Family") {
            return $this->familyRules();
        } elseif ($this->taxonRank == "Subfamily") {
            return $this->subFamilyRules();
        } elseif ($this->taxonRank == "Tribe") {
            return $this->tribeRules();
        } elseif ($this->taxonRank == "Genus") {
            return $this->genusRules();
        } elseif ($this->taxonRank == "Subgenus") {
            return $this->subGenusRules();
        } elseif ($this->taxonRank == "Species") {
            return $this->specificRules();
        } elseif ($this->taxonRank == "Subspecies") {
            return $this->infraSpecificRules();
        }
    }

    protected function kingdomRules(): array
    {
        return [
            /* 'kingdom' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_kingdoms')->ignore($this->id, 'id')
            ], */
            'kingdom' => 'required|string',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function phylumRules(): array
    {
        return [
            /* 'phylum' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_phylums')->ignore($this->id, 'id')
            ], */
            'phylum' => 'required|string',
            'kingdom_id' => 'required|exists:mysql_insect.insect_kingdoms,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function classRules(): array
    {
        return [
            /* 'class' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_classes')->ignore($this->id, 'id')
            ], */
            'class' => 'required|string',
            'phylum_id' => 'required|exists:mysql_insect.insect_phylums,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function orderRules(): array
    {
        return [
            /* 'order' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_orders')->ignore($this->id, 'id')
            ], */
            'order' => 'required|string',
            'class_id' => 'required|exists:mysql_insect.insect_classes,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function subOrderRules(): array
    {
        return [
            /* 'subOrder' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_sub_orders')->ignore($this->id, 'id')
            ], */
            'subOrder' => 'required|string',
            'order_id' => 'required|exists:mysql_insect.insect_orders,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function infraOrderRules(): array
    {
        return [
            /* 'infraOrder' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_infra_orders')->ignore($this->id, 'id')
            ], */
            'infraOrder' => 'required|string',
            'sub_order_id' => 'required|exists:mysql_insect.insect_sub_orders,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function superFamilyRules(): array
    {
        return [
            /* 'superFamily' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_super_families')->ignore($this->id, 'id')
            ], */
            'superFamily' => 'required|string',
            'infra_order_id' => 'required|exists:mysql_insect.insect_infra_orders,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function familyRules(): array
    {
        return [
            /* 'family' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_families')->ignore($this->id, 'id')
            ], */
            'family' => 'required|string',
            'order_id' => 'required|exists:mysql_insect.insect_orders,id',
            'sub_order_id' => 'nullable|exists:mysql_insect.insect_sub_orders,id',
            'infra_order_id' => 'nullable|exists:mysql_insect.insect_infra_orders,id',
            'super_family_id' => 'nullable|exists:mysql_insect.insect_super_families,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function subFamilyRules(): array
    {
        return [
            /* 'subFamily' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_sub_families')->ignore($this->id, 'id')
            ], */
            'subFamily' => 'required|string',
            'family_id' => 'required|exists:mysql_insect.insect_families,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function tribeRules(): array
    {
        return [
            /* 'tribe' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_tribes')->ignore($this->id, 'id')
            ], */
            'tribe' => 'required|string',
            'sub_family_id' => 'required|exists:mysql_insect.insect_sub_families,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function genusRules(): array
    {
        return [
            /* 'genus' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_genuses')->ignore($this->id, 'id')
            ], */
            'genus' => 'required|string',
            'family_id' => 'required|exists:mysql_insect.insect_families,id',
            'sub_family_id' => 'nullable|exists:mysql_insect.insect_sub_families,id',
            'tribe_id' => 'nullable|exists:mysql_insect.insect_tribes,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function subGenusRules(): array
    {
        return [
            /* 'subGenus' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_sub_genuses')->ignore($this->id, 'id')
            ], */
            'subGenus' => 'required|string',
            'genus_id' => 'required|exists:mysql_insect.insect_genuses,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable'
        ];
    }

    protected function specificRules(): array
    {
        return [
            /* 'specific' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_specifics')->ignore($this->id, 'id')
            ], */
            'specific' => 'required|string',
            'genus_id' => 'required|exists:mysql_insect.insect_genuses,id',
            'sub_genus_id' => 'nullable|exists:mysql_insect.insect_sub_genuses,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable',
            'originalNameGenus' => 'nullable',
            'originalNameSubGenus' => 'nullable',
            'originalNameSpecificEpithet' => 'nullable',
            'originalNameInfraSpecificEpithet' => 'nullable',
            'originalNameDate' => 'nullable',
            'originalNameReference_id' => 'nullable|exists:mysql_insect.insect_references,id',
            'originalNameTaxonRank' => 'nullable',
            'originalNameTaxonRemarks' => 'nullable',
            'originalNameLanguage' => 'nullable',
            'originalNameRights' => 'nullable',
            'originalNameLicense' => 'nullable',
            'originalNameInstitutionID' => 'nullable',
            'originalNameInstitutionCode' => 'nullable',
            'originalNameRightSholder' => 'nullable',
            'originalNameAccessRights' => 'nullable',
            'originalNameSource' => 'nullable',
            'originalNameDatasetID' => 'nullable',
            'originalNameDatasetName' => 'nullable'
        ];
    }

    protected function infraSpecificRules(): array
    {
        return [
            /* 'infraSpecific' => [
                'required',
                'string',
                Rule::unique('mysql_insect.insect_infra_specifics')->ignore($this->id, 'id')
            ], */
            'infraSpecific' => 'required|string',
            'specific_id' => 'required|exists:mysql_insect.insect_specifics,id',
            'reference_id' => 'required|exists:mysql_insect.insect_references,id',
            'taxonRank' => 'nullable',
            'taxonRemarks' => 'nullable',
            'language' => 'nullable',
            'rights' => 'nullable',
            'license' => 'nullable',
            'institutionID' => 'nullable',
            'institutionCode' => 'nullable',
            'rightSholder' => 'nullable',
            'accessRights' => 'nullable',
            'source' => 'nullable',
            'datasetID' => 'nullable',
            'datasetName' => 'nullable',
            'originalNameGenus' => 'nullable',
            'originalNameSubGenus' => 'nullable',
            'originalNameSpecificEpithet' => 'nullable',
            'originalNameInfraSpecificEpithet' => 'nullable',
            'originalNameDate' => 'nullable',
            'originalNameReference_id' => 'nullable|exists:mysql_insect.insect_references,id',
            'originalNameTaxonRank' => 'nullable',
            'originalNameTaxonRemarks' => 'nullable',
            'originalNameLanguage' => 'nullable',
            'originalNameRights' => 'nullable',
            'originalNameLicense' => 'nullable',
            'originalNameInstitutionID' => 'nullable',
            'originalNameInstitutionCode' => 'nullable',
            'originalNameRightSholder' => 'nullable',
            'originalNameAccessRights' => 'nullable',
            'originalNameSource' => 'nullable',
            'originalNameDatasetID' => 'nullable',
            'originalNameDatasetName' => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'kingdom.unique' => 'Kingdom already exists',
            'phylum.unique' => 'Phylum already exists',
            'class.unique' => 'Class already exists',
            'order.unique' => 'Order already exists',
            'subOrder.unique' => 'Suborder already exists',
            'infraOrder.unique' => 'Infraorder already exists',
            'superFamily.unique' => 'Superfamily already exists',
            'family.unique' => 'Family already exists',
            'subfamily.unique' => 'Subfamily already exists',
            'tribe.unique' => 'Tribe already exists',
            'genus.unique' => 'Genus already exists',
            'subGenus.unique' => 'Subgenus already exists',
            'specific.unique' => 'Species already exists',
            'infraSpecific.unique' => 'Subspecies already exists',
            
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
