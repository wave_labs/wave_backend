<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\Source;

class StoreIotDeviceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        //
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'serial_number' => 'required|string|unique:mysql_iot.iot_device',
            'id_type' => 'required',
            'id_environment' => 'required',
            'id_state' => 'required',
            'template_id' => 'required|exists:mysql_iot.iot_templates,id',
            'category' => 'required',
            'category_latitude' => ['required-if:category,1', 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'category_longitude' => ['required-if:category,1', 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'fields' => 'nullable',
            'sections' => 'required_unless:template_id,1',
            'section_title' => 'nullable',
            'users.*' => 'required|exists:users,id',
            'gateways.*' => 'required|exists:mysql_iot.iot_device,id',
            'photo' => 'nullable|mimes:jpeg,png,jpg,gif,svg,webp'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'photo.image' => 'Invalid photo format, it must be jpeg, png, jpg, svg, or webp',
            'category_latitude.regex' => 'The latitude format is invalid. Latitude range from -90 to 90',
            'category_longitude.regex' => 'The longitude format is invalid. Longitude range from -180 to 80',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
