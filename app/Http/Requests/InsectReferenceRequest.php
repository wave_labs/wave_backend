<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class InsectReferenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $response = "";

        if (!empty($this->authors)) {

            foreach ($this->authors as $index => $aAuthor) {
                $separator = ", ";

                if ($index == count($this->authors) - 1) {
                    $separator = "";
                } else if ($index == count($this->authors) - 2) {
                    $separator = " & ";
                }

                $response = $response . $aAuthor . $separator;
            }

            $this->merge([
                'author' => $response,
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'nullable|string',
            'identifier' => 'nullable',
            'description' => 'nullable',
            'bibliographicCitation' => 'nullable',
            'editorList' => 'nullable',
            'date' => 'nullable',
            'journal' => 'nullable',
            'chapterTitle' => 'nullable',
            'editor' => 'nullable',
            'volume' => 'nullable',
            'pages' => 'nullable',
            'pageStart' => 'nullable',
            'pageEnd' => 'nullable',
            'edition' => 'nullable',
            'publisher' => 'nullable',
            'language' => 'nullable',
            'subject' => 'nullable',
            'taxonRemarks' => 'nullable',
            'type' => 'nullable',
            'validated' => 'nullable',
            'datasetName' => 'nullable',
            'datasetID' => 'nullable',
            'modified' => 'nullable',
            'institutionID' => 'nullable',
            'collectionID' => 'nullable',
            'institutionCode' => 'nullable',
            'collectionCode' => 'nullable',
            'ownerInstitutionCode' => 'nullable',
            'issue' => 'nullable',
            'degree' => 'nullable',
            'university' => 'nullable',
            'conferenceTittle' => 'nullable',
            'accessDate' => 'nullable',
            'project_id' => 'nullable|integer|exists:mysql_insect.insect_projects,id'
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
