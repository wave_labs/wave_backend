<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use JWTAuth;

class UpdateMeRequest extends FormRequest
{
    private $user;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->header('Authorization'))
            $this->user = JWTAuth::setToken($this->header('Authorization'))->user();

        $this->merge([
            'userable_type' => $this->user->userable_type,
            'user_id' => $this->user->id
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => Rule::unique('users')->ignore($this->user->id),
            'user_id' => 'required',
            'userable_type' => 'required|string',
            'name' => 'nullable|string',
            'note' => 'nullable|min:1|max:1000',

            'country' => 'nullable|string',
            'gender' => 'nullable|string',
            'b_day' => 'nullable|date',
            'user_occupation_id' => 'nullable|integer|exists:user_occupations,id',
            'number_dives' => 'nullable|string',

            'latitude' => 'nullable|string',
            'longitude' => 'nullable|string',
            'address' => 'nullable|string',
            'members' => 'nullable',
            'members.*' => 'exists:users,email|email',

        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'latitude.regex' => 'The latitude format is invalid. Latitude range from -90 to 90',
            'longitude.regex' => 'The longitude format is invalid. Longitude range from -180 to 80',
            'members.*.exists' => 'User :input does not exist.',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
