<?php

namespace App\Http\Requests;

use App\LitterSubCategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\User;
use JWTAuth;

class StoreLitterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->header('Authorization'))
            $user = JWTAuth::setToken($this->header('Authorization'))->user()->id;
        else if ($this->user_id == null || $this->user_id == "undefined" || !$this->user_id)
            $user = User::getGuest();
        else $user = $this->user_id;

        $this->litter_category ? $categories = $this->litter_category : $categories = [];

        if ($this->litter_sub_category) {
            foreach ($this->litter_sub_category as $element) {
                $sub = LitterSubCategory::find($element);

                if (!in_array($sub->litter_category_id, $categories)) {
                    array_push($categories, $sub->litter_category_id);
                }
            }
        }

        $this->merge([
            'user_id' => $user,
            'litter_category' => $categories
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer|exists:users,id',
            'latitude' => ['required', 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'longitude' => ['required', 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'date' => 'required|date',
            'collected' => 'nullable|boolean',
            'multiple' => 'nullable|boolean',
            'source' => 'required|string|in:core,beach,seafloor,floating,micro,biota,dive',
            'quantity' => 'required_if:source,beach|string|in:1-5,5-10,10-50,>50',
            'radius' => 'required_if:source,beach|string|in:<1,1-5,5-20,>20',
            'litter_sub_category' => 'nullable',
            'litter_sub_category.*' => 'exists:litter_sub_categories,id',
            'litter_category' => 'nullable',
            'litter_category.*' => 'exists:litter_categories,id',
            'photo' => 'required_if:source,beach|required_if:source,floating|mimes:jpeg,png,jpg,gif,svg,webp',
            'dive_id' => 'required_if:source,dive|integer|exists:dives,id',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'photo.image' => 'Invalid photo format, it must be jpeg, png, bmp, gif, svg, or webp',
            'source.required' => 'Report source is required.',
            'latitude.regex' => 'The latitude format is invalid. Latitude range from -90 to 90',
            'longitude.regex' => 'The longitude format is invalid. Longitude range from -180 to 80',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
