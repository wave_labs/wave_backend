<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class UpdateDiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'diving_spot_id' => 'required|integer|exists:diving_spots,id',
            'max_depth' => 'required|integer',
            'dive_time' => 'required|integer',
            'number_diver' => 'required|integer',
            'date' => 'required|date',
            'creatures' => 'nullable',
            'creatures.*' => 'integer|distinct|exists:creatures,id',
            'abundance' => 'nullable',
            'abundance.*' => 'string',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.exists' => 'The selected user is invalid.',
            'diving_spot_id.exists' => 'The selected diving spot is invalid.',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
