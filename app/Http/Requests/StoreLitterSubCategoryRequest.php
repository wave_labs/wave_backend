<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class StoreLitterSubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:litter_sub_categories',
            'litter_category_id' => 'required|integer|exists:litter_categories,id',
            'OSPAR_code' => 'nullable|string',
            'UNEP_code' => 'nullable|string',
            'Core' => 'nullable|boolean',
            'Beach' => 'nullable|boolean',
            'Seafloor' => 'nullable|boolean',
            'Floating' => 'nullable|boolean',
            'Biota' => 'nullable|boolean',
            'Micro' => 'nullable|boolean',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
