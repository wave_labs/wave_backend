<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $role = null;

        if ($this->admin == 1) $role = "admin";

        if ($this->route()->user->hasRole('guest')) {
            if ($role == null) $role = "guest";
        } else if ($this->userable_type == 'App\UserDiveCenter') {
            if ($role == null) $role = "dive-center";
        } else if ($this->userable_type == 'App\UserPerson') {
            if ($role == null) $role = "person";
        }

        $this->merge([
            'role' => $role,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => Rule::unique('users')->ignore($this->route()->user->id),
            'userable_type' => 'required|string',
            'role' => 'required|string|in:person,dive-center,admin,guest',
            'active' => 'nullable|boolean',
            'is_verified' => 'nullable|boolean',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'latitude.regex' => 'The latitude format is invalid. Latitude range from -90 to 90',
            'longitude.regex' => 'The longitude format is invalid. Longitude range from -180 to 80',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
