<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\LitterSubCategory;

class UpdateLitterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->litter_category ? $categories = $this->litter_category : $categories = [];

        if ($this->litter_sub_category) {
            foreach ($this->litter_sub_category as $element) {
                $sub = LitterSubCategory::find($element);

                if (!in_array($sub->litter_category_id, $categories)) {
                    array_push($categories, $sub->litter_category_id);
                }
            }
        }

        $this->merge([
            'litter_category' => $categories
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'latitude' => ['required', 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'longitude' => ['required', 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'date' => 'required|date',
            'source' => 'required|string|in:core,beach,seafloor,floating,micro,biota,dive',
            'collected' => 'nullable|boolean',
            'multiple' => 'nullable|boolean',
            'quantity' => 'nullable|required_if:source,beach|string|in:1-5,5-10,10-50,>50',
            'radius' => 'nullable|required-if:source,beach|string|in:<1,1-5,5-20,>20',
            'litter_sub_category' => 'nullable',
            'litter_sub_category.*' => 'exists:litter_sub_categories,id',
            'litter_category' => 'nullable',
            'litter_category.*' => 'exists:litter_categories,id',
            'photo' => 'nullable'
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
