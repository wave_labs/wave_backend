<?php

namespace App\Http\Requests;

use App\Helper;
use App\InsectSinonimia;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class InsectReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $coordinatesString = null;
        if ($this->latitude && $this->longitude) {
            $degrees = [];
            $minutes = [];
            $seconds = [];
            $direction = [];

            $latitude = explode(".", $this->latitude);
            array_push($degrees, str_replace("-", "", $latitude[0]));
            $latitude_degrees = explode(".", floatval("." . $latitude[1]) * 60);
            array_push($minutes, $latitude_degrees[0]);
            array_push($seconds, round(floatval("." . $latitude_degrees[1]) * 60, 2));
            if (floatval($latitude[0]) > 0) {
                array_push($direction, 'N');
            } else array_push($direction, 'S');

            $longitude = explode(".", $this->longitude);
            array_push($degrees, str_replace("-", "", $longitude[0]));
            $longitude_degrees = explode(".", floatval("." . $longitude[1]) * 60);
            array_push($minutes, $longitude_degrees[0]);
            array_push($seconds, round(floatval("." . $longitude_degrees[1]) * 60, 2));
            if (floatval($longitude[0]) > 0) {
                array_push($direction, 'E');
            } else array_push($direction, 'W');

            $coordinatesString =
                $degrees[0] . "º" . $minutes[0] . "'" . $seconds[0] . "''" . $direction[0] . ", " .
                $degrees[1] . "º" . $minutes[1] . "'" . $seconds[1] . "''" . $direction[1];
        }
        $sinonimia = null;
        if ($this->sinonimia) {
            $sinonimia = InsectSinonimia::where('name', $this->sinonimia)->where('species_id', $this->species[1])->first();

            if (!$sinonimia) {
                $sinonimia = InsectSinonimia::create([
                    'name' => $this->sinonimia,
                    'species_id' => $this->species[1]
                ]);
            }
        }

        $this->merge([
            'species_id' => $this->species[1],
            'sinonimia_id' => $sinonimia ? $sinonimia->id : $sinonimia,
            'start' => gettype($this->date) == "array" ? $this->date[0] : $this->date,
            'end' => gettype($this->date) == "array" ? $this->date[1] : $this->date,
            'date' => gettype($this->date) == "array" ? null : $this->date,
            'coordinates' => $coordinatesString
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'zone_id' => 'required-without:region|exists:mysql_insect.insect_zones,id',
            'region' => 'required-without:zone_id|string',
            'province' => 'required-with:region|string',
            'locality' => 'required-with:region|string',
            'reference_id' => 'nullable|exists:mysql_insect.insect_references,id',
            'sinonimia_id' => 'nullable|exists:mysql_insect.insect_sinonimias,id',
            'species_id' => 'required|exists:mysql_insect.insect_species,id',
            'user_id' => 'nullable|exists:users,id',
            'start' => 'required-without:date|date',
            'end' => 'required-without:date|date',
            'date' => 'required-without:start',
            'date_type' => 'required|string',
            'collector' => 'nullable|string',
            'page' => 'nullable|string',
            'n_male' => 'nullable|integer',
            'n_female' => 'nullable|integer',
            'n_children' => 'nullable|integer',
            'latitude' => ['nullable', 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'longitude' => ['nullable', 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'coordinates' => 'nullable|string',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
