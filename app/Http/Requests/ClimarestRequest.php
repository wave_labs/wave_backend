<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ClimarestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'area.activitiesRestrictions' => 'required|string',
            'area.adjacentOwnership' => 'required|string',
            'area.areaActivities' => 'required|string',
            'area.climateVulnerability' => 'required|string',
            'area.conflicts' => 'required|string',
            'area.contreversialMethods' => 'required|string',
            'area.distanceShore' => 'required|string',
            'area.marineRights' => 'required|string',
            'area.name' => 'required|string',
            'area.natureSolutions' => 'required|string',
            'area.ownership' => 'required|string',
            'area.protectedArea' => 'required|array|min:1',
            'area.protectedArea.*.lat' => ['required', 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'area.protectedArea.*.lng' => ['required', 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'area.protectedArea.*.label' => 'required|string',
            'area.restorationArea' => 'required|string',
            'area.resultRestrictions' => 'required|string',
            'area.technologicSolutions' => 'required|string',
            'area.typeArea' => 'required|string',

            'stakeholder.*.*.affect' => 'required|string',
            'stakeholder.*.*.affectRestoration' => 'required|string',
            'stakeholder.*.*.engage' => 'required|array|min:0|max:5',
            'stakeholder.*.*.levelInfluence' => 'required|string',
            'stakeholder.*.*.levelInterest' => 'required|string',
            'stakeholder.*.*.levelTrust' => 'required|string',
            'stakeholder.*.*.name' => 'required|string',
            'stakeholder.*.*.priority' => 'required|string',
            'stakeholder.*.*.role' => 'required|string',
            'stakeholder.*.*.type' => 'required|string',
            'stakeholder.*.*.typeEngagement' => 'required|array|min:0|max:5',

        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
