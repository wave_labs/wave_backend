<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\User;
use App\Feedback;
use App\Question;
use JWTAuth;

class StoreFormResultRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->header('Authorization'))
            $user = JWTAuth::setToken($this->header('Authorization'))->user()->id;
        else if ($this->user_id == null || $this->user_id == "undefined" || !$this->user_id)
            $user = User::getGuest();
        else $user = $this->user_id;

        $this->merge([
            'user_id' => $user
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arrayLength = Question::countQuestions($this->form_id);

        return [
            'user_id' => 'required|integer|exists:users,id',
            'uniqid' => 'nullable|string',
            'form_id' => 'required|integer|exists:forms,id',
            'source_id' => 'required|integer|exists:sources,id',
            'questions' => 'required|array|size:' . $arrayLength,
            'questions.*' => 'integer',
            'ratings' => 'required|array|size:' . $arrayLength,
            'ratings.*' => 'integer',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'questions.size' => 'The number of questions does not match with the form',
            'ratings.size' => 'The number of answers does not match with the number of questions',
        ];
    }


    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
