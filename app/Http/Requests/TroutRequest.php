<?php

namespace App\Http\Requests;

use App\Helper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Arr;

class TroutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $sightings = [];
        if ($this->sightingsA) {
            foreach ($this->sightingsA as $sight) {
                array_push($sightings, [
                    "animal" => $sight["animal"],
                    "latitude" => $sight["latitude"],
                    "stream" => $sight["stream"],
                    "longitude" => $sight["longitude"],
                    "location" => Arr::get($sight, "location") ?  $sight["location"] : "I don't remember",
                    "year" => $sight["year"]
                ]);
            }
        }
        if ($this->sightingsB) {
            foreach ($this->sightingsB as $sight) {
                array_push($sightings, [
                    "animal" => $sight["animal"],
                    "latitude" => $sight["latitude"],
                    "stream" => $sight["stream"],
                    "longitude" => $sight["longitude"],
                    "location" => Arr::get($sight, "location") ?  $sight["location"] : "I don't remember",
                    "year" => $sight["year"]
                ]);
            }
        }
        if ($this->sightingsC) {
            foreach ($this->sightingsC as $sight) {
                array_push($sightings, [
                    "animal" => $sight["animal"],
                    "latitude" => $sight["latitude"],
                    "stream" => $sight["stream"],
                    "longitude" => $sight["longitude"],
                    "location" => Arr::get($sight, "location") ?  $sight["location"] : "I don't remember",
                    "year" => $sight["year"]
                ]);
            }
        }

        $this->merge([
            'knowledge' => implode(" / ", $this->knowledge),
            'sightings' => $sightings
        ]);
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'age' => 'required|string',
            'sightings' => 'nullable|array',
            'sightings.*.animal' => 'required|string',
            'sightings.*.year' => 'required|string',
            'sightings.*.stream' => 'required|string',
            'sightings.*.location' => 'required',
            'sightings.*.latitude' => 'required',
            'sightings.*.longitude' => 'required',
            'knowledge' => 'required|string',
            'nameA' => 'nullable|string',
            'nameB' => 'nullable|string',
            'nameC' => 'nullable|string',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
