<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use JWTAuth;

class StoreDivingSpotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->header('Authorization'))
            $user = JWTAuth::setToken($this->header('Authorization'))->user()->id;
        else if ($this->user_id == null || $this->user_id == "undefined" || !$this->user_id)
            $user = User::getGuest();
        else $user = $this->user_id;

        $isAdmin = JWTAuth::setToken($this->header('Authorization'))->user()->hasRole('admin');

        $this->merge([
            'user_id' => $user,
            'validated' => $isAdmin ? 1 : 0
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'validated' =>  'required|integer',
            'user_id' => 'required|integer|exists:users,id',
            'name' => 'required|string|unique:diving_spots',
            'range' => 'nullable|string',
            'latitude' => ['required', 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'longitude' => ['required', 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'substract' => 'nullable|array',
            'substract.*' => 'required|integer|exists:diving_spot_substracts,id',
            'protection' => 'required|string|in:Marine Protected Area,Natural Reserve,None',
            'description' => 'nullable|string|min:3|max:250',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'latitude.regex' => 'The latitude format is invalid. Latitude range from -90 to 90',
            'longitude.regex' => 'The longitude format is invalid. Longitude range from -180 to 80',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
