<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\User;
use JWTAuth;

class StoreSightingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->header('Authorization'))
            $user = JWTAuth::setToken($this->header('Authorization'))->user()->id;
        else if ($this->user_id == null || $this->user_id == "undefined" || !$this->user_id)
            $user = User::getGuest();
        else $user = $this->user_id;

        $this->merge([
            'user_id' => $user
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|integer|exists:users,id',
            'capsule_id' => 'nullable|integer|exists:capsules,id',
            'trip_id' => 'nullable|integer|exists:trips,id',
            'creature_id' => 'required|integer|exists:creatures,id',
            'vehicle_id' => 'nullable|integer|exists:vehicles,id',
            'behaviour_id' => 'nullable|integer|exists:sighting_behaviours,id',
            'beaufort_scale_id' => 'nullable|integer|exists:beaufort_scale,id',
            'date' => 'required|date',
            'latitude' => ['required', 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'longitude' => ['required', 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,20})?))$/'],
            'report_source' => 'nullable|max:100',
            'notes' => 'nullable|max:350',
            'calves' => 'nullable|integer',
            'is_group_mixed' => 'nullable|boolean',
            'group_size' => 'nullable|integer',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'latitude.regex' => 'The latitude format is invalid. Latitude range from -90 to 90',
            'longitude.regex' => 'The longitude format is invalid. Longitude range from -180 to 80',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
