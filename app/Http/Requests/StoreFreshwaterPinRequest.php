<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class StoreFreshwaterPinRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        //
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'creature_id' => 'nullable|integer|exists:creatures,id',
            'form.age' => 'required|string',
            'form.animal' => 'required|string',
            'form.council' => 'nullable|string',
            'form.knowledge' => 'required|string',
            'form.location' => 'nullable|string',
            'form.nameA' => 'nullable|string',
            'form.nameB' => 'nullable|string',
            'form.period_day' => 'required|string',
            'form.period_year' => 'required|string',
            'form.quantity' => 'required|string',
            'form.residence' => 'required|string',
            'form.stream' => 'required|string',
            'form.year' => 'required|string',
            'form.observations' => 'nullable|string',
            'positions.*.*' => 'nullable',
            'positions.*.*' => 'nullable',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'positions.*.*.0.regex' => 'The latitude format is invalid. Latitude range from -90 to 90',
            'positions.*.*.1.regex' => 'The longitude format is invalid. Longitude range from -180 to 80',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
