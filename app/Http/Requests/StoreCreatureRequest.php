<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\Source;

class StoreCreatureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $sources = [];

        $diveReporter = Source::where('name', 'DIVE_REPORTER')->value('id');
        $whaleReporter = Source::where('name', 'WHALE_REPORTER')->value('id');

        if ($this->dive_center_app == 1) {
            array_push($sources, $diveReporter);
        }
        if ($this->whale_reporter_app == 1) {
            array_push($sources, $whaleReporter);
        }

        $this->merge([
            'sources' => $sources,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:creatures',
            'name_scientific' => 'required|string|unique:creatures',
            'type_id' => 'required|integer|exists:creature_types,id',
            'conserv' => 'required|string|in:Extinct,Extinct in the Wild,Critical Endangered,Endangered,Vulnerable,Near Threatened,Least Concern,Data Deficient,Not Evaluated',
            'size' => 'required|string',
            'group_size' => 'nullable|string',
            'calves' => 'nullable|integer',
            'nis_status' => 'required|string|in:Native,Non-Indigenous Species',
            'show_order' => 'nullable',
            'description' => 'required|string',
            'curiosity' => 'required|string',
            'sources' => 'nullable',

            'level_1' => 'nullable|integer',
            'level_2' => 'nullable|integer',
            'level_3' => 'nullable|integer',
            'level_4' => 'nullable|integer',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.unique' => 'There is already a creature with that name.',
            'name_scientific.unique' => 'There is already a creature with that scientific name.',
            'type_id.exists' => 'Not an existing creature type.',
            'type_id.required' => 'Creature type is required.',
        ];
    }

    /**
     * Return validation errors as json response
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], 422));
    }
}
