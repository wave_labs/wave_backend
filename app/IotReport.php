<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

use App\IotDevice;
use Carbon\Carbon;
use stdClass;

class IotReport extends Model
{
    use FiltersRecords;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'iot_device_report';
    protected $fillable = ['id_device', 'collected_at'];
    protected $appends = ['coordinates', 'date'];
    protected $connection = 'mysql_iot';

    public function getCoordinatesAttribute()
    {
        $coordinates = [32.6, -16.8];

        foreach ($this->fields as $field) {
            $field = $field->field;
            if ($field) {
                $features = $field->features;

                foreach ($features as $feature) {

                    if ($feature->name == 'latitude') {
                        $coordinates[0] = (float) $field->value;
                    } else if ($feature->name == 'longitude') {
                        $coordinates[1] = (float) $field->value;
                    }
                }
            }
        }

        return $coordinates;
    }

    public static function generateCustomGraph($device, $date, $graph)
    {
        $from = Carbon::parse($date[0]);
        $to = Carbon::parse($date[1]);

        $reports = IotReport::where('id_device', $device)
            ->whereBetween('collected_at', [$from, $to])
            ->orderBy('collected_at')
            ->take(1440)->get();

        $values = [];
        $labels = [];

        foreach ($reports as $report) {

            array_push($labels, Carbon::parse($report->collected_at)->toDateTimeString());

            foreach ($report->fields as $field) {

                if ($field->device_field_id == $graph) {
                    array_push($values, $field->value);
                }
            }
        }
        return [
            'data' => [
                'values' => $values,
                'labels' => $labels,
            ]
        ];
    }

    public static function getTimeline($device)
    {
        $first = Carbon::now()->subHour();
        $last = Carbon::now();
        $period = [];
        $average = [];
        $max = [];
        $significant = [];
        $labels = [];
        $reports = self::whereIdDevice($device)->whereBetween("collected_at", [$first, $last])->orderBy('collected_at')->get();

        foreach ($reports as $key => $report) {
            array_push($labels, Carbon::parse($report->collected_at)->toTimeString());

            foreach ($report->fields as $key => $field) {
                switch ($field->type->name) {
                    case 'Wave Period':
                        array_push($period, (float) $field->value);
                        break;
                    case 'Wave Height Average':
                        array_push($average, (float) $field->value);
                        break;
                    case 'Wave Height Max':
                        array_push($max,  (float) $field->value);
                        break;
                    case 'Wave Height Significant':
                        array_push($significant, (float) $field->value);
                        break;
                    default:
                        break;
                }
            }
        }
        return [$period, $average, $max, $significant, $labels];
    }

    public static function getFieldsTimeline($device, $aFields)
    {
        $values = []; // graph values with Y axis labels
        $labels = []; // graph X axis labels

        foreach ($aFields as $aField) $values[$aField] = [];

        $reports = IotReport::where('id_device', $device)->latest()->limit(50)->get();

        foreach ($reports as $report) {
            array_push($labels, Carbon::parse($report->created_at)->toTimeString());
            foreach ($report->fields as $field) {
                $key = $field->field->key;


                if (array_key_exists($key, $values)) {
                    array_push($values[$key], (float) $field->value);
                }
            }
        }
        return [
            'labels' => $labels,
            'values' => $values,
        ];
    }

    public function getDateAttribute()
    {
        $date = "";
        foreach ($this->fields as $field) {
            if (strtolower($field->field->key) == 'date' || strtolower($field->field->key) == 'time') {
                $date = $date . $field->value . " ";
            }
        }
        return $date ==  "" ? "Not specified" : $date;
    }

    public function validateFields($value, $field_id)
    {
        $validated = true;
        $isTimestamp = false;
        $epsilon = 0.00001;

        $rules = IotRule::whereHas('fields', function ($query) use ($field_id) {
            $query->where('field_id', $field_id);
        })->get();

        foreach ($rules as $rule) {
            switch ($rule->name) {
                case 'latitude':

                    if (abs((int) $value - 0) < $epsilon) {
                        $validated = false;
                    }
                    $regex = "/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,20})?))$/";

                    if (!preg_match($regex, $value)) {
                        $validated = false;
                    }
                    break;
                case 'longitude':
                    if (abs((int) $value - 0) < $epsilon) {
                        $validated = false;
                    }
                    $regex = "/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,20})?))$/";
                    if (!preg_match($regex, $value)) {
                        $validated = false;
                    }
                    break;

                case 'timestamp':
                    if ($value === "0:0:0") {
                        $validated = false;
                    } else {
                        $startDate = Carbon::createFromFormat('d-m-Y', '01-01-2020');
                        $endDate = Carbon::now();

                        if (!Carbon::createFromTimestamp($value)->between($startDate, $endDate) || $value == "0-0-2020") {
                            $validated = false;
                        }
                    }
                    $isTimestamp = true;
                    break;
                default:
                    # code...
                    break;
            }
        }
        $object = new stdClass;
        $object->validated = $validated;
        $object->timestamp = $isTimestamp;

        return $object;
    }


    public function fields()
    {
        return $this->hasMany('App\IotReportField', 'report_id');
    }

    public function device()
    {
        return $this->hasOne(IotDevice::class, 'id', 'id_device');
    }

    public function scopeGetRecordsFromUser($query, $users)
    {
        if ($users)
            return $query->whereHas('device', function ($query) use ($users) {
                $query->whereHas('users', function ($query) use ($users) {
                    $query->whereIn('user_id', $users);
                });
            });
        else
            return $query;
    }
}
