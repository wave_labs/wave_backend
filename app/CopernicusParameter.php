<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CopernicusParameter extends Model
{
    //

    public function products()
    {
        return $this->belongsToMany('App\CopernicusProduct', 'copernicus_has_parameters');
    }
}
