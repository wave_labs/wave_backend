<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoftCoralCoordinate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'soft_coral_coordinates';
    protected $connection = 'mysql_studies';

    protected $fillable = [
        'latitude',
        'longitude',
        'soft_coral_id',
        'year',
        'dimensions',
        'dimension_change',
        'depth',
    ];

    public function softCoral()
    {
        return $this->belongsTo('App\SoftCoral');
    }
}
