<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotDeviceHasSection extends Model
{
    protected $connection = 'mysql_iot';
}
