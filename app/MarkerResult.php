<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\StudyQuestion;

class MarkerResult extends Model
{
    protected $fillable = ['answer', 'study_question_id', 'marker_id'];



    public function marker()
    {
        return $this->belongsTo('App\Marker');
    }

    public function question()
    {
        return $this->belongsTo('App\StudyQuestion', 'study_question_id');
    }
}
