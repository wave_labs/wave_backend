<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

use App\MarkerActivity;
use App\MarkerCoordinates;
use App\MarkerCreatureSize;
use App\MarkerResult;
use App\StudyQuestion;
use App\Activity;

class Marker extends Model
{
    use FiltersRecords;

    protected $fillable = ['creature_id'];

    public function storeActivity($activities, $coordinates)
    {
        foreach ($activities as $key => $value) {
            $markerActivity = MarkerActivity::create([
                'experience' => $value[0],
                'frequency' => $value[1],
                'time' => $value[2],
                'activity_id' => Activity::find($key)->id,
                'marker_id' => $this->id,

            ]);

            foreach ($coordinates[$key] as $v) {
                MarkerCoordinates::create([
                    'latitude' => $v[0],
                    'longitude' => $v[1],
                    'marker_activity_id' => $markerActivity->id,
                ]);
            }
        }
    }

    public function storeResults($form)
    {
        foreach ($form as $key => $value) {
            $question = StudyQuestion::whereCode($key)->first();

            MarkerResult::create([
                'answer' => $value,
                'marker_id' => $this->id,
                'study_question_id' => $question->id,
            ]);
        }
    }

    public function storeCreatureSize($size)
    {
        MarkerCreatureSize::create([
            'marker_id' => $this->id,
            'sm' => $size['sm'],
            'md' => $size['md'],
            'lg' => $size['lg'],
        ]);
    }

    public function creature()
    {
        return $this->belongsTo('App\Creature');
    }

    public function activities()
    {
        return $this->hasMany('App\MarkerActivity');
    }

    public function results()
    {
        return $this->hasMany('App\MarkerResult');
    }

    public function creatureSize()
    {
        return $this->hasOne('App\MarkerCreatureSize');
    }
}
