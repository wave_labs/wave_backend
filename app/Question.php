<?php

namespace App;

use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasTranslations;

    protected $fillable = ['form_id', 'question', 'scale'];
    public $translatable = ['question'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'questions';

    /**
     * Get feedback type
     */
    public function form()
    {
        return $this->belongsTo('App\Form');
    }

    /**
     * Get feedback results
     */
    public function result()
    {
        return $this->hasMany('App\Result');
    }

    /**
     * Get the number of questions belonging to a form
     */
    public static function countQuestions($form)
    {
        return Question::where('form_id', $form)->count();
    }
}
