<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrainwaveReportHasAnnotation extends Model
{
    protected $connection = 'mysql_brainwave';
}
