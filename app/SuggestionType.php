<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cerbero\QueryFilters\FiltersRecords;

class SuggestionType extends Model
{
    use FiltersRecords;
    use HasTranslations;

    protected $fillable = ['name'];
    public $translatable = ['name'];

    public function suggestions()
    {
        return $this->hasMany('App\Suggestion');
    }
}
