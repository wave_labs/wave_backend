<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectAuthor extends Model
{
    protected $fillable = [
        'name'
    ];
    protected $connection = 'mysql_insect';

    use FiltersRecords;

    public function references()
    {
        return $this->belongsToMany('App\InsectReport', 'insect_reference_has_authors', 'author_id', 'reference_id');
    }

    public function identifications()
    {
        return $this->hasMany('App\InsectOccurrence');
    }
}
