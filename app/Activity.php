<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Spatie\Translatable\HasTranslations;

class Activity extends Model
{
    use FiltersRecords, HasTranslations;

    public $translatable = ['name'];

    public function creatures()
    {
        return $this->belongsToMany('App\Creature', 'creature_activity_pivots');
    }

    public function markerActivities()
    {
        return $this->hasMany('App\MarkerActivity');
    }
}
