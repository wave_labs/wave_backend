<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LitterReport extends Model
{
    //
    protected $fillable = ['litter_id', 'reason'];

    public function litter()
    {
        return $this->belongsTo('App\Litter');
    }
}
