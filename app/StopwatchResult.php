<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class StopwatchResult extends Model
{
    use FiltersRecords;
    
    protected $fillable = [
        'video_id',
        'occupation',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function video()
    {
        return $this->belongsTo('App\StopwatchVideo', 'video_id');
    }

    public function stopwatchTimestamps()
    {
        return $this->hasMany('App\StopwatchTimestamp', 'result_id');
    }
}
