<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dome extends Model
{

    public static function getClosest($aLat, $aLon)
    {
        $domes = Dome::whereActive(true)->get();

        if ($domes->count()) {
            $coordinates = DomePosition::whereTo(null)->get(['id', 'latitude', 'longitude']);

            $distances = [];
            foreach ($coordinates as $coordinate) {

                array_push($distances, self::distance($coordinate, [$aLat, $aLon]));
            }

            asort($distances);

            return $coordinates[key($distances)]->id;
        }

        return 0;
    }

    private static function distance($a, $b)
    {
        $theta = $a->longitude - $b[1];

        $dist = sin(deg2rad($a->latitude)) * sin(deg2rad($b[0])) +  cos(deg2rad($a->latitude)) * cos(deg2rad($b[0])) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);

        return $dist;
    }



    public function arReports()
    {
        return $this->hasMany('App\ArDome');
    }

    public function counters()
    {
        return $this->hasMany('App\DomeCounter');
    }

    public function positions()
    {
        return $this->hasMany('App\DomePosition');
    }

    public function interactions()
    {
        return $this->belongsToMany('App\DomeInteraction', 'dome_has_interactions');
    }
}
