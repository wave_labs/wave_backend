<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Carbon\Carbon;
use App\Coordinate;

class Sighting extends Model
{

    use FiltersRecords;

    protected $fillable = [
        'user_id',
        'capsule_id',
        'creature_id',
        'trip_id',
        'beaufort_scale_id',
        'date',
        'latitude',
        'longitude',
        'is_group_mixed',
        'group_size',
        'time',
        'report_source',
        'calves',
        'vehicle_id',
        'behaviour_id',
        'notes'
    ];
    protected $appends = ['class'];
    protected $table = 'sightings';

    public function getClassAttribute()
    {
        return "WHALE_REPORTER";
    }

    /**
     * Get sighting history of a user
     */
    public static function getActivity($user_id)
    {
        return static::where('user_id', $user_id)->with('creature')->with('vehicle')->with('beaufort_scale')->with('behaviour')->orderBy('date', 'desc')->limit(4)->get();
    }

    public function scopeGetPosts($query, $users)
    {
        if ($users)
            return $query->whereIn('user_id', $users);
        else
            return $query;
    }

    /**
     * Pick an arbitrary level of precision, and group your points into X degree buckets. 
     * Pick the bucket with the most points in it and take the average of those points.
     */
    public static function getAverage()
    {
        $lat = Sighting::pluck('latitude')->toArray();
        $long = Sighting::pluck('longitude')->toArray();

        $latrange = Coordinate::AverageCoordinate($lat);
        $longrange = Coordinate::AverageCoordinate($long);

        $latitude = Sighting::whereBetween('latitude', $latrange)->avg('latitude');
        $longitude = Sighting::whereBetween('longitude', $longrange)->avg('longitude');

        $coordinates = [$latitude, $longitude];

        return $coordinates;
    }

    public static function reportsPerMonth($users)
    {
        $date = Carbon::now()->firstOfMonth()->subMonths(11);
        $total = [];

        for ($i = 0; $i <= 11; $i++) {

            $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
            $last = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();

            $sightings = Sighting::whereBetween("date", [$first, $last])->fromUsers($users)->count();

            //$range = getRange($sightings);

            $total[$date->format('M')] = $sightings;

            $date->addMonths(1);
        }

        return $total;
    }

    public static function reportsPerMonthPerCreature($filter, $users)
    {
        $filter ? $date = Carbon::createFromFormat('Y-m', $filter) : $date = Carbon::now()->firstOfMonth()->subMonths(11);

        $total = [];

        for ($i = 0; $i <= 11; $i++) {
            $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
            $last = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();

            $values = [];

            $source_id = Source::whereName('WHALE_REPORTER')->value('id');
            $creatures = Creature::whereHas('sources', function ($query) use ($source_id) {
                $query->whereSourceId($source_id);
            })->get();

            foreach ($creatures as $creature) {
                $sightings = Sighting::whereBetween("date", [$first, $last])->whereCreatureId($creature->id)->fromUsers($users)->count();


                $values[$creature->name] = $sightings;
            }

            $total[$date->format('M')] = $values;

            if ($filter) break;
            else $date->addMonths(1);
        }

        return $total;
    }

    public static function reportsPerMonthPerSpecificCreature($creature, $filter)
    {
        $filter ? $date = Carbon::createFromFormat('Y-m', $filter) : $date = Carbon::now()->firstOfMonth()->subMonths(11);
        $total = [];

        for ($i = 0; $i <= 11; $i++) {
            $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
            $last = Carbon::now()->month($date->month)->year($date->year)->lastOfMonth();

            $values = [];

            $sightings = Sighting::whereBetween("date", [$first, $last])->whereCreatureId($creature->id)->count();

            $range = getRange($sightings);

            $values[$creature->name] = $range;


            $total[$date->format('M')] = $values;

            if ($filter) break;
            else $date->addMonths(1);
        }

        return $total;
    }

    public function scopeFromUsers($query, $users)
    {
        if ($users)
            return $query->whereIn('user_id', $users);
        else
            return $query;
    }

    //Relations

    public function trip()
    {
        return $this->belongsTo('App\Trip');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function creature()
    {
        return $this->belongsTo('App\Creature');
    }

    public function capsule()
    {
        return $this->belongsTo('App\Capsule');
    }

    public function beaufort_scale()
    {
        return $this->belongsTo('App\BeaufortScale');
    }

    public function behaviour()
    {
        return $this->belongsTo('App\SightingBehaviour');
    }

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }

    public function data()
    {
        return $this->hasMany('App\SightingsData');
    }

    public function acoustic()
    {
        return $this->hasManyThrough('App\Acoustic', 'App\SightingsData');
    }

    public function estimations()
    {
        return $this->hasMany('App\SightingsEstimation');
    }

    public function sighting_photo()
    {
        return $this->hasMany('App\SightingPhoto');
    }
}
