<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    public static function get($type)
    {
    	return Version::pluck($type)->first();
    }

    public static function updateVersion($type)
    {
        self::query()->increment($type, 1);
    }
}
