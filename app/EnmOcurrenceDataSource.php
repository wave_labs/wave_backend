<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnmOcurrenceDataSource extends Model
{
    //

    public function enm()
    {
        return $this->hasMany('App\Enm', 'enm_ocurrence_data_source_id');
    }
}
