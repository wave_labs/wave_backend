<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnmModel extends Model
{
    public function predictionTypes()
    {
        return $this->hasMany('App\EnmModelPrediction', 'model_id');
    }

    public function enms()
    {
        return $this->belongsToMany('App\Enm', 'enm_has_models');
    }
}
