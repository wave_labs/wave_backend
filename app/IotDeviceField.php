<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotDeviceField extends Model
{
    protected $fillable = ['device_id', 'key'];
    protected $connection = 'mysql_iot';
    public function device()
    {
        return $this->belongsTo('App\IotDevice', 'device_id');
    }

    public function reports()
    {
        return $this->hasMany('App\IotReportField', 'device_field_id');
    }

    public function fieldFact()
    {
        return $this->hasMany(IotFieldFact::class, 'field_id');
    }

    public function features()
    {
        return $this->belongsToMany(IotFeature::class, 'iot_field_facts', 'field_id', 'feature_id')
            ->withPivot('section_id');
    }

    public function rules()
    {
        return $this->belongsToMany(IotRule::class, 'iot_device_field_has_rules', 'field_id', 'rule_id');
    }
}
