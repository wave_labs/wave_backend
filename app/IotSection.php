<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotSection extends Model
{
    protected $connection = 'mysql_iot';

    public function template()
    {
        return $this->belongsToMany(IotTemplate::class, 'iot_template_has_sections', 'section_id', 'template_id');
    }

    public function devices()
    {
        return $this->belongsToMany(IotDevice::class, 'iot_device_has_sections', 'section_id', 'device_id')->withPivot('title');
    }
}
