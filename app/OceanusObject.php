<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OceanusObject extends Model
{
    protected $fillable =
    [
        'detection_id',
        'class_id',
        'accuracy',
        'xmin',
        'ymin',
        'xmax',
        'ymax',
    ];

    public function detection()
    {
        return $this->belongsTo('App\Detection');
    }
    
}
