<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectPhylum extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'phylum', 'kingdom_id', 'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function kingdom()
    {
        return $this->belongsTo(InsectKingdom::class, 'kingdom_id');
    }

    public function classes()
    {
        return $this->hasMany(InsectClass::class);
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }
}
