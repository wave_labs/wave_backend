<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotDeviceHasIotDevice extends Model
{
    protected $connection = 'mysql_iot';
}
