<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IotFeature extends Model
{
    protected $fillable = ['name', 'has_timeline'];
    protected $table = 'iot_features';
    protected $connection = 'mysql_iot';
    public function fieldFact()
    {
        return $this->hasMany(IotFieldFact::class, 'feature_id');
    }

    public function fields()
    {
        return $this->belongsToMany(IotDeviceField::class, 'iot_field_facts', 'feature_id', 'field_id')->withPivot('section_id');
    }

    public function parameters()
    {
        return $this->belongsToMany(IotParameter::class, 'iot_feature_has_parameters', 'feature_id', 'parameter_id');
    }
}
