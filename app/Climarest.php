<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Climarest extends Model
{
    public function area()
    {
        return $this->hasOne('App\ClimarestAreaForm');
    }

    public function stakeholder()
    {
        return $this->hasMany('App\ClimarestStakeholdersForm');
    }
}
