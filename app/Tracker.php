<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Intervention\Image\Facades\Image as IMG;

class Tracker extends Model
{
    use FiltersRecords;

    public $fillable = ['path', 'specie', 'predict', 'date'];
    public $table = 'tracker';

    public static function storeImage($file)
    {
        if ($file->isValid()) {
            $filename = uniqid() . '.png';

            IMG::make($file)->encode('png', 65)->resize(760, null, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            })->save(public_path('/images/tracker/' . $filename));

            return '/images/tracker/' . $filename;
        }
    }
}
