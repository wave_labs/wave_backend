<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsNewMessage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message', 'whats_new_type_id'
    ];


    public function whatsNew()
    {
        return $this->belongsToMany('App\WhatsNew', 'whats_new_message_pivot', 'whats_new_message_id', 'whats_new_id');
    }

    public function whatsNewType()
    {
        return $this->belongsTo('App\WhatsNewType');
    }
}
