<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class IotDeviceAlertEmail extends Mailable
{
    public $tries = 3;
    use Queueable, SerializesModels;

    public $deviceName;
    public $deviceId;
    public $activity;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($aDeviceName, $aDeviceId, $aActivity)
    {
        $this->deviceName = $aDeviceName;
        $this->deviceId = $aDeviceId;
        $this->activity = $aActivity;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.iot-device-activity')->subject('Device activity alert');
    }
}
