<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;


class UserDiveCenter extends Model
{
    use FiltersRecords;

    protected $fillable = ['name', 'longitude', 'latitude', 'address'];

    /**
     * Get the user_dive_center's user.
     */
    public function user()
    {
        return $this->morphOne('App\User', 'userable');
    }

    public function persons()
    {
        return $this->belongsToMany('App\UserPerson', 'company_has_users');
    }
}
