<?php

namespace App;

use App\Form;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['form_id', 'user_id', 'score', 'source_id', 'uniqid'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'feedbacks';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function source()
    {
        return $this->belongsTo('App\Source');
    }

    public function form()
    {
        return $this->belongsTo('App\Form');
    }

    public function result()
    {
        return $this->hasMany('App\Result');
    }

    /**
     * Get average form score
     */
    public static function getAverageScore($form_id, $ratings)
    {
        $form_name = Form::where('id', $form_id)->value('form');
        $score = 0;

        if ($form_name == 'PANAS-SF') {
            $score = $ratings[0] + $ratings[2] + $ratings[4] + $ratings[8] + $ratings[9] + $ratings[11] + $ratings[13] + $ratings[15] + $ratings[16] + $ratings[18] - $ratings[1] - $ratings[3] - $ratings[5] - $ratings[6] - $ratings[7] - $ratings[10] - $ratings[12] - $ratings[14] - $ratings[17] - $ratings[19];
        } else if ($form_name == 'SUS') {
            $score = (($ratings[0] - 1) + ($ratings[2] - 1) + ($ratings[4] - 1) + ($ratings[6] - 1) + ($ratings[8] - 1) + (5 - $ratings[1]) + (5 - $ratings[3]) + (5 - $ratings[5]) + (5 - $ratings[7]) + (5 - $ratings[9])) * 2.5;
        } else if ($form_name == 'NEP') {
            $score = $ratings[0] + $ratings[1] + $ratings[2] + $ratings[3] + $ratings[4] + $ratings[5] + $ratings[6] + $ratings[7] + $ratings[8] + $ratings[9] + $ratings[10] + $ratings[11] + $ratings[12] + $ratings[13] + $ratings[14];
        }

        return $score;
    }
}
