<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class IotDeviceType extends Model
{
    use FiltersRecords;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'iot_device_type';
    protected $fillable = [
        'name',
        'description',
        'icon'
    ];
    protected $connection = 'mysql_iot';

}
