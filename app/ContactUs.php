<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    public $fillable = ['name','email','message', 'subject', 'section'];
    public $table = 'contact_uses';
}
