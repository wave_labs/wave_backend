<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrainwaveReport extends Model
{
    protected $connection = 'mysql_brainwave';
    protected $fillable = ['user_id', 'file', 'annotations', 'og_values'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function annotations()
    {
        return $this->belongsToMany(BrainwaveAnnotation::class, 'wave_brainwave.brainwave_report_has_annotations', 'annotation_id', 'report_id');
    }
}
