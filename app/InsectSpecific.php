<?php

namespace App;

use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;

class InsectSpecific extends Model
{
    use FiltersRecords;
    protected $fillable = [
        'specific',
        'genus_id',
        'sub_genus_id',
        'reference_id',
        'taxonRank',
        'taxonRemarks',
        'language',
        'rights',
        'license',
        'institutionID',
        'institutionCode',
        'rightSholder',
        'accessRights',
        'source',
        'datasetID',
        'datasetName'
    ];
    protected $connection = 'mysql_insect';

    public function subSpecific()
    {
        return $this->hasMany(InsectInfraSpecific::class);
    }

    public function originalName()
    {
        return $this->hasOne(InsectOriginalName::class, 'specific_id', 'id');
    }

    public function sinonimia()
    {
        return $this->hasMany(InsectSinonimia::class, 'specific_id', 'id');
    }

    public function genus()
    {
        return $this->belongsTo(InsectGenus::class, 'genus_id');
    }

    public function subGenus()
    {
        return $this->belongsTo(InsectSubGenus::class, 'sub_genus_id');
    }

    public function reference()
    {
        return $this->belongsTo(InsectReference::class, 'reference_id');
    }

    public function occurrenceSpecific()
    {
        return $this->belongsTo(InsectOccurrence::class);
    }

    public function occurrenceSubSpecific()
    {
        return $this->belongsTo(InsectInfraSpecific::class);
    }

    public function occurrenceReference()
    {
        return $this->belongsTo(InsectReference::class);
    }

    
}
