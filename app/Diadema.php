<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class Diadema extends Model
{
    use FiltersRecords;

    protected $fillable = [
        "dead",
        "sick",
        "latitude",
        "longitude"
    ];
}
