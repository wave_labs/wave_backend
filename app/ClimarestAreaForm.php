<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClimarestAreaForm extends Model
{
    protected $fillable = [
        "climarest_id", "typeArea",
        "restorationArea",
        "distanceShore",
        "ownership",
        "adjacentOwnership",
        "marineRights",
        "resultRestrictions",
        "areaActivities",
        "activitiesRestrictions",
        "technologicSolutions",
        "natureSolutions",
        "climateVulnerability",
        "conflicts",
        "contreversialMethods",
        "name"
    ];

    public static function store($validator)
    {
        return self::create([
            'climarest_id' => $validator['climarest_id'],
            'activitiesRestrictions' => $validator["area"]["activitiesRestrictions"],
            'adjacentOwnership' => $validator["area"]["adjacentOwnership"],
            'areaActivities' => $validator["area"]["areaActivities"],
            'climateVulnerability' => $validator["area"]["climateVulnerability"],
            'conflicts' => $validator["area"]["conflicts"],
            'contreversialMethods' => $validator["area"]["contreversialMethods"],
            'distanceShore' => $validator["area"]["distanceShore"],
            'marineRights' => $validator["area"]["marineRights"],
            'name' => $validator["area"]["name"],
            'natureSolutions' => $validator["area"]["natureSolutions"],
            'ownership' => $validator["area"]["ownership"],
            'restorationArea' => $validator["area"]["restorationArea"],
            'resultRestrictions' => $validator["area"]["resultRestrictions"],
            'technologicSolutions' => $validator["area"]["technologicSolutions"],
            'typeArea' => $validator["area"]["typeArea"],
        ]);
    }

    public function climarest()
    {
        return $this->belongsTo('App\Climarest');
    }

    public function coordinates()
    {
        return $this->hasMany('App\ClimarestAreaCoordinates');
    }
}
