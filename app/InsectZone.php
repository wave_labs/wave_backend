<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectZone extends Model
{
    protected $fillable = ['region', 'province', 'locality', 'nameunit'];
    protected $connection = 'mysql_insect';


    public function reports()
    {
        return $this->hasMany('App\InsectReport', 'species_id');
    }
}
