<?php

namespace App\Jobs;

use App\GpsGatewaySatellite;
use App\GpsTagSatellite;
use App\Helper;
use Illuminate\Support\Facades\DB;
use App\IotDevice;
use App\IotReport;
use App\IotReportField;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Validator;

class StoreIotReport implements ShouldQueue
{
    public $tries = 1;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $validator;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($validator)
    {
        $this->validator = $validator;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->validator['payload'] as $payloadReport) {
            $rules = [
                'device' => 'required|exists:mysql_iot.iot_device,id',
            ];
            $validator = Validator::make($payloadReport, $rules);

            if ($validator->passes()) {
                $device = IotDevice::findOrFail($payloadReport["device"]);

                foreach ($payloadReport["fields"] as $payload) {
                    DB::beginTransaction();
                    try {
                        $report = IotReport::create(['id_device' => $device->id]);

                        $fields = $device->fields;
                        foreach ($fields as $field) {

                            $key = $field->key;
                            if (array_key_exists($key, $payload)) {
                                if (is_array($payload[$key])) {
                                    foreach ($payload[$key] as $counter => $satelliteData) {

                                        if (array_key_exists('satID', $satelliteData)) {
                                            $hasSatNumCheck = false;
                                            if (array_key_exists('sat_num', $payload)) {
                                                $hasSatNumCheck = true;
                                            }

                                            if ($hasSatNumCheck) {
                                                if ($counter <= $payload['sat_num']) {

                                                    if (count($satelliteData) > 3) {
                                                        GpsGatewaySatellite::create([
                                                            'lat' => $payload['lat'],
                                                            'lon' => $payload['lon'],
                                                            'gpsWeek' => $payload['gpsWeek'],
                                                            'gpsTimeWeek_M7' => $payload['gpsTimeWeek_M7'],
                                                            'gpsTimeWeek_M41' => $payload['gpsTimeWeek_M41'],
                                                            'clockBias_M7' => $payload['clockBias_M7'],
                                                            'satID' => $satelliteData['satID'],
                                                            'clockBias_M30' => $satelliteData['clockBias_M30'],
                                                            'gpsTime' => $satelliteData['gpsTime'],
                                                            'gpsSoftwareTime' => $satelliteData['gpsSoftwareTime'],
                                                            'pseudorange' => $satelliteData['pseudorange'],
                                                            'positionX' => $satelliteData['positionX'],
                                                            'positionY' => $satelliteData['positionY'],
                                                            'positionZ' => $satelliteData['positionZ'],
                                                            'velocityX' => $satelliteData['velocityX'],
                                                            'velocityY' => $satelliteData['velocityY'],
                                                            'velocityZ' => $satelliteData['velocityZ'],
                                                            'iot_report_id' => $report->id,
                                                        ]);
                                                    } else {
                                                        if ($satelliteData['pseudorange'] != 0 ||  $satelliteData['satID'] != 0) {
                                                            GpsTagSatellite::create([
                                                                'satID' => $satelliteData['satID'],
                                                                'pseudorange' => $satelliteData['pseudorange'],
                                                                'gpsWeek' => $payload['gpsWeek'],
                                                                'gpsTimeWeek' => $payload['gpsTimeWeek'],
                                                                'iot_report_id' => $report->id,
                                                            ]);
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (count($satelliteData) > 3) {
                                                    GpsGatewaySatellite::create([
                                                        'lat' => $payload['lat'],
                                                        'lon' => $payload['lon'],
                                                        'gpsWeek' => $payload['gpsWeek'],
                                                        'gpsTimeWeek_M7' => $payload['gpsTimeWeek_M7'],
                                                        'gpsTimeWeek_M41' => $payload['gpsTimeWeek_M41'],
                                                        'clockBias_M7' => $payload['clockBias_M7'],
                                                        'satID' => $satelliteData['satID'],
                                                        'clockBias_M30' => $satelliteData['clockBias_M30'],
                                                        'gpsTime' => $satelliteData['gpsTime'],
                                                        'gpsSoftwareTime' => $satelliteData['gpsSoftwareTime'],
                                                        'pseudorange' => $satelliteData['pseudorange'],
                                                        'positionX' => $satelliteData['positionX'],
                                                        'positionY' => $satelliteData['positionY'],
                                                        'positionZ' => $satelliteData['positionZ'],
                                                        'velocityX' => $satelliteData['velocityX'],
                                                        'velocityY' => $satelliteData['velocityY'],
                                                        'velocityZ' => $satelliteData['velocityZ'],
                                                        'iot_report_id' => $report->id,
                                                    ]);
                                                } else {
                                                    if ($satelliteData['pseudorange'] != 0 ||  $satelliteData['satID'] != 0) {
                                                        GpsTagSatellite::create([
                                                            'satID' => $satelliteData['satID'],
                                                            'pseudorange' => $satelliteData['pseudorange'],
                                                            'gpsWeek' => $payload['gpsWeek'],
                                                            'gpsTimeWeek' => $payload['gpsTimeWeek'],
                                                            'iot_report_id' => $report->id,
                                                        ]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $status = $report->validateFields($payload[$key], $field->id);
                                    if ($status->validated) {
                                        IotReportField::create([
                                            'value' => $payload[$key],
                                            'report_id' => $report->id,
                                            'device_field_id' => $field->id
                                        ]);

                                        if ($status->timestamp) {
                                            $report->collected_at = Carbon::createFromTimestamp($payload[$key])->toDateTimeString();
                                            $report->save();
                                        }
                                    }
                                }
                            }
                        }

                        DB::commit();
                    } catch (\Throwable $th) {
                        DB::rollBack();
                    }
                }
            }
        }
    }
}
