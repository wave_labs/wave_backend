<?php

namespace App\Jobs;

use App\BrainwaveLog;
use App\BrainwaveReport;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Spatie\Ssh\Ssh;
use Symfony\Component\Process\Process;

class HandleBrainwaveJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    public $timeout = 1860;
    protected $filename;
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($aFilename, $aId)
    {
        $this->filename = $aFilename;
        $this->id = $aId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $process = Ssh::create('eeg', '10.20.4.1', 10022)->disablePasswordAuthentication()->execute('python3 datasets/eeg/eeg.py ' . $this->filename)->setTimeout(1500);

        if ($process->isSuccessful()) {
            BrainwaveLog::create(["message" => "script ran of file " . $this->filename]);
            $brainwave = BrainwaveReport::find($this->id);
            $brainwave->finished = Carbon::now();
            $brainwave->save();
        } else {
            BrainwaveLog::create(["message" => "script failed on file " . $this->filename]);
        }

        $zipprocess = Ssh::create('eeg', '10.20.4.1', 10022)->disablePasswordAuthentication()->execute('python3 datasets/eeg/zip.py ' . $this->filename)->setTimeout(300);

        if ($zipprocess->isSuccessful()) {
            BrainwaveLog::create(["message" => "zipped outputs of directory " . $this->filename]);
        } else {
            BrainwaveLog::create(["message" => "zip failed on directory " . $this->filename]);
        }

        return $process;
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addMinutes(90);
    }
}
