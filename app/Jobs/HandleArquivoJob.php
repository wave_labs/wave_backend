<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Spatie\Ssh\Ssh;
use Symfony\Component\Process\Process;

class HandleArquivoJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    public $timeout = 1860;
    protected $filename;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($aFilename)
    {
        $this->filename = $aFilename;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $process = Ssh::create('eeg', '10.20.4.1', 10022)->disablePasswordAuthentication()->execute('ls');

        return $process;
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addMinutes(90);
    }
}
