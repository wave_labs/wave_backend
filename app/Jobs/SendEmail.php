<?php

namespace App\Jobs;

use App\Mail\IotDeviceAlertEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    public $tries = 3;
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $deviceName;
    protected $deviceId;
    protected $activity;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($aEmail, $aDeviceName, $aDeviceId, $aActivity)
    {
        $this->email = $aEmail;
        $this->deviceName = $aDeviceName;
        $this->deviceId = $aDeviceId;
        $this->activity = $aActivity;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->email)->queue(new IotDeviceAlertEmail($this->deviceName, $this->deviceId, $this->activity));
    }
}
