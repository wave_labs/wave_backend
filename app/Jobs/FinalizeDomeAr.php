<?php

namespace App\Jobs;

use App\ArDome;
use App\DomeHasInteraction;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FinalizeDomeAr implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $tries = 1;

    protected $report;
    protected $interaction;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($aReport, $aInteraction)
    {
        $this->report = $aReport;
        $this->interaction = $aInteraction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $arDome = ArDome::find($this->report);
        !$arDome->finalized && DomeHasInteraction::decrementCounter($arDome->dome_id, $this->interaction);
    }
}
