<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomeInteraction extends Model
{


    public function domes()
    {
        return $this->belongsToMany('App\Dome', 'dome_has_interactions');
    }
}
