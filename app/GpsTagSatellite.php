<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GpsTagSatellite extends Model
{
    protected $fillable = [
        'satID',
        'pseudorange',
        'gpsWeek',
        'gpsTimeWeek',
        'iot_report_id'
    ];
    protected $connection = 'mysql_iot';

    public function reports()
    {
        return $this->belongsTo('App\IotReport', 'iot_report_id');
    }
}
