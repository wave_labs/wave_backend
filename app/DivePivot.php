<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Dive;
use App\DivingSpot;
use Carbon\Carbon;

class DivePivot extends Model
{
    protected $fillable =
    [
        'creature_id',
        'abundance_value',
        'dive_id'
    ];

    public function dive()
    {
        return $this->belongsToOne('App\Dive');
    }

    public function creature()
    {
        return $this->belongsTo('App\Creature');
    }

    public static function storePivot($current_creature, $current_abundance, $dive)
    {
        if (!is_null($current_creature) && !is_null($current_abundance)) {
            $data = [
                'creature_id' => $current_creature,
                'abundance_value' => $current_abundance,
                'dive_id' => $dive->id,
            ];
            DivePivot::firstOrCreate($data);
        }
    }

    public static function getAverage($creature)
    {
        //Due to the fact that we return all diving spots from a specific month
        // We need to check if the value is null before sorting it

        $start = Carbon::now()->subMonths(12);
        $i = 0;
        $total = [];

        while ($i <= 11) {
            if ($i == 0) {
                $month_counter = $start->month;
                $year_counter = $start->year;
            }

            //Returns all diving spots
            $diving_spots = DivingSpot::all();

            //Array to store current dives
            $abundance_values = [];
            //Array to store monthly abundances
            $values = [];

            foreach ($diving_spots as $diving_spot) {
                //Get all dives for this diving_spot
                $dives = Dive::whereBetween("date", [Carbon::now()->month($month_counter)->year($year_counter)->firstOfMonth(), Carbon::now()->month($month_counter)->year($year_counter)->lastOfMonth()])
                    ->with(['dive_pivot' => function ($query) use ($creature) {
                        $query->where('creature_id', $creature);
                    }])->where('diving_spot_id', $diving_spot->id)->pluck('id');


                //Get the abundance of this creature for each dive
                foreach ($dives as $dive_id) {
                    $abundance = DivePivot::where('creature_id', $creature)->where('dives_id', $dive_id)->value('abundance_value');
                    //Store abundance values on the array
                    if (!is_null($abundance)) {
                        array_push($abundance_values, $abundance);
                    }
                }

                if (!is_null($abundance_values)) {
                    //Get values of ocorrence of each abundance
                    $array = array_count_values($abundance_values);
                    //Get the most common option
                    arsort($array);
                    //Store it on a variable
                    $element = key($array);

                    $values[$diving_spot->name] = $element;
                }

                //Reset array for next iteration
                $abundance_values = [];
            }

            $total[Carbon::now()->month($month_counter)->year($year_counter)->format('M')] = $values;

            if ($month_counter == 12) {
                $month_counter = 1;
                $year_counter++;
            } else
                $month_counter++;

            $i++;
        }

        return $total;
    }
}
