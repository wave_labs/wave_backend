<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectOccurrenceHasRecordedByAuthor extends Model
{
    protected $connection = 'mysql_insect';

    protected $fillable =
    [
        'author_id',
        'occurrence_id',
    ];
}
