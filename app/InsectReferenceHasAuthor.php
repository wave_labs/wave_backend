<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsectReferenceHasAuthor extends Model
{
    protected $connection = 'mysql_insect';

    protected $fillable =
    [
        'author_id',
        'reference_id',
    ];
}
