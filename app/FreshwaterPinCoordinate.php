<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreshwaterPinCoordinate extends Model
{
    protected $fillable = [
        'latitude',
        'longitude',
        'freshwater_pin_id',
        'timeframe',
        'image'
    ];

    public function freshwaterPin()
    {
        return $this->belongsTo('App\FreshwaterPin');
    }
}
