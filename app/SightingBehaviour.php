<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class SightingBehaviour extends Model
{
    use HasTranslations;


    protected $fillable = ['behaviour'];
    public $translatable = ['behaviour'];

    public function sightings()
    {
        return $this->hasMany('App\Sighting');
    }
}
