<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class VehicleType extends Model
{
    use HasTranslations;
    
    protected $fillable = ['type'];
    public $translatable = ['type'];

    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }
}
