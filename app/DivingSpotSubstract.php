<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DivingSpotSubstract extends Model
{
    protected $fillable =
    [
        "name",
    ];

    public function divingSpots()
    {
        return $this->belongsToMany(DivingSpot::class, 'diving_spot_has_substracts');
    }
}
