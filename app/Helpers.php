<?php

namespace App;

use Exception;
use Illuminate\Http\Request;
use JWTAuth;

if (!function_exists('getNextLevel')) {
    function getNextLevel($points)
    {
        $values = [];
        $current_level = null;
        $next_level = null;

        $levels = config('constant.LEVELS');

        foreach ($levels as $level) {
            if ($points > 500000) {
                $current_level = 5;
                $next_level = null;
            } else if ($points >= $level[1] && $points < $level[2]) {
                $current_level = $level[0];
                $next_level = $level[2];
                break;
            }
        }

        $values = [
            'points' => $points,
            'level' => $current_level,
            'next_level' => $next_level,
        ];

        return $values;
    }
}

if (!function_exists('getRange')) {
    function getRange($value)
    {
        $defaultRange = config('constant.DEFAULT_RANGE');
        $range = null;

        if ($value >= $defaultRange[0] && $value <= $defaultRange[1])
            $range = $defaultRange[0] . " - " . $defaultRange[1];
        else if ($value >= $defaultRange[2] && $value <= $defaultRange[3])
            $range = $defaultRange[2] . " - " . $defaultRange[3];
        else if ($value >= $defaultRange[4] && $value <= $defaultRange[5])
            $range = $defaultRange[4] . " - " . $defaultRange[5];
        else if ($value >= $defaultRange[6] && $value <= $defaultRange[7])
            $range = $defaultRange[6] . " - " . $defaultRange[7];
        else if ($value > $defaultRange[7])
            $range = $defaultRange[8];

        return $range;
    }
}

if (!function_exists('getRangeValues')) {
    function getRangeValues()
    {
        $defaultRange = config('constant.DEFAULT_RANGE');

        return
            [
                'level_1' => $defaultRange[0] . " - " . $defaultRange[1],
                'level_2' => $defaultRange[2] . " - " . $defaultRange[3],
                'level_3' => $defaultRange[4] . " - " . $defaultRange[5],
                'level_4' => $defaultRange[6] . " - " . $defaultRange[7],
                'level_5' => $defaultRange[8],
            ];
    }
}

if (!function_exists('getUserFromRequest')) {
    function getUserFromRequest(Request $request, $forceSelf)
    {
        $user = null;
        $users = [];

        if ($request->self || $forceSelf) {
            if ($request->header('Authorization')) {
                $user = auth()->user();

                array_push($users, $user->id);
                
                if ($user->userable_type == "App\UserDiveCenter") {
                    $persons = $user->userable->persons;
                    foreach ($persons as $person) {
                        array_push($users, $person->id);
                    }
                }
            }
            $user->hasRole("admin") && $users = [];
        }

        return $users;
    }
}

if (!function_exists('getFieldTranslation')) {
    function getFieldTranslation(Request $request, $field, $model)
    {
        return $model->getTranslation($field, $request->language ? $request->language : 'en');
    }
}

if (!function_exists('uniqueString')) {
    function getUniqueString($length)
    {
        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($length / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $length);
    }
}
