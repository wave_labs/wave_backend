<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image as IMG;
use Illuminate\Support\Facades\Storage;

class Helper extends Model
{
    public static function getHelperRangeValues()
    {
        return getRangeValues();
    }

    public static function getHelperNextLevel($points)
    {
        return getNextLevel($points);
    }

    public static function getUserFromRequest($arg, $forceSelf = false)
    {
        return getUserFromRequest($arg, $forceSelf);
    }

    public static function getFieldTranslation($request, $field, $model)
    {
        return getFieldTranslation($request, $field, $model);
    }

    public static function printToConsole($arg)
    {
        $print = new \Symfony\Component\Console\Output\ConsoleOutput();
        $print->writeln($arg);
    }

    public static function storePhoto($photo, $path, $disk = 'wave-server', $extension = '.jpg')
    {
        $filename = uniqid() . $extension;
        $photo->storeAs($path, $filename, 'wave-server');
        Storage::disk($disk)->setVisibility($path . '/' . $filename, 'public');

        return $filename;
    }

    public static function getPhoto($path)
    {
        return IMG::make(Storage::disk('wave-server')->get($path))->response();
    }

    public static function deletePhoto($path)
    {
        return Storage::disk('wave-server')->delete($path);
    }


    public static function savePhoto($path)
    {
        return IMG::make(Storage::disk('wave-server')->get($path));
    }
}
