<?php

namespace App\Console\Commands;

use App\Vehicle;
use Illuminate\Console\Command;

class TranslateVehicle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:vehicle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $vehicles = array(
            array('id' => '1', 'vehicle' => '{"en":"Austronesian ship","pt":"Navio austronésio"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '2', 'vehicle' => '{"en":"Barges","pt":"Barcaça"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '3', 'vehicle' => '{"en":"Canoe‎","pt":"Canoa"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '4', 'vehicle' => '{"en":"Catamaran","pt":"Catamarã"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '5', 'vehicle' => '{"en":"Chesapeake Bay boat","pt":"Barco de Baía"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '6', 'vehicle' => '{"en":"Dinghie","pt":"Bote"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '7', 'vehicle' => '{"en":"Electric boat","pt":"Barco elétrico"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '8', 'vehicle' => '{"en":"Ferry","pt":" Balsa"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '9', 'vehicle' => '{"en":"Fireboat","pt":"Barco de bombeiros"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '10', 'vehicle' => '{"en":"Fishing vessel","pt":"Barco de pesca"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '11', 'vehicle' => '{"en":"High-speed craft‎","pt":"Barco rápido"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '12', 'vehicle' => '{"en":"Houseboat","pt":"Casa flutuante"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '13', 'vehicle' => '{"en":"Hydrofoil","pt":"Hidrofólio"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '14', 'vehicle' => '{"en":"Indigenous boat","pt":"Barco indígena"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '15', 'vehicle' => '{"en":"Inflatable boat","pt":"Barco insuflável"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '16', 'vehicle' => '{"en":"Kayak","pt":"caiaque"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '17', 'vehicle' => '{"en":"Lifeboat","pt":"Barco salva vidas"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '18', 'vehicle' => '{"en":"Military boat","pt":"Barco militar"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '19', 'vehicle' => '{"en":"Model boat","pt":"Barco modelo"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '20', 'vehicle' => '{"en":"Riverboat","pt":"Barco de rio"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '21', 'vehicle' => '{"en":"Rowing boat","pt":"Barco a remos"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '22', 'vehicle' => '{"en":"Sailboat","pt":"Barco à vela"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '23', 'vehicle' => '{"en":"Scow","pt":"Barcaça"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '24', 'vehicle' => '{"en":"Ship’s boat","pt":"Navio"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '25', 'vehicle' => '{"en":"Sponge diving boat","pt":"Barco de mergulho"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '26', 'vehicle' => '{"en":"Steamboat","pt":"Barco a vapor"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '27', 'vehicle' => '{"en":"Towboat","pt":"Rebocador"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '28', 'vehicle' => '{"en":"Traditional boat","pt":"Barco tradicional"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '29', 'vehicle' => '{"en":"Tugboat","pt":"Barco piloto"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '30', 'vehicle' => '{"en":"Water taxi","pt":"Barco taxi"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '31', 'vehicle' => '{"en":"Windglider‎","pt":"Planador"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '32', 'vehicle' => '{"en":"Yachts","pt":"Iate"}', 'vehicle_type_id' => '5', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15')
        );

        foreach ($vehicles as $question) {
            $dbQuestion = Vehicle::find($question['id']);

            if ($dbQuestion) {
                $dbQuestion->vehicle = $question['vehicle'];
                $dbQuestion->save();
            }
        }
    }
}
