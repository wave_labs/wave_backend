<?php

namespace App\Console\Commands;

use App\CreatureType;
use Illuminate\Console\Command;

class TranslateCreatureType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:creature_type';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $creature_types = array(
            array('id' => '1', 'taxa_category' => '{"en":"Whale","pt":"Baleia"}', 'image' => '/images/creature-type/1', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '2', 'taxa_category' => '{"en":"Plant","pt":"Planta"}', 'image' => '/images/creature-type/2', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '3', 'taxa_category' => '{"en":"Fish","pt":"Peixe"}', 'image' => '/images/creature-type/3', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '4', 'taxa_category' => '{"en":"Coral","pt":"Coral"}', 'image' => '/images/creature-type/4', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '5', 'taxa_category' => '{"en":"Crab","pt":"Caranguejo"}', 'image' => '/images/creature-type/5', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '6', 'taxa_category' => '{"en":"Sessile Invertebrate","pt":"Invertebrado Séssil"}', 'image' => '/images/creature-type/6', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '7', 'taxa_category' => '{"en":"Reptile","pt":"réptil"}', 'image' => '/images/creature-type/7', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '8', 'taxa_category' => '{"en":"Algae","pt":"Alga"}', 'image' => '/images/creature-type/8', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '9', 'taxa_category' => '{"en":"Mollusc","pt":"Molusco"}', 'image' => '/images/creature-type/9', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '10', 'taxa_category' => '{"en":"Turtle","pt":"Tartaruga"}', 'image' => '/images/creature-type/10', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '11', 'taxa_category' => '{"en":"Dolphin","pt":"Golfinho"}', 'image' => '/images/creature-type/11', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '12', 'taxa_category' => '{"en":"Seal","pt":"Foca"}', 'image' => '/images/creature-type/12', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '13', 'taxa_category' => '{"en":"Bird","pt":"Ave"}', 'image' => '/images/creature-type/13', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20'),
            array('id' => '14', 'taxa_category' => '{"en":"Other","pt":"Outro"}', 'image' => '/images/creature-type/14', 'acoustic_data' => '0', 'acoustic_category' => '0', 'created_at' => '2021-01-06 15:27:20', 'updated_at' => '2021-01-06 15:27:20')
        );

        foreach ($creature_types as $question) {
            $dbQuestion = CreatureType::find($question['id']);

            if ($dbQuestion) {
                $dbQuestion->taxa_category = $question['taxa_category'];
                $dbQuestion->save();
            }
        }
    }
}
