<?php

namespace App\Console\Commands;

use App\Helper;
use App\Version;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class SQLUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sql:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the SQLite database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $old = Version::first();

        if ((time() - strtotime($old->dive_updated_at)) <= 3600) {
            $old->dive = $old->dive + 1;
            $old->save();

            exec('php artisan sql:dump --dive');
            exec('php artisan sql:convert --dive --name=' . $old->dive);
        }
        if ((time() - strtotime($old->litter_updated_at)) <= 3600) {
            $old->litter = $old->litter + 1;
            $old->save();
            exec('php artisan sql:dump --litter');
            exec('php artisan sql:convert --litter --name=' . $old->litter);
        }
        if ((time() - strtotime($old->sighting_updated_at)) <= 3600) {
            $old->sighting = $old->sighting + 1;
            $old->save();
            exec('php artisan sql:dump --sighting');
            exec('php artisan sql:convert --sighting --name=' . $old->sighting);
        }
    }
}
