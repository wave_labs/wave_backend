<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Hash;
use Spatie\Permission\Exceptions\RoleDoesNotExist;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create {--dummy}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('dummy')) {
            $user = User::create([
                'name' => 'Admin',
                'gender' => 'm',
                'country' => 'Portugal',
                'userable_type' => 'App\UserPerson',
                'email' => 'admin@admin.wave',
                'password' => Hash::make('admin'),
                'note' => 'admin account',
                'active' => 1,
                'is_verified' => 1
            ]);

            try {
                $user->syncRoles(['admin']);
            } catch (RoleDoesNotExist $e) {
                $this->info('Ooopsie, it seems the role admin doesn\'t exist. Have you run the following command to generate roles and permissions: "php artisan permissions:create"?');
            }
        } else {
            $email = $this->ask('Email');

            $existingUser = User::where('email', '=', $email)->first();

            if ($existingUser) {
                if ($this->confirm('A user with this email exists. Do you want to give it admin permissions?')) {
                    try {
                        $existingUser->syncRoles(['admin']);
                    } catch (RoleDoesNotExist $e) {
                        $this->info('Ooopsie, it seems the role admin doesn\'t exist. Have you run the following command to generate roles and permissions: "php artisan permissions:create"?');
                    }
                }
            } else {
                $password = $this->secret('Password');
                $confirm_password = $this->secret('Confirm Password');

                while ($password != $confirm_password) {
                    $password = $this->secret('Passwords don\'t match. Password:');
                    $confirm_password = $this->secret('Confirm Password');
                }

                $name = $this->ask('Name');

                $country = $this->ask('Country');
                $b_day = $this->ask('Birthday (YYYY-MM-DD)');
                $gender = $this->choice('Gender', ['male', 'female']);
                $gender = 'male' ? 'm' : 'f';


                $user = User::create([
                    'name' => $name,
                    'gender' => $gender,
                    'country' => $country,
                    'b_day' => $b_day,
                    'userable_type' => 'App\UserPerson',
                    'email' => $email,
                    'password' => Hash::make($password),
                    'note' => 'admin account',
                    'active' => 1,
                    'is_verified' => 1
                ]);

                try {
                    $user->syncRoles(['admin']);
                } catch (RoleDoesNotExist $e) {
                    $this->info('Ooopsie, it seems the role admin doesn\'t exist. Have you run the following command to generate roles and permissions: "php artisan permissions:create"?');
                }
            }
        }
    }
}
