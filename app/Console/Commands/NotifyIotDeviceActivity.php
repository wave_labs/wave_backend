<?php

namespace App\Console\Commands;

use App\IotDevice;
use App\IotReport;
use App\Jobs\SendEmail;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NotifyIotDeviceActivity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iot-device:notify  {--type=} {--category=} {--minutes=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify activity for iot devices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $category = $this->option('category');

        $devices = IotDevice::where('id_type', $this->option('type'))->whereHas('categories', function ($query) use ($category) {
            $query->where('iot_device_category_id', $category);
        })->get();
        $treshold = Carbon::now()->subMinutes($this->option('minutes'));

        foreach ($devices as $device) {
            $reports = IotReport::whereIdDevice($device->id)->where('created_at', '>=', $treshold)->get();

            if ($device->updated_at < $treshold) {
                if (!$reports->count()) {
                    if ($device->id_state == 1) {
                        SendEmail::dispatch('vitor.aguiar@iti.larsys.pt', $device->name, $device->id, 'no longer active');
                    }
                } else {
                    if ($device->id_state == 2) {
                        SendEmail::dispatch('vitor.aguiar@iti.larsys.pt', $device->name, $device->id, 'active again');
                    }
                }
            }
        }
    }
}
