<?php

namespace App\Console\Commands;

use App\Abundance;
use App\BeaufortScale;
use App\Creature;
use App\Dive;
use App\DivingSpot;
use App\Litter;
use App\LitterSubCategory;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Sighting;
use App\User;
use App\DivePivot;
use App\SightingBehaviour;
use App\Source;
use App\Vehicle;
use Faker\Factory as Faker;

class FakeData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:data {--litter} {--dive} {--sighting}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate fake data to apps';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $output = new \Symfony\Component\Console\Output\ConsoleOutput();

        $longitude = (float) -16.8;
        $latitude = (float) 32.6;
        $faker = Faker::create();

        if ($this->option('litter')) {
            $date = Carbon::now()->firstOfMonth()->subMonths(12);
            for ($i = 0; $i <= 12; $i++) {
                $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
                $random = rand(1, 100);
                $DiveRandom = rand(1, 30);

                for ($j = 0; $j <= $random; $j++) {
                    $user = User::inRandomOrder()->first();
                    $SubCatRandom = rand(1, 5);
                    $litter_sub_category = LitterSubCategory::inRandomOrder()->limit($SubCatRandom)->get();

                    $radius = rand(5.0, 30.0); // in miles

                    $lng_min = $longitude - $radius / abs(cos(deg2rad($latitude)) * 69);
                    $lng_max = $longitude + $radius / abs(cos(deg2rad($latitude)) * 69);
                    $lat_min = $latitude - ($radius / 69);
                    $lat_max = $latitude + ($radius / 69);

                    $litter = Litter::create([
                        'user_id' => $user->id,
                        'date' => $first,
                        'latitude' => $faker->randomFloat(6, $lat_min, $lat_max),
                        'longitude' => $faker->randomFloat(6, $lng_min, $lng_max),
                        'collected' => rand(1, 10) > 5 ? true : false,
                        'multiple' => rand(1, 10) > 5 ? true : false,
                        'source' => rand(1, 10) > 6 ? 'beach' : 'floating',
                        'quantity' => '1-5',
                        'radius' => '1-5',
                    ]);

                    foreach ($litter_sub_category as $sub_category) {
                        $litter->litterSubCategory()->attach($sub_category->id);
                        $litter->litterCategory()->syncWithoutDetaching($sub_category->litter_category_id);
                    }
                }

                for ($j = 0; $j <= $DiveRandom; $j++) {
                    $user = User::inRandomOrder()->first();
                    $SubCatRandom = rand(1, 5);

                    $litter = Litter::create([
                        'user_id' => $user->id,
                        'date' => $first,
                        'latitude' => '32.6',
                        'longitude' => '-16.8',
                        'collected' => rand(1, 10) > 5 ? true : false,
                        'source' => 'dive',
                    ]);
                }

                $date->addMonths(1);
                $output->writeln($i * 100 / 12 . " %");
            }
        }

        if ($this->option('dive')) {
            $date = Carbon::now()->firstOfMonth()->subMonths(12);
            $source_id = Source::whereName('DIVE_REPORTER')->value('id');

            for ($i = 0; $i <= 12; $i++) {
                $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
                $random = rand(1, 150);

                for ($j = 0; $j <= $random; $j++) {
                    $user = User::inRandomOrder()->first();
                    $diving_spot = DivingSpot::inRandomOrder()->first();
                    $behaviour = SightingBehaviour::inRandomOrder()->first();
                    $beaufort = BeaufortScale::inRandomOrder()->first();

                    $creatures = Creature::whereHas('sources', function ($query) use ($source_id) {
                        $query->whereSourceId($source_id);
                    })->inRandomOrder()->limit(rand(1, 6))->get();

                    $dive = Dive::create([
                        'user_id' => $user->id,
                        'diving_spot_id' => $diving_spot->id,
                        'max_depth' => rand(15, 45),
                        'dive_time' => rand(40, 75),
                        'number_diver' => rand(2, 5),
                        'date' => $first,
                    ]);

                    foreach ($creatures as $element) {
                        $abundances = [];
                        $abundance = Abundance::whereCreatureId($element->id)->first();
                        array_push($abundances, $abundance->level_1, $abundance->level_2, $abundance->level_3, $abundance->level_4);
                        $randomAbundance = $abundances[array_rand($abundances)];

                        $data = [
                            'creature_id' => $element->id,
                            'abundance_value' => $randomAbundance,
                            'dive_id' => $dive->id,
                        ];

                        DivePivot::firstOrCreate($data);
                        $abundances = [];
                    }
                }

                $date->addMonths(1);
                $output->writeln($i * 100 / 12 . " %");
            }
        }

        if ($this->option('sighting')) {
            $date = Carbon::now()->firstOfMonth()->subMonths(12);
            $source_id = Source::whereName('WHALE_REPORTER')->value('id');

            for ($i = 0; $i <= 12; $i++) {
                $first = Carbon::now()->month($date->month)->year($date->year)->firstOfMonth();
                $random = rand(1, 150);

                $creatures = Creature::whereHas('sources', function ($query) use ($source_id) {
                    $query->whereSourceId($source_id);
                })->inRandomOrder()->limit(7)->get();

                for ($j = 0; $j <= $random; $j++) {
                    $user = User::inRandomOrder()->first();
                    $vehicle = Vehicle::inRandomOrder()->first();
                    $behaviour = SightingBehaviour::inRandomOrder()->first();
                    $beaufort = BeaufortScale::inRandomOrder()->first();
                    $creature = $creatures->shuffle()->first();

                    $radius = rand(5.0, 30);

                    $lng_min = $longitude - $radius / abs(cos(deg2rad($latitude)) * 69);
                    $lng_max = $longitude + $radius / abs(cos(deg2rad($latitude)) * 69);
                    $lat_min = $latitude - ($radius / 69);
                    $lat_max = $latitude + ($radius / 69);

                    Sighting::create([
                        'user_id' => $user->id,
                        'creature_id' => $creature->id,
                        'vehicle_id' => $vehicle->id,
                        'behaviour_id' => $behaviour->id,
                        'beaufort_scale_id' => $beaufort->id,
                        'date' => $first,
                        'group_size' => rand(1, 25),
                        'latitude' => $faker->randomFloat(6, $lat_min, $lat_max),
                        'longitude' => $faker->randomFloat(6, $lng_min, $lng_max),
                    ]);
                }

                $date->addMonths(1);
                $output->writeln($i * 100 / 12 . " %");
            }
        }
    }
}
