<?php

namespace App\Console\Commands;

use App\BeaufortScale;
use Illuminate\Console\Command;

class TranslateBeaufortScale extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:beaufort_scale';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $beaufort_scales = array(
            array('id' => '1', 'scale' => '0', 'desc' => '{"en":"Calm","pt":"Calmo"}', 'sea_conditions' => 'Sea like a mirror', 'land_conditions' => 'Smoke rises vertically.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '2', 'scale' => '1', 'desc' => '{"en":"Light air","pt":"Ar leve"}', 'sea_conditions' => 'Ripples with appearance of scales are formed, without foam crests', 'land_conditions' => 'Direction shown by smoke drift but not by wind vanes.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '3', 'scale' => '2', 'desc' => '{"en":"Light breeze","pt":"Brisa leve"}', 'sea_conditions' => 'Small wavelets still short but more pronounced; crests have a glassy appearance but do not break', 'land_conditions' => 'Wind felt on face; leaves rustle; wind vane moved by wind.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '4', 'scale' => '3', 'desc' => '{"en":"Gentle breeze","pt":"Brisa suave"}', 'sea_conditions' => 'Large wavelets; crests begin to break; foam of glassy appearance; perhaps scattered white horses', 'land_conditions' => 'Leaves and small twigs in constant motion; light flags extended.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '5', 'scale' => '4', 'desc' => '{"en":"Moderate breeze","pt":"Brisa moderada"}', 'sea_conditions' => 'Small waves becoming longer; fairly frequent white horses', 'land_conditions' => 'Raises dust and loose paper; small branches moved.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '6', 'scale' => '5', 'desc' => '{"en":"Fresh breeze","pt":"Brisa fresca"}', 'sea_conditions' => 'Moderate waves taking a more pronounced long form; many white horses are formed; chance of some spray', 'land_conditions' => 'Small trees in leaf begin to sway; crested wavelets form on inland waters.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '7', 'scale' => '6', 'desc' => '{"en":"Strong breeze","pt":"Brisa forte"}', 'sea_conditions' => 'Large waves begin to form; the white foam crests are more extensive everywhere; probably some spray', 'land_conditions' => 'Large branches in motion; whistling heard in telegraph wires; umbrellas used with difficulty.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '8', 'scale' => '7', 'desc' => '{"en":"High wind, moderate gale, near gale","pt":"Vento forte"}', 'sea_conditions' => 'Sea heaps up and white foam from breaking waves begins to be blown in streaks along the direction of the wind; spindrift begins to be seen', 'land_conditions' => 'Whole trees in motion; inconvenience felt when walking against the wind.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '9', 'scale' => '8', 'desc' => '{"en":"Gale, fresh gale","pt":"Vendaval fresco"}', 'sea_conditions' => 'Moderately high waves of greater length; edges of crests break into spindrift; foam is blown in well-marked streaks along the direction of the wind', 'land_conditions' => 'Twigs break off trees; generally impedes progress.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '10', 'scale' => '9', 'desc' => '{"en":"Strong/severe gale","pt":"Vendaval forte"}', 'sea_conditions' => 'High waves; dense streaks of foam along the direction of the wind; sea begins to roll; spray affects visibility', 'land_conditions' => 'Slight structural damage (chimney pots and slates removed).', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '11', 'scale' => '10', 'desc' => '{"en":"Storm, whole gale","pt":"Tempestade"}', 'sea_conditions' => 'Very high waves with long overhanging crests; resulting foam in great patches is blown in dense white streaks along the direction of the wind; on the whole the surface of the sea takes on a white appearance; rolling of the sea becomes heavy; visibility affected', 'land_conditions' => 'Seldom experienced inland; trees uprooted; considerable structural damage.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '12', 'scale' => '11', 'desc' => '{"en":"Violent storm","pt":"Tempestade violenta"}', 'sea_conditions' => 'Exceptionally high waves; small- and medium-sized ships might be for a long time lost to view behind the waves; sea is covered with long white patches of foam; everywhere the edges of the wave crests are blown into foam; visibility affected', 'land_conditions' => 'Very rarely experienced; accompanied by widespread damage.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15'),
            array('id' => '13', 'scale' => '12', 'desc' => '{"en":"Hurricane force","pt":"Furacão"}', 'sea_conditions' => 'The air is filled with foam and spray; sea is completely white with driving spray; visibility very seriously affected', 'land_conditions' => 'Devastation.', 'created_at' => '2020-05-25 11:58:15', 'updated_at' => '2020-05-25 11:58:15')
        );

        foreach ($beaufort_scales as $question) {
            $dbQuestion = BeaufortScale::find($question['id']);

            if ($dbQuestion) {
                $dbQuestion->desc = $question['desc'];
                $dbQuestion->save();
            }
        }
    }
}
