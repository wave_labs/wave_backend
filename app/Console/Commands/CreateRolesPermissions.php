<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class CreateRolesPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create roles and permissions \n Warning: This is just for setup, this is not for adding mid-productiong';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allPermissions = [
            'user-list',
            'user-create',
            'user-update',
            'user-delete',
            'sighting-list',
            'sighting-update',
            'sighting-create',
            'sighting-delete',
            'capsule-list',
            'capsule-create',
            'capsule-update',
            'capsule-delete',
            'trip-list',
            'trip-create',
            'trip-update',
            'trip-delete',
            'creature-list',
            'creature-create',
            'creature-update',
            'creature-delete',
            'diving-spot-list',
            'diving-spot-create',
            'diving-spot-update',
            'diving-spot-delete',
            'dive-list',
            'dive-create',
            'dive-update',
            'dive-delete',
            'litter-list',
            'litter-create',
            'litter-update',
            'litter-delete',
        ];

        $guestPermissions = [
            'sighting-list',
            'sighting-create',
            'capsule-list',
            'capsule-create',
            'creature-list',
            'diving-spot-list',
            'diving-spot-create',
            'dive-list',
            'dive-create',
            'litter-list',
            'litter-create',
            'trip-list',
            'trip-create',
        ];

        $personPermissions = [
            'creature-list',
            'sighting-list',
            'sighting-update',
            'sighting-create',
            'sighting-delete',
            'dive-list',
            'dive-create',
            'dive-update',
            'dive-delete',
            'litter-list',
            'litter-create',
            'litter-update',
            'litter-delete',
            'trip-list',
            'trip-create',
            'trip-update',
            'trip-delete',
        ];

        $diveCenterPermissions = [
            'user-list',
            'creature-list',
            'diving-spot-list',
            'diving-spot-create',
            'diving-spot-update',
            'diving-spot-delete',
            'dive-list',
            'dive-create',
            'dive-update',
            'dive-delete',
            'sighting-list',
            'sighting-create',
            'sighting-update',
            'sighting-delete',
            'litter-list',
            'litter-create',
            'litter-update',
            'litter-delete',
            'trip-list',
            'trip-create',
            'trip-update',
            'trip-delete',
        ];

        $roles = [
            'admin' => $allPermissions,
            'person' => $personPermissions,
            'dive-center' => $diveCenterPermissions,
            'guest' => $guestPermissions
        ];

        DB::table('roles')->delete();
        DB::table('permissions')->delete();

        foreach ($allPermissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        foreach ($roles as $name => $permissions) {
            Role::create(['name' => $name])->syncPermissions($permissions);
        }
    }
}
