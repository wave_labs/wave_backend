<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class SQLdump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sql:dump {--litter} {--dive} {--sighting} {--total}';

    /**
     * The console command description.{--dive}
     *
     * @var string
     */
    protected $description = 'Dumps database to sql file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $host = config('app.db_host');
        $user = config('app.db_user');
        $pass = config('app.db_password');
        $database = config('app.db_database');

        if ($this->option('litter')) {
            exec('mysqldump -h' . $host . ' -u' . $user . ' -p' . $pass . ' ' . $database . ' litter_categories litter_sub_categories sources suggestion_types > ' . public_path() . '/sqlite/litter/mysql.sql');
        } else if ($this->option('dive')) {
            exec('mysqldump -h' . $host . ' -u' . $user . ' -p' . $pass . ' ' . $database . ' dives diving_spots creatures creature_types creature_photos surveys abundances sources creature_has_sources suggestion_types litter_categories litter_sub_categories user_has_diving_spots diving_spot_has_substracts > ' . public_path() . '/sqlite/dive/mysql.sql');
        } else if ($this->option('sighting')) {
            exec('mysqldump -h' . $host . ' -u' . $user . ' -p' . $pass . ' ' . $database . ' beaufort_scale creatures creature_types creature_photos sighting_behaviours vehicle_types vehicles sources creature_has_sources suggestion_types > ' . public_path() . '/sqlite/sighting/mysql.sql');
        } else if ($this->option('total')) {
            exec('mysqldump -h' . $host . ' -u' . $user . ' -p' . $pass . ' ' . $database . '  > ' . storage_path() . '/backup/' . Carbon::now()->toDateString() . '.sql');
        }
    }
}
