<?php

namespace App\Console\Commands;

use App\IotDevice;
use App\IotReport;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckIotDeviceActivity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iot-device:activity  {--type=} {--category=} {--minutes=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update activity for iot devices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $category = $this->option('category');

        $devices = IotDevice::where('id_type', $this->option('type'))->whereHas('categories', function ($query) use ($category) {
            $query->where('iot_device_category_id', $category);
        })->get();

        foreach ($devices as $device) {
            $reports = IotReport::whereIdDevice($device->id)->where('created_at', '>=', Carbon::now()->subMinutes(1))->get();

            if ($reports->count() && $device->id_state == 2) {
                $device->id_state = 1;
            }

            $reports = IotReport::whereIdDevice($device->id)->where('created_at', '>=', Carbon::now()->subMinutes(5))->get();

            if (!$reports->count() && $device->id_state == 1) {
                $device->id_state = 2;
            }
            $device->save();
        }
    }
}
