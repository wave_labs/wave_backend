<?php

namespace App\Console\Commands;

use App\Helper;
use App\IotReportField;
use Illuminate\Console\Command;

class ReplaceIotFieldForeign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'replace:foreign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $device_id = 12;
        //$old_keys = [49, 11, 59, 61, 60, 63, 3, 4, 14, 15, 57, 65];
        //$new_keys = [2802, 2803, 2804, 2805, 2806, 2807, 2808, 2809, 2810, 2811, 2812, 2813];

        $reports = IotReportField::whereHas('report', function ($query) use ($device_id) {
            $query->where('id_device', $device_id);
        })->where('device_field_id', 2763)->get();
        $length = count($reports);

        foreach ($reports as $key => $report) {
            Helper::printToConsole($key . " / " . $length);
            if (strlen(substr(strrchr($report->value, "."), 1)) >= 1 && (float) $report->value < 360 && (float) $report->value > 1) {
                //Helper::printToConsole($report->value);
                $report->device_field_id = 2756;
                $report->save();
            }
        }
    }
}
