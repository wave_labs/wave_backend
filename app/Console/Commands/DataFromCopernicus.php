<?php

namespace App\Console\Commands;

use App\CopernicusProduct;
use App\Helper;
use File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class DataFromCopernicus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:copernicus-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve and store data from copernicus marine service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*$path = $product->identifier . "/" .  $product->path;
        $years = Storage::disk('copernicus-ms')->directories("Core/" . $path);
        $latestYear = end($years);
        $months = Storage::disk('copernicus-ms')->directories($latestYear);

        if (count($months)) {
            $latestMonth = end($months);
            $files = Storage::disk('copernicus-ms')->files($latestMonth);
        } else {
            $files = Storage::disk('copernicus-ms')->files($latestYear);
        }

        foreach ($files as $file) {
            $fileData = explode("/", $file);
            Storage::disk('storage')->put("copernicus/" . $path . "/" . end($fileData), $file);
        }*/

        // Storage::disk('storage')->put("copernicus/teste/", Storage::disk('copernicus-ms')->get("Core/GLOBAL_ANALYSIS_FORECAST_PHY_001_024/global-analysis-forecast-phy-001-024/2021/01/mercatorpsy4v3r1_gl12_mean_20210101_R20210113.nc"));
        $products = CopernicusProduct::all();
        foreach ($products as $product) {
            $path = $product->identifier . "/" .  $product->path;

            $years = Storage::disk('copernicus-ms')->directories("Core/" . $path);

            foreach ($years as $year) {
                $yearArray = explode("/", $year);
                $months = Storage::disk('copernicus-ms')->directories($year);

                if (count($months)) {
                    foreach ($months as $month) {
                        $monthArray = explode("/", $month);
                        $files = Storage::disk('copernicus-ms')->files($month);

                        foreach ($files as $file) {
                            $fileData = explode("/", $file);
                            Storage::disk('storage')->put("copernicus/" . $product->identifier . "/" . end($yearArray) . "/" . end($monthArray) . "/" . end($fileData), Storage::disk('copernicus-ms')->get($file));
                        }
                    }
                } else {
                    $files = Storage::disk('copernicus-ms')->files($year);

                    foreach ($files as $file) {
                        $fileData = explode("/", $file);
                        Storage::disk('storage')->put("copernicus/" . $product->identifier . "/" . end($yearArray) . "/" . end($fileData), Storage::disk('copernicus-ms')->get($file));
                    }
                }
            }
        }
    }
}
