<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Exceptions\RoleDoesNotExist;
use App\User;
use Hash;

class CreateGuest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'guest:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the guest user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::create([
            'name' => "Guest",
            'userable_type' => 'App\UserPerson',
            'email' => "guest@guest.wave",
            'password' => Hash::make("guest"),
            'note' => 'guest account',
            'active' => 1,
            'is_verified' => 1
        ]);

        try {
            $user->syncRoles(['guest']);
        } catch (RoleDoesNotExist $e) {
            $this->info('Ooopsie, it seems the role guest doesn\'t exist. Have you run the following command to generate roles and permissions: "php artisan permissions:create"?');
        }
    }
}
