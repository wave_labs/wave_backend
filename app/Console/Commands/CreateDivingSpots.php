<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DivingSpot;
use App\DivingSpotSubstract;

class CreateDivingSpots extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'diving-spots:seeder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Popula diving spots with new diving spots';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $divingSpots = [
            [
                'latitude' => 32.636248,
                'longitude' => -16.850542,
                'range' => '',
                'name' => 'Holy Wall',
                'description' => '',
                'protection' => 'Marine Protected Area',
                'substracts' => [1,2,3]
            ],
            [
                'latitude' => 32.637295,
                'longitude' => -16.853303,
                'range' => '30',
                'name' => 'Garajau',
                'description' => '',
                'protection' => 'Marine Protected Area',
                'substracts' => [1,2]
            ],
            [
                'latitude' => 32.643527,
                'longitude' => -16.824359,
                'range' => '22',
                'name' => 'Blue Hole',
                'description' => '',
                'protection' => 'None',
                'substracts' => [1,2,3,4]
            ],
            [
                'latitude' => 32.639511,
                'longitude' => -16.866827,
                'range' => '32',
                'name' => 'T-Reef',
                'description' => '',
                'protection' => 'Marine Protected Area',
                'substracts' => [1,2]
            ],
            [
                'latitude' => 32.639511,
                'longitude' => -16.866827,
                'range' => '32',
                'name' => 'Mamas',
                'description' => '',
                'protection' => 'Marine Protected Area',
                'substracts' => [1,2]
            ],
            [
                'latitude' => 32.6495627,
                'longitude' => -16.991191,
                'range' => '32',
                'name' => 'Corveta Afonso Cerqueira',
                'description' => '',
                'protection' => 'Marine Protected Area',
                'substracts' => [2,5]
            ],
            [
                'latitude' => 32.640321,
                'longitude' => -16.915023,
                'range' => '32',
                'name' => 'Pronto',
                'description' => '',
                'protection' => 'Marine Protected Area',
                'substracts' => [5]
            ],
            [
                'latitude' => 32.717106,
                'longitude' => -16.757016,
                'range' => '20',
                'name' => 'Tres Marias',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.640902,
                'longitude' => -16.921495,
                'range' => '15',
                'name' => 'Carlton Housereef',
                'description' => '',
                'protection' => 'None',
                'substracts' => [1,2]
            ],
            [
                'latitude' => 32.7349365,
                'longitude' => -16.740839,
                'range' => '20',
                'name' => 'Pedra D eira',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.7349365,
                'longitude' => -16.740839,
                'range' => '20',
                'name' => 'Cais da Junta',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.6906164,
                'longitude' => -16.772698,
                'range' => '50',
                'name' => 'Baixa de Santa Catarina',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.6670658,
                'longitude' => -16.8016477,
                'range' => '24',
                'name' => 'Ponta da Santola',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.6875274,
                'longitude' => -16.7833413,
                'range' => '25',
                'name' => 'Baixa da Cadeia',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.691557,
                'longitude' => -16.772638,
                'range' => '22',
                'name' => 'Haera',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.7070822,
                'longitude' => -16.760222,
                'range' => '',
                'name' => 'Pedra das Anémonas',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.731475,
                'longitude' => -16.747192,
                'range' => '',
                'name' => 'Ponta do Altar',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.737466,
                'longitude' => -16.68441,
                'range' => '40',
                'name' => 'Parede do Cais da Sardinha',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.7309201,
                'longitude' => -16.6458126,
                'range' => '40',
                'name' => 'Badajeira',
                'description' => '',
                'protection' => 'Marine Protected Area',
                'substracts' => []
            ],
            [
                'latitude' => 32.699402,
                'longitude' => -17.136981,
                'range' => '34',
                'name' => 'Bowbelle',
                'description' => '',
                'protection' => 'None',
                'substracts' => [5]
            ],
            [
                'latitude' => 32.699402,
                'longitude' => -17.136981,
                'range' => '34',
                'name' => 'Bom Rei',
                'description' => '',
                'protection' => 'None',
                'substracts' => [5]
            ],
            [
                'latitude' => 0,
                'longitude' => 0,
                'range' => '35',
                'name' => 'Baixa da Cruz',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.71182,
                'longitude' => -17.16206,
                'range' => '18',
                'name' => 'Baixa da Fortuna',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.71493,
                'longitude' => -17.16698,
                'range' => '',
                'name' => 'Quebradinha',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.70293,
                'longitude' => -17.1449,
                'range' => '',
                'name' => 'Pedra do Leão',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.724,
                'longitude' => -17.19179,
                'range' => '',
                'name' => 'Ponta da Galé',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ],
            [
                'latitude' => 32.71945,
                'longitude' => -17.18821,
                'range' => '',
                'name' => 'Baixa do Preto',
                'description' => '',
                'protection' => 'None',
                'substracts' => []
            ]
        ];


        foreach ($divingSpots as $key => $divingSpot) {
            $newDivingSpot = DivingSpot::create([
                'latitude' => $divingSpot["latitude"],
                'longitude' => $divingSpot["longitude"],
                'range' => $divingSpot["range"],
                'name' => $divingSpot["name"],
                'description' => $divingSpot["description"],
                'protection' => $divingSpot["protection"]
            ]);
            $newDivingSpot->substracts()->sync($divingSpot['substracts']);
        }
    }
}
