<?php

namespace App\Console\Commands;

use App\Creature;
use Illuminate\Console\Command;

class TranslateCreature extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:creature';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $PTitems = [
            [
                'name' => 'Foca monge',
                'description' => 'Machos possuem penugem preta com barriga branca, enquanto que as fêmeas são castanhas com barriga mais clara',
                'curiosity' => 'Foca monge é a foca mais em risco do mundo, com apenas 700 espécimes, dos quais 40 na Madeira. No passado, eram caçadas por pêlo, óleos, carne e medicamentos. A espécie possui importância comunitária e a sua população é protegida legalmente.',
                'size' => 'Até 2.8 m de comprimento',
            ],
            [
                'name' => 'Algas marinhas',
                'description' => 'Algas marinhas possuem folhas estreitas de cor verde clara ou verde acinzentado',
                'curiosity' => 'Algas marinhas cresce em áreas ariosas e necessita de água fresca para fotosíntese. Um prado de algas marinhas gera produtividade biológica e fornece habitat para diversas espécies, tal como cavalos marinhos. É ameaçada por pesca, âncoras de barcos e desenvolvimento costal',
                'size' => 'As folhas poderão alcançar 40 cm de comprimento',
            ],
            [
                'name' => 'Cavalo Marinho',
                'description' => 'Cavalo Marinho possui uma cabeça em formato de cavalo, com um focinho longo, boca enrugada e cauda preênsil',
                'curiosity' => 'Cavalo Marinho é o único animal conhecido que o macho carrega crias por nascer. Cavalos marinhos são utilizados para fins medicinais, decoração em aquários, curiosidades e símbolos de boa sorte quando avistados',
                'size' => 'Altura entre 7 e 13 cm',
            ],
            [
                'name' => 'Garoupa escura',
                'description' => 'Cabeça e corpo escuro com manchas claras irregulares, geralmente em séries verticais. Mais claro ventralmente',
                'curiosity' => 'Grande alvo de pesca, que resulta numa redução da sua população e consequentemente uma espécie de vulnerável. Adultos preferem fundos rochosos e são solitários e territoriais. Jovens vivem perto das costas rochosas. Modificam o seu sexo de fêmea para macho no crescimento',
                'size' => 'Até 150cm de comprimento',
            ],
            [
                'name' => 'Peixe porco vermelho',
                'description' => 'Corpo e cabeça vermelhos. Fêmea: mais claro ventralmente com linha horizontal amarela no meio do corpo',
                'curiosity' => 'Tem uma distribuição limitada no Atlântico Este, com presença em menos de dez localidades. A sua população está diminuindo e é listada como espécie vulnerável. Está marcado como um sistema territorial harêmico, tipicamente existe um macho para três fêmeas. Ele muda seu sexo de feminino para masculino com o crescimento.',
                'size' => 'Até 50 cm de comprimento',
            ],
            [
                'name' => 'Coral preto',
                'description' => 'Apesar do seu nome, poderá apresentar folhas brancas, amarelas, rosa, cinzentas, verdes or castanho avermelhado',
                'curiosity' => 'O esqueleto córneo, sendo duro e preto por intercalações minerais, pode ser usado para a fabricação de jóias. Os gregos o usavam para fazer amuletos explicando o nome científico (anti pathes significa contra doenças). Os arbustos de coral negro são o lar de vários animais.',
                'size' => 'Até 1.5 m de altura',
            ],
            [
                'name' => 'Caranguejo',
                'description' => 'Cores diferentes do laranja ao castanho. É um caranguejo nadador com comportamento agressivo',
                'curiosity' => 'Caracterizado por garras delgadas com dentes em forma de agulha, conferindo uma ação rápida que em combinação com patas traseiras em forma de remo para nadar, o tornam um predador eficaz de peixes e outras presas de corpo mole que se movem rapidamente. O ritual de namoro para acasalamento é comum (por meio de pistas olfativas e táteis).',
                'size' => 'Até 5cm de comprimento',
            ],
            [
                'name' => 'Coral de recife verde',
                'description' => 'Colônia incrustante verde / castanho formada por muitos pequenos pólipos. Pode crescer em galhos ou planos',
                'curiosity' => 'A cor verde / castanho se deve à presença de algas simbióticas chamadas zooxantelas que vivem no tecido coral e auxiliam na solicitação de energia por meio da fotossíntese. Sem algas, o coral é branco.',
                'size' => 'Ramos até 20 mm, pólipos de 2 mm de comprimento',
            ],
            [
                'name' => 'Alga vermelha',
                'description' => 'Ramos vermelho-púrpura ou ramificado e filamentoso vermelho acastanhado',
                'curiosity' => 'É uma alga vermelha e apresenta duas fases de vida distintas, morfologicamente diferentes. Apresenta o talo com tufos de quase o mesmo comprimento e dispostos em espiral. Ele mostrou uma forte citotoxicidade contra linhagens de células cancerígenas humanas.',
                'size' => 'Ramos até 30 mm',
            ],
            [
                'name' => 'Tunicado de botão',
                'description' => 'Colônias laranja ou roxas brilhantes, formando lóbulos ou cabeças desenvolvidas em roseta e cluster',
                'curiosity' => '8-20 indivíduos (zoóides) formam uma roseta. Cada zoóide tem sua própria boca, mas compartilha uma abertura exalante central comum. Várias rosetas formam um aglomerado. A espécie foi descrita pela primeira vez nos Açores, mas parece ser uma espécie caribenha, provavelmente chegada por iates que atravessam o Atlântico.',
                'size' => 'Zoóide de 2-4 mm de comprimento, roseta de 1,5 cm de diâmetro e cluster de 40 cm de diâmetro',
            ],
            [
                'name' => 'Tartaruga',
                'description' => 'Carapaça em forma de coração, geralmente de tonalidade marrom-avermelhada com tons de azeitona',
                'curiosity' => 'São as maiores tartarugas marinhas de carapaça dura. Nidifica em praias arenosas insulares e continentais em todas as regiões temperadas e subtropicais em todo o mundo. Ele pode realizar migrações que atravessam zonas oceânicas que abrangem centenas a milhares de quilômetros. É protegido por vários tratados e leis.',
                'size' => 'Até 1m de comprimento',
            ],
            [
                'name' => 'Coral de fogo',
                'description' => 'Colônia verde-amarela com pontas brancas e muitas formas de crescimento, de ramos cilíndricos a lajes planas',
                'curiosity' => 'Apesar do nome, não é um coral verdadeiro. O nome coral de fogo vem de sua capacidade de queimar; na verdade, os pólipos que formam a colônia são minúsculos, mas picam ferozmente através de células chamadas cnidócitos. Eles podem injetar um veneno que causa uma sensação dolorosa de queimação, erupções na pele, bolhas e cicatrizes.',
                'size' => 'Até 45 cm de altura',
            ],
            [
                'name' => 'Anêmona incrustante',
                'description' => 'Os pólipos que formam a colônia têm muitos tentáculos com uma cor amarela intensa ou ligeiramente laranja',
                'curiosity' => 'Para apoiar a construção de seu esqueleto, ele prefere um coral preto ligeiramente saliente. Uma colônia de anêmonas espesso e incrustante pode ser espalhada em um metro quadrado ou mais.',
                'size' => 'Pólipo único de até 1,2 cm de altura e 0,6 cm de diâmetro',
            ],
            [
                'name' => 'Caracol guarda chuva',
                'description' => 'A concha em forma de limpet e o grande manto são cobertos por numerosas papilas arredondadas. Cor variável',
                'curiosity' => 'Esta é uma espécie grande e primitiva de lesma com um corpo amarelo redondo e suas guelras podem ser visíveis sob a concha. A concha pode ser coberta com plantas incrustantes e crescimentos de animais. Como meio de defesa, produz um composto de sabor muito desagradável para os peixes.',
                'size' => 'Até 20 cm de altura',
            ],
            [
                'name' => 'Caulerpa',
                'description' => 'Minúsculos cerdas verdes de filamentos ramificados, inseridos em espirais em torno da "haste" central',
                'curiosity' => 'Também conhecida como alga marinha verde pincel para garrafas, porque a forma de seus ramos dá a aparência geral de uma escova para garrafas rígida. É uma espécie tropical, comum no Caribe e no Havaí. Normalmente vive ancorado em sedimentos ou crescendo em recifes de coral.',
                'size' => 'Única haste de 4-6 cm de comprimento',
            ],
            [
                'name' => 'Polvo',
                'description' => 'O polvo tem 8 braços forrados de ventosas, tem manto grande e falta a concha interna',
                'curiosity' => 'O polvo usa a camuflagem para se assemelhar a outros animais ou para se misturar ao seu ambiente, mudando sua cor e sua aparência ao elevar ou abaixar pequenas protuberâncias por todo o corpo mole. Normalmente, a entrada de sua toca está cheia de sobras de sua comida, como conchas de mexilhão.',
                'size' => 'Até 25 cm de comprimento de manto, até 1 m de braços de comprimento',
            ],
            [
                'name' => 'Dourada',
                'description' => 'Corpo cinza prateado com uma linha dourada entre os olhos e uma mancha escura no início do corpo',
                'curiosity' => 'É uma das espécies mais importantes comercialmente cultivadas. É utilizado na aquicultura em gaiola oceânica na Madeira desde 1997. É um predador oportunista voraz, capaz de adaptar a sua alimentação aos alimentos disponíveis no seu ambiente. Ele muda seu sexo de masculino para feminino com o crescimento.',
                'size' => 'Até 70 cm de comprimento',
            ],
            [
                'name' => 'Algas de lâmina plana',
                'description' => 'Lâminas lisas castanho-dourado ou verde-oliva com pontas largas quadradas crescendo em forma de leque',
                'curiosity' => 'Apresenta bandas de material de diversas cores desenvolvidas paralelamente à borda de crescimento incluindo uma banda verde iridescente que dá à alga uma atraente aparência zoneada. Ele está preso a rochas e frequentemente forma camadas densas. Possui diversos compostos com função defensiva.',
                'size' => 'Até 50 cm de comprimento',
            ],
            [
                'name' => 'Golfinho pintado do atlântico',
                'description' => 'O golfinho pintado do Atlântico é um golfinho encontrado na Corrente do Golfo do Oceano Atlântico Norte. Os membros mais velhos da espécie têm uma coloração manchada muito distinta em todo o corpo.',
                'curiosity' => 'A foca-monge é a espécie de foca mais ameaçada do mundo, com menos de 700 indivíduos no total e cerca de 40 na Madeira. No passado, eram caçados por sua pele, óleo, carne e para uso medicinal. A foca-monge é considerada uma espécie de importância comunitária e suas populações estão legalmente protegidas.',
                'size' => 'Até 2.29 m de comprimento',
            ],
            [
                'name' => 'Foca barbuda',
                'description' => 'As características distintivas deste selo sem orelhas incluem nadadeiras frontais quadradas e cerdas grossas em seu focinho. Os adultos são castanho-acinzentados, mais escuros no dorso. Ocasionalmente, o rosto e o pescoço são marrom-avermelhados, os filhotes de focas nascem com uma pelagem natal marrom-acinzentada com manchas brancas espalhadas nas costas e na cabeça.',
                'curiosity' => 'Focas bentônicas e barbadas se alimentam principalmente de uma variedade de pequenas presas encontradas ao longo do fundo do oceano, incluindo mariscos, lulas e peixes. Seus bigodes funcionam como antenas nos sedimentos do fundo macio. Os adultos tendem a não mergulhar muito fundo, preferindo áreas costeiras rasas com não mais de 300 m de profundidade. Filhotes de até um ano de idade, no entanto, se aventurarão muito mais fundo, mergulhando a uma profundidade de até 450 m',
                'size' => 'Até 2.7 m',
            ],
            [
                'name' => 'Baleia branca',
                'description' => 'O estreitamento súbito da base do pescoço dá-lhe a aparência de ombros, único entre os cetáceos. A cauda cresce e torna-se cada vez mais curvada com ornamentos à medida que o animal envelhece. As nadadeiras são largas e curtas - tornando-as quase quadradas.',
                'curiosity' => 'Estes cetáceos são altamente sociáveis ​​e regularmente formam pequenos grupos, ou vagens, que podem conter entre dois e 25 indivíduos, com uma média de 10 membros. Os vagens tendem a ser instáveis, o que significa que os indivíduos tendem a se mover de um vagem para outro. O rastreamento de rádio mostrou até mesmo que as belugas podem começar em um casulo e, em poucos dias, estar a centenas de quilômetros daquele casulo.',
                'size' => 'Adulto pode alcançar até 5.5 m',
            ],
            [
                'name' => 'Golfinho nariz de garrafa',
                'description' => 'Suas mandíbulas superiores e inferiores alongadas formam o que é chamado de rostro, ou focinho, que dá ao animal seu nome comum. O nariz real e funcional é a bolha de ar no topo de sua cabeça; o septo nasal é visível quando o orifício de respiração está aberto.',
                'curiosity' => 'Os golfinhos nariz-de-garrafa podem viver mais de 40 anos. As mulheres geralmente vivem de 5 a 10 anos a mais do que os homens, com algumas mulheres excedendo os 60 anos. Essa idade extrema é rara e menos de 2% de todos os golfinhos-nariz-de-garrafa viverão mais de 60 anos. Os golfinhos nariz-de-garrafa podem pular a uma altura de 6 metros de altura; eles usam isso para se comunicarem uns com os outros.',
                'size' => 'Até 4m de comprimento',
            ],
            [
                'name' => 'Baleia da groenlândia',
                'description' => 'Tem um corpo grande, robusto e de cor escura e um queixo / maxilar inferior brancos. A baleia tem um enorme crânio triangular, que usa para quebrar o gelo do Ártico para respirar.',
                'curiosity' => 'Eles vivem inteiramente em águas férteis do Ártico e subártico, ao contrário de outras baleias que migram para águas de baixa latitude para se alimentar ou se reproduzir. A cabeça de arco também era conhecida como baleia franca da Groenlândia ou baleia ártica. Os baleeiros americanos os chamavam de campanário, baleia polar ou Rússia ou baleia russa. A cabeça de arco tem a maior boca de qualquer animal.',
                'size' => 'Até 18m de comprimento',
            ],
            [
                'name' => 'Golfinho clímene',
                'description' => 'É muito parecido com o golfinho-rotador. De perto, é possível observar que o bico do Clymene é ligeiramente mais curto que o de seu parente. A barbatana dorsal também é menos ereta e triangular.',
                'curiosity' => 'Passam a maior parte de suas vidas em águas com mais de 100 m de profundidade, mas ocasionalmente se movem para regiões costeiras mais rasas. Alimentam-se de lulas e pequenos peixes de cardume, caçando à noite ou em águas mesopelágicas onde a luz é limitada. Predadores incluem tubarões cortadores de biscoitos, como evidenciado por marcas de mordidas vistas em vários animais.',
                'size' => 'Até 2m de comprimento',
            ],
            [
                'name' => 'Golfinho comum',
                'description' => 'O padrão de cores no corpo é incomum. O dorso é escuro e a barriga é branca, enquanto em cada lado há um padrão de ampulheta colorido em cinza claro, amarelo ou dourado na frente e cinza sujo atrás.',
                'curiosity' => 'Enfrentam uma mistura de ameaças devido à influência humana. Níveis moderados de poluentes metálicos, que parecem ter um impacto negativo na saúde dos golfinhos, foram medidos em algumas populações. Populações foram caçadas na costa do Peru para uso como alimento e isca para tubarões. Na maioria das outras áreas, os golfinhos não foram caçados diretamente.',
                'size' => 'Até 2.5m de comprimento',
            ],
            [
                'name' => 'Falsa orca',
                'description' => 'A falsa baleia assassina é preta ou cinza escuro, embora um pouco mais clara na parte inferior. Possui corpo esguio com cabeça alongada e afilada e 44 dentes. A barbatana dorsal tem forma de foice e as suas barbatanas são estreitas, curtas e pontiagudas',
                'curiosity' => 'Grande golfinho oceânico que habita oceanos em todo o mundo, mas frequenta principalmente regiões tropicais. Foi descrita pela primeira vez em 1846 como uma espécie de toninha baseada em um crânio, que foi revisada quando as primeiras carcaças foram observadas em 1861. O nome "falsa baleia assassina" vem das características do crânio semelhantes às da baleia assassina (Orcinus orca).',
                'size' => 'Até 6m de comprimento',
            ],
            [
                'name' => 'Baleia comum',
                'description' => 'A baleia-comum é geralmente caracterizada por seu bico alto, dorso longo, nadadeira dorsal proeminente e coloração assimétrica.',
                'curiosity' => 'Também conhecido como baleia finback ou rorqual comum e anteriormente conhecido como baleia arenque ou baleia razorback, é um mamífero marinho pertencente à parvorder das baleias de barbatanas. É a segunda maior espécie da Terra, depois da baleia azul.',
                'size' => 'Até 25.9m de comprimento',
            ],
            [
                'name' => 'Golfinho de fraser',
                'description' => 'Eles têm uma constituição robusta, uma barbatana pequena em relação ao tamanho do corpo, nadadeiras visivelmente pequenas. A barbatana dorsal e o bico também são insubstanciais.',
                'curiosity' => 'O golfinho é normalmente avistado em águas tropicais profundas; entre 30 ° S e 20 ° N. O Pacífico Oriental é o local mais confiável para visualizações. Grupos de golfinhos encalhados foram encontrados em lugares distantes como França e Uruguai. No entanto, eles são considerados anômalos e possivelmente devido a condições oceanográficas incomuns, como o El Niño.',
                'size' => 'Até 2.75m de comprimento',
            ],
            [
                'name' => 'Golfinho de risso',
                'description' => 'Corpo anterior relativamente grande e barbatana dorsal, enquanto o posterior diminui para uma cauda relativamente estreita. A cabeça bulbosa possui uma prega vertical na frente.',
                'curiosity' => 'É a única espécie de golfinho do gênero Grampus. É comumente conhecido como golfinho-monge entre os pescadores taiwaneses.',
                'size' => 'Até 4m de comprimento',
            ],
            [
                'name' => 'Foca de Harpa',
                'description' => 'A foca harpa madura tem olhos negros puros. Possui um pelo cinza prateado cobrindo seu corpo, com marcas em forma de harpa preta ou em forma de osso da sorte, dorsalmente.',
                'curiosity' => 'Comparada a outras focas focas, a foca harpa mergulha de profundidades rasas a moderadamente profundas. A profundidade do mergulho varia com a temporada, hora do dia e local. Na subpopulação do Mar da Groenlândia, a taxa média de mergulho é de cerca de 8,3 mergulhos por hora e os mergulhos variam de uma profundidade de menos de 20 a mais de 500m.',
                'size' => 'Até 1.9m de comprimento',
            ],
            [
                'name' => 'Baleia jubarte',
                'description' => 'As jubartes podem ser facilmente identificadas por seu corpo atarracado, corcunda óbvia, coloração dorsal preta e barbatanas peitorais alongadas. A cabeça e a mandíbula são cobertas por protuberâncias chamadas tubérculos, que são folículos capilares e são característicos da espécie. ',
                'curiosity' => 'As baleias jubarte são rorquais, membros da família Balaenopteridae que inclui as baleias azul, barbatana, Bryde, sei e minke. Acredita-se que os rorquals tenham divergido das outras famílias da subordem Mysticeti já na era do Mioceno médio.',
                'size' => 'Adultos alcançam um comprimento entre 12 e 16 m',
            ],
            [
                'name' => 'Orca assassina',
                'description' => 'Uma típica baleia assassina apresenta distintamente o dorso, o peito e os lados brancos e uma mancha branca acima e atrás do olho. Os bezerros nascem com uma tonalidade amarelada ou laranja, que desbota para o branco. Possui um corpo pesado e robusto com uma grande barbatana dorsal de até 1,8 m de altura.',
                'curiosity' => 'As baleias assassinas são encontradas em todos os oceanos e na maioria dos mares. Devido à sua enorme variedade, número e densidade, a distribuição relativa é difícil de estimar, mas eles claramente preferem latitudes mais altas e áreas costeiras a ambientes pelágicos.',
                'size' => 'Variam entre 6 e 8 metros',
            ],
            [
                'name' => 'Foca leopardo',
                'description' => 'A foca leopardo tem uma forma corporal distintamente longa e musculosa, quando comparada com outras focas. Esta espécie de foca é conhecida por sua enorme cabeça e mandíbulas semelhantes a répteis, que permitem que ela seja um dos principais predadores em seu ambiente.',
                'curiosity' => 'As focas leopardo são muito vocais debaixo d\'água durante o verão austral. As focas macho produzem chamadas altas (153 a 177 dB re 1 μPa a 1 m) por muitas horas todos os dias.',
                'size' => 'O comprimento total desta espécie é 2.4–3.5 m',
            ],
            [
                'name' => 'Baleia piloto de aleta longa',
                'description' => 'Apesar de seu nome comum, a baleia-piloto de nadadeira comprida é na verdade uma grande espécie de golfinho. O mesmo é verdade para orcas e várias outras pequenas baleias. Tem uma testa bulbosa e é de cor preta ou cinza escuro com manchas cinza claro ou brancas nas regiões da garganta e da barriga. ',
                'curiosity' => 'As baleias-piloto receberam seu nome da crença original de que havia um "piloto" ou indivíduo líder em seus grupos. O nome do gênero, "Globicephala" é derivado de uma combinação das palavras latinas globus ("globo") e kephale ("cabeça"). ',
                'size' => 'Até 6,7 metros',
            ],
            [
                'name' => 'Golfinho cabeça de melão',
                'description' => 'A cabeça da baleia com cabeça de melão é um cone arredondado que dá ao animal o seu nome comum. O corpo é mais ou menos uniformemente cinza claro, exceto por um rosto cinza escuro - às vezes chamado de "máscara". As nadadeiras são longas e pontudas. A barbatana dorsal é alta com uma ponta pontiaguda - uma reminiscência de sua parente, a baleia assassina. ',
                'curiosity' => 'As baleias com cabeça de melão são animais muito sociais que vivem em grandes grupos de 100 a 1.000. Eles foram observados nadando próximos um do outro e tocando as nadadeiras. Dentro do grupo grande, eles geralmente nadam em grupos menores de 10-14 ',
                'size' => 'Adulto cresce até 3 m',
            ],
            [
                'name' => 'Baleia minke',
                'description' => 'A baleia minke é preta / cinza / roxa.',
                'curiosity' => 'As baleias minke são a segunda menor baleia de barbatana; apenas a baleia franca pigmeu é menor. Ao atingir a maturidade sexual (6–8 anos de idade), os homens medem uma média de 6,9 ​​m (23 pés) e as mulheres 8 m (26 pés) de comprimento, respectivamente. ',
                'size' => 'Comprimentos máximos relatados variam entre 9,1 e 10,7 m',
            ],
            [
                'name' => 'Narval',
                'description' => 'A pigmentação dos narvais é um padrão mosqueado, com manchas marrom-pretas sobre um fundo branco. Eles são mais escuros quando nascem e tornam-se mais brancos com a idade ',
                'curiosity' => 'A característica mais notável do narval macho é uma única presa longa, um dente canino que se projeta do lado esquerdo da mandíbula superior, através do lábio, e forma uma espiral em hélice para a esquerda.',
                'size' => 'Um comprimento médio de 4,1 m',
            ],
            [
                'name' => 'Baleia franca do atlântico norte',
                'description' => 'Como outras baleias francas, a baleia franca do Atlântico Norte, também conhecida como baleia franca do norte ou baleia franca negra, é facilmente distinguida de outras baleias pelas calosidades em sua cabeça',
                'curiosity' => 'Além das atividades de acasalamento realizadas por grupos de fêmeas solteiras e vários machos, chamados SAG (Surface Active Group), as baleias francas do Atlântico Norte parecem menos ativas em comparação com as subespécies no hemisfério sul.',
                'size' => 'Baleias francas adultas do Atlântico Norte em média 13-16 m',
            ],
            [
                'name' => 'Golfinho pintado pantropical',
                'description' => 'Varia significativamente em tamanho e coloração em toda a sua gama. A divisão mais significativa é entre as variedades costeiras e pelágicas. A forma costeira é maior e mais manchada. ',
                'curiosity' => 'É uma espécie de golfinho encontrada em todos os oceanos temperados e tropicais do mundo. A espécie estava começando a ficar ameaçada devido à morte de milhões de indivíduos em redes de cerco com retenida de atum. ',
                'size' => 'Adultos têm cerca de 2,5 m de comprimento',
            ],
            [
                'name' => 'Foca de Ross',
                'description' => 'A pelagem é castanha-escura na zona dorsal e branca prateada na parte inferior. No início do inverno antártico, a pelagem desbota gradualmente para se tornar marrom claro. ',
                'curiosity' => 'Capaz de produzir uma variedade de sons complexos e semelhantes a sirenes que são executados no gelo e debaixo d\'água, onde são transportados por longas distâncias.',
                'size' => 'Alcance um comprimento de cerca de 1,68–2,09 m',
            ],
            [
                'name' => 'Golfinho de dentes rugosos',
                'description' => 'Como o nome comum da espécie indica, os dentes também são distintos, tendo uma superfície rugosa formada por numerosas cristas estreitas e irregulares. ',
                'curiosity' => 'Animais tipicamente sociais, embora indivíduos solitários também sejam avistados. Um grupo médio tem entre dez e vinte membros, mas eles podem variar de dois a noventa. ',
                'size' => 'Alcance de 2,09 a 2,83 metros',
            ],
            [
                'name' => 'Baleia piloto de aleta curta',
                'description' => 'Tem um corpo atarracado, uma testa bulbosa, nenhum bico proeminente, nadadeiras longas agudamente pontiagudas na ponta, cor preta ou cinza escuro, e a nadadeira dorsal inserida para a frente no corpo. ',
                'curiosity' => 'Pode ser confundida com seus parentes, as baleias-piloto de nadadeiras compridas, mas existem várias diferenças. Como seus nomes indicam, suas nadadeiras são mais curtas do que as da baleia-piloto de nadadeiras longas, com uma curva mais suave na borda. Eles têm menos dentes do que a baleia-piloto de nadadeiras compridas, com 14 a 18 em cada mandíbula. ',
                'size' => 'Alcance até 5,5 metros',
            ],
            [
                'name' => 'Baleia franca austral',
                'description' => 'Sua pele é cinza muito escura ou preta, ocasionalmente com algumas manchas brancas na barriga. As calosidades da baleia franca aparecem brancas devido a grandes colônias de ciamídeos ',
                'curiosity' => 'As baleias francas normalmente não cruzam as águas equatoriais quentes para se conectar com outras espécies e (inter) raças: suas camadas espessas de gordura isolante dificultam a dissipação do calor interno do corpo em águas tropicais. ',
                'size' => 'Alcance até 17,5-18 m',
            ],
            [
                'name' => 'Cachalote',
                'description' => 'No topo do crânio da baleia está posicionado um grande complexo de órgãos preenchido com uma mistura líquida de gorduras e ceras chamada espermacete. O objetivo deste complexo é gerar sons de clique poderosos e focados, que o cachalote usa para ecolocalização e comunicação. ',
                'curiosity' => 'É a maior das baleias dentadas e o maior predador dentado. É o único membro vivo do gênero Physeter e uma das três espécies existentes na família do cachalote, junto com o cachalote pigmeu e o cachalote anão do gênero Kogia ',
                'size' => 'Alcance até 20,5 metros',
            ],
            [
                'name' => 'Golfinho rotador',
                'description' => 'A área dorsal é cinza escuro, os lados cinza claro e a parte inferior cinza claro ou branco. Além disso, uma faixa escura vai do olho até a nadadeira, delimitada acima por uma linha fina e clara. ',
                'curiosity' => 'O golfinho rotador vive em quase todas as águas tropicais e subtropicais entre 40 ° N e 40 ° S. A espécie habita principalmente águas costeiras, ilhas ou bancos. No entanto, no Pacífico tropical oriental, os golfinhos vivem longe da costa. Os golfinhos-rotadores podem usar diferentes habitats, dependendo da estação. ',
                'size' => 'Os adultos têm normalmente 129-235 cm de comprimento',
            ],
            [
                'name' => 'Golfinho riscado',
                'description' => 'Sua coloração é muito diferente e torna-se relativamente fácil de notar no mar. A parte inferior é azul, branca ou rosa. Uma ou duas faixas pretas circundam os olhos e, em seguida, percorrem as costas, até a nadadeira. ',
                'curiosity' => 'Eles também podem se misturar com golfinhos comuns. O golfinho listrado é tão capaz quanto qualquer golfinho na realização de acrobacias - frequentemente rompendo e saltando muito acima da superfície da água. ',
                'size' => 'Os adultos têm normalmente 2,6 m de comprimento',
            ],
            [
                'name' => 'Morsa',
                'description' => 'A característica mais proeminente das espécies vivas são suas longas presas. Estes são caninos alongados, que estão presentes em morsas machos e fêmeas e podem atingir um comprimento de 1 m e pesar até 5,4 kg ',
                'curiosity' => 'A morsa é um mamífero da ordem Carnivora. É o único membro sobrevivente da família Odobenidae, uma das três linhagens na subordem Pinnipedia junto com focas verdadeiras (Phocidae) e focas orelhudas (Otariidae). ',
                'size' => 'Adultos têm tipicamente 5 m de comprimento',
            ],
            [
                'name' => 'Foca de weddell',
                'description' => 'A foca Weddell desenvolve um fino casaco de pele ao redor de todo o corpo, exceto em pequenas áreas ao redor das nadadeiras. A cor e o padrão da pelagem variam, muitas vezes desbotando para uma cor mais opaca à medida que o selo envelhece. ',
                'curiosity' => 'Focas de Weddell são comumente encontradas em gelo rápido, ou gelo preso à terra, e se reúnem em pequenos grupos ao redor de rachaduras e buracos no gelo. No inverno, eles ficam na água para evitar nevascas, apenas com a cabeça cutucando através dos orifícios de respiração no gelo. ',
                'size' => 'Adultos têm tipicamente 2,5–3,5 m de comprimento',
            ],
            [
                'name' => 'Golfinho de bico branco',
                'description' => 'O golfinho de bico branco é uma espécie robusta de golfinho com bico curto. ',
                'curiosity' => 'Golfinhos de bico branco têm de 25 a 28 dentes em cada mandíbula, embora os três dentes mais próximos à parte frontal da boca muitas vezes não sejam visíveis, não podendo irromper da gengiva. Eles têm até 92 vértebras, mais do que qualquer outra espécie de golfinho oceânico ',
                'size' => 'Adultos podem atingir 2,3 a 3,1 m',
            ],
            [
                'name' => 'Golfinho de laterais brancas do atlântico',
                'description' => 'A principal característica distintiva é a mancha branca a amarelo pálido encontrada atrás da barbatana dorsal do golfinho em cada lado. Esta variação de cor é única entre as misturas de branco, cinza e azul de outros cetáceos pelágicos. ',
                'curiosity' => 'Frequentemente viajando em grandes pods e exibindo comportamentos aéreos enquanto viajam. Apesar de serem criaturas dóceis, até mesmo conhecido por interagir com várias espécies de cetecos de forma não violenta, mais notavelmente com a baleia-piloto de nadadeiras compridas ',
                'size' => 'Adultos podem atingir 2,5 m',
            ],
            [
                'name' => 'Lobo marinho',
                'description' => 'Lobos-marinhos são caracterizados por seus pavilhões externos, subpêlo denso, vibrissas e membros longos e musculosos. Eles compartilham com outros otariídeos a capacidade de girar seus membros traseiros para a frente, apoiando seus corpos e permitindo-lhes deambular em terra. ',
                'curiosity' => 'Normalmente, as focas se reúnem durante o verão em grandes colônias em praias específicas ou afloramentos rochosos para dar à luz e procriar. Todas as espécies são políginas, o que significa que os machos dominantes se reproduzem com mais de uma fêmea. ',
                'size' => 'O tamanho varia de cerca de 1,5 m, 64 kg no macho Galápagos a 2,5 m, 180 kg no macho adulto da foca da Nova Zelândia.',
            ],
            [
                'name' => 'Leão marinho',
                'description' => 'Leões marinhos são pinípedes caracterizados por abas de orelha externas, patas dianteiras longas, a capacidade de andar de quatro, cabelo curto e espesso e um grande peito e barriga.',
                'curiosity' => 'Leões marinhos consomem grandes quantidades de comida por vez e são conhecidos por comer cerca de 5–8% de seu peso corporal em uma única alimentação. Os leões marinhos podem se mover cerca de 30 km / h na água e, no seu ritmo mais rápido, podem atingir uma velocidade de cerca de 56 km / h. ',
                'size' => 'Um leão-marinho macho da Califórnia pesa em média cerca de 300 kg e tem cerca de 2,4 m de comprimento, enquanto a leoa marinha fêmea pesa 100 kg e tem 1,8 m de comprimento.',
            ],
            [
                'name' => 'Gaivota',
                'description' => 'Eles são geralmente de forma uniforme, com corpos pesados, asas longas e pescoços moderadamente longos. As caudas de todas as espécies, exceto três, são arredondadas; as exceções são a gaivota de Sabine e as gaivotas com cauda de andorinha, que têm cauda bifurcada, e a gaivota de Ross, que tem cauda em forma de cunha. ',
                'curiosity' => 'Gaivotas são alimentadores altamente adaptáveis ​​que oportunisticamente tomam uma grande variedade de presas. Os alimentos ingeridos pelas gaivotas incluem peixes e invertebrados marinhos e de água doce, vivos e já mortos, artrópodes e invertebrados terrestres, como insetos e minhocas, roedores, ovos, carniça, vísceras, répteis, anfíbios, itens de plantas como sementes e frutas, humanos refugo, chips e até outros pássaros. ',
                'size' => 'Gaivotas variam em tamanho de 120 gramas e 29 centímetros a 1,75 kg e 76 cm.',
            ],
            [
                'name' => 'Burrfish manchado',
                'description' => 'Tem um corpo rotundo, que pode ser inflado, com uma cabeça larga e romba e olhos grandes. O órgão nasal dos adultos fica em uma xícara aberta, sem caroço, que nos jovens é um tentáculo com duas aberturas. ',
                'curiosity' => 'Os dentes são fundidos em um bico parecido com um papagaio sem ranhura frontal e a boca é grande.',
                'size' => 'Eles crescem até um comprimento padrão de 50 cm, mas foram registrados até 75 cm.',
            ],
            [
                'name' => 'Enguia-europeia',
                'description' => 'A enguia europeia (Anguilla anguilla) é uma espécie de enguia, um peixe catádromo semelhante a uma cobra. As enguias têm sido importantes fontes de alimento tanto na idade adulta (incluindo enguias gelatinosas do leste de Londres) quanto na forma de enguias de vidro. ',
                'curiosity' => 'Embora a expectativa de vida da espécie na natureza não tenha sido determinada, os espécimes em cativeiro viveram mais de 80 anos. Um espécime conhecido como \ "o Brantevik Eel \" viveu por 155 anos no poço de uma casa de família em Brantevik, uma vila de pescadores no sul da Suécia. ',
                'size' => 'Eles têm normalmente cerca de 60–80 cm (2,0–2,6 pés) e raramente alcançam mais de 1 m (3 pés 3 pol.), mas podem atingir um comprimento de até 1,5 m (4 pés 11 pol.) em casos excepcionais. ',
            ],
        ];

        $creatures = Creature::all();
        foreach ($creatures as $key => $creature) {
            $creature
                ->setTranslation('name', 'pt', $PTitems[$key]['name'])
                ->setTranslation('description', 'pt', $PTitems[$key]['description'])
                ->setTranslation('curiosity', 'pt', $PTitems[$key]['curiosity'])
                ->setTranslation('size', 'pt', $PTitems[$key]['size'])
                ->save();
        }
    }
}
