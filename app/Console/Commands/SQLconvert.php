<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SQLconvert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sql:convert {--litter} {--dive} {--sighting} {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert mysql file to sqlite format';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('litter')) {
            exec(public_path() . '/sqlite/mysql2sqlite.sh ' . public_path() . '/sqlite/litter/mysql.sql | sqlite3 ' . public_path() . '/sqlite/litter/' . $this->option('name') . '.db');
        } else if ($this->option('dive')) {
            exec(public_path() . '/sqlite/mysql2sqlite.sh ' . public_path() . '/sqlite/dive/mysql.sql | sqlite3 ' . public_path() . '/sqlite/dive/' . $this->option('name') . '.db');
        } else if ($this->option('sighting')) {
            exec(public_path() . '/sqlite/mysql2sqlite.sh ' . public_path() . '/sqlite/sighting/mysql.sql | sqlite3 ' . public_path() . '/sqlite/sighting/' . $this->option('name') . '.db');
        }
    }
}
