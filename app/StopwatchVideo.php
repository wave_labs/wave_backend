<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class StopwatchVideo extends Model
{
    use FiltersRecords;

    protected $fillable =
    [
        'video',
        'thumbnail',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo('App\StopwatchCategory', 'category_id');
    }
}
