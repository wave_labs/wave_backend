<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrainwaveAnnotation extends Model
{
    protected $connection = 'mysql_brainwave';

    public function reports()
    {
        return $this->belongsToMany(BrainwaveReport::class, 'wave_brainwave.brainwave_report_has_annotations', 'report_id', 'annotation_id');
    }
}
