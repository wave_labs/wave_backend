<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;

class Report extends Model
{
    use FiltersRecords;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $fillable = ['user_id', 'question', 'response', 'subject', 'section'];

    /**
     * Get answers
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
