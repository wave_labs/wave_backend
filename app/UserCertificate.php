<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Http\Request;

use Intervention\Image\Facades\Image as IMG;
use Illuminate\Support\Facades\Validator;


class UserCertificate extends Model
{
    use FiltersRecords;
    protected $table = 'user_certificates';

    protected $guarded = ['id'];

    protected $fillable = ['user_id', 'url'];

    protected $appends = ['uid', 'status'];

    public function getUidAttribute()
    {
        return "-1";
    }

    public function getStatusAttribute()
    {
        return "done";
    }


    /**
     * Get the user associated with certificate
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Store certificates
     */
    public static function store($request, $user_id)
    {

        if ($request->hasfile('certificates')) {
            $certificates = $request->file('certificates');

            $count = count($certificates);

            foreach ($certificates as $key => $certificate) {

                if ($certificate->extension() == 'png' || $certificate->extension() == 'jpeg' || $certificate->extension() == 'jpg') {
                    $image = $certificate;
                    $filename = $user_id . '-' . uniqid() . '.png';

                    IMG::make($image)->encode('png', 65)->resize(760, null, function ($c) {
                        $c->aspectRatio();
                        $c->upsize();
                    })->save(storage_path('/uploaded/certificates/' . $filename));

                    $table_entry = new Request([
                        'user_id' => $user_id,
                        'url' => 'uploaded/certificates/' . $filename,
                    ]);

                    UserCertificate::create($table_entry->toArray());
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Certificates files with unsupported format',
                    ], 400);
                }
            }

            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Certificates upload failed',
            ], 400);
        }
    }
}
