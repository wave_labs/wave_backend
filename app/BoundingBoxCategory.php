<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoundingBoxCategory extends Model
{
    protected $fillable = ['name'];

    public function classes()
    {
        return $this->hasMany('App\BoundingBoxClass', 'category_id');
    }

    public function images()
    {
        return $this->hasMany('App\BoundingBoxImage', 'image_id');
    }
}
