<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image as IMG;

class Image extends Model
{
    protected $fillable =
    [
        'type',
        'photo_number',
        'url',
        'creature_id'
    ];

    public function creature()
    {
        return $this->belongsTo('App\Creature');
    }

    public static function getImage($index, $class)
    {
        $storagePath = storage_path('/image/' . $class . '/' . $index . '.png');

        return IMG::make($storagePath)->response();
    }

    public static function getCreatureImage($index)
    {
        $storagePath = storage_path('/image/creature/' . $index);

        if (file_exists($storagePath)) {
            return IMG::make($storagePath);
        } else {
            if (is_numeric($index)) {
                $storagePath = storage_path('/image/creature/' . $index . 'b');
                if (file_exists($storagePath)) { }
            } else {
                $newIndex = substr($index, 0, -1);;
                $storagePath = storage_path('/image/creature/' . $newIndex);
            }
        }

        return IMG::make($storagePath);
    }
}
