<?php

use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// SERVER UPDATE
Route::get('/laravel/update', 'LaravelController@update');
Route::get('/laravel/migrate', 'LaravelController@migrate');

//image download
//Route::get('/imgDownload', 'IMGDownloadController@imgDownload');

// SQLITE EXPORTS
Route::get('/create/{type}', 'ExportController@create');
Route::get('/sqlite/{type}', 'ExportController@download');


Route::get('{slug}', function () {
    return view('welcome');
})->where('slug', '(?!api)([A-z\d\/_.]+)?');


// UI
/*
//Auth
Route::name('auth.resend_confirmation')->get('/register/confirm/resend', 'Auth\RegisterController@resendConfirmation');
Route::name('auth.confirm')->get('/register/confirm/{confirmation_code}', 'Auth\RegisterController@confirm');
Route::get('/login/{social}','Auth\LoginController@socialLogin')->where('social','google|facebook');
Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','google|facebook');
Auth::routes();
*/