<?php

use App\Http\Controllers\AddDivingSpotToUserInvokable;
use App\Http\Controllers\API\Brainwave\BrainwaveReportController;
use App\Http\Controllers\API\Brainwave\FetchBrainwaveAnnotationsInvokable;
use App\Http\Controllers\InsectClassesInvokable;
use App\Http\Controllers\InsectKingdomsInvokable;
use App\Http\Controllers\InsectPhylumsInvokable;
use App\Http\Controllers\InsectOrderInvokable;
use App\Http\Controllers\InsectSubOrderInvokable;
use App\Http\Controllers\InsectInfraOrderInvokable;
use App\Http\Controllers\InsectSuperFamilyInvokable;
use App\Http\Controllers\InsectFamilyInvokable;
use App\Http\Controllers\InsectSubFamilyInvokable;
use App\Http\Controllers\InsectTribeInvokable;
use App\Http\Controllers\InsectGenusInvokable;
use App\Http\Controllers\InsectSpecificInvokable;
use App\Http\Controllers\InsectSubGenusInvokable;
use App\Http\Controllers\InsectInfraSpecificInvokable;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Route;

Route::get('/country', 'API\CountryController@index'); //Get all countries
Route::get('validate/{pending_points_id}', 'API\UserController@validation'); //Validate points for a report

Route::get('password-recover', [MailController::class, 'passwordRecover']);
Route::get('confirm-email', [MailController::class, 'confirmEmail']);

Route::prefix('database')->group(function () {
    Route::get('version/{type}', 'ExportController@index'); //Get current version of DB
    Route::get('create/{type}', 'ExportController@create'); //Dump sql file and create sqlite
    Route::get('download/{type}', 'ExportController@download'); //Download sqlite file
});

Route::get('sighting/{sighting}/trip', 'API\SightingController@indexTrip');
Route::get('climarests/areas', 'API\Studies\ClimarestController@indexArea');
Route::get('climarests/areaStakeholders/{climarest_id}', 'API\Studies\ClimarestController@indexAreaStakeholders');

//Auth routes
Route::post('register', 'Auth\AuthController@register');
Route::post('login', 'Auth\AuthController@login');
Route::patch('profile-picture/{user}', 'API\UserController@changePicture');
Route::post('activate/{user}', 'Auth\AuthController@activateUser');
Route::get('refresh', 'Auth\AuthController@refresh');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('me', 'Auth\AuthController@me');
    Route::put('me', 'Auth\AuthController@updateMe');
    Route::get('history', 'Auth\AuthController@history');
    Route::get('logout', 'Auth\AuthController@logout');
});

Route::prefix('password')->group(function () {
    //api call for when the user introduces the email to recover the account
    Route::post('recover', 'Auth\AuthController@recoverPassword');
    //api call for when the user already introduced the new password
    Route::post('reset', 'Auth\AuthController@resetPassword');
});

Route::get('/gpstagsatellite', 'GpsTagSatelliteController@index');
Route::get('/gpsgatewaysatellite', 'GpsGatewaySatelliteController@index');

//Emailed Auth routes
Route::get('user/verify/{token}', 'Auth\AuthController@verifyUser');
Route::get('password/reset/{token}', 'Auth\AuthController@showResetForm');

//App images routes
Route::prefix('image')->group(function () {
    Route::get('ai/{index}', 'API\ImageController@showAi');
    Route::get('creature/{index}', 'API\ImageController@showCreature');
    Route::get('{class}/{index}', 'API\ImageController@show');
    Route::get('sighting', 'API\ImageController@indexSighting');
    Route::get('dive', 'API\ImageController@indexDive');
    Route::get('litter', 'API\ImageController@indexLitter');
    Route::get('litter-category', 'API\ImageController@indexLitterCategory');
});

Route::prefix('audio')->group(function () {
    Route::get('{class}/{index}', 'API\AudioController@show');
    Route::get('sighting', 'API\AudioController@indexSighting');
});

Route::prefix('video')->group(function () {
    Route::get('{class}/{index}', 'API\VideoController@show');
    Route::get('sighting', 'API\VideoController@indexSighting');
});

Route::prefix('photo')->group(function () {
    Route::get('litter', 'API\LitterPhotoController@index');
    Route::get('litter/{litter}', 'API\LitterPhotoController@show');
    Route::get('iotDevice', 'API\IotDevicePhotoController@index');
    Route::get('iotDevice/{iotDevice}', 'API\IotDevicePhotoController@show');
    Route::get('sighting', 'API\SightingPhotoController@index');
    Route::get('sighting/{sighting}', 'API\SightingPhotoController@show');
    Route::get('dive', 'API\DivePhotoController@index');
    Route::get('dive/{dive}', 'API\DivePhotoController@show');
});

//Remote select containers routes
Route::prefix('selector')->group(function () {
    Route::get('insect-reports', 'API\Insect\InsectReportController@selector');
    Route::get('insect-references', 'API\Insect\InsectReferenceController@selector');
    Route::get('creature', 'API\CreatureController@selector');
    Route::get('creature-type', 'API\CreatureTypeController@selector');
    Route::get('litter-sub-category', 'API\LitterSubCategoryController@selector');
    Route::get('litter-category', 'API\LitterCategoryController@selector');
    Route::get('user', 'API\UserController@selector');
    Route::get('diving-spot-substract', 'API\DivingSpotSubstractController@selector');
    Route::get('user-occupation', 'API\UserOccupationController@selector');
    Route::get('diving-spot', 'API\DivingSpotController@selector');
    Route::get('user-diving-spot/{userID}', 'API\DivingSpotController@selectorUser');
    Route::get('beaufort-scale', 'API\BeaufortScaleController@selector');
    Route::get('vehicle', 'API\VehicleController@selector');
    Route::get('vehicle-type', 'API\VehicleTypeController@selector');
    Route::get('sighing-behaviour', 'API\SightingBehaviourController@selector');
    Route::get('suggestion-type', 'API\SuggestionTypeController@selector');
    Route::get('source', 'API\SourceController@selector');

    Route::get('freshwater-pin', 'API\FreshwaterPinController@selector');
    Route::get('trout', 'API\Studies\TroutController@selector');
    Route::get('soft-coral', 'API\Studies\SoftCoralController@selector');
    Route::get('diadema', 'API\DiademaController@selector');

    Route::get('insect-family', 'API\Insect\InsectFamilyController@selector');
    Route::get('insect-report', 'API\Insect\InsectReportController@selector');

    Route::get('activity', 'API\ActivityController@selector');

    // Route::get('iot-unit', 'API\IotUnitController@selector');
    // Route::get('iot-data-type', 'API\IotDataTypeController@selector');
    Route::get('iot-device-type', 'API\IotDeviceTypeController@selector');
    Route::get('iot-device', 'API\IotDeviceController@selector');
    Route::get('iot-environment-type', 'API\IoTEnvironmentTypeController@selector');
    Route::get('iot-device-state', 'API\IoTDeviceStateController@selector');
    Route::get('iot-report', 'API\IotReportController@selector');
    Route::get('iot-feature', 'API\IotFeatureController@selector');
    Route::get('iot-template', 'API\IotTemplateController@selector');
    Route::get('iot-rule', 'API\IotRuleController@selector');

    //taxons
    Route::get('insect-species', 'API\Insect\InsectOccurrenceController@speciesSelector');
    Route::get('insect-subspecies', 'API\Insect\InsectOccurrenceController@subspeciesSelector');
});

//Map coordinates
Route::prefix('coordinates')->group(function () {
    Route::get('index', 'API\CoordinateController@index');
    Route::get('center', 'API\CoordinateController@coordinatesAverage');
    Route::get('sighting', 'API\SightingController@indexAllCoordinates');
    Route::get('litter', 'API\LitterController@indexAllCoordinates');
    Route::get('iot-report', 'API\IotReportController@indexAllCoordinates');
    Route::get('dive', 'API\DiveController@indexAllCoordinates');
    Route::get('markers', 'API\MarkerCoordinatesController');
});

Route::prefix('coordinates-for-line-draw')->group(function () {
    Route::get('iot-report', 'API\IotReportController@indexAllCoordinatesForLineDraw');
});



Route::prefix('sighting')->group(function () {
    Route::get('next-prior/{sighting}', 'API\SightingController@nextAndPrior');
});

Route::prefix('litter')->group(function () {
    Route::get('next-prior/{litter}', 'API\LitterController@nextAndPrior');
});

Route::prefix('iot-report')->group(function () {
    Route::get('next-prior/{litter}', 'API\IotReportController@nextAndPrior');
});

Route::prefix('iot-device')->group(function () {
    Route::get('next-prior/{litter}', 'API\IotDeviceController@nextAndPrior');
});

Route::get('iot-report/generateCustomGraph', 'API\IotReportController@generateCustomGraph');
Route::get('iot-report/waveMeasurements', 'API\IotReportController@getWaveMeasurements');
Route::get('iot-report/fieldsTimeline', 'API\IotReportController@getFieldsTimeline');
Route::get('iot-report/latestReport', 'API\IotReportController@latestReport');
Route::delete('iot-report/delete/multiple', 'API\IotReportController@destroyMultiple');

Route::apiResource('capsule', 'API\CapsuleController');

Route::apiResource('report', 'API\ReportController');

Route::apiResource('sighting', 'API\SightingController');
Route::apiResource('sighting-data', 'API\SightingsDataController');
Route::apiResource('sighting-estimation', 'API\SightingEstimationController');
Route::apiResource('trip', 'API\TripController');
Route::apiResource('trip-position', 'API\TripPositionController');
Route::apiResource('beaufort-scale', 'API\BeaufortScaleController');

Route::apiResource('creature', 'API\CreatureController');
Route::apiResource('creature-type', 'API\CreatureTypeController');

Route::apiResource('litter-category', 'API\LitterCategoryController');
Route::apiResource('litter-sub-category', 'API\LitterSubCategoryController');
Route::apiResource('litter', 'API\LitterController');
Route::apiResource('litter-report', 'API\LitterReportController');

Route::apiResource('user', 'API\UserController');
Route::apiResource('user-occupation', 'API\UserOccupationController');
Route::apiResource('user-dive-center', 'API\UserDiveCenterController');
Route::apiResource('user-person', 'API\UserPersonController');
Route::middleware('auth:api', 'role:admin')->post('user/send-password-reset-email/{id}', 'API\UserController@sendResetPasswordEmail');

Route::apiResource('diving-spot', 'API\DivingSpotController');
Route::post('diving-spot-user/{divingSpot}', 'AddDivingSpotToUserInvokable');
Route::post('remove-diving-spot-user', 'RemoveDivingSpotFromUserInvokable');
Route::apiResource('dive', 'API\DiveController');
Route::get('dive-monthly', 'API\DiveController@monthlyReport');
Route::get('dive-most-reported', 'API\DiveController@mostReportedSpecies');
Route::get('dive-species-reports', 'API\DiveController@speciesReports');
Route::get('dive-ranking', 'API\DiveController@rankingReports');
Route::apiResource('abundance', 'API\AbundanceController');

Route::apiResource('survey', 'API\SurveyController');
Route::apiResource('form', 'API\FormController');
// Route::apiResource('message', 'API\MessageController');

Route::apiResource('whats-new', 'API\WhatsNewController');

Route::apiResource('suggestion', 'API\SuggestionController');
Route::apiResource('contact', 'API\ContactUsController');
Route::apiResource('suggestion-type', 'API\SuggestionTypeController');

Route::apiResource('ai-megafauna', 'API\TrackerController');
Route::apiResource('ai-detection', 'API\AiDetectionController');
Route::post('ai-detection/preview', 'API\AiDetectionController@storePreview');
Route::apiResource('ai-label', 'API\AiLabelController');
Route::apiResource('ai-detection-image', 'API\AiDetectionImageController');
Route::apiResource('ai-category', 'API\AiCategoryController');
Route::apiResource('ai-model', 'API\AiModelController');

Route::apiResource('ar-dome', 'API\ArDomeController');
Route::apiResource('domes', 'API\DomeController');

Route::prefix('dome-interaction')->group(function () {
    Route::get('/', 'API\DomeHasInteractionController@index');
    Route::put('/increment', 'API\DomeHasInteractionController@increment');
});

// Route::apiResource('iot-unit', 'API\IotUnitController');
// Route::apiResource('iot-data-type', 'API\IotDataTypeController');
Route::apiResource('iot-device-type', 'API\IotDeviceTypeController');
Route::apiResource('iot-device', 'API\IotDeviceController');
Route::apiResource('iot-environment-type', 'API\IotEnvironmentTypeController');
Route::apiResource('iot-device-state', 'API\IotDeviceStateController');
Route::apiResource('iot-report', 'API\IotReportController');
Route::apiResource('iot-template', 'API\IotTemplateController');
Route::apiResource('iot-feature', 'API\IotFeatureController');

Route::apiResource('insect-references', 'API\Insect\InsectReferenceController');
Route::get('reference-authors', 'API\Insect\InsectReferenceController@authorsSelector');
Route::apiResource('insect-family', 'API\Insect\InsectFamilyController');
Route::apiResource('insect-reports', 'API\Insect\InsectReportController');
Route::apiResource('insect-zones', 'API\Insect\InsectZoneController');
Route::apiResource('insect-species', 'API\Insect\InsectSpeciesController');
Route::post('delete-insect-taxon', 'API\Insect\InsectSpeciesController@deleteTaxon');
Route::post('insect-species/{taxonRank}/{id}', 'API\Insect\InsectSpeciesController@updateTaxon');
Route::apiResource('insect-sinonimia', 'API\Insect\InsectSinonimiaController');
Route::apiResource('insect-occurrences', 'API\Insect\InsectOccurrenceController');
Route::apiResource('insect-projects', 'API\Insect\InsectProjectController');
Route::post('insect-projects/add-member/{projectId}', 'API\Insect\InsectProjectController@addMember');
Route::post('insect-projects/remove-member/{userId}/{projectId}', 'API\Insect\InsectProjectController@removeMember');


Route::prefix('studies')->group(function () {
    Route::prefix('stopwatch')->group(function () {
        Route::apiResource('videos', 'API\StopwatchVideoController');
        Route::get('next-video/{stopwatchVideo}', 'API\StopwatchVideoController@next');
        Route::apiResource('categories', 'API\StopwatchCategoryController');
        Route::apiResource('class', 'API\StopwatchClassController');
        Route::apiResource('result', 'API\StopwatchResultController');
    });
    Route::prefix('bounding-box')->group(function () {
        Route::apiResource('images', 'API\BoundingBoxImageController');
        Route::apiResource('class', 'API\BoundingBoxClassController');
        Route::apiResource('result', 'API\BoundingBoxResultController');
    });
    Route::apiResource('diadema', 'API\DiademaController');
    Route::apiResource('', 'API\StudyController');
});

Route::apiResource('activities', 'API\ActivityController');
Route::apiResource('markers', 'API\MarkerController');

Route::apiResource('climarests', 'API\Studies\ClimarestController');

Route::apiResource('freshwater-pin', 'API\FreshwaterPinController');
Route::apiResource('trout', 'API\Studies\TroutController');
Route::apiResource('soft-coral', 'API\Studies\SoftCoralController');
Route::apiResource('copernicus-product', 'API\CopernicusProductController');

Route::prefix('statistics')->group(function () {
    Route::prefix('litter')->group(function () {
        Route::get('', 'API\AnalyticsController@litter');
        Route::get('category', 'API\AnalyticsController@litterCategory');
        Route::get('category/{category}', 'API\AnalyticsController@litterSpecificCategory');
    });
    Route::prefix('sighting')->group(function () {
        Route::get('', 'API\AnalyticsController@sighting');
        Route::get('creature', 'API\AnalyticsController@sightingCreature');
        Route::get('creature/{creature}', 'API\AnalyticsController@sightingSpecificCreature');
    });
    Route::prefix('dive')->group(function () {
        Route::get('', 'API\AnalyticsController@dive');
        Route::get('creature/{creature}', 'API\AnalyticsController@diveCreature');
        Route::get('diving-spot', 'API\AnalyticsController@diveDivingSpot');
        Route::get('diving-spot/{divingSpot}', 'API\AnalyticsController@diveSpecificDivingSpot');
    });
    Route::prefix('ai')->group(function () {
        Route::get('specie', 'API\AnalyticsController@aiAccuracy');
        Route::get('history', 'API\AnalyticsController@aiFrequency');
    });
});

Route::get('dive/diving-spot/{diving_spot}', 'API\DiveController@indexDivingSpot');

//Feedback routes
Route::get('form/{form}', 'API\QuestionController@index');
Route::get('feedback/{form}', 'API\FeedbackController@index');
Route::get('result/{id}', 'API\ResultController@index');
Route::post('result', 'API\ResultController@store');
Route::put('question/{question}', 'API\QuestionController@update');


//Routes para a API de recolha de datos dos IoT
Route::get('reports', 'API\IotReportsController@index');
Route::get('reports/{report}', 'API\IotReportsController@show');
Route::post('reports', 'API\IotReportsController@store');
Route::put('reports/{report}', 'API\IotReportsController@update');

Route::prefix('record')->group(function () {
    Route::post('', 'API\RecordController@increment');
    Route::prefix('ardome')->group(function () {
        Route::post('image', 'API\RecordController@ardomeImage');
    });
});



//Export csv file of surveys
Route::prefix('export')->group(function () {
    Route::prefix('csv')->group(function () {
        Route::get('dive', 'API\DiveController@csvExport');
        Route::get('litter', 'API\LitterController@csvExport');
        Route::get('sighting', 'API\SightingController@csvExport');
        Route::get('marker', 'API\MarkerController@csvExport');
        Route::get('freshwater-pin', 'API\FreshwaterPinController@csvExport');
        Route::get('trout', 'API\Studies\TroutController@csvExport');
        Route::get('soft-coral', 'API\Studies\SoftCoralController@csvExport');
        Route::get('climarest', 'API\Studies\ClimarestController@csvExport');
        Route::get('diadema', 'API\DiademaController@csvExport');
        Route::get('iot-report', 'API\IotReportController@csvExport');
        Route::get('stopwatch-result', 'API\StopwatchResultController@csvExport');
        Route::get('insect', 'API\Insect\InsectReferenceController@csvExport');
        Route::get('sinonimias', 'API\Insect\InsectReferenceController@exportSinonimia');
    });
    Route::get('/gpstagsatellite', 'GpsTagSatelliteController@download');
    Route::get('/gpsgatewaysatellite', 'GpsGatewaySatelliteController@download');
});

//Import csv file of surveys
Route::prefix('import')->group(function () {
    Route::prefix('csv')->group(function () {
        Route::post('/insect-sinonimias', 'API\Insect\InsectSinonimiaController@import_sinonimias');
        Route::post('/insect-reports', 'API\Insect\InsectReportController@import_reports');
        Route::post('/insect-references', 'API\Insect\InsectReferenceController@importReferences');
        Route::post('/insect-taxon', 'API\Insect\InsectSpeciesController@importTaxon');
        Route::post('/insect-occurrences', 'API\Insect\InsectOccurrenceController@importOccurrence');
    });
});

route::post('/success', 'API\Insect\InsectSpeciesController@success');


Route::get('/update/spectrograms', 'API\AcousticController@softUpdate');
Route::get('/download/spectrograms', 'API\AcousticController@download');

Route::get('export/{Idate}/{Fdate}', 'API\ResultController@storeExcel');

Route::post('litter/{litter}', 'API\LitterController@confirm');

Route::get('creature/size/{creature}', 'API\CreatureSizeController');

Route::get('creature/activities/{creature}', 'API\CreatureActivityController');

Route::get('insect-kingdoms', 'InsectKingdomsInvokable');
Route::get('insect-phylums', 'InsectPhylumsInvokable');
Route::get('insect-classes', 'InsectClassesInvokable');
Route::get('insect-orders', 'InsectOrderInvokable');
Route::get('insect-sub-orders', 'InsectSubOrderInvokable');
Route::get('insect-infra-orders', 'InsectInfraOrderInvokable');
Route::get('insect-super-families', 'InsectSuperFamilyInvokable');
Route::get('insect-families', 'InsectFamilyInvokable');
Route::get('insect-sub-families', 'InsectSubFamilyInvokable');
Route::get('insect-tribes', 'InsectTribeInvokable');
Route::get('insect-genuses', 'InsectGenusInvokable');
Route::get('insect-sub-genuses', 'InsectSubGenusInvokable');
Route::get('insect-specifics', 'InsectSpecificInvokable');
Route::get('insect-infra-specifics', 'InsectInfraSpecificInvokable');


Route::prefix('enm')->group(function () {
    Route::get('queryOcurrence', 'API\EnmController@queryOcurrenceData');
    Route::post('uploadOcurrence', 'API\EnmController@uploadOcurrenceData');
    Route::get('getOcurrenceDataFromPipeline', 'API\EnmController@getOcurrenceDataFromPipeline');
    Route::put('deleteRecord/{Enm}/{record}', 'API\EnmController@deleteRecord');
    Route::put('cropGeographicExtent/{Enm}', 'API\EnmController@cropGeographicExtent');
    Route::put('deleteDuplicates/{Enm}', 'API\EnmController@deleteDuplicates');
    Route::put('limitOcurrences/{Enm}', 'API\EnmController@limitOcurrenceNumber');
    Route::get('getEnvData/{Enm}', 'API\EnmController@getEnvData');
    Route::post('uploadEnvironmentalData/{Enm}', 'API\EnmController@uploadEnvironmentalData');
    Route::get('getCopernicusData/{Enm}', 'API\EnmController@getCopernicusData');
    Route::get('getEnvImage', 'API\EnmController@getEnvImage');
    Route::get('getPredictionImage', 'API\EnmController@getPredictionImage');
    Route::post('createPrediction/{Enm}', 'API\EnmController@createPrediction');
    Route::put('setStep/{Enm}', 'API\EnmController@setStep');
    Route::get('', 'API\EnmController@index');
    Route::delete('{Enm}', 'API\EnmController@destroy');
});

Route::apiResource('desertas', 'API\DesertaController');


Route::apiResource('brainwave/report', 'API\Brainwave\BrainwaveReportController');
Route::get('brainwave/annotation/{brainwaveReport}', 'API\Brainwave\FetchBrainwaveAnnotationsInvokable');
Route::get('brainwave/zip/{brainwaveReport}', 'API\Brainwave\ZipBrainwaveInvokable');

Route::post('arquivo/upload', 'API\GetArquivoPredictionInvokable');
