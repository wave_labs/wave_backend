<?php

namespace Tests\Feature;


use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DivingSpot extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function it_will_show_all_divingSpot()
    {
        $divingSpot = factory(DivingSpot::class, 10)->create();

        $response = $this->get(route('divingspot.index'));

        $response->assertStatus(200);

        $response->assertJson($divingSpot->toArray());
    }

    /** @test */
    public function it_will_create_divingSpot()
    {
        $response = $this->post(route('divingspot.store'), [
            'longitude'       => '0000000',
            'latitude'        => '0000000',
            'range' => 'This is a range',
            'name'       => 'This is a name',
            'description' => 'This is a description',
  
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('divingspot', [
            'longitude'       => '0000000',
            'latitude'        => '0000000',
            'range' => 'This is a range',
            'name'       => 'This is a name',
            'description' => 'This is a description',
           
        ]);

        $response->assertJsonStructure([
            'message',
            'divingspot' => [
            'longitude',
            'latitude',
            'range',
            'name',
            'description',
            'substract',
            'protection',
            'updated_at',
            'created_at',
            'id'
            ]
        ]);
    }

    /** @test */
    public function it_will_show_a_divingspot()
    {
        $this->post(route('divingspot.store'), [
            'longitude'       => '0000000',
            'latitude'        => '0000000',
            'range' => 'This is a range',
            'name'       => 'This is a name',
            'description' => 'This is a description',
           
        ]);

        $divingSpot = DivingSpot::all()->first();

        $response = $this->get(route('divingspot.show', $diveSpot->id));

        $response->assertStatus(200);

        $response->assertJson($divingSpot->toArray());
    }

    /** @test */
    public function it_will_update_a_divingSpot()
    {
        $this->post(route('divingspot.store'), [
            'longitude'       => '0000000',
            'latitude'        => '0000000',
            'range' => 'This is a range',
            'name'       => 'This is a name',
            'description' => 'This is a description',
           
        ]);

        $divingSpot = DivingSpot::all()->first();

        $response = $this->put(route('divingspot.update', $task->id), [
            'longitude'       => '0000000',
            'latitude'        => '0000000',
            'range' => 'This is a range',
            'name'       => 'This is a name',
            'description' => 'This is a description',
          
        ]);

        $response->assertStatus(200);

        $divingSpot = $divingSpot->fresh();

        $this->assertEquals($divingSpot->title, 'This is the updated title');

        $response->assertJsonStructure([
           'message',
            'divingspot' => [
            'longitude',
            'latitude',
            'range',
            'name',
            'description',
            'substract',
            'protection',
            'updated_at',
            'created_at',
            'id'
           ]
       ]);
    }

    /** @test */
    public function it_will_delete_a_divingSpot()
    {
        $this->post(route('divingspot.store'), [
            'longitude'       => '0000000',
            'latitude'        => '0000000',
            'range' => 'This is a range',
            'name'       => 'This is a name',
            'description' => 'This is a description',
          
        ]);

        $divingSpot = DivingSpot::all()->first();

        $response = $this->delete(route('divingspot.destroy', $task->id));

        $divingSpot = $divingSpot->fresh();

        $this->assertNull($divingSpot);

        $response->assertJsonStructure([
            'message'
        ]);
    }
}
