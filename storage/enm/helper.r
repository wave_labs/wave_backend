library("CoordinateCleaner")

readOcurrenceData <- function(filename, nrows=FALSE) {
  data <- read.csv(file = paste("data", paste(filename, "csv", sep="."), sep="/"), nrows = nrows)
  return (data)
}

writeOcurrenceData <- function(data, filename) {
  write.csv(data, file = paste("data", paste(filename, "csv", sep="."), sep="/"), row.names = FALSE)
}

argsToVars <- function(args, parameters) {
  i<-1
  vector<-c()
  
  for (par in parameters) {
    vector[par] <- args[i]
    i<-i+1
  }
  
  return (vector)
}

validateCoordinates <- function(data) {
  for(i in 1:nrow(data))
  {
    data$longitude[i] <- as.numeric(sub(",", ".", data$longitude[i], fixed = TRUE))
    data$latitude[i] <- as.numeric(sub(",", ".", data$latitude[i], fixed = TRUE))
  }
  
 
  data <- cc_val(data,
                lon = "longitude", 
                lat = "latitude",
                value = "clean",
                verbose = TRUE)
  return (data)
  }
 

