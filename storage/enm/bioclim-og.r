library("sp")
library("raster")
library("maptools")
library("rgdal")
library("dismo")
library("ncdf4") # package for netcdf manipulation
setwd("~/projects/wave/storage/enm")

# bioclim.data <- getData(name = "worldclim", var = "bio",res = 2.5, path = "data/")
# bioclim.data <- stack(raster("data/envs/Mad_500m.tif"))
# bioclim.data <- stack(raster("data/envs/gebo.asc"))
bioclim.data <- nc_open('data/envs/2003_1.netcdf')
bioclim.variables <- names(bioclim.data$var)

lon <- ncvar_get(bioclim.data, "longitude")
lat <- ncvar_get(bioclim.data, "latitude", verbose = F)
t <- ncvar_get(bioclim.data, "time")
mlotst <- ncvar_get(bioclim.data, "mlotst")

# fillvalue <- ncvar_get(bioclim.data, "mlotst", "_FillValue")
nc_close(bioclim.data) 

#mlotst[mlotst == fillvalue$value] <- NA
#ndvi.slice <- mlotst[, , 1] 

r <- raster(mlotst, xmn=min(lon), xmx=max(lon), ymn=min(lat), ymx=max(lat))
r <- flip(r, direction='y')
plot(r)

bioclim.data <- stack(r)

# Read in saguaro observations
obs.data <- read.csv(file = "data/Carnegiea-gigantea-GBIF.csv")

# Notice NAs - drop them before proceeding
obs.data <- obs.data[!is.na(obs.data$latitude), ]

# Determine geographic extent of our data
max.lat <- ceiling(max(obs.data$latitude))
min.lat <- floor(min(obs.data$latitude))
max.lon <- ceiling(max(obs.data$longitude))
min.lon <- floor(min(obs.data$longitude))
geographic.extent <- extent(x = c(min.lon, max.lon, min.lat, max.lat))

# Load the data to use for our base map
data(wrld_simpl)

# Crop environmental variables to geographic extent of saguaro
bioclim.data <- crop(x = bioclim.data, y = geographic.extent)

# bioclim takes 2 args, removing id and keeping lat and lng
obs.data <- obs.data[, c("latitude", "longitude")]

# reverse order bcs bioclim takes lng then lat
obs.data <- obs.data[, c("longitude", "latitude")]

# Build species distribution model
bc.model <- bioclim(x = bioclim.data, p = obs.data)

# Predict presence from model
predict.presence <- dismo::predict(object = bc.model, 
                                   x = bioclim.data, 
                                   ext = geographic.extent)

# Plot base map
plot(wrld_simpl, 
     xlim = c(min.lon, max.lon),
     ylim = c(min.lat, max.lat),
     axes = TRUE, 
     col = "grey95")


# Add model probabilities
plot(predict.presence, add = TRUE)

# Redraw those country borders
plot(wrld_simpl, add = TRUE, border = "grey5")

# Add original observations
points(obs.data$longitude, obs.data$latitude, col = "olivedrab", pch = 20, cex = 0.75)

