library("jsonlite")

setwd("~/projects/wave/storage/enm")
args = commandArgs(trailingOnly=TRUE)

source("helper.r")
args<- argsToVars(args, list("referenceId", "record"))
obs.data <- readOcurrenceData(args["referenceId"])
noRecord <- obs.data[-c(as.integer(args["record"])), ]

df <- jsonlite::toJSON(noRecord)
cat(df)

writeOcurrenceData(noRecord, args["referenceId"])
