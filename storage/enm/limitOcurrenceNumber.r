library("jsonlite")
setwd("~/projects/wave/storage/enm")
args = commandArgs(trailingOnly=TRUE)

source("helper.r")
args<- argsToVars(args, list("referenceId", "nrows"))
obs.data <- readOcurrenceData(args["referenceId"], nrows=as.numeric(args["nrows"]))

df <- jsonlite::toJSON(obs.data)
cat(df)

writeOcurrenceData(obs.data, args["referenceId"])