setwd("~/projects/wave/storage/enm")
args = commandArgs(trailingOnly=TRUE)

aReferenceId <- args[1]
aBioclimVars <- args[2]
aResolution <- args[3]
environmentalDataSource <- args[4]
aKfold <- args[5]
environmentalDataPath <- args[6]

source("runModel.r")

# Build species distribution model
bc.model <- bioclim(x = bioworldclim, p = presence.train)

# Predict presence from model
predict.presence <- dismo::predict(object = bc.model, x = bioworldclim, ext = geographic.extent)

# Use testing data for model evaluation
bc.eval <- evaluate(p = presence.test,   # The presence testing data
                    a = background.test, # The absence testing data
                    model = bc.model,    # The model we are evaluating
                    x = bioworldclim)    # Climatic variables for use by model

# Determine minimum threshold for "presence"
bc.threshold <- threshold(x = bc.eval, stat = "spec_sens")

#----------------------------Presence/Absence


#png(filename= paste("output", paste(paste(args[2], "boxplot", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
#boxplot(bc.eval)
#dev.off()
#png(filename= paste("output", paste(paste(args[2], "density", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
# density(bc.eval)
# dev.off()

png(filename= paste("output", paste(paste(paste(aReferenceId, "bioclim", sep="-"), "presence-absence", sep="-"), "png", sep="."), sep="/"), width=720, height=720)

plot(wrld_simpl, xlim = c(min.lon, max.lon), ylim = c(min.lat, max.lat), axes = TRUE, col = "grey95")
plot(predict.presence > bc.threshold, add = TRUE, legend = FALSE)
plot(wrld_simpl, add = TRUE, border = "grey5")
points(obs.data, col = "red", pch = 20, cex = 0.75)

dev.off()

#---------------------------- RASTER OBJECT

png(filename= paste("output", paste(paste(paste(aReferenceId, "bioclim", sep="-"), "rasterobject", sep="-"), "png", sep="."), sep="/"), width=720, height=720)

plot(wrld_simpl, xlim = c(min.lon, max.lon), ylim = c(min.lat, max.lat), axes = TRUE, col = "grey95")
plot(predict.presence, add = TRUE)
plot(wrld_simpl, add = TRUE, border = "grey5")

dev.off()
#----------------------------

png(filename= paste("output", paste(paste(paste(aReferenceId, "bioclim", sep="-"), "responseplot", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
response(bc.model)
dev.off()

png(filename= paste("output", paste(paste(paste(aReferenceId, "bioclim", sep="-"), "tpr", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
plot(bc.eval, 'TPR')
dev.off()

png(filename= paste("output", paste(paste(paste(aReferenceId, "bioclim", sep="-"), "roc", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
plot(bc.eval, 'ROC')
dev.off()




