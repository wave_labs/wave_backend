library("sp")
library("raster")
library("maptools")
library("rgdal")
library("dismo")

data(wrld_simpl)
variables <- aBioclimVars
variables <- as.integer(unlist(strsplit(variables, ",")))

# Read in occurrences
obs.data <- read.csv(file = paste("data", paste(aReferenceId, "csv", sep="."), sep="/"))

# Determine geographic extent of our data
max.lat <- ceiling(max(obs.data$latitude))
min.lat <- floor(min(obs.data$latitude))
max.lon <- ceiling(max(obs.data$longitude))
min.lon <- floor(min(obs.data$longitude))
geographic.extent <- extent(x = c(min.lon, max.lon, min.lat, max.lat))

source("getEnvData.r")



bioworldclim <- crop(x = bioworldclim, y = geographic.extent)
pred_nf <- dropLayer(bioworldclim, 'biome')

# Create pseudo-absence, or background, points
# Use the bioclim data files for sampling resolution
bil.files <- list.files(path = "data/wc2-5", 
                        pattern = "*.bil$", 
                        full.names = TRUE)

mask <- raster(bil.files[1])

# Randomly sample points (same number as our observed points)
background <- randomPoints(mask = mask,     # Provides resolution of sampling points
                           n = nrow(obs.data),      # Number of random points
                           ext = geographic.extent, # Spatially restricts sampling
                           extf = 1.25)             # Expands sampling a little bit

# Create vector of group memberships
group.presence <- kfold(x = obs.data, k = as.integer(aKfold))

testing.group <- 1
# Separate observations into training and testing groups
presence.train <- obs.data[group.presence != testing.group, ]
presence.test <- obs.data[group.presence == testing.group, ]

# Repeat the process for pseudo-absence points
group.background <- kfold(x = background, k = as.integer(aKfold))
background.train <- background[group.background != testing.group, ]
background.test <- background[group.background == testing.group, ]





