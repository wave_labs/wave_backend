library("raster")

args = commandArgs(trailingOnly=TRUE)

setwd("~/projects/wave/storage/enm")

source("helper.r")

args<- argsToVars(args, list("referenceId", "extension", "key"))

if(args["extension"] != "tif"){
  file <- paste(args["key"], args["extension"], sep=".")
  env.data <- raster(paste(paste("data/envs", args["referenceId"], sep="/"), file, sep="/"))
  
  writeRaster(env.data, paste(paste("data/envs", args["referenceId"], sep="/"), paste(args["key"], "tif", sep="."), sep="/"), format="GTiff", overwrite=TRUE)
  unlink(paste(paste("data/envs", args["referenceId"], sep="/"), file, sep="/"))
}
