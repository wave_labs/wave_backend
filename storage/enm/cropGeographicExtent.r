library("jsonlite")

args = commandArgs(trailingOnly=TRUE)
setwd("~/projects/wave/storage/enm")

source("helper.r")

args<- argsToVars(args, list("referenceId", "latMin", "lonMin", "latMax", "lonMax"))
obs.data <- readOcurrenceData(args["referenceId"])

max.lat <- as.numeric(args["latMax"])
min.lat <- as.numeric(args["latMin"])
max.lon <- as.numeric(args["lonMax"])
min.lon <- as.numeric(args["lonMin"])

removeRows = c()
for(i in 1:nrow(obs.data))
{
  if(as.numeric(obs.data$longitude[i]) > max.lon || 
     as.numeric(obs.data$longitude[i]) < min.lon || 
     as.numeric(obs.data$latitude[i]) > max.lat || 
     as.numeric(obs.data$latitude[i]) < min.lat) {
    removeRows <- c(removeRows, i)
  }
}
obs.data <- obs.data[-removeRows, ]

df <- jsonlite::toJSON(obs.data)
cat(df)

writeOcurrenceData(obs.data, args["referenceId"])
