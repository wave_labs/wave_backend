library("sp")
library("raster")
library("maptools")
library("rgdal")
library("biomod2")

bioclim.data <- getData(name = "worldclim",
                        var = "bio",
                        res = 2.5,
                        path = "data/")

# Read in saguaro observations
obs.data <- read.csv(file = "data/Carnegiea-gigantea-GBIF.csv")

# Notice NAs - drop them before proceeding
obs.data <- obs.data[!is.na(obs.data$latitude), ]

# Determine geographic extent of our data
max.lat <- ceiling(max(obs.data$latitude))
min.lat <- floor(min(obs.data$latitude))
max.lon <- ceiling(max(obs.data$longitude))
min.lon <- floor(min(obs.data$longitude))
geographic.extent <- extent(x = c(min.lon, max.lon, min.lat, max.lat))

# Load the data to use for our base map
data(wrld_simpl)

# Crop environmental variables to geographic extent of saguaro
bioclim.data <- crop(x = bioclim.data, y = geographic.extent)
# bioclim takes 2 args, removing id and keeping lat and lng
obs.data <- obs.data[, c("longitude", "latitude")]


bioclim.data <- as.matrix(bioclim.data)
bioclim.data <- head(bioclim.data, 400)
# Build species distribution model
bc.model <- sre(Explanatory = bioclim.data, Response = obs.data, NewData=obs.data)

# Predict presence from model
predict.presence <- predict(object = bc.model, 
                                   x = bioclim.data, 
                                   ext = geographic.extent)

png(file="output/preditction.png")
# Plot base map
plot(wrld_simpl, 
     xlim = c(min.lon, max.lon),
     ylim = c(min.lat, max.lat),
     axes = TRUE, 
     col = "grey95")


# Add model probabilities
plot(predict.presence, add = TRUE)

# Redraw those country borders
plot(wrld_simpl, add = TRUE, border = "grey5")

# Add original observations
points(obs.data$longitude, obs.data$latitude, col = "olivedrab", pch = 20, cex = 0.75)
dev.off()
