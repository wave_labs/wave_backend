library("sp")
library("raster")
library("maptools")
library("rgeos")
library("rgdal")
library("dismo")
library(ncdf4)

setwd("~/projects/wave/storage/enm")


if(environmentalDataSource == 2){
  files<-list.files(path= paste("data/envs", aReferenceId, sep="/"), pattern='*', full.names = TRUE)
  bioworldclim <- stack(files)
} else if(environmentalDataSource == 1){
  bioclim.data <- getData(name = "worldclim",
                          var = "bio",
                          res = aResolution,
                          path = "data/")
  bioworldclim <- stack(bioclim.data[[c(variables)]]) 
} else if(environmentalDataSource == 3){
  files<-list.files(path= paste("..", environmentalDataPath, sep="/"), pattern='*', full.names = TRUE)
  nc<-nc_open(files[1])
  vars<-names(nc[['var']])
  vector<-c()
  for (var in vars) {
    vector <- c(vector,(stack(files[1],varname=var)))
  }
  s<-stack(vector)
  bioworldclim <- stack(s)
}
