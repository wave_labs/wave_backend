library("jsonlite")
setwd("~/projects/wave/storage/enm")
args = commandArgs(trailingOnly=TRUE)

source("helper.r")

args<- argsToVars(args, list("referenceId"))

obs.data <- readOcurrenceData(args['referenceId'])
noDups <- unique(obs.data)
df <- jsonlite::toJSON(noDups)
cat(df)
writeOcurrenceData(noDups, args['referenceId'])