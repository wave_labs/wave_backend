library("sp")
library("raster")
library("maptools")
library("rgdal")
library("dismo")

data(wrld_simpl)
setwd("~/projects/wave/storage/enm")
args = commandArgs(trailingOnly=TRUE)

variables <- args[3]
variables <- as.integer(unlist(strsplit(variables, ",")))

bioclim.data <- getData(name = "worldclim",
                        var = "bio",
                        res = args[4],
                        path = "data/")

bioworldclim <- stack(bioclim.data[[c(variables)]]) 

obs.data <- read.csv(file = paste("data", args[1], sep="/"))

# Determine geographic extent of our data
max.lat <- ceiling(max(obs.data$latitude))
min.lat <- floor(min(obs.data$latitude))
max.lon <- ceiling(max(obs.data$longitude))
min.lon <- floor(min(obs.data$longitude))
geographic.extent <- extent(x = c(min.lon, max.lon, min.lat, max.lat))

# Crop environmental variables to geographic extent of saguaro
bioworldclim <- crop(x = bioworldclim, y = geographic.extent)
pred_nf <- dropLayer(bioworldclim, 'biome')

# Create pseudo-absence, or background, points
# Use the bioclim data files for sampling resolution
bil.files <- list.files(path = "data/wc2-5", 
                        pattern = "*.bil$", 
                        full.names = TRUE)

mask <- raster(bil.files[1])

# Randomly sample points (same number as our observed points)
backg <- randomPoints(mask = mask,     # Provides resolution of sampling points
                           n = nrow(obs.data),      # Number of random points
                           ext = geographic.extent, # Spatially restricts sampling
                           extf = 1.25)             # Expands sampling a little bit

# Create vector of group memberships
group.presence <- kfold(x = obs.data, k = 5)

testing.group <- 1
# Separate observations into training and testing groups
pres_train <- obs.data[group.presence != testing.group, ]
pres_test <- obs.data[group.presence == testing.group, ]

# Repeat the process for pseudo-absence points
group.background <- kfold(x = backg, k = 5)
backg_tain <- backg[group.background != testing.group, ]
backg_test <- backg[group.background == testing.group, ]

# Build species distribution model
dm <- domain(pred_nf, pres_train)

# Use testing data for model evaluation
e <- evaluate(p = pres_test,   # The presence testing data
              a = backg_test, # The absence testing data
              model = dm,    # The model we are evaluating
              x = pred_nf)    # Climatic variables for use by model

# Predict presence from model
pd = predict(x = pred_nf, object = dm, ext = geographic.extent)
par(mfrow=c(1,2))

#----------------------------RASTER OBJECT
png(filename= paste("output", paste(paste(paste(args[2], "domain", sep="-"), "rasterobject", sep="-"), "png", sep="."), sep="/"), width=720, height=720)


plot(wrld_simpl, xlim = c(min.lon, max.lon), ylim = c(min.lat, max.lat), axes = TRUE, col = "grey95")
plot(pd, add = TRUE)
plot(wrld_simpl, add = TRUE, border = "grey5")

dev.off()

#---------------------------- Presence/Absence
tr <- threshold(e, 'spec_sens')
png(filename= paste("output", paste(paste(paste(args[2], "domain", sep="-"), "presence-absence", sep="-"), "png", sep="."), sep="/"), width=720, height=720)

plot(wrld_simpl, xlim = c(min.lon, max.lon), ylim = c(min.lat, max.lat), axes = TRUE, col = "grey95")
plot(pd > tr, main='presence/absence')
plot(wrld_simpl, add = TRUE, border = "grey5")
points(obs.data, col = "red", pch = 20, cex = 0.75)

dev.off()
#----------------------------

png(filename= paste("output", paste(paste(paste(args[2], "domain", sep="-"), "responseplot", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
response(dm)
dev.off()

png(filename= paste("output", paste(paste(paste(args[2], "domain", sep="-"), "tpr", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
plot(e, 'TPR')
dev.off()

png(filename= paste("output", paste(paste(paste(args[2], "domain", sep="-"), "roc", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
plot(e, 'ROC')
dev.off()
#png(filename= paste("output", paste(paste(args[2], "boxplot", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
#boxplot(bc.eval)
#dev.off()
#png(filename= paste("output", paste(paste(args[2], "density", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
# density(bc.eval)
# dev.off()

