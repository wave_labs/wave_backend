library("sp")
library("raster")
library("maptools")
library("rgdal")
library("dismo")

setwd("~/projects/wave/storage/enm")
args = commandArgs(trailingOnly=TRUE)

variables <- args[3]
variables <- as.integer(unlist(strsplit(variables, ",")))

bioclim.data <- getData(name = "worldclim",
                        var = "bio",
                        res = args[4],
                        path = "data/")
bioworldclim <- stack(bioclim.data[[c(variables)]]) 

# Read in occurrences
obs.data <- read.csv(file = paste("data", args[1], sep="/"))

# Determine geographic extent of our data
max.lat <- ceiling(max(obs.data$latitude))
min.lat <- floor(min(obs.data$latitude))
max.lon <- ceiling(max(obs.data$longitude))
min.lon <- floor(min(obs.data$longitude))
geographic.extent <- extent(x = c(min.lon, max.lon, min.lat, max.lat))

# Crop environmental variables to geographic extent of saguaro
bioworldclim <- crop(x = bioworldclim, y = geographic.extent)

# Create pseudo-absence, or background, points
# Use the bioclim data files for sampling resolution
bil.files <- list.files(path = "data/wc2-5", 
                        pattern = "*.bil$", 
                        full.names = TRUE)

mask <- raster(bil.files[1])

# Randomly sample points (same number as our observed points)
background <- randomPoints(mask = mask,     # Provides resolution of sampling points
                           n = nrow(obs.data),      # Number of random points
                           ext = geographic.extent, # Spatially restricts sampling
                           extf = 1.25)             # Expands sampling a little bit

# Create vector of group memberships
group.presence <- kfold(x = obs.data, k = 5)

testing.group <- 1
# Separate observations into training and testing groups
presence.train <- obs.data[group.presence != testing.group, ]
presence.test <- obs.data[group.presence == testing.group, ]

# Repeat the process for pseudo-absence points
group.background <- kfold(x = background, k = 5)
background.train <- background[group.background != testing.group, ]
background.test <- background[group.background == testing.group, ]

# Build species distribution model
bc.model <- bioclim(x = bioworldclim, p = presence.train)

# Predict presence from model
predict.presence <- dismo::predict(object = bc.model, x = bioworldclim, ext = geographic.extent)

# Use testing data for model evaluation
bc.eval <- evaluate(p = presence.test,   # The presence testing data
                    a = background.test, # The absence testing data
                    model = bc.model,    # The model we are evaluating
                    x = bioworldclim)    # Climatic variables for use by model

# Determine minimum threshold for "presence"
bc.threshold <- threshold(x = bc.eval, stat = "spec_sens")

#----------------------------Presence/Absence

png(filename= paste("output", paste(paste(paste(args[2], "bioclim", sep="-"), "presence-absence", sep="-"), "png", sep="."), sep="/"), width=720, height=720)

data(wrld_simpl)
plot(wrld_simpl, xlim = c(min.lon, max.lon), ylim = c(min.lat, max.lat), axes = TRUE, col = "grey95")
plot(predict.presence > bc.threshold, add = TRUE, legend = FALSE)
plot(wrld_simpl, add = TRUE, border = "grey5")
points(obs.data, col = "red", pch = 20, cex = 0.75)

dev.off()

#---------------------------- RASTER OBJECT

png(filename= paste("output", paste(paste(paste(args[2], "bioclim", sep="-"), "rasterobject", sep="-"), "png", sep="."), sep="/"), width=720, height=720)

data(wrld_simpl)
plot(wrld_simpl, xlim = c(min.lon, max.lon), ylim = c(min.lat, max.lat), axes = TRUE, col = "grey95")
plot(predict.presence, add = TRUE)
plot(wrld_simpl, add = TRUE, border = "grey5")

dev.off()
#----------------------------

png(filename= paste("output", paste(paste(paste(args[2], "bioclim", sep="-"), "responseplot", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
response(bc.model)
dev.off()

png(filename= paste("output", paste(paste(paste(args[2], "bioclim", sep="-"), "tpr", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
plot(bc.eval, 'TPR')
dev.off()

png(filename= paste("output", paste(paste(paste(args[2], "bioclim", sep="-"), "roc", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
plot(bc.eval, 'ROC')
dev.off()

#png(filename= paste("output", paste(paste(args[2], "boxplot", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
#boxplot(bc.eval)
#dev.off()
#png(filename= paste("output", paste(paste(args[2], "density", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
# density(bc.eval)
# dev.off()

