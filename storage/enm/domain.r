setwd("~/projects/wave/storage/enm")
args = commandArgs(trailingOnly=TRUE)

aReferenceId <- args[1]
aBioclimVars <- args[2]
aResolution <- args[3]
environmentalDataSource <- args[4]
aKfold <- args[5]
environmentalDataPath <- args[6]

source("runModel.r")


dm <- domain(pred_nf, presence.train)

# Use testing data for model evaluation
e <- evaluate(p = presence.test,   # The presence testing data
              a = background.test, # The absence testing data
              model = dm,    # The model we are evaluating
              x = pred_nf)    # Climatic variables for use by model

# Predict presence from model
pd = predict(x = pred_nf, object = dm, ext = geographic.extent)

#----------------------------RASTER OBJECT
png(filename= paste("output", paste(paste(paste(aReferenceId, "domain", sep="-"), "rasterobject", sep="-"), "png", sep="."), sep="/"), width=720, height=720)


plot(wrld_simpl, xlim = c(min.lon, max.lon), ylim = c(min.lat, max.lat), axes = TRUE, col = "grey95")
plot(pd, add = TRUE)
plot(wrld_simpl, add = TRUE, border = "grey5")

dev.off()

#---------------------------- Presence/Absence
tr <- threshold(e, 'spec_sens')
png(filename= paste("output", paste(paste(paste(aReferenceId, "domain", sep="-"), "presence-absence", sep="-"), "png", sep="."), sep="/"), width=720, height=720)

plot(wrld_simpl, xlim = c(min.lon, max.lon), ylim = c(min.lat, max.lat), axes = TRUE, col = "grey95")
plot(pd > tr, add = TRUE, legend = FALSE)
plot(wrld_simpl, add = TRUE, border = "grey5")
points(obs.data, col = "red", pch = 20, cex = 0.75)

dev.off()
#----------------------------

png(filename= paste("output", paste(paste(paste(aReferenceId, "domain", sep="-"), "responseplot", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
response(dm)
dev.off()

png(filename= paste("output", paste(paste(paste(aReferenceId, "domain", sep="-"), "tpr", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
plot(e, 'TPR')
dev.off()

png(filename= paste("output", paste(paste(paste(aReferenceId, "domain", sep="-"), "roc", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
plot(e, 'ROC')
dev.off()
#png(filename= paste("output", paste(paste(aReferenceId, "boxplot", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
#boxplot(bc.eval)
#dev.off()
#png(filename= paste("output", paste(paste(aReferenceId, "density", sep="-"), "png", sep="."), sep="/"), width=720, height=720)
# density(bc.eval)
# dev.off()

