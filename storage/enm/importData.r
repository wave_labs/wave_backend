library("sp")
library("raster")
library("jsonlite")
library("CoordinateCleaner")

args = commandArgs(trailingOnly=TRUE)
setwd("~/projects/wave/storage/enm")

source("helper.r")

args<- argsToVars(args, list("referenceId", "separator", "latCol", "lonCol"))

separator <- switch(  
  args["separator"],  
  "comma"= ",",  
  "tab"= "\t",  
  "semicolon"= ";",  
  "space"= " ",
)  

obs.data <- read.csv(file = paste("data", paste(args["referenceId"], "csv", sep="."), sep="/"), nrows = 2000, sep=separator)
obs.data <- obs.data[, c(args["latCol"], args["lonCol"])]
colnames(obs.data) <- c("latitude", "longitude")

# Notice NAs
obs.data <- obs.data[!is.na(obs.data$latitude), ]
obs.data <- obs.data[, c("longitude", "latitude")]

for(i in 1:nrow(obs.data))
{
  obs.data$longitude[i] <- as.numeric(sub(",", ".", obs.data$longitude[i], fixed = TRUE))
  obs.data$latitude[i] <- as.numeric(sub(",", ".", obs.data$latitude[i], fixed = TRUE))
}

#validate coordinates
obs.clean.data <- cc_val(obs.data ,
                         lon = "longitude", 
                         lat = "latitude",
                         value = "clean",
                         verbose = TRUE)

df <- jsonlite::toJSON(obs.data)
cat(df)

writeOcurrenceData(obs.data, args["referenceId"])
