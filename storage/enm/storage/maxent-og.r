library("sp")
library("raster")
library("maptools")
library("rgdal")
library("dismo")
library("rJava")

data(wrld_simpl)
#predictors <- stack(list.files(file.path(system.file(package="dismo"), 'ex'), pattern='grd$', full.names=TRUE ))

bioclim.data <- getData(name = "worldclim",
                        var = "bio",
                        res = 2.5,
                        path = "data/")
bioworldclim <- stack(bioclim.data) 

obs.data <- read.csv(file = "data/Carnegiea-gigantea-GBIF.csv")
obs.data <- obs.data[, c("longitude", "latitude")]
obs.data <- obs.data[!is.na(obs.data$latitude), ]

# Determine geographic extent of our data
max.lat <- ceiling(max(obs.data$latitude))
min.lat <- floor(min(obs.data$latitude))
max.lon <- ceiling(max(obs.data$longitude))
min.lon <- floor(min(obs.data$longitude))
geographic.extent <- extent(x = c(min.lon, max.lon, min.lat, max.lat))

bioclim.data <- crop(x = bioclim.data, y = geographic.extent)
pred_nf <- dropLayer(bioclim.data, 'biome')

# Create pseudo-absence, or background, points
# Use the bioclim data files for sampling resolution
bil.files <- list.files(path = "data/wc2-5", 
                        pattern = "*.bil$", 
                        full.names = TRUE)

mask <- raster(bil.files[1])

# Randomly sample points (same number as our observed points)
backg <- randomPoints(mask = mask,     # Provides resolution of sampling points
                      n = nrow(obs.data),      # Number of random points
                      ext = geographic.extent, # Spatially restricts sampling
                      extf = 1.25)             # Expands sampling a little bit

# Create vector of group memberships
group.presence <- kfold(x = obs.data, k = 5)

testing.group <- 1
# Separate observations into training and testing groups
pres_train <- obs.data[group.presence != testing.group, ]
pres_test <- obs.data[group.presence == testing.group, ]

# Repeat the process for pseudo-absence points
group.background <- kfold(x = backg, k = 5)
backg_tain <- backg[group.background != testing.group, ]
backg_test <- backg[group.background == testing.group, ]
maxent()
xm <- maxent(pred_nf, pres_train)

px = predict(pred_nf, xm, ext = geographic.extent)
par(mfrow=c(1,2))

#png(filename= "output/res", width=720, height=720)
plot(wrld_simpl, xlim = c(min.lon, max.lon), ylim = c(min.lat, max.lat), add=TRUE, col='dark grey')
plot(px,  main='main')
plot(wrld_simpl, add = TRUE, border = "grey5")
#dev.off()

tr <- threshold(e, 'spec_sens')
#plot(pd > tr, main='presence/absence')
#plot(wrld_simpl, add = TRUE, border = "grey5")
#points(pres_train, col = "red", pch = 20, cex = 0.75)

