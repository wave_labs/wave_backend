library('spocc')

setwd("~/projects/wave/storage/enm")
args = commandArgs(trailingOnly=TRUE)

source("helper.r")
args<- argsToVars(args, list("referenceId", "source", "specie"))

obs.occ <- occ(query = args['specie'], from = args['source'], gbifopts = list(hasCoordinate=TRUE))

obs.data <- occ2df(obs.occ) 

obs.data <- obs.data[, c("latitude", "longitude")]
colnames(obs.data) <- c("latitude", "longitude")
obs.data <- obs.data[!is.na(obs.data$latitude), ]
obs.data <- obs.data[, c("longitude", "latitude")]

obs.data <- validateCoordinates(obs.data)

df <- jsonlite::toJSON(obs.data)
cat(df)

writeOcurrenceData(obs.data, args['referenceId'])