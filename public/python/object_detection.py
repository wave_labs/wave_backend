######## Image Object Detection Using Tensorflow-trained Classifier #########
#
# This program uses a TensorFlow Lite object detection model to perform object 
# detection on an image. It draws boxes and scores 
# around the objects of interest in each image.
#
#############################################################################

import os
import argparse
import cv2
import numpy as np
import json
import sys
import glob
import importlib.util
from PIL import ImageFont, ImageDraw, Image

detectionJSON = []

class ObjectDetection(): 
  
    def __init__(self, modeldir='models/mobilenet_ssd', threshold=0.5, useEdgeTPU=False): 
        self.MODEL_NAME = modeldir              # directory containing the model files
        self.min_conf_threshold = threshold     # only display detections above this threshold
        self.use_TPU = useEdgeTPU               # perform inference using edge tpu
        self.LABELMAP_NAME = 'labelmap.txt'     # name of the labelmap file inside the model directory
        
        if self.use_TPU:
            self.GRAPH_NAME = 'edgetpu.tflite'
        else:
            self.GRAPH_NAME = 'detect.tflite'

        self.boundingbox_bgr = (80,83,239)           # opencv doesnt use RGB it uses BGR

        self.importTensorFlow(self.use_TPU)       # imports tensorflow or tflite (depending on which is available on the device
        self.getProgramPath()                   # get python program directory
        self.labels = self.loadLabels()         # read labelmap file and store it in labels list
        self.interpreter = self.loadModel()     # loads the model
    

    # Import TensorFlow libraries
    def importTensorFlow(self, use_TPU): 
        # Tries to import tflite_runtime, if not found then tries regular tensorflow
        # If using Coral Edge TPU, import the load_delegate library
        global Interpreter
        try:
            from tflite_runtime.interpreter import Interpreter
            if use_TPU:
                from tflite_runtime.interpreter import load_delegate
        except ImportError:
            from tensorflow.lite.python.interpreter import Interpreter
            if use_TPU:
                from tensorflow.lite.python.interpreter import load_delegate
            #print('tflite_runtime not installed therefore TensorFlow will be imported.')


    def getProgramPath(self):
        # Get program path (to ensure funcionality even when executing from other directories)
        self.PROGRAM_PATH = os.path.split(__file__)[0] + "/"
        if self.PROGRAM_PATH == "/":
            self.PROGRAM_PATH = ""


    # Load the label map
    def loadLabels(self):
        # Path to label map file
        PATH_TO_LABELS = os.path.join(self.PROGRAM_PATH,self.MODEL_NAME,self.LABELMAP_NAME)

        with open(PATH_TO_LABELS, 'r') as f:
            labels = [line.strip() for line in f.readlines()]

        # Have to do a weird fix for label map if using the COCO "starter model" first label is '???', which has to be removed.
        if labels[0] == '???':
            del(labels[0])
        
        return labels
    

    def loadModel(self):
        # Path to .tflite file, which contains the model that is used for object detection
        PATH_TO_CKPT = os.path.join(self.PROGRAM_PATH,self.MODEL_NAME,self.GRAPH_NAME)
        
        # Load the Tensorflow Lite model.
        # If using Edge TPU, use special load_delegate argument
        if self.use_TPU:
            interpreter = Interpreter(model_path=PATH_TO_CKPT,
                                    experimental_delegates=[load_delegate('libedgetpu.so.1.0')])
        else:
            interpreter = Interpreter(model_path=PATH_TO_CKPT)
        interpreter.allocate_tensors()
        return interpreter

    def draw_label(self, image, text, x, y, color, font_size):
        # Draw label using PIL library, since opencv font selection is limited
        
        # use a truetype font  
        font = ImageFont.truetype(os.path.join(self.PROGRAM_PATH,"assets/DejaVuSans.ttf"), font_size) 
        # font = ImageFont.load_default()

        # Convert the image to RGB (OpenCV uses BGR)  
        cv2_im_rgb = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)  
        # Pass the image to PIL  
        pil_im = Image.fromarray(cv2_im_rgb)  

        draw = ImageDraw.Draw(pil_im)  
        # Draw the text  
        draw.text((x, y), text, font=font,fill=color)  
        
        # Get back the image to OpenCV  
        image = cv2.cvtColor(np.array(pil_im), cv2.COLOR_RGB2BGR)

        return image
    

    def add_to_json(self, accuracy, label, xmin, xmax, ymin, ymax):
        global detectionJSON
        detection = {"accuracy": round(accuracy, 2), "label": label, "xmin": round(xmin, 2), "xmax": round(xmax, 2), "ymin": round(ymin, 2), "ymax": round(ymax, 2)}
        detectionJSON.append(detection)

    def imageInference(self, image):
        global detectionJSON
        detectionJSON = []
        # allocate memory for the interpreter
        self.interpreter.allocate_tensors()

        # Get model details
        input_details = self.interpreter.get_input_details()
        output_details = self.interpreter.get_output_details()
        height = input_details[0]['shape'][1]
        width = input_details[0]['shape'][2]

        floating_model = (input_details[0]['dtype'] == np.float32)

        input_mean = 127.5
        input_std = 127.5

        # Load image and resize to expected shape [1xHxWx3]
        image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        imH, imW, _ = image.shape 
        image_resized = cv2.resize(image_rgb, (width, height))
        input_data = np.expand_dims(image_resized, axis=0)

        # Normalize pixel values if using a floating model (i.e. if model is non-quantized)
        if floating_model:
            input_data = (np.float32(input_data) - input_mean) / input_std

        # Perform the actual detection by running the model with the image as input
        self.interpreter.set_tensor(input_details[0]['index'],input_data)
        self.interpreter.invoke()

        # Retrieve detection results
        boxes = self.interpreter.get_tensor(output_details[0]['index'])[0] # Bounding box coordinates of detected objects
        classes = self.interpreter.get_tensor(output_details[1]['index'])[0] # Class index of detected objects
        scores = self.interpreter.get_tensor(output_details[2]['index'])[0] # Confidence of detected objects
        #num = interpreter.get_tensor(output_details[3]['index'])[0]  # Total number of detected objects (inaccurate and not needed)
        
        # Loop over all detections and draw detection box if confidence is above minimum threshold
        for i in range(len(scores)):
            if ((scores[i] > self.min_conf_threshold)):
                
                # Get bounding box coordinates and draw box
                # Interpreter can return coordinates that are outside of image dimensions, need to force them to be within image using max() and min()
                ymin_real = max(1, (boxes[i][0] * imH))
                xmin_real = max(1, (boxes[i][1] * imW))
                ymax_real = min(imH, (boxes[i][2] * imH))
                xmax_real = min(imW, (boxes[i][3] * imW))

                # Convert bb to ints
                ymin = int(ymin_real)
                xmin = int(xmin_real)
                ymax = int(ymax_real)
                xmax = int(xmax_real)
                
                cv2.rectangle(image, (xmin,ymin), (xmax,ymax), self.boundingbox_bgr , 2)

                # Draw label rectangle
                object_name = self.labels[int(classes[i])] # Look up object name from "labels" array using class index
                object_score = scores[i]*100
                label = '%s: %d%%' % (object_name, int(object_score)) # Example: 'person: 72%'
                labelSize, baseLine = cv2.getTextSize(label, cv2.FONT_HERSHEY_TRIPLEX, 0.7, 2) # Get font size
                label_ymin = max(ymin, labelSize[1] + 10) # Make sure not to draw label too close to top of window
                cv2.rectangle(image, (xmin-1, label_ymin-labelSize[1]+15), (xmin+labelSize[0]-1, label_ymin+baseLine+15), self.boundingbox_bgr, cv2.FILLED) # Draw white box to put label text in
                image = self.draw_label(image, label, xmin+3, label_ymin-5, (0,0,0,255), 25)

                # Add current detection to a detectionJson var
                self.add_to_json(object_score, object_name, xmin_real, xmax_real, ymin_real, ymax_real)

        return image, detectionJSON

    # Save image into AI folder
    def saveImage(self, image_path, image): 
        cv2.imwrite(image_path, image)
