# Example: python python/image_inference.py --modeldir=python/models/OceanusNet_100k/ --threshold=0.6 --image=images/ai/turtle.jpeg 

import object_detection as OD
import argparse
import cv2
import json

# Define and parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--modeldir', help='Folder the .tflite file is located in', required=True)
parser.add_argument('--threshold', help='Minimum confidence threshold for displaying detected objects', default=0.5)
parser.add_argument('--image', help='Name of the single image to perform detection on.', required=True)


args = parser.parse_args()
MODEL_NAME = args.modeldir
MODEL_NAME = MODEL_NAME.split('/',1)[1]
IM_NAME = args.image
THRESHOLD = float(args.threshold)

if __name__ == "__main__":
    objectDetector = OD.ObjectDetection(MODEL_NAME, THRESHOLD, False)
    processedImage, detectionJSON = objectDetector.imageInference(cv2.imread(IM_NAME))
    objectDetector.saveImage(IM_NAME, processedImage)
    json.dumps(print(detectionJSON))
