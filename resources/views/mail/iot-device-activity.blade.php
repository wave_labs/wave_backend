<div style="font-size: 1em;">
    Device <span style="font-weight: bold;">{{ $deviceName }}</span> with id <span
        style="font-weight: bold;">{{ $deviceId }}</span> is {{ $activity }}.
</div>

<style>
    .text {
        font-size: 1em;
    }

    .footer {
        font-weight: bold;
        margin-top: 3px;
    }
</style>