<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/site.webmanifest">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="msapplication-config" content="/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">


    <title>Wave Labs | Interaquatic Dashboard</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <link href='https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css' rel='stylesheet' />
    <link href="https://fonts.googleapis.com/css?family=B612&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Anton&display=swap" rel="stylesheet">
</head>

<style>
    @font-face {
        font-family: Aquatico Regular;
        src: url('{!! asset('fonts/Aquatico-Regular.otf') !!}');
    }

    .full-page-loader {
        display: flex;
        align-items: center;
        flex-direction: column;
        justify-content: center;
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
    }

    .c-class {
        width: 200px;
        position: absolute;
        opacity: 0;
        will-change: auto;
    }

    .c-class:nth-child(n) {
        animation: 1.10s infinite wave;
        animation-delay: var(--n);
    }

    @keyframes wave {
        0% {
            opacity: 0;
        }

        90% {
            opacity: 0;
        }

        91% {
            opacity: 1;
        }

        100% {
            opacity: 1;
        }
    }
</style>

<body>

    <div id="index" class="row">

        <div class="script-loader">
            <div class="full-page-loader">
                <img src="/storage/images/loading-animation/cm0.png" style="--n: 1.0s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/cm1.png" style="--n: .9s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/cm2.png" style="--n: .8s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/cm3.png" style="--n: .7s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/cm4.png" style="--n: .6s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/cm5.png" style="--n: .5s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/cm4.png" style="--n: .4s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/cm3.png" style="--n: .3s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/cm2.png" style="--n: .2s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/cm1.png" style="--n: .1s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/cm0.png" style="--n: 0s" alt="loading" class="c-class">

                <img src="/storage/images/loading-animation/m0.png" style="--n: 1.10s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/m1.png" style="--n: 1.00s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/m2.png" style="--n: .90s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/m3.png" style="--n: .80s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/m4.png" style="--n: .70s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/m5.png" style="--n: .60s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/m4.png" style="--n: .50s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/m3.png" style="--n: .40s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/m2.png" style="--n: .30s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/m1.png" style="--n: .20s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/m0.png" style="--n: .10s" alt="loading" class="c-class">


                <img src="/storage/images/loading-animation/mw0.png" style="--n: 1.2s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/mw1.png" style="--n: 1.1s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/mw2.png" style="--n: 1.0s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/mw3.png" style="--n: .9s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/mw4.png" style="--n: .8s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/mw5.png" style="--n: .7s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/mw4.png" style="--n: .6s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/mw3.png" style="--n: .5s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/mw2.png" style="--n: .4s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/mw1.png" style="--n: .3s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/mw0.png" style="--n: .2s" alt="loading" class="c-class">


                <img src="/storage/images/loading-animation/w0.png" style="--n: 1.3s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/w1.png" style="--n: 1.2s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/w2.png" style="--n: 1.1s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/w3.png" style="--n: 1.0s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/w4.png" style="--n: .9s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/w5.png" style="--n: .8s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/w4.png" style="--n: .7s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/w3.png" style="--n: .6s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/w2.png" style="--n: .5s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/w1.png" style="--n: .4s" alt="loading" class="c-class">
                <img src="/storage/images/loading-animation/w0.png" style="--n: .3s" alt="loading" class="c-class">
            </div>
            <script src="{{mix('js/index.js')}}"></script>

        </div>
    </div>
</body>

</html>