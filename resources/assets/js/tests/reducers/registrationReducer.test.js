import reducer from '../../reducers/registrationReducer'
import { authConstants } from '../../constants/authConstants'
​
describe('registration reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual([
      {
        registering: false,
        registered: false
      }
    ])
  })
​
  it('should handle REGISTER_REQUEST', () => {
    expect(
      reducer([], {
        type: authConstants.REGISTER_REQUEST,
      })
    ).toEqual([
      {
        registering: true,
        registered: false
      }
    ])
  })

  it('should handle REGISTER_SUCCESS', () => {
    expect(
      reducer([], {
        type: authConstants.REGISTER_REQUEST,
      })
    ).toEqual([
      {
        registering: false,
        registered: true
      } 
    ])
  })

})