import { combineReducers } from "redux";
import auth from "redux-modules/auth";
import alert from "redux-modules/alert";
import user from "redux-modules/user";
import userOccupation from "redux-modules/userOccupation";
import beaufortScale from "redux-modules/beaufortScale";
import capsule from "redux-modules/capsule";
import editCreateModal from "redux-modules/editCreateModal";
import sighting from "redux-modules/sighting";
import sightingData from "redux-modules/sightingData";
import creature from "redux-modules/creature";
import userDiveCenter from "redux-modules/userDiveCenter";
import creatureType from "redux-modules/creatureType";
import divingSpot from "redux-modules/divingSpot";
import source from "redux-modules/source";
import suggestion from "redux-modules/suggestion";
import suggestionType from "redux-modules/suggestionType";
import forms from "redux-modules/form";
import question from "redux-modules/question";
import vehicle from "redux-modules/vehicle";
import vehicleType from "redux-modules/vehicleType";
import sightingBehaviour from "redux-modules/sightingBehaviour";
import result from "redux-modules/result";
import feedback from "redux-modules/feedback";
import dive from "redux-modules/dive";
import survey from "redux-modules/survey";
import litterCategory from "redux-modules/litterCategory";
import litterSubCategory from "redux-modules/litterSubCategory";
import coordinate from "redux-modules/coordinate";
import litter from "redux-modules/litter";
import userPerson from "redux-modules/userPerson";
import me from "redux-modules/account";

import trackerHistory from "redux-modules/trackerHistory";
import iotDeviceType from "redux-modules/iotDeviceType";
import iotDevice from "redux-modules/iotDevice";
import iotEnvironmentType from "redux-modules/iotEnvironmentType";
import iotDeviceState from "redux-modules/iotDeviceState";
import iotReport from "redux-modules/iotReport";
import iotFeature from "redux-modules/iotFeature";
import iotTemplate from "redux-modules/iot/iotTemplate";
import iotRule from "redux-modules/iotRule";
import enm from "redux-modules/enm";

import freshwaterPin from "redux-modules/freshwaterPin";
import trout from "redux-modules/trout";
import climarest from "redux-modules/climarest";
import diadema from "redux-modules/diadema";
import copernicusProduct from "redux-modules/copernicusProduct";

import marker from "redux-modules/marker";
import activity from "redux-modules/activity";

import stopwatch from "redux-modules/stopwatch";
import stopwatchCategory from "redux-modules/stopwatch/category";
import boundingBox from "redux-modules/boundingBox";

import divingSpotSubstract from "redux-modules/divingSpotSubstract";

import analytic from "redux-modules/analytic";

import aiCategory from "redux-modules/ai/category";
import aiDetectionImage from "redux-modules/ai/detectionImage";
import aiDetection from "redux-modules/trackerCatalogue";

import domeInteraction from "redux-modules/dome/domeInteraction";
import gps from "redux-modules/gps";

import insectReport from "redux-modules/insectReport";
/* import insectFamily from 'redux-modules/insectFamily'; */
import insectReference from "redux-modules/insectReference";
import insectSpecies from "redux-modules/insectSpecies";
import insectSinonimia from "redux-modules/insectSinonimia";
import insectOccurrence from "redux-modules/insectOccurrence";
import insectProject from "redux-modules/insectProject";
import softCoral from "redux-modules/softCoral";

const rootReducer = combineReducers({
    copernicusProduct,
    enm,
    alert,
    auth,
    user,
    userOccupation,
    beaufortScale,
    capsule,
    editCreateModal,
    sighting,
    creature,
    source,
    suggestion,
    suggestionType,
    creatureType,
    divingSpot,
    dive,
    survey,
    userDiveCenter,
    sightingData,
    forms,
    litterCategory,
    litterSubCategory,
    userPerson,
    litter,
    question,
    feedback,
    result,
    coordinate,
    me,
    aiDetection,
    trackerHistory,
    vehicle,
    vehicleType,
    sightingBehaviour,
    iotFeature,
    iotTemplate,
    analytic,
    iotDeviceType,
    iotDevice,
    iotEnvironmentType,
    iotDeviceState,
    iotReport,
    iotRule,
    marker,
    activity,
    stopwatch,
    boundingBox,
    aiCategory,
    aiDetectionImage,
    freshwaterPin,
    stopwatchCategory,
    domeInteraction,
    diadema,
    gps,
    insectReport,
    /* insectFamily, */
    insectReference,
    trout,
    climarest,
    divingSpotSubstract,
    insectSpecies,
    insectSinonimia,
    insectOccurrence,
    insectProject,
    softCoral,
});

export default rootReducer;
