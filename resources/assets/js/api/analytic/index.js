import axios from "axios";
import { stringify } from "query-string";

const fetchAnalyticSighting = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/statistics/sighting?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchAnalyticSightingCreature = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/statistics/sighting/creature?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchAnalyticSightingSpecificCreature = (id, filters = {}) =>
    axios.get(
        `${window.location.origin}/api/statistics/sighting/creature/${id}?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchAnalyticLitter = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/statistics/litter?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchAnalyticLitterCategory = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/statistics/litter/category?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );



const api = {
    fetchAnalyticSighting,
    fetchAnalyticSightingCreature,
    fetchAnalyticSightingSpecificCreature,

    fetchAnalyticLitter,
    fetchAnalyticLitterCategory
};

export default api;