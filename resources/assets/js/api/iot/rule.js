import axios from "axios";
import { stringify } from "query-string";

const url = `${window.location.origin}/api/iot-rule`

const fetchIotRules = (page = 1, filters = {}) =>
    axios.get(`${url}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`);

const fetchIotRule = (id) =>
    axios.get(`${url}/${id}`);

const fetchSelector = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/iot-rule/?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const api = {
    fetchIotRules,
    fetchSelector,
    fetchIotRule
};

export default api;
