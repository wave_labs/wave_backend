import axios from "axios";
import { stringify } from "query-string";

const url = `${window.location.origin}/api/iot-feature`

const fetchIotFeatures = (page = 1, filters = {}) =>
    axios.get(`${url}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`);

const fetchIotFeature = (id) =>
    axios.get(`${url}/${id}`);

const fetchSelector = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/iot-feature/?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const api = {
    fetchIotFeatures,
    fetchSelector,
    fetchIotFeature
};

export default api;
