import axios from "axios";
import { stringify } from "query-string";

const url = `${window.location.origin}/api/iot-template`

const fetchIotTemplates = (page = 1, filters = {}) =>
    axios.get(`${url}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`);

const fetchSelector = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/iot-template/?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const api = {
    fetchIotTemplates,
    fetchSelector,
};

export default api;
