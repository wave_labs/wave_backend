import axios from "axios";
import { stringify } from "query-string";

const fetchLitterCategories = (page = 1, filters = {}) =>
    axios.get(
        `${window.location.origin}/api/litter-category?${stringify(filters, {
            arrayFormat: "index"
        })}&page=${page}`
    );


const fetchSelector = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/litter-category?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const deleteLitterCategory = id =>
    axios.delete(`${window.location.origin}/api/litter-category/${id}`);

const createLitterCategory = data =>
    axios.post(`${window.location.origin}/api/litter-category`, data);

const updateLitterCategory = (id, data) =>
    axios.put(`${window.location.origin}/api/litter-category/${id}`, data);

const api = {
    fetchLitterCategories,
    fetchSelector,
    deleteLitterCategory,
    createLitterCategory,
    updateLitterCategory
};

export default api;