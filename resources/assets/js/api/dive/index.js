import axios from "axios";
import { stringify } from "query-string";

const fetchDives = (page = 1, filters = {}) =>
  axios.get(
    `${window.location.origin}/api/dive?${stringify(filters, {
      arrayFormat: "index",
    })}&page=${page}`
  );

const fetchDiveCoords = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/coordinates/dive?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const deleteDive = (id) =>
  axios.delete(`${window.location.origin}/api/dive/${id}`);

const createDive = (data) =>
  axios.post(`${window.location.origin}/api/dive`, data);

const updateDive = (id, data) =>
  axios.put(`${window.location.origin}/api/dive/${id}`, data);

const fetchMonthlyDives = () =>
  axios.get(`${window.location.origin}/api/dive-monthly`);

const fetchMostReportedSpecies = () =>
  axios.get(`${window.location.origin}/api/dive-most-reported`);

const fetchSpeciesReports = () =>
  axios.get(`${window.location.origin}/api/dive-species-reports`);

const api = {
  fetchDives,
  deleteDive,
  createDive,
  updateDive,
  fetchDiveCoords,
  fetchMonthlyDives,
  fetchMostReportedSpecies,
  fetchSpeciesReports
};

export default api;
