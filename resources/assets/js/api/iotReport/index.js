import axios from "axios";
import { stringify } from "query-string";

const fetchIotReports = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/iot-report?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const fetchShowIotReport = id =>
	axios.get(`${window.location.origin}/api/iot-report/${id}`);

const latestIotReport = filters =>
	axios.get(
		`${window.location.origin}/api/iot-report/latestReport?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);

const fetchFieldsTimeline = filters =>
	axios.get(
		`${window.location.origin}/api/iot-report/fieldsTimeline?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);

const generateCustomGraph = filters =>
	axios.get(
		`${window.location.origin}/api/iot-report/generateCustomGraph?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);


const fetchIotReportsCoord = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/coordinates/iot-report?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);

const fetchIotReportsCoordForLineDraw = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/coordinates-for-line-draw/iot-report?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);

const fetchIotReportNextAndPrior = id =>
	axios.get(`${window.location.origin}/api/iot-report/next-prior/${id}`);

const deleteIotReport = id =>
	axios.delete(`${window.location.origin}/api/iot-report/${id}`);

const createIotReport = data =>
	axios.post(`${window.location.origin}/api/iot-report`, data);

const updateIotReport = (id, data) =>
	axios.put(`${window.location.origin}/api/iot-report/${id}`, data);

const api = {
	fetchIotReports,
	fetchIotReportNextAndPrior,
	fetchShowIotReport,
	fetchIotReportsCoord,
	fetchIotReportsCoordForLineDraw,
	deleteIotReport,
	createIotReport,
	updateIotReport,
	latestIotReport,
	fetchFieldsTimeline,
	generateCustomGraph
};

export default api;
