import axios from "axios";
import { stringify } from "query-string";

const fetchCopernicusProducts = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/copernicus-product?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);


const api = {
	fetchCopernicusProducts,
};

export default api;
