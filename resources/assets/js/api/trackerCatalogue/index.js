import axios from "axios";
import { stringify } from "query-string";

const fetchAiDetections = (page, filters = {}) =>
	axios.get(`${window.location.origin}/api/ai-detection?${stringify(filters, {
		arrayFormat: "index"
	})}&page=${page}`
	);

const createAiDetection = data =>
	axios.post(`${window.location.origin}/api/ai-detection`, data);

const deleteAiDetection = id =>
	axios.delete(`${window.location.origin}/api/ai-detection/${id}`);

const api = {
	fetchAiDetections,
	createAiDetection,
	deleteAiDetection
};

export default api
