import axios from "axios";
import { stringify } from "query-string";
const apiUrl = window.location.origin + "/api/soft-coral";

const fetchSoftCorals = (page = 1, filters = {}) =>
    axios.get(
        `${apiUrl}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
    );
const fetchSoftCoral = (id) => axios.get(`${apiUrl}/${id}`);
const deleteSoftCoral = (id) => axios.delete(`${apiUrl}/${id}`);
const createSoftCoral = (data) => axios.post(`${apiUrl}`, data);
const updateSoftCoral = (id, data) => axios.post(`${apiUrl}/${id}`, data);

const fetchSoftCoralCoordinates = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/soft-coral?${stringify(
            filters,
            {
                arrayFormat: "index",
            }
        )}`
    );

const api = {
    fetchSoftCorals,
    fetchSoftCoralCoordinates,
    fetchSoftCoral,
    deleteSoftCoral,
    createSoftCoral,
    updateSoftCoral,
};

export default api;
