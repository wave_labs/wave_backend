import axios from "axios";
import { stringify } from "query-string";

const fetchBoundingBoxImages = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/studies/bounding-box/images/?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchBoundingBoxClasses = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/studies/bounding-box/class/?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const createBoundingBoxResult = data =>
    axios.post(`${window.location.origin}/api/studies/bounding-box/result`, data);


const fetchBoundingBoxResults = (page = 1, filters = {}) =>
    axios.get(
        `${window.location.origin}/api/studies/bounding-box/result/?${stringify(filters, {
            arrayFormat: "index"
        })}&page=${page}`
    );

const deleteBoundingBoxResult = (id) =>
    axios.delete(
        `${window.location.origin}/api/studies/bounding-box/result/${id}`
    );

const api = {
    fetchBoundingBoxImages,
    fetchBoundingBoxClasses,
    createBoundingBoxResult,
    fetchBoundingBoxResults,
    deleteBoundingBoxResult
};

export default api;
