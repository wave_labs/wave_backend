import axios from "axios";
import { stringify } from "query-string";

const fetchUserDiveCenters = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/user-dive-center?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const deleteUserDiveCenter = id =>
	axios.delete(`${window.location.origin}/api/user-dive-center/${id}`);

const createUserDiveCenter = data =>
	axios.post(`${window.location.origin}/api/register`, data);

const updateUserDiveCenter = (id, data) =>
	axios.put(`${window.location.origin}/api/user-dive-center/${id}`, data);

const api = {
	fetchUserDiveCenters,
	deleteUserDiveCenter,
	createUserDiveCenter,
	updateUserDiveCenter
};

export default api;