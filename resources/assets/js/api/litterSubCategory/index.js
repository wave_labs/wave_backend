import axios from "axios";
import { stringify } from "query-string";

const fetchLitterSubCategories = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/litter-sub-category?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);


const fetchSelector = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/selector/litter-sub-category?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);

const deleteLitterSubCategory = id =>
	axios.delete(`${window.location.origin}/api/litter-sub-category/${id}`);

const createLitterSubCategory = data =>
	axios.post(`${window.location.origin}/api/litter-sub-category`, data);

const updateLitterSubCategory = (id, data) =>
	axios.put(`${window.location.origin}/api/litter-sub-category/${id}`, data);

const api = {
	fetchLitterSubCategories,
	fetchSelector,
	deleteLitterSubCategory,
	createLitterSubCategory,
	updateLitterSubCategory
};

export default api;