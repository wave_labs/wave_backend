import axios from "axios";
import { stringify } from "query-string";
const apiUrl = window.location.origin + '/api/trout';

const fetchTrouts = (page = 1, filters = {}) =>
    axios.get(
        `${apiUrl}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
    );
const fetchTrout = id => axios.get(`${apiUrl}/${id}`);
const deleteTrout = id => axios.delete(`${apiUrl}/${id}`);
const createTrout = data => axios.post(`${apiUrl}`, data);
const updateTrout = (id, data) => axios.post(`${apiUrl}/${id}`, data);

const fetchTroutCoordinates = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/trout?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );


const api = {
    fetchTrouts,
    fetchTroutCoordinates,
    fetchTrout,
    deleteTrout,
    createTrout,
    updateTrout,
};

export default api;
