import axios from "axios";
import { stringify } from "query-string";

const fetchMe = () => axios.get(`${window.location.origin}/api/me`);

const fetchHistory = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/history?${stringify(
			filters,
			{
				arrayFormat: "index"
			}
		)}`
	);

const deleteMe = id => axios.delete(`${window.location.origin}/api/user/${id}`);

const updateMe = (id, data) =>
	axios.put(`${window.location.origin}/api/user/${id}`, data);

const api = {
	fetchMe,
	fetchHistory,
	deleteMe,
	updateMe
};

export default api;
