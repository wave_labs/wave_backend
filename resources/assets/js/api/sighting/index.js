import axios from "axios";
import { stringify } from "query-string";

const fetchSightings = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/sighting?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const fetchSightingsCoord = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/coordinates/sighting?${stringify(
			filters,
			{
				arrayFormat: "index"
			}
		)}`
	);

const fetchSighting = id =>
	axios.get(`${window.location.origin}/api/sighting/${id}`);

const fetchSightingNextAndPrior = id =>
	axios.get(`${window.location.origin}/api/sighting/next-prior/${id}`);

const deleteSighting = id =>
	axios.delete(`${window.location.origin}/api/sighting/${id}`);

const createSighting = data =>
	axios.post(`${window.location.origin}/api/sighting`, data);

const updateSighting = (id, data) =>
	axios.put(`${window.location.origin}/api/sighting/${id}`, data);

const fetchSightingData = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/sighting-data?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const deleteSightingData = id =>
	axios.delete(`${window.location.origin}/api/sighting-data/${id}`);

const createSightingData = data =>
	axios.post(`${window.location.origin}/api/sighting-data`, data);

const updateSightingData = (id, data) =>
	axios.post(`${window.location.origin}/api/sighting-data/${id}`, data);

const api = {
	fetchSightings,
	deleteSighting,
	createSighting,
	updateSighting,
	fetchSighting,
	fetchSightingNextAndPrior,
	fetchSightingsCoord,
	deleteSightingData,
	createSightingData,
	updateSightingData,
	fetchSightingData
};

export default api;
