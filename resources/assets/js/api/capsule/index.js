import axios from "axios";
import { stringify } from "query-string";

const fetchCapsules = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/capsule?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const deleteCapsule = id =>
	axios.delete(`${window.location.origin}/api/capsule/${id}`);

const createCapsule = data =>
	axios.post(`${window.location.origin}/api/capsule`, data);

const updateCapsule = (id, data) =>
	axios.post(`${window.location.origin}/api/capsule/${id}`, data);

const api = {
	fetchCapsules,
	deleteCapsule,
	createCapsule,
	updateCapsule
};

export default api;
