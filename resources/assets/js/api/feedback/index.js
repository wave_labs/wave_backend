import axios from "axios";
import { stringify } from "query-string";

const fetchFeedback = (form, page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/feedback/${form}?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const deleteFeedback = id =>
	axios.delete(`${window.location.origin}/api/feedback/${id}`);

const createFeedback = data =>
	axios.post(`${window.location.origin}/api/feedback`, data);

const updateFeedback = (id, data) =>
	axios.put(`${window.location.origin}/api/feedbackd/${id}`, data);

const api = {
	fetchFeedback,
	deleteFeedback,
	createFeedback,
	updateFeedback
};

export default api;
