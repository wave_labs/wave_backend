import axios from "axios";
import { stringify } from "query-string";

const fetchSurveys = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/survey?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const deleteSurvey = id =>
	axios.delete(`${window.location.origin}/api/survey/${id}`);

const createSurvey = data =>
	axios.post(`${window.location.origin}/api/survey`, data);

const updateSurvey = (id, data) =>
	axios.put(`${window.location.origin}/api/survey/${id}`, data);

const api = {
	fetchSurveys,
	deleteSurvey,
	createSurvey,
	updateSurvey
};

export default api;