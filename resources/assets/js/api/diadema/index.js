import axios from "axios";
import { stringify } from "query-string";
const apiUrl = window.location.origin + '/api/studies/diadema';

const fetchDiademas = (page = 1, filters = {}) =>
    axios.get(
        `${apiUrl}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
    );
const fetchDiadema = id => axios.get(`${apiUrl}/${id}`);
const deleteDiadema = id => axios.delete(`${apiUrl}/${id}`);
const createDiadema = data => axios.post(`${apiUrl}`, data);
const updateDiadema = (id, data) => axios.post(`${apiUrl}/${id}`, data);

const fetchDiademaCoordinates = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/diadema?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );


const api = {
    fetchDiademas,
    fetchDiadema,
    deleteDiadema,
    createDiadema,
    updateDiadema,
    fetchDiademaCoordinates
};

export default api;
