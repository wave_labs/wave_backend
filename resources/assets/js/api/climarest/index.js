import axios from "axios";
import { stringify } from "query-string";
const apiUrl = window.location.origin + "/api/climarests";

const fetchClimarests = (page = 1, filters = {}) =>
  axios.get(
    `${apiUrl}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
  );
const fetchClimarest = (id) => axios.get(`${apiUrl}/${id}`);
const deleteClimarest = (id) => axios.delete(`${apiUrl}/${id}`);
const createClimarest = (data) => axios.post(`${apiUrl}`, data);
const updateClimarest = (id, data) => axios.post(`${apiUrl}/${id}`, data);

const fetchClimarestAreas = (page = 1, filters = {}) =>
  axios.get(
    `${apiUrl}/areas?${stringify(filters, {
      arrayFormat: "index",
    })}&page=${page}`
  );

const fetchClimarestAreaStakeholders = (id) =>
  axios.get(
    `${apiUrl}/areaStakeholders/${id}`
  );

const api = {
  fetchClimarests,
  fetchClimarest,
  deleteClimarest,
  createClimarest,
  updateClimarest,
  fetchClimarestAreas,
  fetchClimarestAreaStakeholders
};

export default api;
