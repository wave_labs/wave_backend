import axios from "axios";
import { stringify } from "query-string";

const specieHistory = () => 
    axios.get(`${window.location.origin}/api/statistics/ai/specie`);

const timeHistory = () =>
    axios.get(`${window.location.origin}/api/statistics/ai/history`);

const api = {
    specieHistory,
    timeHistory,
};

export default api