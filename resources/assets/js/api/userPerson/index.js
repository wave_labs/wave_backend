import axios from "axios";
import { stringify } from "query-string";

const fetchUserPerson = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/user-person?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const deleteUserPerson = id =>
	axios.delete(`${window.location.origin}/api/user-person/${id}`);

const createUserPerson = data =>
	axios.post(`${window.location.origin}/api/register`, data);

const updateUserPerson = (id, data) =>
	axios.put(`${window.location.origin}/api/user-person/${id}`, data);

const api = {
	fetchUserPerson,
	deleteUserPerson,
	createUserPerson,
	updateUserPerson
};

export default api;