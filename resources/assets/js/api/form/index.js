import axios from "axios";
import { stringify } from "query-string";

const fetchForm = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/form?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const deleteForm = id =>
	axios.delete(`${window.location.origin}/api/form/${id}`);

const createForm = data =>
	axios.post(`${window.location.origin}/api/form`, data);

const updateForm = (id, data) =>
	axios.put(`${window.location.origin}/api/form/${id}`, data);

const api = {
	fetchForm,
	deleteForm,
	createForm,
	updateForm
};

export default api;
