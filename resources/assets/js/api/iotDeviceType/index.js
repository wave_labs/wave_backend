import axios from "axios";
import { stringify } from "query-string";

const fetchIotDeviceTypes = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/iot-device-type?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const fetchSelector = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/selector/iot-device-type/?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);

const deleteIotDeviceType = id =>
	axios.delete(`${window.location.origin}/api/iot-device-type/${id}`);

const createIotDeviceType = data =>
	axios.post(`${window.location.origin}/api/iot-device-type`, data);

const updateIotDeviceType = (id, data) =>
	axios.put(`${window.location.origin}/api/iot-device-type/${id}`, data);

const api = {
	fetchIotDeviceTypes,
	fetchSelector,
	deleteIotDeviceType,
	createIotDeviceType,
	updateIotDeviceType
};

export default api;
