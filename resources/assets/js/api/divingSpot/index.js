import axios from "axios";
import { stringify } from "query-string";

const fetchDivingSpots = (page = 1, filters = {}) =>
    axios.get(
        `${window.location.origin}/api/diving-spot?${stringify(filters, {
            arrayFormat: "index",
        })}&page=${page}`
    );

const fetchSelector = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/diving-spot?${stringify(filters, {
            arrayFormat: "index",
        })}`
    );

const deleteDivingSpot = (id) =>
    axios.delete(`${window.location.origin}/api/diving-spot/${id}`);

const createDivingSpot = (data) =>
    axios.post(`${window.location.origin}/api/diving-spot`, data);

const addToUser = (data) =>
    axios.post(`${window.location.origin}/api/diving-spot-user/${data}`);

const removeFromUser = (data) =>
    axios.post(`${window.location.origin}/api/remove-diving-spot-user`, data);

const updateDivingSpot = (id, data) =>
    axios.put(`${window.location.origin}/api/diving-spot/${id}`, data);

const api = {
    fetchDivingSpots,
    fetchSelector,
    deleteDivingSpot,
    createDivingSpot,
    updateDivingSpot,
    addToUser,
    removeFromUser
};

export default api;
