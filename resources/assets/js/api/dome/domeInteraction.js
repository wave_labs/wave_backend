import axios from "axios";
import { stringify } from "query-string";

const getDomeInteraction = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/dome-interaction?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const incrementDomeInteraction = (filters) =>
    axios.put(
        `${window.location.origin}/api/dome-interaction/increment?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const api = {
    getDomeInteraction,
    incrementDomeInteraction,
};

export default api;
