import axios from "axios";
import { stringify } from "query-string";
const apiUrl = window.location.origin + '/api/litter';

const fetchLitter = (page = 1, filters = {}) =>
	axios.get(
		`${apiUrl}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
	);

const fetchShowLitter = id =>
	axios.get(`${window.location.origin}/api/litter/${id}`);

const fetchLittersCoord = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/coordinates/litter?${stringify(
			filters,
			{
				arrayFormat: "index"
			}
		)}`
	);

const fetchLitterNextAndPrior = id =>
	axios.get(`${window.location.origin}/api/litter/next-prior/${id}`);

const deleteLitter = id =>
	axios.delete(`${window.location.origin}/api/litter/${id}`);

const createLitter = data =>
	axios.post(`${window.location.origin}/api/litter`, data);

const updateLitter = (id, data) =>
	axios.post(`${window.location.origin}/api/litter/${id}`, data);

const incrementCounter = data =>
	axios.post(`${window.location.origin}/api/record`, data);

const api = {
	fetchLitter,
	fetchLitterNextAndPrior,
	fetchShowLitter,
	fetchLittersCoord,
	deleteLitter,
	createLitter,
	updateLitter,
	incrementCounter
};

export default api;
