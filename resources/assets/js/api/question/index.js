import axios from "axios";
import { stringify } from "query-string";

const fetchQuestion = (form, page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/form/${form}?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const deleteQuestion = id =>
	axios.delete(`${window.location.origin}/api/question/${id}`);

const createQuestion = data =>
	axios.post(`${window.location.origin}/api/question`, data);

const updateQuestion = (id, data) =>
	axios.put(`${window.location.origin}/api/question/${id}`, data);

const api = {
	fetchQuestion,
	deleteQuestion,
	createQuestion,
	updateQuestion
};

export default api;
