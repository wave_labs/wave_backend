import axios from "axios";
import { stringify } from "query-string";

const fetchStopwatchVideos = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/studies/stopwatch/videos/?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchNextStopwatchVideo = (video, filters) =>
    axios.get(
        `${window.location.origin}/api/studies/stopwatch/next-video/${video}?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchStopwatchClasses = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/studies/stopwatch/class/?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const createStopwatchResult = data =>
    axios.post(`${window.location.origin}/api/studies/stopwatch/result`, data);


const fetchStopwatchResults = (page, filters = {}) =>
    axios.get(
        `${window.location.origin}/api/studies/stopwatch/result/?${stringify(filters, {
            arrayFormat: "index"
        })}&page=${page}`
    );

const fetchStopwatchResult = (id) =>
    axios.get(
        `${window.location.origin}/api/studies/stopwatch/result/${id}`
    );


const deleteStopwatchResult = (id) =>
    axios.delete(
        `${window.location.origin}/api/studies/stopwatch/result/${id}`
    );


const api = {
    fetchStopwatchVideos,
    fetchNextStopwatchVideo,
    fetchStopwatchClasses,
    createStopwatchResult,
    fetchStopwatchResults,
    deleteStopwatchResult,
    fetchStopwatchResult
};

export default api;
