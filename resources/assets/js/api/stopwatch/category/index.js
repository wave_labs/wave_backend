import axios from "axios";
import { stringify } from "query-string";

const fetchStopwatchCategories = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/studies/stopwatch/categories/?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchStopwatchCategory = (category) =>
    axios.get(
        `${window.location.origin}/api/studies/stopwatch/categories/${category}`
    );


const api = {
    fetchStopwatchCategories,
    fetchStopwatchCategory,
};

export default api;
