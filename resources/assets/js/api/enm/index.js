import axios from "axios";
import { stringify } from "query-string";

const createOcurrenceData = (data, filters) =>
	axios.post(`${window.location.origin}/api/enm/uploadOcurrence?${stringify(filters, {
		arrayFormat: "index"
	})}`, data);

const getOcurrenceDataFromPipeline = (filters) =>
	axios.get(`${window.location.origin}/api/enm/getOcurrenceDataFromPipeline?${stringify(filters, {
		arrayFormat: "index"
	})}`);



const uploadEnvironmentalData = (data, id) =>
	axios.post(`${window.location.origin}/api/enm/uploadEnvironmentalData/${id}`, data);

const getCopernicusData = (id, filters) =>
	axios.get(`${window.location.origin}/api/enm/getCopernicusData/${id}?${stringify(filters, {
		arrayFormat: "index"
	})}`);

const queryOcurrenceData = (filters) =>
	axios.get(`${window.location.origin}/api/enm/queryOcurrence?${stringify(filters, {
		arrayFormat: "index"
	})}`);

const fetchEnms = (page, filters) =>
	axios.get(`${window.location.origin}/api/enm/?${stringify(filters, {
		arrayFormat: "index"
	})}&page=${page}`);

const deleteEnm = id =>
	axios.delete(`${window.location.origin}/api/enm/${id}`);


const setStep = (id, data) =>
	axios.put(`${window.location.origin}/api/enm/setStep/${id}`, data);

const deleteOcurrenceDataDuplicates = id =>
	axios.put(`${window.location.origin}/api/enm/deleteDuplicates/${id}`);

const cropOcurrenceDataGeographicExtent = (id, data) =>
	axios.put(`${window.location.origin}/api/enm/cropGeographicExtent/${id}`, data);

const deleteOcurrenceDataRecord = (id, record) =>
	axios.put(`${window.location.origin}/api/enm/deleteRecord/${id}/${record}`);

const limitOcurrenceData = (id, limit) =>
	axios.put(`${window.location.origin}/api/enm/limitOcurrences/${id}`, limit);

const getEnvData = (id) =>
	axios.get(`${window.location.origin}/api/enm/getEnvData/${id}`);

const createPrediction = (id, data) =>
	axios.post(`${window.location.origin}/api/enm/createPrediction/${id}`, data);

const api = {
	createOcurrenceData,
	deleteOcurrenceDataDuplicates,
	getOcurrenceDataFromPipeline,
	queryOcurrenceData,
	limitOcurrenceData,
	cropOcurrenceDataGeographicExtent,
	getEnvData,
	createPrediction,
	deleteOcurrenceDataRecord,
	fetchEnms,
	deleteEnm,
	setStep,
	uploadEnvironmentalData,
	getCopernicusData
};

export default api;
