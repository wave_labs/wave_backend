import axios from "axios";
import { stringify } from "query-string";
import { history } from "routes";

const url = `${window.location.origin}/api/insect-projects`;

const createInsectProject = (data) => axios.post(`${url}`, data);

const fetchInsectProjects = (filters = {}) =>
  axios.get(
    `${url}?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectProject = (id) => axios.get(`${url}/${id}`);

const addMemberToProject = (projectId, data) =>
  axios.post(`${url}/add-member/${projectId}`, data);

const removeMemberFromProject = (userId, projectId) =>
  axios.post(`${url}/remove-member/${userId}/${projectId}`);

const updateInsectProject = (id, data) => axios.post(`${url}/${id}`, data);

const api = {
  createInsectProject,
  fetchInsectProjects,
  fetchInsectProject,
  addMemberToProject,
  removeMemberFromProject,
  updateInsectProject,
};

export default api;
