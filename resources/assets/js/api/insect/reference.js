import axios from "axios";
import { stringify } from "query-string";

const url = `${window.location.origin}/api/insect-references`;

const fetchInsectReferences = (page = 1, filters = {}) =>
  axios.get(
    `${url}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
  );

const fetchInsectReferencesSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/selector/insect-references?${stringify(
      filters,
      { arrayFormat: "index" }
    )}`
  );

const fetchInsectReference = (id) => axios.get(`${url}/${id}`);

const deleteInsectReference = (id) => axios.delete(`${url}/${id}`);

const createInsectReference = (data) => axios.post(`${url}`, data);

const updateInsectReference = (id, data) => axios.put(`${url}/${id}`, data);

const fetchReferenceAuthorsSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/reference-authors?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const api = {
  fetchInsectReferences,
  fetchInsectReference,
  deleteInsectReference,
  createInsectReference,
  updateInsectReference,
  fetchInsectReferencesSelector,
  fetchReferenceAuthorsSelector,
};

export default api;
