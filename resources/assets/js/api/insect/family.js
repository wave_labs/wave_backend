import axios from "axios";
import { stringify } from "query-string";

const url = `${window.location.origin}/api/insect-family`

const fetchInsectFamilies = (page = 1, filters = {}) =>
    axios.get(`${url}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`);

const fetchInsectFamilySelector = (filters = {}) =>
    axios.get(`${window.location.origin}/api/selector/insect-family?${stringify(filters, { arrayFormat: "index" })}`);

const fetchInsectFamily = (id) =>
    axios.get(`${url}/${id}`);

const deleteInsectFamily = id =>
    axios.delete(`${url}/${id}`);

const createInsectFamily = data =>
    axios.post(`${url}`, data);

const updateInsectFamily = (id, data) =>
    axios.put(`${url}/${id}`, data);


const api = {
    fetchInsectFamilies,
    fetchInsectFamilySelector,
    fetchInsectFamily,
    deleteInsectFamily,
    createInsectFamily,
    updateInsectFamily
};

export default api;
