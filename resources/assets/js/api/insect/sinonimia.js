import axios from "axios";
import { stringify } from "query-string";

const url = `${window.location.origin}/api/insect-sinonimia`;

const fetchInsectSinonimias = (page = 1, filters = {}) =>
  axios.get(
    `${url}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
  );

const fetchInsectSinonimia = (id) => axios.get(`${url}/${id}`);

const createInsectSinonimia = (data) => axios.post(`${url}`, data);

const updateInsectSinonimia = (id, data) => axios.put(`${url}/${id}`, data);

const deleteInsectSinonimia = id => axios.delete(`${url}/${id}`);

const api = {
  fetchInsectSinonimias,
  fetchInsectSinonimia,
  updateInsectSinonimia,
  createInsectSinonimia,
  deleteInsectSinonimia
};

export default api;
