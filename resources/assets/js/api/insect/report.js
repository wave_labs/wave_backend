import axios from "axios";
import { stringify } from "query-string";

const url = `${window.location.origin}/api/insect-reports`

const fetchInsectReports = (page = 1, filters = {}) =>
    axios.get(`${url}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`);

const fetchInsectReportsSelector = (filters = {}) =>
    axios.get(`${window.location.origin}/api/selector/insect-report?${stringify(filters, { arrayFormat: "index" })}`);

const fetchInsectReport = (id) =>
    axios.get(`${url}/${id}`);

const deleteInsectReport = id =>
    axios.delete(`${url}/${id}`);

const createInsectReport = data =>
    axios.post(`${url}`, data);

const updateInsectReport = (id, data) =>
    axios.put(`${url}/${id}`, data);


const api = {
    fetchInsectReports,
    fetchInsectReport,
    deleteInsectReport,
    createInsectReport,
    updateInsectReport,
    fetchInsectReportsSelector
};

export default api;
