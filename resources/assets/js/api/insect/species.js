import axios from "axios";
import { stringify } from "query-string";

const url = `${window.location.origin}/api/insect-species`;

const fetchInsectSpecies = (page = 1, filters = {}) =>
  axios.get(
    `${url}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
  );

const fetchInsectSpecie = (id) => axios.get(`${url}/${id}`);

const createInsectSpecie = (data) => axios.post(`${url}`, data);

const updateInsectSpecie = (id, data) => axios.put(`${url}/${id}`, data);

const deleteInsectSpecie = (data) =>
  axios.post(`${window.location.origin}/api/delete-insect-taxon`, data);

const updateTaxon = (taxonRank, id, data) =>
  axios.post(`${url}/${taxonRank}/${id}`, data);

const fetchInsectKingdomSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-kingdoms?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectPhylumSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-phylums?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectClassSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-classes?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectOrderSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-orders?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectSubOrderSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-sub-orders?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectInfraOrderSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-infra-orders?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectSuperFamilySelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-super-families?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectFamilySelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-families?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectSubFamilySelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-sub-families?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectTribeSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-tribes?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectGenusSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-genuses?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectSubGenusSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-sub-genuses?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectSpecificSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-specifics?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const fetchInsectInfraSpecificSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/insect-infra-specifics?${stringify(filters, {
      arrayFormat: "index",
    })}`
  );

const insectImportTaxon = (data) =>
  axios.post(`${window.location.origin}/api/import/csv/insect-taxon`, data);

const api = {
  fetchInsectSpecies,
  fetchInsectSpecie,
  updateInsectSpecie,
  updateTaxon,
  createInsectSpecie,
  deleteInsectSpecie,
  fetchInsectKingdomSelector,
  fetchInsectPhylumSelector,
  fetchInsectClassSelector,
  fetchInsectOrderSelector,
  fetchInsectSubOrderSelector,
  fetchInsectInfraOrderSelector,
  fetchInsectSuperFamilySelector,
  fetchInsectFamilySelector,
  fetchInsectSubFamilySelector,
  fetchInsectTribeSelector,
  fetchInsectGenusSelector,
  fetchInsectSubGenusSelector,
  fetchInsectSpecificSelector,
  fetchInsectInfraSpecificSelector,
  insectImportTaxon,
};

export default api;
