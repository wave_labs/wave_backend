import axios from "axios";
import { stringify } from "query-string";

const url = `${window.location.origin}/api/insect-occurrences`;

const fetchInsectSpeciesSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/selector/insect-species?${stringify(
      filters,
      {
        arrayFormat: "index",
      }
    )}`
  );

const fetchInsectSubspeciesSelector = (filters = {}) =>
  axios.get(
    `${window.location.origin}/api/selector/insect-subspecies?${stringify(
      filters,
      {
        arrayFormat: "index",
      }
    )}`
  );

const createInsectOccurrence = (data) => axios.post(`${url}`, data);

const fetchInsectOccurrences = (page = 1, filters = {}) =>
  axios.get(
    `${url}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
  );

const updateInsectOccurrence = (id, data) => axios.put(`${url}/${id}`, data);

const insectImportOccurrences = (data) =>
  axios.post(
    `${window.location.origin}/api/import/csv/insect-occurrences`,
    data
  );

const api = {
  fetchInsectSpeciesSelector,
  fetchInsectSubspeciesSelector,
  createInsectOccurrence,
  fetchInsectOccurrences,
  updateInsectOccurrence,
  insectImportOccurrences,
};

export default api;
