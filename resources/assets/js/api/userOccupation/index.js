import axios from "axios";
import { stringify } from "query-string";

const fetchSelector = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/user-occupation/?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const createUserOccupation = data =>
    axios.post(`${window.location.origin}/api/user-occupation`, data);

const api = {
    fetchSelector,
    createUserOccupation
};

export default api;
