import axios from "axios";

const fetchSelector = () =>
  axios.get(`${window.location.origin}/api/selector/diving-spot-substract`);

const api = {
  fetchSelector,
};

export default api;
