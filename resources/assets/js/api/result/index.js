import axios from "axios";
import { stringify } from "query-string";

const fetchResult = (id, page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/result/${id}?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const deleteResult = id =>
	axios.delete(`${window.location.origin}/api/result/${id}`);

const createResult = data =>
	axios.post(`${window.location.origin}/api/result`, data);

const updateResult = (id, data) =>
	axios.put(`${window.location.origin}/api/result/${id}`, data);

const api = {
	fetchResult,
	deleteResult,
	createResult,
	updateResult
};

export default api;
