import axios from "axios";
import { stringify } from "query-string";

const fetchAiCategories = (page = 1, filters = {}) =>
    axios.get(
        `${window.location.origin}/api/ai-category/?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchAiCategory = id =>
    axios.get(`${window.location.origin}/api/ai-category/${id}`);

const api = {
    fetchAiCategories,
    fetchAiCategory
};

export default api;
