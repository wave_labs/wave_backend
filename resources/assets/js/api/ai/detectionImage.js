import axios from "axios";
import { stringify } from "query-string";

const fetchAiDetectionImages = (page, filters = {}) =>
    axios.get(`${window.location.origin}/api/ai-detection-image?${stringify(filters, {
        arrayFormat: "index"
    })}&page=${page}`
    );

const api = {
    fetchAiDetectionImages,
};

export default api
