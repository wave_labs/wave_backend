import axios from "axios";
import { stringify } from "query-string";
const apiUrl = window.location.origin + '/api/markers';

const fetchMarkers = (page = 1, filters = {}) =>
    axios.get(
        `${apiUrl}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
    );
const fetchMarker = id => axios.get(`${apiUrl}/${id}`);
const deleteMarker = id => axios.delete(`${apiUrl}/${id}`);
const createMarker = data => axios.post(`${apiUrl}`, data);
const updateMarker = (id, data) => axios.post(`${apiUrl}/${id}`, data);

const fetchMarkerCoordinates = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/coordinates/markers?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );


const api = {
    fetchMarkers,
    fetchMarker,
    deleteMarker,
    createMarker,
    updateMarker,
    fetchMarkerCoordinates,
};

export default api;
