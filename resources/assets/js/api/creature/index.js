import axios from "axios";
import { stringify } from "query-string";

const fetchCreatures = (page = 1, filters = {}) =>
    axios.get(
        `${window.location.origin}/api/creature?${stringify(filters, {
            arrayFormat: "index"
        })}&page=${page}`
    );

const fetchSelector = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/creature?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchCreatureSize = id =>
    axios.get(`${window.location.origin}/api/creature/size/${id}`);

const fetchCreatureActivities = (id, filters = {}) =>
    axios.get(
        `${window.location.origin}/api/creature/activities/${id}?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const deleteCreature = id =>
    axios.delete(`${window.location.origin}/api/creature/${id}`);

const createCreature = data =>
    axios.post(`${window.location.origin}/api/creature`, data);

const updateCreature = (id, data) =>
    axios.put(`${window.location.origin}/api/creature/${id}`, data);

const api = {
    fetchCreatures,
    fetchSelector,
    deleteCreature,
    createCreature,
    updateCreature,
    fetchCreatureSize,
    fetchCreatureActivities
};

export default api;
