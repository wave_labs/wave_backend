import axios from "axios";
import { stringify } from "query-string";

const fetchGpsGatewaySatellites = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/gpsgatewaysatellite?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const fetchGpsTagSatellites = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/gpstagsatellite?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );

const api = {
    fetchGpsGatewaySatellites,
    fetchGpsTagSatellites,
};

export default api;
