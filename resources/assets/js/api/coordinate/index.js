import axios from "axios";
import { stringify } from "query-string";

const fetchCenter = () =>
	axios.get(
		`${window.location.origin}/api/coordinates/center`
	);

const fetchIndex = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/coordinates/index?${stringify(
			filters,
			{
				arrayFormat: "index"
			}
		)}`
	);


const api = {
	fetchCenter,
	fetchIndex,

};

export default api;
