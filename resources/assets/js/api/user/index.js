import axios from "axios";
import { stringify } from "query-string";

const fetchUsers = (page = 1, filters = {}) =>
    axios.get(
        `${window.location.origin}/api/user?${stringify(filters, {
            arrayFormat: "index",
        })}&page=${page}`
    );

const fetchSelector = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/user?${stringify(filters, {
            arrayFormat: "index",
        })}`
    );

const deleteUser = (id) =>
    axios.delete(`${window.location.origin}/api/user/${id}`);

const createUser = (data) =>
    axios.post(`${window.location.origin}/api/user`, data);

const updateUser = (id, data) =>
    axios.put(`${window.location.origin}/api/user/${id}`, data);

const activateUser = (id) =>
    axios.post(`${window.location.origin}/api/activate/${id}`);

const sendRecoverPasswordEmail = (id) =>
    axios.post(
        `${window.location.origin}/api/user/send-password-reset-email/${id}`
    );

const api = {
    fetchUsers,
    fetchSelector,
    deleteUser,
    createUser,
    updateUser,
    activateUser,
    sendRecoverPasswordEmail,
};

export default api;
