import axios from "axios";
import { stringify } from "query-string";

const fetchIotDevices = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/iot-device?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);

const fetchIotDevice = id =>
	axios.get(`${window.location.origin}/api/iot-device/${id}`);

const fetchIotGateway = id =>
	axios.get(`${window.location.origin}/api/iot-device/${id}`);

const fetchSelector = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/selector/iot-device/?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);

const fetchIotDeviceNextAndPrior = id =>
	axios.get(`${window.location.origin}/api/iot-device/next-prior/${id}`);

const deleteIotDevice = id =>
	axios.delete(`${window.location.origin}/api/iot-device/${id}`);

const createIotDevice = data =>
	axios.post(`${window.location.origin}/api/iot-device`, data);

const updateIotDevice = (id, data) =>
	axios.put(`${window.location.origin}/api/iot-device/${id}`, data);

const api = {
	fetchIotDevices,
	fetchSelector,
	fetchIotDeviceNextAndPrior,
	fetchIotDevice,
	fetchIotGateway,
	deleteIotDevice,
	createIotDevice,
	updateIotDevice
};

export default api;
