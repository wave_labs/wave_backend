import axios from "axios";
import { stringify } from "query-string";
const apiUrl = window.location.origin + '/api/freshwater-pin';

const fetchFreshwaterPins = (page = 1, filters = {}) =>
    axios.get(
        `${apiUrl}?${stringify(filters, { arrayFormat: "index" })}&page=${page}`
    );
const fetchFreshwaterPin = id => axios.get(`${apiUrl}/${id}`);
const deleteFreshwaterPin = id => axios.delete(`${apiUrl}/${id}`);
const createFreshwaterPin = data => axios.post(`${apiUrl}`, data);
const updateFreshwaterPin = (id, data) => axios.post(`${apiUrl}/${id}`, data);

const fetchFreshwaterPinCoordinates = (filters = {}) =>
    axios.get(
        `${window.location.origin}/api/selector/freshwater-pin?${stringify(filters, {
            arrayFormat: "index"
        })}`
    );


const api = {
    fetchFreshwaterPins,
    fetchFreshwaterPinCoordinates,
    fetchFreshwaterPin,
    deleteFreshwaterPin,
    createFreshwaterPin,
    updateFreshwaterPin,
};

export default api;
