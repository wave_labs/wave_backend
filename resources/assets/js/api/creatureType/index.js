import axios from "axios";
import { stringify } from "query-string";

const fetchCreatureTypes = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/creature-type?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const fetchSelector = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/selector/creature-type/?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);

const deleteCreatureType = id =>
	axios.delete(`${window.location.origin}/api/creature-type/${id}`);

const createCreatureType = data =>
	axios.post(`${window.location.origin}/api/creature-type`, data);

const updateCreatureType = (id, data) =>
	axios.put(`${window.location.origin}/api/creature-type/${id}`, data);

const api = {
	fetchCreatureTypes,
	fetchSelector,
	deleteCreatureType,
	createCreatureType,
	updateCreatureType
};

export default api;
