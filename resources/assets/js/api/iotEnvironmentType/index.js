import axios from "axios";
import { stringify } from "query-string";

const fetchSelector = (filters = {}) =>
	axios.get(
		`${window.location.origin}/api/selector/iot-environment-type/?${stringify(filters, {
			arrayFormat: "index"
		})}`
	);

const api = {
	fetchSelector,
};

export default api;
