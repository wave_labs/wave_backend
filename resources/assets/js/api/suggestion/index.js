import axios from "axios";
import { stringify } from "query-string";

const fetchSuggestions = (page = 1, filters = {}) =>
	axios.get(
		`${window.location.origin}/api/suggestion?${stringify(filters, {
			arrayFormat: "index"
		})}&page=${page}`
	);

const deleteSuggestion = id =>
	axios.delete(`${window.location.origin}/api/suggestion/${id}`);

const createSuggestion = data =>
	axios.post(`${window.location.origin}/api/suggestion`, data);

const updateSuggestion = (id, data) =>
	axios.put(`${window.location.origin}/api/suggestion/${id}`, data);

const api = {
	fetchSuggestions,
	deleteSuggestion,
	createSuggestion,
	updateSuggestion
};

export default api;
