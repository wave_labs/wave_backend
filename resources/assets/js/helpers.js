import styled from "styled-components";
import React from "react";

export const ICON = `M20.2,15.7L20.2,15.7c1.1-1.6,1.8-3.6,1.8-5.7c0-5.6-4.5-10-10-10S2,4.5,2,10c0,2,0.6,3.9,1.6,5.4c0,0.1,0.1,0.2,0.2,0.3
  c0,0,0.1,0.1,0.1,0.2c0.2,0.3,0.4,0.6,0.7,0.9c2.6,3.1,7.4,7.6,7.4,7.6s4.8-4.5,7.4-7.5c0.2-0.3,0.5-0.6,0.7-0.9
  C20.1,15.8,20.2,15.8,20.2,15.7z`;

export const dimensions = {
    "xs": "575px",
    "sm": "576px",
    "md": "768px",
    "lg": "992px",
    "xl": "1200px",
    "xxl": "1600px",
};

export function download(response, filename) {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", filename);
    document.body.appendChild(link);
    link.click();
};

export function dummyRequest({ file, onSuccess }) {
    setTimeout(() => {
        onSuccess("ok");
    }, 0);
};

export function handleArrayToFormData(formData, array, field) {
    for (var i = 0; i < array.length; i++) {
        formData.append(`${field}[]`, array[i]);
    }

    return formData;
};

export function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
}

export function isOdd(num) {
    return num % 2;
}

export function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail)) return true;
    else return false;
};

export function getErrorMessages(errors) {
    let messages = [];

    Object.values(errors).map(function (message) {
        messages.push(message[0])
    })

    return messages;
};

export function updateDrawerDimensions(window) {
    let width = "30%";
    if (window.innerWidth > 1300) {
        width = "30%";
    } else if (window.innerWidth > 768 && window.innerWidth < 1300) {
        width = "50%";
    } else if (window.innerWidth < 768) {
        width = "80%";
    }

    return width;
};

export const litterRangeConverter = {
    "1-5": "one to five",
    "5-10": "five to ten",
    "5-20": "five to twenty",
    "10-50": "ten to fifty",
    ">20": "Over twenty",
    ">50": "Over fifty",
};

export const menuFromRoute = {
    "/dashboard/apps": "/dashboard/apps",
    "/dashboard/apps/whaleReporter": "/dashboard/apps",
    "/dashboard/apps/diveReporter": "/dashboard/apps",
    "/dashboard/apps/litterReporter": "/dashboard/apps",

    "/dashboard/creatures": "/dashboard/data",
    "/dashboard/creatureTypes": "/dashboard/data",
    "/dashboard/divingSpot": "/dashboard/data",
    "/dashboard/litter/categories": "/dashboard/data",
    "/dashboard/iotUnits": "/dashboard/data",
    "/dashboard/iotDataType": "/dashboard/data",

    "/dashboard/users": "/dashboard/admin",
    "/dashboard/suggestions": "/dashboard/admin",
    "/dashboard/form": "/dashboard/admin",
    "/dashboard/monitor": "/dashboard/admin",
    "/dashboard/workplace": "/dashboard/admin",

    "/dashboard/capsules": "/dashboard/iot",
    "/dashboard/sightingData": "/dashboard/iot",
    "/dashboard/iotDeviceType": "/dashboard/iot",
    "/dashboard/iotReports": "/dashboard/iot",
    "/dashboard/iotDevices": "/dashboard/iot",

    "/dashboard/catalogue": "/dashboard/tracker",
    "/dashboard/history": "/dashboard/tracker",
    "/dashboard/dataset": "/dashboard/tracker",

    "/dashboard/biodiversitynet": "/dashboard/ai",
    "/dashboard/litternet": "/dashboard/ai",

    "/dashboard/studies/markers": "/dashboard/studies",
    "/dashboard/studies/stopwatch": "/dashboard/studies",
};

export const roleConverter = {
    "person": "Individual",
    "dive-center": "Company",
    "guest": "Guest",
    "admin": "Admin",
};

export const genderConverter = {
    m: "Male",
    f: "Female",
};

export const sourceConverter = {
    "WHALE_REPORTER": "Whale Reporter",
    "DIVE_REPORTER": "Dive Reporter",
    "LITTER_REPORTER": "Litter Reporter",
    "WEB": "Website",
};

export const levelConverter = [{
    "1": 1,
    "0 - 1 m^2": 2,
    "1 - 3": 3,
    "1 - 5": 4,
    "1 - 15": 5,
}, {
    "2": 1,
    "1 - 5 m^2": 2,
    "4 - 6": 3,
    "2 - 3": 4,
    "6 - 10": 5,
    "16 - 30": 6,
}, {
    "3": 1,
    "5 - 10 m^2": 2,
    "7 - 10": 3,
    "4 - 5": 4,
    "11 - 15": 5,
    "31 - 50": 6,
}, {
    "> 3": 1,
    "> 10 m^2": 2,
    "> 10": 3,
    "> 5": 4,
    "> 15": 5,
    "> 50": 6,
}];


export function getCarouselBreakpoints(aItems, aBreakpoints = [[0, 600], [600, 1024], [1024, 1400], [1400, 1800], [1800, 100000]], aItemsToSlide = [1, 1, 1, 1, 1]) {
    return {
        desktop: {
            breakpoint: { max: aBreakpoints[4][1], min: aBreakpoints[4][0] },
            items: aItems[4],
            itemsToSlide: aItemsToSlide[4]
        },
        laptop: {
            breakpoint: { max: aBreakpoints[3][1], min: aBreakpoints[3][0] },
            items: aItems[3],
            itemsToSlide: aItemsToSlide[3]
        },
        tablet: {
            breakpoint: { max: aBreakpoints[2][1], min: aBreakpoints[2][0] },
            items: aItems[2],
            itemsToSlide: aItemsToSlide[2]
        },
        smartphone: {
            breakpoint: { max: aBreakpoints[1][1], min: aBreakpoints[1][0] },
            items: aItems[1],
            itemsToSlide: aItemsToSlide[1]
        },
        mobile: {
            breakpoint: { max: aBreakpoints[0][1], min: aBreakpoints[0][0] },
            items: aItems[0],
            itemsToSlide: aItemsToSlide[0]
        },
    };
};

export const formatPosition = sighting => ({
    lat: parseFloat(sighting.latitude),
    lng: parseFloat(sighting.longitude)
});

export const formatCenter = coordinates => ({
    lat: parseFloat(coordinates[0]),
    lng: parseFloat(coordinates[1])
});

export const limitPosition = e => ({
    lat: parseFloat(e.latitude).toFixed(3),
    lng: parseFloat(e.longitude).toFixed(3)
});

export function NoDataMessage(message = "Field Not Provided") {
    return message;
};

export function isFieldNull(data) {
    return data ? data : NoDataMessage();
};


export function profileInfo(info) {
    return info ? info : "-----";
};

export const StyledTitle = styled.h1`
    font-size: 3.5rem;
    text-align: center;
    margin-bottom: 1%;
`;
export const StyledSubTitle = styled.h2`
    font-size: 1.4rem;
    text-align: center;
    margin-bottom: 50px;
    color: #777;
`;

export const TitleSection = ({ title, subtitle }) => (
    <div>
        <StyledTitle>{title}</StyledTitle>
        <StyledSubTitle>{subtitle}</StyledSubTitle>
    </div>
)

export const WhaleReporterUrl = "https://play.google.com/store/apps/details?id=com.tigerwhale.sighting";
export const DiveReporterUrl = "https://play.google.com/store/apps/details?id=com.tigerwhale.dive";
export const LitterReporterUrl = "https://play.google.com/store/apps/details?id=com.tigerwhale.litter";
export const ARDomeUrl = "https://play.google.com/store/apps/details?id=com.tigerwhale.ardome";



export const AppsPublicationsContent = [

    {
        image: "/images/publications/oceangame.png",
        title: "Gaming versus storytelling: understanding children's interactive experiences in a museum setting.",
        authors:
            "Radeta, M., Cesario, V., Matos, S., & Nisi, V.",
        conference: "ICIDS2017",
        file:
            "/download/articles/oceangame.pdf"
    },
    {
        image: "/images/publications/lorattle.png",
        title: "LoRattle: An Exploratory Game With a Purpose using LoRa and IoT.",
        authors:
            "Radeta M., Ribeiro M. Vasconcelos D., & Nunes, N.",
        conference: "ICEC2019",
        file:
            "/download/articles/lorattle.pdf"
    },

];

export const IotsPublicationsContent = [

    {
        image: "/images/publications/poseidon.png",
        title: "POSEIDON: Passive-acoustic Ocean Sensor for Entertainment and Interactive Data-gathering in Opportunistic Nautical-activities.",
        authors:
            "Radeta, M., Nunes, N. J., Vasconcelos, D., & Nisi, V.",
        conference: "DIS2018",
        file:
            "/download/articles/poseidon.pdf"
    },
    {
        image: "/images/publications/seamote.png",
        title: "SeaMote: Interactive Remotely Operated Apparatus for Aquatic Expeditions.",
        authors:
            "Radeta, M., Ribeiro, M., Vasconcelos, D., Lopes, J., Sousa, M., Monteiro, J., & Nunes, N. J.",
        conference: "INTERACT2019",
        file:
            "/download/articles/seamote.pdf"
    },
    {
        image: "/images/publications/loraquatica.png",
        title: "LoRaquatica: Studying Range and Location Estimation using LoRa and IoT in Aquatic Sensing",
        authors:
            "Radeta, M., Ribeiro, M., Vasconcelos, D., Noronha, H., & Nunes, N.",
        conference: "PERCOM2020",
        file:
            "/download/articles/loraquatica.pdf"
    },


];

export const DomesPublicationsContent = [
    {
        image: "/images/publications/interaquatica.png",
        title: "INTERAQUATICA: Designing Interactive Aquatic Experiences with Geodesic Domes In-the-Wild",
        authors:
            "Radeta, M., Andrade, M., Freitas, R., Sousa, L., Ramos, A., Azevedo, V., Lopes, J., Jardim, R., Gouveia, J., Gouveia, M., Gonçalves, T., Pinto, P., Rodrigues, R., França, C., Freitas, J., Andrade, A., Faria, A., Abreu, L., Mendes, M., Nunes, N. & Nisi, V. ",
        conference: "IMX2020",
        file:
            "/download/articles/interaquatica.pdf"
    },

];

export const FAQContent = [
    {
        question: "What is Wave?",
        content: "Wave is an interactive dashboard which directly contributes to this goal by understanding the impact of human presence on marine environment. It provides an insight to recent cetacean sightings, underwater taxa, marine litter, sea vessels, including the acoustics and footage of marine flora and fauna near Madeira island."
    },
    {
        question: "What does Wave do?",
        content: "Wave is built around kits, which include mobile applications that collect data with citizen science, IoT devices collecting information in harsh aquatic environments, artificial intelligence for prediction of marine megafauna and marine litter through deep learning."
    },
    {
        question: "Where is Wave located?",
        content: "Wave is located at Polo Cientifico e Tecnologico da Madeira, floor -2, Caminho da Penteada."
    },
    {
        question: "Are there job vacancies at Wave?",
        content: "We want to empower everyone with knowledge about ocean, and understand the human impact on biodiversity. We believe that by equipping people with the best tools to understand the ocean, we can solve the climate change, together. You may visit our Lab -> Join page to be updated regarding vacancies."
    },
    {
        question: "We are an academic / R&D institution, how can we work with Wave?",
        content: "You may visit us directly or contact us through email. For more information visit out \"Contact\" page"
    },
    {
        question: "I am from the media, who should I talk to?",
        content: "Please check our Lab -> Media Kit page and/or contact Marko Radeta (Coordinator, PI) at marko@wave-labs.org"
    },
    {
        question: "I need IT support, who can I contact for assistance?",
        content: "Fill out the form on the of our Contact page or email us directly at team@wave-labs.org"
    }
];