import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import { injectGlobal } from "styled-components";

import PrivateRoute from "components/Common/PrivateRoute";
import ScrollToTop from "components/Common/ScrollToTop";

//public pages
import MainPageLayout from "components/MainPage/MainPageLayout";
import LoginForm from "components/MainPage/LoginForm";
import RegisterForm from "components/MainPage/RegistrationForm";
import Homepage from "components/MainPage/Homepage";
import Contact from "components/MainPage/Contact";
import FAQ from "components/MainPage/FAQ";

import About from "components/MainPage/Lab/About";
import MediaKit from "components/MainPage/Lab/MediaKit";
import JoinUs from "components/MainPage/Lab/JoinUs";

import VerifyUser from "components/MainPage/VerifyUser";
import ForgotPasswordForm from "components/MainPage/ForgotPasswordForm";
import ResetPasswordForm from "components/MainPage/ResetPasswordForm";

import Intertagua from "components/MainPage/Projects/Intertagua";
import Largescale from "components/MainPage/Projects/Largescale";
import Interwhale from "components/MainPage/Projects/Interwhale";

import Apps from "components/MainPage/Kits/Apps";
import IoT from "components/MainPage/Kits/IoT";
import Domes from "components/MainPage/Kits/Domes";
import Ai from "components/MainPage/Kits/Ai";
import Triton from "components/MainPage/Kits/IoT/Triton";
import Poseidon from "components/MainPage/Kits/IoT/Poseidon";
import SeaMote from "components/MainPage/Kits/IoT/SeaMote";

import Studies from "components/MainPage/Research/Studies";

import Publications from "components/MainPage/Research/Publications";
import WhatsNew from "components/MainPage/Research/WhatsNew";
import Team from "components/MainPage/Research/Team";

import LitterReporterPrivacy from "components/MainPage/Privacy/LitterReporter";
import PrivacyContent from "components/MainPage/Privacy/PrivacyContent";

//dashboard
import DashboardLayout from "components/Dashboard/DashboardLayout";
import Ardome from "./components/MainPage/Kits/Apps/Ardome";
import LitterReporter from "./components/MainPage/Kits/Apps/LitterReporter";
import DiveReporter from "./components/MainPage/Kits/Apps/DiveReporter";
import WhaleReporter from "./components/MainPage/Kits/Apps/WhaleReporter";
import EmailVerification from "./components/MainPage/EmailVerification";
import Datasets from "./components/MainPage/Research/Datasets";
import Models from "./components/MainPage/Research/Models";


export const history = createBrowserHistory();

injectGlobal`
  	body{
		font-weight: 100;
		color: black !important;
  		overflow-y: scroll;
	} 
`;

const Routes = () => {
    return (
        <Router history={history}>
            <ScrollToTop>
                <Switch>
                    <PrivateRoute path="/dashboard" component={DashboardLayout} route="/login" source="main" />
                    <MainPageLayout>
                        <Route exact path="/" component={Homepage} />

                        <Route path="/contacts" component={Contact} />
                        <Route path="/faq" component={FAQ} />
                        <Route path="/login" component={LoginForm} />
                        <Route path="/about" component={About} />
                        <Route path="/mediakit" component={MediaKit} />
                        <Route path="/join" component={JoinUs} />


                        <Route path="/studies" component={Studies} />
                        <Route path="/studies/markers/:creatureId" component={VerifyUser} />
                        <Route path="/news" component={WhatsNew} />
                        <Route path="/publications" component={Publications} />
                        <Route path="/datasets" component={Datasets} />
                        <Route path="/models" component={Models} />
                        {/* <Route path="/datasets" component={Datasets} /> */}
                        <Route path="/publications" component={Publications} />
                        <Route path="/publications" component={Publications} />
                        <Route path="/team" component={Team} />

                        <Route path="/register" component={RegisterForm} />
                        <Route
                            path="/password/forgot"
                            component={ForgotPasswordForm}
                        />
                        <Route
                            path="/email/verify/:email"
                            component={EmailVerification}
                        />
                        <Route
                            path="/password/reset/:token"
                            component={ResetPasswordForm}
                        />
                        {/* <Route path="/user/verify/:token" component={VerifyUser} /> */}
                        <Route path="/intertagua" component={Intertagua} />
                        <Route path="/largescale" component={Largescale} />
                        <Route path="/interwhale" component={Interwhale} />

                        <Route path="/kits/apps/whaleReporter" component={WhaleReporter} />
                        <Route path="/kits/apps/litterReporter" component={LitterReporter} />
                        <Route path="/kits/apps/diveReporter" component={DiveReporter} />
                        <Route path="/kits/apps/ardome" component={Ardome} />
                        <Route exact path="/kits/apps" component={Apps} />
                        <Route exact path="/kits/iot" component={IoT} />
                        <Route path="/kits/domes" component={Domes} />
                        <Route path="/kits/ai" component={Ai} />
                        <Route path="/kits/iot/triton" component={Triton} />
                        <Route path="/kits/iot/poseidon" component={Poseidon} />
                        <Route path="/kits/iot/seamote" component={SeaMote} />

                        <Route path="/privacy/litterReporter" component={LitterReporterPrivacy} />
                        <Route exact path="/privacy" component={PrivacyContent} />
                    </MainPageLayout>
                </Switch>
            </ScrollToTop>
        </Router>
    );
};

export default Routes;
