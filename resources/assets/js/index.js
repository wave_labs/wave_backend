import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import Routes from "routes";
import {
	loginSuccess,
	me,
	setAuthorizationToken,
	refreshAuthorizationToken
} from "redux-modules/auth/actions";
import jwtDecode from "jwt-decode";
import { store } from "store";
import "../css/Style.css"
import "antd/dist/antd.css";
import "react-multi-carousel/lib/styles.css";
import "react-image-lightbox/style.css";


if (localStorage.jwtToken) {
	const token = jwtDecode(localStorage.jwtToken);
	const tokenExp = token.exp < Date.now() / 1000;
	tokenExp && console.log("token expired");

	if (tokenExp) {
		store.dispatch(refreshAuthorizationToken(localStorage.jwtToken));
	} else {
		setAuthorizationToken(localStorage.jwtToken);
		store.dispatch(me());
		store.dispatch(loginSuccess(jwtDecode(localStorage.jwtToken)));
	}
}

ReactDOM.render(
	<Provider store={store}>
		<Routes />
	</Provider>,
	document.querySelector("#index")
);
