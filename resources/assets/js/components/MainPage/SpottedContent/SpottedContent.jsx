import { Tooltip, Modal, Row, Button, Select, Col } from "antd";
import React, { Component } from "react";
import MapBoxMap from "../../Dashboard/Common/MapBoxMap";
import { QuestionCircleFilled } from "@ant-design/icons";
import styled from "styled-components";
import { Marker } from "react-map-gl";
import SpottedGeneralQuestionsForm from "./SpottedGeneralQuestionsForm";
import SpottedActivityForm from "./SpottedActivityForm";
const Option = Select.Option;

const ICON = `M20.2,15.7L20.2,15.7c1.1-1.6,1.8-3.6,1.8-5.7c0-5.6-4.5-10-10-10S2,4.5,2,10c0,2,0.6,3.9,1.6,5.4c0,0.1,0.1,0.2,0.2,0.3
  c0,0,0.1,0.1,0.1,0.2c0.2,0.3,0.4,0.6,0.7,0.9c2.6,3.1,7.4,7.6,7.4,7.6s4.8-4.5,7.4-7.5c0.2-0.3,0.5-0.6,0.7-0.9
  C20.1,15.8,20.2,15.8,20.2,15.7z`;

const InfoContainer = styled.div`
  margin: 50px auto;
  margin-bottom: 30px;
  h1,
  p {
    text-align: center;
  }

  p {
    font-size: 1.2em;
    width: 80%;
    display: block;
    margin: auto;
  }
`;

const StyledCreatureImage = styled.div`
  display: block;
  position: relative;
  width: 250px;
  height: 250px;
  margin: 0px auto 30px auto;
  padding: 40px;
  border-radius: 50%;
  background-color: #eeeeee;
  background-image: url(${(props) => props.background});
  background-size: 100% auto;
  background-repeat: no-repeat;
  background-position: center;

  &:before {
    display: block;
    padding-top: 100%;
  }
`;

const StyledQuestionCircleFilled = styled(QuestionCircleFilled)`
  position: absolute;
  top: 10px;
  right: 10px;
`;

const StyledImage = styled.img`
  width: 100px;
  cursor: pointer;
  opacity: ${(props) => (props.active ? 1 : 0.3)};
  display: block;
  margin: auto;
`;

const ActivityPickerTitle = styled.div`
  font-size: 1.2em;
  text-align: center;
  text-transform: capitalize;
`;

const FormContainer = styled.div`
  display: block;
  width: 80%;
  margin: 50px auto;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

const ActivityPickerContainer = styled.div``;

const ActivityPicker = ({ title, children }) => {
  return (
    <ActivityPickerContainer>
      <div>
        {children}
        <ActivityPickerTitle>{title}</ActivityPickerTitle>
      </div>
    </ActivityPickerContainer>
  );
};

class SpottedContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      markers: [[], [], []], //markers positions for diving, fishing and spearfishing
      formFields: [], //filled fields on first page
      currentStep: 0, // current modal page
      activeOnClick: true,
      formValidated: false, //form validation for first page
      currentActive: 0, //current activity: 0 => diving; 1 => fishing; 2 => spearfishing
      formData: [],
      creatureId: undefined, //creature for this form
      requiredFields: [false, false, false], //forms required for diving, fishing and spearfishing
      submitReady: false, //at least one marker is required
    };

    // preserve the initial state in a new object
    this.baseState = this.state;
  }

  componentDidMount() {
    let { random, creatureId } = this.props;

    this.setState({
      creatureId: random ? Math.floor(Math.random() * 54) + 1 : creatureId,
    });
  }

  componentDidUpdate(prevProps) {
    let { random, creatureId, visible } = this.props;

    if (visible && prevProps != this.props) {
      this.setState({
        creatureId: random ? Math.floor(Math.random() * 54) + 1 : creatureId,
      });
    }
  }

  handleDragEnd = (event, index, category) => {
    let { markers } = this.state;
    markers[category][index] = { lat: event.lngLat[1], lng: event.lngLat[0] };

    setTimeout(() => {
      this.setState(
        {
          markers,
          activeOnClick: true,
        },
        this.handleFormData
      );
    }, 1);
  };

  resetState = () => {
    this.setState({
      activeOnClick: true,
      markers: [[], [], []],
    });
  };

  handleDragStart = () => {
    this.setState({ activeOnClick: false });
  };

  handleMarkerClick = (index) => {
    // let { markers } = this.state;
    // markers.splice(index, 1); //Remove 1 record at index
    // this.setState({ markers });
  };

  handleMapClick = (e) => {
    let { markers, activeOnClick, currentActive, requiredFields } = this.state;
    if (activeOnClick) {
      //if (e.leftButton) {
      requiredFields[currentActive] = true;
      markers[currentActive].push({ lat: e.lngLat[1], lng: e.lngLat[0] });
      this.setState(
        { markers, requiredFields, submitReady: true },
        this.handleFormData
      );
      //}
    }
  };

  next = () => {
    if (this.state.formValidated) {
      const form = this.formRef.props.form;
      form.validateFields((err, data) => {
        if (err) {
          this.setState({ formValidated: false });
          return;
        } else {
          const currentStep = this.state.currentStep + 1;
          this.setState({ formData: data, currentStep });
        }
      });
    }
  };

  prev = () => {
    const currentStep = this.state.currentStep - 1;
    this.setState({ currentStep, formValidated: false });
    this.resetState();
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  saveActivityFormRef = (activityFormRef) => {
    this.activityFormRef = activityFormRef;
  };

  handleFormValidation = (element) => {
    this.setState({ formValidated: false });
    var { formFields } = this.state;

    if (formFields.indexOf(element) === -1) {
      formFields.push(element);
      this.setState({ formFields });
    }

    if (formFields.length == 6) {
      setTimeout(() => {
        const form = this.formRef.props.form;
        form.validateFields((err, data) => {
          if (err) {
            this.setState({ formValidated: false });
            return;
          } else {
            this.setState({ formValidated: true });
          }
        });
      }, 1);
    }
  };

  handleFormData = () => {
    setTimeout(() => {
      let { markers, formData } = this.state;
      var data = this.formRef.props.form.getFieldsValue();
      let markersObj = {
        coordinates: {
          diving: markers[0],
          fishing: markers[1],
          spearfishing: markers[2],
        },
      };
      this.setState({ formData: { ...formData, ...data, ...markersObj } });
    }, 1);
  };

  handleSubmit = () => {
    let { formData } = this.state;

    const form = this.formRef.props.form;

    form.validateFieldsAndScroll((err) => {
      if (err) {
        Object.keys(err).map((element) => {
          this.setState({
            currentActive:
              element == "diving" ? 0 : element == "fishing" ? 1 : 2,
          });
        });
        return;
      } else {
        this.handleModalClose();
        alert("everything ok, currently working on storing the data");
      }
    });
  };

  handleFilterChange = (e) => {
    this.setState({
      currentActive: e,
    });
  };

  handleCreatureChange = (creatureId) => {
    this.setState({ creatureId });
  };

  handleModalClose = () => {
    // reset to initial state
    this.setState(this.baseState);

    this.props.handleModalClose();
  };

  render() {
    var {
      markers,
      currentStep,
      formValidated,
      currentActive,
      requiredFields,
      creatureId,
      submitReady,
    } = this.state;

    const steps = [
      {
        key: 0,
        title: "Taxa",
        content: (
          <InfoContainer>
            <StyledCreatureImage
              background={
                creatureId
                  ? `/api/image/creature/${creatureId}`
                  : "/images/what-have-you-seen/default"
              }
            />
            <h1>Have you seen me lately?</h1>
            <p>
              No âmbito das atividades de investigação desenvolvidas na Madeira
              estamos a fazer uma avaliação sobre a presença e distribuição de
              espécies nas águas costeiras do Arquipélago da Madeira. O
              inquérito é de resposta voluntária, é anónimo e os dados serão
              analisados de forma global.
            </p>
            <FormContainer>
              <SpottedGeneralQuestionsForm
                random={this.props.random}
                creatureId={creatureId}
                handleCreatureChange={this.handleCreatureChange}
                handleFormValidation={this.handleFormValidation}
                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
              />
            </FormContainer>
          </InfoContainer>
        ),
      },
      {
        key: 1,
        title: "Picker",
        content: (
          <div>
            <Row
              type="flex"
              justify="space-around"
              align="bottom"
              style={{ margin: 30 }}
            >
              <ActivityPicker title="diving">
                <StyledImage
                  active={currentActive == 0 && true}
                  src="/images/what-have-you-seen/diving.svg"
                  alt="diving"
                  onClick={() => this.handleFilterChange(0)}
                />
              </ActivityPicker>
              <ActivityPicker title="fishing">
                <StyledImage
                  active={currentActive == 1 && true}
                  src="/images/what-have-you-seen/fishing.svg"
                  alt="fishing"
                  onClick={() => this.handleFilterChange(1)}
                />
              </ActivityPicker>
              <ActivityPicker title="spearfishing">
                <StyledImage
                  active={currentActive == 2 && true}
                  src="/images/what-have-you-seen/spearfishing.svg"
                  alt="spearfishing"
                  onClick={() => this.handleFilterChange(2)}
                />
              </ActivityPicker>
            </Row>
            <FormContainer>
              <InfoContainer>
                <p>
                  Selecione uma atividade, responda às três perguntas e
                  posicione o(s) pin(s) no mapa onde avistou/pescou esta
                  espécie. Caso faça mais que uma atividade repita os passos
                  para cada uma das atividades.
                </p>
                <div style={{ marginTop: "50px" }}>
                  <SpottedActivityForm
                    requiredFields={requiredFields}
                    handleFormData={this.handleFormData}
                    currentActive={currentActive}
                    wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                  />
                </div>
              </InfoContainer>

              <MapBoxMap
                fixedSize={true}
                size="500"
                zoom={10}
                onClick={this.handleMapClick}
              >
                {markers.map((element, index) => {
                  {
                    return element.map((e, i) => {
                      return (
                        <Marker
                          key={i}
                          longitude={e.lng}
                          latitude={e.lat}
                          draggable
                          onDragEnd={(event) =>
                            this.handleDragEnd(event, i, index)
                          }
                          onDragStart={this.handleDragStart}
                        >
                          <svg
                            onContextMenu={() => this.handleMarkerClick(i)}
                            height="25"
                            viewBox="0 0 24 24"
                            style={{
                              fill:
                                index == 0
                                  ? "#000000"
                                  : index == 1
                                  ? "#ff0000"
                                  : index == 2 && "#ff00ff",
                              stroke: "none",
                            }}
                          >
                            <path d={ICON} />
                          </svg>
                        </Marker>
                      );
                    });
                  }
                })}

                <Tooltip
                  placement="topLeft"
                  title="Left click to create new entry points of taxa or right click existing points to remove them"
                >
                  <StyledQuestionCircleFilled style={{ fontSize: "25px" }} />
                </Tooltip>
              </MapBoxMap>
            </FormContainer>
          </div>
        ),
      },
    ];

    return (
      <Modal
        onCancel={this.handleModalClose}
        centered
        visible={this.props.visible}
        closable
        style={{ top: "80px" }}
        width={960}
        footer={[
          currentStep < steps.length - 1 ? (
            <Row key={0} type="flex" justify="center">
              <Tooltip title="Select requested information before advancing">
                <Button
                  disabled={!formValidated}
                  size="large"
                  key="submit"
                  type="primary"
                  onClick={this.next}
                >
                  Next
                </Button>
              </Tooltip>
            </Row>
          ) : (
            <Row key={1} type="flex" justify="center" gutter={16}>
              <Button size="large" onClick={this.prev}>
                Back
              </Button>
              <Button
                disabled={!submitReady}
                size="large"
                key="submit"
                type="primary"
                onClick={this.handleSubmit}
              >
                Submit
              </Button>
            </Row>
          ),
        ]}
      >
        <div>{steps[currentStep].content}</div>
      </Modal>
    );
  }
}

export default SpottedContent;
