import React, { Component } from "react";
import styled, { css } from "styled-components";
import { Form, Input, Row, Col, Button, Radio } from "antd";
import { UserOutlined, UsergroupAddOutlined } from "@ant-design/icons";
import { register } from "redux-modules/auth/actions";
import { connect } from "react-redux";
import { dimensions } from "helpers";
import MapBoxMap from "../Dashboard/Common/MapBoxMap";
import { Marker } from "react-map-gl";
import AuthTemplate from "./AuthTemplate";

const FormItem = Form.Item;

const ICON = `M20.2,15.7L20.2,15.7c1.1-1.6,1.8-3.6,1.8-5.7c0-5.6-4.5-10-10-10S2,4.5,2,10c0,2,0.6,3.9,1.6,5.4c0,0.1,0.1,0.2,0.2,0.3
  c0,0,0.1,0.1,0.1,0.2c0.2,0.3,0.4,0.6,0.7,0.9c2.6,3.1,7.4,7.6,7.4,7.6s4.8-4.5,7.4-7.5c0.2-0.3,0.5-0.6,0.7-0.9
  C20.1,15.8,20.2,15.8,20.2,15.7z`;

const iconStyle = css`
  font-size: 60px;
  border-radius: 50%;
  padding: 20px;
`;

const SubmitButton = styled(Button)`
  width: 100%;
  margin-top: 20px;
`;

const UserSelectorCardContainer = styled.div`
  width: 20%;
  margin: 0 5px 30px 5px;

  margin: auto;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (max-width: ${dimensions.xs}) {
    min-width: 100px;
  }

  h3 {
    font-weight: bold;
    margin: 10px auto;
    font-size: 1.2em;
  }
`;

const UserIcon = styled(UserOutlined)`
${iconStyle}
  border: 1px solid
    ${(props) => (props.active ? "#4f91ce !important" : "gray !important")};
  background: ${(props) =>
    props.active ? "#4f91ce !important" : "white !important"};
  color: ${(props) =>
    props.active ? "white !important" : "gray !important"};`;

const CompanyIcon = styled(UsergroupAddOutlined)`
  ${iconStyle}
  border: 1px solid
    ${(props) => (props.active ? "#4f91ce !important" : "gray !important")};
  background: ${(props) =>
    props.active ? "#4f91ce !important" : "white !important"};
  color: ${(props) => (props.active ? "white !important" : "gray !important")};
`;

const Information = styled.div`
  text-align: center;
  font-size: 0.8em;
  color: #777;
`;

class RegistrationForm extends Component {
  state = {
    confirmDirty: false,
    userType: undefined,
    disabled: false,
    currentStep: 0,
    coordinates: {
      lat: 32.6600150717693,
      lng: -16.92564526551496,
    },
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;

    if (value && value !== form.getFieldValue("password")) {
      callback("The passwords don't match!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;

    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    if (
      value &&
      form.getFieldValue("password_confirmation") &&
      value !== form.getFieldValue("password_confirmation")
    ) {
      callback("The passwords don't match!");
    } else {
      callback();
    }
    callback();
  };

  handleSubmit = (e) => {
    const form = this.props.form;
    const { register } = this.props;
    e.preventDefault();

    form.validateFieldsAndScroll((err, data) => {
      if (!err) {
        const { lng, lat } = this.state.coordinates;

        this.setState({ submitting: true });

        register({
          ...data,
          latitude: lat,
          longitude: lng,
        }).then(() => this.setState({ submitting: false }));
      }
    });
  };

  handleChange = (value) => {
    this.setState({
      userType: value,
      disabled: false,
    });
  };

  next() {
    this.state.userType &&
      this.setState({ currentStep: this.state.currentStep + 1 });
  }

  prev() {
    this.setState({ currentStep: this.state.currentStep - 1 });
  }

  rules = {
    confirmPassword: [
      {
        required: true,
        message: "Please confirm your password!",
      },
      {
        validator: this.compareToFirstPassword,
      },
    ],
    password: [
      {
        required: true,
        message: "Please input your password!",
      },
      {
        min: 6,
        message: "Your password must have atleast 6 characters",
      },
      {
        validator: this.validateToNextPassword,
      },
    ],
    email: [
      {
        type: "email",
        message: "This is not a valid email!",
      },
      {
        required: true,
        message: "Please input your E-mail!",
      },
    ],
    name: [
      {
        required: true,
        message: "Please input your name!",
      },
      {
        min: 2,
        message: "The name must be at least 2 characters.",
      },
    ],
    userable_type: [
      {
        required: true,
        message: "Please choose you type of user!",
      },
    ],
  };

  handleDragEnd = (event) => {
    this.setState({
      coordinates: {
        lng: event.lngLat[0],
        lat: event.lngLat[1],
      },
    });
  };

  next = () => {
    this.setState({ currentStep: this.state.currentStep + 1 });
  };

  prev = () => {
    this.setState({ currentStep: this.state.currentStep - 1 });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { userType, coordinates, currentStep } = this.state;

    const steps = [
      <Radio.Group
        buttonStyle="solid"
        style={{ width: "100%", marginBottom: "15px" }}
        value={userType}
      >
        <Row type="flex" justify="space-around" align="middle">
          <UserSelectorCardContainer>
            <UserIcon
              active={userType == "UserPerson" && true}
              onClick={() => this.handleChange("UserPerson")}
            />
            <h3>User</h3>
          </UserSelectorCardContainer>
          <UserSelectorCardContainer>
            <CompanyIcon
              active={userType == "UserDiveCenter" && true}
              onClick={() => this.handleChange("UserDiveCenter")}
            />
            <h3>Organization</h3>
          </UserSelectorCardContainer>

          <Information>
            {userType == "UserPerson"
              ? "Select this option if you registering an individual account, even if you work to any organization registered on the platform."
              : userType == "UserDiveCenter"
              ? "Select this option if you registering an account for your whale watching company, research center or other similar."
              : "Select one of the options above for which type of user you are."}
          </Information>
        </Row>
      </Radio.Group>,
      <Form layout="vertical">
        <Row gutter={16}>
          <Col xs={24} md={12}>
            <FormItem hasFeedback label="E-mail">
              {getFieldDecorator("email", {
                rules: this.rules.email,
              })(<Input placeholder="Your email" />)}
            </FormItem>
          </Col>
          <Col xs={24} md={12}>
            <FormItem hasFeedback label="Name">
              {getFieldDecorator("name", {
                rules: this.rules.firstName,
              })(<Input placeholder="Name" />)}
            </FormItem>
          </Col>

          <Col xs={24} md={12}>
            <FormItem hasFeedback label="Password">
              {getFieldDecorator("password", {
                rules: this.rules.password,
              })(
                <Input
                  type="password"
                  placeholder="Must have at least 6 characters"
                />
              )}
            </FormItem>
          </Col>
          <Col xs={24} md={12}>
            <FormItem hasFeedback label="Confirm Password">
              {getFieldDecorator("password_confirmation", {
                rules: this.rules.confirmPassword,
              })(
                <Input
                  type="password"
                  placeholder="Confirm Password"
                  onBlur={this.handleConfirmBlur}
                />
              )}
            </FormItem>
          </Col>
        </Row>

        {userType == "UserDiveCenter" && (
          <div>
            <Information>
              Drag the pin below to match the physical location of your company,
              institute or organization.
            </Information>
            <MapBoxMap fixedSize={true} size="230" zoom={12}>
              <Marker
                longitude={coordinates.lng}
                latitude={coordinates.lat}
                draggable
                onDragEnd={this.handleDragEnd}
              >
                <svg
                  height="25"
                  viewBox="0 0 24 24"
                  style={{
                    fill: "#40a9ff",
                    stroke: "none",
                  }}
                >
                  <path d={ICON} />
                </svg>
              </Marker>
            </MapBoxMap>
          </div>
        )}

        <Form.Item style={{ display: "none" }}>
          {getFieldDecorator("userable_type", {
            initialValue: userType,
            rules: this.rules.userable_type,
          })(<Input />)}
        </Form.Item>
      </Form>,
    ];

    return (
      <AuthTemplate image="register.svg" registration={true}>
        <div>{steps[currentStep]}</div>
        {currentStep === 0 ? (
          <SubmitButton
            type="default"
            onClick={this.next}
            disabled={!userType && true}
          >
            Next
          </SubmitButton>
        ) : (
          <Row gutter={16}>
            <Col span={12}>
              <SubmitButton type="default" onClick={this.prev}>
                Back
              </SubmitButton>
            </Col>
            <Col span={12}>
              <SubmitButton
                onClick={this.handleSubmit}
                loading={this.state.submitting}
                type="primary"
                htmlType="submit"
                disabled={this.state.disabled}
              >
                Sign Up
              </SubmitButton>
            </Col>
          </Row>
        )}
      </AuthTemplate>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    register: (user) => dispatch(register(user)),
  };
};

export default Form.create()(
  connect(
    null,
    mapDispatchToProps
  )(RegistrationForm)
);
