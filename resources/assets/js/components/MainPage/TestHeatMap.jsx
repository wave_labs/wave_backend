import React, { Component } from "react";
import styled from "styled-components";
import HeatMapList from "./LocationMap/HeatMapList";
import axios from "axios";

const StyledMap = styled.div`
  width: 96%;
  margin: auto;
  -webkit-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  margin-bottom: 5%;
  min-height: 600px;
`;

const StyledSubtitle = styled.div`
  font-size: 36px;
  text-align: center;
  margin-bottom: 1%;
  font-weight: 700;
  line-height: 1.3;

  @media screen and (max-width: 1000px) {
    font-size: 24px;
  }
`;

const coord = [];

class TestHeatMap extends Component {

  state = {
    isLoading: true,
  }
   
  async componentDidMount(){
    await axios.get(`${window.location.origin}/api/coordinates/sighting`).then(
      function(res){
        res.data.data.forEach(element => {
          coord.push([element.longitude, element.latitude]);
        });
      } 
    );
    this.setState({isLoading: false});
  }

  render() {
    const { isLoading } = this.state;
    return (
      <div>
        <StyledSubtitle>Heatmap</StyledSubtitle>
        
        <StyledMap>
          {!isLoading && <HeatMapList size="600" data={coord}/>}
        </StyledMap>
      </div>
    );
  }
}
export default TestHeatMap;