import React, { Component } from "react";
import { List, Collapse, Empty } from "antd";
import styled from "styled-components";
import axios from "axios";
import { StyledTitle } from "helpers";
import moment from "moment";

const { Panel } = Collapse;

let locale = {
  emptyText: "none"
};

const Container = styled.div`
  width: 80%;
  margin: auto;

  @media screen and (max-width: 500px) {
    width: 95%;
  }
`;

const ListContainer = styled.div`
  width: 100%;
`;

const StyledSubTitle = styled.h1`
  margin-bottom: 2px;
`;
const StyledDate = styled.h4`
  color: #777777;
  margin-bottom: 3%;
`;

const StyledDescription = styled.h3`
  margin-bottom: 2px;
`;

const StyledImage = styled.img`
  margin: 3% auto;
  max-width: 100%;
  height: auto;
  display: block;
  max-height: 300px;
`;

class WhatsNew extends Component {
  state = {
    initLoading: true,
    loading: false,
    data: []
  };

  componentDidMount() {
    axios.get(`${window.location.origin}/api/whats-new`).then(({ data }) => {
      this.setState({
        initLoading: false,
        data: data.data
      });
    });
  }

  render() {
    const { data } = this.state;

    return (
      <div>
        <Container>
          <StyledTitle>What's New</StyledTitle>

          <List
            itemLayout="horizontal"
            dataSource={data.slice(0, 10)}
            renderItem={item => (
              <List.Item
                style={{
                  borderBottom: 0,
                  borderBottomColor: "transparent",
                  padding: 0
                }}
              >
                <ListContainer>
                  <StyledImage src={item.image} />
                  <StyledSubTitle>{item.title}</StyledSubTitle>
                  <StyledDescription>{item.description}</StyledDescription>
                  <StyledDate>{moment(item.date).format("ll")}</StyledDate>

                  {item.update["length"] != 0 && (
                    <div>
                      <h4 style={{ fontWeight: "bold" }}>Updates</h4>
                      <ul>
                        {Object.values(item.update).map(function(element) {
                          return <li key={element.id}>{element.message}</li>;
                        })}
                      </ul>
                    </div>
                  )}

                  {item.research["length"] != 0 && (
                    <div>
                      <h4 style={{ fontWeight: "bold" }}>Research</h4>
                      <ul>
                        {Object.values(item.research).map(function(element) {
                          return <li key={element.id}>{element.message}</li>;
                        })}
                      </ul>
                    </div>
                  )}

                  {item.event["length"] != 0 && (
                    <div>
                      <h4 style={{ fontWeight: "bold" }}>Events</h4>
                      <ul>
                        {Object.values(item.event).map(function(element) {
                          return <li key={element.id}>{element.message}</li>;
                        })}
                      </ul>
                    </div>
                  )}
                </ListContainer>
              </List.Item>
            )}
          />
          <List
            itemLayout="horizontal"
            dataSource={data.slice(10)}
            locale={{
              emptyText: (
                <Empty
                  description={<span></span>}
                  imageStyle={{ display: "none" }}
                />
              )
            }}
            style={{
              marginTop: "10%"
            }}
            renderItem={item => (
              <List.Item
                style={{
                  borderBottom: 0,
                  borderBottomColor: "transparent",
                  padding: 0
                }}
              >
                <div>
                  <Collapse bordered={false}>
                    <Panel
                      header={
                        <div style={{ fontWeight: "bold" }}>
                          {moment(item.date).format("ll")} - {item.title}
                        </div>
                      }
                      style={{
                        borderBottom: "none"
                      }}
                      key={item.id}
                    >
                      <StyledImage src={item.image} />
                      <StyledSubTitle>{item.title}</StyledSubTitle>
                      <StyledDate>{moment(item.date).format("ll")}</StyledDate>
                      {item.update["length"] != 0 && (
                        <div>
                          <h4 style={{ fontWeight: "bold" }}>Updates</h4>
                          <ul>
                            {Object.values(item.update).map(function(element) {
                              return (
                                <li key={element.id}>{element.message}</li>
                              );
                            })}
                          </ul>
                        </div>
                      )}

                      {item.research["length"] != 0 && (
                        <div>
                          <h4 style={{ fontWeight: "bold" }}>Research</h4>
                          <ul>
                            {Object.values(item.research).map(function(
                              element
                            ) {
                              return (
                                <li key={element.id}>{element.message}</li>
                              );
                            })}
                          </ul>
                        </div>
                      )}

                      {item.event["length"] != 0 && (
                        <div>
                          <h4 style={{ fontWeight: "bold" }}>Events</h4>
                          <ul>
                            {Object.values(item.event).map(function(element) {
                              return (
                                <li key={element.id}>{element.message}</li>
                              );
                            })}
                          </ul>
                        </div>
                      )}
                    </Panel>
                  </Collapse>
                </div>
              </List.Item>
            )}
          />
        </Container>
      </div>
    );
  }
}

export default WhatsNew;
