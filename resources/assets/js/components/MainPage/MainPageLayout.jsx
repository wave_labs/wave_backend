import React, { Component } from "react";
import { Row, BackTop } from "antd";
import NavBar from "../Common/NavBar";
import MainPageAlert from "./MainPageAlert";
import Footer from "../Common/Footer";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { alertActions } from "redux-modules/alert/actions";
import styled from "styled-components";

const Page = styled.div`
  background: rgb(255, 254, 252);
  position: relative;
`;

const Container = styled(Page)`
  max-width: 960px;
  margin: 0 auto;
  
  @media screen and (max-width: 900px) {
    width: 95%;
  }
`;

const ContentContainer = styled.div`
  padding-top: 100px;
`;

class MainPageLayout extends Component {
  constructor(props) {
    super(props);

    this.unlisten = this.props.history.listen((location, action) => {
      this.props.clearAlert();
    });

    this.state = {
      prevScrollpos: window.pageYOffset,
      visible: true
    };
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    this.unlisten();
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    const currentScrollPos = window.pageYOffset;
    const visible = 120 > currentScrollPos;

    this.setState({
      visible
    });
  };

  render() {
    return (
      <Page>
        <BackTop />
        <MainPageAlert />
        <Container>
          <NavBar scroll={this.state.visible} />

          <ContentContainer>
            <Row>{this.props.children}</Row>
          </ContentContainer>

          <Footer />
        </Container>
      </Page>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    clearAlert: () => dispatch(alertActions.clear())
  };
};

export default withRouter(connect(null, mapDispatchToProps)(MainPageLayout));
