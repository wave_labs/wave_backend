import React, { Component } from "react";
import { Row, Divider, Col, Button } from "antd";
import styled from "styled-components";
import { StyledTitle } from "helpers";
import axios from "axios";

const Container = styled.div`
  width: 100%;
`;

const StyledSubtitle = styled.div`
  font-size: 36px;
  text-align: center;
  margin-bottom: 3%;
  font-weight: 700;
  line-height: 1.3;
  margin: auto;
  display: block;

  @media screen and (max-width: 1000px) {
    font-size: 24px;
  }
`;

const ProjectDescriptionContainer = styled.div`
  display: block;
  margin: auto;
  margin-bottom: 5%;
  width: 60%;
  text-align: center;

  @media screen and (max-width: 1000px) {
    width: 80%;
  }
`;

const ProjectDescription = styled.div`
  font-size: 20px;

  @media screen and (max-width: 1000px) {
    font-size: 18px;
  }
`;

const ProjectRef = styled.div`
  font-size: 18px;
  color: #777777;

  @media screen and (max-width: 1000px) {
    font-size: 16px;
  }
`;

const ProjectFunds = styled.div`
font-size: 18px;
color: #777777;
font-style: italic;

@media screen and (max-width: 1000px) {
  font-size: 16px;
}
`;


const StyledImage = styled.img`
  width: 100%;
`;

const DataContainer = styled.div`
  width: 80%;
  display: block;
  margin: auto;

`;

const ImageContainer = styled(DataContainer)`
  margin: 5% auto;

  @media screen and (max-width: 1000px) {
    width: 96%;
  }
`;

class Largescale extends Component {
  
  render() {
    
    return (
      <Container>
        <StyledTitle>Largescale</StyledTitle>

        <ProjectDescriptionContainer>
          <ProjectDescription>
          Location-based Augmented Reality Gadgets and Environment-friendly Sightseeing of 
          Cultural Attractions for Locals and Excursionists. 
          </ProjectDescription>
          <ProjectRef>Ref: FCT32474.</ProjectRef>
          <ProjectFunds>EUR 250k</ProjectFunds>
        </ProjectDescriptionContainer>

        <ImageContainer>
          <Row type="flex" align="middle" justify="space-around">
            <Col span={6}>
              <StyledImage src="/images/largescale/largescale.png" alt="" />
            </Col>
          </Row>
        </ImageContainer>

      
      </Container>
    );
  }
}

export default Largescale;
