import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import { Form, Icon, Input, Button, Row } from "antd";
import { connect } from "react-redux";
import { recoverPassword } from "redux-modules/auth/actions";
import Logo from "../Common/TitleImage";
import AuthTemplate from "./AuthTemplate";
import RecoverTemplate from "./RecoverTemplate";

const FormItem = Form.Item;
const SubmitButton = styled(Button)`
  width: 100%;
`;

const StyledForm = styled(Form)`
  max-width: 300px;
  width: 100%;
`;

const StyledResetButton = styled(Button)`
  width: 100%;
`;

class ForgotPasswordForm extends Component {
  state = {};

  rules = {
    email: [
      {
        required: true,
        message: "Please input your email!",
      },
      {
        type: "email",
        message: "Please input a valid email!",
      },
    ],
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { recoverPassword } = this.props;

    this.props.form.validateFieldsAndScroll((err, email) => {
      if (!err) {
        this.setState({ submitting: true });
        recoverPassword(email).then((res) => {
          this.setState({ submitting: false });

          if (!res.success) {
            this.props.form.setFields({
              email: {
                value: email.value,
                errors: [new Error("Your email doesn't exist!")],
              },
            });
          }
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <RecoverTemplate image="login.svg" title={"Recover Account"} subtitle={"The ocean forgot your credentials."}>
        <Fragment>
          <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
            <FormItem hasFeedback>
              {getFieldDecorator("email", { rules: this.rules.email })(
                <Input
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.5)" }} />
                  }
                  placeholder="Email"
                />
              )}
            </FormItem>
            <SubmitButton
              loading={this.state.submitting}
              type="primary"
              htmlType="submit"
            >Reset Password</SubmitButton>
          </Form>
        </Fragment>
      </RecoverTemplate>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    recoverPassword: (email) => dispatch(recoverPassword(email)),
  };
};

export default Form.create()(
  connect(null, mapDispatchToProps)(ForgotPasswordForm)
);
