import React, { Component } from "react";
import { Row, Col, Button } from "antd";
import styled from "styled-components";
import { StyledTitle, dimensions } from "helpers";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import CreatureRemoteSelectContainer from "../../Dashboard/CreatureContent/CreatureRemoteSelectContainer";
import MarkerContent from "../MarkerContent/MarkerContent";
import {
  fetchCreatureSize,
  fetchCreatureActivities,
} from "redux-modules/creature/actions";

const Container = styled.div`
  width: 80%;
  margin: auto;

  @media (max-width: ${dimensions.xl}) {
    width: 96%;
  }
`;

const Study = styled.div`
  font-size: 1.6em;
  margin-top: 3%;

  @media (max-width: ${dimensions.sm}) {
    font-size: 1.2em;
    text-align: center;
  }
`;

const StudyDescription = styled(Study)`
  color: #777777;
  font-size: 1em;
  margin: 0;

  @media (max-width: ${dimensions.sm}) {
    font-size: 1em;
  }
`;

const StyledImage = styled.img`
  width: 100%;
  min-width: 50px;
  max-width: 300px;
  margin: auto;
  display: block;

  @media (max-width: ${dimensions.sm}) {
    width: 50%;
  }
`;

const StyledSubtitle = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
  margin-bottom: 50px;
`;

class StudiesContent extends Component {
  state = {
    searchParams: {},
    loadingParams: true,
    random: false,
  };

  componentDidMount() {
    var { search } = this.props.location;
    var { searchParams } = this.state;
    const params = new URLSearchParams(search);
    params.forEach((value, key) => {
      if (value) {
        searchParams[key] = value;
      }
    });

    if (!searchParams["creatureId"]) {
      searchParams["creatureId"] = Math.floor(Math.random() * 54) + 1;
    }

    this.setState({ searchParams });

    if (searchParams["study"] == "markers") {
      this.handleCreatureData(searchParams["creatureId"]);
    }

    this.setState({ loadingParams: false });
  }

  handleCreatureData = (id) => {
    this.props.fetchCreatureSize(id);
    this.props.fetchCreatureActivities(id);
  };

  handleModalClose = () => {
    this.setState({ searchParams: {} });
  };

  handleSearchParamsChange = (key, value) => {
    var { searchParams, random } = this.state;
    searchParams[key] = value;
    if (!searchParams.creatureId) {
      searchParams.creatureId = Math.floor(Math.random() * 54) + 1;
      random = true;
    }

    this.handleCreatureData(searchParams.creatureId);
    this.setState({ searchParams, random });
  };

  render() {
    var { searchParams, loadingParams, random } = this.state;
    return (
      <div>
        <Container>
          <StyledTitle>Studies</StyledTitle>
          <StyledSubtitle>
            Help us conserv these species worldwide
          </StyledSubtitle>

          {!loadingParams && (
            <MarkerContent
              handleCreatureData={this.handleCreatureData}
              handleModalClose={this.handleModalClose}
              visible={
                searchParams.study && searchParams.study == "markers" && true
              }
              creatureId={searchParams.creatureId && searchParams.creatureId}
              random={random}
            />
          )}

          <Row type="flex" align="middle" justify="space-around">
            <Col xs={24} sm={8}>
              <StyledImage src="/storage/images/logo/markers" />
            </Col>
            <Col xs={24} sm={14}>
              <Study>Markers - Have you seen me lately?</Study>
              <StudyDescription>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus
                necessitatibus, alias excepturi corrupti unde explicabo maxime
                molestiae nisi suscipit culpa laudantium quos a, fugiat
                veritatis deserunt. Tempora officiis vero fugit.
              </StudyDescription>
              <Row
                type="flex"
                justify="space-between"
                align="middle"
                gutter={16}
                style={{ margin: "10px auto" }}
              >
                <div style={{ width: "50%" }}>
                  <CreatureRemoteSelectContainer
                    onChange={(value) =>
                      this.handleSearchParamsChange("creatureId", value)
                    }
                    value={
                      searchParams.creatureId
                        ? searchParams.creatureId
                        : undefined
                    }
                    scientific
                    placeholder="List of studied species"
                  />
                </div>

                <Button
                  onClick={() =>
                    this.handleSearchParamsChange("study", "markers")
                  }
                  ghost="true"
                  type="primary"
                >
                  Start
                </Button>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCreatureSize: (id) => dispatch(fetchCreatureSize(id)),
    fetchCreatureActivities: (id) => dispatch(fetchCreatureActivities(id)),
  };
};

export default connect(
  null,
  mapDispatchToProps
)(StudiesContent);
