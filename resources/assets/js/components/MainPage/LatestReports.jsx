import React, { Component } from "react";
import { Row, Divider, Col, Button } from "antd";
import styled from "styled-components";
import { StyledTitle } from "helpers";
import axios from "axios";

const Container = styled.div`
  width: 100%;
`;

const StyledSubtitle = styled.div`
  font-size: 36px;
  text-align: center;
  margin-bottom: 3%;
  font-weight: 700;
  line-height: 1.3;
  margin: auto;
  display: block;

  @media screen and (max-width: 1000px) {
    font-size: 24px;
  }
`;

const ProjectDescriptionContainer = styled.div`
  display: block;
  margin: auto;
  margin-bottom: 5%;
  width: 60%;
  text-align: center;

  @media screen and (max-width: 1000px) {
    width: 80%;
  }
`;

const ProjectDescription = styled.div`
  font-size: 20px;

  @media screen and (max-width: 1000px) {
    font-size: 18px;
  }
`;

const ProjectRef = styled.div`
  font-size: 18px;
  color: #777777;

  @media screen and (max-width: 1000px) {
    font-size: 16px;
  }
`;

const ProjectFunds = styled.div`
font-size: 18px;
color: #777777;
font-style: italic;

@media screen and (max-width: 1000px) {
  font-size: 16px;
}
`;

const DataValue = styled.div`
  margin-top: 0;
`;

const DataName = styled.div`
  font-weight: 700;
  margin-bottom: 0;
`;

const DataSubContainer = styled.div`
  margin: 5px 0px;
  font-size: 20px;

  @media screen and (max-width: 1000px) {
    margin: 1px 0px;
    font-size: 16px;
  }
`;

const StyledImage = styled.img`
  width: 100%;
`;

const DataContainer = styled.div`
  width: 80%;
  display: block;
  margin: auto;

`;

const ImageContainer = styled(DataContainer)`
  margin: 5% auto;

  @media screen and (max-width: 1000px) {
    width: 96%;
  }
`;

class LatestReports extends Component {
  constructor(props) {
    super(props);
    let initialReport = {
      id: "-",
      id_device: "-",
      name_device: "-"
    };
    let latestLat = "-";
    let latestLon = "-";
    let lastUpdate = "-";
    this.state = {
      lastReport: initialReport,
      latestLat: latestLat,
      latestLon: latestLon,
      lastUpdate: lastUpdate,
      lastReportId: ""
    };
  }

  getLatestReport() {
    axios.get(`${window.location.origin}/api/reports`).then(({ data }) => {
      //get last report
      let lastReport = data.pop();

      //deafult values of fields
      let latestLat = "-";
      let latestLon = "-";
      let lastUpdate = "-";
      let lastReportId = this.state.lastReportId;

      if (lastReportId != lastReport.id) {
        //check if has lat and lon
        lastReport.fields.map((key, value) => {
          if (key.type == "Latitude") {
            latestLat = key.value;
          }

          if (key.type == "Longitude") {
            latestLon = key.value;
          }
        });

        //check if has collected_at
        if (lastReport.collected_at) {
          lastUpdate = lastReport.collected_at;
        }

        lastReportId = lastReport.id;
      } else {
        lastReport = {
          id: "-",
          id_device: "-",
          name_device: "-"
        };
      }

      this.setState({
        lastReport: lastReport,
        latestLat: latestLat,
        latestLon: latestLon,
        lastUpdate: lastUpdate,
        lastReportId: lastReportId
      });
    });
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      this.getLatestReport();
    }, 5000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    let turtleImage =
      this.state.lastReport.name_device != "-" ? (
        <StyledImage src="images/iot/turtle-icon.png" />
      ) : (
        <StyledImage src="images/iot/no-content-icon.png" />
      );

    return (
      <Container>
        <StyledTitle>Intertagua</StyledTitle>

        <ProjectDescriptionContainer>
          <ProjectDescription>
            Interfaces Aquáticas Interativas para Deteção e Visualização da
            Megafauna Marinha Atlântica e Embarcações na Macaronésia usando
            Marcadores Rádio-transmissores.
          </ProjectDescription>
          <ProjectRef>Ref: MAC2/1.1.a /385</ProjectRef>
          <ProjectFunds>EUR 408k</ProjectFunds>
        </ProjectDescriptionContainer>

        <ImageContainer>
          <Row type="flex" align="middle" justify="space-around">
            <Col span={6}>
              <StyledImage src="/images/intertagua/intertagua2.png" alt="" />
            </Col>
            <Col span={12}>
              <StyledImage src="/images/intertagua/mac.png" alt="" />
            </Col>
          </Row>
        </ImageContainer>

        <StyledSubtitle>Latest from Sea</StyledSubtitle>

        <DataContainer>
          <Row type="flex" align="middle" justify="space-between">
            <Col span={14}>
              <DataSubContainer>
                <DataName>Device Name:</DataName>
                <DataValue>{this.state.lastReport.name_device}</DataValue>
              </DataSubContainer>
              <DataSubContainer>
                <DataName>Device ID:</DataName>
                <DataValue>{this.state.lastReport.id_device} </DataValue>
              </DataSubContainer>
              <DataSubContainer>
                <DataName>Report ID:</DataName>
                <DataValue>{this.state.lastReport.id} </DataValue>
              </DataSubContainer>
              <DataSubContainer>
                <DataName>LAT:</DataName>
                <DataValue>{this.state.latestLat}</DataValue>
              </DataSubContainer>
              <DataSubContainer>
                <DataName>LON:</DataName>
                <DataValue>{this.state.latestLon}</DataValue>
              </DataSubContainer>
              <DataSubContainer>
                <DataName>Last Update:</DataName>
                <DataValue>{this.state.lastUpdate}</DataValue>
              </DataSubContainer>
            </Col>
            <Col span={10}>{turtleImage}</Col>
          </Row>
        </DataContainer>
      </Container>
    );
  }
}

export default LatestReports;
