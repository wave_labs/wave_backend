import { Row } from 'antd'
import React, { Component } from 'react'
import styled from "styled-components";
import { TitleSection } from "helpers"
import { Link } from "react-router-dom";

const Container = styled(Link)`
    width: 30%;

    h2 {
    font-size: 1.4em;
    text-align: center;
    }

    img {
    width: 70%;
    display: block;
    margin: auto;
    }

    p {
    color: #777777;
    text-align: center;
    }
`;


const App = ({ title, image, to }) => {
    return (
        <Container to={to}>
            <img src={image} />
            <h2>{title}</h2>
        </Container>
    );
};

class PrivacyContent extends Component {
    render() {
        return (
            <div>
                <TitleSection
                    title="Terms & Privacy"
                    subtitle="Our policies and procedures on the collection, use and disclosure of your information."
                />


                <Row type="flex" justify="space-around">
                    <App
                        title="Litter Reporter"
                        image="/storage/images/logo/litter-reporter"
                        to="/privacy/litterReporter"
                    />
                </Row>
            </div>

        )
    }
}

export default PrivacyContent
