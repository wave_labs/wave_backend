export const team = [
    {
        avatar: "antonio",
        name: "António Ramos",
        job: "Prototyping of Low-cost Biodiversity Loggers for Marine Megafauna",
    },
    {
        avatar: "diogo",
        name: "Diogo Martins",
        job:
            "Aerial Marine Biodiversity Assessments using Aquatic Internet of Things",
    },
    {
        avatar: "claudio",
        name: "Cláudio Rodrigues",
        job:
            "High-altitude Balloon and LoRa for Real-time Environmental Telemetry",
    },
    {
        avatar: "fabio",
        name: "Fábio Abreu",
        job: "Application Programming Interfaces for Depicting Aquatic IoT",
    },
    {
        avatar: "alex",
        name: "João Faria",
        job: "Cyber-Physical Systems for Increasing of Aquatic Literacy",
    },
    {
        avatar: "joaoP",
        name: "João Pestana",
        job: "Estimation of Underwater Location of Divers using IoT",
    },
    {
        avatar: "joaoF",
        name: "João Freitas",
        job:
            "Creation of Tangible Interactive Interfaces for Depicting Marine-based Data",
    },
    {
        avatar: "joaoG",
        name: "João Gouveia",
        job:
            "Acoustic Assessments and Artificial Intelligence of Cetacean Vocal Calls",
    },
    {
        avatar: "franco",
        name: "João Franco",
        job:
            "Front-end Development and Graphical User Interfaces for Depicting the Marine Assessments",
    },
    {
        avatar: "jorge",
        name: "Jorge Lopes",
        job:
            "Enhancement of the IoT with Battery Autonomy, and LoRa for In-the-Wild and Long-term Deployments",
    },
    {
        avatar: "rubenf",
        name: "Rúben Freitas",
        job: "Full Stack Development for Depicting the Marine Biodiversity",
    },
    {
        avatar: "louis",
        name: "Louis Rodrigues",
        job: "Graphical Design and Prototyping of the Interactive Environments",
    },
    {
        avatar: "miguel",
        name: "Miguel Andrade",
        job:
            "Enhancing the Augmented Reality Experience through Interactive 3D Holography",
    },
    {
        avatar: "paulo",
        name: "Paulo Aguiar",
        job: "Dashboard for Depicting and Alerting the Marine Megafauna Presence",
    },
    {
        avatar: "pinto",
        name: "Pedro Pinto",
        job: "GPS-free Location Estimation of Surface Marine Objects",
    },
    {
        avatar: "ricardo",
        name: "Ricardo Jardim",
        job:
            "Surface Biodiversity Assessments using Whale-Watching Sea-vessels in Whale Reporter App",
    },
    {
        avatar: "rubenS",
        name: "Rúben Sousa",
        job:
            "Underwater Biodiversity Assessments using Scuba-Divers in Dive Reporter App",
    },
    {
        avatar: "telmo",
        name: "Telmo Silva",
        job:
            "Cross Reality Interfaces for On- and Off- Shore Interaction with Greater Whales",
    },
    {
        avatar: "victor",
        name: "Victor Azevedo",
        job: "Mechanics and Design applied to Biologgers and Geodesic Structures",
    },
];

export const advisory = [
    {
        avatar: "avatar",
        name: "Albert Taxonera",
        job: "BIOSAL, Sal, CV",
    },
    {
        avatar: "ana",
        name: "Ana Dinis",
        job: "MARE, Madeira, PT",
    },
    {
        avatar: "avatar",
        name: "Cátia Gouveia",
        job: "SPEA, Madeira, PT",
    },
    {
        avatar: "filipe",
        name: "Filipe Alves",
        job: "MARE, Madeira, PT",
    },
    {
        avatar: "joao",
        name: "João Monteiro",
        job: "MARE, Madeira, PT",
    },
    {
        avatar: "avatar",
        name: "Mafalda Freitas",
        job: "EMBF, Madeira, PT",
    },
    {
        avatar: "avatar",
        name: "Marc Fernández",
        job: "Monicet, Azores, PT",
    },
    {
        avatar: "nuno",
        name: "Nuno Nunes",
        job: "TECNICO, Lisbon, PT",
    },
    {
        avatar: "avatar",
        name: "Rosa Pires",
        job: "IFCN, Madeira, PT",
    },
    {
        avatar: "avatar",
        name: "Rui Prieto",
        job: "DRAM, Azores, PTPLOCAN",
    },
    {
        avatar: "avatar",
        name: "Silvana Neves",
        job: "Canary Islands, ES",
    },
    {
        avatar: "avatar",
        name: "Thomas Dellinger",
        job: "UMa, Madeira, PT",
    },
    {
        avatar: "valentina",
        name: "Valentina Nisi",
        job: "UMa, Madeira, PT",
    },
];

export const contributors = [
    { name: "Adriano Andrade" },
    { name: "André Nunes" },
    { name: "Alexandre Borges" },
    { name: "Bernardo Galvão" },
    { name: "Cristiano França" },
    { name: "Fábio Pacheco" },
    { name: "Francisco Serrão" },
    { name: "Hildegardo Noronha" },
    { name: "Leonardo Abreu" },
    { name: "Luís Aguiar" },
    { name: "Marcos Oliveira" },
    { name: "Pedro Barata" },
    { name: "Rodrigo Rocha" },
    { name: "Tomás Ferreira" },
    { name: "Wilson Agrela" },
];