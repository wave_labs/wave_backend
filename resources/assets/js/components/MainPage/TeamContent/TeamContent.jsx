import React from "react";
import { Row, Avatar } from "antd";
import styled from "styled-components";
import { StyledTitle, dimensions } from "helpers";
import { advisory, team, contributors } from "./helper";

const Container = styled.div`
  width: 100%;
`;

const StyledName = styled.h1`
  margin-top: 3%;
  margin-bottom: 2%;
  text-align: center;
  font-weight: 500;
  font-size: 1.4rem;

  @media screen and (max-width: ${dimensions.md}) {
    font-size: 1.2rem;
  }
`;

const SectionTitle = styled(StyledTitle)`
  font-size: 36px;
  text-align: center;
  margin-top: 5%;
  font-weight: 700;
  line-height: 1.3;
  margin-bottom: 1%;

  @media screen and (max-width: ${dimensions.lg}) {
    font-size: 28px;
  }
`;

const SectionSubtitle = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
  margin-bottom: 1%;

  @media screen and (max-width: ${dimensions.lg}) {
    font-size: 18px;
  }
`;

const StyledDescription = styled.p`
  text-align: center;
  font-size: 1.1rem;

  @media screen and (max-width: ${dimensions.md}) {
    font-size: 0.9rem;
  }
`;

const StyledContributor = styled.div`
  text-align: center;
  font-weight: bold;
  margin: 2% 0%;
  width: 30%;

  @media screen and (max-width: ${dimensions.sm}) {
    width: 50%;
  }
`;

const StyledAvatar = styled(Avatar)`
  box-shadow: 0px 0px 36px -28px rgba(0, 0, 0, 0.4);
  margin: auto !important;
  display: block !important;
`;

const StyledCard = styled.div`
  width: 30%;
  margin: 3% auto;

  @media screen and (max-width: ${dimensions.md}) {
    width: 45%;
  }
`;

const TeamMember = ({ avatar, name, job }) => (
  <StyledCard>
    <StyledAvatar size={120} src={"/images/team/" + avatar + ".png"} />

    <StyledName>{name}</StyledName>

    <StyledDescription>{job}</StyledDescription>
  </StyledCard>
);

const Section = ({ title, subtitle, children }) => (
  <div>
    <SectionTitle>{title}</SectionTitle>
    <SectionSubtitle>{subtitle}</SectionSubtitle>
    <Row type="flex" justify="space-around" align="top">
      {children}
    </Row>
  </div>
);

const TeamContent = () => {
  return (
    <Container>
      <StyledTitle>Team</StyledTitle>
      <Row type="flex" justify="center" align="middle">
        <TeamMember
          avatar="marko"
          name="Marko Radeta"
          job="Coordinator (PI), Aquatic User Interfaces (AUI), Human Computer Biosphere Interaction (HCBI), Internet of Water Things (IoWT), Open Waters Long-Range (OWLoRA)"
        />
      </Row>
      <Row type="flex" justify="space-around" align="top">
        {team.map((member) => (
          <TeamMember
            avatar={member.avatar}
            name={member.name}
            job={member.job}
          />
        ))}
      </Row>

      <Section
        title="Scientific Advisory"
        subtitle="United Marine Biologists and HCI Specialists"
      >
        {advisory.map((member) => (
          <TeamMember
            avatar={member.avatar}
            name={member.name}
            job={member.job}
          />
        ))}
      </Section>

      <Section
        title="Past Contributors"
        subtitle="Engineers, Designers and Research Assistants"
      >
        {contributors.map((member) => (
          <StyledContributor>{member.name}</StyledContributor>
        ))}
      </Section>
    </Container>
  );
};

export default TeamContent;
