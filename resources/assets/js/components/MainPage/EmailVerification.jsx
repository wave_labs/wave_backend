import React from "react";
import styled from "styled-components";
import { Row } from "antd";
import Logo from "../Common/TitleImage";
import { Container } from "./Projects/projectsHelper";



const SubtitleContainer = styled.div`
  display: block;
  margin: auto;
  width: 60%;
  text-align: center;

  @media screen and (max-width: 1000px) {
    width: 80%;
  }
`;

const Subtitle = styled.h2`
  font-size: 20px;

  @media screen and (max-width: 1000px) {
    font-size: 18px;
  }
`;

const EmailVerification = () => {
  return (
    <Container>
      <Row type="flex" justify="center" align="middle">
        <Logo />
      </Row>
      <Row>
      <SubtitleContainer>
        <Subtitle>Your email has been confirmed. You can now start using Wave Labs</Subtitle>
      </SubtitleContainer>
      </Row>
    </Container>
  );
};

export default EmailVerification;
