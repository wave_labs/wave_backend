import React, { Component } from "react";
import { Row, Col, Modal } from "antd";
import styled from "styled-components";
import { StyledTitle, DomeContent } from "helpers";
import "./css/Style.css";

const StyledFrameLinks = styled.div`
  display: block;
  color: #0275d8;
  font-weight: bold;
  margin: auto 6px;
  cursor: pointer;

  &:hover {
    color: #5bc0de;
  }
`;

const StyledTitleDescription = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
  margin-bottom: 50px;
`;

const FrameContainer = styled(Col)`
  margin-bottom: 65px;
  margin-top: 50px;

  @media screen and (max-width: 500px) {
    width: 70%;
  }
`;

const StyledIframe = styled.iframe`
  width: 100%;
  height: 90%;
  border: none;
`;

const StyledModalIframe = styled(StyledIframe)`
  width: 100%;
  height: 100%;
`;

const IFrameContainer = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`;

const StyledIframeDescriptionContainer = styled.div`
  position: absolute;
  width: 30%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  text-align: left;
`;

const StyledIframeDescription = styled.div`
  position: relative;
  top: 50%;
  transform: translateY(-50%);
  width: 85%;
  margin: auto;
`;

const StyledIframeDescriptionTitle = styled.h1`
  color: white;
  font-weight: bold;
  font-size: 3em;
  margin-bottom: 0;
`;

const StyledIframeDescriptionContent = styled.h2`
  color: white;
  font-size: 1.3em;
`;

const StyledFrameTitle = styled.h1`
  margin-top: 3%;
  margin-bottom: 2%;
  text-align: center;
  font-weight: 500;
  font-size: 1.4rem;

  @media screen and (max-width: 800px) {
    font-size: 1.2rem;
  }
`;

const StyledFrameSubTitle = styled.p`
  text-align: center;
  font-size: 1.1rem;
  color: #777777;
  margin: 0 auto;

  @media screen and (max-width: 800px) {
    font-size: 0.9rem;
  }
`;

const Container = styled.div`
  width: 100%;
`;

const StyledRow = styled(Row)`
  margin: 1% 3%;
`;

class Domes extends Component {
    state = {
        visible: false,
        active_dome: false,
        active_env: false,
        active_int: false,
        dome: false,
        environment: false,
        element: false,
        interaction: false,
    };

    async showModal(act_d, act_e, act_i, el, dom, env, inte) {
        await this.setState({
            visible: true,
            active_dome: act_d,
            active_env: act_e,
            active_int: act_i,
            dome: dom,
            environment: env,
            element: el,
            interaction: inte,
        });
    }

    handleOk = (e) => {
        this.setState({
            visible: false,
            active_dome: false,
            active_env: false,
            active_int: false,
            dome: false,
            environment: false,
            element: false,
            interaction: false,
        });
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
            active_dome: false,
            active_env: false,
            active_int: false,
            dome: false,
            environment: false,
            element: false,
            interaction: false,
        });
    };

    render() {
        const {
            visible,
            active_dome,
            active_env,
            active_int,
            dome,
            environment,
            element,
            interaction,
        } = this.state;

        return (
            <Container>
                <StyledTitle>Domes</StyledTitle>
                <StyledTitleDescription>
                    Interact with the data in immersive environments
                </StyledTitleDescription>
                <StyledRow gutter={8} type="flex" justify="center">
                    {DomeContent.map((element) => {
                        return (
                            <FrameContainer md={24} lg={8}>
                                <StyledIframe
                                    src={element.url}
                                    scrolling="off"
                                    name="iframe-scene"
                                >
                                    <p>Your browser does not support iframes.</p>
                                </StyledIframe>

                                <StyledFrameTitle>{element.title}</StyledFrameTitle>

                                <StyledFrameSubTitle>{element.subtitle}</StyledFrameSubTitle>

                                <Row type="flex" justify="center">
                                    <StyledFrameLinks
                                        onClick={() => {
                                            this.showModal(
                                                true,
                                                false,
                                                false,
                                                element,
                                                true,
                                                false,
                                                false
                                            );
                                        }}
                                    >
                                        Dome
                                    </StyledFrameLinks>
                                    <StyledFrameLinks
                                        onClick={() => {
                                            this.showModal(
                                                false,
                                                true,
                                                false,
                                                element,
                                                false,
                                                true,
                                                false
                                            );
                                        }}
                                    >
                                        Interaction
                                    </StyledFrameLinks>
                                    <StyledFrameLinks
                                        onClick={() => {
                                            this.showModal(
                                                false,
                                                false,
                                                true,
                                                element,
                                                false,
                                                false,
                                                true
                                            );
                                        }}
                                    >
                                        Environment
                                    </StyledFrameLinks>
                                </Row>
                            </FrameContainer>
                        );
                    })}
                </StyledRow>
                <Modal
                    title={null}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={null}
                    closable={false}
                    centered
                    width="80%"
                    bodyStyle={{ padding: "0", height: "80vh" }}
                >
                    {this.state.active_dome == false &&
                        this.state.active_env == true &&
                        this.state.active_int == false &&
                        this.state.element &&
                        this.state.dome == false &&
                        this.state.environment == true &&
                        this.state.interaction == false ? (
                        <IFrameContainer>
                            <StyledIframeDescriptionContainer>
                                <StyledIframeDescription>
                                    <StyledIframeDescriptionTitle>
                                        {this.state.element.title}
                                    </StyledIframeDescriptionTitle>
                                    <StyledIframeDescriptionContent>
                                        {this.state.element.content}
                                    </StyledIframeDescriptionContent>
                                </StyledIframeDescription>
                            </StyledIframeDescriptionContainer>
                            <StyledModalIframe
                                src={this.state.element.urlwb}
                                scrolling="off"
                                name="iframe-scene"
                            >
                                <p>Your browser does not support iframes.</p>
                            </StyledModalIframe>
                        </IFrameContainer>
                    ) : (
                        <div />
                    )}

                    {this.state.active_dome == true &&
                        this.state.active_env == false &&
                        this.state.active_int == false &&
                        this.state.element &&
                        this.state.dome == true &&
                        this.state.environment == false &&
                        this.state.interaction == false ? (
                        <IFrameContainer>
                            <StyledIframeDescriptionContainer>
                                <StyledIframeDescription>
                                    <StyledIframeDescriptionTitle>
                                        {this.state.element.title}
                                    </StyledIframeDescriptionTitle>
                                    <StyledIframeDescriptionContent>
                                        {this.state.element.content}
                                    </StyledIframeDescriptionContent>
                                </StyledIframeDescription>
                            </StyledIframeDescriptionContainer>
                            <StyledModalIframe
                                src={this.state.element.url}
                                scrolling="off"
                                name="iframe-scene"
                            >
                                <p>Your browser does not support iframes.</p>
                            </StyledModalIframe>
                        </IFrameContainer>
                    ) : (
                        <div />
                    )}

                    {this.state.active_dome == false &&
                        this.state.active_env == false &&
                        this.state.active_int == true &&
                        this.state.element &&
                        this.state.dome == false &&
                        this.state.environment == false &&
                        this.state.interaction == true ? (
                        <IFrameContainer>
                            <img
                                src={this.state.element.image}
                                width="100%"
                                height="auto"
                                display="block"
                            />
                        </IFrameContainer>
                    ) : (
                        <div />
                    )}
                </Modal>
            </Container>
        );
    }
}

export default Domes;
