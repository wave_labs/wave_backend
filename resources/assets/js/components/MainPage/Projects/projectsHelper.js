import styled from "styled-components";

export const ProjectImage = styled.img`
    width: 90%;
    display: block;
    margin: auto;
    min-width: 200px;
    margin-bottom: 100px;

    @media screen and (max-width: 1000px) {
    width: 96%;
    }
`;

export const Container = styled.div`
  width: 100%;
`;

const mediaPath = "/images/projects/";

export const mediaPaths = {
    largescale: mediaPath + "largescale/",
    interwhale: mediaPath + "interwhale/",
    intertagua: mediaPath + "intertagua/",
};

export const largescaleVideos = [
    {
        id: "ahab",
        title: "Cardboard Dome",
        description:
            "Timelapse video of the prototype constructed reusing cardboard thrown away from local businesses. The constructed dome is built based on pentagons and hexagons, creating a strong structure for 3D video projections.",
    },
    {
        id: "interaquatica",
        title: "Wooden Dome",
        description:
            "Through the concepts developed on the prototype, a wood dome was created as an augmentation base for ARDome and VRDome. Similarly to the prototype, the dome reused old pallets that were thrown away, combined with 3D printed junctions.",
    },
    {
        id: "ardome",
        title: "ARDome",
        description:
            "Built as a standalone ARCore mobile application, it leverages the Augmented Reality and allowing the mapping of the underwater cyber environment onto the geodesic dome structure. Participants enter into the experience in a quest to aid the endangered species.",
    },
    {
        id: "vrdome",
        title: "VRDome",
        description:
            "VRDome leverages the same underwater cyber enviroment onto the geodesic dome structure used previously on ARDome. For the experience a VR headseat was used, granting interaction alternatives between previous studies. ",
    },
];