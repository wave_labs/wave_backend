import React, { Fragment } from "react";
import { Row, Col } from "antd";
import styled from "styled-components";
import { StyledTitle } from "helpers";

const SubtitleContainer = styled.div`
  display: block;
  margin: auto;
  width: 60%;
  text-align: center;

  @media screen and (max-width: 1000px) {
    width: 80%;
  }
`;

const Subtitle = styled.h2`
  font-size: 20px;

  @media screen and (max-width: 1000px) {
    font-size: 18px;
  }
`;

const Reference = styled.div`
  font-size: 18px;
  color: #777777;

  @media screen and (max-width: 1000px) {
    font-size: 16px;
  }
`;

const Header = ({ title, subtitle, reference }) => {
  return (
    <Fragment>
      <StyledTitle>{title}</StyledTitle>
      <SubtitleContainer>
        <Subtitle>{subtitle}</Subtitle>
        <Reference>Ref: {reference}.</Reference>
      </SubtitleContainer>
    </Fragment>
  );
};

export default Header;
