import React from "react";
import { Row, Col } from "antd";
import styled from "styled-components";

const ProjectInfo = styled(Col)`
  text-align: center;
  margin: 40px auto;
  margin-bottom: 100px;

  h3 {
    font-size: 2em;
    font-weight: bold;
    color: #2e2e2e;
    margin: 0px;

    @media screen and (max-width: 1000px) {
      font-size: 1.2em;
    }
  }

  p {
    font-size: 1.6em;
    color: #777777;

    @media screen and (max-width: 1000px) {
      font-size: 1em;
    }
  }

  img {
    width: 10%;
    min-width: 150px;
  }
`;

const ProjectDetails = ({ funding, timeline, logo }) => {
  return (
    <Row type="flex" align="middle" justify="space-around">
      <ProjectInfo span={8}>
        <h3>EUR {funding}</h3>
        <p>Funding</p>
      </ProjectInfo>
      <ProjectInfo span={8}>
        <img src={logo} alt="logo" />
      </ProjectInfo>
      <ProjectInfo span={8}>
        <h3>{timeline}</h3>
        <p>Timeline</p>
      </ProjectInfo>
    </Row>
  );
};

export default ProjectDetails;
