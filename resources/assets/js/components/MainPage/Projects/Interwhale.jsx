import React, { Component } from "react";
import { mediaPaths, ProjectImage, Container } from "./projectsHelper";
import ProjectDetails from "./common/ProjectDetails";
import Header from "./common/Header";

class Interwhale extends Component {
  render() {
    return (
      <Container>
        <Header
          title="INTERWHALE"
          subtitle="Advancing Interactive Technology for Responsible Whale-Watching."
          reference="PTDC/CCI-COM/0450/2020"
        />

        <ProjectDetails
          funding="250k"
          timeline="2021 / 2024"
          logo={mediaPaths.interwhale + "logo.svg"}
        />

        <ProjectImage
          src={mediaPaths.interwhale + "interwhale.webp"}
          alt="interwhale"
        />
      </Container>
    );
  }
}

export default Interwhale;
