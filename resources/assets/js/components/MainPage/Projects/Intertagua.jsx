import React, { Component } from "react";
import { Row, Col } from "antd";
import styled from "styled-components";
import { StyledTitle } from "helpers";
import axios from "axios";
import { mediaPaths, ProjectImage, Container } from "./projectsHelper";
import ProjectDetails from "./common/ProjectDetails";
import Header from "./common/Header";

const StyledSubtitle = styled.h2`
  font-size: 3em;
  text-align: center;
  margin-bottom: 50px;
  font-weight: bold;

  @media screen and (max-width: 1000px) {
    font-size: 2.2em;
  }
`;

const DataValue = styled.div`
  margin-top: 0;
`;

const DataName = styled.div`
  font-weight: 700;
  margin-bottom: 0;
`;

const DataSubContainer = styled.div`
  margin: 5px 0px;
  font-size: 20px;

  @media screen and (max-width: 1000px) {
    margin: 1px 0px;
    font-size: 16px;
  }
`;

const StyledImage = styled.img`
  width: 100%;
`;

const DataContainer = styled(Row)`
  width: 80%;
  margin: auto;
`;

const DataRow = ({ name, value }) => {
  return (
    <DataSubContainer>
      <DataName>{name}:</DataName>
      <DataValue>{value}</DataValue>
    </DataSubContainer>
  );
};

class Intertagua extends Component {
  constructor(props) {
    super(props);
    let initialReport = {
      id: "-",
      id_device: "-",
      name_device: "-",
    };
    let latestLat = "-";
    let latestLon = "-";
    let lastUpdate = "-";
    this.state = {
      lastReport: initialReport,
      latestLat: latestLat,
      latestLon: latestLon,
      lastUpdate: lastUpdate,
      lastReportId: "",
    };
  }

  getLatestReport() {
    axios.get(`${window.location.origin}/api/reports`).then(({ data }) => {
      //get last report
      let lastReport = data.pop();

      //deafult values of fields
      let latestLat = "-";
      let latestLon = "-";
      let lastUpdate = "-";
      let lastReportId = this.state.lastReportId;

      if (lastReportId != lastReport.id) {
        //check if has lat and lon
        lastReport.fields.map((key, value) => {
          if (key.type == "Latitude") {
            latestLat = key.value;
          }

          if (key.type == "Longitude") {
            latestLon = key.value;
          }
        });

        //check if has collected_at
        if (lastReport.collected_at) {
          lastUpdate = lastReport.collected_at;
        }

        lastReportId = lastReport.id;
      } else {
        lastReport = {
          id: "-",
          id_device: "-",
          name_device: "-",
        };
      }

      this.setState({
        lastReport: lastReport,
        latestLat: latestLat,
        latestLon: latestLon,
        lastUpdate: lastUpdate,
        lastReportId: lastReportId,
      });
    });
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      this.getLatestReport();
    }, 5000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <Container>
        <Header
          title="INTERTAGUA"
          subtitle="Interfaces Aquáticas Interativas para Deteção e Visualização da
          Megafauna Marinha Atlântica e Embarcações na Macaronésia usando
          Marcadores Rádio-transmissores."
          reference="MAC2/1.1.a /385"
        />

        <ProjectDetails
          funding="408k"
          timeline="2019 / 2022"
          logo={mediaPaths.intertagua + "logo.png"}
        />

        <Row>
          <Col span={12}>
            <ProjectImage
              src={mediaPaths.intertagua + "intertagua.webp"}
              alt="intertagua"
            />
          </Col>
          <Col span={12}>
            <ProjectImage
              src={mediaPaths.intertagua + "intertagua2.webp"}
              alt="intertagua"
            />
          </Col>
        </Row>

        <StyledSubtitle>Latest from Sea</StyledSubtitle>

        <DataContainer type="flex" align="middle" justify="space-between">
          <Col span={14}>
            <DataRow
              name="Device Name"
              value={this.state.lastReport.name_device}
            />
            <DataRow name="Device ID" value={this.state.lastReport.id_device} />
            <DataRow name="Report ID" value={this.state.lastReport.id_device} />
            <DataRow name="Latitude" value={this.state.latestLat} />
            <DataRow name="Longitude" value={this.state.latestLon} />
            <DataRow name="Last Update" value={this.state.lastUpdate} />
          </Col>
          <Col span={10}>
            <StyledImage
              src={
                this.state.lastReport.name_device != "-"
                  ? "images/iot/turtle-icon.png"
                  : "images/iot/no-content-icon.png"
              }
            />
          </Col>
        </DataContainer>
      </Container>
    );
  }
}

export default Intertagua;
