import React, { Component } from "react";
import { Row } from "antd";
import styled from "styled-components";
import ReactPlayer from "react-player/lazy";
import {
  largescaleVideos,
  mediaPaths,
  ProjectImage,
  Container,
} from "./projectsHelper";
import ProjectDetails from "./common/ProjectDetails";
import Header from "./common/Header";

let observer;

const Product = styled.div`
  width: 96%;
  display: block;
  margin: auto;

  h2 {
    font-weight: bold;
    font-size: 2.7em;
    margin-bottom: 10px;

    @media (max-width: 1000px) {
      font-size: 2.2em;
    }
  }

  p {
    font-size: 1.2em;
    color: #777;

    @media (max-width: 1000px) {
      font-size: 1em;
    }
  }

  .video-container {
    margin-top: 10px;
    margin-bottom: 100px;
    width: 100% !important;
    height: 100% !important;
    border-radius: 6px;

    video {
      border-radius: inherit;
      width: 100% !important;
      height: auto !important;
      cursor: pointer;
      box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
    }
  }
`;

const ProductContainer = ({ title, description, id, playing }) => {
  return (
    <Product>
      <Row type="flex" align="middle" justify="space-between">
        <h2>{title}</h2>
        <p>{description}</p>
      </Row>
      <ReactPlayer
        id={id}
        playing={playing}
        loop={true}
        muted={true}
        className="video-container"
        url={"/storage/videos/largescale/" + id + ".webm"}
        controls
      />
    </Product>
  );
};

class Largescale extends Component {
  state = {
    ardome: false,
    vrdome: false,
    ahab: false,
    interaquatica: false,
  };

  componentDidMount() {
    this.createObserver();
  }

  createObserver = () => {
    let container = document.getElementsByClassName("video-container");

    let options = {
      root: null,
      rootMargin: "0px",
      threshold: 1.0,
    };

    observer = new IntersectionObserver(this.handleIntersect, options);
    for (let element of container) {
      observer.observe(element);
    }
  };

  handleIntersect = (entries, observer) => {
    entries.forEach((entry) => {
      let newEntry = {};
      if (this.state[entry.target.id] != undefined) {
        if (entry.isIntersecting) {
          newEntry[entry.target.id] = true;
        } else {
          newEntry[entry.target.id] = false;
        }

        this.setState({ ...newEntry });
      }
    });
  };

  handleVideoClick = (element) => {
    var newState = {};
    this.state[element]
      ? (newState[element] = undefined)
      : (newState[element] = true);

    this.setState(newState);
  };

  render() {
    return (
      <Container>
        <Header
          title="LARGESCALE"
          subtitle="Location-based Augmented Reality Gadgets and Environment-friendly
            Sightseeing of Cultural Attractions for Locals and Excursionists."
          reference="FCT32474"
        />

        <ProjectDetails
          funding="250k"
          timeline="2018 / 2021"
          logo={mediaPaths.largescale + "logo.png"}
        />

        <ProjectImage
          src={mediaPaths.largescale + "largescale.webp"}
          alt="largescale"
        />

        {largescaleVideos.map((video, index) => {
          return (
            <div key={video.id} onClick={() => this.handleVideoClick(video.id)}>
              <ProductContainer
                id={video.id}
                playing={this.state[video.id] && true}
                title={video.title}
                description={video.description}
                order={index % 2}
              />
            </div>
          );
        })}
      </Container>
    );
  }
}

export default Largescale;
