import React, { Component } from "react";
import styled from "styled-components";
import { Form, Button, Row, Col } from "antd";
import { Link } from "react-router-dom";
import { dimensions } from "helpers";

const FormItem = Form.Item;

const StyledLoginButton = styled(Button)`
  width: 100%;
`;

const Container = styled(Row)`
  width: 85%;
  margin: auto;
  box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.1);
  border-radius: 6px;

  @media (max-width: ${dimensions.md}) {
    width: 95%;
  }
`;
const Title = styled.div`
  h1 {
    margin-bottom: 0;
  }

  p {
    margin-bottom: 40px;
    color: #777;
  }
`;

const LinkContainer = styled.div`
  font-size: 0.9rem;
  text-align: center;
  margin: auto;
  display: block;
  text-decoration: none;
  margin-top: 20px;
`;

const RegisterLink = styled(Link)`
  font-weight: bold;
  color: black;

  &:hover {
    color: #777777;
  }
`;
const SectionContainer = styled(Col)`
  background: ${(props) => (props.background ? "#e1f0ff" : "none")};
  border-radius: ${(props) =>
    props.registration ? "0px 6px 6px 0px" : "6px 0px 0px 6px"};
  padding: 40px !important;
  display: ${(props) => (props.background ? "flex !important" : "block")};

  @media (max-width: ${dimensions.md}) {
    border-radius: 6px 6px 0px 0px;
  }
`;
const Image = styled.img`
  width: 100%;
  height: auto;
  margin: auto;
  display: block;

  @media (max-width: ${dimensions.md}) {
    width: 50%;
  }
`;

const RecoverTemplate = ({ image, children, title, subtitle }) => {
  return (
    <Container type="flex">
      <SectionContainer
        background={1}
        xs={{ order: 1, span: 24 }}
        md={{ order: 2, span: 10 }}
      >
        <Image src={"/images/" + image} />
      </SectionContainer>
      <SectionContainer
        xs={{ order: 2, span: 24 }}
        md={{ order: 2, span: 14 }}
      >
        <Title>
            <div>
              <h1>{title}</h1>
              <p>
                {subtitle}
              </p>
            </div>
        </Title>
        {children}

      </SectionContainer>
    </Container>
  );
};

export default RecoverTemplate;
