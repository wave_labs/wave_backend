import React, { Component } from "react";
import { Row, Button, List, Col } from "antd";
import styled from "styled-components";
import { StyledTitle } from "helpers";
import { NavLink } from "react-router-dom";
import { Card } from "antd";
import "./css/Style.css";
import { GalleryContent, VacanciesContent } from "../helpers/joinUsHelpers";
import ModalImage from "../Common/ModalImage";

const Container = styled.div`
  width: 90%;
  margin: auto;
  display: block;
`;

const StyledSubTitle = styled.h2`
  font-size: 36px;
  text-align: center;
  font-weight: bold;
  line-height: 1.3;

  @media screen and (max-width: 1000px) {
    font-size: 24px;
  }
`;

const StyledText = styled.div`
  font-size: 1rem;
  color: #777777;
  margin-left: 2%;
  margin-bottom: 2%;
`;

const Redirect = styled.h2`
  font-size: 1.7rem;
  text-align: center;
  margin-top: 2rem;
  font-weight: bold;
`;

const StyledCard = styled.div`
  margin: 20px auto !important;
  width: 30%;
  min-width: 250px;
  cursor: pointer;
`;

const CardImage = styled.div`
  height: 230px;
  width: 100%;
  background: url(${(props) => props.src});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

const CardTitle = styled.h3`
  font-weight: bold;
  color: black;
  margin: 10px 0 0 0;
`;
const CardDescription = styled.p`
  color: #777777;
`;

const StyledTitleDescription = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
`;

const StyledLink = styled.a`
  color: #777777;
  font-size: 1rem;

  &:hover {
    color: black;
  }
`;

const { Meta } = Card;

class JoinUs extends Component {
  state = { visible: false };

  handleModal = (image = null) => {
    this.setState({
      visible: image && true,
      currentImage: image ? image : null,
    });
  };

  render() {
    let { currentImage, visible } = this.state;
    return (
      <Container>
        <StyledTitle>Join</StyledTitle>
        <StyledTitleDescription>
          Collect, Analyze, Predict, Design, Develop, Research
        </StyledTitleDescription>
        <StyledTitleDescription style={{ marginBottom: "5%" }}>
          Impact climate change with the marine data.
        </StyledTitleDescription>

        <StyledSubTitle>Mission</StyledSubTitle>
        <StyledText>
          We want to empower everyone with knowledge about ocean, and understand
          the human impact on biodiversity. We believe that by equipping people
          with the best tools to understand the ocean, we can solve the climate
          change, together.
        </StyledText>

        <StyledSubTitle>Vacancies</StyledSubTitle>
        <StyledText>
          <List
            itemLayout="vertical"
            dataSource={VacanciesContent}
            renderItem={(item) => (
              <List.Item>
                <ul>
                  <StyledLink href={item.link} target="_blank">
                    <li>{item.title}</li>
                  </StyledLink>
                </ul>
              </List.Item>
            )}
          />
        </StyledText>

        <br />

        <StyledSubTitle>Life at Wave</StyledSubTitle>

        <ModalImage
          src={currentImage}
          visible={visible}
          handleModal={this.handleModal}
        />

        <Row type="flex" justify="space-around" align="top">
          {GalleryContent.map((item) => (
            <StyledCard onClick={() => this.handleModal(item.image)}>
              <CardImage src={item.image} />
              <CardTitle>{item.title}</CardTitle>
              <CardDescription>{item.description}</CardDescription>
            </StyledCard>
          ))}
        </Row>

        <Redirect>If you have any doubt, make sure to check</Redirect>
        <Row gutter={4} type="flex" justify="center">
          <Col span={3}>
            <NavLink to="/team">
              <Button ghost="true" size="large" type="primary">
                Team
              </Button>
            </NavLink>
          </Col>
          <Col span={3}>
            <NavLink to="/about">
              <Button ghost="true" size="large" type="primary">
                About
              </Button>
            </NavLink>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default JoinUs;
