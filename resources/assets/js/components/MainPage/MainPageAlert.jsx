import React, { Component } from "react";
import { Alert } from "antd";
import { connect } from "react-redux";
import { isEmpty } from "underscore";
import styled from "styled-components";
import { alertActions } from "redux-modules/alert/actions";

const StyledAlert = styled(Alert)`
  width: 100%;
`;

const AlertContainer = styled.div`
  min-width: 300px;
  max-width: 500px;
  position: fixed;
  right: 5%;
  top: 100px;
  z-index: 1001;
`;
class MainPageAlert extends Component {
    handleClose = () => {
        this.props.clearAlert();
    };

    componentDidUpdate(prevProps) {
        if (!isEmpty(this.props.alert)) {
            setTimeout(() => {
                this.handleClose();
            }, 5000);
        }
    }

    render() {
        return (
            <AlertContainer>
                {!isEmpty(this.props.alert) && (
                    <StyledAlert
                        message={this.props.alert.message}
                        description={
                            Array.isArray(this.props.alert.description)
                                ? this.props.alert.description.map((description, index) => (
                                    <p key={index}>{description}</p>
                                ))
                                : this.props.alert.description
                        }
                        type={this.props.alert.type}
                        showIcon
                        onClose={this.handleClose}
                        closable
                    />
                )}
            </AlertContainer>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        clearAlert: () => dispatch(alertActions.clear()),
    };
};

const mapStateToProps = (state) => {
    return {
        alert: state.alert,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MainPageAlert);
