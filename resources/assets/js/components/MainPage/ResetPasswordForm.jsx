import React, { Component, Fragment } from "react";
import styled from "styled-components";
import { Form, Input, Row, Button } from "antd";
import { resetPassword } from "redux-modules/auth/actions";
import { connect } from "react-redux";
import Logo from "../Common/TitleImage";
import RecoverTemplate from "./RecoverTemplate";

const FormItem = Form.Item;

const SubmitButton = styled(Button)`
  width: 100%;
`;

const StyledResetButton = styled(Button)`
  width: 100%;
`;

const StyledForm = styled(Form)`
  max-width: 300px;
  width: 100%;
`;

class ResetPasswordForm extends Component {
  state = {
    confirmDirty: false,
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;

    if (value && value !== form.getFieldValue("password")) {
      callback("The passwords don't match!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;

    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  handleSubmit = (e) => {
    const form = this.props.form;
    const { resetPassword } = this.props;
    e.preventDefault();

    this.props.form.validateFieldsAndScroll((err, data) => {
      if (!err) {
        this.setState({ submitting: true });

        resetPassword({
          ...data,
          token: this.props.match.params.token,
        }) /*.then( () => this.setState({submitting: false}))*/;
      }
    });
  };

  rules = {
    confirmPassword: [
      {
        required: true,
        message: "Please confirm your password!",
      },
      {
        validator: this.compareToFirstPassword,
      },
    ],
    password: [
      {
        required: true,
        message: "Please input your password!",
      },
      {
        min: 6,
        message: "Your password must have atleast 6 characters",
      },
      {
        validator: this.validateToNextPassword,
      },
    ],
    email: [
      {
        required: true,
        type: "email",
        message: "This is not a valid email!",
      },
      {
        message: "Please input your E-mail!",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <RecoverTemplate
        image="login.svg"
        title={"Change Password"}
        subtitle={"The ocean will once again remember your credentials"}
      >
        <Fragment>
          <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
            <FormItem hasFeedback label="E-mail">
              {getFieldDecorator("email", { rules: this.rules.email })(
                <Input placeholder="E-mail" />
              )}
            </FormItem>
            <FormItem hasFeedback label="Password">
              {getFieldDecorator("password", { rules: this.rules.password })(
                <Input type="password" placeholder="Password" />
              )}
            </FormItem>
            <FormItem hasFeedback label="Confirm Password">
              {getFieldDecorator("password_confirmation", {
                rules: this.rules.confirmPassword,
              })(
                <Input
                  type="password"
                  placeholder="Confirm Password"
                  onBlur={this.handleConfirmBlur}
                />
              )}
            </FormItem>
            <SubmitButton
              loading={this.state.submitting}
              type="primary"
              htmlType="submit"
            >
              Reset Password
            </SubmitButton>
          </Form>
        </Fragment>
      </RecoverTemplate>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetPassword: (data) => dispatch(resetPassword(data)),
  };
};

export default Form.create()(
  connect(null, mapDispatchToProps)(ResetPasswordForm)
);
