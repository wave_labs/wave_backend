import React, { Component } from "react";
import styled from "styled-components";
import {
  StyledTitle,
  WhaleReporterUrl,
  DiveReporterUrl,
  LitterReporterUrl,
  ARDomeUrl,
} from "helpers";
import "./css/Style.css";

const Container = styled.div`
  width: 100%;
`;

const KitsTitle = styled.h2`
  font-size: 1.5rem;
  text-align: center;
  @media screen and (max-width: 1000px) {
    text-align: center;
    font-size: 20px;
  }
`;

const StyledKit = styled.img`
  width: 70%;
  display: block;
  border-radius: 20px;
  margin-top: 2%;
  margin-bottom: 5%;
  -webkit-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);

  @media screen and (max-width: 1000px) {
    width: 500px;
  }

  @media screen and (max-width: 500px) {
    width: 300px;
  }
`;

const StyledTitleDescription = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
  margin-bottom: 50px;
`;

const StyledAppDescription = styled.div`
  font-size: 20px;
  color: #777777;
  text-align: center;
  text-align: justify;
  margin-bottom: 50px;
  margin-left: 15%;
  margin-right: 15%;

  @media screen and (max-width: 500px) {
    font-size: 15px;
  }
`;

class Apps extends Component {
  render() {
    return (
      <Container>
        <StyledTitle>Apps</StyledTitle>
        <StyledTitleDescription>
          Collect the data with citizen science
        </StyledTitleDescription>

        <div>
          <div type="flex" justify="space-around" align="middle">
            <KitsTitle>Whale Reporter</KitsTitle>
            <a
              href={WhaleReporterUrl}
              target="_blank"
              rel="noopener noreferrer"
            >
              <StyledKit src="/images/kits/whale.png" />
            </a>
            <StyledAppDescription>
              Building on the iconic value of whales and the economic impact of
              whale watching as a form of ecotourism, we developed a system
              which supports on-shore and off-shore whale watching, offering an
              unobtrusive way to spot cetaceans to greater audiences. Here, we
              compare whale watchers from sea vessels and land viewpoints, and
              we report on a mixed analysis of usability and environmental
              awareness.
            </StyledAppDescription>
          </div>

          <div type="flex" justify="space-around" align="middle">
            <KitsTitle>Dive Reporter</KitsTitle>
            <a href={DiveReporterUrl} target="_blank" rel="noopener noreferrer">
              <StyledKit src="/images/kits/dive.png" />
            </a>
            <StyledAppDescription>
              Tablet application with scuba divers as citizen scientists for
              post-dive surveys. We collect the abundance/scarcity of given
              marine taxa and ask divers to estimate their total number. We
              provide a pilot study with the collected data and discuss the
              usability of the application with the divers. Our application will
              be used for longitudinal studies necessary to marine biologists.
            </StyledAppDescription>
          </div>

          <div type="flex" justify="space-around" align="middle">
            <KitsTitle>Litter Reporter</KitsTitle>
            <a
              href={LitterReporterUrl}
              target="_blank"
              rel="noopener noreferrer"
            >
              <StyledKit src="/images/kits/litter.png" />
            </a>
            <StyledAppDescription>
              Mobile application with tourists as citizen scientists for
              reporting marine litter. Using gamification, we motivate users to
              geotag beached and floating marine litter. Application will be
              used for longitudinal studies necessary to marine biologists.
            </StyledAppDescription>
          </div>

          <div type="flex" justify="space-around" align="middle">
            <KitsTitle>ARDome</KitsTitle>
            <a href={ARDomeUrl} target="_blank" rel="noopener noreferrer">
              <StyledKit src="/images/kits/ardome.png" />
            </a>
            <StyledAppDescription>
              Built as a standalone ARCore mobile application, it leverages the
              Augmented Reality and allowing the mapping of the underwater cyber
              environment onto the geodesic dome structure. Participants enter
              into the experience in a quest to aid the endangered species.
            </StyledAppDescription>
          </div>
        </div>
      </Container>
    );
  }
}

export default Apps;
