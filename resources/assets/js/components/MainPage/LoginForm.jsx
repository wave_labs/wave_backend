import React, { Component } from "react";
import styled from "styled-components";
import { login } from "redux-modules/auth/actions";
import { Form, Icon, Input, Button, Checkbox, Row, Col } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { dimensions } from "helpers";
import AuthTemplate from "./AuthTemplate";
import { Fragment } from "react";

const FormItem = Form.Item;

const SubmitButton = styled(Button)`
  width: 100%;
`;

const Container = styled(Row)`
  width: 80%;
  margin: auto;
  box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.1);
  border-radius: 6px;

  @media (max-width: ${dimensions.md}) {
    width: 95%;
  }
`;


const LinkContainer = styled.div`
  font-size: 0.9rem;
  text-align: center;
  margin: auto;
  display: block;
  text-decoration: none;
  margin-top: 40px;
`;

const RegisterLink = styled(Link)`
  font-weight: bold;
  color: black;

  &:hover {
    color: #777777;
  }
`;

class LoginForm extends Component {
  state = {
    submitting: false,
  };

  rules = {
    email: [
      {
        required: true,
        message: "Please input your email!",
      },
      {
        type: "email",
        message: "Please input a valid email!",
      },
    ],
    password: [
      {
        required: true,
        message: "Please input your password!",
      },
    ],
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { login } = this.props;

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({ submitting: true });
        login(values).catch(() => this.setState({ submitting: false }));
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <AuthTemplate image="login.svg" registration={false}>
        <Fragment>
          
          <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
            <FormItem hasFeedback>
              {getFieldDecorator("email", { rules: this.rules.email })(
                <Input
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.5)" }} />
                  }
                  placeholder="Email"
                />
              )}
            </FormItem>

            <FormItem hasFeedback>
              {getFieldDecorator("password", {
                rules: this.rules.password,
              })(
                <Input
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.5)" }} />
                  }
                  type="password"
                  placeholder="Password"
                />
              )}
            </FormItem>

            <FormItem>
              {getFieldDecorator("remember", {
                valuePropName: "checked",
                initialValue: true,
              })(<Checkbox>Remember me</Checkbox>)}
            </FormItem>

            <SubmitButton
              loading={this.state.submitting}
              type="primary"
              htmlType="submit"
            >
              Log in
            </SubmitButton>
          </Form>
        </Fragment>
      </AuthTemplate>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: (user) => dispatch(login(user)),
  };
};

export default Form.create()(
  connect(
    null,
    mapDispatchToProps
  )(LoginForm)
);
