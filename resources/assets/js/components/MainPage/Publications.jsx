import React from "react";
import { Row, Col, Button, List } from "antd";
import styled from "styled-components";
import { StyledTitle, AppsPublicationsContent, IotsPublicationsContent, DomesPublicationsContent } from "helpers";
import { Link } from "react-router-dom";

const EditedTitle = styled(StyledTitle)`
  @media screen and (max-width: 400px) {
    font-size: 40px;
  }
`;
const StyledTitleDescription = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
  margin-bottom: 50px;
`;

const StyledSubTitle = styled.h2`
  font-size: 1.1rem;
  margin-top: 3%;

  @media screen and (max-width: 400px) {
    font-size: 0.9rem;
  }
`;

const Container = styled.div`
  width: 80%;
  margin-bottom: 5%;
  margin: auto;

  @media screen and (max-width: 1200px) {
    width: 90%;
    text-align: center;
  }
`;

const Authors = styled.div`
  font-size: 0.8rem;
  color: #777777;
`;

const Conferences = styled.div`
font-size: 0.8rem;
color: #777777;
font-style: italic;
`;

const StyledButton = styled(Button)`
  margin-top: 2%;
`;

const StyledColumn = styled(Col)`
  margin: 4%;
`;

const StyledRow = styled(Row)`
  margin: 1% 3%;
`;

const StyledImage = styled.img`
  width: 100%;
  min-width: 50px;
  max-width: 300px;
  margin: auto;
  display: block;

  @media screen and (max-width: 400px) {
    width: 70%;
  }
`;

const TextDescription = styled(Link)`
  font-size: 1rem;
  display: block;
  text-decoration: none;
  color: black;

  &:hover {
    color: #777777;
  }

  @media screen and (max-width: 1200px) {
    display: flex;
    justify-content: center;
  }
`;

const AdaptedTitle = styled(StyledTitle)`
  margin-top: 3%;
  margin-bottom: 0;
`;


const Publications = () => {
  return (
    <Container>
      <EditedTitle>Publications</EditedTitle>
      <StyledTitleDescription>Double-blind Peer Reviewed Research Articles</StyledTitleDescription>
      
      <AdaptedTitle name="apps" id="apps">
        Apps
      </AdaptedTitle>
      <StyledTitleDescription>
        Collect the data with citizen science
      </StyledTitleDescription>
      <List
        itemLayout="horizontal"
        dataSource={AppsPublicationsContent}
        renderItem={item => (
          <List.Item
            style={{
              borderBottom: 0,
              borderBottomColor: "transparent",
              padding: 0
            }}
          >
            <StyledRow type="flex" align="middle">
              <StyledColumn xs={24} lg={8}>
                <StyledImage src={item.image} />
              </StyledColumn>
              <Col xs={24} lg={14}>
                <StyledSubTitle>{item.title}</StyledSubTitle>
                <Authors>
                  <p>{item.authors}</p>
                </Authors>
                <Conferences>
                  <p>{item.conference}</p>
                </Conferences>
                <TextDescription to={item.file} target="_blank" download>
                  <StyledButton ghost="true" size="large" type="primary">
                    Download
                  </StyledButton>
                </TextDescription>
              </Col>
            </StyledRow>
          </List.Item>
        )}
      />
      
      <AdaptedTitle name="iot" id="iot">
        IoT
      </AdaptedTitle>
      <StyledTitleDescription>
        Interpret the data using AI
      </StyledTitleDescription>
      <List
        itemLayout="horizontal"
        dataSource={IotsPublicationsContent}
        renderItem={item => (
          <List.Item
            style={{
              borderBottom: 0,
              borderBottomColor: "transparent",
              padding: 0
            }}
          >
            <StyledRow type="flex" align="middle">
              <StyledColumn xs={24} lg={8}>
                <StyledImage src={item.image} />
              </StyledColumn>
              <Col xs={24} lg={14}>
                <StyledSubTitle>{item.title}</StyledSubTitle>
                <Authors>
                  <p>{item.authors}</p>
                </Authors>
                <Conferences>
                  <p>{item.conference}</p>
                </Conferences>
                <TextDescription to={item.file} target="_blank" download>
                  <StyledButton ghost="true" size="large" type="primary">
                    Download
                  </StyledButton>
                </TextDescription>
              </Col>
            </StyledRow>
          </List.Item>
        )}
      />

      <AdaptedTitle name="domes" id="domes">
        Domes
      </AdaptedTitle>
      <StyledTitleDescription>
        Interact with the data in immersive environments
      </StyledTitleDescription>

      <List
        itemLayout="horizontal"
        dataSource={DomesPublicationsContent}
        renderItem={item => (
          <List.Item
            style={{
              borderBottom: 0,
              borderBottomColor: "transparent",
              padding: 0
            }}
          >
            <StyledRow type="flex" align="middle">
              <StyledColumn xs={24} lg={8}>
                <StyledImage src={item.image} />
              </StyledColumn>
              <Col xs={24} lg={14}>
                <StyledSubTitle>{item.title}</StyledSubTitle>
                <Authors>
                  <p>{item.authors}</p>
                </Authors>
                <Conferences>
                  <p>{item.conference}</p>
                </Conferences>
                <TextDescription to={item.file} target="_blank" download>
                  <StyledButton ghost="true" size="large" type="primary">
                    Download
                  </StyledButton>
                </TextDescription>
              </Col>
            </StyledRow>
          </List.Item>
        )}
      />
    </Container>
  );
};

export default Publications;
