import React, { Component } from "react";
import { Row} from "antd";
import styled from "styled-components";
import { StyledTitle } from "helpers";

const Container = styled.div`
  width: 100%;
`;

const StyledTitleDescription = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
  margin-bottom: 50px;
`;

const StyledImgApps = styled.img`
  width: 150px;
  
  margin-bottom: 5%;
  @media screen and (max-width: 1000px) {
    width: 100px;
  }

  @media screen and (max-width: 500px) {
    width: 60px;
  }
`;

const StyledParagraph = styled.div`
  font-size: 16px;
  display: block;
  text-align: center;
  font-weight: bold;

  @media screen and (max-width: 1000px) {
    display: none;
  }
`;

class IoT extends Component {
  
  render() {
    
    return (
      <Container>
        <StyledTitle>IoT</StyledTitle>
        <StyledTitleDescription>
            Interpret the data using AI
        </StyledTitleDescription>

        <Row
            style={{ margin: "5% 0%" }}
            type="flex"
            justify="space-around"
            align="middle"
          >
            <div>
              <StyledImgApps src="/images/kits/logo/buoy.svg" />
              <StyledParagraph>Poseidon</StyledParagraph>
            </div>
            <div>
              <StyledImgApps src="/images/kits/logo/submarine.svg" />
              <StyledParagraph>SeaMote</StyledParagraph>
            </div>
            <div>
              <StyledImgApps src="/images/kits/logo/tag.svg" />
              <StyledParagraph>Triton</StyledParagraph>
            </div>
          </Row>
      
      </Container>
    );
  }
}

export default IoT;