import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Checkbox } from "antd";
import styled from "styled-components";
import { fetchSightingsCoord } from "redux-modules/sighting/actions";
import { fetchIndex } from "redux-modules/coordinate/actions";
import {
  fetchMarkerCoordinates,
  resetMarkerCoordinates,
} from "redux-modules/marker/actions";
import ReportsMapList from "./LocationMap/ReportsMapList";

const iconPath = "/storage/images/map-markers/";
const StyledRow = styled(Row)`
  margin: 1rem;
`;

const StyledMap = styled.div`
  width: 96%;
  margin: auto;
  -webkit-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  margin-bottom: 5%;
  min-height: 600px;
`;

const LabelIcon = styled.img`
  height: 16px;
  margin-bottom: 4px;
`;

class MapFilters extends Component {
  state = {
    filters: {
      litter: true,
      dive: true,
      sighting: true,
      marker: true,
    },
  };

  componentDidMount() {
    this.props.fetchMarkerCoordinates();
    this.props.fetchIndex({
      ...this.state.filters,
    });
  }

  onMarkerChange = () => {
    !this.state.filters.marker
      ? this.props.fetchMarkerCoordinates()
      : this.props.resetMarkerCoordinates();

    this.setState({
      filters: {
        ...this.state.filters,
        marker: !this.state.filters.marker,
      },
    });
  };

  onFilterChange = (e) => {
    let { filters } = this.state;
    filters[e] = !this.state.filters[e];
    this.setState({ filters });

    this.props.fetchIndex({
      ...filters,
    });
  };

  render() {
    let { filters } = this.state;
    return (
      <div>
        <StyledRow type="flex" justify="center">
          <Checkbox
            onChange={() => this.onFilterChange("litter")}
            checked={filters.litter}
          >
            <LabelIcon src={iconPath + "litterHeat.png"} />
            Litter Reporter
          </Checkbox>
          <Checkbox
            onChange={() => this.onFilterChange("sighting")}
            checked={filters.sighting}
          >
            <LabelIcon src={iconPath + "sightingheat.png"} />
            Whale Reporter
          </Checkbox>
          <Checkbox
            onChange={() => this.onFilterChange("dive")}
            checked={filters.dive}
          >
            <LabelIcon src={iconPath + "divingheat.png"} />
            Dive Reporter
          </Checkbox>

          <Checkbox onChange={this.onMarkerChange} checked={filters.marker}>
            <LabelIcon src={iconPath + "markerheat.svg"} />
            Spotfin burrfish
          </Checkbox>
        </StyledRow>
        <StyledMap>
          <ReportsMapList filters={filters} size="600" />
        </StyledMap>
      </div>
    );
  }
}

export default connect(
  null,
  {
    fetchSightingsCoord,
    fetchIndex,
    fetchMarkerCoordinates,
    resetMarkerCoordinates,
  }
)(MapFilters);
