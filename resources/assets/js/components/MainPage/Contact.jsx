import React from "react";
import { Row, Col, Icon } from "antd";
import styled from "styled-components";
import ContactForm from "./ContactForm";
import MapList from "./LocationMap/MapList";
import { alertActions } from "redux-modules/alert/actions";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { StyledTitle } from "helpers";

const StyledIcon = styled(Icon)`
  font-size: 20px;
  padding: 5px;
`;

const StyledDescription = styled.p`
  font-size: 1rem;
  margin: auto;
  display: block;
  padding-left: 10px;
`;

const StyledMapRow = styled(Row)`
  width: 96%;
  position: relative;
  margin-right: auto;
  margin-left: auto;
  -webkit-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  margin-bottom: 20px;
  min-height: 600px;

  &.ant-row {
    margin-right: auto;
    margin-left: auto;
  }
`;

const StyledContactRow = styled(Row)`
  width: 70%;
  margin: auto;
  max-width: 500px;

  @media screen and (max-width: 768px) {
    width: 90%;
  }
`;

const Container = styled.div`
  padding-left: 10%;
  padding-right: 10%;

  @media screen and (max-width: 767px) {
    padding-left: 5%;
    padding-right: 5%;
  }
  @media screen and (max-width: 1200px) {
    padding-left: 5%;
    padding-right: 5%;
  }
`;

const StyledLinkDescription = styled.h2`
  font-size: 1.7rem;
  width: 80%;
  color: black;
  margin-top: 5%;
  display: inline-block;
`;

const StyledNavLink = styled(NavLink)`
  text-decoration: underline;
  font-weight: bold;
  color: black;
  margin-left: 5px;

  &:hover {
    text-decoration: underline;
    color: black;
  }
`;

const AppItem = styled(Col)`
  padding: 0 10px;
  align-items: center;
  display: flex;
  flex-direction: column;
  gap: 10px;
  margin: 15px 0;

  img {
    width: 60%;
    max-width: 140px;
  }

  h4 {
    width: 100%;
    font-family: Aquatico Regular;
    font-size: 1.125rem;
    text-align: center;
    margin: 0;
  }
`;

class Contact extends React.Component {
  handleNewSuggestion() {
    this.success({
      message: "Success",
      description: "Thank you for your feedback!",
    });

    setTimeout(() => {
      this.clearAlert();
    }, 5000);
  }

  render() {
    return (
      <Row justify="center" align="middle">
        <div>
          <StyledTitle>Contact Us</StyledTitle>
          <Container>
            <Row gutter={48}>
              <Col sm={24} md={24}>
                <div>
                  <Row>
                    <StyledDescription>
                      <StyledIcon type="phone" />
                      +351 291 721 006
                    </StyledDescription>
                    <StyledDescription>
                      <StyledIcon type="hourglass" />
                      Monday - Friday 8:00 AM - 6:00PM (GMT+1)
                    </StyledDescription>
                    <StyledDescription>
                      <a href="mailto:team@wave-labs.org">
                        <StyledIcon type="mail" />
                      </a>
                      team@wave-labs.org
                    </StyledDescription>
                    <StyledDescription>
                      <StyledIcon type="pushpin" />
                      Polo Cientifico e Tecnologico da Madeira, floor -2,
                      Caminho da Penteada
                    </StyledDescription>
                  </Row>

                  <br />
                  <br />

                  <StyledMapRow>
                    <MapList size="600" />
                  </StyledMapRow>
                </div>

                <Row sm={24} md={24}>
                  <Col sm={12} md={6}>
                    <AppItem>
                      <img
                        src="/storage/images/logo/whale-reporter"
                        alt="whale reporter"
                      />
                      <div>
                        <h4>WHALE</h4>
                        <h4>REPORTER</h4>
                      </div>
                    </AppItem>
                  </Col>
                  <Col sm={12} md={6}>
                    <AppItem>
                      <img
                        src="/storage/images/logo/dive-reporter"
                        alt="dive reporter"
                      />
                      <div>
                        <h4>DIVE</h4>
                        <h4>REPORTER</h4>
                      </div>
                    </AppItem>
                  </Col>
                  <Col sm={12} md={6}>
                    <AppItem>
                      <img
                        src="/storage/images/logo/litter-reporter"
                        alt="litter reporter"
                      />
                      <div>
                        <h4>LITTER</h4>
                        <h4>REPORTER</h4>
                      </div>
                    </AppItem>
                  </Col>

                  <Col sm={12} md={6}>
                    <AppItem>
                      <img src="/storage/images/logo/ardome" alt="ardome" />
                      <h4>ARDOME</h4>
                    </AppItem>
                  </Col>
                </Row>
              </Col>

              <Col sm={24} md={24}>
                <center>
                  <StyledDescription>
                    Fill out the form below and we'll get back to you shortly
                  </StyledDescription>
                </center>

                <StyledContactRow type="flex" justify="center" align="middle">
                  <ContactForm
                    handleNewSuggestion={this.handleNewSuggestion}
                    success={this.props.success}
                    clearAlert={this.props.clearAlert}
                  />
                </StyledContactRow>
              </Col>
            </Row>
          </Container>

          <center>
            <StyledLinkDescription>
              Or, find your answers here
              <StyledNavLink to="/faq">FAQ</StyledNavLink>
            </StyledLinkDescription>
          </center>
        </div>
      </Row>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    success: (message) => dispatch(alertActions.success(message)),
    clearAlert: () => dispatch(alertActions.clear()),
  };
};

export default connect(null, mapDispatchToProps)(Contact);
