import React from "react";
import { Row, Col, Icon } from "antd";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { StyledTitle } from "helpers";

const StyledSubTitle = styled.h2`
  font-size: 1.3rem;
  color: #777777;
  width: 60%;
  margin: auto;

  @media screen and (max-width: 1200px) {
    width: 90%;
  }
`;

const StyledDescription = styled(StyledSubTitle)`
  font-size: 1.2rem;
  width: 80%;
  margin: auto;
  color: black;
  margin-top: 5%;
`;

const StyledEmail = styled.a`
  font-size: 1.2rem;
  width: 80%;
  margin: auto;
  color: black;
  text-decoration: underline;
  font-weight: bold;

  @media screen and (max-width: 1200px) {
    width: 100%;
  }

  &:hover {
    text-decoration: underline;
    color: black;
  }
`;

const Container = styled.div`
  width: 100%;
  margin-bottom: 5%;
  text-align: center;
`;

const TextDescription = styled(Link)`
  font-size: 1rem;
  display: block;
  text-decoration: none;
  color: black;

  &:hover {
    color: #777777;
  }

  @media screen and (min-width: 1200px) {
    text-align: left;
  }
`;

const StyledRow = styled(Row)`
  margin: 5%;
`;

const StyledImg = styled.img`
  width: 70%;

  @media screen and (max-width: 500px) {
    width: 90%;
  }

  @media screen and (max-width: 1000px) {
    width: 40%;
    min-width: 250px;
    margin: auto;
    display: block;
  }

  @media screen and (min-width: 999px) {
    float: right;
  }
`;

const MediaKit = () => {
  return (
    <Container>
      <StyledTitle>Media & Press Kits</StyledTitle>
      <StyledSubTitle>
        Brand assets and guidelines for you to use in any digital platforms for
        display purposes.
      </StyledSubTitle>

      <StyledRow type="flex" align="middle" gutter={48}>
        <Col xs={24} lg={14}>
          <StyledImg src="/images/media.png" />
        </Col>
        <Col xs={24} lg={10}>
          <TextDescription to="/wave.png" target="_blank" download>
            <Icon type="paper-clip" /> wave-logo.png
          </TextDescription>
          <TextDescription to="/wave.png" target="_blank" download>
            <Icon type="paper-clip" /> wave-photos.zip
          </TextDescription>
          <TextDescription
            to="/download/wave-publications.zip"
            target="_blank"
            download
          >
            <Icon type="paper-clip" /> wave-publications.zip
          </TextDescription>
        </Col>
      </StyledRow>

      <StyledDescription>
        We're happy to help with any other questions you may have.
      </StyledDescription>

      <StyledEmail href="mailto:team@wave-labs.org">
        team@wave-labs.org
      </StyledEmail>
    </Container>
  );
};

export default MediaKit;
