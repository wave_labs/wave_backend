import React, { Component } from "react";
import { Row, Col, Form, Input, Button, Select } from "antd";
import { alertActions } from "redux-modules/alert/actions";
import styled from "styled-components";
import axios from "axios";

const FormItem = Form.Item;
const Option = Select.Option;
const TextArea = Input.TextArea;

const StyledForm = styled(Form)`
  width: 100%;
`;

class ContactForm extends Component {
  state = {
    initLoading: true,
    loading: false,
    data: [],
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        axios
          .post(`${window.location.origin}/api/suggestion`, values)
          .then((response) => {
            this.resetForm();
            this.props.handleNewSuggestion();
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    });
  };

  resetForm = () => {
    this.props.form.resetFields();
  };

  componentDidMount() {
    axios
      .get(`${window.location.origin}/api/suggestion-type`)
      .then(({ data }) => {
        this.setState({
          initLoading: false,
          data: data,
        });
      });
  }

  rules = {
    section: [
      {
        required: true,
        message: "Section is required",
      },
    ],
    description: [
      {
        required: true,
        message: "Description is required",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    let { data } = this.state;

    return (
      <StyledForm onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row gutter={24}>
          <Col md={24} lg={24}>
            <FormItem>
              {getFieldDecorator(
                "suggestion_type_id",
                {}
              )(
                <Select placeholder="Section">
                  {data.map((el) => (
                    <Option value={el.id} key={el.id}>
                      {el.name.en}
                    </Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <FormItem>
            {getFieldDecorator(
              "suggestion",
              {}
            )(
              <TextArea
                placeholder="Message"
                autosize={{ minRows: 4, maxRows: 6 }}
              />
            )}
          </FormItem>
        </Row>
        <FormItem>
          <center>
            <Button ghost="true" size="large" type="primary" htmlType="submit">
              SEND YOUR MESSAGE
            </Button>
          </center>
        </FormItem>
        <FormItem>
          {getFieldDecorator("source_id", {
            initialValue: "WEB",
          })(<Input hidden />)}
        </FormItem>
      </StyledForm>
    );
  }
}

export default Form.create()(ContactForm);
