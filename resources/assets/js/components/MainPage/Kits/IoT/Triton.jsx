import React, { Component } from "react";
import { Row } from "antd";
import styled from "styled-components";
import { TitleSection} from "helpers";

const StyledImgApps = styled.img`
  width: 50%;
  display: block;
  margin: auto;
`;

const StyledDescription = styled.p`
    font-size: 1rem;
    text-align: center;
    margin-bottom: 50px;
    color: #000000;
`;

class Triton extends Component {
  render() {
    return (
      <div>
        <TitleSection
          title="Triton"
          />
        <StyledDescription>Biologging and biotelemetry are important tools to understand marine species, and consequently, contribute to our understanding of marine ecosystems as a whole. Assessing marine megafauna trajectories is traditionally performed with significantly high cost and labor, without guaranteeing the retrieval of the equipment, where georeferencing techniques remain highly proprietary and power intensive. Ubiquitous computing, Internet of Things (IoT) and open radio communication protocols such as Long-Range (LoRa) provide opportunities for tackling such issues by creation of robust and low-cost sensors networks which still need to be further contested in the harsh oceanic environment and on marine species. In this study, we enhance current state-of-the-art methods used in radio biotelemetry and marine monitoring with three-fold contribution. First, we contribute TRITON, an open telemetry sensor for marine species, showcasing a novel IoT tag and steps to replicate it, embedding computation units in resin, withstanding pressure at 1.5 km depth. Second, we provide a novel location estimation technique based on pseudoranging and minimum LoRa payload carrying satellite message identificators (MID). Avoiding the computation of latitude and longitude on GNSS receivers or usage of uplink satellite connection, we convert obtained raw tag GNSS satellite messages into limited LoRa payload and forwarding them to fixed land gateways serving as a differential GNSS reference. We provide error correction server-side, removing the tag errors with gateway errors and using 4+ satellites. Third, we validate the location estimation pipeline throughout in-the-wild studies by tracking cars, boats and divers, mimicking behaviour of marine species, obtaining 500 m grand average error across 4 case studies and optimal usage of 7 satellites. Based on the results, we discuss how TRITON system may be leveraged for long- and short- term monitoring of marine megafauna, paving the road ahead for more LoRa biotelemetry applications.</StyledDescription>
        <TitleSection
        subtitle="Designing"
        />
        <Row type="flex" justify="space-around" align="middle">
          <StyledImgApps src={"/images/iot/triton/design/design_triton.png"}/>
        </Row>
        <TitleSection
        subtitle="Prototyping"
        />
        <Row type="flex" justify="space-around" align="middle">
          <StyledImgApps src={"/images/iot/triton/prototype/prototype_triton.png"}/>
        </Row>
        <TitleSection
        subtitle="Validating"
        />
        <Row type="flex" justify="space-around" align="middle">
          <StyledImgApps src={"/images/iot/triton/validation/validation_triton.png"}/>
        </Row>
        <TitleSection
        subtitle="Field tests"
        />
        <Row type="flex" justify="space-around" align="middle">
          <StyledImgApps src={"/images/iot/triton/field/field_triton.png"}/>
        </Row>
        <TitleSection
        subtitle="Animal tests"
        />
        <Row type="flex" justify="space-around" align="middle">
          <StyledImgApps src={"/images/iot/triton/animal/animal_1.png"}/>
        </Row>
      </div>
    );
  }
}

export default Triton;