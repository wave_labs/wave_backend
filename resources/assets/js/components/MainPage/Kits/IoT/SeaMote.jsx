import React, { Component } from "react";
import { Row } from "antd";
import styled from "styled-components";
import { TitleSection} from "helpers";

const StyledImgApps = styled.img`
  width: 50%;
  display: block;
  margin: auto;
`;

const StyledDescription = styled.p`
    font-size: 1rem;
    text-align: center;
    margin-bottom: 50px;
    color: #000000;
`;

class SeaMote extends Component {
  render() {
    return (
      <div>
        <TitleSection
          title="SeaMote"
          />
      </div>
    );
  }
}

export default SeaMote;