export const logoPath = "/storage/images/logo/";

export const playStoreUrl = {
    whaleReporter: "https://play.google.com/store/apps/details?id=com.tigerwhale.sighting",
    diveReporter: "https://play.google.com/store/apps/details?id=com.tigerwhale.dive",
    litterReporter: "https://play.google.com/store/apps/details?id=com.tigerwhale.litterreporter&hl=en",
    arDome: "https://play.google.com/store/apps/details?id=com.tigerwhale.ardome",
};

export const AiData = [
    {
        to: "ai/1",
        image: logoPath + "biodiversitynet.svg",
        name: "BiodiversityNet",
        description:
            "Identify dorsal fins, flukes, fishes and marine megafauna from whale watching and online imagery",
        content:
            "Low-cost PAM system for nautical citizen science and real-time acoustic augmentation of whale-watching experiences. POSEIDON identifies vocal acoustic samples of whales and dolphins.",
    },
    {
        to: "ai/2",
        image: logoPath + "litternet.svg",
        name: "LitterNet",
        description:
            "Identify plastic and litter categories from ROV, AUV, UAV and mobile phone imagery",
        content:
            "Low-cost, long-range, radio controlled USV, based on IoT and LoRa, intended to be used for aquatic expeditions collecting environmental telemetry, like temperature, humidity, GPS position...",
    },
    {
        to: "ai/3",
        image: logoPath + "acousticnet.svg",
        name: "AcousticNet",
        description:
            "Identify marine mammal vocal calls using acoustic spectrograms imagery",
        content:
            "Low-cost, long-range, radio controlled USV, based on IoT and LoRa, intended to be used for aquatic expeditions collecting environmental telemetry, like temperature, humidity, GPS position...",
    },
];


export const IotData = [
    {
        to: "iot/poseidon",
        image: logoPath + "poseidon.svg",
        name: "Poseidon",
        description:
            "Passive-acoustic Ocean Sensor for Entertainment and Interactive Data-gathering in Opportunistic Nautical-activities",
        content:
            "Low-cost PAM system for nautical citizen science and real-time acoustic augmentation of whale-watching experiences. POSEIDON identifies vocal acoustic samples of whales and dolphins.",
    },
    {
        to: "iot/seamote",
        image: logoPath + "seamote.svg",
        name: "SeaMote",
        description:
            "Interactive Remotely Operated Apparatus for Aquatic Expeditions",
        content:
            "Low-cost, long-range, radio controlled USV, based on IoT and LoRa, intended to be used for aquatic expeditions collecting environmental telemetry, like temperature, humidity, GPS position...",
    },
    {
        to: "iot/triton",
        image: logoPath + "triton.svg",
        name: "Triton",
        description:
            "Low-cost, long-range bio-marker for assessing environmental telemetry using LoRa and IoT.",
        content:
            "Experimental apparatus of a bio-tag, to be placed on turtles, capable of collecting the environmental telemetry. Study reports feasibilty analysis using remote sensing through LoRa protocol and modeling the geolocation position.",

    },
    // {
    //     to: "iot/oceanus",
    //     name: "Oceanus",
    //     image: logoPath + "oceanus.svg",
    //     description:
    //         "ARCore mobile application for mapping of underwater environments onto geodesic domes.",
    //     content:
    //         "Experimental apparatus of a bio-tag, to be placed on turtles, capable of collecting the environmental telemetry. Study reports feasibilty analysis using remote sensing through LoRa protocol and modeling the geolocation position.",
    // },
    /* {
        to: "iot/intertagua",
        image: logoPath + "intertagua.svg",
        name: "Intertagua",
        description:
            "Interfaces Acuáticas Interactivas Detección y Visualización de la MegaFauna Marina Atlántica y Embarcaciones en la Macaronesia usando Marcadores Radiotransmisores.",
        content:
            "Interfaces Acuáticas Interactivas Detección y Visualización de la MegaFauna Marina Atlántica y Embarcaciones en la Macaronesia usando Marcadores Radiotransmisores.",
    }, */
];

export const AppsData = [
    {
        to: "apps/whaleReporter",
        image: logoPath + "whale-reporter",
        name: "Whale Reporter",
        o: "/kits/apps/whaleReporter",
        download: "https://play.google.com/store/apps/details?id=com.tigerwhale.whalereporter",
        ios: "https://apps.apple.com/in/app/whale-reporter/id1639080414",
        description:
            "Marine Biodiversity Assessments using Whale Watchers",
        content: "Building on the economic impact of whale watching as a form of ecotourism, we developed a system which supports on-shore and off-shore whale watching, offering an unobtrusive way to spot cetaceans.",
    },
    {
        to: "apps/diveReporter",
        image: logoPath + "dive-reporter",
        name: "Dive Reporter",
        o: "/kits/apps/diveReporter",
        download: "https://play.google.com/store/apps/details?id=com.tigerwhale.divereporter&hl=en&pli=1",
        ios: "https://apps.apple.com/br/app/dive-reporter/id1636665151",
        description:
            "Underwater Marine Biodiversity Assessments using Scuba Divers",
        content:
            "Tablet application with scuba divers as citizen scientists for post-dive surveys. We collect the abundance/scarcity of marine taxa and compare the results between tourists and dive-masters.",
    },
    {
        to: "apps/litterReporter",
        image: logoPath + "litter-reporter",
        name: "Litter Reporter",
        o: "/kits/apps/litterReporter",
        ios: "https://apps.apple.com/us/app/beached-litter-reporter/id1620396129?uo=2",
        download: "https://play.google.com/store/apps/details?id=com.tigerwhale.litterreporter&hl=en",
        description:
            "Marine Litter Geotagging using Crowdsourcing",
        content:
            "Mobile application with tourists as citizen scientists for reporting marine litter. Using gamification, we motivate users to geotag beached and floating marine litter.",
    },
    {
        to: "apps/",
        image: logoPath + "ardome",
        o: "/kits/apps/ardome",
        name: "ARDome",
        download: "https://play.google.com/store/apps/details?id=com.tigerwhale.ardome",
        ios: "https://apps.apple.com/br/app/ardome/id1634627244",
        description:
            "Experience and interact within AR cyberphysical systems",
        content: "Built as a standalone ARCore mobile application, it leverages the Augmented Reality and allowing the mapping of the underwater cyber environment onto the geodesic dome structure.",
    }
];
export const DomesData = [
    {
        image: logoPath + "dolphin.svg",
        name: "Dolphin",
        param: "dolphin",
        description: "Understanding the Carbon footprint of Sea-vessels",
        url: "https://domes.wave-labs.org/html/dolphin.html",
        urlwb: "https://domes.wave-labs.org/html/dolphin_environment.html",
        content: "Dome floats at the sea surface, and is moored to the public beach with ropes. Is built on an existing water platform, allowing visitors to explore it. It explains the consequences of CO2 emissions, coming from the whale watching sea-vessels",
    },
    {
        image: logoPath + "seal.svg",
        name: "Seal",
        param: "seal",
        description: "Exploration of Scuba Divers revealing Seal Record Depths",
        url: "https://domes.wave-labs.org/html/seal.html",
        urlwb: "https://domes.wave-labs.org/html/seal_environment.html",
        content: "Proposed installation resembles one of the endangered seals on Madeira archipelago, called Lobo Marinho. Such seals have been recently spotted having in-water short naps, and is therefore the rationale for being deployed at seabed.",
    },
    {
        image: logoPath + "turtle.svg",
        name: "Turtle",
        param: "turtle",
        description: "Revealing Turtle Encounters with Marine Litter from Nesting Beaches",
        url: "https://domes.wave-labs.org/html/turtle.html",
        urlwb: "https://domes.wave-labs.org/html/turtle_environment.html",
        content: "Deployed at the protected turtle nesting beaches, the users scan the dome with their own mobile phone’s camera. The dome acts as a 2D marker to trigger a Cross Reality (XR) experience.",
    },
    {
        image: logoPath + "whale.svg",
        name: "Whale",
        param: "whale",
        description: "Fixing Carbon using On-Shore Interactive Projection Mapping",
        url: "https://domes.wave-labs.org/html/whale.html",
        urlwb: "https://domes.wave-labs.org/html/whale_environment.html",
        content: "Designed for the public beach and night projections, audience interacts with it using body gestures and integrated sonar modules. Interaction is based on the proximity of the person to the dome.",
    },
    {
        image: logoPath + "bird.svg",
        name: "Seabird",
        param: "seabird",
        description: "Depicting the Food Diet using Off-shore Resting House for Seabirds",
        url: "https://domes.wave-labs.org/html/bird.html",
        urlwb: "https://domes.wave-labs.org/html/bird_environment.html",
        content: "Installed at the sea surface and acting as a lighthouse close to the harbor, it is deployed on root trajectories of whale-watching sea-vessels. Such vessels pass by the geodesic dome to observe the gathering of sea-birds.",
    }

];