import React, { Component } from "react";
import styled from "styled-components";
import { TitleSection } from "helpers";
import { playStoreUrl } from "./kitsHelper";
import { Link } from "react-router-dom";

const Container = styled.div`
  width: 100%;
`;

const KitsTitle = styled.h2`
  font-size: 1.5rem;
  text-align: center;
  @media screen and (max-width: 1000px) {
    text-align: center;
    font-size: 20px;
  }
`;

const StyledKit = styled.img`
  width: 70%;
  display: block;
  margin: auto;
  border-radius: 20px;
  margin-top: 2%;
  margin-bottom: 5%;
  -webkit-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);

  @media screen and (max-width: 1000px) {
    width: 500px;
  }

  @media screen and (max-width: 500px) {
    width: 300px;
  }
`;

const StyledAppDescription = styled.div`
  font-size: 20px;
  color: #777777;
  text-align: center;
  text-align: justify;
  margin-bottom: 50px;
  margin-left: 15%;
  margin-right: 15%;

  @media screen and (max-width: 500px) {
    font-size: 15px;
  }
`;
const App = ({ title, to, image, description }) => {
    return (
        <Link to={to}>
            <KitsTitle>{title}</KitsTitle>
            <StyledKit src={image} />

            <StyledAppDescription>{description}</StyledAppDescription>
        </Link>
    );
};

class Apps extends Component {
    render() {
        return (
            <Container>
                <TitleSection title="Apps" subtitle="Collect data with citizen science" />

                <App
                    title="Whale Reporter"
                    playStore={playStoreUrl.whaleReporter}
                    to="/kits/apps/whaleReporter"
                    image="/images/kits/whale.png"
                    description="Building on the iconic value of whales and the economic impact of
              whale watching as a form of ecotourism, we developed a system
              which supports on-shore and off-shore whale watching, offering an
              unobtrusive way to spot cetaceans to greater audiences. Here, we
              compare whale watchers from sea vessels and land viewpoints, and
              we report on a mixed analysis of usability and environmental
              awareness."
                />

                <App
                    title="Dive Reporter"
                    playStore={playStoreUrl.diveReporter}
                    to="/kits/apps/diveReporter"
                    image="/images/kits/dive.png"
                    description="Tablet application with scuba divers as citizen scientists for
            post-dive surveys. We collect the abundance/scarcity of given
            marine taxa and ask divers to estimate their total number. We
            provide a pilot study with the collected data and discuss the
            usability of the application with the divers. Our application will
            be used for longitudinal studies necessary to marine biologists."
                />

                <App
                    title="Litter Reporter"
                    playStore={playStoreUrl.litterReporter}
                    to="/kits/apps/litterReporter"
                    image="/images/kits/litter.png"
                    description="Mobile application with tourists as citizen scientists for
            reporting marine litter. Using gamification, we motivate users to
            geotag beached and floating marine litter. Application will be
            used for longitudinal studies necessary to marine biologists."
                />

                <App
                    title="ARDome"
                    playStore={playStoreUrl.arDome}
                    to="/kits/apps/ardome"
                    image="/images/kits/ardome.png"
                    description="Built as a standalone ARCore mobile application, it leverages the
            Augmented Reality and allowing the mapping of the underwater cyber
            environment onto the geodesic dome structure. Participants enter
            into the experience in a quest to aid the endangered species."
                />
            </Container>
        );
    }
}

export default Apps;
