import React, { Component } from "react";
import { Row } from "antd";
import styled from "styled-components";
import { TitleSection, dimensions } from "helpers";
import { AiData } from "./kitsHelper";

const IoTContainer = styled.div`
  width: 30%;
  min-width: 220px;

  margin-bottom: 50px;

  @media screen and (max-width: ${dimensions.sm}) {
    width: 70%;
  }
`;

const StyledImgApps = styled.img`
  width: 80%;
  display: block;
  margin: auto;
`;

const Name = styled.h3`
  font-size: 1.4em;
  display: block;
  text-align: center;
  font-weight: bold;
`;

class Ai extends Component {
  render() {
    return (
      <div>
        <TitleSection
          title="AI"
          subtitle="Predict marine megafauna and marine litter with deep learning"
        />

        <Row type="flex" justify="space-around" align="middle">
          {AiData.map((element, index) => (
            <IoTContainer key={index}>
              <StyledImgApps src={element.image} />
              <Name>{element.name}</Name>
            </IoTContainer>
          ))}
        </Row>
      </div>
    );
  }
}

export default Ai;
