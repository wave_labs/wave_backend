import React, { Component } from "react";
import { Row, Col, Modal } from "antd";
import styled from "styled-components";
import { TitleSection, dimensions } from "helpers";
import { DomesData } from "./kitsHelper";
import { PlayCircleOutlined } from "@ant-design/icons";

const StyledIframe = styled.iframe`
  width: 100%;
  height: 90%;
  border: none;
`;

const StyledModalIframe = styled(StyledIframe)`
  width: 100%;
  height: 100%;
`;

const IFrameContainer = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`;

const IframeInfoContainer = styled.div`
  width: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  text-align: left;
  padding: 25px;
  height: 100%;

  @media (max-width: ${dimensions.md}) {
    display: none;
  }

  h1 {
    color: white;
    font-weight: bold;
    font-size: 3em;
    margin-bottom: 0;
  }

  p {
    color: white;
    font-size: 1.3em;
  }
`;

const DomeContainer = styled(Col)`
  text-align: center;

  img {
    width: 60%;
    margin: auto;
    display: block;
  }
  h3 {
    font-size: 1.4em;

    @media screen and (max-width: 800px) {
      font-size: 1.2rem;
    }
  }
  p {
    color: #777777;
    margin: 0 auto;
  }
`;

const Container = styled.div`
  width: 100%;
`;

class Domes extends Component {
    state = {
        visible: false,
        data: {
            urlSimple: undefined,
            urlBackground: undefined,
            content: undefined,
            name: undefined,
        },
    };

    openModal(dome) {
        this.setState({
            visible: true,
            data: {
                urlSimple: dome.url,
                urlBackground: dome.urlwb,
                content: dome.content,
                name: dome.name,
            },
        });
    }

    closeModal = () => {
        this.setState({
            visible: false,
            data: {
                urlSimple: undefined,
                urlBackground: undefined,
                content: undefined,
                name: undefined,
            },
        });
    };

    render() {
        let { name, content, urlSimple, urlBackground } = this.state.data;
        return (
            <Container>
                <TitleSection
                    title="Domes"
                    subtitle="Interact with the data in immersive environments"
                />

                <Row gutter={8} type="flex" justify="center">
                    {DomesData.map((element, index) => {
                        return (
                            <DomeContainer key={index} span={8}>
                                <img src={element.image} />

                                <h3>{element.name}</h3>
                                <p>{element.description}</p>
                                <PlayCircleOutlined onClick={() => this.openModal(element)} />
                            </DomeContainer>
                        );
                    })}
                </Row>
                <Modal
                    title={null}
                    visible={this.state.visible}
                    onCancel={this.closeModal}
                    footer={null}
                    closable={false}
                    centered
                    width="80%"
                    bodyStyle={{ padding: "0", height: "80vh" }}
                >
                    <Row type="flex" style={{ height: "100%" }}>
                        <Col span={6}>
                            <IframeInfoContainer>
                                <div>
                                    <h1>{name}</h1>
                                    <p>{content}</p>
                                </div>
                            </IframeInfoContainer>
                        </Col>
                        <Col span={18}>
                            <IFrameContainer>
                                <StyledModalIframe
                                    src={urlSimple}
                                    scrolling="off"
                                    name="iframe-scene"
                                >
                                    <p>Your browser does not support iframes.</p>
                                </StyledModalIframe>
                            </IFrameContainer>
                        </Col>
                    </Row>
                </Modal>
            </Container>
        );
    }
}

export default Domes;
