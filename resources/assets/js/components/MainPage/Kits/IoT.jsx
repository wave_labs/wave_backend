import React, { Component } from "react";
import { Row } from "antd";
import styled from "styled-components";
import { TitleSection, dimensions } from "helpers";
import { IotData } from "./kitsHelper";
import { Link } from "react-router-dom";

const IoTContainer = styled.div`
  width: 20%;
  min-width: 180px;

  margin-bottom: 50px;

  @media screen and (max-width: ${dimensions.sm}) {
    width: 70%;
  }
`;

const StyledImgApps = styled.img`
  width: 80%;
  display: block;
  margin: auto;
`;

const Name = styled.h3`
  font-size: 1.4em;
  display: block;
  text-align: center;
  font-weight: bold;
`;


class IoT extends Component {
  render() {
    return (
      <div>
        <TitleSection
          title="IoT"
          subtitle="Survive sensing in harsh aquatic environments"
        />

        <Row type="flex" justify="space-around" align="middle">
          {IotData.map((element, index) => (
              <IoTContainer key={index}>
                <Link to={"/kits/iot/"+element.name.toLowerCase()}>
                  <StyledImgApps src={element.image} />
                  <Name>{element.name}</Name>
                </Link>
              </IoTContainer>
          ))}
        </Row>
      </div>
    );
  }
}

export default IoT;
