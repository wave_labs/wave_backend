import React, { useEffect } from 'react'
import styled from "styled-components";
import { dimensions } from '../../../../helpers';
import { connect } from "react-redux";
import { incrementCounter } from "redux-modules/litter/actions"
import { Row } from 'antd';


const InfoContainer = styled.section`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-top: 50px;

  @media (max-width: ${dimensions.md}) {
    margin-top: 0px;
  }

  img {
      width: 50%;

      @media (max-width: ${dimensions.md}) {
        width: 80%;
        margin: auto;
        display:  block;
        margin-top: 50px;
      }
  }
`;

const Info = styled.div`
  width: 45%;

  @media (max-width: ${dimensions.md}) {
    width: 80%;
    margin: auto;
        display:  block;
  }

  h3 {
    text-transform: uppercase;
    color: #777;
    font-size: 18px;
  }

  h4 {
    font-size: 26px;
    font-weight: bold;
    margin-bottom: 30px;
  }

  p {
    font-size: 16px;
    opacity: .8;
  }

`;

const Download = styled.img`
  width: 150px !important;
  display: block;
  margin: 30px 5px 0px 0px;

  @media screen and (max-width: 1000px) {
    margin-top: 30px;
  }
  
`;

const Partner = styled.img`
  width: 25%;
  padding: 30px;
  box-sizing: border-box;
  display: block;

  @media screen and (max-width: 767px) {
    width: 50%;
  }
  
`;

function WhaleReporter({ incrementCounter }) {

    useEffect(() => {
        incrementCounter({ reference: "dive-reporter" });
    }, [])

    return (
        <>
            <InfoContainer>
                <Info>
                    <h3>whale reporter</h3>
                    <h4>Report surface marine biodiversity using citizen science</h4>

                    <p>Building on the iconic value of whales and the economic impact of whale watching as a form of ecotourism, we developed a system which supports on-shore and off-shore whale watching, offering an unobtrusive way to spot cetaceans to greater audiences. Here, we compare whale watchers from sea vessels and land viewpoints, and we report on a mixed analysis of usability and environmental awareness.</p>
                    <Row type='flex' gutter={16}>
                        <a href="https://play.google.com/store/apps/details?id=com.tigerwhale.whalereporter" target="_blank" rel="noopener noreferrer">
                            <Download src="/images/google-play.png" />
                        </a>
                        <a href="https://apps.apple.com/in/app/whale-reporter/id1639080414" target="_blank" rel="noopener noreferrer">
                            <Download src="/images/apple-store.png" />
                        </a>
                    </Row>
                </Info>
                <img src="/images/apps/whale-reporter-mockup.png" alt="" />
            </InfoContainer>
        </>

    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        incrementCounter: (data) =>
            dispatch(incrementCounter(data)),
    };
};

export default connect(
    null,
    mapDispatchToProps
)(WhaleReporter);