import React, { useEffect, useState } from 'react'
import styled, { keyframes } from "styled-components";
import { dimensions } from '../../../../helpers';
import { connect } from "react-redux";
import { getDomeInteraction, incrementDomeInteraction } from "redux-modules/dome/domeInteraction/actions"
import { Row } from 'antd';

const scroll = () => keyframes`
  0% {
    top: 10px;
    opacity:1;
  }
  80% {
    top: 35px;
    opacity:0;
  }
  100% {
    top: 35px;
    opacity:0;
  } 
`;

const CounterContainer = styled.section`
  height: calc(100vh - 165px);
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;

  h2 {
    margin-bottom: 165px;
    font-size: 32vw;
    font-weight: bold;
    font-family: 'Anton', sans-serif;

    @media (max-width: ${dimensions.md}) {
        font-size: 200px;
    }
  }
`;




const InfoContainer = styled.section`
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;
    margin-top: 50px;
`;

const Info = styled.div`
    width: 45%;
    order: ${props => props.order};

    @media (max-width: ${dimensions.md}) {
        width: 80%;
        margin: auto;
        display:  block;
        order: 2;
    }

    h3 {

        color: #777;
        font-size: 18px;
    }

    h4 {
        font-size: 26px;
        font-weight: bold;
        margin-bottom: 30px;
    }

    p {
        font-size: 16px;
        opacity: .8;
    }
`;

const Image = styled.img`
    width: 50%;
    order: ${props => props.order};

    @media (max-width: ${dimensions.md}) {
        width: 80%;
        margin: auto;
        display:  block;
        margin-top: 50px;
        order: 2;
    }
`;

const Download = styled.img`
  width: 150px !important;
  display: block;
  margin: 30px 15px 0px 0px;

  @media screen and (max-width: 1000px) {
    margin-top: 30px;
  }
  
`;

function Ardome({ incrementDomeInteraction, location }) {
    useEffect(() => {
        var aDome = null;
        try {
            aDome = location.search[4];

        } catch (err) {
            aDome = null;
        }
        if (aDome) {
            incrementDomeInteraction({ dome: aDome, interaction: 1 })
        }


    }, [])

    return (
        <div>
            <InfoContainer>
                <Info order={1}>
                    <h3>ARDOME - WHALE</h3>
                    <h4>Explore and learn about the greater whales</h4>

                    <p>Explore the mysteries of North Right Whales in Augmented Reality and learn through interactive quizzes how they contribute to the buffering of human carbon footprints.</p>


                </Info>
                <Image order={2} src="/images/apps/ardome-whale-mockup.png" alt="" />
            </InfoContainer>
            <InfoContainer>
                <Info order={2}>
                    <h3>ARDOME - TURTLE</h3>
                    <h4>Collect marine litter using augmented reality with geodesic domes</h4>

                    <p>Built as a standalone ARCore mobile application, it leverages the Augmented Reality and allowing the mapping of the underwater cyber environment onto the geodesic dome structure. Participants enter into the experience in a quest to aid the endangered species.</p>

                </Info>
                <Image order={1} src="/images/apps/ardome-turtle-mockup.png" alt="" />
            </InfoContainer>
            

            <Row type='flex' justify='center' gutter={64}>
                <a href="https://play.google.com/store/apps/details?id=com.tigerwhale.ardome" target="_blank" rel="noopener noreferrer">
                    <Download src="/images/google-play.png" />
                </a>
                <a href="https://apps.apple.com/us/app/ardome/id1634627244" target="_blank" rel="noopener noreferrer">
                    <Download src="/images/apple-store.png" />
                </a>
            </Row>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        counter: state.domeInteraction.counter,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDomeInteraction: (filters) =>
            dispatch(getDomeInteraction(filters)),
        incrementDomeInteraction: (filters) =>
            dispatch(incrementDomeInteraction(filters)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Ardome);