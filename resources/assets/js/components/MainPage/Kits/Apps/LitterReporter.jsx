import React, { useEffect } from 'react'
import styled from "styled-components";
import { dimensions } from '../../../../helpers';
import { connect } from "react-redux";
import { incrementCounter } from "redux-modules/litter/actions"
import { Row } from 'antd';


const InfoContainer = styled.section`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-top: 50px;

  @media (max-width: ${dimensions.md}) {
    margin-top: 0px;
  }

  img {
      width: 50%;

      @media (max-width: ${dimensions.md}) {
        width: 80%;
        margin: auto;
        display:  block;
        margin-top: 50px;
      }
  }
`;

const Info = styled.div`
  width: 45%;

  @media (max-width: ${dimensions.md}) {
    width: 80%;
    margin: auto;
        display:  block;
  }

  h3 {
    text-transform: uppercase;
    color: #777;
    font-size: 18px;
  }

  h4 {
    font-size: 26px;
    font-weight: bold;
    margin-bottom: 30px;
  }

  p {
    font-size: 16px;
    opacity: .8;
  }

`;

const Download = styled.img`
  width: 150px !important;
  display: block;
  margin: 30px 5px 0px 0px;

  @media screen and (max-width: 1000px) {
    margin-top: 30px;
  }
  
`;

const Partner = styled.img`
  width: 25%;
  padding: 30px;
  box-sizing: border-box;
  display: block;

  @media screen and (max-width: 767px) {
    width: 50%;
  }
  
`;

function LitterReporter({ incrementCounter }) {

    useEffect(() => {
        incrementCounter({ reference: "litter-reporter" });
    }, [])

    return (
        <>
            <InfoContainer>
                <Info>
                    <h3>litter reporter</h3>
                    <h4>Marine litter geotagging using citizen scientists and marine ecologists</h4>

                    <p>Mobile application with tourists as citizen scientists for reporting marine litter. Using gamification, we motivate users to geotag beached and floating marine litter. Application will be used for longitudinal studies necessary to marine biologists.</p>
                    <Row type='flex' gutter={16}>
                        <a href="https://play.google.com/store/apps/details?id=com.tigerwhale.litterreporter&hl=en" target="_blank" rel="noopener noreferrer">
                            <Download src="/images/google-play.png" />
                        </a>
                        <a href="https://apps.apple.com/us/app/beached-litter-reporter/id1620396129?uo=2" target="_blank" rel="noopener noreferrer">
                            <Download src="/images/apple-store.png" />
                        </a>
                    </Row>
                </Info>
                <img src="/images/apps/litter-reporter-mockup.png" alt="" />
            </InfoContainer>
            <Row type='flex' justify='space-around' align='middle'>
                <Partner src="/images/partners/mare" />
                <Partner src="/images/partners/uma" />
                <Partner src="/images/partners/arditi2.png" />
                <Partner src="/images/partners/cleanatlantic" />
            </Row>
        </>

    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        incrementCounter: (data) =>
            dispatch(incrementCounter(data)),
    };
};

export default connect(
    null,
    mapDispatchToProps
)(LitterReporter);