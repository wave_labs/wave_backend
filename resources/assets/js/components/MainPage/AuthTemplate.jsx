import React, { Component } from "react";
import styled from "styled-components";
import { Form, Button, Row, Col } from "antd";
import { Link } from "react-router-dom";
import { dimensions } from "helpers";

const FormItem = Form.Item;

const StyledLoginButton = styled(Button)`
  width: 100%;
`;

const Container = styled(Row)`
  width: 85%;
  margin: auto;
  box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.1);
  border-radius: 6px;

  @media (max-width: ${dimensions.md}) {
    width: 95%;
  }
`;
const Title = styled.div`
  h1 {
    margin-bottom: 0;
  }

  p {
    margin-bottom: 40px;
    color: #777;
  }
`;

const LinkContainer = styled.div`
  font-size: 0.9rem;
  text-align: center;
  margin: auto;
  display: block;
  text-decoration: none;
  margin-top: 20px;
`;

const RegisterLink = styled(Link)`
  font-weight: bold;
  color: black;

  &:hover {
    color: #777777;
  }
`;
const SectionContainer = styled(Col)`
  background: ${(props) => (props.background ? "#e1f0ff" : "none")};
  border-radius: ${(props) =>
    props.registration ? "0px 6px 6px 0px" : "6px 0px 0px 6px"};
  padding: 40px !important;
  display: ${(props) => (props.background ? "flex !important" : "block")};

  @media (max-width: ${dimensions.md}) {
    border-radius: 6px 6px 0px 0px;
  }
`;
const Image = styled.img`
  width: 100%;
  height: auto;
  margin: auto;
  display: block;

  @media (max-width: ${dimensions.md}) {
    width: 50%;
  }
`;

const AuthTemplate = ({ image, children, registration }) => {
  return (
    <Container type="flex">
      <SectionContainer
        background={1}
        registration={registration ? 1 : 0}
        xs={{ order: 1, span: 24 }}
        md={{ order: registration ? 2 : 1, span: 10 }}
      >
        <Image src={"/images/" + image} />
      </SectionContainer>
      <SectionContainer
        xs={{ order: 2, span: 24 }}
        md={{ order: registration ? 1 : 2, span: 14 }}
      >
        <Title>
          {registration ? (
            <div>
              <h1>Welcome the Wave</h1>
              <p>
                Oceans call you to help it by conserving nature and reducing
                pollutants.
              </p>
            </div>
          ) : (
            <div>
              <h1>Welcome Back</h1>
              <p>
                The ocean still remembers your credentials to tackle noise and
                litter pollution.
              </p>
            </div>
          )}
        </Title>
        {children}
        <LinkContainer>
          {registration
            ? "Already have an account?"
            : "Don't have an account yet?"}

          <RegisterLink to={registration ? "login" : "register"}>
            {" Sign " + (registration ? "In" : "Up")}
          </RegisterLink>
        </LinkContainer>
        <LinkContainer>
            Forgot password? 
          <RegisterLink to={"password/forgot"}> Recover password </RegisterLink>
        </LinkContainer>
      </SectionContainer>
    </Container>
  );
};

export default AuthTemplate;
