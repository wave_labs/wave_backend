import React, { Component } from "react";
import { Row, Col, Select, Form, Input, Radio, Table } from "antd";
import CreatureRemoteSelectContainer from "../../Dashboard/CreatureContent/CreatureRemoteSelectContainer";

const FormItem = Form.Item;
const Option = Select.Option;
const TextArea = Input.TextArea;

class MarkerGeneralQuestionsForm extends Component {
  state = {
    row1: null,
    row2: null,
    row3: null,
  };

  rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  resetRadio = (col, radioValues, index) => {
    index.forEach((element) => {
      radioValues[element] == col && (radioValues[element] = null);
    });

    return radioValues;
  };

  handleRadioClick = (e) => {
    const { row1, row2, row3 } = this.state;
    let radioValues = [row1, row2, row3];
    var element = this.splitValue(e.target.value);
    var row = element[0];
    var col = element[1];

    switch (row) {
      case "1":
        this.resetRadio(col, radioValues, [1, 2]);
        radioValues[0] = col;
        break;
      case "2":
        this.resetRadio(col, radioValues, [0, 2]);
        radioValues[1] = col;
        break;
      case "3":
        this.resetRadio(col, radioValues, [0, 1]);
        radioValues[2] = col;
        break;
      default:
        break;
    }

    this.setState(
      {
        row1: radioValues[0],
        row2: radioValues[1],
        row3: radioValues[2],
      },
      this.checkRadioFinished
    );
  };

  checkRadioFinished = () => {
    const { row1, row2, row3 } = this.state;

    if (row1 && row2 && row3) {
      this.props.form.setFieldsValue({
        size: { sm: row1, md: row2, lg: row3 },
      });
      this.handleFormChange("size");
    }
  };

  handleFormChange = (element) => {
    this.props.handleFormValidation(element);
  };

  splitValue = (element) => {
    return element.split("/");
  };

  handleCreatureChange = (creatureId) => {
    this.props.handleCreatureChange(creatureId);
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { row1, row2, row3 } = this.state;
    const { dimensions } = this.props;

    const RadioButton = ({ row, value }) => {
      let splitValue = this.splitValue(value);
      return (
        <Radio
          checked={row == splitValue[1] && true}
          value={value}
          onClick={this.handleRadioClick}
        />
      );
    };

    const dataSource = [
      {
        name: `Menos de ${dimensions.sm} ${dimensions.unit}`,
        frequently: <RadioButton row={row1} value="1/frequently" />,
        rarely: <RadioButton row={row1} value="1/rarely" />,
        never: <RadioButton row={row1} value="1/never" />,
      },
      {
        name: `Entre ${dimensions.sm} e ${dimensions.lg} ${dimensions.unit}`,
        frequently: <RadioButton row={row2} value="2/frequently" />,
        rarely: <RadioButton row={row2} value="2/rarely" />,
        never: <RadioButton row={row2} value="2/never" />,
      },
      {
        name: `Mais de ${dimensions.lg} ${dimensions.unit}`,
        frequently: <RadioButton row={row3} value="3/frequently" />,
        rarely: <RadioButton row={row3} value="3/rarely" />,
        never: <RadioButton row={row3} value="3/never" />,
      },
    ];

    const columns = [
      {
        title: "",
        dataIndex: "name",
        align: "center",
      },
      {
        title: "Frequentemente",
        dataIndex: "frequently",
        align: "center",
      },
      {
        title: "Raramente",
        dataIndex: "rarely",
        align: "center",
      },
      {
        title: "Nunca",
        dataIndex: "never",
        align: "center",
      },
    ];

    return (
      <Form hideRequiredMark={true} className="wrap-label">
        <Row type="flex" align="middle" gutter={32}>
          <Col
            xs={24}
            style={{ display: this.props.random ? "block" : "none" }}
          >
            <FormItem label="Escolha a espécie desejada">
              {getFieldDecorator("creature_id", {
                initialValue: this.props.creatureId,
              })(
                <CreatureRemoteSelectContainer
                  placeholder="Listagem de espécies"
                  onChange={this.handleCreatureChange}
                />
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem label="Conhece bem esta espécie? *">
              {getFieldDecorator("form[knowledge]", {
                rules: this.rules.requiredField,
              })(
                <Select
                  onChange={() => this.handleFormChange("knowledge")}
                  placeholder="Sim / Não"
                >
                  <Option value="yes">Sim</Option>
                  <Option value="no">Não</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem label="Se sim, pode escrever o seu nome?">
              {getFieldDecorator("form[name_specie]", {})(
                <Input placeholder="Introduza o nome da espécie" />
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem label="Lembra-se em que ano e/ou mês que viu pela primeira vez esta espécie? *">
              {getFieldDecorator("form[date]", {
                rules: this.rules.requiredField,
              })(
                <Input
                  onChange={() => this.handleFormChange("date")}
                  placeholder="Janeiro 2018 / 2020"
                />
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem label="Nos últimos 10 anos, como considera os avistamentos e/ou capturas desta espécie? *">
              {getFieldDecorator("form[abundance]", {
                rules: this.rules.requiredField,
              })(
                <Select
                  onChange={() => this.handleFormChange("abundance")}
                  placeholder="Aumentaram / Constantes / Diminuíram / Não sei"
                >
                  <Option value="increased">Aumentaram</Option>
                  <Option value="constant">Manteram-se constantes</Option>
                  <Option value="decreased">Diminuíram</Option>
                  <Option value="not sure">Não sei</Option>
                </Select>
              )}
            </FormItem>
          </Col>

          <Col xs={24}>
            <FormItem label="Em média, que tamanho tinha(m) o (s) peixe(s) que viu/capturou? *">
              {getFieldDecorator("size", {
                rules: this.rules.requiredField,
              })(
                <Table
                  rowKey={(record) => record.name}
                  pagination={false}
                  dataSource={dataSource}
                  columns={columns}
                />
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem label="Considera que nos últimos 10 anos, apareceram outras espécies de peixes nas águas da Madeira? *">
              {getFieldDecorator("form[other_species]", {
                rules: this.rules.requiredField,
              })(
                <Select
                  onChange={() => this.handleFormChange("other_species")}
                  placeholder="Sim / Não / Não sei"
                >
                  <Option value="yes">Sim</Option>
                  <Option value="no">Não</Option>
                  <Option value="not sure">Não sei</Option>
                </Select>
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem label="Se sim, pode escrever os seus nomes?">
              {getFieldDecorator("form[name_species]", {})(
                <Input placeholder="Nome das espécies separados por vírgulas" />
              )}
            </FormItem>
          </Col>

          <Col xs={24}>
            <FormItem label="Uma da consequência das alterações climáticas é o aquecimento da água dos Oceanos. Temperaturas da água mais quentes permitem a chegada e o estabelecimento de outras espécies marinhas. Considera isto uma ameaça para a biodiversidade marinha? *">
              {getFieldDecorator("form[consequences]", {
                rules: this.rules.requiredField,
              })(
                <Select
                  onChange={() => this.handleFormChange("consequences")}
                  placeholder="Sim / Não / Não sei"
                >
                  <Option value="yes">Sim</Option>
                  <Option value="no">Não</Option>
                  <Option value="not sure">Não sei</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(MarkerGeneralQuestionsForm);
