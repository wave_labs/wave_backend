import { Tooltip, Modal, Row, Button } from "antd";
import React, { Component } from "react";
import MapBoxMap from "../../Dashboard/Common/MapBoxMap";
import { QuestionCircleFilled } from "@ant-design/icons";
import styled from "styled-components";
import { Marker } from "react-map-gl";
import MarkerGeneralQuestionsForm from "./MarkerGeneralQuestionsForm";
import MarkerActivityForm from "./MarkerActivityForm";
import { connect } from "react-redux";
import { createMarker } from "redux-modules/marker/actions";
import { alertActions } from "redux-modules/alert/actions";

import { getErrorMessages, ICON } from "helpers";

const InfoContainer = styled.div`
  margin: 50px auto;
  margin-bottom: 30px;
  h1,
  p {
    text-align: center;
  }

  p {
    font-size: 1.2em;
    width: 80%;
    display: block;
    margin: auto;
  }
`;

const StyledCreatureImage = styled.div`
  display: block;
  position: relative;
  width: 250px;
  height: 250px;
  margin: 0px auto 30px auto;
  padding: 40px;
  border-radius: 50%;
  background-color: #eeeeee;
  background-image: url(${(props) => props.background});
  background-size: 100% auto;
  background-repeat: no-repeat;
  background-position: center;

  &:before {
    display: block;
    padding-top: 100%;
  }
`;

const StyledQuestionCircleFilled = styled(QuestionCircleFilled)`
  position: absolute;
  top: 10px;
  right: 10px;
`;

const StyledImage = styled.img`
  width: 100px;
  cursor: pointer;
  opacity: ${(props) => (props.active ? 1 : 0.3)};
  display: block;
  margin: auto;
`;

const ActivityPickerTitle = styled.div`
  font-size: 1.2em;
  text-align: center;
  text-transform: capitalize;
`;

const FormContainer = styled.div`
  display: block;
  width: 80%;
  margin: 50px auto;

  @media (max-width: 768px) {
    width: 100%;
  }
`;

const ActivityPickerContainer = styled.div`
  width: 23%;
  min-width: 150px;
  margin: 20px auto;
`;

const ActivityPicker = ({ title, children }) => {
  return (
    <ActivityPickerContainer>
      <div>
        {children}
        <ActivityPickerTitle>{title}</ActivityPickerTitle>
      </div>
    </ActivityPickerContainer>
  );
};

class MarkerContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      markers: {}, //markers positions for activities
      formFields: [], //filled fields on first page
      currentStep: 0, // current modal page
      activeOnClick: true,
      formValidated: false, //form validation for first page
      currentActive: "diving", //current actidivthis form
      requiredFields: {}, //forms required for activities
      submitReady: false, //at least one marker is required
    };

    // preserve the initial state in a new object
    this.baseState = this.state;
  }

  componentDidMount() {
    let { creatureId } = this.props;

    this.setState({
      creatureId: creatureId,
    });
  }

  componentDidUpdate(prevProps) {
    let { creatureId, visible, creatureActivities } = this.props;
    let { markers, requiredFields } = this.state;

    if (visible && prevProps != this.props) {
      creatureActivities.map((activity) => {
        markers[activity.name] = [];
        requiredFields[activity.name] = false;
      });
      this.setState({
        creatureId: creatureId,
        markers,
        requiredFields,
      });
    }
  }

  handleDragEnd = (event, index, category) => {
    let { markers } = this.state;
    markers[category][index] = [event.lngLat[1], event.lngLat[0]];

    setTimeout(() => {
      this.setState(
        {
          markers,
          activeOnClick: true,
        },
        this.handleFormData
      );
    }, 1);
  };

  resetState = () => {
    this.setState({
      activeOnClick: true,
      markers: [[], [], []],
    });
  };

  handleDragStart = () => {
    this.setState({ activeOnClick: false });
  };

  handleMarkerClick = (index) => {
    // let { markers } = this.state;
    // markers.splice(index, 1); //Remove 1 record at index
    // this.setState({ markers });
  };

  handleMapClick = (e) => {
    let { markers, activeOnClick, currentActive, requiredFields } = this.state;
    if (activeOnClick) {
      //if (e.leftButton) {
      requiredFields[currentActive] = true;
      markers[currentActive].push([e.lngLat[1], e.lngLat[0]]);
      this.setState(
        { markers, requiredFields, submitReady: true },
        this.handleFormData
      );
      //}
    }
  };

  next = () => {
    if (this.state.formValidated) {
      const form = this.formRef.props.form;
      form.validateFields((err, data) => {
        if (err) {
          this.setState({ formValidated: false });
          return;
        } else {
          const currentStep = this.state.currentStep + 1;
          this.setState({ formData: data, currentStep });
        }
      });
    }
  };

  prev = () => {
    const currentStep = this.state.currentStep - 1;
    this.setState({ currentStep, formValidated: false });
    this.resetState();
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  saveActivityFormRef = (activityFormRef) => {
    this.activityFormRef = activityFormRef;
  };

  handleFormValidation = (element) => {
    this.setState({ formValidated: false });
    var { formFields } = this.state;

    if (formFields.indexOf(element) === -1) {
      formFields.push(element);
      this.setState({ formFields });
    }

    if (formFields.length == 6) {
      setTimeout(() => {
        const form = this.formRef.props.form;
        form.validateFields((err, data) => {
          if (err) {
            this.setState({ formValidated: false });
            return;
          } else {
            this.setState({ formValidated: true });
          }
        });
      }, 1);
    }
  };

  handleFormData = () => {
    setTimeout(() => {
      let { creatureActivities } = this.props;
      let { markers, formData } = this.state;
      var data = this.formRef.props.form.getFieldsValue();
      let additionalData = { coordinates: {}, activities: {} };

      creatureActivities.map((activity) => {
        if (markers[activity.name].length > 0) {
          additionalData.coordinates[activity.name] = markers[activity.name];
          additionalData.activities[activity.name] = data[activity.name];
        }
      });

      this.setState({ formData: { ...formData, ...additionalData } });
    }, 1);
  };

  handleSubmit = () => {
    let { formData } = this.state;

    const form = this.formRef.props.form;

    form.validateFieldsAndScroll((err) => {
      if (err) {
        Object.keys(err).map((element) => {
          this.setState({
            currentActive: element,
          });
        });
        return;
      } else {
        this.props
          .createMarker(formData)
          .then(() => {
            this.handleModalClose();
            this.props.successAlert({
              message: "Markers - Have you seen me lately?",
              description: "Thank you for participating!",
            });
          })
          .catch((error) => {
            let messages = getErrorMessages(error.response.data.errors);

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  handleFilterChange = (e) => {
    this.setState({
      currentActive: e,
    });
  };

  handleCreatureChange = (creatureId) => {
    this.setState({ creatureId });
    this.props.handleCreatureData(creatureId);
  };

  handleModalClose = () => {
    // reset to initial state
    this.setState(this.baseState);

    this.props.handleModalClose();
  };

  getMarkerColor = (marker) => {
    let { creatureActivities } = this.props;
    var activity = creatureActivities.filter((activity) => {
      return activity.name == marker;
    });

    return activity[0].color;
  };

  render() {
    var {
      markers,
      currentStep,
      formValidated,
      currentActive,
      requiredFields,
      creatureId,
      submitReady,
    } = this.state;

    //console.log(this.state);

    var { creatureActivities } = this.props;

    const steps = [
      {
        key: 0,
        title: "Taxa",
        content: (
          <InfoContainer>
            <StyledCreatureImage
              background={
                creatureId
                  ? `/api/image/creature/${creatureId}`
                  : "/images/studies/markers"
              }
            />
            <h1>Have you seen me lately?</h1>
            <p>
              No âmbito das atividades de investigação desenvolvidas na Madeira
              estamos a fazer uma avaliação sobre a presença e distribuição de
              espécies nas águas costeiras do Arquipélago da Madeira. O
              inquérito é de resposta voluntária, é anónimo e os dados serão
              analisados de forma global.
            </p>
            <FormContainer>
              <MarkerGeneralQuestionsForm
                dimensions={this.props.creatureDimensions}
                random={this.props.random}
                creatureId={creatureId}
                handleCreatureChange={this.handleCreatureChange}
                handleFormValidation={this.handleFormValidation}
                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
              />
            </FormContainer>
          </InfoContainer>
        ),
      },
      {
        key: 1,
        title: "Picker",
        content: (
          <FormContainer>
            <Row
              type="flex"
              justify="space-between"
              align="middle"
              style={{ margin: "30 0" }}
            >
              {creatureActivities.map((element, index) => {
                return (
                  <ActivityPicker key={element.id} title={element.name}>
                    <StyledImage
                      active={currentActive == element.name && true}
                      src={element.image}
                      alt={element.name}
                      onClick={() => this.handleFilterChange(element.name)}
                    />
                  </ActivityPicker>
                );
              })}
            </Row>
            <InfoContainer>
              <p>
                Selecione uma atividade, responda às três perguntas e posicione
                o(s) pin(s) no mapa onde avistou/pescou esta espécie. Caso faça
                mais que uma atividade repita os passos para cada uma das
                atividades.
              </p>
              <div style={{ marginTop: "50px" }}>
                <MarkerActivityForm
                  creatureActivities={this.props.creatureActivities}
                  requiredFields={requiredFields}
                  handleFormData={this.handleFormData}
                  currentActive={currentActive}
                  wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                />
              </div>
            </InfoContainer>

            <MapBoxMap
              fixedSize={true}
              size="500"
              zoom={10}
              onClick={this.handleMapClick}
            >
              {Object.entries(markers).map((element, index) => {
                {
                  return element[1].map((e, i) => {
                    return (
                      <Marker
                        key={i}
                        latitude={e[0]}
                        longitude={e[1]}
                        draggable
                        onDragEnd={(event) =>
                          this.handleDragEnd(event, i, index)
                        }
                        onDragStart={this.handleDragStart}
                      >
                        <svg
                          onContextMenu={() => this.handleMarkerClick(i)}
                          height="25"
                          viewBox="0 0 24 24"
                          style={{
                            fill: this.getMarkerColor(element[0]),
                            stroke: "none",
                          }}
                        >
                          <path d={ICON} />
                        </svg>
                      </Marker>
                    );
                  });
                }
              })}

              <Tooltip
                placement="topLeft"
                title="Left click to create new entry points of taxa or right click existing points to remove them"
              >
                <StyledQuestionCircleFilled style={{ fontSize: "25px" }} />
              </Tooltip>
            </MapBoxMap>
          </FormContainer>
        ),
      },
    ];

    return (
      <Modal
        onCancel={this.handleModalClose}
        centered
        visible={this.props.visible}
        closable
        style={{ top: "80px" }}
        width={960}
        footer={[
          currentStep < steps.length - 1 ? (
            <Row key={0} type="flex" justify="center">
              <Tooltip title="Select requested information before advancing">
                <Button
                  disabled={!formValidated}
                  size="large"
                  key="submit"
                  type="primary"
                  onClick={this.next}
                >
                  Next
                </Button>
              </Tooltip>
            </Row>
          ) : (
            <Row key={1} type="flex" justify="center" gutter={16}>
              <Button size="large" onClick={this.prev}>
                Back
              </Button>
              <Button
                disabled={!submitReady}
                size="large"
                key="submit"
                type="primary"
                onClick={this.handleSubmit}
              >
                Submit
              </Button>
            </Row>
          ),
        ]}
      >
        <div>{steps[currentStep].content}</div>
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createMarker: (data) => dispatch(createMarker(data)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
    successAlert: (data) => dispatch(alertActions.success(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    creatureDimensions: state.creature.dimensions,
    creatureActivities: state.creature.activities,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MarkerContent);
