import React, { Component } from "react";
import { Row, Col, Select, Form } from "antd";
import styled from "styled-components";

const Option = Select.Option;
const FormItem = Form.Item;

const FormContainer = styled.div`
  display: ${(props) => (props.active ? "block" : "none")};
`;

class MarkerActivityForm extends Component {
  render() {
    const { currentActive, requiredFields } = this.props;
    const { getFieldDecorator } = this.props.form;

    var rules = {
      requiredDiving: [
        {
          required: requiredFields.diving,
          message: "Required",
        },
      ],
      requiredFishing: [
        {
          required: requiredFields.fishing,
          message: "Required",
        },
      ],
      requiredSpearfishing: [
        {
          required: requiredFields.spearfishing,
          message: "Required",
        },
      ],
    };

    return (
      <Form hideRequiredMark={true} className="wrap-label">
        <Row gutter={32}>
          {Object.keys(requiredFields).map((activity) => {
            return (
              <FormContainer
                key={activity}
                active={currentActive == activity && true}
              >
                <Col xs={24} sm={8}>
                  <FormItem>
                    {getFieldDecorator(`${activity}[0]`, {
                      rules: rules[activity],
                    })(
                      <Select
                        onChange={this.props.handleFormData}
                        placeholder="Anos de experiência"
                      >
                        <Option value="1 year">1 ano</Option>
                        <Option value="1-5 years">1-5 anos</Option>
                        <Option value="5-10 years">5-10 anos</Option>
                        <Option value="over 10 years">Mais de 10 anos</Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col xs={24} sm={8}>
                  <FormItem>
                    {getFieldDecorator(`${activity}[1]`, {
                      rules: rules[activity],
                    })(
                      <Select
                        placeholder="Frequência da atividade"
                        onChange={this.props.handleFormData}
                      >
                        <Option value="1 per month">1 vez por mês</Option>
                        <Option value="2 to 3 times per month">
                          2 a 3 vezes por mês
                        </Option>
                        <Option value="1 per week">1 vez por semana</Option>
                        <Option value="2 to 5 times per week">
                          2 a 5 vezes por semana
                        </Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col xs={24} sm={8}>
                  <FormItem>
                    {getFieldDecorator(`${activity}[2]`, {
                      rules: rules[activity],
                    })(
                      <Select
                        placeholder="Tempo por atividade"
                        onChange={this.props.handleFormData}
                      >
                        <Option value="1h per time">1h por vez</Option>
                        <Option value="1-3h per time">1-3h por vez</Option>
                        <Option value="over 3h per time">
                          Mais de 3h por vez
                        </Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
              </FormContainer>
            );
          })}
        </Row>
      </Form>
    );
  }
}

export default Form.create()(MarkerActivityForm);
