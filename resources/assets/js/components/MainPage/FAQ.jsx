import React from "react";
import { Row, Col, Anchor, Card, List } from "antd";
import styled from "styled-components";
import { NavLink } from "react-router-dom";
import { TitleSection, FAQContent } from "helpers";

const { Link } = Anchor;

const Question = styled.a`
  font-weight: 700;
  font-size: 1.8em;
  margin-bottom: 30px;
  text-decoration: none;
  color: black;
  display: block;

  &:hover {
    color: black;
  }
`;

const StyledAnchor = styled(Anchor)`
  white-space: nowrap;
  overflow: hidden !important;
  background-color: transparent !important;
`;

const StyledLink = styled(Link)`
  text-overflow: ellipsis !important;
  width: 90%;
  font-weight: bold;
  margin: 5px 0;
`;

const Answer = styled.p`
  color: #6a7695;
`;

const Container = styled.div`
  width: 100%;

  .ant-anchor-ink::before {
    background: transparent;
  }
`;

const StyledCard = styled.div`
  width: 100%;
  background-color: rgb(245, 244, 251);
  margin-bottom: 7em;
  padding: 30px;
  border-radius: 5px;
`;

const StyledLinkDescription = styled.h2`
  font-size: 1.7rem;
  width: 80%;
  color: black;
  margin-top: 5%;
  display: inline-block;
`;

const StyledNavLink = styled(NavLink)`
  text-decoration: underline;
  font-weight: bold;
  color: black;
  margin-left: 5px;

  &:hover {
    text-decoration: underline;
    color: black;
  }
`;

const FAQ = () => {
  return (
    <Container>
      <TitleSection
        title="FAQ"
        subtitle="Frequently asked questions regarding our platform"
      />

      <Row gutter={48}>
        <Col xs={0} lg={8}>
          <StyledAnchor affix={true} offsetTop={120}>
            <List
              itemLayout="horizontal"
              dataSource={FAQContent}
              renderItem={(item, index) => (
                <List.Item
                  style={{
                    borderBottom: 0,
                    borderBottomColor: "transparent",
                    padding: 0,
                  }}
                >
                  <StyledLink
                    href={"#question" + index}
                    title={item.question}
                  />
                </List.Item>
              )}
            />
          </StyledAnchor>
        </Col>

        <Col xs={24} lg={16}>
          <List
            itemLayout="horizontal"
            dataSource={FAQContent}
            renderItem={(item, index) => (
              <List.Item
                style={{
                  borderBottom: 0,
                  borderBottomColor: "transparent",
                  padding: 0,
                }}
              >
                <StyledCard id={"question" + index} bordered={false}>
                  <Question name={"question" + index}>{item.question}</Question>
                  <Answer>{item.content}</Answer>
                </StyledCard>
              </List.Item>
            )}
          />
        </Col>
      </Row>

      <center>
        <StyledLinkDescription>
          Did not find what you're looking for?
          <StyledNavLink to="/contacts">Contact us</StyledNavLink>
        </StyledLinkDescription>
      </center>
    </Container>
  );
};

export default FAQ;
