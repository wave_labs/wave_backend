import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Button } from "antd";
import { Link } from "react-router-dom";
import MapFilters from "./MapFilters";
import { AppsData, IotData, DomesData } from "./Kits/kitsHelper";
import "./css/Style.css";
import AICataloguePreview from "../Dashboard/AI/AiContent/AiCataloguePreview";
import KitsContent from "./Homepage/KitsContent";
import { dimensions } from "helpers";

const HowToContent = styled.div`
  width: 30%;
  min-width: 250px;
  text-align: center;
  padding: 10px;

  @media screen and (max-width: 768px) {
    width: 90%;
  }

  h3 {
    font-size: 1.3em;
    margin-bottom: 5px;
    font-weight: bold;
  }
`;

const SectionContainer = styled(Row)`
  margin: 100px auto;
  margin-bottom: ${(props) => (props.last ? "0px" : "100px")};
`;

const StyledAppInfo = styled.section`
  max-width: 96%;
  text-align: left;
  font-size: 19px;

  @media screen and (max-width: 1000px) {
    font-size: 16px;
    text-align: center;
  }
`;

const StyledSubtitle = styled.h2`
  font-size: 36px;
  text-align: center;
  margin-bottom: 1%;
  font-weight: bold;
  line-height: 1.3;

  @media screen and (max-width: 1000px) {
    font-size: 24px;
  }
`;

const SubContainer = styled.div`
  width: 90%;
  min-width: 200px;
  margin: 0 auto;
  margin-bottom: 10%;
`;

const StyledTitle = styled.div`
  font-size: 68px;
  font-weight: bold;
  max-width: 96%;
  line-height: 1.1;

  @media screen and (max-width: 1000px) {
    font-size: 50px;
    text-align: center;
  }
`;

const TextDescription = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
  margin-bottom: 5%;

  @media screen and (max-width: 1000px) {
    font-size: 18px;
  }
`;

const StyledParagraph = styled.div`
  font-size: 18px;
  display: block;
  text-align: center;
  font-weight: bold;

  @media (max-width: ${dimensions.md}) {
    display: none;
  }
`;

const StyledProjImg = styled.img`
  width: 180px;
  display: block;
  margin: 20px auto;
  margin-bottom: 10px;
`;

const JoinUsContainer = styled.div`
  width: 70%;
  margin: auto;

  @media screen and (max-width: 1000px) {
    text-align: center;
    img {
      width: 90%;
    }
  }

  h3 {
    font-size: 1.5rem;
    @media screen and (max-width: 1000px) {
      display: none;
    }
  }
  p {
    font-size: 1rem;
    color: #777777;
  }

  span {
    font-weight: bold;
  }
`;

const StyledDiv = styled.div`
  display: flex;
  justify-content: flex-start;

  @media screen and (max-width: 1000px) {
    justify-content: space-around;
  }
`;

const Section = ({ title, subtitle, children, redirectTo, last }) => (
    <SectionContainer last={last ? 1 : 0}>
        <StyledSubtitle>{title}</StyledSubtitle>
        <TextDescription>{subtitle}</TextDescription>

        {children}
        {redirectTo && <WatchMore redirectTo={redirectTo} />}
    </SectionContainer>
);

const WatchMore = ({ redirectTo, message = "See More" }) => (
    <Row type="flex" justify="center" style={{ marginTop: "20px" }}>
        <Link to={redirectTo}>
            <Button ghost="true" size="large" type="primary">
                {message}
            </Button>
        </Link>
    </Row>
);

const Study = ({ study, title }) => (
    <div>
        <StyledProjImg src={"/storage/images/logo/" + study + ".svg"} />
        <StyledParagraph>{title}</StyledParagraph>

        <WatchMore message="Start Now" redirectTo={`/studies?study=` + study} />
    </div>
);

class Homepage extends Component {
    state = {
        aiModelFilter: null,
    };
    render() {
        return (
            <div>
                <SubContainer>
                    <Row type="flex" justify="space-around" align="middle" gutter={48}>
                        <Col xs={24} lg={12}>
                            <StyledTitle>Marine Technology</StyledTitle>
                        </Col>
                        <Col xs={24} lg={12}>
                            <StyledAppInfo>Understand how the ocean works.</StyledAppInfo>
                            <StyledAppInfo>Use Apps, IoT, AI and Dome kits.</StyledAppInfo>

                            <br />

                            <StyledDiv>
                                <Link to="/register">
                                    <Button ghost="true" size="large" type="primary">
                                        Get Started
                                    </Button>
                                </Link>
                            </StyledDiv>
                        </Col>
                    </Row>
                </SubContainer>

                <Section
                    title="Your Latest Reports"
                    subtitle="Explore underwater, surface and aerial aquatic assessments by citizen
          scientists"
                >
                    <MapFilters />
                </Section>

                <Section
                    title="Studies"
                    subtitle="Help us conserve these species worldwide"
                >
                    <Row type="flex" justify="space-around" align="bottom">
                        <Study study="markers" title="Markers" />
                        <Study study="stopwatch" title="Stopwatch" />
                        <Study study="bounding-box" title="Bounding Box" />
                    </Row>
                </Section>

                <Section
                    title="Apps"
                    subtitle="Report and explore marine biodiversity and litter"
                    redirectTo="/kits/apps"
                >
                    <KitsContent data={AppsData} />
                </Section>

                <Section
                    title="IoT"
                    subtitle="Survive sensing in harsh aquatic environments"
                    redirectTo="/kits/iot"
                >
                    <KitsContent data={IotData} />
                </Section>

                <Section
                    title="AI"
                    subtitle="Identify marine species or debris with deep learning"
                    redirectTo="/kits/ai"
                >
                    <Row type="flex" justify="space-around" align="top">
                        <HowToContent>
                            <h3>1. Select the model</h3>
                            <p>
                                Select one of the models right after this section. Each model
                                interprets imagery based on its training.
                            </p>
                        </HowToContent>
                        <HowToContent>
                            <h3>2. Upload an image</h3>
                            <p>
                                Select an image and upload it to our server. Our AI will
                                evaluate and grant you the results.
                            </p>
                        </HowToContent>
                        <HowToContent>
                            <h3>3. Check the results</h3>
                            <p>
                                The image is now classified. You can confirm the identification
                                drawn on the image.
                            </p>
                        </HowToContent>
                    </Row>

                    <AICataloguePreview />
                </Section>

                <Section
                    title="Domes"
                    subtitle="Impact aquatic literacy with interactive environments"
                    redirectTo="/kits/domes"
                >
                    <KitsContent data={DomesData} />
                </Section>

                <Section title="Projects" subtitle="Explore ongoing research">
                    <Row type="flex" justify="space-around" align="bottom">
                        <div>
                            <StyledProjImg src="/storage/images/logo/intertagua.svg" />
                            <StyledParagraph>Intertagua</StyledParagraph>
                            <WatchMore redirectTo="/intertagua" />
                        </div>
                        <div>
                            <StyledProjImg src="/storage/images/logo/largescale.svg" />
                            <StyledParagraph>Largescale</StyledParagraph>
                            <WatchMore redirectTo="/largescale" />
                        </div>
                        <div>
                            <StyledProjImg src="/storage/images/logo/interwhale.svg" />
                            <StyledParagraph>Interwhale</StyledParagraph>
                            <WatchMore redirectTo="/interwhale" />
                        </div>
                    </Row>
                </Section>

                <Section
                    title="Become a Marinesourcer"
                    subtitle="Gather, Analyze and Understand the Marine Data"
                    redirectTo="/contacts"
                    last
                >
                    <JoinUsContainer>
                        <Row type="flex" align="middle" gutter={24}>
                            <Col xs={24} lg={14}>
                                <h3>Collaborate with Wave</h3>

                                <p>
                                    <span>Companies</span> gather data and use for statistics.
                                </p>

                                <p>
                                    <span>Environmentalists</span> participate in actions and
                                    interactive environments.
                                </p>

                                <p>
                                    <span>Marine biologists</span> study the occurrence and
                                    dynamic flows of cetaceans.
                                </p>
                            </Col>
                            <Col xs={24} lg={10}>
                                <img src="/images/join-us.png" width="100%" />
                            </Col>
                        </Row>
                    </JoinUsContainer>
                </Section>
            </div>
        );
    }
}

export default Homepage;
