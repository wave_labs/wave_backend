import React, { Component } from "react";
import { connect } from "react-redux";
import MapBoxMap from "../../Dashboard/Common/MapBoxMap";
import { Marker } from "react-map-gl";

class MapList extends Component {
  render() {
    return (
      <div>
        {this.props.filter}
        <MapBoxMap
          size={this.props.size}
          fixedSize={true}
        >
          <Marker
            longitude={parseFloat(-16.92564526551496)}
            latitude={parseFloat(32.6600150717693)}
          >
            <img
              src={`${window.location.origin}/storage/images/map-markers/0.png`}
            />
          </Marker>
        </MapBoxMap> 
      </div>
    );
  }
}

export default connect()(MapList);
