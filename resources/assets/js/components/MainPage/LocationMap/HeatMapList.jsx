import React, { Component } from "react";
import { connect } from "react-redux";
import { Layer , Source, Marker} from "react-map-gl";
import MapBoxMap from "../../Dashboard/Common/MapBoxMap";


class HeatMapList extends Component {
  
  render() {
    console.log(this.props.data);
    const geojson = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: { type: "MultiPoint", coordinates: this.props.data },
        },
      ],
    };

    return (
      <div>
        {this.props.filter}
        <MapBoxMap size={this.props.size} fixedSize={true} zoom={10}>
          <Source id="my-data" type="geojson" data={geojson}>
            <Marker
              longitude={parseFloat(-16.92564526551496)}
              latitude={parseFloat(32.6600150717693)}
            >
              <img
                src={`${window.location.origin}/storage/images/map-markers/litterHeat.png`}
              />
            </Marker>
            <Layer
              id="point"
              type="heatmap"
              paint={{
                "heatmap-radius": 20,
                "heatmap-color": [
                  "interpolate",
                  ["linear"],
                  ["heatmap-density"],
                  0,
                  "rgba(0, 0, 255, 0)",
                  0.1,
                  "#ffffb2",
                  0.3,
                  "#feb24c",
                  0.5,
                  "#fd8d3c",
                  0.7,
                  "#fc4e2a",
                  1,
                  "#e31a1c",
                ],
              }}
            />
          </Source>
        </MapBoxMap>
      </div>
    );
  }
}

export default connect()(HeatMapList);
