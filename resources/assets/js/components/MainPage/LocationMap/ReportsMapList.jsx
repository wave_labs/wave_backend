import React, { Component } from "react";
import { connect } from "react-redux";
import MapBoxMap from "../../Dashboard/Common/MapBoxMap";
import { Marker } from "react-map-gl";
import { fetchCenter, fetchIndex } from "redux-modules/coordinate/actions";
import { formatCenter } from "helpers";
import styled from "styled-components";

const StyledImg = styled.img`
  width: 15px;
`;

function checkNull(params) {
  if (params) {
    return true;
  }
}

class ReportsMapList extends Component {
  state = {
    sightingPopUp: null,
    litterPopUp: null,
    divePopUp: null,
    position: {
      lat: parseFloat(32.6),
      lng: parseFloat(-16.9),
    },
  };

  componentDidMount() {
    this.props.fetchCenter().then(() => {
      this.setCenter();
    });
  }

  setCenter = () => {
    this.setState({ position: formatCenter(this.props.center) });
  };

  getLitterMarker = () =>
    `${window.location.origin}/storage/images/map-markers/litterHeat.png`;

  getSightingMarker = () =>
    `${window.location.origin}/storage/images/map-markers/sightingheat.png`;

  render() {
    const { position } = this.state;

    return (
      <div className="map-container teste">
        {position.lat && position.lng && (
          <MapBoxMap fixedSize={true} size="600px" defaultCenter={position}>
            {this.props.litter &&
              this.props.litter.map((litter) => (
                <Marker
                  key={litter.id}
                  longitude={parseFloat(litter.longitude)}
                  latitude={parseFloat(litter.latitude)}
                >
                  <StyledImg
                    onClick={() => this.setState({ litterPopUp: litter })}
                    src={this.getLitterMarker()}
                  />
                </Marker>
              ))}

            {this.props.dive &&
              this.props.dive.map((dive) => (
                <Marker
                  key={dive.id}
                  longitude={parseFloat(dive.longitude)}
                  latitude={parseFloat(dive.latitude)}
                >
                  <StyledImg
                    onClick={() => this.setState({ divePopUp: dive })}
                    src={`${
                      window.location.origin
                    }/storage/images/map-markers/divingheat.svg`}
                  />
                </Marker>
              ))}

            {this.props.filters.marker &&
              !this.props.loadingMarkers &&
              this.props.marker.map((record) =>
                record.activities.map((activities) =>
                  activities.coordinates.map((coordinates) => (
                    <Marker
                      key={coordinates.id}
                      longitude={parseFloat(coordinates.longitude)}
                      latitude={parseFloat(coordinates.latitude)}
                    >
                      <StyledImg
                        src={`${
                          window.location.origin
                        }/storage/images/map-markers/markerheat.svg`}
                      />
                    </Marker>
                  ))
                )
              )}

            {this.props.sighting &&
              this.props.sighting.map((sighting) => (
                <Marker
                  key={sighting.id}
                  longitude={parseFloat(sighting.longitude)}
                  latitude={parseFloat(sighting.latitude)}
                >
                  <StyledImg
                    onClick={() => this.setState({ sightingPopUp: sighting })}
                    src={this.getSightingMarker()}
                  />
                </Marker>
              ))}
          </MapBoxMap>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCenter: () => dispatch(fetchCenter()),
    fetchIndex: (filters) => dispatch(fetchIndex(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    center: state.coordinate.center,
    litter: state.coordinate.litter,
    dive: state.coordinate.dive,
    sighting: state.coordinate.sighting,
    marker: state.marker.coordinates,
    loadingMarkers: state.marker.loadingCoordinates,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportsMapList);
