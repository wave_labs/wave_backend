import React, { Component } from "react";
import { connect } from "react-redux";
import MapBoxMap from "../../Dashboard/Common/MapBoxMap";
import { Marker, Popup } from "react-map-gl";
import { fetchSightingsCoord } from "redux-modules/sighting/actions";
import SightingMapFilter from "../../Dashboard/SightingContent/SightingMapFilter";
import SightingPopUp from "../../Dashboard/SightingContent/SightingPopUp";

class HomePageList extends Component {
  state = {
    sightingPopUp: null,
  };
  componentDidMount() {
    this.props.fetchSightingsCoord();
  }

  showSighting = () => {
    const { sightingPopUp } = this.state;

    return (
      sightingPopUp && (
        <Popup
          tipSize={5}
          longitude={parseFloat(sightingPopUp.longitude)}
          latitude={parseFloat(sightingPopUp.latitude)}
          closeOnClick={false}
          onClose={() => this.setState({ sightingPopUp: null })}
        >
          <SightingPopUp
            viewMore={this.props.viewMore}
            sighting={sightingPopUp}
          />
        </Popup>
      )
    );
  };
  getMarkerLink = (id) =>
    `${window.location.origin}/storage/images/map-markers/32.png`;

  render() {
    return (
      <div className="map-container">
        {this.props.filter && <SightingMapFilter />}
        <MapBoxMap>
          {this.props.sightingsByCoord.map((sighting) => (
            <Marker
              key={sighting.id}
              longitude={parseFloat(sighting.longitude)}
              latitude={parseFloat(sighting.latitude)}
            >
              <img
                onClick={() => this.setState({ sightingPopUp: sighting })}
                src={this.getMarkerLink(sighting.creature.id)}
              />
            </Marker>
          ))}

          {this.showSighting()}
        </MapBoxMap>
      </div>
    );
  }
}

export default connect(
  (state) => ({ sightingsByCoord: state.sighting.sightingsByCoord }),
  { fetchSightingsCoord }
)(HomePageList);
