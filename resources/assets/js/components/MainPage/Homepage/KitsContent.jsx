import React, { Fragment } from "react";
import { Row, Col, List } from "antd";
import styled from "styled-components";
import { isOdd } from "helpers";

const StyledTitle = styled.div`
  font-size: 1.5em;
  color: black;
  display: block;
  margin-bottom: 0;

  @media screen and (max-width: 768px) {

    font-size: 1.3em;
  }
`;

const StyledSubtitle = styled(StyledTitle)`
  font-size: 1.3em;

  @media screen and (max-width: 768px) {
    font-size: 1.1em;
  }
`;

const StyledDescription = styled(StyledSubtitle)`
  color: #777777;
  font-size: 1.1em;

  @media screen and (max-width: 768px) {

    font-size: 1em;
  }
`;

const StyledDownload = styled.img`
  width: 130px;
  display: block;
  margin-top: 15px;
  padding-right: 10px;
  box-sizing: border-box;
  display: inline-block;

  @media screen and (max-width: 1000px) {
    margin:15px  auto;
    width: 120px;
  }
`;

const StyledImage = styled.img`
  width: 100%;
  min-width: 150px;
  max-width: 200px;
  display: block;
  margin: auto;
`;

const StyledRow = styled(Row)`
  margin: 30px auto;
`;

const Item = ({ item, index }) => (
    <StyledRow type="flex" justify="space-around" align="middle">
        <Col
            xs={{ span: 24, order: 2 }}
            lg={{ span: 6, order: isOdd(index) ? 2 : 1 }}
        >
            <StyledImage src={item.image} />
        </Col>
        <Col
            xs={{ span: 24, order: 1 }}
            lg={{ span: 16, order: isOdd(index) ? 1 : 2 }}
        >
            <StyledTitle>{item.name}</StyledTitle>
            <StyledSubtitle>{item.description}</StyledSubtitle>
            <StyledDescription>{item.content}</StyledDescription>

            {item.download && (
                <a href={item.download} target="_blank" rel="noopener noreferrer">
                    <StyledDownload src="/images/google-play.png" />
                </a>
            )}
            {item.ios && (
                <a href={item.ios} target="_blank" rel="noopener noreferrer">
                    <StyledDownload src="/images/apple-store.png" />
                </a>
            )}
        </Col>
    </StyledRow>
);

const KitsContent = ({ data }) => {
    return (
        <Fragment>
            <List
                itemLayout="horizontal"
                dataSource={data}
                renderItem={(item, index) => (
                    <List.Item
                        style={{
                            borderBottom: 0,
                            borderBottomColor: "transparent",
                            padding: 0,
                        }}
                    >
                        <Item item={item} index={index} />
                    </List.Item>
                )}
            />
        </Fragment>
    );
};

export default KitsContent;
