import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';  
import { verify } from  'redux-modules/auth/actions'

class VerifyUser extends Component{

	constructor(props){
		super(props);

		this.props.verify(this.props.match.params.token);
	}

	render() {
		 	return (null)
	}
}

const mapDispatchToProps = dispatch => {
  return {
    verify: (token) => dispatch(verify(token)) 
  };
};

const mapStateToProps = state => {
    return {
        registering: state.registration.registering,
    };
};


export default connect(null, mapDispatchToProps)(VerifyUser);