import React from "react";
import { Row } from "antd";
import styled from "styled-components";
import { dimensions } from "helpers";

const GifContainer = styled.img`
  width: 50%;
  margin: auto;
  display: block;
  min-width: 150px;
`;

const BoundingBoxInstructions = () => {
  return (
    <div>
      <h1>Instructions</h1>
      <p>
        1. Select a class on the dropdown and click on the map to position the
        selection area
      </p>
      <GifContainer
        src="/images/studies/bounding-box/dropdown-instruction.gif"
        alt=""
      />
      <p>
        2. Resize and drag the selection area to fit around the objects as in
        the image below
      </p>
      <GifContainer
        src="/images/studies/bounding-box/bounding-boxes.png"
        alt=""
      />
      <p>
        3. Repeat from step 1 for each class present on the image, to a maximum
        of 5 classes
      </p>
      <p>
        4. Click on the "Next" button when all objects / animals are identified
      </p>
    </div>
  );
};

export default BoundingBoxInstructions;
