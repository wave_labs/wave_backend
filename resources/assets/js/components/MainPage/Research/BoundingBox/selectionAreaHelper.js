let selections = [
    {
        id: 0,
        ...getInitialSelectionState(),
        color: "rgb(72, 64, 65)",
        tColor: "rgba(72, 64, 65, 0.3)",
    },
    {
        id: 1,
        ...getInitialSelectionState(),
        color: "rgba(67, 67, 113, 1)",
        tColor: "rgba(67, 67, 113, 0.3)",
    },
    {
        id: 2,
        ...getInitialSelectionState(),
        color: "rgba(121, 174, 163, 1)",
        tColor: "rgba(121, 174, 163, 0.3)",
    },
    {
        id: 3,
        ...getInitialSelectionState(),
        color: "rgba(112, 238, 156, 1)",
        tColor: "rgba(112, 238, 156, 0.3)",
    },
];

function getInitialSelectionState() {
    return {
        dimensions: {
            height: 1,
            width: 1,
        },
        coordinates: {
            x: 0,
            y: 0,
        },
        isVisible: false,
        class: undefined,
    };
};

export function selectionsInitialState() {
    return selections;
}

export function resetSelections() {
    selections = [
        {
            id: 0,
            ...getInitialSelectionState(),
            color: "rgb(72, 64, 65)",
            tColor: "rgba(72, 64, 65, 0.3)",
        },
        {
            id: 1,
            ...getInitialSelectionState(),
            color: "rgba(67, 67, 113, 1)",
            tColor: "rgba(67, 67, 113, 0.3)",
        },
        {
            id: 2,
            ...getInitialSelectionState(),
            color: "rgba(121, 174, 163, 1)",
            tColor: "rgba(121, 174, 163, 0.3)",
        },
        {
            id: 3,
            ...getInitialSelectionState(),
            color: "rgba(112, 238, 156, 1)",
            tColor: "rgba(112, 238, 156, 0.3)",
        },
    ];
}