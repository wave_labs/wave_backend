import { Modal, Row, Button } from "antd";
import React, { Component } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { createBoundingBoxResult } from "redux-modules/boundingBox/actions";
import BoundingBoxThanks from "./BoundingBoxThanks";
import BoundingBoxInstructions from "./BoundingBoxInstructions";
import BoundingBoxClassification from "./BoundingBoxClassification";

const Thumbnail = styled.img`
  width: 23%;
  min-width: 150px;
  margin: 20px auto;
  cursor: pointer;

  &:hover {
    filter: brightness(50%);
  }
`;

class BoundingBoxContent extends Component {
  constructor(props) {
    super(props);
    this.modal = React.createRef();
    this.state = {
      currentStep: 0,
      formData: {},
      disabled: true,
    };
  }

  componentDidMount() {}

  next = () => {
    const currentStep = this.state.currentStep + 1;
    this.setState({ currentStep });
  };

  prev = () => {
    const currentStep = this.state.currentStep - 1;
    this.setState({ currentStep });
  };

  handleModalClose = () => {
    this.setState({ currentStep: 0 });
    this.props.handleModalClose();
  };

  setFormData = (data) => {
    var { formData } = this.state;

    var newFormData = { ...formData, ...data };

    this.setState({ formData: newFormData });
  };

  handleSubmit = () => {
    setTimeout(() => {
      var { formData } = this.state;

      let form = new FormData();
      form.append("image_id", formData.image_id);
      for (var i = 0; i < formData.classifications.length; i++) {

        form.append(`classifications[]`, formData.classifications[i]);
      }

      this.props.createBoundingBoxResult(form);
    }, 1000);
    this.next();
  };

  handleRepeatClick = (e) => {
    if (e.target.value) {
      this.setState({
        currentStep: 1,
        formData: {},
      });
    } else {
      this.handleModalClose();
    }
  };

  handleDisabled = (value = false) => {
    this.setState({ disabled: value });
  };

  render() {
    var { currentStep, disabled } = this.state;

    const steps = [
      {
        key: 0,
        content: (
          <div>
            <BoundingBoxInstructions />
          </div>
        ),
      },
      {
        key: 1,
        content: (
          <BoundingBoxClassification
            setFormData={this.setFormData}
            handleDisabled={this.handleDisabled}
            playing={!this.props.visible && false}
          />
        ),
      },
      {
        key: 2,
        content: (
          <BoundingBoxThanks handleRepeatClick={this.handleRepeatClick} />
        ),
      },
    ];

    return (
      <div>
        <Modal
          onCancel={this.handleModalClose}
          centered
          visible={this.props.visible}
          closable
          width={960}
          maskClosable={false}
          footer={
            currentStep == 0 ? (
              <Row type="flex" justify="center">
                <Button
                  size="large"
                  key="submit"
                  type="primary"
                  onClick={this.next}
                >
                  Start
                </Button>
              </Row>
            ) : currentStep == 1 ? (
              <Row type="flex" justify="center">
                <Button
                  disabled={disabled}
                  size="large"
                  key="submit"
                  type="primary"
                  onClick={this.handleSubmit}
                >
                  Next
                </Button>
              </Row>
            ) : null
          }
        >
          <div>{steps[currentStep].content}</div>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createBoundingBoxResult: (data) => dispatch(createBoundingBoxResult(data)),
  };
};

const mapStateToProps = (state) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BoundingBoxContent);
