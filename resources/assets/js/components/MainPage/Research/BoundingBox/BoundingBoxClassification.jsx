import { Col, Select, Row, Button, message } from "antd";
import React, { Component } from "react";
import { Selection, SelectionContainer } from "react-selections";
import styled from "styled-components";
import { connect } from "react-redux";
import {
  fetchBoundingBoxImages,
  fetchBoundingBoxClasses,
} from "redux-modules/boundingBox/actions";
import { Fragment } from "react";
import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import { selectionsInitialState, resetSelections } from "./selectionAreaHelper";

const StyledSelectionContainer = styled(SelectionContainer)`
  height: ${(props) => props.height} !important;
`;

const ColorIndicator = styled.div`
  background: ${(props) => props.background};
  height: 20px;
  width: 20px;
  margin-right: 5px;
  border-radius: 50%;
`;

const NoDataMessage = styled.div`
  text-align: center;
  color: #777;
`;

const ClassesContainer = styled.div`
  margin: auto 10px;

  .anticon-delete {
    justify-self: end;
  }
`;

const AddSelectionContainer = styled(Row)`
  margin: 30px auto;
`;

const { Option } = Select;

const displayError = () => {
  message.warning("Maximum number of classes in one image identified.");
};

class BoundingBoxClassification extends Component {
  constructor() {
    super();
    this.state = {
      activeSelections: 0,
      selectionOption: undefined,
      imgDimensions: {
        height: null,
      },
      imgOgDimensions: {},
      selections: selectionsInitialState(),
    };
  }

  updateSelection = (selection) => {
    const nextSelections = this.state.selections.map((s) => {
      if (s.id === selection.id) {
        return selection;
      }

      return s;
    });

    this.setState({
      selections: nextSelections,
    });
  };

  getImageDetails = (e) => {
    let imageSizeIndicators = e.target.getBoundingClientRect();

    let imagePosition = [
      e.clientX - imageSizeIndicators.left,
      e.clientY - imageSizeIndicators.top,
    ];

    const sizeFactor = 0.2;

    let imageSize = [
      imagePosition[1] + imageSizeIndicators.height * sizeFactor >
      imageSizeIndicators.height
        ? imageSizeIndicators.height - imagePosition[1]
        : imageSizeIndicators.height * sizeFactor,
      imagePosition[0] + imageSizeIndicators.width * sizeFactor >
      imageSizeIndicators.width
        ? imageSizeIndicators.width - imagePosition[0]
        : imageSizeIndicators.width * sizeFactor,
    ];

    return {
      imageSize: imageSize,
      imagePosition: imagePosition,
    };
  };

  addSelectionArea = (e) => {
    if (this.state.selectionOption) {
      var { selections, selectionOption } = this.state;

      let imageDetails = this.getImageDetails(e);

      for (var i = 0; i < selections.length; i++) {
        if (!selections[i]["isVisible"]) {
          selections[i] = {
            ...selections[i],
            ...this.getInitialSelectionState(
              i,
              imageDetails.imageSize,
              imageDetails.imagePosition
            ),
            isVisible: true,
            class: selectionOption,
          };
          this.props.handleDisabled();
          break;
        }
        if (i == selections.length - 1) {
          displayError();
        }
      }

      this.setState({
        selections,
        selectionOption: undefined,
        activeSelections: this.state.activeSelections + 1,
      });
    }
  };

  componentDidMount() {
    this.props.fetchBoundingBoxImages({ random: true });
  }

  updateDimensions = (img) => {
    this.setState({
      imgDimensions: {
        width: img.offsetWidth,
        height: img.offsetHeight,
      },
    });
  };

  onImgLoad = ({ target: img }) => {
    this.setState({
      imgDimensions: {
        width: img.offsetWidth,
        height: img.offsetHeight,
      },
      imgOgDimensions: {
        width: img.naturalWidth,
        height: img.naturalHeight,
      },
    });

    window.addEventListener("resize", () => this.updateDimensions(img));
  };

  onImgUnload({ target: img }) {
    img.removeEventListener("resize", this.updateDimensions);
  }

  componentWillUnmount() {
    let { selections, imgDimensions, imgOgDimensions } = this.state;
    let formData = [];
    selections.forEach((selection) => {
      if (selection.isVisible) {
        let square = {
          A: selection.coordinates,
          B: {
            x: selection.coordinates.x + selection.dimensions.width,
            y: selection.coordinates.y + selection.dimensions.height,
          },
        };
        let Rwidth = imgOgDimensions.width / imgDimensions.width; //width ratio
        let Rheight = imgOgDimensions.height / imgDimensions.height; //height ratio
        square = [
          Math.round(Rwidth * square.A.x),
          Math.round(Rheight * square.A.y),
          Math.round(Rwidth * square.B.x),
          Math.round(Rheight * square.B.y),
          selection.class,
        ];

        formData.push(square);
      }
    });
    resetSelections();
    this.props.setFormData({
      classifications: formData,
      image_id: this.props.image.id,
    });
  }

  handleSelect = (e) => {
    this.setState({
      selectionOption: e,
    });
  };

  getInitialSelectionState = (id, size = [1, 1], coords = [0, 0]) => {
    return {
      id: id,
      dimensions: {
        height: size[0],
        width: size[1],
      },
      coordinates: {
        x: coords[0],
        y: coords[1],
      },
    };
  };

  handleDelete = (e) => {
    var { selections, activeSelections } = this.state;

    for (var i = 0; i < selections.length; i++) {
      if (selections[i]["id"] == e) {
        selections[i] = {
          ...selections[i],
          ...this.getInitialSelectionState(i),
          isVisible: false,
          selectionOption: undefined,
        };
        break;
      }
    }
    if (activeSelections - 1 == 0) {
      this.props.handleDisabled(true);
    }
    this.setState({
      selections,
      activeSelections: activeSelections - 1,
    });
  };

  handleImageClick = (e) => {};

  render() {
    let {
      selections,
      imgDimensions,
      selectionOption,
      activeSelections,
    } = this.state;
    let { image, classes } = this.props;
    return (
      <div>
        {image && (
          <Fragment>
            <AddSelectionContainer type="flex">
              <h3>Classes identified: </h3>
              <Select
                style={{ width: "30%", marginLeft: "8px", minWidth: 180 }}
                onChange={this.handleSelect}
                value={selectionOption}
              >
                {classes.map((element) => (
                  <Option key={element.id} value={element.id}>
                    {element.name}
                  </Option>
                ))}
              </Select>
            </AddSelectionContainer>

            <Row>
              <Col span={18}>
                {imgDimensions.height ? (
                  <StyledSelectionContainer
                    height={imgDimensions.height + "px"}
                  >
                    {selections.map((selection, index) => (
                      <Selection
                        key={index}
                        interactive
                        area={selection}
                        style={{
                          opacity: selection.isVisible ? 1 : 0,
                          "--border-color": "#000",
                          "--handle-color": "#000",
                          "--bg-color": selection.tColor,
                        }}
                        onAreaUpdate={this.updateSelection}
                      />
                    ))}

                    <Row>
                      <img
                        onClick={this.addSelectionArea}
                        onLoad={this.onImgLoad}
                        style={{
                          width: "100%",
                          height: "auto",
                          cursor: selectionOption ? "pointer" : "auto",
                        }}
                        src={image.path}
                      />
                    </Row>
                  </StyledSelectionContainer>
                ) : (
                  <Row>
                    <img
                      onLoad={this.onImgLoad}
                      style={{ width: "100%", height: "auto" }}
                      src={image.path}
                    />
                  </Row>
                )}
              </Col>
              <Col span={6}>
                <ClassesContainer>
                  <h2>Classes</h2>
                  {activeSelections == 0 ? (
                    <NoDataMessage>No data identified yet.</NoDataMessage>
                  ) : (
                    selections.map(
                      (selection, index) =>
                        selection.isVisible && (
                          <Row
                            type="flex"
                            justify="space-between"
                            key={index}
                            align="middle"
                          >
                            <Col span={20}>
                              <Row type="flex" align="middle">
                                <ColorIndicator background={selection.color} />
                                <div>
                                  {classes.map(
                                    (element) =>
                                      element.id == selection.class &&
                                      element.name
                                  )}
                                </div>
                              </Row>
                            </Col>

                            <DeleteOutlined
                              onClick={() => this.handleDelete(selection.id)}
                              style={{ cursor: "pointer" }}
                            />
                          </Row>
                        )
                    )
                  )}
                </ClassesContainer>
              </Col>
            </Row>
          </Fragment>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchBoundingBoxImages: (filters) =>
      dispatch(fetchBoundingBoxImages(filters)),
    fetchBoundingBoxClasses: (filters) =>
      dispatch(fetchBoundingBoxClasses(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    image: state.boundingBox.image,
    classes: state.boundingBox.classes,
    loading: state.boundingBox.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BoundingBoxClassification);
