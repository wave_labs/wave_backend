import React from "react";

export const Descriptions = {
    pt: {
        0: "Dentro das atividades de investigação científica a desenvolver, o MARE-Madeira está a avaliar a presença e distribuição de um coral mole em zonas costeiras do arquipélago da Madeira. Desta forma, pedimos a vossa colaboração ao preencherem um breve questionário (que demora, sensivelmente, 5min), com o intuito de nos ajudarem a mapear e rastrear a chegada e distribuição deste coral à região autónoma da Madeira.",
        1: "Nota: O preenchimento deste questionário é totalmente voluntário e anónimo e os dados recolhidos serão analisados de forma conjunta e global.",
        2: "Muito Obrigado",
        3: "Agradecemos a vossa colaboração pelo preenchimento deste questionário e por apoiarem o avanço do conhecimento científico do ambiente marinho na região autónoma da Madeira.",
        4: (
            <p>
                Em caso de posse de informação adicional relativa a este assunto
                que queiram partilhar conosco (ex. número de colónias,
                fotografias), não hesite em contactar-nos para:{" "}
                <a href="mailto:soniafernandezmartin22@gmail.com">
                    soniafernandezmartin22@gmail.com
                </a>
            </p>
        ),
    },
    en: {
        0: "Within the scope of scientific activities and research, MARE-Madeira is assessing this soft coral's presence and distribution in the Madeira Archipelago's coastal waters. We kindly ask for your collaboration by filling out this survey (approximately 5 minutes) to help us map and track the arrival and distribution of this coral in the region.",
        1: "Note: The survey is voluntary and anonymous, and the data collected will be analysed globally.",
        2: "Thank you",
        3: "Thank you for assisting with our research by completing the questionnaire.",
        4: (
            <p>
                If you have additional information regarding your sighting that
                you would like to chare (e.g. number of colonies, photo), please
                contact us at:{" "}
                <a href="mailto:soniafernandezmartin22@gmail.com">
                    soniafernandezmartin22@gmail.com
                </a>
            </p>
        ),
    },
    es: {
        0: "Como parte de sus actividades de investigación científica, MARE-Madeira está evaluando la presencia y distribución de este coral blando en las zonas costeras del archipiélago de Madeira. Por ello, solicitamos su colaboración para rellenar un breve cuestionario (que dura aproximadamente 5 minutos), con el fin de ayudarnos a cartografiar y rastrear la llegada y distribución de este coral a Madeira.",
        1: "Nota: La respuesta a este cuestionario es totalmente voluntaria y anónima, y los datos recogidos se analizarán de forma global.",
        2: "Muchas Gracias",
        3: "Gracias por colaborar con nosotros rellenando este cuestionario y por contribuir al conocimiento científico del medio marino en Madeira.",
        4: (
            <p>
                Si tiene alguna información adicional sobre este tema que le
                gustaría compartir con nosotros (por ejemplo, número de colonias
                o fotografías), no dude en ponerse en contacto con nosotros a
                través de:{" "}
                <a href="mailto:soniafernandezmartin22@gmail.com">
                    soniafernandezmartin22@gmail.com
                </a>
            </p>
        ),
    },
};

export const Alerts = {
    pt: {
        0: "Muito obrigada pela sua colaboração!",
        1: "Por favor aumente o zoom do mapa, de modo a aumentar a precisão",
        2: "Figura",
    },
    en: {
        0: "We thank you for your collaboration!",
        1: "Please increase map's zoom before adding pins in order to increase precision",
        2: "Figure",
    },
    es: {
        0: "Le agradecemos su colaboración.",
        1: "Please increase map's zoom before adding pins in order to increase precision",
        2: "Figure",
    },
};

export const Informations = {
    pt: [
        "Preencha todos os campos necessários antes de avançar",
        "Por favor, posicione o “pin” no mapa (clicar com o botão esquerdo do rato) no local de observação. Em caso de engano, para remover o “pin”, basta clicar com o botão direito do rato.",
        "Onde é que já observou este organismo?",
        "Adicionar avistamento",
    ],
    en: [
        "Select requested information before advancing",
        "Please, position the pin(s) on the map (left click). To remove a pin right click.",
        "Where have you seen the coral?",
        "Add Sighting",
    ],
    es: [
        "Seleccione la información solicitada antes de avanzar",
        "Por favor, marca el lugar de observación en el mapa (haga clic con el botón izquierdo). Si te equivocas, haz clic con el botón derecho para eliminar la marca.",
        "¿Donde lo ha visto a este coral?",
        "añadir otro avistamiento",
    ],
};

export const Questions = {
    pt: [
        {
            q: "Já alguma vez observou esta espécie? *",
            p: "-",
            opt: ["Sim", "Não"],
        },
        { q: "Sabe que organismo é este?", p: "Organismo" },
        {
            q: "Quando é observou pela primeira vez este coral? *",
            p: "-",
            opt: [
                "Antes de 2010",
                "Entre 2010-2015",
                "Entre 2015-2020",
                "Depois 2020",
            ],
        },
        {
            q: "Recorda-se das dimensões aproximadas da colónia? *",
            p: "-",
            opt: [
                "Menor de 10cm",
                "Entre 10-50 cm",
                "Entre 50-100cm",
                "Maior de 100 cm",
            ],
        },
        {
            q: "Desde a primeira observação, considera que a dimensão da colónia se alterou? *",
            p: "-",
            opt: [
                "Aumentou",
                "Manteve-se constante",
                "Diminuiu",
                "Não sei",
                "Só observei uma vez",
            ],
        },
        {
            q: "A que profundidade foi efetuada a observação? *",
            p: "-",
            opt: ["0-5 metros", "5-10 metros", "10-15 metros", "+15 metros"],
        },
    ],
    en: [
        {
            q: "Have you ever seen this species? *",
            p: "-",
            opt: ["Yes", "No"],
        },
        { q: "Do you know what it is?", p: "Specie" },
        {
            q: "Do you remember when you first saw this coral? *",
            p: "-",
            opt: [
                "Before 2010",
                "Between 2010-2015",
                "Between 2015-2020",
                "After 2020",
            ],
        },
        {
            q: "What size was it? *",
            p: "-",
            opt: [
                "Less than 10cm",
                "Between 10-50 cm",
                "Between 50-100cm",
                "More than 100 cm",
            ],
        },
        {
            q: "Since you first saw it, do you consider it has changed in size? *",
            p: "-",
            opt: [
                "Increased",
                "Constant",
                "Decreased",
                "I don't know",
                "I've only seen it once",
            ],
        },
        {
            q: "At what depth was it? *",
            p: "-",
            opt: ["0-5 meters", "5-10 meters", "10-15 meters", "+15 meters"],
        },
    ],
    es: [
        {
            q: "¿Ha observado alguna vez esta especie? *",
            p: "-",
            opt: ["Sí", "No"],
        },
        { q: "¿Sabría decir su nombre?", p: "Nombre" },
        {
            q: "¿Recuerda cuándo vió este coral por primera vez? *",
            p: "-",
            opt: [
                "Antes de 2010",
                "Entre 2010 y 2015",
                "Entre 2015 y 2020",
                "Después de 2020",
            ],
        },
        {
            q: "¿Sabría decir el tamaño aproximado que tenía la colonia? *",
            p: "-",
            opt: [
                "Menos de 10cm",
                "Entre 10 y 50cm",
                "Entre 50 y 100 cm",
                "Más de 100 cm",
            ],
        },
        {
            q: "Desde la primera vez que la vió, ¿cree que ha cambiado de tamaño? *",
            p: "-",
            opt: [
                "Ha aumentado",
                "Se ha mantenido constante",
                "Ha disminuido",
                "No lo sé",
                "Solo he visto una vez",
            ],
        },
        {
            q: "¿A qué profundidad se encontraba? *",
            p: "-",
            opt: [
                "0-5 metros",
                "5-10 metros",
                "10-15 metros",
                "Más de 15 metros",
            ],
        },
    ],
};
