import { message } from "antd";
import React, { Component } from "react";
import { Marker } from "react-map-gl";
import MapBoxMap from "../../../Dashboard/Common/MapBoxMap";
import { Alerts } from "./Translations";

class CoordinatesForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            position: [], 
            activeOnClick: true,
        };
        this.mapContainerRef = React.createRef();
    }

    rules = {
        requiredField: [
            {
                required: true,
                message: "Field is required",
            },
        ],
    };

    getMarkerColor = (index) => {
        let color;
        switch (index) {
            case 0:
                color = "red";
                break;
            case 1:
                color = "blue";
                break;
            default:
                color = "black";
                break;
        }
        return color;
    };

    handleMarkerClick = (index, e) => {
        if (!e.leftButton) {
            this.setState({ position: [] });
        }
    };

    handleMapClick = (e, key) => {
        if (e.leftButton) {
            let zoomLevel = this.getZoomLevel(key);
            if (zoomLevel <= 13.0) {
                message.warning(Alerts[this.props.lang][1]);
            } else {
                let { activeOnClick } = this.state;
                if (activeOnClick) {
                    this.setState({ position: [e.lngLat[1], e.lngLat[0]] });
                }
            }
        }
    };

    handleDragEnd = (event, index) => {
        let { position } = this.state;

        position = [event.lngLat[1], event.lngLat[0]];

        setTimeout(() => {
            this.setState({
                position,
                activeOnClick: true,
            });
        }, 1);
    };
    getZoomLevel = (key) => {
        return this.mapContainerRef.current.getMapZoom();
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.index != this.props.index) {
            this.setState({
                position: [],
            });
        } else if (prevState.position != this.state.position) {
            if (this.state.position.length == 2) {
                var object = {};
                object[
                    this.props.index + "[latitude]"
                ] = this.state.position[0];
                object[
                    this.props.index + "[longitude]"
                ] = this.state.position[1];

                this.props.handlePositions(object);
            }
        }
    }

    render() {
        const { position } = this.state;

        return (
            <div>
                <MapBoxMap
                    style="mapbox://styles/tigerwhale/ckt1c37d70vi317nyz3as5pza"
                    fixedSize={true}
                    size="400"
                    minZoom={8}
                    zoom={8}
                    onClick={(e) => this.handleMapClick(e, position)}
                    ref={this.mapContainerRef}
                >
                    {position[0] && position[1] && (
                        <Marker
                            key={index}
                            latitude={position[0]}
                            longitude={position[1]}
                            draggable
                            onDragEnd={(event) =>
                                this.handleDragEnd(
                                    event,

                                    index
                                )
                            }
                            onDragStart={() =>
                                this.setState({
                                    activeOnClick: false,
                                })
                            }
                        >
                            <svg
                                onContextMenu={(action) =>
                                    this.handleMarkerClick(index, action)
                                }
                                style={{
                                    position: "absolute",
                                    top: "-10px",
                                    left: "-10px",
                                    width: "20px",
                                    height: "20px",
                                    fill: this.getMarkerColor(0),
                                }}
                            >
                                <circle cx="10" cy="10" r="10" stroke="none" />
                            </svg>
                        </Marker>
                    )}
                </MapBoxMap>
            </div>
        );
    }
}

export default CoordinatesForm;
