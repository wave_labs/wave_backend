import { Button, Modal, Row, Select, Carousel } from "antd";
import React, { Component } from "react";

import styled from "styled-components";

import { connect } from "react-redux";
import { alertActions } from "redux-modules/alert/actions";
import { createSoftCoral } from "redux-modules/softCoral/actions";
import { Alerts, Descriptions } from "./Translations";

import SoftCoralForm from "./SoftCoralForm";
import SoftCoralFormSighting from "./SoftCoralFormSighting";

const Option = Select.Option;
const Flag = styled.img`
    height: 25px;
    margin: 2px auto;
    display: block;
`;
const InfoContainer = styled.div`
    margin: 50px auto;
    margin-bottom: 30px;
    h1,
    p {
        text-align: center;
    }

    p {
        margin-bottom: 30px;
        font-size: 1.1em;
        width: 90%;
        display: block;
        margin: auto;
    }
`;

const CreatureImageContainer = styled(Row)`
    width: 90%;
    margin: 0 auto 80px auto;
    gap: 15px;
    position: relative;

    &.ant-row-flex {
        flex-flow: row;
    }

    .ant-carousel {
        background: #ffffff;
        width: 90%;
    }

    & .ant-carousel .slick-slide {
        text-align: center;
        line-height: 160px;
        overflow: hidden;
        width: 100%;
        height: 320px;
        background: #ffffff;
    }

    & .ant-carousel .slick-slide > div {
        border: 1px solid transparent;
    }

    & .ant-carousel .slick-slide > div > div {
        border: 1px solid transparent;
    }

    & .ant-carousel .slick-slide img {
        border: 1px solid transparent;
    }
`;

const FormContainer = styled.div`
    display: block;
    width: 80%;
    margin: 50px auto;

    @media (max-width: 768px) {
        width: 100%;
    }
`;

const StyledImage = styled.div`
    display: block;
    position: relative;
    width: 100%;
    height: 320px;
    background-image: url(${(props) => props.background});
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
`;

class SoftCoralContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            finishForm: false,
            currentStep: 0, // current modal page
            formValidated: true, //form validation for first page
            lang: "pt",
            addonSteps: [],
            formData: {
                coordinates: {},
            },
        };

        // preserve the initial state in a new object
        this.baseState = this.state;
        this.carouselRef = React.createRef();
    }

    next = () => {
        this.setState({
            currentStep: this.state.finishForm ? 3 : this.state.currentStep + 1,
            formValidated: false,
        });
    };

    prev = () => {
        const currentStep = this.state.currentStep - 1;
        this.setState({
            currentStep,
            formValidated: currentStep === 0 ? true : false,
        });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    saveActivityFormRef = (activityFormRef) => {
        this.activityFormRef = activityFormRef;
    };

    handleFormValidation = (data) => {
        if (this.state.currentStep === 1) {
            if (data.knowledge === "Yes") {
                var array = [];
                array.push({
                    key: index + 3,
                    content: (
                        <FormContainer>
                            <SoftCoralFormSighting
                                lang={this.state.lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) =>
                                    (this.formRef = formRef)
                                }
                                data={this.state.formData}
                            />
                        </FormContainer>
                    ),
                });

                this.setState({ addonSteps: array });
            } else {
                this.setState({ addonSteps: [] });
            }
        }
        this.setState({
            formValidated: true,
            formData: { ...this.state.formData, ...data },
        });
    };

    handleSubmit = () => {
        let { formData } = this.state;

        let formQuestions = {
            knowledge: formData.knowledge,
            name: formData.name,
            coordinates: formData.sightings,
        };

        this.props.createSoftCoral(formQuestions).then(() => {
            this.handleModalClose();
        });
    };

    handleModalClose = () => {
        this.setState(this.baseState);
        this.props.handleModalClose();
    };

    handleLanguageChange = (e) => {
        this.setState({
            lang: e,
        });
    };

    render() {
        var { currentStep, formValidated, lang, formData } = this.state;

        const LanguageContainer = () => (
            <Row type="flex" justify="space-around">
                <Select
                    onChange={this.handleLanguageChange}
                    style={{ width: "80px" }}
                    value={lang}
                >
                    <Option value="en">
                        <Flag src="/images/flags/en.png" alt="" />
                    </Option>
                    <Option value="pt">
                        <Flag src="/images/flags/pt.png" alt="" />
                    </Option>
                    <Option value="es">
                        <Flag src="/images/flags/es.png" alt="" />
                    </Option>
                </Select>
            </Row>
        );

        const CarouselButton = (props) => {
            const { invert, onClick } = props;

            return (
                <div
                    onClick={onClick}
                    style={{
                        transform: invert ? "rotate(180deg)" : "rotate(0deg)",
                        cursor: "pointer",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <img
                        src="/storage/images/logo/svg/arrow.svg"
                        alt="arrow"
                        style={{ width: "20px" }}
                    />
                </div>
            );
        };

        const steps = [
            {
                key: 0,
                content: (
                    <InfoContainer>
                        <h1>Soft Coral's Presence</h1>

                        <p>{Descriptions[lang][0]}</p>
                        <br />
                        <p>{Descriptions[lang][1]}</p>
                        <br />
                        <p>{Alerts[lang][0]}</p>

                        <FormContainer>
                            <LanguageContainer />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            {
                key: 1,
                content: (
                    <InfoContainer>
                        <CreatureImageContainer
                            type="flex"
                            justify="space-around"
                        >
                            <CarouselButton
                                onClick={() => this.carouselRef.current.prev()}
                            />
                            <Carousel
                                autoplaySpeed={2500}
                                draggable
                                ref={this.carouselRef}
                                autoplay
                            >
                                <div>
                                    <StyledImage
                                        background={
                                            "storage/images/corals/1.png"
                                        }
                                    />
                                </div>
                                <div>
                                    <StyledImage
                                        background={
                                            "storage/images/corals/2.jpg"
                                        }
                                    />
                                </div>
                                <div>
                                    <StyledImage
                                        background={
                                            "storage/images/corals/3.jpg"
                                        }
                                    />
                                </div>
                                <div>
                                    <StyledImage
                                        background={
                                            "storage/images/corals/4.jpg"
                                        }
                                    />
                                </div>{" "}
                                <div>
                                    <StyledImage
                                        background={
                                            "storage/images/corals/5.jpg"
                                        }
                                    />
                                </div>
                                <div>
                                    <StyledImage
                                        background={
                                            "storage/images/corals/6.jpg"
                                        }
                                    />
                                </div>
                            </Carousel>
                            <CarouselButton
                                invert
                                onClick={() => this.carouselRef.current.next()}
                            />
                        </CreatureImageContainer>

                        <FormContainer>
                            <SoftCoralForm
                                lang={lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) =>
                                    (this.formRef = formRef)
                                }
                                data={this.state.formData}
                                finishForm={(finish = true) =>
                                    this.setState({
                                        finishForm: finish,
                                        formValidated: finish,
                                    })
                                }
                            />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            ...this.state.addonSteps,
            {
                key: 3,
                content: (
                    <InfoContainer>
                        <h1>{Descriptions[lang][2]}</h1>

                        <p>{Descriptions[lang][3]}</p>
                        {Descriptions[lang][4]}
                    </InfoContainer>
                ),
            },
        ];

        return (
            <Modal
                onCancel={this.handleModalClose}
                centered
                visible={this.props.visible}
                closable
                style={{ maxWidth: "960px", color: "black" }}
                width={"100%"}
                footer={[
                    currentStep < 1 ? (
                        <Row key={0} type="flex" justify="center">
                            <Button
                                disabled={!formValidated}
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.next}
                            >
                                Next
                            </Button>
                        </Row>
                    ) : currentStep == steps.length - 1 ? (
                        <Row key={2} type="flex" justify="center" gutter={16}>
                            <Button
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.handleSubmit}
                            >
                                Close
                            </Button>
                        </Row>
                    ) : (
                        <Row key={1} type="flex" justify="center" gutter={16}>
                            <Button size="large" onClick={this.prev}>
                                Back
                            </Button>
                            <Button
                                disabled={!formValidated}
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.next}
                            >
                                Next
                            </Button>
                        </Row>
                    ),
                ]}
            >
                <div>{steps[currentStep].content}</div>
            </Modal>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createSoftCoral: (data) => dispatch(createSoftCoral(data)),
        errorAlert: (data) => dispatch(alertActions.error(data)),
        successAlert: (data) => dispatch(alertActions.success(data)),
    };
};

const mapStateToProps = (state) => {
    return {
        creatureDimensions: state.creature.dimensions,
        creatureActivities: state.creature.activities,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SoftCoralContent);
