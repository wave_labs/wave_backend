import React, { Component, Fragment } from "react";
import { Row, Col, Select, Form, Input, Button, Icon, Checkbox } from "antd";
import { Questions, Informations } from "./Translations";
import { isArrayNull } from "./helper";
import styled from "styled-components";
import CoordinatesForm from "./CoordinatesForm";

const FormItem = Form.Item;
const Option = Select.Option;

const Notice = styled.p`
    font-weight: bold;
    margin: 40px auto 30px auto !important;
`;

class SoftCoralFormSighting extends Component {
    state = {
        occurrences: 1,
        remembers: false,
    };

    rules = {
        nullableField: [
            {
                required: false,
                message: "Field is required",
            },
        ],
        requiredField: [
            {
                required: true,
                message: "Field is required",
            },
        ],
    };

    handleFormChange = () => {
        setTimeout(() => {
            let form = this.props.form.getFieldsValue();
            var hasEmptyField = false;

            [...Array(this.state.occurrences)].map((_, index) => {
                if (
                    !form["sightings"][index]["year"] ||
                    !form["sightings"][index]["dimensions"] ||
                    !form["sightings"][index]["dimension_change"] ||
                    !form["sightings"][index]["depth"] ||
                    !form["sightings"][index]["latitude"] ||
                    !form["sightings"][index]["longitude"]
                )
                    hasEmptyField = true;
            });

            if (!hasEmptyField) {
                this.props.handleFormValidation({ ...form });
            }
        }, 5);
    };

    componentDidUpdate(prevProps) {
        if (prevProps.animal != this.props.animal) {
            this.setState({
                occurrences: 1,
            });
        }
    }

    handleCoordinateChange = (object) => {
        this.props.form.setFieldsValue(object);
        this.handleFormChange();
    };

    handleNewSighting = () => {
        this.setState({ occurrences: this.state.occurrences + 1 });
    };

    render() {
        const { lang, data } = this.props;
        const { getFieldDecorator } = this.props.form;
        let question = Questions[lang];

        const SelectFormItem = ({
            fieldName,
            index,
            handleField = this.handleFormChange,
        }) => (
            <Col span={24}>
                <FormItem label={question[index].q}>
                    {getFieldDecorator(fieldName, {
                        rules: this.rules.requiredField,
                        initialValue: isArrayNull(data[fieldName]),
                    })(
                        <Select
                            onChange={handleField}
                            placeholder={question[index].p}
                        >
                            {question[index].opt.map((option, i) => (
                                <Option
                                    key={i}
                                    value={Questions["en"][index].opt[i]}
                                >
                                    {option}
                                </Option>
                            ))}
                        </Select>
                    )}
                </FormItem>
            </Col>
        );

        return (
            <Form hideRequiredMark className="wrap-label">
                <Notice>{Informations[lang][2]}</Notice>

                {[...Array(this.state.occurrences)].map((_, index) => (
                    <Fragment>
                        <Col xs={24}>
                            <h4>{Informations[lang][1]}</h4>
                            <CoordinatesForm
                                lang={lang}
                                index={"sightings" + "[" + index + "]"}
                                handlePositions={(positions) =>
                                    this.handleCoordinateChange(positions)
                                }
                            />
                        </Col>
                        <Col xs={24}>
                            <Row gutter={16}>
                                <SelectFormItem
                                    fieldName={
                                        "sightings" + "[" + index + "][year]"
                                    }
                                    index={2}
                                />
                                <SelectFormItem
                                    fieldName={
                                        "sightings" +
                                        "[" +
                                        index +
                                        "][dimensions]"
                                    }
                                    index={3}
                                />
                                <SelectFormItem
                                    fieldName={
                                        "sightings" +
                                        "[" +
                                        index +
                                        "][dimension_change]"
                                    }
                                    index={4}
                                />
                                <SelectFormItem
                                    fieldName={
                                        "sightings" + "[" + index + "][depth]"
                                    }
                                    index={5}
                                />

                                {getFieldDecorator(
                                    "sightings" + "[" + index + "][latitude]",
                                    { initialValue: undefined }
                                )}
                                {getFieldDecorator(
                                    "sightings" + "[" + index + "][longitude]",
                                    { initialValue: undefined }
                                )}
                            </Row>
                        </Col>
                    </Fragment>
                ))}

                <Button type="dashed" onClick={this.handleNewSighting}>
                    <Icon type="plus" /> {Informations[lang][3]}
                </Button>
            </Form>
        );
    }
}

export default Form.create()(SoftCoralFormSighting);
