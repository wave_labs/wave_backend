import { Tooltip, Modal, Row, Button, Select } from "antd";
import React, { Component } from "react";

class ModalSteps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 0, // current modal page
      visible: true, // current modal page
    };
  }

  next = () => {
    const currentStep = this.state.currentStep + 1;
    this.setState({ currentStep });
  };

  prev = () => {
    const currentStep = this.state.currentStep - 1;
    this.setState({ currentStep });
  };

  handleModalClose = () => {
    this.setState({ visible: false });
  };

  render() {
    var { currentStep, visible } = this.state;
    var { steps, footer } = this.props;

    return (
      <Modal
        onCancel={this.handleModalClose}
        centered
        visible={visible}
        closable
        style={{ top: "80px", color: "black" }}
        width={960}
        footer={footer}
      >
        <div>{steps[currentStep].content}</div>
      </Modal>
    );
  }
}

export default ModalSteps;
