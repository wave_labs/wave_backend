import React from "react";
import { Row, Avatar } from "antd";
import styled from "styled-components";
import { StyledTitle, dimensions } from "helpers";
import { advisory, team, collaborators, contributors } from "./researchHelper";

const Container = styled.div`
  width: 100%;
`;

const SectionTitle = styled(StyledTitle)`
  font-size: 36px;
  text-align: center;
  margin-top: 5%;
  font-weight: 700;
  line-height: 1.3;
  margin-bottom: 1%;

  @media screen and (max-width: ${dimensions.lg}) {
    font-size: 28px;
  }
`;

const SectionSubtitle = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
  margin-bottom: 1%;

  @media screen and (max-width: ${dimensions.lg}) {
    font-size: 18px;
  }
`;

const StyledContributor = styled.div`
  text-align: center;
  font-weight: bold;
  margin: 2% 0%;
  width: 30%;

  @media screen and (max-width: ${dimensions.sm}) {
    width: 50%;
  }
`;


const CardContainer = styled.div`
  width: 33%;
  padding: 25px 10px;
  box-sizing: border-box;
  text-align: center;

  @media screen and (max-width: ${dimensions.md}) {
    width: 50%;
  }

  img {
    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.2);
    margin: auto;
    display: block;
    width: 140px;
    height: 140px;
    border-radius: 140px;
  }

  h3 {
    margin: 10px auto;
    font-size: 20px;

    @media screen and (max-width: ${dimensions.md}) {
      font-size: 18px;
    }
  }

  p {
    font-size: 16px;
    color: #585858;
  }
`;

const TeamMember = ({ avatar, name, job, lazy }) => (
  <CardContainer>
    <img
      src={"/images/team/" + avatar + ".png"}
      alt={name}
      loading={lazy ? "lazy" : "eager"}
    />

    <h3>{name}</h3>
    <p>{job}</p>
  </CardContainer>
);

const Section = ({ title, subtitle, children }) => (
  <div>
    <SectionTitle>{title}</SectionTitle>
    <SectionSubtitle>{subtitle}</SectionSubtitle>
    <Row type="flex" justify="space-around" align="top">
      {children}
    </Row>
  </div>
);

const Team = () => {
  return (
    <Container>
      <StyledTitle>Wave Team</StyledTitle>

      <Row type="flex" justify="center" align="middle">
        <TeamMember avatar="marko" name="Marko Radeta" job="Coordinator (PI)" />
      </Row>

      <Row type="flex" justify="space-around" align="top">
        {team.map((member, index) => (
          <TeamMember
            key={index}
            avatar={member.avatar}
            name={member.name}
            job={member.job}
          />
        ))}
      </Row>

      <Section
        title="Scientific Advisory"
        subtitle="United Marine Biologists and HCI Specialists"
      >
        {advisory.map((member, index) => (
          <TeamMember
            key={index}
            avatar={member.avatar}
            name={member.name}
            job={member.job}
            lazy
          />
        ))}
      </Section>

      <Section
        title="Collaborators"
      >

        {collaborators.map((member, index) => (
          <TeamMember
            key={index}
            avatar={member.avatar}
            name={member.name}
            job={member.job}
            lazy
          />
        ))}
      </Section>

      <Section
        title="Past Contributors"
        //subtitle="Engineers, Designers and Research Assistants"
      >
        {contributors.map((member, index) => (
          <StyledContributor key={index}>{member.name}</StyledContributor>
        ))}
      </Section>
    </Container>
  );
};

export default Team;
