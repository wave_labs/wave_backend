import React from "react";
import { Row, Col, Button, List } from "antd";
import styled from "styled-components";
import { TitleSection, dimensions } from "helpers";
import { publications } from "./researchHelper";
import { Link } from "react-router-dom";
import { Fragment } from "react";

const StyledTitleDescription = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
  margin-bottom: 50px;
`;

const StyledSubTitle = styled.h2`
  font-size: 1.1rem;
  margin-top: 3%;

  @media screen and (max-width: ${dimensions.xs}) {
    font-size: 0.9rem;
  }
`;

const Container = styled.div`
  width: 90%;
  margin-bottom: 5%;
  margin: auto;

  @media (max-width: ${dimensions.xl}) {
    text-align: center;
  }
`;

const Authors = styled.div`
  font-size: 0.8rem;
  color: #777777;
`;

const Conferences = styled(Authors)`
  font-style: italic;
`;

const StyledRow = styled(Row)`
  margin: 30px auto;
`;

const StyledImage = styled.img`
  width: 100%;
  min-width: 50px;
  max-width: 300px;
  margin: auto;
  display: block;

  @media screen and (max-width: ${dimensions.md}) {
    width: 70%;
  }
`;

const DownloadButton = styled(Link)`
  font-size: 1rem;
  display: block;
  text-decoration: none;
  color: black;
  margin-top: 20px;

  &:hover {
    color: #777777;
  }

  @media screen and (max-width: ${dimensions.xl}) {
    display: flex;
    justify-content: center;
  }
`;

const PublicationTitle = styled.h1`
  font-size: 2.5em;
  text-align: center;
`;

const PublicationSection = ({ title, subtitle, list }) => {
  return (
    <Fragment>
      <PublicationTitle>{title}</PublicationTitle>
      <StyledTitleDescription>{subtitle}</StyledTitleDescription>

      <List
        itemLayout="horizontal"
        dataSource={list}
        renderItem={(item) => (
          <List.Item
            style={{
              borderBottom: 0,
              borderBottomColor: "transparent",
              padding: 0,
            }}
          >
            <StyledRow type="flex" align="middle" gutter={16}>
              <Col xs={24} lg={10}>
                <StyledImage src={item.image} />
              </Col>
              <Col xs={24} lg={12}>
                <StyledSubTitle>{item.title}</StyledSubTitle>
                <Authors>{item.authors}</Authors>
                <Conferences>{item.conference}</Conferences>
                <DownloadButton to={item.file} target="_blank" download>
                  <Button ghost="true" size="large" type="primary">
                    Download
                  </Button>
                </DownloadButton>
              </Col>
            </StyledRow>
          </List.Item>
        )}
      />
    </Fragment>
  );
};

const Publications = () => {
  return (
    <Container>
      <TitleSection
        title="Publications"
        subtitle="Double-blind Peer Reviewed Research Articles"
      />

      <PublicationSection
        title="Apps"
        subtitle="Collect the data with citizen science"
        list={publications.app}
      />

      <PublicationSection
        title="IoT"
        subtitle="Interpret the data using AI"
        list={publications.iot}
      />

      <PublicationSection
        title="Domes"
        subtitle="Interact with the data in immersive environments"
        list={publications.dome}
      />
    </Container>
  );
};

export default Publications;
