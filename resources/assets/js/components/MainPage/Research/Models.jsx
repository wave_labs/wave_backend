import React from 'react'
import styled from "styled-components";
import { TitleSection } from '../../../helpers';
import { Button } from 'antd';

const Element = styled.div`
    margin-bottom: 50px;
    display: flex;
    gap: 30px;
    align-items: flex-start;

    h3 {
        font-size: 18px;
    }

    .description {
        margin-bottom: 0px;
    }

    .files {
        opacity: .6;
        font-size: 12px;
        margin: 5px 0px 15px 0px;
    }
`;

function Models() {

    const data = [
        {
            title: "OCEANUS object detection models",
            files: "7 Models",
            description: "",
            size: "3,8GB",
            img: "/images/models/oceanus.jpg",
            download: "/download/oceanus_models.zip"
        },
        {
            title: "Watkins Marine Mammal Sound Database image classification model",
            files: "1 Model",
            description: "The William A. Watkins Collection of Marine Mammal Sound Recordings consists of recordings of various marine mammal species collected over a span of seven decades in a wide range of geographic areas by Watkins and many others.",
            size: "11,7MB",
            img: "/images/datasets/watkins.jpg",
            download: "/download/MNV3_watkins_model.zip"

        },
        {
            title: "MARE-Madeira acoustic image classification model",
            files: "1 Model",
            description: "",
            size: "37,2MB",
            img: "/images/datasets/mare_spectograms.jpg",
            download: "/download/MNV3_mare_madeira_acoustics.zip"

        }
    ];

    return (
        <div>
            <TitleSection
                title="AI Models"
                subtitle="Download some of the models we developed over the years."
            />

            {data.map(dataset => (
                <Element key={dataset.title}>
                    <img src={dataset.img} alt="" />
                    <div>
                        <h3>{dataset.title}</h3>
                        <p className='description'>{dataset.description}</p>
                        <p className='files'>{dataset.files}</p>

                        <a href={dataset.download} download><Button ghost="true" type="primary">Download ({dataset.size})</Button></a>
                    </div>
                </Element>
            ))}

        </div>
    )
}

export default Models