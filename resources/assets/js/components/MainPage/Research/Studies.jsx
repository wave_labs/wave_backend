import React, { Component } from "react";
import { Row, Col, Button } from "antd";
import styled from "styled-components";
import { StyledTitle, dimensions } from "helpers";
import { connect } from "react-redux";
import CreatureRemoteSelectContainer from "../../Dashboard/Data/CreatureContent/CreatureRemoteSelectContainer";
import MarkerContent from "./MarkerContent/MarkerContent";
import {
    fetchCreatureSize,
    fetchCreatureActivities,
} from "redux-modules/creature/actions";
import { studies } from "./researchHelper";
import StopwatchContent from "./Stopwatch/StopwatchContent";
import BoundingBoxContent from "./BoundingBox/BoundingBoxContent";
import FreshwaterPinContent from "./FreshwaterPin/FreshwaterPinContent";
import DiademaContent from "./Diadema/DiademaContent";
import TroutContent from "./Trout/TroutContent";
import ClimarestContent from "./Climarest/ClimarestContent";
import { TitleSection } from "../../../helpers";
import SoftCoralContent from "./SoftCoral/SoftCoralContent";

const Container = styled.div`
    width: 80%;
    margin: auto;

    @media (max-width: ${dimensions.xl}) {
        width: 96%;
    }
`;

const Study = styled.div`
    font-size: 1.6em;
    margin-top: 3%;

    @media (max-width: ${dimensions.sm}) {
        font-size: 1.2em;
        text-align: center;
    }
`;

const StudyDescription = styled(Study)`
    color: #777777;
    font-size: 1em;
    margin: 0;

    @media (max-width: ${dimensions.sm}) {
        font-size: 1em;
    }
`;

const StyledImage = styled.img`
    width: 80%;
    min-width: 50px;
    max-width: 300px;
    margin: 20px auto;
    display: block;

    @media (max-width: ${dimensions.sm}) {
        width: 50%;
    }
`;

const Element = styled.div`
    margin-bottom: 50px;
    display: flex;
    gap: 30px;
    align-items: flex-start;

    h3 {
        font-size: 18px;
    }

    .description {
        margin-bottom: 0px;
    }

    .files {
        opacity: 0.6;
        font-size: 12px;
        margin: 5px 0px 15px 0px;
    }
`;

class Studies extends Component {
    state = {
        searchParams: {},
        loadingParams: true,
        random: false,
    };

    componentDidMount() {
        var { search } = this.props.location;
        var { searchParams } = this.state;
        const params = new URLSearchParams(search);
        params.forEach((value, key) => {
            if (value) {
                searchParams[key] = value;
            }
        });

        if (!searchParams["creatureId"]) {
            searchParams["creatureId"] = Math.floor(Math.random() * 54) + 1;
        }

        this.setState({ searchParams });

        if (searchParams["study"] == "markers") {
            this.handleCreatureData(searchParams["creatureId"]);
        }

        this.setState({ loadingParams: false });
    }

    handleCreatureData = (id, filters = {}) => {
        this.props.fetchCreatureSize(id);
        this.props.fetchCreatureActivities(id, filters);
    };

    handleModalClose = () => {
        this.setState({ searchParams: {} });
    };

    handleSearchParamsChange = (key, value) => {
        var { searchParams, random } = this.state;
        searchParams[key] = value;

        if (value == "markers") {
            if (!searchParams.creatureId) {
                searchParams.creatureId = Math.floor(Math.random() * 54) + 1;
                random = true;
            }

            this.handleCreatureData(searchParams.creatureId, {
                language: "pt",
            });
        } else if (value == "stopwatch") {
        }

        this.setState({ searchParams, random });
    };

    render() {
        var { searchParams, loadingParams, random } = this.state;

        const resources = [
            {
                title: "Stopwatch annotation videos",
                files:
                    "56 videos, 28 in original state and 28 with AI annotations",
                description:
                    "Web-based stopwatch of marine occurrences in various videos, providing comparison metrics with AI generated detections.",
                size: "4,2GB",
                img: "/images/studies/stopwatch_videos.jpg",
                download: "/download/stopwatch_videos.zip",
            },
        ];

        const StudyContainer = ({ element, children }) => {
            return (
                <Row
                    type="flex"
                    align="middle"
                    justify="space-around"
                    key={element.name}
                >
                    <Col xs={24} sm={8}>
                        <StyledImage src={element.image} />
                    </Col>
                    <Col xs={24} sm={14}>
                        <Study>{element.name}</Study>
                        <StudyDescription>
                            {element.description}
                        </StudyDescription>
                        <Row
                            type="flex"
                            justify="space-between"
                            align="middle"
                            gutter={16}
                            style={{ margin: "10px auto" }}
                        >
                            {children}

                            <Button
                                ghost="true"
                                type="primary"
                                onClick={() =>
                                    this.handleSearchParamsChange(
                                        "study",
                                        element.index
                                    )
                                }
                            >
                                Start
                            </Button>
                        </Row>
                    </Col>
                </Row>
            );
        };

        const MarkersCreature = () => {
            return (
                <div style={{ width: "50%" }}>
                    <CreatureRemoteSelectContainer
                        onChange={(value) =>
                            this.handleSearchParamsChange("creatureId", value)
                        }
                        value={
                            searchParams.creatureId
                                ? searchParams.creatureId
                                : undefined
                        }
                        scientific
                        placeholder="List of studied species"
                    />
                </div>
            );
        };

        return (
            <div>
                <Container>
                    <TitleSection
                        title="Studies"
                        subtitle="Help us protect these species worldwide."
                    />

                    {!loadingParams && (
                        <MarkerContent
                            handleCreatureData={this.handleCreatureData}
                            handleModalClose={this.handleModalClose}
                            visible={
                                searchParams.study &&
                                searchParams.study == "markers" &&
                                true
                            }
                            creatureId={
                                searchParams.creatureId &&
                                searchParams.creatureId
                            }
                            random={random}
                        />
                    )}

                    <StopwatchContent
                        visible={
                            searchParams.study &&
                            searchParams.study == "stopwatch" &&
                            true
                        }
                        handleModalClose={this.handleModalClose}
                    />

                    <BoundingBoxContent
                        visible={
                            searchParams.study &&
                            searchParams.study == "bounding-box" &&
                            true
                        }
                        handleModalClose={this.handleModalClose}
                    />

                    <DiademaContent
                        handleModalClose={this.handleModalClose}
                        visible={
                            searchParams.study &&
                            searchParams.study == "diadema" &&
                            true
                        }
                    />

                    <FreshwaterPinContent
                        handleModalClose={this.handleModalClose}
                        visible={
                            searchParams.study &&
                            searchParams.study == "freshwater-pin" &&
                            true
                        }
                        creatureId={
                            searchParams.creatureId && searchParams.creatureId
                        }
                    />

                    <TroutContent
                        handleModalClose={this.handleModalClose}
                        visible={
                            searchParams.study &&
                            searchParams.study == "freshwater-vertebrates" &&
                            true
                        }
                    />

                    <ClimarestContent
                        handleModalClose={this.handleModalClose}
                        visible={
                            searchParams.study &&
                            searchParams.study == "climarest" &&
                            true
                        }
                    />

                    <SoftCoralContent
                        handleModalClose={this.handleModalClose}
                        visible={
                            searchParams.study &&
                            searchParams.study == "soft-corals-presence" &&
                            true
                        }
                    />

                    {studies.map((element, index) => (
                        <StudyContainer key={index} element={element}>
                            {index == 1 ? <MarkersCreature /> : <div />}
                        </StudyContainer>
                    ))}
                </Container>

                <TitleSection
                    title="Resources"
                    subtitle="Download some of the resources employed in our studies."
                />

                {resources.map((dataset) => (
                    <Element key={dataset.title}>
                        <img src={dataset.img} alt="" />
                        <div>
                            <h3>{dataset.title}</h3>
                            <p className="description">{dataset.description}</p>
                            <p className="files">{dataset.files}</p>

                            <a href={dataset.download} download>
                                <Button ghost="true" type="primary">
                                    Download ({dataset.size})
                                </Button>
                            </a>
                        </div>
                    </Element>
                ))}
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCreatureSize: (id) => dispatch(fetchCreatureSize(id)),
        fetchCreatureActivities: (id, filters) =>
            dispatch(fetchCreatureActivities(id, filters)),
    };
};

export default connect(null, mapDispatchToProps)(Studies);
