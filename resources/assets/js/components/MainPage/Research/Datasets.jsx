import React from 'react'
import styled from "styled-components";
import { TitleSection } from '../../../helpers';
import { Button } from 'antd';

const Element = styled.div`
    margin-bottom: 50px;
    display: flex;
    gap: 30px;
    align-items: flex-start;

    h3 {
        font-size: 18px;
    }

    .description {
        margin-bottom: 0px;
    }

    .files {
        opacity: .6;
        font-size: 12px;
        margin: 5px 0px 15px 0px;
    }
`;

function Datasets() {

    const datasets = [
        {
            title: "Annotated Watkins Marine Mammal Sound Database",
            files: "2 Folders (images as jpg, annotations as json)",
            description: "The William A. Watkins Collection of Marine Mammal Sound Recordings consists of recordings of various marine mammal species collected over a span of seven decades in a wide range of geographic areas by Watkins and many others.",
            size: "2,9GB",
            img: "/images/datasets/watkins.jpg",
            download: "/download/watkins.zip"
        },
        {
            title: "MARE-Madeira Marine Mammal Sound Database",
            files: "3 Folders (images as jpg, annotations as json, sounds as wav)",
            description: "",
            size: "185MB",
            img: "/images/datasets/mare_spectograms.jpg",
            download: "/download/mare_spectograms.zip"

        }
    ];

    return (
        <div>
            <TitleSection
                title="Datasets"
                subtitle="Download some of the datasets created and used by us."
            />

            {datasets.map(dataset => (
                <Element key={dataset.title}>
                    <img src={dataset.img} alt="" />
                    <div>
                        <h3>{dataset.title}</h3>
                        <p className='description'>{dataset.description}</p>
                        <p className='files'>{dataset.files}</p>

                        <a href={dataset.download} download><Button ghost="true" type="primary">Download ({dataset.size})</Button></a>
                    </div>
                </Element>
            ))}

        </div>
    )
}

export default Datasets