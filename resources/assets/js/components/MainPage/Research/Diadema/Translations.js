import React from 'react'

export const Title = {
    pt: "Viste ouriços doentes ou mortos nas últimas duas semanas?",
    en: "Have you seen sick or dead hedgehogs in the last two weeks?"
}

export const Header = {
    pt: {
        title: "Colabora connosco e mostra-nos onde!",
        description: "Este ano os ouriços-de-espinhos-longos (Diadema africanum) estão novamente a adoecer e morrer. O MARE-Madeira está a fazer uma avaliação cada duas semanas sobre a distribuição da mortalidade dos ouriços nas águas costeiras do Arquipélago de Madeira. Assim pedimos a vossa colaboração com o preenchimento deste inquérito (aproximadamente 3 minutos) para nos ajudar a mapear e avaliar a progressão da doença na RAM entre 20 e 31 de Julho.",
        thankyou: (<span>Este inquérito enquadra-se nas atividades de investigação do MARE-Madeira/Arditi. Se está interessado no resultado de esta (e outras) investigação visite-nos em <a href="https://mare-madeira.pt/" target="_blank">https://mare-madeira.pt/</a></span>)
    },
    en: {
        title: "Collaborate with us and show us where!",
        description: "This year the longthorned urchins (Diadema africanum) are sickening and dying again. MARE-Madeira is carrying out an assessment every two weeks on the distribution of urchin mortality in the coastal waters of the Madeira Archipelago. Therefore, we ask for your collaboration in completing this survey (approximately 3 minutes) to help us map and assess the progression of the disease in RAM between the 20th and 31st of July.",
        thankyou: (<span>This survey is part of the research activities of MARE-Madeira/Arditi. If you are interested in the results of this (and other) investigation, visit us at <a href="https://mare-madeira.pt/" target="_blank">https://mare-madeira.pt/</a></span>)
    }
}

export const Repeat = {
    pt: {
        title: "Vamos repetir?",
        description: "Existe mais um avistamento que pretende partilhar connosco? Selecione a opção sim caso queira submeter um novo avistamento.",
        yes: "Sim",
        no: "Não"
    },
    en: {
        title: "Let's repeat?",
        description: "Is there one more sighting you want to share with us? Select the yes option if you want to submit a new sighting.",
        yes: "Yes",
        no: "No"
    }
}

export const Alerts = {
    pt: {
        0: "Muito obrigada pela sua colaboração!",
        1: "Por favor aumente o zoom do mapa, de modo a aumentar a precisão",
        2: "Figura",
    },
    en: {
        0: "We thank you for your collaboration!",
        1: "Please increase map's zoom before adding pins in order to increase precision",
        2: "Figure",
    }
}

export const Status = {
    pt: {
        0: "Saudável",
        1: "Doente",
        2: "Morto",
    },
    en: {
        0: "Healthy",
        1: "Sick",
        2: "Dead",
    }
}


export const Informations = {
    pt: [
        'Preencha todos os campos necessários antes de avançar',
        'Selecione uma opção temporal na qual tenha avistado a espécie em questão e posicione o(s) ponto(s) no mapa. Repita o processo para cada opção temporal respetivamente',
        'Clique no lado esquerdo do rato para criar um novo ponto ou clique no lado direto do rato para o remover ',
        'As perguntas que se seguem referem-se à sua última observação'
    ],
    en: [
        'Select requested information before advancing',
        'Select a temporal option in which you detected this species and position the pin(s) on the map. Repeat the process for each of the options respectively',
        'Left click to create a new pin or right click to delete it',
        'Following questions refer to your latest observation'
    ],
}
export const Questions = {
    pt: [
        { q: "Posiciona o pin no mapa no sítio onde viste ouriços doentes ou mortos" },
        { q: "Relativamente á quantidade total de ouriços existente nessa zona, que percentagem de ouriços doentes calcula ter visto?", p: "Selecione uma das opções abaixo de acordo com a sua estimativa", opt: ['0%', '1-10%', '11-40%', '41-70%', '71-90%', '>90%'] },
        { q: "Relativamente á quantidade total de ouriços existente nessa zona, que percentagem de ouriços mortos calcula ter visto?", p: "Selecione uma das opções abaixo de acordo com a sua estimativa", opt: ['0%', '1-10%', '11-40%', '41-70%', '71-90%', '>90%'] },
    ],
    en: [
        { q: "Place the pin on the map where you have seen sick or dead hedgehogs" },
        { q: "Regarding the total amount of hedgehogs in that area, what percentage of sick hedgehogs do you think you have seen?", p: "Choose one of the ranges below according to your guess", opt: ['0%', '1-10%', '11-40%', '41-70%', '71-90%', '>90%'] },
        { q: "Regarding the total amount of hedgehogs in that area, what percentage of dead hedgehogs do you think you have seen?", p: "Choose one of the ranges below according to your guess", opt: ['0%', '1-10%', '11-40%', '41-70%', '71-90%', '>90%'] },
    ],
}