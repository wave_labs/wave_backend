import React from 'react'
import styled from "styled-components";

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;

  div {
    padding: 15px 30px;
    box-sizing: border-box;
    margin: 0 30px;
    cursor: pointer;
    color: blue;
    border: 1px solid blue;  
    transition: all .4s ease;
    border-radius: 4px;
    
    &:hover {
        border: 1px solid darkblue; 
        color: darkblue;
    }
  }

  .background {
    background: blue;
    color: white;
    border: 0px;

    &:hover {
        background: darkblue;
        color: white;
        border: 0px;
    }
  }
`;
const Container = styled.div`
    padding: 50px 0px;
    box-sizing: border-box;

    h2 {
        text-align: center;
        font-size: 28px;
    }

    p {
        text-align: center;
        width: 60%;
        margin: auto;
        margin-bottom: 40px;
    }
`;


function DiademaRepeat({ handleRepeat, text }) {
    return (
        <Container>
            <h2>{text.title}</h2>
            <p>{text.description}</p>

            <ButtonContainer>
                <div onClick={() => handleRepeat(true)} className='background'>{text.yes}</div>
                <div onClick={() => handleRepeat(false)}>{text.no}</div>
            </ButtonContainer>
        </Container>
    )
}

export default DiademaRepeat