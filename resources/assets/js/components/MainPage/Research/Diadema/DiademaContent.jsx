import { Modal, Row, Button, Select } from "antd";
import React, { Component } from "react";

import styled from "styled-components";

import { connect } from "react-redux";
import { createDiadema } from "redux-modules/diadema/actions";
import { alertActions } from "redux-modules/alert/actions";
import { Header, Alerts, Title, Repeat } from "./Translations";

import DiademaForm from "./DiademaForm";
import DiademaRepeat from "./DiademaRepeat";

const Option = Select.Option;

const Flag = styled.img`
    height: 25px;
    margin: 2px auto;
    display: block;
`;
const InfoContainer = styled.div`
    margin: 50px auto;
    margin-bottom: 30px;

    h1,
    h3,
    p {
        text-align: center;
    }

    p {
        margin-bottom: 30px;
        font-size: 1.1em;
        width: 90%;
        display: block;
        margin: auto;
    }
`;

const FormContainer = styled.div`
    display: block;
    width: 90%;
    margin: 50px auto;

    @media (max-width: 768px) {
        width: 100%;
    }

    .ant-form label {
        color: black !important;
    }
`;

class DiademaContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            finishForm: false,
            currentStep: 0,
            lang: "pt",
            formValidated: true,
        };

        // preserve the initial state in a new object
        this.baseState = this.state;
    }

    next = () => {
        this.setState({
            currentStep: this.state.currentStep + 1,
            formValidated: false,
        });
    };

    prev = () => {
        const currentStep = this.state.currentStep - 1;
        this.setState({ currentStep, formValidated: false });
    };

    handleSubmit = (formData) => {
        formData.latitude = formData.position[0];
        formData.longitude = formData.position[1];
        this.props.createDiadema(formData);
    };

    handleModalClose = () => {
        this.setState(this.baseState);
        this.props.handleModalClose();
    };

    handleLanguageChange = (e) => {
        this.setState({
            lang: e,
        });
    };

    handleRepeat = (repeat) => {
        if (repeat) this.setState(this.baseState);
        else this.next();
    };

    render() {
        var { currentStep, formValidated, lang } = this.state;

        const LanguageContainer = () => (
            <Row type="flex" justify="space-around">
                <Select
                    onChange={this.handleLanguageChange}
                    style={{ width: "80px" }}
                    value={lang}
                >
                    <Option value="en">
                        <Flag src="/images/flags/en.png" alt="" />
                    </Option>
                    <Option value="pt">
                        <Flag src="/images/flags/pt.png" alt="" />
                    </Option>
                </Select>
            </Row>
        );

        const steps = [
            {
                key: 0,
                content: (
                    <InfoContainer>
                        <h1>{Title[lang]}</h1>

                        <h3>{Header[lang].title}</h3>
                        <br />
                        <p>{Header[lang].description}</p>
                        <br />
                        <p>{Alerts[lang][0]}</p>

                        <FormContainer>
                            <LanguageContainer />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            {
                key: 1,
                content: (
                    <FormContainer>
                        <DiademaForm
                            handleSubmit={this.handleSubmit}
                            lang={lang}
                            setFormValidated={(val = true) =>
                                this.setState({
                                    formValidated: val,
                                })
                            }
                        />
                    </FormContainer>
                ),
            },
            {
                key: 2,
                content: <DiademaRepeat text={Repeat[lang]} handleRepeat={this.handleRepeat} />,
            },
            {
                key: 3,
                content: (
                    <InfoContainer>
                        <h1>{lang == "en" ? "Thank you" : "Muito Obrigado(a)"}</h1>

                        <p>{Header[lang].thankyou}</p>
                    </InfoContainer>
                ),
            },
        ];

        return (
            <Modal
                onCancel={this.handleModalClose}
                centered
                visible={this.props.visible}
                closable
                style={{ maxWidth: "960px", color: "black" }}
                width={"100%"}
                footer={[
                    currentStep == steps.length - 1 ? (
                        <Row type="flex" justify="center" gutter={16}>
                            <Button
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.handleModalClose}
                            >
                                Close
                            </Button>
                        </Row>
                    ) : currentStep == 2 ? null : (
                        <Row type="flex" justify="center">
                            <Button
                                disabled={!formValidated}
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.next}
                            >
                                Next
                            </Button>
                        </Row>
                    ),
                ]}
            >
                <div>{steps[currentStep].content}</div>
            </Modal>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createDiadema: (data) => dispatch(createDiadema(data)),
        errorAlert: (data) => dispatch(alertActions.error(data)),
        successAlert: (data) => dispatch(alertActions.success(data)),
    };
};

export default connect(null, mapDispatchToProps)(DiademaContent);
