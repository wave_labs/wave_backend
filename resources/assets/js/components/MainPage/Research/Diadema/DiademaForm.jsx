import React, { Component } from "react";
import { Row, Form, Input, message, Select } from "antd";
import { Fragment } from "react";
import { Marker } from "react-map-gl";
import MapBoxMap from "../../../Dashboard/Common/MapBoxMap";
import styled from "styled-components";
import { Informations, Alerts, Questions, Status } from "./Translations";

const FormItem = Form.Item;

const ImageContainer = styled.div`
  width: 33%;
  padding: 20px;
  box-sizing: border-box;

  img {
    width: 100%;
    border-radius: 50%;
  }

  h3 {
    text-align: center;
    margin-top: 20px;
  }
`;

const StyledSelect = styled(Select)`
  width: 100%;
`;

class DiademaForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            markerPoint: [],
            validated: false,
            rules: { dead: false, sick: false, coordinates: false }
        };
        this.mapContainerRef = React.createRef();
    }

    setMarkerValue = (value) => {
        this.setState({ markerPoint: value });
        this.props.form.setFieldsValue({ position: value });
    };

    handleMarkerClick = (e) => {
        if (!e.leftButton) {
            this.setState({ markerPoint: [] });
            this.handleChange("coordinates", false)
        }
    };

    validateZoom = (e) => {
        let zoomLevel = this.mapContainerRef.current.getMapZoom();
        if (zoomLevel <= 9) return true;
        else return false;
    };

    handleMapClick = (e) => {
        if (e.leftButton) {
            var isZoomInvalid = this.validateZoom();
            if (isZoomInvalid) {
                message.warning(Alerts[this.props.lang][1]);
            } else {
                this.setMarkerValue([e.lngLat[1], e.lngLat[0]])
                this.handleChange("coordinates")
            }
        }
    };


    componentDidUpdate(prevProps, prevState) {
        if (prevState.validated != this.state.validated) {
            this.props.setFormValidated(this.state.validated);
        }
    }

    componentWillUnmount() {
        if (this.state.validated) {
            const { form } = this.props;
            this.props.handleSubmit(form.getFieldsValue());
        }

    }

    handleChange = (field, value = true) => {
        const { rules } = this.state;
        rules[field] = value;
        var validated = true;

        Object.values(rules).map((rule) => {
            if (!rule) validated = false;
        })

        this.setState({ rules: rules, validated: validated })
    };

    render() {
        const { lang } = this.props;
        const { markerPoint } = this.state;
        const { getFieldDecorator } = this.props.form;

        const SelectFormItem = ({ fieldName, index }) => (
            <FormItem label={Questions[lang][index].q}>
                {getFieldDecorator(fieldName, {})(
                    <StyledSelect onChange={() => this.handleChange(fieldName)} placeholder={Questions[lang][index].p}>
                        {Questions[lang][index].opt.map((option, i) => (
                            <Select.Option key={i} value={Questions["en"][index].opt[i]}>
                                {option}
                            </Select.Option>
                        ))}
                    </StyledSelect>
                )}
            </FormItem>
        );

        return (
            <Fragment>
                <Row type="flex">
                    <ImageContainer>
                        <img src="/storage/images/studies/diadema/diadema_ok.jpg" alt="healthy urchin" />
                        <h3>{Status[lang][0]}</h3>
                    </ImageContainer>
                    <ImageContainer>
                        <img src="/storage/images/studies/diadema/diadema_sick.jpg" alt="sick urchin" />
                        <h3>{Status[lang][1]}</h3>
                    </ImageContainer>
                    <ImageContainer>
                        <img src="/storage/images/studies/diadema/diadema_dead.jpg" alt="dead urchin" />
                        <h3>{Status[lang][2]}</h3>
                    </ImageContainer>
                </Row>

                <FormItem label={Questions[lang][0].q}>
                    {getFieldDecorator("position", {})(
                        <>
                            <p onClick={this.handleFormValidation} style={{ textAlign: "center" }}>{Informations[lang][2]}</p>
                            <MapBoxMap
                                style="mapbox://styles/tigerwhale/ckt1c37d70vi317nyz3as5pza"
                                fixedSize={true}
                                size="400"
                                minZoom={8}
                                zoom={8}
                                onClick={(e) => this.handleMapClick(e)}
                                ref={this.mapContainerRef}
                            >
                                {markerPoint.length &&
                                    <Marker
                                        latitude={markerPoint[0]}
                                        longitude={markerPoint[1]}
                                    ><svg
                                        onContextMenu={(action) =>
                                            this.handleMarkerClick(
                                                action
                                            )
                                        }
                                        style={{
                                            position: "absolute",
                                            top: "-10px",
                                            left: "-10px",
                                            width: "20px",
                                            height: "20px",
                                            fill: "red",
                                        }}
                                    >
                                            <circle cx="10" cy="10" r="10" stroke="none" />
                                        </svg>
                                    </Marker>}
                            </MapBoxMap>
                        </>
                    )}
                </FormItem>

                <SelectFormItem fieldName="sick" index={1} />
                <SelectFormItem fieldName="dead" index={2} />
            </Fragment>
        );
    }
}
export default Form.create()(DiademaForm);
