import { Modal, Row, Button } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  createStopwatchResult,
  fetchNextStopwatchVideo,
} from "redux-modules/stopwatch/actions";

import StopwatchVideo from "./StopwatchVideo";
import StopwatchOccupation from "./StopwatchOccupation";
import StopwatchThanks from "./StopwatchThanks";
import StopwatchInteraction from "./StopwatchInteraction";
import StopwatchCategory from "./StopwatchCategory";
import StopwatchVideoList from "./StopwatchVideoList";

class StopwatchContent extends Component {
  constructor(props) {
    super(props);
    this.modal = React.createRef();
    this.state = {
      lang: "pt",
      video: null,
      nextVideo: null,
      currentStep: 0,
      disabled: true,
      formData: {},
      filters: {
        category: undefined,
        detection: undefined
      },
    };
  }

  handleVideoClick = (video) => {
    this.setState({ video: video });
    this.next();
  };

  next = () => {
    const currentStep = this.state.currentStep + 1;
    this.setState({ currentStep });
  };

  prev = () => {
    const currentStep = this.state.currentStep - 1;
    this.setState({ currentStep });
  };

  handleModalClose = () => {
    this.setState({ currentStep: 0 });
    this.props.handleModalClose();
  };

  handleDisabled = (value = true) => {
    this.setState({ disabled: value });
  };

  setFormData = (data) => {
    var { formData } = this.state;
    var newFormData = { ...formData, ...data };
    this.setState({ formData: newFormData });
  };

  setFilters = (data) => {
    var { filters } = this.state;
    var newData = { ...filters, ...data };
    this.setState({ filters: newData });
  };

  handleSubmit = () => {
    var { formData, video } = this.state;

    let form = new FormData();
    form.append("video_id", formData.video_id);
    form.append("occupation", formData.occupation);
    for (var i = 0; i < formData.timestamps.length; i++) {
      form.append(`timestamps[]`, formData.timestamps[i]);
    }
    this.props.createStopwatchResult(form);
    this.props
      .fetchNextStopwatchVideo(video.id, this.state.filters)
      .then((res) => {
        this.setState({ currentStep: 4, video: res.value.data.data });
      })
      .catch((error) => {
        this.next();
      });
  };

  render() {
    var { currentStep, video, disabled } = this.state;

    const steps = [
      {
        content: (
          <div>
            <StopwatchOccupation
              handleDisabled={this.handleDisabled}
              setFormData={this.setFormData}
            />
          </div>
        ),
      },
      {
        content: (
          <div>
            <StopwatchInteraction
              handleDisabled={this.handleDisabled}
              setFilters={this.setFilters}
              detection={this.state.filters.detection}
            />
          </div>
        ),
      },
      {
        content: (
          <div>
            <StopwatchCategory
              handleDisabled={this.handleDisabled}
              setFilters={this.setFilters}
              category={this.state.filters.category}
            />
          </div>
        ),
      },
      {
        content: <StopwatchVideoList filters={this.state.filters} handleVideoClick={this.handleVideoClick} />,
      },
      {
        content: (
          <StopwatchVideo
            prev={this.prev}
            setFormData={this.setFormData}
            handleDisabled={this.handleDisabled}
            handleSubmit={this.handleSubmit}
            video={video}
            playing={!this.props.visible && false}
          />
        ),
      },
      {
        content: <StopwatchThanks handleModalClose={this.handleModalClose}/>,
      },
    ];

    return (
      <div>
        <Modal
          onCancel={this.handleModalClose}
          centered
          visible={this.props.visible}
          closable
          width={960}
          maskClosable={false}
          footer={
            currentStep >= 3 ? null : (
              <Row type="flex" justify="center">
                <Button
                  disabled={disabled}
                  size="large"
                  key="submit"
                  type="primary"
                  onClick={this.next}
                >
                  Next
                </Button>
              </Row>
            )
          }
        >
          <div>{steps[currentStep].content}</div>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createStopwatchResult: (data) => dispatch(createStopwatchResult(data)),
    fetchNextStopwatchVideo: (id, filters) => dispatch(fetchNextStopwatchVideo(id, filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    videos: state.stopwatch.videos,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StopwatchContent);
