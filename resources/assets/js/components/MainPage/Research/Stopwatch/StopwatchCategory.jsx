import React, { Component } from "react";
import { Row, Col } from "antd";
import styled from "styled-components";
import { connect } from "react-redux";
import { dimensions } from "helpers"
import { fetchStopwatchCategories } from "redux-modules/stopwatch/category/actions";

const Container = styled.div`
  h2,
  p {
    text-align: center;
    display: block;
    margin: auto;
  }

  h2 {
    font-size: 1.6em;
  }
  p {
    font-size: 1.2em;
    margin-bottom: 10px;
    color: #777;
  }
`;

const SelectionItem = styled(Col)`
  padding: 20px;
  margin: 10px 0px;
  border-radius: 6px;
  scale: ${(props) => (props.active ? 1.01 : 1)};
  cursor: pointer;
  box-shadow: ${(props) =>
    props.active ? "0 0 10px 0 rgba(0, 0, 0, 0.2)" : "0 0 0 0"};

  &:hover {
    scale: 1.01;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.2);
  }

  h3 {
    margin-bottom: 2px;
    text-align: center;
    font-size: 1.6em;
    text-transform: capitalize;

    @media (max-width: ${dimensions.md}) {
      font-size: 1.2em;
    }
  }

  img {
    width: 80%;
    min-width: 100px;
    display: block;
    margin: 40px auto 10px auto;
    transition: all 0.5s ease;
  }
`;

class StopwatchCategory extends Component {
  handleChange = (e) => {
    this.props.setFilters({
      category: e,
    });
    this.props.handleDisabled(false);
  };

  componentDidMount() {
    this.props.handleDisabled();
    this.props.fetchStopwatchCategories();
  }

  render() {
    const { data } = this.props;

    return (
      <Container>
        <h2>Select a dataset</h2>
        <p>
          Choose the type of classification imagery between biodiversity,
          litter, and acoustics.
        </p>
        <Row type="flex" justify="space-around">
          {data.map((category) => (
            <SelectionItem
              key={category.id}
              span={8}
              active={this.props.category == category.id}
              onClick={() => this.handleChange(category.id)}
            >
              <img src={"images/studies/stopwatch/" + category.name + ".svg"} />
              <h3>{category.name}</h3>
            </SelectionItem>
          ))}
        </Row>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchStopwatchCategories: (filters) =>
      dispatch(fetchStopwatchCategories(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.stopwatchCategory.data,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StopwatchCategory);
