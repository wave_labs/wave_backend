import React, { Component } from "react";
import { Row, Radio } from "antd";
import styled from "styled-components";
import { connect } from "react-redux";
import {
  fetchStopwatchVideos,
} from "redux-modules/stopwatch/actions";

const Thumbnail = styled.img`
  width: 23%;
  min-width: 150px;
  margin: 20px auto;
  cursor: pointer;

  &:hover {
    filter: brightness(50%);
  }
`;

const Instruction = styled.div`
  h2 {
    font-size: 1.6em;
  }
  p {
    font-size: 1.2em;
    color: #777;
    margin-bottom: 10px;
  }
`;

class StopwatchVideoList extends Component {
  componentDidMount() {
    this.props.fetchStopwatchVideos(this.props.filters);
  }

  render() {
    return (
      <div>
        <Instruction>
          <h2>Select the video</h2>
          <p>Click on one thumbnail to select a video to classify</p>
        </Instruction>
        <Row type="flex" justify="space-around">
          {this.props.videos.length ? this.props.videos.map((video, index) => (
            <Thumbnail
              key={index}
              onClick={() => this.props.handleVideoClick(video)}
              key={video.id}
              src={video.thumbnail}
              alt={video.thumbnail}
            />
          )) : <p>No videos currently available</p>}
        </Row>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    fetchStopwatchVideos: (filters) => dispatch(fetchStopwatchVideos(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    videos: state.stopwatch.videos,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StopwatchVideoList);
