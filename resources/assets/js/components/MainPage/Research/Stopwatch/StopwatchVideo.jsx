import React, { Component } from "react";
import { Row, Progress } from "antd";
import { connect } from "react-redux";
import { fetchStopwatchClasses } from "redux-modules/stopwatch/actions";
import ReactPlayer from "react-player/lazy";
import styled from "styled-components";
import { LeftCircleOutlined } from "@ant-design/icons";

const Container = styled.div`
  .video-container {
    margin: auto;
    margin-top: 30px;
    width: 100% !important;
    height: 100% !important;
    min-height: 300px;
    border-radius: 6px;

    .react-player__preview {
      height: 400px !important;
      background-size: contain !important;
      background-repeat: no-repeat;
    }

    video {
      border-radius: inherit;
      width: 100% !important;
      height: auto !important;
      cursor: pointer;
      box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
    }
  }

  h2,
  p {
    text-align: center;
    display: block;
    margin: auto;
  }

  h2 {
    font-size: 1.6em;
  }
  p {
    font-size: 1.2em;
    color: #777;
    width: 80%;
    margin: 0px auto 10px auto;
    display: block;
  }
`;

const ClassContainer = styled.div`
  opacity: ${(props) => (props.active ? 1 : 0.5)};
  width: 12%;
  min-width: 80px;
  cursor: pointer;
  height: auto;
  margin: 30px auto;

  img {
    width: 100%;
  }

  h3 {
    text-transform: capitalize;
    text-align: center;
  }
`;

const VideoInstructions = styled.h3`
  font-weight: bold;
  text-align: center;
  font-size: 2em;
`;

const PreviousPage = styled(LeftCircleOutlined)`
  font-size: 20px;
  cursor: pointer;
  color: rgba(0, 0, 0, 0.45) !important;
  transition: color 0.3s;

  &:hover {
    color: rgba(0, 0, 0, 0.75) !important;
  }
`;

const ActiveClass = styled(VideoInstructions)`
  text-align: center;
  font-size: 1.5em;
  font-weight: normal;

  span {
    text-transform: capitalize;
    font-weight: bold;
  }
`;

class StopwatchVideo extends Component {
  constructor(props) {
    super(props);
    this.videoPlayer = React.createRef();
    this.state = {
      active: [],
      timer: [],
      playing: false,
      visibleClass: "Nothing",
      videoPercentage: 0,
    };
  }

  handleClick = (id) => {
    var { active, timer } = this.state;

    if (active.includes(id)) {
      var newActive = active.filter((element) => element != id);
      active = newActive;
      timer = this.closeTimestamp(timer, id);
    } else {
      active.push(id);
      timer = this.openTimestamp(timer, id);
    }
    var visibleClass = "Nothing";
    if (active.length > 1) {
      visibleClass = "Multiple objects";
    } else if (active.length == 1) {
      Object.values(this.props.classes).map((element) => {
        element.id == active[0] && (visibleClass = element.name);
      });
    }

    this.setState({ active, timer, visibleClass: visibleClass });
  };

  openTimestamp(timer, id) {
    timer.push([
      id,
      this.roundInt(this.videoPlayer.current.getCurrentTime()),
      null,
    ]);

    return timer;
  }

  closeTimestamp(timer, id) {
    timer.map(
      (element) =>
        element[0] === id &&
        element[2] == null &&
        (element[2] = this.roundInt(this.videoPlayer.current.getCurrentTime()))
    );

    return timer;
  }

  handleVideoEnd = () => {
    var { timer, active } = this.state;

    active.forEach((element) => {
      timer = this.closeTimestamp(timer, element);
    });

    this.props.handleDisabled(false);

    this.props.setFormData({
      timestamps: timer,
      video_id: this.props.video.id,
    });

    this.setState({ active: [], timer });

    this.props.handleSubmit();
  };

  roundInt(int) {
    return parseFloat(int.toFixed(2));
  }

  componentDidMount() {
    this.props.handleDisabled();
    this.getVideoPercentage();

    this.props.fetchStopwatchClasses({
      category: this.props.video.category_id,
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.video.id != this.props.video.id) {
      this.videoPlayer.current.showPreview();
      this.setState({
        playing: false,
        visibleClass: "Nothing",
        videoPercentage: 0,
        active: [],
        timer: [],
      });

      this.props.handleDisabled();
      this.getVideoPercentage();

      this.props.fetchStopwatchClasses({
        category: this.props.video.category_id,
      });
    }
  }

  componentWillUnmount() {
    this.setState({
      playing: false,
      visibleClass: "Nothing",
      videoPercentage: 0,
      active: [],
      timer: [],
    });
  }

  startVideo = () => {
    this.setState({ playing: true });
  };

  getVideoPercentage = (e) => {
    if (e) {
      this.setState({ videoPercentage: e.played * 100 });
    }
  };

  render() {
    var { video, classes } = this.props;
    var { active, playing, visibleClass, videoPercentage } = this.state;

    return (
      <Container>
        <PreviousPage onClick={this.props.prev} />
        <h2>Classify the video</h2>
        <p>
          Click on video to start and select from the buttons below which
          species/categories are currently appearing on the screen.
        </p>

        <div onClick={this.startVideo}>
          <ReactPlayer
            ref={this.videoPlayer}
            playing={playing}
            className="video-container"
            url={video.video}
            muted
            onEnded={this.handleVideoEnd}
            light={video.thumbnail}
            onProgress={this.getVideoPercentage}
          />
          <Progress percent={videoPercentage} showInfo={false} />
          {!playing ? (
            <VideoInstructions>Click on video to start</VideoInstructions>
          ) : (
            <ActiveClass>
              I see: <span>{visibleClass}</span>
            </ActiveClass>
          )}
        </div>
        <Row type="flex" justify="space-around">
          {Object.values(classes).map((element) => (
            <ClassContainer
              active={active.includes(element.id)}
              key={element.id}
              onClick={() => this.handleClick(element.id)}
            >
              <img src={element.image} alt={element.name} />
              <h3>{element.name}</h3>
            </ClassContainer>
          ))}
        </Row>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchStopwatchClasses: (filters) =>
      dispatch(fetchStopwatchClasses(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    classes: state.stopwatch.classes,
    loading: state.auth.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StopwatchVideo);
