import React, { Component } from "react";
import { Input, Row, Select, Col } from "antd";
import styled from "styled-components";

const { Option } = Select;

const Container = styled.div`
  .video-container {
    margin: 30px auto;
    width: 100% !important;
    height: 100% !important;
    border-radius: 6px;

    video {
      border-radius: inherit;
      width: 100% !important;
      height: auto !important;
      cursor: pointer;
      box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
    }
  }

  img {
    width: 40%;
    min-width: 200px;
    display: block;
    margin: auto;
    margin-bottom: 30px;
  }

  h2,
  p {
    text-align: center;
    display: block;
    margin: auto;
  }

  h2 {
    font-size: 1.6em;
  }
  p {
    font-size: 1.2em;
    margin-bottom: 10px;
    color: #777;
  }
`;

class StopwatchOccupation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      occupation: undefined,
      customOccupation: undefined,
      visible: false,
    };
  }

  handleChange = (e) => {
    var visible = false;
    if (e == "other") {
      visible = true;
    }
    this.props.handleDisabled(visible);
    this.setState({ occupation: e, visible: visible });
  };

  handleInputChange = (e) => {
    if (e.target.value) {
      this.props.handleDisabled(false);
    } else {
      this.props.handleDisabled();
    }

    this.setState({ customOccupation: e.target.value });
  };

  componentWillUnmount() {
    this.props.setFormData({
      occupation:
        this.state.occupation !== "other"
          ? this.state.occupation
          : this.state.customOccupation,
    });
  }

  render() {
    return (
      <Container>
        <img src="images/studies/stopwatch/occupation.svg" alt="" />
        <h2>Select an occupation</h2>
        <p>
          Select a working occupation in the dropdown below or select "Other"
          and specify your own.
        </p>
        <Row type="flex" justify="space-around">
          <Col span={12}>
            <Select
              style={{ width: this.state.visible ? "80%" : "100%" }}
              onChange={this.handleChange}
              value={this.state.occupation}
              placeholder="Select your working area"
            >
              <Option value="researcher">Researcher</Option>
              <Option value="marine biologist">Marine Biologist</Option>
              <Option value="teacher">Teacher</Option>
              <Option value="student">Student</Option>
              <Option value="other">Other</Option>
            </Select>
          </Col>
          {this.state.visible && (
            <Col span={12}>
              <Input
                value={this.state.customOccupation}
                onChange={(value) => this.handleInputChange(value)}
                placeholder="Write your custom occupation"
              />
            </Col>
          )}
        </Row>
      </Container>
    );
  }
}

export default StopwatchOccupation;
