import React, { Component } from "react";
import { Row, Col } from "antd";
import styled from "styled-components";
import { dimensions } from "helpers"

const Container = styled.div`
  .video-container {
    margin: 30px auto;
    width: 100% !important;
    height: 100% !important;
    border-radius: 6px;

    video {
      border-radius: inherit;
      width: 100% !important;
      height: auto !important;
      cursor: pointer;
      box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.3);
    }
  }

  h2,
  p {
    text-align: center;
    display: block;
    margin: auto;
  }

  h2 {
    font-size: 1.6em;
  }
  p {
    font-size: 1.2em;
    margin-bottom: 10px;
    color: #777;
  }
`;

const SelectionItem = styled(Col)`
  padding: 20px;
  margin: 10px 0px;
  border-radius: 6px;
  scale: ${(props) => (props.active ? 1.01 : 1)};
  box-shadow: ${(props) =>
    props.active ? "0 0 10px 0 rgba(0, 0, 0, 0.2)" : "0 0 0 0"};
  cursor: pointer;

  &:hover {
    scale: 1.01;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.2);
  }

  h3 {
    margin-bottom: 2px;
    text-align: center;
    font-size: 1.6em;

    @media (max-width: ${dimensions.md}) {
      font-size: 1.2em;
    }
  }

  img {
    width: 80%;
    min-width: 100px;
    display: block;
    margin: 40px auto 10px auto;
    transition: all 0.5s ease;
  }
`;

class StopwatchInteraction extends Component {

  handleChange = (e) => {
    this.props.setFilters({
      detection: e,
    });
    this.props.handleDisabled(false);
  };

  componentDidMount() {
    this.props.handleDisabled();
  }

  render() {
    return (
      <Container>
        <h2>Select a classification type</h2>
        <p>
          Select an interaction between unassisted classification or AI
          assisted.
        </p>
        <Row type="flex" justify="space-around">
          <SelectionItem
            xs={10}
            md={8}
            active={!this.props.detection && this.props.detection != undefined}
            onClick={() => this.handleChange(0)}
          >
            <img src="images/studies/stopwatch/interaction-man.png" />
            <h3>Unassisted</h3>
            <div />
          </SelectionItem>
          <SelectionItem
            xs={10}
            md={8}
            active={this.props.detection && true}
            onClick={() => this.handleChange(1)}
          >
            <img src="images/studies/stopwatch/interaction-machine.png" />
            <h3>AI assisted</h3>
            <div />
          </SelectionItem>
        </Row>
      </Container>
    );
  }
}

export default StopwatchInteraction;
