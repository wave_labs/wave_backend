import React from "react";
import { Row, Radio } from "antd";
import styled from "styled-components";

const Container = styled.div`
  text-align: center;

  img {
    width: 40%;
    min-width: 200px;
    display: block;
    margin: auto;
    margin-bottom: 30px;
  }

  h1,
  h2,
  h3 {
    width: 40%;
    display: block;
    margin: auto;
  }

  h1 {
    font-size: 2.5em;
  }
  h2 {
    font-size: 1.6em;
    margin-bottom: 30px;
  }
  h3 {
    width: 40%;
    font-size: 1.2em;
    margin-bottom: 10px;
    color: #777;
  }
`;

const RadioButton = styled(Radio.Button)`
  margin: auto 10px !important;

  text-align: center;
`;

const StopwatchThanks = ({ handleModalClose }) => {
  return (
    <Container>
      <img src="images/studies/stopwatch/sent.svg" alt="" />
      <h1>Thank you!</h1>
      <h2>You made it! The classification was submitted with success.</h2>
      <h3> Interested in participating in another video?</h3>
      <Row type="flex" justify="space-around">
        <Radio.Group buttonStyle="solid" onChange={handleModalClose}>
          <RadioButton value={false}>Close</RadioButton>
        </Radio.Group>
      </Row>
    </Container>
  );
};

export default StopwatchThanks;
