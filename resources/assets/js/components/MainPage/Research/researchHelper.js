const publicationsPath = "/images/publications/";

export const publications = {
    app: [
        {
            image: publicationsPath + "oceangame.png",
            title: "Gaming versus storytelling: understanding children's interactive experiences in a museum setting.",
            authors: "Radeta, M., Cesario, V., Matos, S., & Nisi, V.",
            conference: "ICIDS2017",
            file: "/download/articles/oceangame.pdf",
        },
        {
            image: publicationsPath + "lorattle.png",
            title: "LoRattle: An Exploratory Game With a Purpose using LoRa and IoT.",
            authors: "Radeta M., Ribeiro M. Vasconcelos D., & Nunes, N.",
            conference: "ICEC2019",
            file: "/download/articles/lorattle.pdf",
        },
    ],
    iot: [
        {
            image: publicationsPath + "poseidon.png",
            title: "POSEIDON: Passive-acoustic Ocean Sensor for Entertainment and Interactive Data-gathering in Opportunistic Nautical-activities.",
            authors: "Radeta, M., Nunes, N. J., Vasconcelos, D., & Nisi, V.",
            conference: "DIS2018",
            file: "/download/articles/poseidon.pdf",
        },
        {
            image: publicationsPath + "seamote.png",
            title: "SeaMote: Interactive Remotely Operated Apparatus for Aquatic Expeditions.",
            authors:
                "Radeta, M., Ribeiro, M., Vasconcelos, D., Lopes, J., Sousa, M., Monteiro, J., & Nunes, N. J.",
            conference: "INTERACT2019",
            file: "/download/articles/seamote.pdf",
        },
        {
            image: publicationsPath + "loraquatica.png",
            title: "LoRaquatica: Studying Range and Location Estimation using LoRa and IoT in Aquatic Sensing",
            authors:
                "Radeta, M., Ribeiro, M., Vasconcelos, D., Noronha, H., & Nunes, N.",
            conference: "PERCOM2020",
            file: "/download/articles/loraquatica.pdf",
        },
    ],
    dome: [
        {
            image: publicationsPath + "interaquatica.png",
            title: "INTERAQUATICA: Designing Interactive Aquatic Experiences with Geodesic Domes In-the-Wild",
            authors:
                "Radeta, M., Andrade, M., Freitas, R., Sousa, L., Ramos, A., Azevedo, V., Lopes, J., Jardim, R., Gouveia, J., Gouveia, M., Gonçalves, T., Pinto, P., Rodrigues, R., França, C., Freitas, J., Andrade, A., Faria, A., Abreu, L., Mendes, M., Nunes, N. & Nisi, V. ",
            conference: "IMX2020",
            file: "/download/articles/interaquatica.pdf",
        },
    ],
};

export const team = [
    {
        avatar: "avatar",
        name: "Ankit Gupta",
        job: "Convolutional neural network for signal processing",
    },
    {
        avatar: "Francisco",
        name: "Francisco Silva",
        job: "Full Stack Developer",
    },
    {
        avatar: "avatar",
        name: "Ivo Rodrigues",
        job: "Understanding underwater location",
    },
    {
        avatar: "JoaoPestana",
        name: "João Pestana",
        job: "IoT programmer and developer, responsible for developing, deploying, and maintaining marine sensing devices and surveying systems",
    },
    {
        avatar: "PedroAbreu",
        name: "Pedro Abreu",
        job: "Leveraging Internet of Things Sensing for Deep Sea Exploration",
    },
    {
        avatar: "Ruben",
        name: "Rúben Freitas",
        job: "Full Stack Development for Wave Platform, IoT, Apps and AI",
    },
    {
        avatar: "Silvia",
        name: "Sílvia Almeida",
        job: "Enhancing AI for marine applications",
    },
    {
        avatar: "telmo",
        name: "Telmo Silva",
        job: "Cross Reality Interfaces for On- and Off- Shore Interaction with Greater Whales",
    },
    {
        avatar: "avatar",
        name: "Čejka Martin",
        job: "Detecting anomalies in optical imagery",
    },
];

export const advisory = [
    { avatar: "ana", name: "Ana Dinis", job: "MARE, Madeira, PT" },
    { avatar: "filipe", name: "Filipe Alves", job: "MARE, Madeira, PT" },
    { avatar: "JCC", name: "João Canning-Clode", job: "MARE, Madeira, PT" },
    { avatar: "Monteiro", name: "João Monteiro", job: "MARE, Madeira, PT" },
    { avatar: "Marc", name: "Marc Fernández", job: "Monicet, Azores, PT" },
    { avatar: "avatar", name: "Mary Barreto", job: "UMa, Madeira, PT" },
    { avatar: "avatar", name: "Morgado Dias", job: "UMa, Madeira, PT" },
    { avatar: "RitaF", name: "Rita Ferreira", job: "MARE, Madeira, PT" },
    { avatar: "avatar", name: "Rui Prieto", job: "DRAM, Azores, PTPLOCAN" },
    { avatar: "avatar", name: "Thomas Dellinger", job: "UMa, Madeira, PT" },
];

export const collaborators = [
    {
        avatar: "avatar",
        name: "Agustin Zuniga",
        job: "University of Helsinki, FI",
    },
    { avatar: "Camila", name: "Camila Campanati", job: "MARE Madeira, PT" },
    { avatar: "avatar", name: "Dora Pombo", job: "University of Madeira, PT" },
    { avatar: "avatar", name: "Eric Delory", job: "PLOCAN, ES" },
    {
        avatar: "avatar",
        name: "Fábio Mendonça",
        job: "University of Madeira, PT",
    },
    { avatar: "avatar", name: "Huber Flores", job: "University of Tartu, EE" },
    { avatar: "InesOrfao", name: "Ines Orfão", job: "MARE Madeira, PT" },
    {
        avatar: "avatar",
        name: "Laurindo Sobrinho",
        job: "University of Madeira, PT",
    },
    { avatar: "avatar", name: "Matej Buzinkai", job: "MARE Madeira, PT" },
    { avatar: "Paola", name: "Paola Parretti", job: "MARE Madeira, PT" },
    { avatar: "Patricio", name: "Patrício Ramalhosa", job: "MARE Madeira, PT" },
    { avatar: "avatar", name: "Pavol Dubovsky", job: "APVV, SK" },
    { avatar: "avatar", name: "Pedro Augusto", job: "University of Porto, PT" },
    {
        avatar: "avatar",
        name: "Petteri Nurmi",
        job: "University of Helsinki, FI",
    },
    { avatar: "Rodrigo", name: "Rodrigo Silva", job: "MARE Madeira, PT" },
    { avatar: "avatar", name: "Silvana Neves", job: "Canary Islands, ES" },
    { avatar: "Susi", name: "Susanne Schäfer", job: "MARE Madeira, PT" },
    {
        avatar: "avatar",
        name: "Thi Ngoc Nguyen",
        job: "University of Helsinki, FI",
    },
    { avatar: "avatar", name: "Victor Azevedo", job: "Arditi, PT" },
    {
        avatar: "avatar",
        name: "Vladimir Zekovic",
        job: "Princeton University, USA",
    },
    { avatar: "avatar", name: "Zhigang Yin", job: "University of Tartu, EE" },
];

export const contributors = [
    { name: "Adriano Andrade" },
    { name: "Albert Taxonera" },
    { name: "Alexandre Borges" },
    { name: "André Nunes" },
    { name: "António Ramos" },
    { name: "Bernardo Galvão" },
    { name: "Cláudio Rodrigues" },
    { name: "Cristiano França" },
    { name: "Cátia Gouveia" },
    { name: "Diogo Ferreira" },
    { name: "Diogo Martins" },
    { name: "Francisco Serrão" },
    { name: "Fábio Abreu" },
    { name: "Fábio Pacheco" },
    { name: "Hildegardo Noronha" },
    { name: "Jocelyne Pestana" },
    { name: "Jorge Lopes" },
    { name: "João Faria" },
    { name: "João Franco" },
    { name: "João Freitas" },
    { name: "João Gouveia" },
    { name: "Leonardo Abreu" },
    { name: "Louis Rodrigues" },
    { name: "Luís Aguiar" },
    { name: "Marcos Oliveira" },
    { name: "Marcos Silva" },
    { name: "Miguel Andrade" },
    { name: "Nuno Nunes" },
    { name: "Paulo Aguiar" },
    { name: "Pedro Barata" },
    { name: "Pedro Damião" },
    { name: "Pedro Pinto" },
    { name: "Ricardo Jardim" },
    { name: "Rodrigo Rocha" },
    { name: "Rosa Pires" },
    { name: "Rúben Sousa" },
    { name: "Tomás Ferreira" },
    { name: "Wilson Agrela" },
];

export const studies = [
    {
        name: "Diadema",
        index: "diadema",
        description:
            "Assessment of the distribution of mortality of longthorn urchins in coastal waters of the Archipelago",
        image: "/storage/images/logo/diadema.svg",
    },
    {
        name: "Markers - Have you seen me lately?",
        description:
            "Evaluating presence, abundance and distribution of biodiversity in the aquatic coast land of Arquipélago da Madeira.",
        image: "/storage/images/logo/markers.svg",
        index: "markers",
    },
    {
        name: "Stopwatch",
        description:
            "Web-based stopwatch of marine occurrences in various videos, providing comparison metrics with AI generated detections.",
        image: "/storage/images/logo/stopwatch.svg",
        index: "stopwatch",
    },
    {
        name: "Bounding Box",
        description:
            "Exploration of user-based image classification for artificial intelligence training procedures with web integrated studies.",
        image: "/storage/images/logo/bounding-box.svg",
        index: "bounding-box",
    },
    {
        name: "Freshwater Pin",
        index: "freshwater-pin",
        description:
            "Mapping species of the freshwater ecosystems at Madeira and Porto Santo islands, evaluating distributions across timeframes.",
        image: "/storage/images/logo/freshwater-pin.svg",
    },

    {
        name: "Freshwater Vertebrates",
        index: "freshwater-vertebrates",
        description:
            "There's an ongoing research project at MARE-Madeira focused on freshwater animal species present in Madeira's streams.",
        image: "/storage/images/logo/trout.svg",
    },

    {
        name: "Climarest: Stakeholder survey",
        index: "climarest",
        description: "On going study for the climarest project.",
        image: "/storage/images/logo/climarest.png",
    },
    {
        name: "Soft Coral's Presence",
        index: "soft-corals-presence",
        description:
            "Within the scope of scientific activities and research, MARE-Madeira is assessing this soft coral's presence and distribution in the Madeira Archipelago's coastal waters.",
        image: "/storage/images/logo/coral.svg",
    },
];
