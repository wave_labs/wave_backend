import React, { Component } from "react";
import { Row, Col, Select, Form, Input, Button, Icon, Checkbox } from "antd";
import { Questions, Informations } from "./Translations";
import { isArrayNull } from "./helper";
import styled from "styled-components";
import CoordinatesForm from "./CoordinatesForm";

const FormItem = Form.Item;
const Option = Select.Option;

const Notice = styled.p`
  font-weight: bold;
  margin: 40px auto 30px auto !important;
`;

class TroutFormSighting extends Component {
  state = {
    occurrences: 1,
    remembers: false,
  };

  rules = {
    nullableField: [
      {
        required: false,
        message: "Field is required",
      },
    ],
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  handleFormChange = () => {
    setTimeout(() => {
      let form = this.props.form.getFieldsValue();
      var hasEmptyField = false;
      var string = "sightings" + this.props.animal;
      [...Array(this.state.occurrences)].map((_, index) => {
        if (
          !form[string][index]["stream"] ||
          !form[string][index]["latitude"] ||
          !form[string][index]["longitude"] ||
          !form[string][index]["animal"] ||
          !form[string][index]["year"]
        )
          hasEmptyField = true;
      });

      if (!hasEmptyField) {
        this.props.handleFormValidation({ [string]: form });
      }
    }, 5);
  };

  componentDidUpdate(prevProps) {
    if (prevProps.animal != this.props.animal) {
      this.setState({
        occurrences: 1,
      });
    }
  }

  handleCoordinateChange = (object) => {
    this.props.form.setFieldsValue(object);
    this.handleFormChange();
  };

  handleNewSighting = () => {
    this.setState({ occurrences: this.state.occurrences + 1 });
  };

  render() {
    const { lang, data, animal } = this.props;
    const { getFieldDecorator } = this.props.form;
    let question = Questions[lang];

    const SelectFormItem = ({
      fieldName,
      index,
      handleField = this.handleFormChange,
    }) => (
      <Col span={24}>
        <FormItem label={question[index].q}>
          {getFieldDecorator(fieldName, {
            rules: this.rules.requiredField,
            initialValue: isArrayNull(data[fieldName]),
          })(
            <Select onChange={handleField} placeholder={question[index].p}>
              {question[index].opt.map((option, i) => (
                <Option key={i} value={Questions["en"][index].opt[i]}>
                  {option}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
      </Col>
    );

    return (
      <Form hideRequiredMark className="wrap-label">
        <img
          style={{
            margin: "auto",
            minWidth: "250px",
            width: "50%",
            display: "block",
          }}
          alt="Image"
          src={`/storage/images/studies/trout/${animal}.jpg`}
        />
        <Notice>
          {Informations[lang][2]} {animal}
        </Notice>

        {[...Array(this.state.occurrences)].map((_, index) => (
          <Row
            key={animal + "-" + index}
            type="flex"
            align="middle"
            style={{ marginBottom: "20px" }}
            gutter={16}
          >
            <Col xs={24} sm={12}>
              <Row gutter={16}>
                <SelectFormItem
                  fieldName={"sightings" + animal + "[" + index + "][year]"}
                  index={5}
                />
                <SelectFormItem
                  fieldName={"sightings" + animal + "[" + index + "][stream]"}
                  index={6}
                />
                <Col span={24}>
                  <FormItem label={question[7].q}>
                    {getFieldDecorator(
                      "sightings" + animal + "[" + index + "][location]"
                    )(
                      <Input
                        onChange={this.handleFormChange}
                        placeholder={question[7].p}
                      />
                    )}
                  </FormItem>
                </Col>

                {getFieldDecorator(
                  "sightings" + animal + "[" + index + "][animal]",
                  { initialValue: animal }
                )}
                {getFieldDecorator(
                  "sightings" + animal + "[" + index + "][latitude]",
                  { initialValue: undefined }
                )}
                {getFieldDecorator(
                  "sightings" + animal + "[" + index + "][longitude]",
                  { initialValue: undefined }
                )}
              </Row>
            </Col>

            <Col xs={24} sm={12}>
              <h4>{Informations[lang][1]}</h4>
              <br />
              <CoordinatesForm
                index={"sightings" + animal + "[" + index + "]"}
                handlePositions={(positions) =>
                  this.handleCoordinateChange(positions)
                }
              />
            </Col>
          </Row>
        ))}

        <Button type="dashed" onClick={this.handleNewSighting}>
          <Icon type="plus" /> {Informations[lang][3]}
        </Button>
      </Form>
    );
  }
}

export default Form.create()(TroutFormSighting);
