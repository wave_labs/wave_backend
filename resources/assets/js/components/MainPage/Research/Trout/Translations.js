export const Descriptions = {
    pt: {
        0: "Pedimos-lhe que preencha este questionário, ajudando-nos a avaliar a presença de espécies de animais nos ecossistemas de água doce da ilha da Madeira. A biodiversidade de água doce do arquipélago da Madeira está a ser investigada no âmbito de um projeto de investigação do MARE-Madeira financiado pela ARDITI (Agência Regional para o Desenvolvimento da Investigação, Tecnologia e Inovação).",
        1: "O inquérito é de resposta voluntária e anónimo, e os dados recolhidos serão tratados de forma confidencial, sem que os participantes possam vir a ser identificados, tendo como única finalidade a sua utilização no âmbito deste trabalho de investigação. As respostas a este inquérito serão guardadas em segurança num computador protegido por senha, de forma a manter o anonimato dos participantes.",
        2: "Selecione uma atividade na qual avistou o peixe.",
        3: "Pedimos que divulgue este questionário e caso queira continuar a colaborar (em particular se pescar nas águas interiores da ilha ou se encontrar algum destes ou outro animal morto) poderá contactar-nos para ines.orfao@mare.arditi.pt.",
    },
    en: {
        0: "We kindly ask you to fill out this survey, helping us in evaluating the presence of animal species at freshwater ecosystems in Madeira Island. Freshwater biodiversity of the archipelago of Madeira is being investigated within the scope of a research project of MARE-Madeira funded by ARDITI (Agência Regional para o Desenvolvimento da Investigação, Tecnologia e Inovação).",
        1: "This survey is voluntary and anonymous, data gathered here will be treated with confidentiality, without identification of respondants, as the only goal is gathering data of use for the research project. Answers to this survey will be safely stored in password-locked computers, protecting all personal data.",
        2: "Please, select an activity in which you detected this fish.",
        3: "Please share the questionnaire and in case you would like to keep collaborating with us (particularly if you fish in the inner waters of the island, or if you find dead specimens of any of these or other animals) please contact ines.orfao@mare.arditi.pt.",
    }
}

export const Alerts = {
    pt: {
        0: "Muito obrigada pela sua colaboração!",
        1: "Por favor aumente o zoom do mapa, de modo a aumentar a precisão",
        2: "Figura",
    },
    en: {
        0: "We thank you for your collaboration!",
        1: "Please increase map's zoom before adding pins in order to increase precision",
        2: "Figure",
    }
}

export const ButtonText = {
    pt: {
        0: "Voltar",
        1: "Seguinte",
        2: "Submeter",
    },
    en: {
        0: "Back",
        1: "Next",
        2: "Submit",
    }
}


export const PersonalQuestions = {
    pt: [
        { q: "Idade?", p: "Idade", opt: ['Menos de 18 anos', '18-39 anos', '40-60 anos', 'Mais de 60 anos'] },
        { q: "Região de residência", p: "Região de residência", opt: ['Arquipélago da Madeira', 'Outra região de Portugal', 'Estrangeiro'] },
    ],
    en: [
        { q: "Age?", p: "Age", opt: ['Less than 18 years', '18-39 years', '40-60 years', 'More than 60 years'] },
        { q: "Residence region", p: "Residence region", opt: ['Madeira archipelago', 'Other region of Portugal', 'Abroad'] },
    ]
}

export const Informations = {
    pt: [
        'Preencha todos os campos necessários antes de avançar',
        'Posicione o ponto no mapa de acordo com o local onde ocorreu o seu avistamento',
        'Por favor indique abaixo cada avistamento do animal ',
        'Adicionar observação',
    ],
    en: [
        'Select requested information before advancing',
        'Position the pin on the map according to where your sighting occurred',
        'Please add each place where you remember to have seen the animal ',
        'Add sighting'
    ],
}

export const CoordinatesQuestions = {
    pt: [
        ["Na última vez (verde)", "Nos últimos 5 anos (vermelho)", "Há mais de 5 anos (azul)"],
        { q: "Observações", p: "Inclua toda e qualquer informação que considere relevante adicionar" },
    ],
    en: [
        ["Last time (green)", "In the last 5 years (red)", "Over 5 years ago (blue)"],
        { q: "Observations", p: "Include all information you consider relevant" },
    ]
}

export const Questions = {
    pt: [
        { q: "Alguma vez viu algum destes animais na Madeira? Escolha uma opção ou mais opções válidas", p: "A / B / C / Nenhum", opt: ['A', 'B', 'C', 'Nenhum'] },
        { q: "Escreva o nome do animal A", p: "Espécie A" },
        { q: "Escreva o nome do animal B", p: "Espécie B" },
        { q: "Escreva o nome do animal C", p: "Espécie C" },
        { q: "Qual é que viu?", p: "A / B / C", opt: ['A', 'B', 'C'] },
        { q: "Em que ano?", p: "2015 ou antes / 2016 ou depois / Não me recordo", opt: ['2015 ou antes', '2016 ou depois', 'Não me recordo'] },
        { q: "Em que corpo de água foi a observação?", p: "Ribeira / Levada / Lago / Lagoa", opt: ['Ribeira', 'Levada', 'Lago', 'Lagoa'] },
        { q: "Por favor indique onde viu ou 'não sei/não me recordo' se for o caso.", p: "ex. Ribeira Amarela" },
    ],
    en: [
        { q: "Have you ever seen any of these animals in Madeira? Select the valid option(s)", p: "A / B / C / None", opt: ['A', 'B', 'C', 'None'] },
        { q: "Write the name of the animal in Figure A", p: "Animal A" },
        { q: "Write the name of the animal in Figure B", p: "Animal B" },
        { q: "Write the name of the animal in Figure C", p: "Animal C" },
        { q: "Which animal did you see?", p: "A / B / C", opt: ['A', 'B', 'C'] },
        { q: "What year was it?", p: "2015 or earlier / 2016 or afterwards / I don't recall", opt: ['2015 or earlier', '2016 or afterwards', 'I don\'t recall'] },
        { q: "What body of water did you find it in?", p: "Stream / Levada / Lake / Pond", opt: ['Stream', 'Levada', 'Lake', 'Pond'] },
        { q: "Please give the place where you have seen it or 'I don't know/don't recall', if that's the case.", p: "e.g. Ribeira Amarela" },
    ]
}