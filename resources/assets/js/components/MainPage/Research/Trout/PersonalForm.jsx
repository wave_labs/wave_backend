import React, { Component } from "react";
import { Row, Col, Select, Form, Input } from "antd";
import { PersonalQuestions } from "./Translations";
import { isArrayNull } from "./helper";

const FormItem = Form.Item;
const Option = Select.Option;

class PersonalForm extends Component {
  rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  componentDidMount = () => {
    this.handleFormChange();
  };

  handleFormChange = () => {
    setTimeout(() => {
      let form = this.props.form.getFieldsValue();
      if (form.age && form.residence) {
        this.props.handleFormValidation({ personal: form });
      }
    }, 1);
  };

  render() {
    const { lang, data } = this.props;
    const { getFieldDecorator } = this.props.form;
    let question = PersonalQuestions[lang];

    return (
      <Form hideRequiredMark={false} className="wrap-label">
        <Row gutter={16}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback={false} label={question[0].q}>
              {getFieldDecorator("age", {
                rules: this.rules.requiredField,
                initialValue: isArrayNull(data.age),
              })(
                <Select
                  onChange={this.handleFormChange}
                  placeholder={question[0].p}
                >
                  {question[0].opt.map((option, index) => (
                    <Option
                      key={index}
                      value={PersonalQuestions["en"][0].opt[index]}
                    >
                      {option}
                    </Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback={false} label={question[1].q}>
              {getFieldDecorator("residence", {
                rules: this.rules.requiredField,
                initialValue: isArrayNull(data.residence),
              })(
                <Select
                  onChange={this.handleFormChange}
                  placeholder={question[1].p}
                >
                  {question[1].opt.map((option, index) => (
                    <Option
                      key={index}
                      value={PersonalQuestions["en"][1].opt[index]}
                    >
                      {option}
                    </Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(PersonalForm);
