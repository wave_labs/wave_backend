import React, { Component } from "react";
import { Row, Col, Select, Form, Input, Button, Icon } from "antd";
import { Questions, Informations } from "./Translations";
import { isArrayNull } from "./helper";
import styled from "styled-components";
import CoordinatesForm from "./CoordinatesForm";

const FormItem = Form.Item;
const Option = Select.Option;

const HiddenForm = styled.div`
    transition: 3s ease-in-out;
    display: ${(props) => (props.hidden ? "none" : "block")};
    width: 100%;
`;

class TroutForm extends Component {
    state = {
        finish: true,
        extraFields: false,
        knowledgeAnswer: [],
    };

    rules = {
        nullableField: [
            {
                required: false,
                message: "Field is required",
            },
        ],
        requiredField: [
            {
                required: true,
                message: "Field is required",
            },
        ],
    };

    componentDidMount = () => {
        this.handleFormChange();
        if (this.props.data.knowledge) {
            this.setState({ finish: false });
        }
    };

    handleFormChange = () => {
        setTimeout(() => {
            let form = this.props.form.getFieldsValue();
            if (!this.state.knowledgeAnswer.includes("None")) {
                if (
                    form.knowledge &&
                    (this.state.knowledgeAnswer.includes("A") ? form.nameA : true) &&
                    (this.state.knowledgeAnswer.includes("B") ? form.nameB : true) &&
                    (this.state.knowledgeAnswer.includes("C") ? form.nameC : true)
                ) {
                    this.props.handleFormValidation({ observations: form, knowledgeArray: this.state.knowledgeAnswer });
                }
            } else {
                this.props.handleFormValidation({ observations: form });
                this.props.form.setFieldsValue({ knowledge: ["None"] })
            }

        }, 5);
    };

    continueForm = (value) => {
        console.log(value);
        let finish = false, extraFields = true;

        if (value.includes("None") && value.length) {
            finish = true;
            extraFields = false;

            this.handleFormChange();
        } else if (!value.length) {
            extraFields = false;
        }

        this.props.finishForm(finish);
        this.setState({ finish: finish, extra: extraFields, knowledgeAnswer: value });
    };



    render() {
        const { lang, data } = this.props;
        const { getFieldDecorator } = this.props.form;
        let question = Questions[lang];


        return (
            <Form hideRequiredMark={true} className="wrap-label">
                <Row type="flex" align="middle" gutter={32}>

                    <Col xs={24} md={24}>
                        <FormItem label={question[0].q}>
                            {getFieldDecorator("knowledge", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.knowledge),
                            })(
                                <Select mode="multiple" onChange={(e) => this.continueForm(e)} placeholder={question[0].p}>
                                    {question[0].opt.map((option, i) => (
                                        <Option key={i} value={Questions["en"][0].opt[i]}>
                                            {option}
                                        </Option>
                                    ))}
                                </Select>
                            )}
                        </FormItem>
                    </Col>

                    <HiddenForm hidden={this.state.finish}>
                        {this.state.knowledgeAnswer.includes("A") &&
                            <Col xs={24} sm={8}>
                                <FormItem label={question[1].q}>
                                    {getFieldDecorator("nameA", {
                                        initialValue: isArrayNull(data.nameA),
                                        rules: [{
                                            required: this.state.knowledgeAnswer.includes("A"),
                                            message: "Field is required",
                                        }],
                                    })(
                                        <Input
                                            onChange={this.handleFormChange}
                                            placeholder={question[1].p}
                                        />
                                    )}
                                </FormItem>
                            </Col>
                        }
                        {this.state.knowledgeAnswer.includes("B") &&
                            <Col xs={24} sm={8}>
                                <FormItem label={question[2].q}>
                                    {getFieldDecorator("nameB", {
                                        initialValue: isArrayNull(data.nameB),
                                        rules: [{
                                            required: this.state.knowledgeAnswer.includes("B"),
                                            message: "Field is required",
                                        }],
                                    })(

                                        <Input
                                            onChange={this.handleFormChange}
                                            placeholder={question[2].p}
                                        />

                                    )}
                                </FormItem>
                            </Col>}

                        {this.state.knowledgeAnswer.includes("C") &&
                            <Col xs={24} sm={8}>
                                <FormItem label={question[3].q}>
                                    {getFieldDecorator("nameC", {
                                        initialValue: isArrayNull(data.nameC),
                                        rules: [{
                                            required: this.state.knowledgeAnswer.includes("C"),
                                            message: "Field is required",
                                        }],
                                    })(
                                        <Input
                                            onChange={this.handleFormChange}
                                            placeholder={question[3].p}
                                        />
                                    )}
                                </FormItem>
                            </Col>
                        }

                    </HiddenForm>


                </Row>
            </Form>
        );
    }
}

export default Form.create()(TroutForm);
