import { Modal, Row, Button, Select } from "antd";
import React, { Component } from "react";

import styled from "styled-components";

import { connect } from "react-redux";
import { dimensions } from "helpers";
import { createTrout } from "redux-modules/trout/actions";
import { alertActions } from "redux-modules/alert/actions";
import { Descriptions, Alerts, ButtonText } from "./Translations";

import TroutForm from "./TroutForm";
import PersonalForm from "./PersonalForm";
import TroutFormSighting from "./TroutFormSighting";

const Option = Select.Option;
const Flag = styled.img`
  height: 25px;
  margin: 2px auto;
  display: block;
`;
const InfoContainer = styled.div`
  margin: 50px auto;
  margin-bottom: 30px;
  h1,
  p {
    text-align: center;
  }

  p {
    margin-bottom: 30px;
    font-size: 1.1em;
    width: 90%;
    display: block;
    margin: auto;
  }
`;

const CreatureImageContainer = styled.div`
    width: 100%;
    margin: 0 auto 80px auto;
    position: relative;
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;

    div {
        width: 33%;
        padding: 10px 20px;
        box-sizing: border-box;

        @media(max-width: ${dimensions.md}) {
            width: 50%;
        }

        @media(max-width: ${dimensions.sm}) {
            width: 100%;
        }

        img {
            width: 100%;
        }

        p {
            text-align: center;
        }
    }
`;

const FormContainer = styled.div`
  display: block;
  width: 80%;
  margin: 50px auto;

  @media (max-width: 768px) {
    width: 100%;
  }

  .ant-form label {
    color: black !important;
  }
`;

class TroutContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            finishForm: false,
            currentStep: 0, // current modal page
            formValidated: false, //form validation for first page
            lang: "pt",
            formData: {
                personal: {},
                observations: {},
                sightingsA: [],
                sightingsB: [],
                sightingsC: [],
                knowledgeArray: []
            },
            addonSteps: [],
        };

        // preserve the initial state in a new object
        this.baseState = this.state;
    }

    componentDidMount() {
        let { creatureId } = this.props;

        this.setState({
            creatureId: creatureId,
        });
    }

    next = () => {
        this.setState({
            currentStep: this.state.finishForm ? 2 : this.state.currentStep + 1,
            formValidated: false,
        });
    };

    prev = () => {
        const currentStep = this.state.currentStep - 1;
        this.setState({ currentStep, formValidated: false });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    saveActivityFormRef = (activityFormRef) => {
        this.activityFormRef = activityFormRef;
    };

    handlePersonalForm = (field) => {
        this.activityFormRef = activityFormRef;
    };

    handleFormValidation = (data) => {
        if (data.knowledgeArray) {
            var array = [];
            data.knowledgeArray.map((ele, index) => {
                array.push({
                    key: index + 3,
                    content: (
                        <FormContainer>
                            <TroutFormSighting
                                animal={ele}
                                lang={this.state.lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                                data={this.state.formData.observations}
                            />
                        </FormContainer>
                    ),
                })
            })

            this.setState({ addonSteps: array })
        }
        console.log(this.steps);
        this.setState({
            formValidated: true,
            formData: { ...this.state.formData, ...data },
        });
    };

    handleSubmit = () => {
        let { formData, finishForm } = this.state;
        if (!finishForm) {
            this.props.createTrout({
                ...formData.personal,
                ...formData.observations,
                ...formData.sightingsA,
                ...formData.sightingsB,
                ...formData.sightingsC,
            }).then(() => {
                this.handleModalClose();
            });
        } else {
            this.handleModalClose();
        }

    };

    handleModalClose = () => {
        this.setState(this.baseState);
        this.props.handleModalClose();
    };

    handleLanguageChange = (e) => {
        this.setState({
            lang: e,
        });
    };

    render() {
        var { currentStep, formValidated, lang, formData } = this.state;
        console.log(formData);
        const LanguageContainer = () => (
            <Row type="flex" justify="space-around">
                <Select
                    onChange={this.handleLanguageChange}
                    style={{ width: "80px" }}
                    value={lang}
                >
                    <Option value="en">
                        <Flag src="/images/flags/en.png" alt="" />
                    </Option>
                    <Option value="pt">
                        <Flag src="/images/flags/pt.png" alt="" />
                    </Option>
                </Select>
            </Row>
        );

        const steps = [
            {
                key: 0,
                content: (
                    <InfoContainer>
                        <h1>Freshwater Vertebrates</h1>

                        <p>{Descriptions[lang][0]}</p>
                        <br />
                        <p>{Descriptions[lang][1]}</p>
                        <br />
                        <p>{Alerts[lang][0]}</p>

                        <FormContainer>
                            <LanguageContainer />
                            <PersonalForm
                                lang={lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                                data={formData.personal}
                            />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            {
                key: 1,
                content: (
                    <InfoContainer>
                        <CreatureImageContainer type="flex" justify="space-around">
                            <div>
                                <img alt="Image A" src={`/storage/images/studies/trout/A.jpg`} />
                                <p>{Alerts[lang][2]} A</p>
                            </div>
                            <div>
                                <img alt="Image C" src={`/storage/images/studies/trout/B.jpg`} />
                                <p>{Alerts[lang][2]} B</p>
                            </div>
                            <div>
                                <img alt="Image D" src={`/storage/images/studies/trout/C.jpg`} />
                                <p>{Alerts[lang][2]} C</p>
                            </div>
                        </CreatureImageContainer>

                        <FormContainer>
                            <TroutForm
                                lang={lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                                data={this.state.formData.observations}
                                finishForm={(finish = true) =>
                                    this.setState({
                                        finishForm: finish,
                                        formValidated: finish,
                                    })
                                }
                            />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            ...this.state.addonSteps,

            {
                key: 100,
                content: (
                    <InfoContainer>
                        <h1>{lang == "en" ? "Thank you" : "Muito Obrigado(a)"}</h1>

                        <p>{Descriptions[lang][3]}</p>
                    </InfoContainer>
                ),
            },
        ];

        return (
            <Modal
                onCancel={this.handleModalClose}
                centered
                visible={this.props.visible}
                closable
                style={{ maxWidth: "1270px", color: "black" }}
                width={"100%"}
                footer={[
                    currentStep < 1 ? (
                        <Row key={0} type="flex" justify="center">
                            <Button
                                disabled={!formValidated}
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.next}
                            >
                                {ButtonText[lang][1]}
                            </Button>
                        </Row>
                    ) : currentStep == steps.length - 1 ? (
                        <Row key={2} type="flex" justify="center" gutter={16}>
                            <Button
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.handleSubmit}
                            >
                                {ButtonText[lang][2]}
                            </Button>
                        </Row>
                    ) : (
                        <Row key={1} type="flex" justify="center" gutter={16}>
                            <Button size="large" onClick={this.prev}>
                                {ButtonText[lang][0]}
                            </Button>
                            <Button
                                disabled={!formValidated}
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.next}
                            >
                                {ButtonText[lang][1]}
                            </Button>
                        </Row>
                    ),
                ]}
            >
                <div>{steps[currentStep].content}</div>
            </Modal>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createTrout: (data) => dispatch(createTrout(data)),
        errorAlert: (data) => dispatch(alertActions.error(data)),
        successAlert: (data) => dispatch(alertActions.success(data)),
    };
};

export default connect(
    null,
    mapDispatchToProps
)(TroutContent);
