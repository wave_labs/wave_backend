import { Modal, Row, Button, Select } from "antd";
import React, { Component } from "react";

import styled from "styled-components";

import { connect } from "react-redux";
import { createFreshwaterPin } from "redux-modules/freshwaterPin/actions";
import { alertActions } from "redux-modules/alert/actions";
import { Descriptions, Alerts } from "./Translations";

import FreshwaterPinForm from "./FreshwaterPinForm";
import PersonalForm from "./PersonalForm";
import CoordinatesForm from "./CoordinatesForm";

const Option = Select.Option;
const Flag = styled.img`
  height: 25px;
  margin: 2px auto;
  display: block;
`;
const InfoContainer = styled.div`
  margin: 50px auto;
  margin-bottom: 30px;
  h1,
  p {
    text-align: center;
  }

  p {
    margin-bottom: 30px;
    font-size: 1.1em;
    width: 90%;
    display: block;
    margin: auto;
  }
`;

const CreatureImageContainer = styled(Row)`
  width: 90%;
  margin: 0 auto 80px auto;
  position: relative;
`;

const Image = styled.div`
  display: block;
  position: relative;
  width: 240px;
  height: 240px;
  border-radius: 50%;
  background-color: #eeeeee;
  background-image: url(${(props) => props.background});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;

  &:before {
    display: block;
    padding-top: 100%;
  }
`;

const FormContainer = styled.div`
  display: block;
  width: 80%;
  margin: 50px auto;

  @media (max-width: 768px) {
    width: 100%;
  }

  .ant-form label {
    color: black !important;
  }
`;

class FreshwaterPinContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            finishForm: false,
            currentStep: 0, // current modal page
            formValidated: false, //form validation for first page
            lang: "pt",
            formData: {
                personal: {},
                observations: {},
                coordinates: {},
            },
        };

        // preserve the initial state in a new object
        this.baseState = this.state;
    }

    componentDidMount() {
        let { creatureId } = this.props;

        this.setState({
            creatureId: creatureId,
        });
    }

    next = () => {
        this.setState({
            currentStep: this.state.finishForm ? 3 : this.state.currentStep + 1,
            formValidated: false,
        });
    };

    prev = () => {
        const currentStep = this.state.currentStep - 1;
        this.setState({ currentStep, formValidated: false });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    saveActivityFormRef = (activityFormRef) => {
        this.activityFormRef = activityFormRef;
    };

    handlePersonalForm = (field) => {
        this.activityFormRef = activityFormRef;
    };

    handleFormValidation = (data) => {
        this.setState({
            formValidated: true,
            formData: { ...this.state.formData, ...data },
        });
    };

    handleSubmit = () => {
        let { formData, finishForm } = this.state;

        let formQuestions = {
            form: {
                ...formData.personal,
                ...formData.observations,
                observations: formData.coordinates.observations,
            },
            positions: formData.coordinates.positions,
        };

        this.props.createFreshwaterPin(formQuestions).then(() => {
            this.handleModalClose();
        });
    };

    handleModalClose = () => {
        this.setState(this.baseState);
        this.props.handleModalClose();
    };

    handleLanguageChange = (e) => {
        this.setState({
            lang: e,
        });
    };

    render() {
        var { currentStep, formValidated, lang, formData } = this.state;

        const LanguageContainer = () => (
            <Row type="flex" justify="space-around">
                <Select
                    onChange={this.handleLanguageChange}
                    style={{ width: "80px" }}
                    value={lang}
                >
                    <Option value="en">
                        <Flag src="/images/flags/en.png" alt="" />
                    </Option>
                    <Option value="pt">
                        <Flag src="/images/flags/pt.png" alt="" />
                    </Option>
                </Select>
            </Row>
        );

        const steps = [
            {
                key: 0,
                content: (
                    <InfoContainer>
                        <h1>Freshwater Pin</h1>

                        <p>{Descriptions[lang][0]}</p>
                        <br />
                        <p>{Descriptions[lang][1]}</p>
                        <br />
                        <p>{Alerts[lang][0]}</p>

                        <FormContainer>
                            <LanguageContainer />
                            <PersonalForm
                                lang={lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                                data={formData.personal}
                            />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            {
                key: 1,
                content: (
                    <InfoContainer>
                        <CreatureImageContainer type="flex" justify="space-around">
                            <div>
                                <Image background={`/api/image/creature/${"55"}`} />
                                <h1>{Alerts[lang][2]} A</h1>
                            </div>
                            <div>
                                <Image background={`/api/image/creature/${"55b"}`} />
                                <h1>{Alerts[lang][2]} B</h1>
                            </div>
                        </CreatureImageContainer>

                        <FormContainer>
                            <FreshwaterPinForm
                                lang={lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                                data={this.state.formData.observations}
                                finishForm={(finish = true) =>
                                    this.setState({
                                        finishForm: finish,
                                        formValidated: finish,
                                    })
                                }
                            />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            {
                key: 2,
                content: (
                    <FormContainer>
                        <CoordinatesForm
                            knowledge={this.state.formData.observations.knowledge}
                            handleFormValidation={this.handleFormValidation}
                            data={this.state.formData.coordinates}
                            lang={lang}
                            setFormValidated={(val = true) =>
                                this.setState({
                                    formValidated: val,
                                })
                            }
                        />
                    </FormContainer>
                ),
            },
            {
                key: 3,
                content: (
                    <InfoContainer>
                        <h1>{lang == "en" ? "Thank you" : "Muito Obrigado(a)"}</h1>

                        <p>{Descriptions[lang][3]}</p>
                    </InfoContainer>
                ),
            },
        ];

        return (
            <Modal
                onCancel={this.handleModalClose}
                centered
                visible={this.props.visible}
                closable
                style={{ maxWidth: "960px", color: "black" }}
                width={"100%"}
                footer={[
                    currentStep < 1 ? (
                        <Row key={0} type="flex" justify="center">
                            <Button
                                disabled={!formValidated}
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.next}
                            >
                                Next
                            </Button>
                        </Row>
                    ) : currentStep == steps.length - 1 ? (
                        <Row key={2} type="flex" justify="center" gutter={16}>
                            <Button
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.handleSubmit}
                            >
                                Close
                            </Button>
                        </Row>
                    ) : (
                        <Row key={1} type="flex" justify="center" gutter={16}>
                            <Button size="large" onClick={this.prev}>
                                Back
                            </Button>
                            <Button
                                disabled={!formValidated}
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.next}
                            >
                                Next
                            </Button>
                        </Row>
                    ),
                ]}
            >
                <div>{steps[currentStep].content}</div>
            </Modal>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createFreshwaterPin: (data) => dispatch(createFreshwaterPin(data)),
        errorAlert: (data) => dispatch(alertActions.error(data)),
        successAlert: (data) => dispatch(alertActions.success(data)),
    };
};

const mapStateToProps = (state) => {
    return {
        creatureDimensions: state.creature.dimensions,
        creatureActivities: state.creature.activities,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FreshwaterPinContent);
