import React from "react";
import { Select } from "antd";
import styled from "styled-components";
import { dimensions } from "helpers";

const Option = Select.Option;

const Container = styled.div`
  width: 100px;
  position: absolute;
  bottom: 0;
  right: 0;

  .ant-select-selection--single {
    height: 40px;
  }

  @media (max-width: ${dimensions.sm}) {
    width: 80px;
    margin: auto -30px -25px auto;

    .ant-select-selection--single {
      height: 32px;
    }

    img {
      height: 22px;
    }
  }
`;

const Flag = styled.img`
  height: 30px;
  margin: 5px auto;
  display: block;
`;

const LanguageContainer = ({ lang, onChange }) => {
  return (
    <Container>
      <Select onChange={onChange} style={{ width: "100%" }} value={lang}>
        <Option value="en">
          <Flag src="/images/flags/en.png" alt="" />
        </Option>
        <Option value="pt">
          <Flag src="/images/flags/pt.png" alt="" />
        </Option>
      </Select>
    </Container>
  );
};

export default LanguageContainer;
