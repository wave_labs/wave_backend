import React, { Component } from "react";
import { Row, Col, Select, Form, Input } from "antd";
import { Questions, Informations } from "./Translations";
import { isArrayNull } from "./helper";
import styled from "styled-components";

const FormItem = Form.Item;
const Option = Select.Option;

const HiddenForm = styled.div`
  transition: 3s ease-in-out;
  display: ${(props) => (props.hidden ? "none" : "block")};
`;

const Notice = styled.p`
  font-weight: bold;
`;

class FreshwaterPinForm extends Component {
  state = {
    finish: true,
  };

  rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  componentDidMount = () => {
    this.handleFormChange();
    if (this.props.data.knowledge) {
      this.setState({ finish: false });
    }
  };

  handleFormChange = () => {
    setTimeout(() => {
      let form = this.props.form.getFieldsValue();

      if (
        form.knowledge &&
        form.animal &&
        form.year &&
        form.period_year &&
        form.period_day &&
        form.quantity &&
        form.stream &&
        form.activity
      ) {
        this.props.handleFormValidation({ observations: form });
      }
    }, 5);
  };

  continueForm = (value) => {
    let finish = true;
    if (value !== "No, none") {
      finish = false;
      this.handleFormChange();
    }

    this.props.finishForm(finish);
    this.setState({ finish: finish });
  };

  render() {
    const { lang, data } = this.props;
    const { getFieldDecorator } = this.props.form;
    let question = Questions[lang];

    const SelectFormItem = ({
      fieldName,
      index,
      last,
      handleField = this.handleFormChange,
    }) => (
      <Col xs={24} sm={last ? 24 : 12}>
        <FormItem label={question[index].q}>
          {getFieldDecorator(fieldName, {
            rules: this.rules.requiredField,
            initialValue: isArrayNull(data[fieldName]),
          })(
            <Select onChange={handleField} placeholder={question[index].p}>
              {question[index].opt.map((option, i) => (
                <Option key={i} value={Questions["en"][index].opt[i]}>
                  {option}
                </Option>
              ))}
            </Select>
          )}
        </FormItem>
      </Col>
    );
    return (
      <Form hideRequiredMark={true} className="wrap-label">
        <Row type="flex" align="middle" gutter={32}>
          <SelectFormItem
            handleField={(e) => this.continueForm(e)}
            last
            fieldName="knowledge"
            index={0}
          />

          <HiddenForm hidden={this.state.finish}>
            <Col xs={24} sm={12}>
              <FormItem label={question[1].q}>
                {getFieldDecorator("nameA", {
                  initialValue: isArrayNull(data.nameA),
                })(
                  <Input
                    onChange={this.handleFormChange}
                    placeholder={question[1].p}
                  />
                )}
              </FormItem>
            </Col>
            <Col xs={24} sm={12}>
              <FormItem label={question[2].q}>
                {getFieldDecorator("nameB", {
                  initialValue: isArrayNull(data.nameB),
                })(
                  <Input
                    onChange={this.handleFormChange}
                    placeholder={question[2].p}
                  />
                )}
              </FormItem>
            </Col>
            <Notice>{Informations[lang][3]}</Notice>
            <SelectFormItem fieldName="animal" index={3} />
            <SelectFormItem fieldName="year" index={4} />
            <SelectFormItem fieldName="period_year" index={5} />
            <SelectFormItem fieldName="period_day" index={6} />
            <SelectFormItem fieldName="quantity" index={7} />
            <Col xs={24} sm={12}>
              <FormItem label={question[8].q}>
                {getFieldDecorator("stream", {
                  initialValue: isArrayNull(data.stream),
                })(<Input placeholder={question[8].p} />)}
              </FormItem>
            </Col>
            <Col xs={24} sm={12}>
              <FormItem label={question[9].q}>
                {getFieldDecorator("location", {
                  initialValue: isArrayNull(data.location),
                })(<Input placeholder={question[9].p} />)}
              </FormItem>
            </Col>
            <SelectFormItem fieldName="council" index={10} />
            <SelectFormItem fieldName="activity" index={11} last />
          </HiddenForm>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(FreshwaterPinForm);
