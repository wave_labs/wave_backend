export const Descriptions = {
    pt: {
        0: "Está a decorrer um estudo sobre as espécies presentes nas ribeiras do Arquipélago da Madeira no âmbito de um projeto de investigação do MARE-Madeira financiado pela ARDITI (Agência Regional para o Desenvolvimento da Investigação, Tecnologia e Inovação). Pedimos que preencha este inquérito e nos ajude a avaliar a presença de uma espécie nas ribeiras das ilhas da Madeira e do Porto Santo.",
        1: "O inquérito é de resposta voluntária e anónimo, e os dados recolhidos serão tratados de forma confidencial, sem que os participantes possam vir a ser identificados, tendo como única finalidade a sua utilização no âmbito deste trabalho de investigação. As respostas a este inquérito serão guardadas em segurança num computador protegido por senha, de forma a manter o anonimato dos participantes.",
        2: "Selecione uma atividade na qual avistou o peixe.",
        3: "Caso queira continuar a colaborar poderá divulgar este questionário, convidar outras pessoas a participar e se desejar falar connosco poderá contactar-nos para inesorfao@mare-centre.pt.",
    },
    en: {
        0: "Species present at the streams of the archipelago of Madeira are being investigated.within the scope of a research project of MARE-Madeira funded by ARDITI (Agência Regional para o Desenvolvimento da Investigação, Tecnologia e Inovação). We kindly ask you to fill this survey, helping us evaluating the presence of one species at the freshwater ecosystems in Madeira and Porto Santo islands.",
        1: "The survey is voluntary and anonymous, and the collected data will be treated as confidential, so participants are not identified, being exclusively used within the scope of this project. To maintain the anonymity of participants answers given to the survey will be securely saved in a password-protected computer.",
        2: "Please, select an activity in which you detected this fish.",
        3: "If you want to collaborate, please share this survey, invite other people to participate and, if you wish to contact us, please send an email to inesorfao@mare-centre.pt.",
    }
}

export const Alerts = {
    pt: {
        0: "Muito obrigada pela sua colaboração!",
        1: "Por favor aumente o zoom do mapa, de modo a aumentar a precisão",
        2: "Figura",
    },
    en: {
        0: "We thank you for your collaboration!",
        1: "Please increase map's zoom before adding pins in order to increase precision",
        2: "Figure",
    }
}


export const PersonalQuestions = {
    pt: [
        { q: "Idade? *", p: "Idade", opt: ['Menos de 20 anos', '20-39 anos', '40-60 anos', 'Mais de 60 anos'] },
        { q: "Região de residência *", p: "Região de residência", opt: ['Arquipélago da Madeira', 'Outra região de Portugal', 'Estrangeiro'] },
    ],
    en: [
        { q: "Age? *", p: "Age", opt: ['Less than 20 years', '20-39 years', '40-60 years', 'More than 60 years'] },
        { q: "Residence region *", p: "Residence region", opt: ['Madeira archipelago', 'Other region of Portugal', 'Abroad'] },
    ]
}

export const Informations = {
    pt: [
        'Preencha todos os campos necessários antes de avançar',
        'Selecione uma opção temporal na qual tenha avistado a espécie em questão e posicione o(s) ponto(s) no mapa. Repita o processo para cada opção temporal respetivamente',
        'Clique no lado esquerdo do rato para criar um novo ponto, arraste-o no mapa ou clique no lado direto do rato para o remover',
        'As perguntas que se seguem referem-se à sua última observação'
    ],
    en: [
        'Select requested information before advancing',
        'Select a temporal option in which you detected this species and position the pin(s) on the map. Repeat the process for each of the options respectively',
        'Left click to create a new pin, drag it to move it on the map or right click to delete it',
        'Following questions refer to your latest observation'
    ],
}

export const CoordinatesQuestions = {
    pt: [
        ["Nos últimos 5 anos", "Há mais de 5 anos"],
        { q: "Observações", p: "Inclua toda e qualquer informação que considere relevante adicionar" },
    ],
    en: [
        ["In the last 5 years", "Over 5 years ago"],
        { q: "Observations", p: "Include all information you consider relevant" },
    ]
}

export const Questions = {
    pt: [
        { q: "Alguma vez viu este animal nas ilhas da Madeira ou do Porto Santo? *", p: "Sim, o A / Sim, o B / Sim, ambos / Não, nenhum", opt: ['Sim, o A', 'Sim, o B', 'Sim, ambos', 'Não, nenhum'] },
        { q: "Escreva o nome do animal A", p: "Espécie A" },
        { q: "Escreva o nome do animal B", p: "Espécie B" },
        { q: "Qual é que viu? *", p: "A / B / Ambos", opt: ['A', 'B', 'Ambos'] },
        { q: "Em que ano? *", p: "2021 / 2020 / 2019-2015 / antes de 2015 / Não me recordo", opt: ['2021', '2020', '2019-2015', 'antes de 2015', 'Não me recordo'] },
        { q: "Em que época do ano? *", p: "Primavera / Verão / Outono / Inverno / Não me recordo", opt: ['Primavera', 'Verão', 'Outono', 'Inverno', 'Não me recordo'] },
        { q: "Em que altura do dia? *", p: "Manhã / Tarde / Noite / Não me recordo", opt: ['Manhã', 'Tarde', 'Noite', 'Não me recordo'] },
        { q: "Quantos viu? *", p: "1 / 2-10 / mais de 10", opt: ['1', '2-10', 'mais de 10'] },
        { q: "Em que ribeira? *", p: "Nome da ribeira" },
        { q: "Qual a localidade mais próxima?", p: "Nome da localidade mais próxima" },
        { q: "Pertencente a que concelho?", p: "Calheta / Câmara de Lobos / Funchal / Machico / Porto Moniz / São Vicente / Ponta do Sol / Ribeira Brava / Santa Cruz / Santana / Porto Santo / Do not know", opt: ['Calheta', 'Câmara de Lobos', 'Funchal', 'Machico', 'Porto Moniz', 'São Vicente', 'Ponta do Sol', 'Ribeira Brava', 'Santa Cruz', 'Santana', 'Porto Santo', 'Do not know'] },
        { q: "Indique qual a atividade que estava a realizar na altura *", p: "Canoagem / Caminhada / Pesca / Nenhuma / Não me recordo", opt: ['Canoagem', 'Caminhada', 'Pesca', 'Nenhuma', 'Não me recordo'] },
    ],
    en: [
        { q: "Have you ever seen this animal in Madeira or Porto Santo islands? *", p: "Yes, the A / Yes, the B / Yes, A and B / No, none", opt: ['Yes, the A', 'Yes, the B', 'Yes, A and B', 'No, none'] },
        { q: "Write the name of the animal A", p: "Creature A" },
        { q: "Write the name of the animal B", p: "Creature B" },
        { q: "Which animal have you seen? *", p: "A / B / Both", opt: ['A', 'B', 'Both'] },
        { q: "In what year? *", p: "2021 / 2020 / 2019-2015 / before 2015 / Do not remember", opt: ['2021', '2020', '2019-2015', 'before 2015', 'Do not remember'] },
        { q: "During which period of the year? *", p: "Spring / Summer / Autumn / Winter / Do not remember", opt: ['Spring', 'Summer', 'Autumn', 'Winter', 'Do not remember'] },
        { q: "During which period of the day? *", p: "Morning / Afternoon / Night / Do not remember", opt: ['Morning', 'Afternoon', 'Night', 'Do not remember'] },
        { q: "How many have you seen? *", p: "1 / 2-10 / more than 10", opt: ['1', '2-10', 'more than 10'] },
        { q: "In what stream? *", p: "Name of the stream" },
        { q: "What was the nearest location?", p: "Name of the nearest place/village" },
        { q: "In which council?", p: "Calheta / Câmara de Lobos / Funchal / Machico / Porto Moniz / São Vicente / Ponta do Sol / Ribeira Brava / Santa Cruz / Santana / Porto Santo / Do not know", opt: ['Calheta', 'Câmara de Lobos', 'Funchal', 'Machico', 'Porto Moniz', 'São Vicente', 'Ponta do Sol', 'Ribeira Brava', 'Santa Cruz', 'Santana', 'Porto Santo', 'Do not know'] },
        { q: "Indicate which activity you were doing at the time *", p: "Canyoning / Trekking / Fishing / None of these / Do not remember", opt: ['Canyoning', 'Trekking', 'Fishing', 'None of these', 'Do not remember'] },
    ]
}