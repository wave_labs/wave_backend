import React, { Component } from "react";
import { Row, Radio, Input, message } from "antd";
import { CoordinatesQuestions } from "./Translations";
import { Fragment } from "react";
import { Marker } from "react-map-gl";
import MapBoxMap from "../../../Dashboard/Common/MapBoxMap";
import styled from "styled-components";
import { Informations, Alerts } from "./Translations";

const { TextArea } = Input;

const Image = styled.div`
  display: block;
  position: relative;
  margin: 30px auto;
  width: 200px;
  height: 200px;
  border-radius: 50%;
  background-color: #eeeeee;
  background-image: url(${(props) => props.background});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;

  &:before {
    display: block;
    padding-top: 100%;
  }
`;

class CoordinatesForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      positions: {
        A: [[], []],
        B: [[], []],
      },
      showMap: [],
      currentActive: { A: 0, B: 0 },
      observations: undefined,
      activeOnClick: true,
    };
    this.mapContainerRef = { A: React.createRef(), B: React.createRef() };
  }

  rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  getMarkerColor = (index) => {
    let color;
    switch (index) {
      case 0:
        color = "red";
        break;
      case 1:
        color = "blue";
        break;
      default:
        color = "black";
        break;
    }
    return color;
  };

  handleMarkerClick = (category, catIndex, index, e) => {
    if (!e.leftButton) {
      let { positions } = this.state;
      positions[category][catIndex].splice(index, 1);
      let length = 0;
      positions.A.map((firstMap) => {
        length += firstMap.length;
      });
      positions.B.map((firstMap) => {
        length += firstMap.length;
      });
      if (length == 0) {
        this.props.setFormValidated(false);
      }
      this.setState({ positions });
    }
  };

  handleMapClick = (e, key) => {
    if (e.leftButton) {
      let zoomLevel = this.getZoomLevel(key);
      if (zoomLevel <= 14.3) {
        message.warning(Alerts[this.props.lang][1]);
      } else {
        let { positions, activeOnClick, currentActive } = this.state;
        if (activeOnClick) {
          positions[key][currentActive[key]].push([e.lngLat[1], e.lngLat[0]]);
          this.setState({ positions }, () => this.props.setFormValidated());
        }
      }
    }
  };

  handleDragEnd = (event, catIndex, index, key) => {
    let { positions } = this.state;

    positions[key][catIndex][index] = [event.lngLat[1], event.lngLat[0]];

    setTimeout(() => {
      this.setState({
        positions,
        activeOnClick: true,
      });
    }, 1);
  };
  getZoomLevel = (key) => {
    return this.mapContainerRef[key].current.getMapZoom();
  };

  handleTimeline = (key, value) => {
    const { currentActive } = this.state;
    currentActive[key] = value;
    this.setState({ currentActive });
  };

  handleObs = (value) => {
    this.setState({ observations: value });
  };

  componentWillUnmount() {
    this.handleFormValidation();
  }

  handleFormValidation = () => {
    const { positions, observations } = this.state;

    this.props.handleFormValidation({
      coordinates: {
        positions: positions,
        observations: observations,
      },
    });
  };

  componentDidMount() {
    let showMap = [];
    switch (this.props.knowledge) {
      case "Yes, A and B":
        showMap.push(0, 1);
        break;
      case "Yes, the A":
        showMap.push(0);
        break;
      case "Yes, the B":
        showMap.push(1);
        break;
      default:
        break;
    }

    this.setState({ showMap: showMap });
  }

  render() {
    const { lang } = this.props;
    const { positions, currentActive, showMap } = this.state;

    const MapSection = ({ img, category }) => {
      return (
        <div>
          <Image background={`/api/image/creature/${img}`} />

          <Radio.Group
            style={{ width: "100%" }}
            onChange={(e) => this.handleTimeline(category, e.target.value)}
            value={currentActive[category]}
          >
            <Row
              type="flex"
              justify="space-between"
              style={{ marginBottom: "20px" }}
            >
              {CoordinatesQuestions[lang][0].map((option, index) => (
                <Radio key={index} value={index}>
                  {option}
                </Radio>
              ))}
            </Row>
          </Radio.Group>
        </div>
      );
    };
    return (
      <Fragment>
        <h2 style={{ textAlign: "center" }}>{Informations[lang][1]}</h2>
        <p style={{ textAlign: "center" }}>{Informations[lang][2]}</p>
        {Object.entries(positions).map((position, i) => (
          <div key={position[0]}>
            {this.state.showMap.includes(i) && (
              <Fragment>
                <MapSection
                  img={i == 0 ? "55" : "55b"}
                  category={position[0]}
                />
                <MapBoxMap
                  style="mapbox://styles/tigerwhale/ckt1c37d70vi317nyz3as5pza"
                  fixedSize={true}
                  size="400"
                  minZoom={8}
                  zoom={8}
                  onClick={(e) => this.handleMapClick(e, position[0])}
                  ref={this.mapContainerRef[position[0]]}
                >
                  {position[1].map((element, catIndex) => {
                    {
                      return element.map((e, index) => {
                        return (
                          <Marker
                            key={index}
                            latitude={e[0]}
                            longitude={e[1]}
                            draggable
                            onDragEnd={(event) =>
                              this.handleDragEnd(
                                event,
                                catIndex,
                                index,
                                position[0]
                              )
                            }
                            onDragStart={() =>
                              this.setState({ activeOnClick: false })
                            }
                          >
                            <svg
                              onContextMenu={(action) =>
                                this.handleMarkerClick(
                                  position[0],
                                  catIndex,
                                  index,
                                  action
                                )
                              }
                              style={{
                                position: "absolute",
                                top: "-10px",
                                left: "-10px",
                                width: "20px",
                                height: "20px",
                                fill: this.getMarkerColor(catIndex),
                              }}
                            >
                              <circle cx="10" cy="10" r="10" stroke="none" />
                            </svg>
                          </Marker>
                        );
                      });
                    }
                  })}
                </MapBoxMap>
              </Fragment>
            )}
          </div>
        ))}
        <br />
        <p>{CoordinatesQuestions[lang][1].q}: </p>
        <TextArea
          value={this.state.observations}
          onChange={(e) => this.handleObs(e.target.value)}
          autosize={{ minRows: 6, maxRows: 6 }}
          placeholder={CoordinatesQuestions[lang][1].p}
        />
      </Fragment>
    );
  }
}

export default CoordinatesForm;
