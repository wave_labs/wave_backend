import { Modal, Row, Button, Select, Icon } from "antd";
import React, { Component } from "react";

import styled from "styled-components";

import { connect } from "react-redux";
import { dimensions } from "helpers";
import { createClimarest } from "redux-modules/climarest/actions";
import { alertActions } from "redux-modules/alert/actions";
import { Descriptions, Alerts, ButtonText, Informations } from "./Translations";

import StakeholdersBForm from "./StakeholdersBForm";
import AreaForm from "./AreaForm";
import StakeholdersCForm from "./StakeholdersCForm";
import StakeholdersDForm from "./StakeholdersDForm";

const Option = Select.Option;
const Flag = styled.img`
  height: 25px;
  margin: 2px auto;
  display: block;
`;
const InfoContainer = styled.div`
  margin: 50px auto;
  margin-bottom: 30px;
  h1,
  p {
    text-align: center;
  }

  p {
    margin-bottom: 30px;
    font-size: 1.1em;
    width: 90%;
    display: block;
    margin: auto;
  }
`;

const TitleContainer = styled.div`
  margin: 50px auto;
  margin-bottom: 30px;
  height: auto;

  h1 {
    text-align: center;
  }
  img {
    height: 50px;
    position: absolute;
    top: 10px;
  }
`;

const FormContainer = styled.div`
  display: block;
  width: 80%;
  margin: 50px auto;

  @media (max-width: 768px) {
    width: 100%;
  }

  .ant-form label {
    color: black !important;
  }
`;

class ClimarestContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            finishForm: false,
            currentStep: 0, // current modal page
            formValidated: false, //form validation for first page
            lang: "en",
            occurrencesB: 1,
            occurrencesC: 1,
            occurrencesD: 1,
            formData: {
                area: {},
                stakeholder: {
                    B: [],
                    C: [],
                    D: [],
                },
            },
        };

        // preserve the initial state in a new object
        this.baseState = this.state;
    }

    next = () => {
        this.setState({
            currentStep: this.state.finishForm ? 2 : this.state.currentStep + 1,
            formValidated: false,
        });
    };

    prev = () => {
        const currentStep = this.state.currentStep - 1;
        this.setState({ currentStep, formValidated: false });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    handleFormValidation = (data) => {
        this.setState({
            formValidated: true,
            formData: { ...this.state.formData, ...data },
        });
    };

    handleSubmit = () => {
        let { formData } = this.state;

        this.props.createClimarest(formData).then(() => {
            this.handleModalClose();
        });
    };

    handleModalClose = () => {
        this.setState(this.baseState);
        this.props.handleModalClose();
    };

    handleNewStakeholder = () => {
        console.log(this.state.currentStep);
        if (this.state.currentStep == 1) {
            this.setState({ occurrencesB: this.state.occurrencesB + 1 });
            console.log(occurrencesB);
        } else if (this.state.currentStep == 2) {
            this.setState({ occurrencesC: this.state.occurrencesC + 1 });
            console.log(occurrencesC);
        } else if (this.state.currentStep == 3) {
            this.setState({ occurrencesD: this.state.occurrencesD + 1 });
            console.log(occurrencesD);
        }
    };

    render() {
        var {
            currentStep,
            formValidated,
            lang,
            formData,
            occurrencesB,
            occurrencesC,
            occurrencesD,
        } = this.state;

        const steps = [
            {
                key: 0,
                content: (
                    <InfoContainer>
                        <TitleContainer>
                            <h1>CLIMAREST: Stakeholder survey</h1>
                            <img src="/storage/images/logo/climarest.png" />
                        </TitleContainer>
                        <Row>
                            <p>{Descriptions[lang][0]}</p>
                            <br />
                            <p>{Descriptions[lang][1]}</p>
                            <br />
                            <p>{Descriptions[lang][2]}</p>
                            <br />
                            <p>{Descriptions[lang][3]}</p>
                            <br />
                            <p>{Descriptions[lang][4]}</p>
                            <br />
                            <p>{Descriptions[lang][5]}</p>
                        </Row>

                        <FormContainer>
                            <AreaForm
                                visible={this.props.visible}
                                lang={lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                                data={formData.area}
                            />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            {
                key: 1,
                content: (
                    <InfoContainer>
                        <TitleContainer>
                            <h1>CLIMAREST: Stakeholder survey</h1>
                            <img src="/storage/images/logo/climarest.png" />
                        </TitleContainer>
                        <p>
                            For section B, please identify stakeholders with formal
                            involvement with regulation, administration, management,
                            enforcement, or harvesting of resources. Based on your experience
                            and perception, please fill out all questions for each
                            stakeholder.
                        </p>

                        <FormContainer>
                            <StakeholdersBForm
                                lang={lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                                data={formData.stakeholder}
                                occurrencesB={occurrencesB}
                            />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            {
                key: 2,
                content: (
                    <InfoContainer>
                        <TitleContainer>
                            <h1>CLIMAREST: Stakeholder survey</h1>
                            <img src="/storage/images/logo/climarest.png" />
                        </TitleContainer>
                        <p>
                            For section C, please identify stakeholders who are commercial /
                            professional users of the seascape / landscape. Based on your
                            experience and perception, please fill out all questions for each
                            stakeholder.
                        </p>

                        <FormContainer>
                            <StakeholdersCForm
                                lang={lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                                data={formData.stakeholder}
                                occurrencesC={occurrencesC}
                            />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            {
                key: 3,
                content: (
                    <InfoContainer>
                        <TitleContainer>
                            <h1>CLIMAREST: Stakeholder survey</h1>
                            <img src="/storage/images/logo/climarest.png" />
                        </TitleContainer>
                        <p>
                            For section D, please identify stakeholders who are recreational
                            users of the seascape / landscape. Based on your experience and
                            perception, please fill out all questions for each stakeholder.
                        </p>

                        <FormContainer>
                            <StakeholdersDForm
                                lang={lang}
                                handleFormValidation={this.handleFormValidation}
                                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                                data={formData.stakeholder}
                                occurrencesD={occurrencesD}
                            />
                        </FormContainer>
                    </InfoContainer>
                ),
            },
            {
                key: 100,
                content: (
                    <InfoContainer>
                        <h1>{lang == "en" ? "Thank you" : "Muito Obrigado(a)"}</h1>

                        <p>{Descriptions[lang][6]}</p>
                    </InfoContainer>
                ),
            },
        ];

        return (
            <Modal
                onCancel={this.handleModalClose}
                centered
                visible={this.props.visible}
                closable
                style={{ maxWidth: "1270px", color: "black" }}
                width={"100%"}
                footer={[
                    currentStep < 1 ? (
                        <Row key={0} type="flex" justify="center">
                            <Button
                                disabled={!formValidated}
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.next}
                            >
                                {ButtonText[lang][1]}
                            </Button>
                        </Row>
                    ) : currentStep == steps.length - 1 ? (
                        <Row key={2} type="flex" justify="center" gutter={16}>
                            <Button
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.handleSubmit}
                            >
                                {ButtonText[lang][2]}
                            </Button>
                        </Row>
                    ) : (
                        <Row key={1} type="flex" justify="center" gutter={16}>
                            <Button type="dashed" onClick={this.handleNewStakeholder}>
                                <Icon type="plus" /> {Informations[lang][3]}
                            </Button>
                            <Button size="large" onClick={this.prev}>
                                {ButtonText[lang][0]}
                            </Button>
                            <Button
                                disabled={!formValidated}
                                size="large"
                                key="submit"
                                type="primary"
                                onClick={this.next}
                            >
                                {ButtonText[lang][1]}
                            </Button>
                        </Row>
                    ),
                ]}
            >
                <div>{steps[currentStep].content}</div>
            </Modal>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.climarest.loading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        createClimarest: (data) => dispatch(createClimarest(data)),
        errorAlert: (data) => dispatch(alertActions.error(data)),
        successAlert: (data) => dispatch(alertActions.success(data)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ClimarestContent);
