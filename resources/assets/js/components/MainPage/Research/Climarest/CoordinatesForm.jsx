import React, { Component } from "react";
import { Marker } from "react-map-gl";
import MapBoxMap from "../../../Dashboard/Common/MapBoxMap";
import { Input, Popover } from "antd";

class CoordinatesForm extends Component {

    rules = {
        requiredField: [
            {
                required: true,
                message: "Field is required",
            },
        ],
    };


    handleMarkerClick = (e, index) => {
        if (!e.leftButton) {
            let { positions } = this.props.state;

            var newPositons = positions.filter((_, i) => {
                return i !== index;
            });
            this.props.setState({ positions: newPositons, currentIndex: null });
        }
    };

    handleMapClick = (e) => {
        if (e.leftButton) {
            let { positions, activeOnClick } = this.props.state;
            if (activeOnClick) {
                positions = [...positions, { lat: e.lngLat[1], lng: e.lngLat[0], label: undefined }];

                this.props.setState({ positions, currentIndex: positions.length - 1 });
            }
        }
    };

    handleDragEnd = (event, index) => {
        let { positions } = this.props.state;

        positions[index] = { lat: event.lngLat[1], lng: event.lngLat[0], label: positions[index].label };

        setTimeout(() => {
            this.props.setState({
                positions,
                activeOnClick: true,
            });
        }, 1);
    };

    handleLabelChange = (value, index) => {
        let { positions } = this.props.state;

        positions[index] = { lat: positions[index].lat, lng: positions[index].lng, label: value };

        this.props.setState({
            positions,
        });
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.state.positions != this.props.state.positions) {
            if (this.props.state.positions.length == 2) {
                var object = {};
                object.latitude = this.props.state.positions[0];
                object.longitude = this.props.state.positions[1];

                this.props.handlePositions(object)
            }
        }
    }



    render() {
        const { positions, currentIndex } = this.props.state;
        console.log(this.props.center)
        return (
            <MapBoxMap
                fixedSize
                size="400px"
                defaultCenter={{ lat: this.props.center[0], lng: this.props.center[1] }}
                zoom={6}
                onClick={(e) => this.handleMapClick(e)}
            >
                {positions.map((position, index) => (
                    <Popover
                        key={index}
                        content={(
                            <div>
                                <Input.Search value={position.label} enterButton="Save" onChange={(e) => this.handleLabelChange(e.target.value, index)} onSearch={(e) => this.props.setState({ currentIndex: null })} />
                            </div>
                        )}

                        visible={currentIndex === index}
                    >
                        <Marker
                            latitude={position.lat}
                            longitude={position.lng}
                            draggable
                            onClick={() =>
                                this.props.setState({ currentIndex: index })
                            }
                            onDragEnd={(e) => this.handleDragEnd(e, index)}
                            onDragStart={() =>
                                this.props.setState({ activeOnClick: false })
                            }
                        >
                            <svg

                                onContextMenu={(e) => this.handleMarkerClick(e, index)}
                                style={{
                                    position: "absolute",
                                    top: "-10px",
                                    left: "-10px",
                                    width: "20px",
                                    height: "20px",
                                    fill: "red",
                                }}
                            >
                                <circle cx="10" cy="10" r="10" stroke="none" />
                            </svg>
                        </Marker>
                    </Popover>
                ))}

            </MapBoxMap>
        );
    }
}

export default CoordinatesForm;
