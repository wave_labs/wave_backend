import React, { Component } from "react";
import { Select, Form, Input } from "antd";
import { AreaQuestions } from "./Translations";
import { isArrayNull } from "./helper";
import { dimensions } from "helpers";
import styled from "styled-components";
import CoordinatesForm from "./CoordinatesForm";

const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const Row = styled.section`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: flex-start;
`;

const Col = styled.div`
    width: ${props => props.width ? props.width : "50%"};
    padding: 10px;
    box-sizing: border-box;

    @media (max-width: ${dimensions.md}) {
        width: 100%;
    }
`;

class AreaForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            center: [32, -16],
            positions: [],
            currentIndex: null,
            activeOnClick: true,
        };
    }

    rules = {
        requiredField: [
            {
                required: true,
                message: "Field is required",
            },
        ],
    };

    componentDidMount = () => {
        this.handleFormChange();
    };

    componentDidUpdate(prevProps) {
        if (prevProps.visible != this.props.visible) {
            this.setState({
                positions: [],
                currentIndex: null,
                activeOnClick: true,
            });
        }
    }

    handleRestorationArea = (val) => {
        this.setState({ center: val })
    };

    handleFormChange = () => {
        setTimeout(() => {
            let { form } = this.props;
            console.log("entrou")
            form.validateFields().then((data) => {
                console.log("val")
                console.log(data)
                console.log({ ...data, protectedArea: this.state.positions })
                this.props.handleFormValidation({ area: { ...data, protectedArea: this.state.positions } });
            });
        }, 1);
    };

    render() {
        const { lang, data } = this.props;
        const { getFieldDecorator } = this.props.form;
        let question = AreaQuestions[lang];
        return (
            <Form hideRequiredMark className="wrap-label">
                <Row gutter={16}>
                    <Col width="100%">
                        <FormItem hasFeedback={false} label={question[0].q}>
                            {getFieldDecorator("typeArea", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.typeArea),
                            })(
                                <Select
                                    onChange={this.handleFormChange}
                                    placeholder={question[0].p}
                                >
                                    {question[0].opt.map((option, index) => (
                                        <Option onClick={() => this.handleRestorationArea(question[0].center[index])} key={index} value={question[0].opt[index]}>
                                            {option}
                                        </Option>
                                    ))}
                                </Select>
                            )}
                        </FormItem>
                    </Col>

                    {/* 
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[1].q}>
                            {getFieldDecorator("protectedArea", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.protectedArea),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[1].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col> */}


                    <Col width="100%">
                        <p>{question[1].q}</p>
                        <CoordinatesForm center={this.state.center} state={this.state} setState={(values) => this.setState(values)} handlePositions={(positions) => this.props.form.setFieldsValue(positions)} />
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[2].q}>
                            {getFieldDecorator("restorationArea", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.restorationArea),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[2].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[3].q}>
                            {getFieldDecorator("distanceShore", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.distanceShore),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[3].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[4].q}>
                            {getFieldDecorator("ownership", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.ownership),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[4].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[5].q}>
                            {getFieldDecorator("adjacentOwnership", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.adjacentOwnership),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[5].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[6].q}>
                            {getFieldDecorator("marineRights", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.marineRights),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[6].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[7].q}>
                            {getFieldDecorator("resultRestrictions", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.resultRestrictions),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[7].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[8].q}>
                            {getFieldDecorator("areaActivities", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.areaActivities),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[8].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[9].q}>
                            {getFieldDecorator("activitiesRestrictions", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.activitiesRestrictions),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[9].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[10].q}>
                            {getFieldDecorator("technologicSolutions", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.technologicSolutions),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[10].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[11].q}>
                            {getFieldDecorator("natureSolutions", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.natureSolutions),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[11].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[12].q}>
                            {getFieldDecorator("climateVulnerability", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.climateVulnerability),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[12].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[13].q}>
                            {getFieldDecorator("conflicts", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.conflicts),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[13].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[14].q}>
                            {getFieldDecorator("contreversialMethods", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.contreversialMethods),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[14].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col xs={24} sm={12}>
                        <FormItem hasFeedback={false} label={question[15].q}>
                            {getFieldDecorator("name", {
                                rules: this.rules.requiredField,
                                initialValue: isArrayNull(data.name),
                            })(
                                <TextArea
                                    onChange={this.handleFormChange}
                                    placeholder={question[15].p}
                                    autosize={{ minRows: 1, maxRows: 4 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                </Row>
            </Form>
        );
    }
}

export default Form.create()(AreaForm);
