export const Descriptions = {
    en: {
        0: "Dear partner,",
        1: "The following survey constitutes a first mapping and prioritisation of the different stakeholders that will be affected or involved in the CLIMAREST restoration activities (c.f. milestone 1 in the application, Part B). Section A has been designed to inform us about the study area and the different activities / interventions that you plan for your case. Sections B to D are designed to identify and prioritise stakeholders within the following three categories: ",
        2: "B) Stakeholders who are formally involved with regulation, administration, management, enforcement, or harvesting of resources within the study area; ",
        3: "C) Stakeholders who are commercial / professional users of the seascape / landscape; and",
        4: "D) Stakeholders who are recreational users of the seascape / landscape.  ",
        5: "For sections B, C and D, we ask that you fill out one form for each stakeholder within each category. For all questions, please answer based on your experience and perception. If you have any doubts, please contact yennie.bredin@nina.no, john.linnell@nina.no, ana.dinis@mare-centre.pt, or paola.parretti@mare-centre.pt. ",
        6: "Thank you for taking the time to fill out this survey. Best wishes, Paola, Ana, John, and Yennie."
    }
}

export const Alerts = {
    en: {
        0: "We thank you for your collaboration!",
        1: "Please increase map's zoom before adding pins in order to increase precision",
        2: "Figure",
    }
}

export const ButtonText = {
    en: {
        0: "Back",
        1: "Next",
        2: "Submit",
    }
}


export const AreaQuestions = {
    en: [
        {
            q: "Which restoration area is this?", p: "",
            opt: ['Madeira', 'Galicia', 'Ireland', 'Brittany', 'Svalbard'],
            center: [[32.6, -16.8], [42.82, -8.02], [53.26, -7.96], [48.21, -2.96], [78.62, 15.93]]
        },
        { q: "Is the location inside a protected area? If yes, name area(s) and identify category(ies) of protection (e.g. Natura 2000, national park, nature reserve etc.) ", p: "" },
        { q: "Describe the restoration area’s size (km2)", p: "" },
        { q: "What is the distance from shore – closest point, furthest point (km)", p: "" },
        { q: "Who has tenure / ownership / management responsibility of the marine space? ", p: "" },
        { q: "Who has tenure / ownership of the adjacent land?", p: "" },
        { q: "Who currently has marine resource access rights (such as permits, quotas, etc.) within the area of intervention?", p: "" },
        { q: "Will the intervention result in restrictions or changes to access and resource use?", p: "" },
        { q: "What other activities (e.g. recreational) are currently conducted within the area of intervention?", p: "" },
        { q: "Will the intervention result in restrictions or changes to these activities? If yes, please explain.", p: "" },
        { q: "Describe the aspects of the intervention that involve technological solutions", p: "" },
        { q: "Describe the aspects of the intervention that involve nature-based solutions", p: "" },
        { q: "How will the restoration decrease climate vulnerability?", p: "" },
        { q: "Have there been any conflicts over resource use, conservation, or restoration at this site before? If yes, please explain.", p: "" },
        { q: "Are any of the methods that you plan to use controversial? In what way/why? If so, which stakeholders have been involved?", p: "" },
        { q: "Your name and institution", p: "" },
    ]
}

export const Informations = {
    en: [
        'Select requested information before advancing',
        'Position the pin on the map according to where your sighting occurred',
        'Please add each place where you remember to have seen the animal ',
        'Add another stakeholder from this category'
    ],
}

export const CoordinatesQuestions = {
    en: [
        ["Last time (green)", "In the last 5 years (red)", "Over 5 years ago (blue)"],
        { q: "Observations", p: "Include all information you consider relevant" },
    ]
}

export const Questions = {
    en: [
        { q: "Type of stakeholder:", p: "" },
        { q: "Name of stakeholder:", p: "" },
        { q: "Role of stakeholder in the intervention area:", p: "" },
        { q: "Type of previous engagements. Please select all that applies", p: "Please select all that applies", opt: ['They have received information', 'They contribute to research or monitoring', 'Actively engaged in restoration activity', 'They have been involved in consultation and discussions', 'There has been active conflict mediation with them', 'None'] },
        { q: "How will the restoration activity affect this stakeholder (mechanism and extent, negative or positive effect)?", p: "" },
        { q: "How will this group affect the restoration activity (mechanism and extent, negative or positive effect)?", p: "" },
        { q: "Rank the level of interest this stakeholder has in the restoration activity", p: "", opt: ['No interest', 'Slight interest', 'Strong interest', 'Not sure'] },
        { q: "Rank the level of influence this stakeholder has for the success of the restoration activity", p: "", opt: ['No interest', 'Slight interest', 'Strong interest', 'Not sure'] },
        { q: "Rank the level of trust between your institution and this stakeholder [we are interested in your relationship with this stakeholder]", p: "", opt: ['Very low', 'Low', 'Medium', 'High', 'Very high'] },
        { q: "To what extent would you suggest we prioritise contact with this stakeholder?", p: "", opt: ['Low priority', 'Medium priority', 'High priority'] },
        { q: "How can we best engage with this stakeholder group? Please select all that applies", p: "Please select all that applies", opt: ['Individual interviews', 'Taking part in focus group interviews (i.e. to learn about their perspectives)', 'Online questionnaires', 'Just send them information', 'Involve them in group workshops to discuss management options (involve them in planning objectives)'] },
    ]
}
