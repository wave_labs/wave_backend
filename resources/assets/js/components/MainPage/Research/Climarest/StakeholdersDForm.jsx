import React, { Component } from "react";
import { Row, Col, Select, Form, Input, Button, Icon } from "antd";
import { Questions, Informations } from "./Translations";
import { isArrayNull } from "./helper";
import styled from "styled-components";

const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const Section = styled(Row)`
  padding: 30px 0px;
  box-sizing: border-box;
  border-bottom: 1px solid #777;
  border-width: ${(props) => (props.hasBorder ? "1px" : "0px")};
`;

class StakeholdersDForm extends Component {
    state = {
        occurrences: 1,
    };
    rules = {
        requiredField: [
            {
                required: true,
                message: "Field is required",
            },
        ],
    };

    componentDidMount = () => {
        this.handleFormChange();
    };

    handleFormChange = () => {
        setTimeout(() => {
            let { form, data } = this.props;

            form.validateFields().then((formData) => {
                formData.stakeholder.C = data.C;
                formData.stakeholder.B = data.B;
                this.props.handleFormValidation(formData);
            });
        }, 1);
    };

    /* handleNewStakeholder = () => {
        this.setState({ occurrences: this.state.occurrences + 1 });
    }; */

    render() {
        const { lang, data, occurrencesD } = this.props;
        const initValue = data.D;
        const { getFieldDecorator } = this.props.form;
        let question = Questions[lang];

        return (
            <Form hideRequiredMark className="wrap-label">
                {[...Array(occurrencesD)].map((_, index) => (
                    <Section
                        hasBorder={index != occurrencesD - 1}
                        key={index}
                        type="flex"
                        align="middle"
                        gutter={32}
                    >
                        <Col xs={24} md={8}>
                            <FormItem hasFeedback={false} label={question[0].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[type]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.type),
                                    }
                                )(
                                    <TextArea
                                        onChange={this.handleFormChange}
                                        placeholder={question[0].p}
                                        autosize={{minRows:1, maxRows:4}}size
                                    />
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={24} md={8}>
                            <FormItem hasFeedback={false} label={question[1].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[name]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.name),
                                    }
                                )(
                                    <TextArea
                                        onChange={this.handleFormChange}
                                        placeholder={question[1].p}
                                        autosize={{minRows:1, maxRows:4}}size
                                    />
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={24} md={8}>
                            <FormItem hasFeedback={false} label={question[2].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[role]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.role),
                                    }
                                )(
                                    <TextArea
                                        onChange={this.handleFormChange}
                                        placeholder={question[2].p}
                                        autosize={{minRows:1, maxRows:4}}size
                                    />
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={24} md={8}>
                            <FormItem label={question[3].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[typeEngagement]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.typeEngagement),
                                    }
                                )(
                                    <Select
                                        mode="multiple"
                                        onChange={this.handleFormChange}
                                        placeholder={question[3].p}
                                    >
                                        {question[3].opt.map((option, i) => (
                                            <Option key={i} value={question[3].opt[i]}>
                                                {option}
                                            </Option>
                                        ))}
                                    </Select>
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={24} md={8}>
                            <FormItem hasFeedback={false} label={question[4].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[affect]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.affectStakeholder),
                                    }
                                )(
                                    <TextArea
                                        onChange={this.handleFormChange}
                                        placeholder={question[4].p}
                                        autosize={{minRows:1, maxRows:4}}size
                                    />
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={24} md={8}>
                            <FormItem hasFeedback={false} label={question[5].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[affectRestoration]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.affectRestoration),
                                    }
                                )(
                                    <TextArea
                                        onChange={this.handleFormChange}
                                        placeholder={question[5].p}
                                        autosize={{minRows:1, maxRows:4}}size
                                    />
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={24} md={8}>
                            <FormItem hasFeedback={false} label={question[6].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[levelInterest]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.levelInterest),
                                    }
                                )(
                                    <Select
                                        onChange={this.handleFormChange}
                                        placeholder={question[6].p}
                                    >
                                        {question[6].opt.map((option, index) => (
                                            <Option key={index} value={question[6].opt[index]}>
                                                {option}
                                            </Option>
                                        ))}
                                    </Select>
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={24} md={8}>
                            <FormItem hasFeedback={false} label={question[7].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[levelInfluence]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.levelInfluence),
                                    }
                                )(
                                    <Select
                                        onChange={this.handleFormChange}
                                        placeholder={question[7].p}
                                    >
                                        {question[7].opt.map((option, index) => (
                                            <Option key={index} value={question[7].opt[index]}>
                                                {option}
                                            </Option>
                                        ))}
                                    </Select>
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={24} md={8}>
                            <FormItem hasFeedback={false} label={question[8].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[levelTrust]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.levelTrust),
                                    }
                                )(
                                    <Select
                                        onChange={this.handleFormChange}
                                        placeholder={question[8].p}
                                    >
                                        {question[8].opt.map((option, index) => (
                                            <Option key={index} value={question[8].opt[index]}>
                                                {option}
                                            </Option>
                                        ))}
                                    </Select>
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={24} md={8}>
                            <FormItem hasFeedback={false} label={question[9].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[priority]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.priority),
                                    }
                                )(
                                    <Select
                                        onChange={this.handleFormChange}
                                        placeholder={question[9].p}
                                    >
                                        {question[9].opt.map((option, index) => (
                                            <Option key={index} value={question[9].opt[index]}>
                                                {option}
                                            </Option>
                                        ))}
                                    </Select>
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={24} md={8}>
                            <FormItem hasFeedback={false} label={question[10].q}>
                                {getFieldDecorator(
                                    "stakeholder[D]" + "[" + index + "]" + "[engage]",
                                    {
                                        rules: this.rules.requiredField,
                                        initialValue: isArrayNull(initValue.engageStakeholder),
                                    }
                                )(
                                    <Select
                                        onChange={this.handleFormChange}
                                        placeholder={question[10].p}
                                        mode="multiple"
                                    >
                                        {question[10].opt.map((option, index) => (
                                            <Option key={index} value={question[10].opt[index]}>
                                                {option}
                                            </Option>
                                        ))}
                                    </Select>
                                )}
                            </FormItem>
                        </Col>
                    </Section>
                ))}
                {/* <Button type="dashed" onClick={this.handleNewStakeholder}>
                    <Icon type="plus" /> {Informations[lang][3]}
                </Button> */}
            </Form>
        );
    }
}

export default Form.create()(StakeholdersDForm);
