import React, { Component } from "react";
import { Row, Col, Select, Form, Input, Radio, Table } from "antd";
import CreatureRemoteSelectContainer from "../../../Dashboard/Data/CreatureContent/CreatureRemoteSelectContainer";
import { GeneralQuestions } from "./Translations";
import styled from "styled-components";
import { isArrayNull } from "./helper";

const StyledTable = styled(Table)`
  .ant-table {
    color: black;
  }
`;

const FormItem = Form.Item;
const Option = Select.Option;
const TextArea = Input.TextArea;

class MarkerGeneralQuestionsForm extends Component {
  state = {
    row1: null,
    row2: null,
    row3: null,
    hasValues: false,
  };

  rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  resetRadio = (col, radioValues, index) => {
    index.forEach((element) => {
      radioValues[element] == col && (radioValues[element] = null);
    });

    return radioValues;
  };

  handleRadioClick = (e) => {
    const { row1, row2, row3 } = this.state;
    let radioValues = [row1, row2, row3];
    var element = this.splitValue(e.target.value);
    var row = element[0];
    var col = element[1];

    switch (row) {
      case "1":
        radioValues[0] = col;
        break;
      case "2":
        radioValues[1] = col;
        break;
      case "3":
        radioValues[2] = col;
        break;
      default:
        break;
    }

    this.setState(
      {
        row1: radioValues[0],
        row2: radioValues[1],
        row3: radioValues[2],
      },
      this.checkRadioFinished
    );
  };

  checkRadioFinished = () => {
    const { row1, row2, row3 } = this.state;

    if (row1 && row2 && row3) {
      this.props.form.setFieldsValue({
        size: { sm: row1, md: row2, lg: row3 },
      });
      this.handleFormChange("size");
    }
  };

  handleFormChange = (element) => {
    this.props.handleFormValidation(element);
  };

  splitValue = (element) => {
    return element.split("/");
  };

  handleCreatureChange = (creatureId) => {
    this.props.handleCreatureChange(creatureId);
  };

  componentDidMount() {
    const { formData } = this.props;

    if (formData.length != 0) {
      this.setState(
        {
          row1: formData.size.lg,
          row2: formData.size.md,
          row3: formData.size.sm,
          hasValues: true,
        },
        this.checkRadioFinished
      );
    }
  }

  render() {
    const { lang, formData } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { row1, row2, row3, hasValues } = this.state;
    const { dimensions } = this.props;
    let question = GeneralQuestions[lang];

    const RadioButton = ({ row, value }) => {
      let splitValue = this.splitValue(value);
      return (
        <Radio
          checked={row == splitValue[1] && true}
          value={value}
          onClick={this.handleRadioClick}
        />
      );
    };

    const dataSource = [
      {
        name: `${question[5].row[0]} ${dimensions.sm} ${dimensions.unit}`,
        frequently: <RadioButton row={row1} value="1/frequently" />,
        rarely: <RadioButton row={row1} value="1/rarely" />,
        never: <RadioButton row={row1} value="1/never" />,
        uncertain: <RadioButton row={row1} value="1/uncertain" />,
      },
      {
        name: `${question[5].row[1]} ${dimensions.sm} e ${dimensions.lg} ${
          dimensions.unit
        }`,
        frequently: <RadioButton row={row2} value="2/frequently" />,
        rarely: <RadioButton row={row2} value="2/rarely" />,
        never: <RadioButton row={row2} value="2/never" />,
        uncertain: <RadioButton row={row2} value="2/uncertain" />,
      },
      {
        name: `${question[5].row[2]} ${dimensions.lg} ${dimensions.unit}`,
        frequently: <RadioButton row={row3} value="3/frequently" />,
        rarely: <RadioButton row={row3} value="3/rarely" />,
        never: <RadioButton row={row3} value="3/never" />,
        uncertain: <RadioButton row={row3} value="3/uncertain" />,
      },
    ];

    const columns = [
      {
        title: "",
        dataIndex: "name",
        align: "center",
      },
      {
        title: question[5].col[0],
        dataIndex: "frequently",
        align: "center",
      },
      {
        title: question[5].col[1],
        dataIndex: "rarely",
        align: "center",
      },
      {
        title: question[5].col[2],
        dataIndex: "never",
        align: "center",
      },
      {
        title: question[5].col[3],
        dataIndex: "uncertain",
        align: "center",
      },
    ];

    return (
      <Form hideRequiredMark={true} className="wrap-label">
        <Row type="flex" align="middle" gutter={32}>
          <Col
            xs={24}
            style={{ display: this.props.random ? "block" : "none" }}
          >
            <FormItem label="Escolha a espécie desejada">
              {getFieldDecorator("creature_id", {
                initialValue: this.props.creatureId,
              })(
                <CreatureRemoteSelectContainer
                  placeholder="Listagem de espécies"
                  onChange={this.handleCreatureChange}
                />
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem label={question[1].q}>
              {getFieldDecorator("form[knowledge]", {
                rules: this.rules.requiredField,
                initialValue: isArrayNull(hasValues, formData, "knowledge"),
              })(
                <Select
                  onChange={() => this.handleFormChange("knowledge")}
                  placeholder={question[1].p}
                >
                  <Option value="yes">{question[1].opt[0]}</Option>
                  <Option value="no">{question[1].opt[1]}</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem label={question[2].q}>
              {getFieldDecorator("form[name_specie]", {
                initialValue: isArrayNull(hasValues, formData, "name_specie"),
              })(<Input placeholder={question[2].p} />)}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem label={question[3].q}>
              {getFieldDecorator("form[date]", {
                rules: this.rules.requiredField,
                initialValue: isArrayNull(hasValues, formData, "date"),
              })(
                <Input
                  onChange={() => this.handleFormChange("date")}
                  placeholder={question[3].p}
                />
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem label={question[4].q}>
              {getFieldDecorator("form[abundance]", {
                rules: this.rules.requiredField,
                initialValue: isArrayNull(hasValues, formData, "abundance"),
              })(
                <Select
                  onChange={() => this.handleFormChange("abundance")}
                  placeholder={question[4].p}
                >
                  <Option value="increased">{question[4].opt[0]}</Option>
                  <Option value="constant">{question[4].opt[1]}</Option>
                  <Option value="decreased">{question[4].opt[2]}</Option>
                  <Option value="not sure">{question[4].opt[3]}</Option>
                </Select>
              )}
            </FormItem>
          </Col>

          <Col xs={24}>
            <FormItem label={question[5].q}>
              {getFieldDecorator("size", {
                rules: this.rules.requiredField,
              })(
                <StyledTable
                  rowKey={(record) => record.name}
                  pagination={false}
                  dataSource={dataSource}
                  columns={columns}
                />
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem label={question[6].q}>
              {getFieldDecorator("form[other_species]", {
                rules: this.rules.requiredField,
                initialValue: isArrayNull(hasValues, formData, "other_species"),
              })(
                <Select
                  onChange={() => this.handleFormChange("other_species")}
                  placeholder={question[6].p}
                >
                  <Option value="yes">{question[6].opt[0]}</Option>
                  <Option value="no">{question[6].opt[1]}</Option>
                  <Option value="not sure">{question[6].opt[2]}</Option>
                </Select>
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem label={question[7].q}>
              {getFieldDecorator("form[name_species]", {
                initialValue: isArrayNull(hasValues, formData, "name_species"),
              })(<Input placeholder={question[7].p} />)}
            </FormItem>
          </Col>

          <Col xs={24}>
            <FormItem label={question[8].q}>
              {getFieldDecorator("form[consequences]", {
                rules: this.rules.requiredField,
                initialValue: isArrayNull(hasValues, formData, "consequences"),
              })(
                <Select
                  onChange={() => this.handleFormChange("consequences")}
                  placeholder={question[8].p}
                >
                  <Option value="yes">{question[8].opt[0]}</Option>
                  <Option value="no">{question[8].opt[1]}</Option>
                  <Option value="not sure">{question[8].opt[2]}</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(MarkerGeneralQuestionsForm);
