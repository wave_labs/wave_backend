import { Tooltip, Modal, Row, Button, Select } from "antd";
import React, { Component } from "react";
import MapBoxMap from "../../../Dashboard/Common/MapBoxMap";
import { QuestionCircleFilled } from "@ant-design/icons";
import styled from "styled-components";
import { Marker } from "react-map-gl";
import MarkerGeneralQuestionsForm from "./MarkerGeneralQuestionsForm";
import MarkerActivityForm from "./MarkerActivityForm";
import { connect } from "react-redux";
import { createMarker } from "redux-modules/marker/actions";
import { alertActions } from "redux-modules/alert/actions";
import { Descriptions, Informations, Alerts } from "./Translations";

import { getErrorMessages, ICON } from "helpers";
import LanguageContainer from "./LanguageContainer";

const Option = Select.Option;

const InfoContainer = styled.div`
  margin: 50px auto;
  margin-bottom: 30px;
  h1,
  p {
    text-align: center;
  }

  p {
    font-size: 1.2em;
    width: 80%;
    display: block;
    margin: auto;
  }
`;

const CreatureImageContainer = styled.div`
  width: 80%;
  margin: auto;
  position: relative;
`;

const StyledCreatureImage = styled.div`
  display: block;
  position: relative;
  width: 350px;
  height: 350px;
  margin: 0px auto 30px auto;
  padding: 40px;
  border-radius: 50%;
  background-color: #eeeeee;
  background-image: url(${(props) => props.background});
  background-size: 100% auto;
  background-repeat: no-repeat;
  background-position: center;

  &:before {
    display: block;
    padding-top: 100%;
  }
`;

const StyledQuestionCircleFilled = styled(QuestionCircleFilled)`
  position: absolute;
  top: 10px;
  right: 10px;
`;

const StyledImage = styled.img`
  width: 80px;
  cursor: pointer;
  opacity: ${(props) => (props.active ? 1 : 0.3)};
  display: block;
  margin: auto;
`;

const ActivityPickerTitle = styled.div`
  font-size: 1.1em;
  text-align: center;
  text-transform: capitalize;
`;

const FormContainer = styled.div`
  display: block;
  width: 80%;
  margin: 50px auto;

  @media (max-width: 768px) {
    width: 100%;
  }

  .ant-form label {
    color: black !important;
  }
`;

const ActivityPickerContainer = styled.div`
  width: 20%;
  min-width: 150px;
  margin: 20px auto;
`;

const ActivityPicker = ({ title, children }) => {
  return (
    <ActivityPickerContainer>
      <div>
        {children}
        <ActivityPickerTitle>{title}</ActivityPickerTitle>
      </div>
    </ActivityPickerContainer>
  );
};

class MarkerContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      markers: {}, //markers positions for activities
      formFields: [], //filled fields on first page
      formData: [], //filled fields on first page
      currentStep: 0, // current modal page
      activeOnClick: true,
      formValidated: false, //form validation for first page
      currentActive: 1, //current activities form
      requiredFields: {}, //forms required for activities
      submitReady: false, //at least one marker is required
      lang: "pt",
    };

    // preserve the initial state in a new object
    this.baseState = this.state;
  }

  componentDidMount() {
    let { creatureId } = this.props;

    this.setState({
      creatureId: creatureId,
    });

    this.props.handleCreatureData(creatureId, {
      language: this.state.lang,
    });
  }

  componentDidUpdate(prevProps) {
    let { creatureId, visible, creatureActivities } = this.props;
    let { markers, requiredFields } = this.state;

    if (visible && prevProps != this.props) {
      creatureActivities.map((activity) => {
        markers[activity.id] = [];
        requiredFields[activity.id] = false;
      });
      this.setState({
        creatureId: creatureId,
        markers,
        requiredFields,
      });
    }
  }

  handleDragEnd = (event, index, category) => {
    let { markers } = this.state;

    markers[category][index] = [event.lngLat[1], event.lngLat[0]];

    setTimeout(() => {
      this.setState(
        {
          markers,
          activeOnClick: true,
        },
        this.handleFormData
      );
    }, 1);
  };

  resetState = () => {
    this.setState({
      activeOnClick: true,
      markers: [[], [], []],
    });
  };

  handleDragStart = () => {
    this.setState({ activeOnClick: false });
  };

  handleMarkerClick = (category, index, e) => {
    if (!e.leftButton) {
      let { markers } = this.state;
      markers[category].splice(index, 1); //Remove 1 record at index
      this.setState({ markers });
    }
  };

  handleMapClick = (e) => {
    if (e.leftButton) {
      let {
        markers,
        activeOnClick,
        currentActive,
        requiredFields,
      } = this.state;
      if (activeOnClick) {
        requiredFields[currentActive] = true;
        markers[currentActive].push([e.lngLat[1], e.lngLat[0]]);
        this.setState(
          { markers, requiredFields, submitReady: true },
          this.handleFormData
        );
      }
    }
  };

  next = () => {
    if (this.state.formValidated) {
      const form = this.formRef.props.form;
      form.validateFields((err, data) => {
        if (err) {
          this.setState({ formValidated: false });
          return;
        } else {
          if (data.form.knowledge == "yes") {
            const currentStep = this.state.currentStep + 1;
            this.setState({ formData: data, currentStep });
          } else {
            const currentStep = this.state.currentStep + 2;
            this.setState({ currentStep });
          }

          document.getElementById("marker-modal").scrollIntoView();
        }
      });
    }
  };

  prev = () => {
    const currentStep = this.state.currentStep - 1;
    this.setState({ currentStep, formValidated: false });
    this.resetState();
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  saveActivityFormRef = (activityFormRef) => {
    this.activityFormRef = activityFormRef;
  };

  handleFormValidation = (element) => {
    this.setState({ formValidated: false });

    var { formFields } = this.state;

    if (formFields.indexOf(element) === -1) {
      formFields.push(element);
      this.setState({ formFields });
    }

    if (formFields.length == 6) {
      setTimeout(() => {
        const form = this.formRef.props.form;
        form.validateFields((err, data) => {
          if (err) {
            this.setState({ formValidated: false });
            return;
          } else {
            this.setState({ formValidated: true });
          }
        });
      }, 1);
    }
  };

  handleFormData = () => {
    setTimeout(() => {
      let { creatureActivities } = this.props;
      let { markers, formData } = this.state;
      var data = this.formRef.props.form.getFieldsValue();
      let additionalData = { coordinates: {}, activities: {} };

      creatureActivities.map((activity) => {
        if (markers[activity.id].length > 0) {
          additionalData.coordinates[activity.id] = markers[activity.id];
          additionalData.activities[activity.id] = data[activity.id];
        }
      });

      this.setState({ formData: { ...formData, ...additionalData } });
    }, 1);
  };

  handleSubmit = () => {
    let { formData } = this.state;

    const form = this.formRef.props.form;

    form.validateFieldsAndScroll((err) => {
      if (err) {
        Object.keys(err).map((element) => {
          this.setState({
            currentActive: element,
          });
        });
        return;
      } else {
        this.props
          .createMarker(formData)
          .then(() => {
            const currentStep = this.state.currentStep + 1;
            this.setState({ currentStep });
          })
          .catch((error) => {
            let messages = getErrorMessages(error.response.data.errors);

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  handleFilterChange = (e) => {
    this.setState({
      currentActive: e,
    });
  };

  handleCreatureChange = (creatureId) => {
    this.setState({ creatureId });
    this.props.handleCreatureData(creatureId, {
      language: this.state.lang,
    });
  };

  handleModalClose = () => {
    // reset to initial state
    this.setState(this.baseState);

    this.props.handleModalClose();
  };

  getMarkerColor = (marker) => {
    let { creatureActivities } = this.props;
    var activity = creatureActivities.filter((activity) => {
      return activity.id == marker;
    });

    return activity[0].color;
  };

  handleLanguageChange = (e) => {
    this.setState({
      lang: e,
    });

    this.props.handleCreatureData(this.state.creatureId, {
      language: e,
    });
  };

  render() {
    var {
      markers,
      currentStep,
      formValidated,
      currentActive,
      requiredFields,
      creatureId,
      submitReady,
      lang,
      formData,
    } = this.state;
    var { creatureActivities } = this.props;

    const steps = [
      {
        key: 0,
        title: "Taxa",
        content: (
          <InfoContainer>
            <CreatureImageContainer>
              <StyledCreatureImage
                background={
                  creatureId
                    ? `/api/image/creature/${creatureId}`
                    : "/images/studies/markers"
                }
              />
              <LanguageContainer
                onChange={this.handleLanguageChange}
                lang={lang}
              />
            </CreatureImageContainer>

            <h1>
              {lang == "en" ? "Have you seen me lately?" : "Já me encontraste?"}
            </h1>

            <p>{Descriptions[lang][0]}</p>
            <br />
            <p>{Alerts[lang][0]}</p>
            <FormContainer>
              <MarkerGeneralQuestionsForm
                lang={lang}
                dimensions={this.props.creatureDimensions}
                random={this.props.random}
                creatureId={creatureId}
                handleCreatureChange={this.handleCreatureChange}
                handleFormValidation={this.handleFormValidation}
                wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                formData={this.state.formData}
              />
            </FormContainer>
          </InfoContainer>
        ),
      },
      {
        key: 1,
        title: "Picker",
        content: (
          <FormContainer>
            <Row
              type="flex"
              justify="space-between"
              align="middle"
              style={{ margin: "30 0" }}
            >
              <InfoContainer>
                <h1>Spot It!</h1>

                <p>{Descriptions[lang][1]}</p>
                <p>{Informations[lang][2]}</p>
              </InfoContainer>
              {creatureActivities.map((element, index) => {
                return (
                  <ActivityPicker
                    key={element.id}
                    title={element.translatedName}
                  >
                    <StyledImage
                      active={currentActive == element.id && true}
                      src={element.image}
                      alt={element.name}
                      onClick={() => this.handleFilterChange(element.id)}
                    />
                  </ActivityPicker>
                );
              })}
            </Row>
            <InfoContainer>
              <div style={{ marginTop: "50px" }}>
                <MarkerActivityForm
                  lang={lang}
                  creatureActivities={this.props.creatureActivities}
                  requiredFields={requiredFields}
                  handleFormData={this.handleFormData}
                  currentActive={currentActive}
                  wrappedComponentRef={(formRef) => (this.formRef = formRef)}
                />
              </div>
            </InfoContainer>
            <p style={{ textAlign: "center" }}>{Informations[lang][3]}</p>
            <MapBoxMap
              fixedSize={true}
              size="500"
              minZoom={8}
              zoom={8}
              onClick={this.handleMapClick}
            >
              {Object.entries(markers).map((element, index) => {
                {
                  return element[1].map((e, i) => {
                    return (
                      <Marker
                        key={i}
                        latitude={e[0]}
                        longitude={e[1]}
                        draggable
                        onDragEnd={(event) =>
                          this.handleDragEnd(event, i, element[0])
                        }
                        onDragStart={this.handleDragStart}
                      >
                        <svg
                          onContextMenu={(action) =>
                            this.handleMarkerClick(element[0], i, action)
                          }
                          height="25"
                          viewBox="0 0 24 24"
                          style={{
                            fill: this.getMarkerColor(element[0]),
                            stroke: "none",
                          }}
                        >
                          <path d={ICON} />
                        </svg>
                      </Marker>
                    );
                  });
                }
              })}

              <Tooltip
                placement="topLeft"
                title={
                  <div>
                    <p>{Informations[lang][0][0]}</p>
                    <p>{Informations[lang][0][1]}</p>
                    <p>{Informations[lang][0][2]}</p>
                  </div>
                }
              >
                <StyledQuestionCircleFilled style={{ fontSize: "25px" }} />
              </Tooltip>
            </MapBoxMap>
          </FormContainer>
        ),
      },
      {
        key: 3,
        title: "Thanks",
        content: (
          <InfoContainer>
            <h1>{lang == "en" ? "Thank you" : "Muito Obrigado(a)"}</h1>

            <p>{Descriptions[lang][2]}</p>
          </InfoContainer>
        ),
      },
    ];

    return (
      <Modal
        onCancel={this.handleModalClose}
        centered
        visible={this.props.visible}
        closable
        style={{ top: "80px", color: "black" }}
        width={960}
        footer={[
          currentStep < 1 ? (
            <Row key={0} type="flex" justify="center">
              <Tooltip title={Informations[lang][1]}>
                <Button
                  disabled={!formValidated}
                  size="large"
                  key="submit"
                  type="primary"
                  onClick={this.next}
                >
                  Next
                </Button>
              </Tooltip>
            </Row>
          ) : currentStep < steps.length - 1 ? (
            <Row key={1} type="flex" justify="center" gutter={16}>
              <Button size="large" onClick={this.prev}>
                Back
              </Button>
              <Button
                disabled={!submitReady}
                size="large"
                key="submit"
                type="primary"
                onClick={this.handleSubmit}
              >
                Submit
              </Button>
            </Row>
          ) : (
            <Row key={2} type="flex" justify="center" gutter={16}>
              <Button
                size="large"
                key="submit"
                type="primary"
                onClick={this.handleModalClose}
              >
                Close
              </Button>
            </Row>
          ),
        ]}
      >
        <div id="marker-modal">{steps[currentStep].content}</div>
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createMarker: (data) => dispatch(createMarker(data)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
    successAlert: (data) => dispatch(alertActions.success(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    creatureDimensions: state.creature.dimensions,
    creatureActivities: state.creature.activities,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MarkerContent);
