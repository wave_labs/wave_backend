export const Descriptions = {
    pt: {
        0: "No âmbito das atividades de investigação o MARE-Madeira está a fazer uma avaliação sobre a presença e distribuição deste peixe nas águas costeiras do Arquipélago da Madeira. Assim pedimos a vossa colaboração com o preenchimento deste inquérito (aproximadamente 5 minutos) para nos ajudar a mapear e avaliar a chegada e distribuição deste peixe na região. O inquérito é de resposta voluntária, anónimo e os dados serão analisados de forma global.",
        1: "Selecione uma atividade na qual avistou o peixe.",
        2: "Este inquérito enquadra-se nas atividades de investigação do MARE-Madeira/ARDITI. Esta iniciativa é co-financiada pelo projeto estratégico do MARE UI&D (UIDB/04292/2020) financiados pela Fundação para a Ciência e Tecnologia e MIMAR+ (MAC2/4.6.d/249) INTERREG MAC 2014-2020.",
    },
    en: {
        0: "Within the scope of scientific activities and research, MARE-Madeira is assessing the presence and distribution of this fish in the coastal waters of the Madeira Archipelago. We kindly ask your collaboration by filling this survey (approximately 5 minutes) to help us mapping and tracking the arrival and distribution of this fish in the region.The survey is voluntary, anonymous and collected data will be analyzed at global level.",
        1: "Please, select an activity in which you detected this fish.",
        2: "This survey is conducted within the scope of research activities at MARE-Madeira/ARDITI. This initiative is co-funded by MARE UI&D (UIDB/04292/2020) project, Fundação para a Ciência e Tecnologia e MIMAR+ (MAC2/4.6.d/249) INTERREG MAC 2014-2020",
    }
}

export const Alerts = {
    pt: {
        0: "Nota: caso já tenha respondido a este inquérito certifique-se que não repete dados de avistamentos ou informação de distribuição.",
    },
    en: {
        0: "NB: in case you have already answered, please make sure you do not repeat data entries",
    }
}


export const ActivityQuestions = {
    pt: {
        1: { q: "Há quantos anos é que pratica esta atividade? *", p: "Anos de experiência", opt: ['1 ano', '1-5 anos', '5-10 anos', 'Mais de 10 anos'] },
        2: { q: "Com que frequência costuma fazer esta atividade? *", p: "Frequência da atividade", opt: ['1 vez por mês', '2 a 3 vezes por mês', '1 vez por semana', '2 a 5 vezes por semana'] },
        3: { q: "Em média quanto tempo costuma passar de cada vez que pratica esta atividade? *", p: "Tempo por atividade", opt: ['1h por vez', '1-3h por vez', 'Mais de 3h por vez'] },
    },
    en: {
        1: { q: "Have you practiced this activity since when? *", p: "Years of experience", opt: ['1 year', '1-5 years', '5-10 years', 'Over 10 years'] },
        2: { q: "How often do you do this activity? *", p: "Frequency of activity", opt: ['1 time per month', '2-3 times per month', '1 time per week', '2-5 times per week'] },
        3: { q: "On average, how much time do you spend doing this activity?*", p: "Time per activity", opt: ['1hour each time', '1-3hour(s) each time', 'Over 3hour(s) each time'] },
    }
}

export const Informations = {
    pt: [
        ['Clique no lado esquerdo do rato para criar um novo ponto', 'Arraste para mover o ponto', 'Clique no lado direto do rato para remover um ponto'],
        'Preencha todos os campos necessários antes de avançar',
        'Se pratica mais que uma atividade na qual detetou este peixe, por favor repita cada passo respetivamente',
        'Por favor, posicione o(s) ponto(s) no mapa. Para remover um ponto clique no lado direito do rato.'
    ],
    en: [
        ['Left click to create a new pin', 'Drag to move the pin', 'Right click to delete the pin'],
        'Select requested information before advancing',
        'If you practice more than one activity in which you have detected this fish, please repeat each step accordingly',
        'Please, position the pin(s) on the map.(Left click). To remove a pin right click.'
    ],
}

export const GeneralQuestions = {
    pt: {
        1: { q: "Conhece e consegue identificar bem este peixe?", p: "Sim / Não", opt: ['Sim', 'Não'] },
        2: { q: "Se sim, pode escrever o seu nome?", p: "Introduza o nome da espécie" },
        3: { q: "Lembra-se quando o viu pela primeira vez (ano e mês)? *", p: "Janeiro 2018 / 2020" },
        4: { q: "Na sua opinião considera que nos últimos 10 anos, os avistamentos e/ou capturas deste peixe nas águas do arquipélago têm *", p: "Aumentaram / Constantes / Diminuíram / Não sei", opt: ['Aumentaram', 'Manteram-se constantes', 'Diminuíram', 'Não sei'] },
        5: { q: "Por favor selecione com que frequência viu ou capturou peixe(s) com os seguintes tamanhos: *", row: ['Menos de', 'Entre', 'Mais de'], col: ['Frequentemente', 'Raramente', 'Nunca', 'Não sei'] },
        6: { q: "Considera que nos últimos 5 anos, apareceram outras espécies de peixes nas águas da Madeira? *", p: "Sim / Não / Não sei", opt: ['Sim', 'Não', 'Não sei'] },
        7: { q: "Se sim, consegue identificar as espécies e escrever o nome?", p: "Nome das espécies separados por vírgulas" },
        8: { q: "Uma consequência das alterações climáticas é o aquecimento da água dos oceanos. O aumento da temperatura da água pode permitir a chegada e estabelecimento de espécies marinhas residentes em águas tipicamente mais quentes. Considera isto uma ameaça para a biodiversidade marinha local? *", p: "Sim / Não / Não sei", opt: ['Sim', 'Não', 'Não sei'] },
    },
    en: {
        1: { q: "Are you able to identify this fish?*", p: "Yes / No", opt: ['Yes', 'No'] },
        2: { q: "If yes, may you please write his name? ", p: "Name the fish" },
        3: { q: "Do you remember when (month and year) you first saw this fish? *", p: "January 2018 / 2020" },
        4: { q: "In the last 10 years, how do you consider sightings and/or captures of this fish? *", p: "Increased / Constant / Decreased / I do not know", opt: ['Increased', 'Kept constant', 'Decreased', 'I do not know'] },
        5: { q: "Please select how frequently you have seen or captured fish(es) of the following sizes: *", row: ['Less than', 'Between', 'Over'], col: ['Frequently', 'Rarely', 'Never', 'I do not know'] },
        6: { q: "Considering the last 5 years, do you think that other fish species have appeared in the archipelagos of Madeira? *", p: "Yes / No / I do not know", opt: ['Yes', 'No', 'I do not know'] },
        7: { q: "If yes, can you identify them and write their name(s)?", p: "Name of the species split by commas" },
        8: { q: "One consequence of climate change is the increase of sea water temperature. Higher water temperatures may allow the arrival and establishment of marine species from other typically warmer regions. Do you think that this can be a threat for marine biodiversity? *", p: "Yes / No / Not sure", opt: ['Yes', 'No', 'Not sure'] },
    }
}