import React, { Component } from "react";
import { Row, Col, Select, Form } from "antd";
import styled from "styled-components";
import { ActivityQuestions } from "./Translations";

const Option = Select.Option;
const FormItem = Form.Item;

const FormContainer = styled.div`
  display: ${(props) => (props.active ? "block" : "none")};
`;

class MarkerActivityForm extends Component {
  render() {
    const { lang } = this.props;
    let question = ActivityQuestions[lang];

    const { currentActive, requiredFields } = this.props;
    const { getFieldDecorator } = this.props.form;

    var rules = {
      1: [
        {
          required: requiredFields[1],
          message: "Required",
        },
      ],
      2: [
        {
          required: requiredFields[2],
          message: "Required",
        },
      ],
      3: [
        {
          required: requiredFields[3],
          message: "Required",
        },
      ],
    };

    return (
      <Form hideRequiredMark={true} className="wrap-label">
        <Row gutter={32}>
          {Object.keys(requiredFields).map((activity) => {
            return (
              <FormContainer
                key={activity}
                active={currentActive == activity && true}
              >
                <Col xs={24} sm={12}>
                  <FormItem label={question[1].q}>
                    {getFieldDecorator(`${activity}[0]`, {
                      rules: rules[activity],
                    })(
                      <Select
                        onChange={this.props.handleFormData}
                        placeholder={question[1].p}
                      >
                        <Option value={ActivityQuestions["en"][1].opt[0]}>
                          {question[1].opt[0]}
                        </Option>
                        <Option value={ActivityQuestions["en"][1].opt[1]}>
                          {question[1].opt[1]}
                        </Option>
                        <Option value={ActivityQuestions["en"][1].opt[2]}>
                          {question[1].opt[2]}
                        </Option>
                        <Option value={ActivityQuestions["en"][1].opt[3]}>
                          {question[1].opt[3]}
                        </Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col xs={24} sm={12}>
                  <FormItem label={question[2].q}>
                    {getFieldDecorator(`${activity}[1]`, {
                      rules: rules[activity],
                    })(
                      <Select
                        placeholder={question[2].p}
                        onChange={this.props.handleFormData}
                      >
                        <Option value={ActivityQuestions["en"][2].opt[0]}>
                          {question[2].opt[0]}
                        </Option>
                        <Option value={ActivityQuestions["en"][2].opt[1]}>
                          {question[2].opt[1]}
                        </Option>
                        <Option value={ActivityQuestions["en"][2].opt[2]}>
                          {question[2].opt[2]}
                        </Option>
                        <Option value={ActivityQuestions["en"][2].opt[3]}>
                          {question[2].opt[3]}
                        </Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
                <Col xs={24} sm={24}>
                  <FormItem label={question[3].q}>
                    {getFieldDecorator(`${activity}[2]`, {
                      rules: rules[activity],
                    })(
                      <Select
                        placeholder={question[3].p}
                        onChange={this.props.handleFormData}
                      >
                        <Option value={ActivityQuestions["en"][3].opt[0]}>
                          {question[3].opt[0]}
                        </Option>
                        <Option value={ActivityQuestions["en"][3].opt[1]}>
                          {question[3].opt[1]}
                        </Option>
                        <Option value={ActivityQuestions["en"][3].opt[2]}>
                          {question[3].opt[2]}
                        </Option>
                      </Select>
                    )}
                  </FormItem>
                </Col>
              </FormContainer>
            );
          })}
        </Row>
      </Form>
    );
  }
}

export default Form.create()(MarkerActivityForm);
