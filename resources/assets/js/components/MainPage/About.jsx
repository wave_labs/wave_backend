import React, { Fragment } from "react";
import { Row, Col } from "antd";
import styled from "styled-components";
import { StyledTitle, dimensions } from "helpers";

const SubTitle = styled.h2`
  font-size: 1.5rem;
  margin-top: 3%;
`;

const TextContainer = styled.div`
  width: 80%;
  margin: auto;

  @media screen and (max-width: ${dimensions.lg}) {
    width: 96%;
  }
`;

const Description = styled.div`
  font-size: 1rem;
  color: #777777;
`;

const StyledPartner = styled.a`
  display: inline;
  width: 30%;
`;

const StyledImage = styled.img`
  width: 100%;
  max-width: 130px;
  min-width: 80px;
  height: auto;
  display: block;
  margin: auto;
`;

const SectionContainer = styled(Row)`
  margin: 5%;
`;

const Partner = ({ url, src }) => {
  return (
    <StyledPartner href={url} target="_blank">
      <StyledImage src={`/images/partners/${src}`} alt={src} />
    </StyledPartner>
  );
};

const AboutSection = ({ title, description, src, reverse }) => {
  return (
    <SectionContainer type="flex" align="middle" gutter={48}>
      <Col xs={24} lg={{ span: 10, order: reverse ? 2 : 1 }}>
        <img src={src} width="100%" />
      </Col>
      <Col xs={24} lg={{ span: 14, order: reverse ? 1 : 2 }}>
        <SubTitle>{title}</SubTitle>
        <Description>{description}</Description>
      </Col>
    </SectionContainer>
  );
};

const About = () => {
  return (
    <div>
      <TextContainer>
        <StyledTitle>About</StyledTitle>

        <AboutSection
          src="/images/about1.jpg"
          title="Problem"
          description={
            <Fragment>
              <p>
                People and wildlife are at serious risks, unless urgent action
                is taken to mitigate and hopefully reverse the loss of plants
                and animals on which our ecosystems depends for food, clean
                water and a stable climate.
              </p>
              <p>
                Oceans are a crucial driver of Earth ecosystems. They cover
                three-quarters of the planet's surface, contain 97% of the
                Earth's water and absorb 30% of the carbon emissions acting as a
                buffer for global warming.
              </p>
            </Fragment>
          }
        />

        <AboutSection
          reverse
          src="/images/about2.png"
          title="Consequences"
          description={
            <Fragment>
              <p>
                Careful management of this essential global resource is a key
                feature of a sustainable future recognized by the United Nations
                (UN) Sustainable Development Goal (SDG) 14 - Life Below Water,
                targeting the conservation and sustainable use of oceans, seas
                and marine resources.
              </p>
            </Fragment>
          }
        />

        <AboutSection
          src="/wave.png"
          title="Wave Project"
          description={
            <Fragment>
              <p>
                Wave is an interactive dashboard which directly contributes to
                this goal by understanding the impact of human presence on
                marine environment.
              </p>
              <p>
                It provides an insight to recent cetacean sightings, underwater
                taxa, marine litter, sea vessels, including the acoustics and
                footage of marine flora and fauna near Madeira island.
              </p>
              <p>
                It uses latest cutting edge algorithms and machine learning for
                estimating the location as well as passive acoustic monitoring,
                ubiquitous computing, long-range radio communication and
                Internet of Things.
              </p>
            </Fragment>
          }
        />
      </TextContainer>

      <SectionContainer>
        <StyledTitle>Partners</StyledTitle>

        <Row type="flex" justify="space-around" align="middle">
          <Partner url="https://iti.larsys.pt/" src="iti" />
          <Partner url="https://tigerwhale.com/" src="tigerwhale" />
          <Partner url="http://larsys.pt/" src="larsys" />
          <Partner url="https://www.uma.pt/" src="uma" />
          <Partner url="https://tecnico.ulisboa.pt" src="tecnico" />
          <Partner url="http://www.mare-centre.pt/" src="mare" />
          <Partner url="https://oom.arditi.pt/" src="oom" />
          <Partner url="https://www.arditi.pt/" src="arditi" />
        </Row>
      </SectionContainer>
      <SectionContainer>
        <StyledTitle>Supported by</StyledTitle>

        <Row type="flex" justify="space-around" align="middle">
          <Partner url="https://www.fct.pt/" src="fct" />
          <Partner url="http://www.mac-interreg.org/index.jsp" src="mac" />
          <Partner url="http://www.idr.gov-madeira.pt/m1420/" src="m2020" />
          <Partner url="http://www.venturadomar.com/" src="ventura" />
          <Partner url="https://www.azuldiving.com/" src="azuldiving" />
          <Partner url="https://madeiradivepoint.com/" src="madeiradive" />
          <Partner url="https://www.atlanticarea.eu/" src="cleanatlantic" />
          <Partner url="https://intertagua.eu/" src="intertagua" />
          <Partner url="https://www.atlanticarea.eu/" src="interreg" />
        </Row>
      </SectionContainer>
    </div>
  );
};

export default About;
