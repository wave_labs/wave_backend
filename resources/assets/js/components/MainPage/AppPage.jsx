import React, { Component } from "react";
import { Row, Col } from "antd";
import styled from "styled-components";
import { StyledTitle, DomeContent } from "helpers";

const StyledTitleDescription = styled.div`
  font-size: 22px;
  color: #777777;
  text-align: center;
  margin-bottom: 50px;
`;

const AdaptedTitle = styled(StyledTitle)`
  margin-top: 3%;
  margin-bottom: 0;
`;

const FrameContainer = styled(Col)`
  margin-bottom: 65px;
  margin-top: 50px;

  @media screen and (max-width: 500px) {
    width: 70%;
  }
`;

const StyledIframe = styled.iframe`
  width: 100%;
  height: 90%;
  border: none;
`;

const StyledFrameTitle = styled.h1`
  margin-top: 3%;
  margin-bottom: 2%;
  text-align: center;
  font-weight: 500;
  font-size: 1.4rem;

  @media screen and (max-width: 800px) {
    font-size: 1.2rem;
  }
`;

const StyledFrameSubTitle = styled.p`
  text-align: center;
  font-size: 1.1rem;
  color: #777777;
  margin: 0 auto;

  @media screen and (max-width: 800px) {
    font-size: 0.9rem;
  }
`;

const StyledDescription = styled.div`
  font-size: 1.2rem;
  color: black;
  display: block;
  text-align: ${(props) => props.align};

  @media screen and (max-width: 1000px) {
    text-align: center;
  }
`;

const Container = styled.div`
  width: 100%;
`;

const ContentContainer = styled.div`
  width: 95%;
  display: block;
  margin: auto;
`;

const StyledRow = styled(Row)`
  margin: 1% 3%;
`;

class AppPage extends Component {
  render() {
    return (
      <Container
        onClick={function() {
          this.handleFrame();
        }}
      >
        <ContentContainer>
          <AdaptedTitle name="domes" id="domes">
            Domes
          </AdaptedTitle>
          <StyledTitleDescription>
            Interact with the data in immersive environments
          </StyledTitleDescription>
          <StyledRow gutter={8} type="flex" justify="center">
            {DomeContent.map((element) => {
              return (
                <FrameContainer md={24} lg={8}>
                  <StyledIframe
                    src={element.url}
                    scrolling="off"
                    name="iframe-scene"
                  >
                    <p>Your browser does not support iframes.</p>
                  </StyledIframe>

                  <StyledFrameTitle>{element.title}</StyledFrameTitle>
                  <StyledFrameSubTitle>{element.subtitle}</StyledFrameSubTitle>
                </FrameContainer>
              );
            })}
          </StyledRow>
        </ContentContainer>
      </Container>
    );
  }
}

export default AppPage;
