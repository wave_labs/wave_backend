import React from "react";
import { Modal } from "antd";

let ModalImage = ({ src, visible, handleModal }) => {
  return (
    <Modal
      width="720px"
      centered
      visible={visible}
      onCancel={() => handleModal()}
      footer={null}
      closable={false}
      bodyStyle={{ padding: 0 }}
    >
      <img src={src} style={{ width: "100%" }} />
    </Modal>
  );
};

export default ModalImage;
