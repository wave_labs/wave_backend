import React from "react";
import { Empty, Row } from "antd";

let NoDataContainer = ({ text = null, hasData, children }) => {
  return (
    <div>
      {hasData ? (
        children
      ) : (
        <Row
          type="flex"
          justify="center"
          align="middle"
          style={{ width: "100%", height: "100%", minHeight: "200px" }}
        >
          <Empty description={text} />
        </Row>
      )}
    </div>
  );
};

export default NoDataContainer;
