import React from "react";
import styled, { keyframes } from "styled-components";

const load = keyframes`
    0% {
      opacity: 0;
    }

    90% {
      opacity: 0;
    }

    91% {
      opacity: 1;
    }

    100% {
      opacity: 1;
    }
`;

const LoadingComponent = styled.img`
  width: 200px;
  margin: auto;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  opacity: 0;
  will-change: opacity;
  animation: 1.1s infinite ${load};
  animation-delay: ${(props) => props.delay};
`;
const Container = styled.div`
  width: 100%;
  height: 300px;
  position: relative;
`;

const LoadingLogo = () => {
  return (
    <Container>
      <LoadingComponent src="/resources/cm0.png" delay="1.0s" alt="loading" />
      <LoadingComponent src="/resources/cm1.png" delay=".9s" alt="loading" />
      <LoadingComponent src="/resources/cm2.png" delay=".8s" alt="loading" />
      <LoadingComponent src="/resources/cm3.png" delay=".7s" alt="loading" />
      <LoadingComponent src="/resources/cm4.png" delay=".6s" alt="loading" />
      <LoadingComponent src="/resources/cm5.png" delay=".5s" alt="loading" />
      <LoadingComponent src="/resources/cm4.png" delay=".4s" alt="loading" />
      <LoadingComponent src="/resources/cm3.png" delay=".3s" alt="loading" />
      <LoadingComponent src="/resources/cm2.png" delay=".2s" alt="loading" />
      <LoadingComponent src="/resources/cm1.png" delay=".1s" alt="loading" />
      <LoadingComponent src="/resources/cm0.png" delay="0s" alt="loading" />

      <LoadingComponent src="/resources/m0.png" delay="1.10s" alt="loading" />
      <LoadingComponent src="/resources/m1.png" delay="1.00s" alt="loading" />
      <LoadingComponent src="/resources/m2.png" delay=".90s" alt="loading" />
      <LoadingComponent src="/resources/m3.png" delay=".80s" alt="loading" />
      <LoadingComponent src="/resources/m4.png" delay=".70s" alt="loading" />
      <LoadingComponent src="/resources/m5.png" delay=".60s" alt="loading" />
      <LoadingComponent src="/resources/m4.png" delay=".50s" alt="loading" />
      <LoadingComponent src="/resources/m3.png" delay=".40s" alt="loading" />
      <LoadingComponent src="/resources/m2.png" delay=".30s" alt="loading" />
      <LoadingComponent src="/resources/m1.png" delay=".20s" alt="loading" />
      <LoadingComponent src="/resources/m0.png" delay=".10s" alt="loading" />

      <LoadingComponent src="/resources/mw0.png" delay="1.2s" alt="loading" />
      <LoadingComponent src="/resources/mw1.png" delay="1.1s" alt="loading" />
      <LoadingComponent src="/resources/mw2.png" delay="1.0s" alt="loading" />
      <LoadingComponent src="/resources/mw3.png" delay=".9s" alt="loading" />
      <LoadingComponent src="/resources/mw4.png" delay=".8s" alt="loading" />
      <LoadingComponent src="/resources/mw5.png" delay=".7s" alt="loading" />
      <LoadingComponent src="/resources/mw4.png" delay=".6s" alt="loading" />
      <LoadingComponent src="/resources/mw3.png" delay=".5s" alt="loading" />
      <LoadingComponent src="/resources/mw2.png" delay=".4s" alt="loading" />
      <LoadingComponent src="/resources/mw1.png" delay=".3s" alt="loading" />
      <LoadingComponent src="/resources/mw0.png" delay=".2s" alt="loading" />

      <LoadingComponent src="/resources/w0.png" delay="1.3s" alt="loading" />
      <LoadingComponent src="/resources/w1.png" delay="1.2s" alt="loading" />
      <LoadingComponent src="/resources/w2.png" delay="1.1s" alt="loading" />
      <LoadingComponent src="/resources/w3.png" delay="1.0s" alt="loading" />
      <LoadingComponent src="/resources/w4.png" delay=".9s" alt="loading" />
      <LoadingComponent src="/resources/w5.png" delay=".8s" alt="loading" />
      <LoadingComponent src="/resources/w4.png" delay=".7s" alt="loading" />
      <LoadingComponent src="/resources/w3.png" delay=".6s" alt="loading" />
      <LoadingComponent src="/resources/w2.png" delay=".5s" alt="loading" />
      <LoadingComponent src="/resources/w1.png" delay=".4s" alt="loading" />
      <LoadingComponent src="/resources/w0.png" delay=".3s" alt="loading" />
    </Container>
  );
};

export default LoadingLogo;
