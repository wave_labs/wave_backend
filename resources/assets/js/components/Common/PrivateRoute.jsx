import React, { Component } from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({
  component: Component,
  path,
  isAuthenticated,
  user,
  route,
  source,
}) =>
  source == "main" ? (
    <Route
      render={(props) =>
        isAuthenticated ? (
          <Route path={path} component={Component} />
        ) : (
          <Redirect
            to={{
              pathname: route,
              state: { from: props.location },
            }}
          />
        )
      }
    />
  ) : (
    <Route
      render={(props) =>
        user.roles ? (
          user.roles[0].name == "admin" ? (
            <Route path={path} component={Component} />
          ) : (
            <Redirect
              to={{
                pathname: route,
                state: { from: props.location },
              }}
            />
          )
        ) : (
          <Redirect
            to={{
              pathname: route,
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    user: state.auth.user,
  };
}

export default connect(mapStateToProps)(PrivateRoute);
