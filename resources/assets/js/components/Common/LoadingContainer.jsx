import React from "react";
import { Spin, Row } from "antd";
import { Fragment } from "react";
import styled from "styled-components";

const Container = styled.div`
  width: 100%;
`;

const SpinContainer = styled(Container)`
  text-align: center;
`;

let LoadingContainer = ({ loading, children }) => {
  return (
    <Container>
      {loading ? (
        <SpinContainer>
          <Spin />
        </SpinContainer>
      ) : (
        <Fragment>{children}</Fragment>
      )}
    </Container>
  );
};

export default LoadingContainer;
