import React, { useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { connect } from "react-redux";
import { logout } from "redux-modules/auth/actions";
import { Layout, Row, Col, Menu, Dropdown, Icon, Button, Affix, Modal, Drawer } from "antd";
import { Fragment } from "react";

const { Header } = Layout;

const StyledNavBarLink = styled(Link)`
  color: black;
  text-decoration: none;
  font-size: 15px;
  padding: 0 0.5rem;

  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    color: black;
  }

  &:hover {
    color: #636b6f;
  }
`;

const Logo = styled(Link)`
  color: black;
  text-decoration: none;
  font-size: 16px;

  img {
    width: 70px;
  margin-right: 3px;
  }

  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    color: black;
  }

  &:hover {
    color: #636b6f;
  }

  @media screen and (max-width: 900px) {
    padding-left: 0;
  }
`;

const StyledSpace = styled.div`
  color: #636b6f;
  font-size: 15px;
  padding: 0 0.5rem;
`;

const StyledNavBar = styled.div`
  color: black;
  text-decoration: none;
  display: inline-block;
  padding: 0 0.5rem;
`;

const StyledHeader = styled(Header)`
  background: rgb(255, 254, 252) !important;
  padding: 0 !important;
  transition: 0.5s ease;
`;

const StyledAffix = styled(Affix)`
  .ant-affix {
    z-index: 999;
  }
`;

const StyledResponsive = styled.div`
box-sizing: border-box;
  @media (min-width: 1000px) {
    display: none;
  }
`;

const Hidden = styled.div`
box-sizing: border-box;
  @media (max-width: 1000px) {
    display: none;
  }
`;

const DrawerNavbar = styled(Drawer)`
  h3{
    font-weight: bold;
    margin-top: 6px;
    margin-bottom: 0px;
  }
  
  .modal-link {
    color: black;
    text-decoration: none;
    font-size: 16px;
    margin-bottom: 0px;
    display: block;

    &:focus,
    &:hover,
    &:visited,
    &:link,
    &:active {
      color: black;
    }

    &:hover {
      color: #636b6f;
    }
  }
`;

const sections = {
    Kits: [
        { url: "/kits/iot", name: "IoT" },
        { url: "/kits/ai", name: "AI" },
        { url: "/kits/domes", name: "Domes" },
        { url: "/kits/apps/", name: "Apps" },
        /* { url: "/kits/apps/whaleReporter", name: "Whale Reporter" },
        { url: "/kits/apps/diveReporter", name: "Dive Reporter" },
        { url: "/kits/apps/litterReporter", name: "Litter Reporter" },
        { url: "/kits/apps/ardome", name: "ARDome" }, */
    ],
    Projects: [
        { url: "/intertagua", name: "Intertagua" },
        { url: "/largescale", name: "Largescale" },
        { url: "/interwhale", name: "Interwhale" },
    ],
    Research: [
        { url: "/publications", name: "Publications" },
        { url: "/studies", name: "Studies" },
        { url: "/news", name: "What's New" },
        { url: "/datasets", name: "Datasets" },
        { url: "/models", name: "AI Models" },
    ],
    Lab: [
        { url: "/about", name: "About" },
        { url: "/team", name: "Team" },
        { url: "/join", name: "Join" },
        { url: "/mediakit", name: "Media Kit" },
    ],
    Support: [
        { url: "/contacts", name: "Contact" },
        { url: "/faq", name: "FAQ's" },
    ],
};

function NavBar({ isAuthenticated, logout, scroll }) {
    const [visible, setVisible] = useState(false);
    return (
        <StyledAffix offsetTop={0}>
            <StyledHeader
                style={{
                    ...(!scroll
                        ? { borderBottom: "1px solid rgb(245, 239, 228)" }
                        : { borderBottom: "1px solid rgb(255, 254, 252)" }),
                }}
            >
                <Row>
                    <Col xs={20} lg={4}>
                        <Logo to="/">
                            <img src="/wave.png" />
                            Wave Labs
                        </Logo>
                    </Col>

                    <Col xs={4} lg={20}>
                        <Hidden>
                            <Row type="flex" justify="end">
                                <Fragment>
                                    {Object.entries(sections).map((section, index) => (
                                        <Dropdown
                                            key={index}
                                            overlay={
                                                <Menu>
                                                    {section[1].map((element) => (
                                                        <Menu.Item key={element.url}>
                                                            <StyledNavBarLink to={element.url}>
                                                                {element.name}
                                                            </StyledNavBarLink>
                                                        </Menu.Item>
                                                    ))}
                                                </Menu>
                                            }
                                        >
                                            <StyledNavBar>
                                                {section[0]} <Icon type="down" />
                                            </StyledNavBar>
                                        </Dropdown>
                                    ))}
                                </Fragment>
                                <StyledSpace>|</StyledSpace>
                                {isAuthenticated ? (
                                    <div>
                                        <StyledNavBarLink to="/dashboard">
                                            Dashboard
                                        </StyledNavBarLink>
                                        <StyledNavBarLink
                                            to=""
                                            onClick={() => {
                                                logout();
                                            }}
                                        >
                                            Logout
                                        </StyledNavBarLink>
                                    </div>
                                ) : (
                                    <div>
                                        <StyledNavBarLink to="/login">Login</StyledNavBarLink>
                                    </div>
                                )}
                            </Row>
                        </Hidden>

                        <StyledResponsive>
                            <Row type="flex" justify="end">
                                <Button onClick={() => setVisible(true)} style={{ margin: "15px" }} type="primary">
                                    <Icon type="menu" />
                                </Button>
                            </Row>
                        </StyledResponsive>
                    </Col>
                </Row>
            </StyledHeader>

            <DrawerNavbar
                visible={visible}
                onClose={() => setVisible(false)}
                placement="right"
            >
                <h3>Account</h3>
                {isAuthenticated ? (
                    <Fragment>
                        <Link onClick={() => setVisible(false)} className="modal-link" to="/dashboard">
                            Dashboard
                        </Link>
                        <Link className="modal-link"
                            to=""
                            onClick={() => {
                                logout();
                            }}
                        >
                            Logout
                        </Link>
                    </Fragment>
                ) : (
                    <Link onClick={() => setVisible(false)} className="modal-link" to="/login">Login</Link>
                )}
                {Object.entries(sections).map((section) => (
                    <Fragment>
                        <h3>{section[0]}</h3>
                        {section[1].map((element) => (
                            <Link onClick={() => setVisible(false)} className="modal-link" to={element.url}>
                                {element.name}
                            </Link>
                        ))}
                    </Fragment>
                ))}
            </DrawerNavbar>
        </StyledAffix >
    );
};

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => dispatch(logout()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NavBar);
