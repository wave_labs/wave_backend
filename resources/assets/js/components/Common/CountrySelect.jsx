import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Select} from 'antd';
import axios from "axios";

const Option = Select.Option;

class CountrySelect extends Component
{
  state = {
    countries: []
  }

  componentDidMount(){
   axios
     .get(`${window.location.origin}/api/country`)
     .then(({ data })=> {
        const countries = data.countries;
        this.setState({ countries });
     })
  }

  render(){
    return (
    	<Select
        {... this.props} //Passing antd props to the select
        showSearch
        placeholder="Select a country"
        optionFilterProp="children"
        filterOption={(input, option) => option.props.children.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0}
        >
          {this.state.countries.map((country,i) => (
           	<Option key={i} value={country.name}> {country.name} </Option>
          ))}
    	</Select>
    	);
  }
}

export default CountrySelect