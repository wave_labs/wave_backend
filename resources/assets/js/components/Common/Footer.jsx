import React from "react";
import { Row, Col } from "antd";
import styled from "styled-components";
import { Link } from "react-router-dom";

const StyledTitle = styled.div`
  font-weight: bold;
  padding-bottom: 1%;

  @media screen and (max-width: 1200px) {
    border-bottom: ${(props) => props.border};
    margin-bottom: ${(props) => props.margin};
  }
`;

const StyledLogo = styled.img`
  filter: grayscale(100%);
`;

const StyledLink = styled(Link)`
  display: block;
  color: black;
  font-weight: 100;

  &:hover {
    color: #777777;
  }
`;

const StyledEmail = styled.a`
  display: block;
  color: black;
  font-weight: 100;

  &:hover {
    color: #777777;
  }
`;

const StyledDescription = styled.div`
  font-size: 0.8rem;
  color: #777777;
`;

const StyledTerms = styled(Link)`
  text-decoration: underline;
  cursor: pointer;
  font-size: 0.8rem;
  color: #777777;

  &:hover {
    color: #2f3538;
    text-decoration: underline;
  }
`;

const StyledColumn = styled.div`
  margin: 30px 20px;
  @media screen and (max-width: 1200px) {
    flex-basis: 100%;
  }
`;

const StyledBigColumn = styled(StyledColumn)`
  max-width: 25%;
  @media screen and (max-width: 1200px) {
    max-width: 100%;
    order: 10; // Needs to be bigger than the last column order. It becomes the last column on mobile
  }
`;

const Container = styled.div`
  width: 100%;
  border-top: 2px solid rgb(245, 239, 228);
  margin-top: 10%;
`;

function Footer() {
  return (
    <Container>
      <Row type="flex" justify="space-around">
        <StyledBigColumn>
          <Col xs={24} xl={7}>
            <StyledLogo src="/wave.png" width="65" />
          </Col>
          <Col xs={24} xl={17}>
            <StyledTitle border="none" margin="0">
              Wave Labs
            </StyledTitle>
            <StyledDescription>
              Impacting United Nations SDG14: Life Below Water
            </StyledDescription>
            <StyledDescription>
              ©{new Date().getFullYear()}| Wave Labs, Inc.
            </StyledDescription>
            <StyledTerms to="/privacy">Terms & Privacy</StyledTerms>
          </Col>
        </StyledBigColumn>

        <StyledColumn>
          <StyledTitle>Kits & Projects</StyledTitle>
          <StyledLink to="/kits/apps">Aplications</StyledLink>
          <StyledLink to="/kits/iot">IoT</StyledLink>
          <StyledLink to="/kits/domes">Domes</StyledLink>
          <StyledLink to="/kits/ai">AI</StyledLink>
          <StyledLink to="/latestReports">Intertagua</StyledLink>
          <StyledLink to="/largescale">Largescale</StyledLink>
        </StyledColumn>

        <StyledColumn>
          <StyledTitle>Research</StyledTitle>
          <StyledLink to="/publications">Publications</StyledLink>
          <StyledLink to="/studies">Studies</StyledLink>
          <StyledLink to="/team">Team</StyledLink>
          <StyledLink to="/news">What's New</StyledLink>
        </StyledColumn>

        <StyledColumn>
          <StyledTitle>Lab</StyledTitle>
          <StyledLink to="/about">About</StyledLink>
          <StyledLink to="/join">Join</StyledLink>
          <StyledLink to="/mediakit">Media Kit</StyledLink>
        </StyledColumn>

        <StyledColumn>
          <StyledTitle>Help & Contact</StyledTitle>
          <StyledLink to="/contacts">Contact Us</StyledLink>
          <StyledLink to="/faq">FAQ's</StyledLink>
          <StyledEmail href="mailto:team@wave-labs.org">Email Us</StyledEmail>
        </StyledColumn>
      </Row>
    </Container>
  );
}

export default Footer;
