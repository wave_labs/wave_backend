import React from "react";
import { Divider, Row } from "antd";
import styled from "styled-components";
import { dimensions } from "helpers";

const StyledTitle = styled.div`
  font-size: 3em;
  font-weight: bold;
  margin: auto 10px;

  @media (max-width: ${dimensions.md}) {
    margin: 0px;
  }
`;

const SelectionContainer = styled(Row)`
  margin: 5% auto;
`;

const ItemContainer = styled.div`
  width: ${(props) => props.width};
  min-width: 250px;
  border: 1px solid lightgray;
  border-radius: 6px;
  box-sizing: border-box;
  padding: 30px;
  cursor: pointer;
  margin: 3% auto;

  &:hover {
    box-shadow: 0 5px 5px 0 rgba(0, 0, 0, 0.2);
  }

  img {
    max-width: 200px;
    width: 60%;
    margin: auto;
    display: block;
  }

  h1 {
    text-align: center;
    font-size: 1.5em;
    font-weight: bold;
    margin-top: 20px;
  }

  p {
    text-align: center;
    font-size: 1.1em;
    color: #777777;
    margin-bottom: 5%;
  }
`;

const TopicListContainer = ({ history, content, topic }) => {
  const Item = ({ to, image, name, description }) => (
    <ItemContainer
      width={content.length >= 4 ? "23%" : "30%"}
      onClick={() => history.push("/dashboard/" + to)}
    >
      <img src={image} alt={name} />
      <h1>{name}</h1>
      <p>{description}</p>
    </ItemContainer>
  );

  return (
    <div>
      <Divider orientation="left">
        <StyledTitle> {topic} </StyledTitle>
      </Divider>
      <SelectionContainer type="flex" justify="space-around" >
        {content.map((element) => (
          <Item
            key={element.name}
            to={element.to}
            image={element.image}
            name={element.name}
            description={element.description}
          />
        ))}
      </SelectionContainer>
    </div>
  );
};

export default TopicListContainer;
