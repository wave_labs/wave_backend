import React, { Component } from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { fetchCreatures } from "redux-modules/creature/actions";
import { startCreating } from "redux-modules/editCreateModal/actions";
import FilterRow from "../../Common/FilterRow";

const FilterContainer = styled.div`
  width: 100%;
  display: block;
  margin: 1rem auto;
`;

class CreatureFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operations: ["reset"],
      filters: {
        search: undefined,
      },
    };
  }

  componentDidMount() {
    this.setState({
      filters: { search: undefined, creatureType: this.props.creatureType },
    });
    if (this.props.isAdmin) {
      let { operations } = this.state;
      operations.push("create");
      this.setState({ operations });
    }
  }

  handleFilterChange = (filter, value) => {
    var { filters } = this.state;
    filters[filter] = value;
    this.setState({ filters });
  };

  handleReset = () => {
    let { filters } = this.state;
    filters.search = undefined;
    this.setState({ filters });
  };

  componentDidUpdate(prevProps, prevState) {
    console.log("newFilters");
    if (prevState !== this.state) {
      console.log("newFilters2");
      this.props.fetchCreatures(1, this.state.filters);
      this.props.handleFilterChange(this.state.filters);
    }
  }

  render() {
    return (
      <FilterContainer>
        <FilterRow
          filters={["search"]}
          filterValues={this.state.filters}
          operations={this.state.operations}
          handleFilterChange={this.handleFilterChange}
          handleCreate={() => this.props.startCreating()}
          handleReset={() => this.handleReset()}
        />
      </FilterContainer>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCreatures: (page, filters) => dispatch(fetchCreatures(page, filters)),
    startCreating: () => dispatch(startCreating()),
  };
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatureFilters);
