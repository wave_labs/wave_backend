import React, { Component } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  InputNumber,
  Button,
  Checkbox,
  Select,
} from "antd";
import { levelConverter } from "helpers";
import CreatureTypeRemoteSelectContainer from "../CreatureTypeContent/CreatureTypeRemoteSelectContainer";
import UploadFileFormItem from "../../Common/UploadFileFormItem";

const FormItem = Form.Item;
const TextArea = Input.TextArea;
const Option = Select.Option;

const StyledInputNumber = styled(InputNumber)`
  min-width: 100% !important;
`;

class CreatureForm extends Component {
  state = {};

  rules = {
    name: [
      {
        required: true,
        message: "Name is required",
      },
    ],
    typeID: [
      {
        required: true,
        message: "Creature type is required",
      },
    ],
    conserv: [
      {
        required: true,
        message: "Conservation is required",
      },
    ],
    name_scientific: [
      {
        required: true,
        message: "Scientific name is required",
      },
    ],
    nis_status: [
      {
        required: true,
        message: "NIS status is required",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { creature, editing, creating } = this.props;

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Scientific Name">
              {getFieldDecorator("name_scientific", {
                initialValue: creature.name_scientific,
                rules: this.rules.name_scientific,
              })(<Input placeholder="Name" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Name">
              {getFieldDecorator("name", {
                initialValue: creature.name,
                rules: this.rules.name,
              })(<Input placeholder="Creature" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Type">
              {getFieldDecorator("type_id", {
                initialValue: creature.type.id,
                rules: this.rules.typeID,
              })(
                <CreatureTypeRemoteSelectContainer>
                  {editing && (
                    <Option value={type.id} key={type.id}>
                      {type.taxa_category}
                    </Option>
                  )}
                </CreatureTypeRemoteSelectContainer>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Conservation">
              {getFieldDecorator("conserv", {
                initialValue: creature.conserv,
                rules: this.rules.conserv,
              })(
                <Select placeholder="Conservation list">
                  <Option value="Extinct">Extinct</Option>
                  <Option value="Extinct in the Wild">
                    Extinct in the Wild
                  </Option>
                  <Option value="Critical Endangered">
                    Critical Endangered
                  </Option>
                  <Option value="Endangered">Endangered</Option>
                  <Option value="Vulnerable">Vulnerable</Option>
                  <Option value="Near Threatened">Near Threatened</Option>
                  <Option value="Least Concern">Least Concern</Option>
                  <Option value="Data Deficient">Data Deficient</Option>
                  <Option value="Not Evaluated">Not Evaluated</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Size">
              {getFieldDecorator("size", {
                initialValue: creature.size,
              })(<Input placeholder="Size" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Group size">
              {getFieldDecorator("group_size", {
                initialValue: creature.group_size,
              })(<Input placeholder="Group size" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="NIS status">
              {getFieldDecorator("nis_status", {
                initialValue: creature.nis_status,
                rules: this.rules.nis_status,
              })(
                <Select placeholder="NIS status">
                  <Option value="Native">Native</Option>
                  <Option value="Non-Indigenous Species">
                    Non-Indigenous Species
                  </Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Calves">
              {getFieldDecorator("calves", {
                initialValue: creature.calves,
              })(<Input placeholder="Calves" />)}
            </FormItem>
          </Col>
        </Row>
        {(creature.dive_center_app || creating) && (
          <Row gutter={8}>
            <Col xs={24} sm={6}>
              <FormItem hasFeedback label="Level 1">
                {getFieldDecorator("level_1", {
                  initialValue:
                    creature.abundance &&
                    levelConverter[0][creature.abundance.level_1],
                })(
                  <Select placeholder="Level 1">
                    <Option value={1}>1</Option>
                    <Option value={2}>0 - 1 m^2</Option>
                    <Option value={3}>1 - 3</Option>
                    <Option value={4}>1 - 5</Option>
                    <Option value={5}>1 - 15</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col xs={24} sm={6}>
              <FormItem hasFeedback label="Level 2">
                {getFieldDecorator("level_2", {
                  initialValue:
                    creature.abundance &&
                    levelConverter[1][creature.abundance.level_2],
                })(
                  <Select placeholder="Level 2">
                    <Option value={1}>2</Option>
                    <Option value={2}>1 - 5 m^2</Option>
                    <Option value={3}>4 - 6</Option>
                    <Option value={4}>2 - 3</Option>
                    <Option value={5}>6 - 10</Option>
                    <Option value={6}>16 - 30</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col xs={24} sm={6}>
              <FormItem hasFeedback label="Level 3">
                {getFieldDecorator("level_3", {
                  initialValue:
                    creature.abundance &&
                    levelConverter[2][creature.abundance.level_3],
                })(
                  <Select placeholder="Level 3">
                    <Option value={1}>3</Option>
                    <Option value={2}>5 - 10 m^2</Option>
                    <Option value={3}>7 - 10</Option>
                    <Option value={4}>4 - 5</Option>
                    <Option value={5}>11 - 15</Option>
                    <Option value={6}>31 - 50</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col xs={24} sm={6}>
              <FormItem hasFeedback label="Level 4">
                {getFieldDecorator("level_4", {
                  initialValue:
                    creature.abundance &&
                    levelConverter[3][creature.abundance.level_4],
                })(
                  <Select placeholder="Level 4">
                    <Option value={1}>{">"} 3</Option>
                    <Option value={2}>{">"} 10 m^2</Option>
                    <Option value={3}>{">"} 10</Option>
                    <Option value={4}>{">"} 5</Option>
                    <Option value={5}>{">"} 15</Option>
                    <Option value={6}>{">"} 50</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
          </Row>
        )}

        <FormItem hasFeedback label="Description">
          {getFieldDecorator("description", {
            initialValue: creature.description,
          })(
            <TextArea
              placeholder="Description"
              autosize={{ minRows: 2, maxRows: 6 }}
            />
          )}
        </FormItem>
        <FormItem hasFeedback label="Curiosity">
          {getFieldDecorator("curiosity", {
            initialValue: creature.curiosity,
          })(
            <TextArea
              placeholder="Curiosity"
              autosize={{ minRows: 2, maxRows: 6 }}
            />
          )}
        </FormItem>
        <Col xs={24}>
          <Row type="flex" justify="center" gutter={32}>
            <FormItem>
              {getFieldDecorator("dive_center_app", {
                initialValue: creature.dive_center_app,
                valuePropName: "checked",
              })(<Checkbox> Dive creature</Checkbox>)}
            </FormItem>
            <FormItem>
              {getFieldDecorator("whale_reporter_app", {
                initialValue: creature.whale_reporter_app,
                valuePropName: "checked",
              })(<Checkbox> Whale Reporter Creature</Checkbox>)}
            </FormItem>
          </Row>
        </Col>

        <UploadFileFormItem
          form={this.props.form}
          rules={null}
          link={creature.photo}
          key={creature.id}
        />
      </Form>
    );
  }
}

export default Form.create()(CreatureForm);
