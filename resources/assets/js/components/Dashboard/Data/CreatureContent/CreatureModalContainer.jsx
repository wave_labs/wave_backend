import React, { Component } from "react";
import {
  createCreature,
  updateCreature,
  resetCreatureEdit,
} from "redux-modules/creature/actions";
import {
  resetModal,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../Common/EditCreateModal";
import CreatureForm from "./CreatureForm";
import { alertActions } from "redux-modules/alert/actions";

class CreatureModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, creature) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        this.props
          .updateCreature(this.props.editCreature.id, creature)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function(message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.resetCreatureEdit();
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, creature) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });

        this.props
          .createCreature(creature)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function(message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit Creature"
        titleCreate="Create Creature"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <CreatureForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          creature={this.props.editCreature}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetCreatureEdit: () => dispatch(resetCreatureEdit()),
    resetModal: () => dispatch(resetModal()),
    createCreature: (data) => dispatch(createCreature(data)),
	updateCreature: (data, id) => dispatch(updateCreature(data, id)),
	errorAlert: (data) => dispatch(alertActions.error(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    editCreature: state.creature.editCreature,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatureModalContainer);
