import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/creature/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class CreatureRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      CreatureSelector,
      loading,
      value,
      onChange,
      mode,
      children,
      placeholder = "Creature",
      scientific,
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
        mode={mode}
      >
        <Option value={undefined} key={0}>{placeholder}</Option>
        {CreatureSelector.map((creature) => (
          <Option value={creature.id} key={creature.id}>
            {scientific ? creature.name_scientific : creature.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    CreatureSelector: state.creature.CreatureSelector,
    loading: state.creature.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatureRemoteSelectContainer);
