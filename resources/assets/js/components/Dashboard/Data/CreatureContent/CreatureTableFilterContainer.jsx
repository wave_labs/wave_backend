import React, { Component } from "react";
import {
  fetchCreatures,
  deleteCreature,
  setCreatureEdit,
  updateCreature,
} from "redux-modules/creature/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import styled from "styled-components";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { NoDataMessage } from "helpers";

const StyledEllipsis = styled.div`
  overflow-y: auto;
  text-overflow: ellipsis;
  width: 300px;
  max-height: 180px;
`;

class CreatureTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.columns = [
      {
        title: "Name",
        dataIndex: "name",
      },
      {
        title: "Scientific name",
        dataIndex: "name_scientific",
      },
      {
        title: "Abundance levels",
        dataIndex: "abundance",
        render: (abundance) =>
          abundance
            ? `${abundance.level_1}, ${abundance.level_2}, ${
                abundance.level_3
              }, ${abundance.level_4}`
            : NoDataMessage(),
      },
      {
        title: "Conservation",
        dataIndex: "conserv",
      },
      {
        title: "Group size",
        dataIndex: "group_size",
        render: (group_size) =>
          group_size ? `${group_size}` : NoDataMessage(),
      },
      {
        title: "Calves",
        dataIndex: "calves",
        render: (calves) => (calves ? `${calves}` : NoDataMessage()),
      },
      {
        title: "NIS Status",
        dataIndex: "nis_status",
      },
      {
        title: "",
        key: "",
        render: (text, creature) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(creature)}
            onDeleteConfirm={() => this.props.deleteCreature(creature.id)}
          />
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.filters = {
      creatureType: this.props.creatureType,
    };

    this.props.fetchCreatures(1, {
      creatureType: this.props.creatureType,
    });
  };

  onUpdateClick = (creature) => {
    this.props.setCreatureEdit(creature);
    this.props.startEditing();
  };

  handleTableChange = (pagination) => {
    this.filters = {
      ...this.filters,
      ...this.props.filters,
    };
    this.props.fetchCreatures(pagination.current, this.filters);
  };
  render() {
    return (
      <div>
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCreatures: (page, filters) => dispatch(fetchCreatures(page, filters)),
    deleteCreature: (id) => dispatch(deleteCreature(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setCreatureEdit: (data) => dispatch(setCreatureEdit(data)),
    updateCreature: (id, data) => dispatch(updateCreature(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.creature.data,
    meta: state.creature.meta,
    loading: state.creature.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatureTableFilterContainer);
