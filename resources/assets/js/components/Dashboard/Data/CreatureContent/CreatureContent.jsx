import React, { Component } from "react";
import CreatureFilters from "./CreatureFilters";
import CreatureModalContainer from "./CreatureModalContainer";
import CreatureTableFilterContainer from "./CreatureTableFilterContainer";

class CreatureContent extends Component {
  state = {
    filters: [],
  };

  handleFilterChange = (newFilters) => {
    this.setState({ filters: newFilters });
  };

  render() {
    return (
      <div>
        <CreatureFilters
          handleFilterChange={(newFilters) =>
            this.handleFilterChange(newFilters)
          }
          creatureType={this.props.match.params.id}
        />
        <CreatureModalContainer />
        <CreatureTableFilterContainer
          creatureType={this.props.match.params.id}
          filters={this.state.filters}
        />
      </div>
    );
  }
}

export default CreatureContent;
