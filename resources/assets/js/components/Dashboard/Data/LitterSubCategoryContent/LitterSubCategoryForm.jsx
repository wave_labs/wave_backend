import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Form, Input, InputNumber, Checkbox } from "antd";
import LitterCategoryRemoteSelectContainer from "../LitterCategoryContent/LitterCategoryRemoteSelectContainer";

const FormItem = Form.Item;

const convertBoolean = {
  1: true,
  0: false,
};

const StyledInputNumber = styled(InputNumber)`
  min-width: 100% !important;
`;

class LitterSubCategoryForm extends Component {
  state = {};

  rules = {
    name: [
      {
        required: true,
        message: "Name  is required",
      },
    ],
    materials: [
      {
        required: true,
        message: "Material  is required",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { litterSubCategory, editing } = this.props;
    const { category } = litterSubCategory;

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Name">
              {getFieldDecorator("name", {
                initialValue: litterSubCategory.name,
                rules: this.rules.name,
              })(<Input placeholder="Name" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Category">
              {getFieldDecorator("litter_category_id", {
                initialValue: category.id,
                rules: this.rules.category,
              })(
                <LitterCategoryRemoteSelectContainer>
                  {editing && (
                    <Option value={category.id} key={category.id}>
                      {category.name}
                    </Option>
                  )}
                </LitterCategoryRemoteSelectContainer>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="OSPAR_code">
              {getFieldDecorator("OSPAR_code", {
                initialValue: litterSubCategory.OSPAR_code,
              })(<Input placeholder="OSPAR code" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="UNEP_code">
              {getFieldDecorator("UNEP_code", {
                initialValue: litterSubCategory.UNEP_code,
              })(<Input placeholder="UNEP code" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={8}>
            <FormItem>
              {getFieldDecorator("Core", {
                initialValue: convertBoolean[litterSubCategory.Core],
                valuePropName: "checked",
              })(<Checkbox> Core </Checkbox>)}
            </FormItem>
          </Col>
          <Col xs={24} sm={8}>
            <FormItem>
              {getFieldDecorator("Beach", {
                initialValue: convertBoolean[litterSubCategory.Beach],
                valuePropName: "checked",
              })(<Checkbox> Beach </Checkbox>)}
            </FormItem>
          </Col>
          <Col xs={24} sm={8}>
            <FormItem>
              {getFieldDecorator("Seafloor", {
                initialValue: convertBoolean[litterSubCategory.Seafloor],
                valuePropName: "checked",
              })(<Checkbox> Seafloor </Checkbox>)}
            </FormItem>
          </Col>
          <Col xs={24} sm={8}>
            <FormItem>
              {getFieldDecorator("Floating", {
                initialValue: convertBoolean[litterSubCategory.Floating],
                valuePropName: "checked",
              })(<Checkbox> Floating </Checkbox>)}
            </FormItem>
          </Col>
          <Col xs={24} sm={8}>
            <FormItem>
              {getFieldDecorator("Biota", {
                initialValue: convertBoolean[litterSubCategory.Biota],
                valuePropName: "checked",
              })(<Checkbox> Biota </Checkbox>)}
            </FormItem>
          </Col>
          <Col xs={24} sm={8}>
            <FormItem>
              {getFieldDecorator("Micro", {
                initialValue: convertBoolean[litterSubCategory.Micro],
                valuePropName: "checked",
              })(<Checkbox> Micro </Checkbox>)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(LitterSubCategoryForm);
