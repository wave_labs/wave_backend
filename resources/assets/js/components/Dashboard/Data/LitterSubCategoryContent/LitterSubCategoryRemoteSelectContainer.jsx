import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/litterSubCategory/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class LitterSubCategoryRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (name) => {
    this.props.fetchSelector({ name });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const { LitterSubCategorySelector, loading } = this.props;

    return (
      <SelectSearch
        value={this.props.value}
        onChange={this.props.onChange}
        onSearch={(name) => this.onSearch(name)}
        loading={loading}
        placeholder="Litter Sub Category"
      >
        {this.props.children}
        {LitterSubCategorySelector.map((litterSubCategory) => (
          <Option value={litterSubCategory.id} key={litterSubCategory.id}>
            {litterSubCategory.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    LitterSubCategorySelector:
      state.litterSubCategory.LitterSubCategorySelector,
    loading: state.litterSubCategory.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LitterSubCategoryRemoteSelectContainer);
