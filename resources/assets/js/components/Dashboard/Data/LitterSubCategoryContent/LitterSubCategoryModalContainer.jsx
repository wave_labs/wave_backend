import React, { Component } from "react";
import {
  createLitterSubCategory,
  updateLitterSubCategory,
  resetLitterSubCategoryEdit,
} from "redux-modules/litterSubCategory/actions";
import { resetModal } from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../Common/EditCreateModal";
import LitterSubCategoryForm from "./LitterSubCategoryForm.jsx";
import { alertActions } from "redux-modules/alert/actions";

class LitterSubCategoryModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, litterSubCategory) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        this.props
          .updateLitterSubCategory(
            this.props.editLitterSubCategory.id,
            litterSubCategory
          )
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function(message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.resetLitterSubCategoryEdit();
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, litterSubCategory) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });

        this.props
          .createLitterSubCategory(litterSubCategory)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function(message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit Sub Category"
        titleCreate="Create Sub Category"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <LitterSubCategoryForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          litterSubCategory={this.props.editLitterSubCategory}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetLitterSubCategoryEdit: () => dispatch(resetLitterSubCategoryEdit()),
    resetModal: () => dispatch(resetModal()),
    createLitterSubCategory: (data) => dispatch(createLitterSubCategory(data)),
    updateLitterSubCategory: (data, id) =>
      dispatch(updateLitterSubCategory(data, id)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    editLitterSubCategory: state.litterSubCategory.editLitterSubCategory,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LitterSubCategoryModalContainer);
