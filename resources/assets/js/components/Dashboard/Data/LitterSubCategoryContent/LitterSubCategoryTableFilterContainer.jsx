import React, { Component } from "react";
import {
  fetchLitterSubCategories,
  deleteLitterSubCategory,
  setLitterSubCategoryEdit,
  updateLitterSubCategory,
} from "redux-modules/litterSubCategory/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { isFieldNull } from "helpers";

const tableFilters = [{ text: "True", value: 1 }, { text: "False", value: 0 }];

class LitterSubCategoryTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {
      category: null,
    };

    this.columns = [
      {
        title: "Name",
        dataIndex: "name",
      },
      {
        title: "OSPAR",
        dataIndex: "OSPAR_code",
        render: (data) => isFieldNull(data),
      },
      {
        title: "UNEP",
        dataIndex: "UNEP_code",
        render: (data) => isFieldNull(data),
      },
      {
        title: "Core",
        dataIndex: "Core",
        render: (Core) => (Core == 1 ? "True" : "False"),
        filters: tableFilters,
        filterMultiple: false,
      },
      {
        title: "Beach",
        dataIndex: "Beach",
        render: (Beach) => (Beach == 1 ? "True" : "False"),
        filters: tableFilters,
        filterMultiple: false,
      },
      {
        title: "Seafloor",
        dataIndex: "Seafloor",
        render: (Seafloor) => (Seafloor == 1 ? "True" : "False"),
        filters: tableFilters,
        filterMultiple: false,
      },
      {
        title: "Floating",
        dataIndex: "Floating",
        render: (Floating) => (Floating == 1 ? "True" : "False"),
        filters: tableFilters,
        filterMultiple: false,
      },
      {
        title: "Biota",
        dataIndex: "Biota",
        render: (Biota) => (Biota == 1 ? "True" : "False"),
        filters: tableFilters,
        filterMultiple: false,
      },
      {
        title: "Micro",
        dataIndex: "Micro",
        render: (Micro) => (Micro == 1 ? "True" : "False"),
        filters: tableFilters,
        filterMultiple: false,
      },
      {
        title: "",
        key: "",
        render: (text, litterSubCategory) => {
          return (
            this.props.isAdmin && (
              <RowOperation
                deleteRow
                updateRow
                onUpdateClick={() => this.onUpdateClick(litterSubCategory)}
                onDeleteConfirm={() =>
                  this.props.deleteLitterSubCategory(litterSubCategory.id)
                }
              />
            )
          );
        },
      },
    ];
  }

  componentDidMount = () => {
    this.filters = {
      category: this.props.param,
    };

    this.props.fetchLitterSubCategories(1, {
      category: this.props.param,
    });
  };

  onUpdateClick = (litterSubCategory) => {
    this.props.setLitterSubCategoryEdit(litterSubCategory);
    this.props.startEditing();
  };

  handleTableChange = (pagination, filters, sorter) => {
    /*
    Object.entries(filters).map(function(element) {
      element[1][0] == 1 && (filters[element[0]] = true);
      element[1][0] == 0 && (filters[element[0]] = false);
    });
    */
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchLitterSubCategories(pagination.current, this.filters);
  };

  render() {
    return (
      <div>
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchLitterSubCategories: (page, filters) =>
      dispatch(fetchLitterSubCategories(page, filters)),
    deleteLitterSubCategory: (id) => dispatch(deleteLitterSubCategory(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setLitterSubCategoryEdit: (data) =>
      dispatch(setLitterSubCategoryEdit(data)),
    updateLitterSubCategory: (id, data) =>
      dispatch(updateLitterSubCategory(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
    data: state.litterSubCategory.data,
    meta: state.litterSubCategory.meta,
    loading: state.litterSubCategory.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LitterSubCategoryTableFilterContainer);
