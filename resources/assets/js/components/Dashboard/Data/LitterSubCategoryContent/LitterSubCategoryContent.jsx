import React, { Component } from "react";
import LitterSubCategoryFilters from "./LitterSubCategoryFilters";
import LitterSubCategoryModalContainer from "./LitterSubCategoryModalContainer";
import LitterSubCategoryTableFilterContainer from "./LitterSubCategoryTableFilterContainer";

class LitterSubCategoryContent extends Component {
  state = {
    filters: [],
  };

  handleFilterChange = (newFilters) => {
    this.setState({ filters: newFilters });
  };

  render() {
    return (
      <div>
        <LitterSubCategoryFilters
          handleFilterChange={(newFilters) =>
            this.handleFilterChange(newFilters)
          }
          param={this.props.match.params.id}
        />
        <LitterSubCategoryModalContainer />
        <LitterSubCategoryTableFilterContainer
          category={this.props.match.params.id}
          filters={this.state.filters}
        />
      </div>
    );
  }
}

export default LitterSubCategoryContent;
