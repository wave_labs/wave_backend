import React, { Component } from "react";
import {
  fetchLitterCategories,
  deleteLitterCategory,
  setLitterCategoryEdit,
} from "redux-modules/litterCategory/actions";
import styled from "styled-components";
import { connect } from "react-redux";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { Row } from "antd";
import CardFooter from "../../Common/CardFooter";
import { Link } from "react-router-dom";
import { dimensions } from "helpers";

const Avatar = styled.img`
  width: 50%;
  margin: 5% auto;
  display: block;
  min-width: 100px;
`;

const Item = styled.div`
  width: 23%;
  min-width: 250px;
  margin: 1.5% 0;
  border: 1px solid lightgray;
  border-radius: 6px;
  box-sizing: border-box;

  @media (max-width: ${dimensions.md}) {
    margin: 1.5% auto;
  }
`;

const Title = styled.div`
  font-size: 1.2em;
  text-align: center;
  margin-bottom: 30px;
  color: black;
`;

class LitterCategoryListContainer extends Component {
  componentDidMount = () => {
    this.props.fetchLitterCategories();
  };

  handleUpdate = (record) => {
    this.props.setLitterCategoryEdit(record);
    this.props.startEditing();
  };

  handleDelete = (record) => {
    this.props.deleteLitterCategory(record.id);
  };

  render() {
    let { data, isAdmin } = this.props;

    return (
      <div>
        <Row type="flex" justify="space-between" align="middle">
          {data.map((record) => (
            <Item key={record.id}>
              <Link to={"/dashboard/litter/categories/" + record.id}>
                <Avatar
                  src={record.menu ? record.menu.url : "/placeholder.png"}
                />

                <Title>{record.name}</Title>
              </Link>
              {isAdmin && (
                <CardFooter
                  handleUpdate={() => this.handleUpdate(record)}
                  handleDelete={() => this.handleDelete(record)}
                />
              )}
            </Item>
          ))}
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchLitterCategories: (page, filters) =>
      dispatch(fetchLitterCategories(page, filters)),
    deleteLitterCategory: (id) => dispatch(deleteLitterCategory(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setLitterCategoryEdit: (data) => dispatch(setLitterCategoryEdit(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
    data: state.litterCategory.data,
    meta: state.litterCategory.meta,
    loading: state.litterCategory.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LitterCategoryListContainer);
