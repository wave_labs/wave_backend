import React from "react";
import LitterCategoryModalContainer from "./LitterCategoryModalContainer";
import LitterCategoryListContainer from "./LitterCategoryListContainer";
import LitterCategoryFilters from "./LitterCategoryFilters";
import { connect } from "react-redux";

const LitterCategoryContent = ({ isAdmin }) => {
  return (
    <div>
      {isAdmin != undefined && <LitterCategoryFilters />}
      <LitterCategoryModalContainer />
      <LitterCategoryListContainer />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(
  mapStateToProps,
  null
)(LitterCategoryContent);
