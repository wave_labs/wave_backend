import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/litterCategory/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class LitterCategoryRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const { LitterCategorySelector, loading } = this.props;

    return (
      <SelectSearch
        value={this.props.value}
        onChange={this.props.onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Litter Category"
      >
        {this.props.children}
        {LitterCategorySelector.map((litterCategory) => (
          <Option value={litterCategory.id} key={litterCategory.id}>
            {litterCategory.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    LitterCategorySelector: state.litterCategory.LitterCategorySelector,
    loading: state.litterCategory.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LitterCategoryRemoteSelectContainer);
