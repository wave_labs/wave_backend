import React, { Component } from "react";
import {
  createLitterCategory,
  updateLitterCategory,
  resetLitterCategoryEdit,
} from "redux-modules/litterCategory/actions";
import { resetModal } from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../Common/EditCreateModal";
import LitterCategoryForm from "./LitterCategoryForm.jsx";
import { alertActions } from "redux-modules/alert/actions";
import { getErrorMessages } from "helpers";

class LitterCategoryModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
    fileList: [],
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, litterCategory) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        this.props
          .updateLitterCategory(
            this.props.editLitterCategory.id,
            litterCategory
          )
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = getErrorMessages(error.response.data.errors);

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({
      confirmLoading: false,
      fileList: null,
      previewImage: null,
    });
    this.props.resetLitterCategoryEdit();
    this.props.resetModal();
    form.resetFields();
  };

  handlePhoto = (photo, preview) => {
    this.setState({ fileList: photo, previewImage: preview });
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, litterCategory) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        let formData = new FormData();
        formData.append("name", litterCategory.name);
        formData.append("image", this.state.fileList);
        this.props
          .createLitterCategory(formData)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = getErrorMessages(error.response.data.errors);

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit  Category"
        titleCreate="Create  Category"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <LitterCategoryForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          litterCategory={this.props.editLitterCategory}
          previewImage={this.state.previewImage}
          handlePhoto={this.handlePhoto}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetLitterCategoryEdit: () => dispatch(resetLitterCategoryEdit()),
    resetModal: () => dispatch(resetModal()),
    createLitterCategory: (data) => dispatch(createLitterCategory(data)),
    updateLitterCategory: (data, id) =>
      dispatch(updateLitterCategory(data, id)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    editLitterCategory: state.litterCategory.editLitterCategory,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LitterCategoryModalContainer);
