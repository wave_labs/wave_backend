import React, { Component } from "react";
import { Row, Col, Form, Input } from "antd";
import { getBase64 } from "helpers";
import ImagePreview from "../../Common/ImagePreview";

const FormItem = Form.Item;

class LitterCategoryForm extends Component {
  state = {};

  rules = {
    name: [
      {
        required: true,
        message: "Name  is required",
      },
    ],
  };

  handleChange = (info) => {
    getBase64(info.file.originFileObj, (imageUrl) =>
      this.props.handlePhoto(info.file.originFileObj, imageUrl)
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { litterCategory, editing, previewImage } = this.props;

    console.log(litterCategory);

    return (
      <Form hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24}>
            <FormItem hasFeedback>
              {getFieldDecorator("name", {
                initialValue: litterCategory.name,
                rules: this.rules.name,
              })(<Input placeholder="Category" />)}
            </FormItem>
          </Col>
          <Col xs={24}>
            <FormItem>
              {getFieldDecorator("image", {})(
                <ImagePreview
                  previewImage={previewImage}
                  handleChange={this.handleChange}
                  tipText="Square white image with transparent background"
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(LitterCategoryForm);
