import React, { Component } from "react";
import { connect } from "react-redux";
import { Cascader } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/litterCategory/actions";

class LitterCategoryRemoteCascadeContainer extends Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  state = { options: [] };

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  handleOptions = async () => {
    const { LitterCategorySelector } = this.props;
    let options = [],
      children = [];

    await LitterCategorySelector.forEach((litterCategory) => {
      litterCategory.sub_category.forEach((litterSubCat) => {
        children.push({
          value: litterSubCat.id,
          label: litterSubCat.name,
        });
      });

      options.push({
        value: litterCategory.id,
        label: litterCategory.name,
        children: children,
      });
      children = [];
    });

    this.setState({
      options: options,
    });
  };

  componentDidMount = async () => {
    let { source } = this.props;

    await this.props.fetchSelector({ source });
    this.handleOptions();
  };

  render() {
    const { options } = this.state;

    return (
      <Cascader
        options={options}
        allowClear={false}
        expandTrigger="hover"
        suffixIcon={<div />}
      />
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    LitterCategorySelector: state.litterCategory.LitterCategorySelector,
    loading: state.litterCategory.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LitterCategoryRemoteCascadeContainer);
