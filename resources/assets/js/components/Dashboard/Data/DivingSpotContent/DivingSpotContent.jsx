import React, { useEffect, useState } from "react";
import { Row, Col, Divider } from "antd";
import DivingSpotFilters from "./DivingSpotFilters";
import {
  fetchDivingSpots,
  fetchSelector,
} from "redux-modules/divingSpot/actions";
import DivingSpotModalContainer from "./DivingSpotModalContainer";
import { connect } from "react-redux";
import DivingSpotMapList from "./DivingSpotMapList";
import DivingSpotTableContainer from "./DivingSpotTableContainer";
import styled from "styled-components";

const StyledTitle = styled.div`
  font-size: 2rem;
`;

function DivingSpotContent(props) {
  const { isAdmin, data, user } = props;
  const [filters, setFilters] = useState({});
  const [mapBounds, setMapBounds] = useState(null);
  const [associatedData, setAssociatedData] = useState(data);
  const [notAssociatedData, setNotAssociatedData] = useState(data);
  const [creating, setCreating] = useState(null);
  const [editing, setEditing] = useState(null);

  useEffect(() => {
    props.fetchSelector(filters);
  }, []);

  useEffect(() => {
    if (!isAdmin) {
      setFilters({ ...filters, validated: 1 });
    }
  }, [isAdmin]);

  useEffect(() => {
    props.fetchSelector(filters);
    console.log(filters);
  }, [filters]);

  useEffect(() => {
    var associated = [];
    var notAssociated = [];
    data.forEach((item) => {
      if (hasDivingSpot(item.users)) {
        associated.push(item);
      }else{
        notAssociated.push(item)
      }
    });
    setAssociatedData(associated);
    setNotAssociatedData(notAssociated);
  }, [data]);

  const hasDivingSpot = (users) => {
    var hasUser = false;
    users.map((element) => {
      if (element.id == user.id) {
        hasUser = true;
      }
    });

    return hasUser;
  };

  const handleZoom = (values) => {
    if (mapBounds) {
      if (
        mapBounds._ne.lat !== values._ne.lat &&
        mapBounds._ne.lng !== values._ne.lng
      ) {
        setMapBounds(values);
      }
    } else {
      setMapBounds(values);
    }
  };
  
  return (
    <div>
      <Row>
        <Divider orientation="left">
          {!isAdmin ? (
            <StyledTitle> My Diving Spots </StyledTitle>
          ) : (
            <StyledTitle> Diving Spots </StyledTitle>
          )}
        </Divider>
      </Row>
      <DivingSpotModalContainer creating={creating} editing={editing} setCreating={setCreating} setEditing={setEditing}/>
      {isAdmin != undefined && <DivingSpotFilters admin={isAdmin} setCreating={setCreating}/>}
      {isAdmin ? (
        <Row type="flex" gutter={24}>
          <Col xs={24} lg={12}>
            <DivingSpotTableContainer data={data} filters={filters} admin={isAdmin} setEditing={setEditing} setCreating={setCreating}/>
          </Col>
          <Col xs={24} lg={12}>
            <DivingSpotMapList
              filters={filters}
              data={data}
              user={user}
              admin={isAdmin}
            />
          </Col>
        </Row>
      ) : (
        <Row type="flex" gutter={24}>
          <Col xs={24} lg={24}>
            <DivingSpotMapList
              setMapBounds={handleZoom}
              filters={filters}
              data={data}
              user={user}
              admin={isAdmin}
            />
          </Col>
          {/* <StyledTitle> Associated dive spots </StyledTitle> */}
          <Col xs={24} lg={12}>
            <DivingSpotTableContainer
              mapbounds={mapBounds}
              filters={filters}
              admin={isAdmin}
              data={associatedData}
              title={"Associated dive spots"}
              color={"#f7f7f7"}
              setEditing={setEditing}
              setCreating={setCreating}
            />
          </Col>
          {/* <StyledTitle> Not associated dive spots </StyledTitle> */}
          <Col xs={24} lg={12}>
            <DivingSpotTableContainer
              mapbounds={mapBounds}
              filters={filters}
              admin={isAdmin}
              data={notAssociatedData}
              title={"Not associated dive spots"}
              color={"#000000"}
              setEditing={setEditing}
              setCreating={setCreating}
            />
          </Col>
        </Row>
      )}
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    isAdmin: state.auth.isAdmin,
    data: state.divingSpot.DivingSpotSelector,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DivingSpotContent);
