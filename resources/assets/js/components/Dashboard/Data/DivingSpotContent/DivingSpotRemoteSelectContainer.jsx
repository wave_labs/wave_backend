import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/divingSpot/actions";
import SelectSearch from "../../Common/SelectSearch";

const Option = Select.Option;

class DivingSpotRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 500);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      DivingSpotSelector,
      loading,
      value,
      onChange,
      mode,
      children,
      placeholder = "Diving Spot",
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
        mode={mode}
      >
        <Option value={undefined} key={0}>
          {placeholder}
        </Option>
        {DivingSpotSelector.map((d) => (
          <Option value={d.id} key={d.id}>
            {d.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    DivingSpotSelector: state.divingSpot.DivingSpotSelector,
    loading: state.divingSpot.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DivingSpotRemoteSelectContainer);
