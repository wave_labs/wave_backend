import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Form, Input, InputNumber, Select } from "antd";
import CoordinatesForm from "./Form/CoordinatesForm";
import DivingSpotSubstractRemoteSelectContainer from "./DivingSpotSubstractRemoteSelectContainer";

const FormItem = Form.Item;
const Option = Select.Option;
const TextArea = Input.TextArea;

const StyledInputNumber = styled(InputNumber)`
  min-width: 100% !important;
`;

class DivingSpotForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      positions: [],
      activeOnClick: true,
    };
  }

  rules = {
    name: [
      {
        required: true,
        message: "Name  is required",
      },
    ],
    latitude: [
      {
        required: true,
        message: "Latitude  is required",
      },
    ],
    longitude: [
      {
        required: true,
        message: "Longitude  is required",
      },
    ],
    address: [
      {
        required: true,
        message: "Address  is required",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { divingSpot, editing } = this.props;
    console.log("diving spot", divingSpot);
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Name">
              {getFieldDecorator("name", {
                initialValue: divingSpot.name,
                rules: this.rules.name,
              })(<Input placeholder="Name" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Max depth">
              {getFieldDecorator("range", {
                initialValue: divingSpot.range,
              })(<Input placeholder="Max depth" />)}
            </FormItem>
          </Col>
          <Col span={24}>
            <CoordinatesForm
              state={this.state}
              setState={(values) => this.setState(values)}
              handlePositions={(positions) =>
                this.props.form.setFieldsValue(positions)
              }
            />
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Latitude">
              {getFieldDecorator("latitude", {
                initialValue: divingSpot.latitude,
                rules: this.rules.latitude,
              })(
                <Input
                  onChange={(e) =>
                    parseFloat(e.target.value) &&
                    this.setState({
                      positions: [
                        parseFloat(e.target.value),
                        this.state.positions[1] ? this.state.positions[1] : 0,
                      ],
                    })
                  }
                  placeholder="Latitude"
                />
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Longitude">
              {getFieldDecorator("longitude", {
                initialValue: divingSpot.longitude,
                rules: this.rules.longitude,
              })(
                <Input
                  onChange={(e) =>
                    parseFloat(e.target.value) &&
                    this.setState({
                      positions: [
                        this.state.positions[0] ? this.state.positions[0] : 0,
                        parseFloat(e.target.value),
                      ],
                    })
                  }
                  placeholder="Longitude"
                />
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Substrate">
              {getFieldDecorator("substract", {
                initialValue: divingSpot.substract,
              })(<DivingSpotSubstractRemoteSelectContainer mode="multiple" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Protection">
              {getFieldDecorator("protection", {
                initialValue: divingSpot.protection,
              })(
                <Select placeholder="Protection">
                  <Option value="Marine Protected Area">
                    Marine Protected Area
                  </Option>
                  <Option value="Natural Reserve">Natural Reserve</Option>
                  <Option value="None">None</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <FormItem hasFeedback label="Description">
          {getFieldDecorator("description", {
            initialValue: divingSpot.description,
          })(
            <TextArea
              placeholder="Description"
              autosize={{ minRows: 2, maxRows: 6 }}
            />
          )}
        </FormItem>
      </Form>
    );
  }
}

export default Form.create()(DivingSpotForm);
