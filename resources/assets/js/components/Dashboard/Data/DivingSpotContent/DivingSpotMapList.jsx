import React, { Component } from "react";
import MapBoxMap from "../../Common/MapBoxMap";
import { Marker, Popup } from "react-map-gl";
import {
  fetchDivingSpots,
  fetchSelector,
  addToUser,
  removeFromUser,
} from "redux-modules/divingSpot/actions";
import MapPin from "../../Common/MapPin";
import { connect } from "react-redux";
import DivingSpotPopUp from "./DivingSpotPopUp";

class DivingSpotMapList extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    popup: null,
  };

  showInfo = () => {
    const { popup } = this.state;

    return (
      popup && (
        <Popup
          tipSize={5}
          longitude={parseFloat(popup.longitude)}
          latitude={parseFloat(popup.latitude)}
          closeOnClick={false}
          onClose={() => this.setState({ popup: null })}
        >
          <DivingSpotPopUp viewMore={this.props.viewMore} diving_spot={popup} />
        </Popup>
      )
    );
  };

  hasDivingSpot = (users) => {
    var hasUser = false;
    users.map((element) => {
      if (element.id == this.props.user.id) {
        hasUser = true;
      }
    });
    return hasUser;
  };

  addSpot = (id) => {
    this.props.addToUser(id);
    this.props.fetchSelector(this.props.filters);
  };

  removeSpot = (id) => {
    this.props.removeFromUser({ divingSpot: id });
    this.props.fetchSelector(this.props.filters);
  };

  getGreenMarkerLink = () =>
    `${window.location.origin}/storage/images/map-markers/33.png`;

  getRedMarkerLink = () =>
    `${window.location.origin}/storage/images/map-markers/0.png`;

  render() {
    return (
      <div className="map-container">
        <MapBoxMap setMapBounds={this.props.setMapBounds} zoom={9}>
          {this.props.admin
            ? this.props.data.map((element) =>
                element.validated == 1 ? (
                  <Marker
                    key={element.id}
                    longitude={parseFloat(element.longitude)}
                    latitude={parseFloat(element.latitude)}
                  >
                    <MapPin
                      color={"#008000"}
                      onClick={() => this.setState({ popup: element })}
                      onMouseEnter={() => this.setState({ popup: element })}
                      onMouseOut={() => this.setState({ popup: null })}
                    />
                  </Marker>
                ) : (
                  <Marker
                    key={element.id}
                    longitude={parseFloat(element.longitude)}
                    latitude={parseFloat(element.latitude)}
                  >
                     <MapPin
                      onClick={() => this.setState({ popup: element })}
                      onMouseEnter={() => this.setState({ popup: element })}
                      onMouseOut={() => this.setState({ popup: null })}
                    />
                  </Marker>
                )
              )
            : this.props.data.map((element) =>
                this.hasDivingSpot(element.users) ? (
                  <Marker
                    key={element.id}
                    longitude={parseFloat(element.longitude)}
                    latitude={parseFloat(element.latitude)}
                  >
                    <MapPin
                      color={"#008000"}
                      onClick={() => this.removeSpot(element.id)}
                      onMouseEnter={() => this.setState({ popup: element })}
                      onMouseOut={() => this.setState({ popup: null })}
                    />
                  </Marker>
                ) : (
                  <Marker
                    key={element.id}
                    longitude={parseFloat(element.longitude)}
                    latitude={parseFloat(element.latitude)}
                  >
                    <MapPin
                      onClick={() => this.addSpot(element.id)}
                      onMouseEnter={() => this.setState({ popup: element })}
                      onMouseOut={() => this.setState({ popup: null })}
                    />
                  </Marker>
                )
              )}
          {this.showInfo()}
        </MapBoxMap>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
    addToUser: (data) => dispatch(addToUser(data)),
    removeFromUser: (data) => dispatch(removeFromUser(data)),
  };
};

export default connect(null, mapDispatchToProps)(DivingSpotMapList);
