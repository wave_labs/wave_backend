import React, { Component } from "react";
import styled from "styled-components";
import { Col, Row } from "antd";
import MapBoxMap from "../../Common/MapBoxMap";
import MapPin from "../../Common/MapPin";
import { Marker } from "react-map-gl";

import { updateDrawerDimensions } from "helpers";

import DrawerContainer from "../../Common/DrawerContainer";

const StyledDiv = styled.div`
  font-size: 14;
  line-height: 22px;
  margin-bottom: 10px;
  color: rgba(0, 0, 0, 0.65);
`;

const TitleDiv = styled.div`
  font-size: 30;
  line-height: 50px;
  margin-bottom: 10px;
  color: rgba(0, 0, 0, 0.65);
  text-align: center;
`;

const StyledRow = styled(Row)`
  flex-direction: column;
`;

const MapContainer = styled.div`
  width: 100%;
  margin-bottom: 30px;
  height: 400px;
`;

const StyledParagraph = styled.span`
  margin-right: 8px;
  display: inline-block;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.85);
  &:after {
    content: ":";
  }
`;

const StyledTitle = styled.span`
  margin-right: 8px;
  display: inline-block;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.85);
`;

const DescriptionItem = ({ span, title, content }) => (
  <Col span={span}>
    <StyledDiv>
      <StyledParagraph>{title}</StyledParagraph>
      {content}
    </StyledDiv>
  </Col>
);

const DescriptionTitle = ({ span, content }) => (
  <Col span={span}>
    <TitleDiv>
      <StyledTitle>{content}</StyledTitle>
    </TitleDiv>
  </Col>
);

class DivingSpotDrawer extends Component {
  state = {
    drawerWidth: "400px",
  };

  updateDimensions() {
    this.setState({ drawerWidth: updateDrawerDimensions(window) });
  }

  componentDidMount = () => {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  render() {
    let { content, position } = this.props;
    let { drawerWidth } = this.state;

    return (
      <DrawerContainer
        handleDrawerClose={this.props.handleDrawerClose}
        visible={this.props.visible}
        width={drawerWidth}
      >
        {content && (
          <StyledRow type="flex">
            <DescriptionTitle span={24} content={content.name} />
            {position.lat && position.lng && (
              <MapContainer>
                <MapBoxMap defaultCenter={position} fxeidSize={true}>
                  <Marker longitude={position.lng} latitude={position.lat}>
                    <MapPin />
                  </Marker>
                </MapBoxMap>
              </MapContainer>
            )}

            <Row type="flex" align="top">
              <DescriptionItem span={12} title="Id" content={content.id} />

              <DescriptionItem
                span={12}
                title="Latitude"
                content={content.latitude}
              />

              <DescriptionItem
                span={12}
                title="Longitude"
                content={content.longitude}
              />

              <DescriptionItem
                span={12}
                title="Max depth"
                content={content.range}
              />

              <DescriptionItem
                span={12}
                title="Substrates"
                content={
                  content.substracts.length > 0 && (
                    <ul>
                      {content.substracts.map(function (element, index) {
                        return (
                          <li key={index}>
                            <span>{element.name}</span>
                          </li>
                        );
                      })}
                    </ul>
                  )
                }
              />

              <DescriptionItem
                span={12}
                title="Protection"
                content={content.protection}
              />
            </Row>
          </StyledRow>
        )}
      </DrawerContainer>
    );
  }
}

export default DivingSpotDrawer;
