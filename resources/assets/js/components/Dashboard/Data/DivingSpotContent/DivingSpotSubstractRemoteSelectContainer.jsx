import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/divingSpotSubstract/actions";
import SelectSearch from "../../Common/SelectSearch";

const Option = Select.Option;

class DivingSpotSubstractRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
    this.props.fetchSelector();
    console.log(this.props);
  };

  render() {
    const { data, loading, value, onChange, mode, children } =
      this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        loading={loading}
        placeholder="Substrate"
        mode={mode}
      >
        {children}
        {data.map((d) => (
          <Option value={d.id} key={d.id}>
            {d.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: () => dispatch(fetchSelector()),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.divingSpotSubstract.selector,
    loading: state.divingSpot.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DivingSpotSubstractRemoteSelectContainer);
