import React, { Component } from "react";
import { Marker } from "react-map-gl";
import MapBoxMap from "../../../Common/MapBoxMap";
import MapPin from "../../../Common/MapPin";

class CoordinatesForm extends Component {
  rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  handleMarkerClick = (e) => {
    if (!e.leftButton) {
      this.props.setState({ positions: [] });
    }
  };

  handleMapClick = (e) => {
    if (e.leftButton) {
      let { positions, activeOnClick } = this.props.state;
      if (activeOnClick) {
        positions = [e.lngLat[1], e.lngLat[0]];
        this.props.setState({ positions });
      }
    }
  };

  handleDragEnd = (event) => {
    let { positions } = this.props.state;

    positions = [event.lngLat[1], event.lngLat[0]];

    setTimeout(() => {
      this.props.setState({
        positions,
        activeOnClick: true,
      });
    }, 1);
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.state.positions != this.props.state.positions) {
      if (this.props.state.positions.length == 2) {
        var object = {};
        object.latitude = this.props.state.positions[0];
        object.longitude = this.props.state.positions[1];

        this.props.handlePositions(object);
      }
    }
  }

  render() {
    const { positions } = this.props.state;

    return (
      <MapBoxMap
        fixedSize
        size="300"
        zoom={3}
        onClick={(e) => this.handleMapClick(e)}
      >
        {positions.length ? (
          <Marker
            latitude={positions[0]}
            longitude={positions[1]}
            draggable
            onDragEnd={this.handleDragEnd}
            onDragStart={() => this.props.setState({ activeOnClick: false })}
          >
            <MapPin onContextMenu={this.handleMarkerClick} />
          </Marker>
        ) : (
          <></>
        )}
      </MapBoxMap>
    );
  }
}

export default CoordinatesForm;
