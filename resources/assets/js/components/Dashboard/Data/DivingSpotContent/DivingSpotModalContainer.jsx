import React, { Component } from "react";
import {
  createDivingSpot,
  updateDivingSpot,
  resetDivingSpotEdit,
} from "redux-modules/divingSpot/actions";
import { resetModal } from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../Common/EditCreateModal";
import DivingSpotForm from "./DivingSpotForm";
import { alertActions } from "redux-modules/alert/actions";

class DivingSpotModalContainer extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    confirmLoading: false, //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
    this.props.setEditing(null);
    this.props.setCreating(null);
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, divingSpot) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        this.props
          .updateDivingSpot(this.props.editDivingSpot.id, divingSpot)
          .then((data) => {
            this.resetModalForm();
            this.props.setEditing(null);
            this.props.setCreating(null);
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function (message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.resetDivingSpotEdit();
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    console.log(form);
    form
      .validateFields()
      .then((data) => {
        this.props
          .createDivingSpot(data)
          .then(() => {
            this.resetModalForm();
            this.props.setEditing(null);
            this.props.setCreating(null);
            this.props.successAlert({
              description:
                "Your diving spot was submited, an admin will aprove it shortly",
            });
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function (message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      })
      .catch(() => {
        this.setState({ confirmLoading: true });
      });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit"
        titleCreate="Suggest"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <DivingSpotForm
          wrappedComponentRef={this.saveFormRef}
          divingSpot={this.props.editDivingSpot}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetDivingSpotEdit: () => dispatch(resetDivingSpotEdit()),
    resetModal: () => dispatch(resetModal()),
    createDivingSpot: (data) => dispatch(createDivingSpot(data)),
    updateDivingSpot: (data, id) => dispatch(updateDivingSpot(data, id)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
    successAlert: (data) => dispatch(alertActions.success(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    editDivingSpot: state.divingSpot.editDivingSpot,
    /* creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing, */
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DivingSpotModalContainer);
