import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Select } from "antd";
import styled from "styled-components";
import { fetchSelector } from "redux-modules/divingSpot/actions";
import FilterRow from "../../Common/FilterRow";
import { startCreating } from "redux-modules/editCreateModal/actions";

const StyledRow = styled(Row)`
  margin: 1rem;
`;

class DivingSpotFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {
        search: undefined,
      },
    };
  }

  handleFilterChange = (filter, value) => {
    var { filters } = this.state;
    filters[filter] = value;
    this.setState({ filters });
  };

  handleReset = () => {
    console.log(this.props.isAdmin);
    this.setState({
      filters: { search: undefined },
    });
    if (!this.props.isAdmin) {
      this.setState({ filters: { search: undefined, validated: 1 } });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state) {
      this.props.fetchSelector(this.state.filters);
    }
  }

  render() {
    return (
      <StyledRow>
        <FilterRow
          filters={[]}
          filterValues={this.state.filters}
          operations={["reset", "create"]}
          handleCreate={() => this.props.setCreating(true)}
          handleReset={this.handleReset}
          createText= {this.props.isAdmin ? "+ Add" : "Suggest"}
          createIcon=""
          additionalFilters={[
            <Select
              mode="tags"
              value={this.state.filters.search}
              style={{ width: "100%" }}
              placeholder="Search"
              onChange={(value) => this.handleFilterChange("search", value)}
            >
            </Select>,
          ]}
        />
      </StyledRow>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
    startCreating: () => dispatch(startCreating()),
  };
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DivingSpotFilters);
