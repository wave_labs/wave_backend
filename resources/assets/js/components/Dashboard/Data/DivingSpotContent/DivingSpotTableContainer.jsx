import React, { useEffect, useState } from "react";
import {
  fetchDivingSpots,
  deleteDivingSpot,
  setDivingSpotEdit,
  updateDivingSpot,
  addToUser,
  removeFromUser,
  fetchSelector,
} from "redux-modules/divingSpot/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "./TablePagination";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { NoDataMessage, formatPosition } from "helpers";
import DivingSpotDrawer from "./DivingSpotDrawer";
import styled from "styled-components";
import StopPropagation from "../../Common/StopPropagation";
import { Button } from "antd";
import { forEach } from "lodash";

const Status = styled.div`
  width: 15px;
  height: 15px;
  border-radius: 15px;
  margin: auto;
  cursor: pointer;
  background-color: ${(props) => (props.validated ? "green" : "red")};
`;


const StyledTitle = styled.div`
  font-size: 2rem;
`;

export const DivingSpotTableContainer = (props) => {
  const {
    data,
    meta,
    loading,
    isAdmin,
    user,
    filters,
    mapbounds,
    title,
    color,
    setEditing,
    setCreating
  } = props;
  const [drawerContent, setDrawerContent] = useState(null);
  const [coordinates, setCoordinates] = useState(null);
  const [inViewData, setInViewData] = useState(data);

  useEffect(() => {
    setInViewData(data);
  }, [data]);

  useEffect(() => {
    var inView = [];
    if (mapbounds != null) {
      data.forEach((item) => {
        if (
          item.latitude >= mapbounds._sw.lat &&
          item.latitude <= mapbounds._ne.lat &&
          item.longitude >= mapbounds._sw.lng &&
          item.longitude <= mapbounds._ne.lng
        ) {
          inView.push(item);
        }
        //calculate if its in view or not
      });
    }
    setInViewData(inView);
  }, [mapbounds, data]);

  const onUpdateClick = (divingSpot) => {
    props.setDivingSpotEdit(divingSpot)
    setEditing(true);
    setCreating(null);
    //props.startEditing();
  };

  const handleTableChange = (pagination, filters, sorter) => {
    props.fetchSelector({
      ...filters,
      order: [sorter.field, sorter.order],
    });
  };

  const handleRowClick = (record) => {
    setDrawerContent(record);
    setCoordinates(formatPosition(record));
  };

  const handleDrawerClose = () => {
    setDrawerContent(null);
    setCoordinates(null);
  };

  const handleStatusChange = (record) => {
    props.updateDivingSpot(record.id, {
      ...record,
      validated: !record.validated,
    });
    props.fetchSelector(filters);
  };

  const hasDivingSpot = (users) => {
    var hasUser = false;
    users.map((element) => {
      if (element.id == user.id) {
        hasUser = true;
      }
    });

    return hasUser;
  };

  const addSpot = (id) => {
    props.addToUser(id);
    props.fetchSelector(filters);
  };

  const removeSpot = (id) => {
    props.removeFromUser({ divingSpot: id });
    props.fetchSelector(filters);
  };

  var columns = [
    {
      title: isAdmin ? "" : "Associated",
      render: (row) => (
        <StopPropagation>
          {isAdmin ? (
            <></>
          ) : (
            <>
              {hasDivingSpot(row.users) ? (
                <Status validated={true} />
              ) : (
                <Status validated={false} />
              )}
            </>
          )}
        </StopPropagation>
      ),
    },
    {
      title: "ID",
      dataIndex: "id",
      sorter: true,
      width: 75,
    },
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Max depth",
      dataIndex: "range",
      render: (range) => (range ? `${range}` : NoDataMessage()),
    },
    {
      title: "Substrates",
      dataIndex: "substracts",
      render: (substracts) =>
        substracts.map((substract, index) => (
          <span>
            {substract.name}
            {index !== substracts.length - 1 && ", "}
          </span>
        )),
    },
    {
      title: "Protection",
      dataIndex: "protection",
    },
    {
      title: isAdmin ? "Validated" : "Associate",
      dataIndex: "validated",
      render: (validated, row) => (
        <StopPropagation>
          {isAdmin ? (
            <Status
              onClick={() => handleStatusChange(row)}
              validated={validated}
            />
          ) : (
            <>
              {hasDivingSpot(row.users) ? (
                <Button onClick={() => removeSpot(row.id)}>-</Button>
              ) : (
                <Button onClick={() => addSpot(row.id)}>+</Button>
              )}
            </>
          )}
        </StopPropagation>
      ),
    },
    {
      title: "",
      key: "",
      render: (text, divingSpot) => (
        <StopPropagation>
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => onUpdateClick(divingSpot)}
            onDeleteConfirm={() => props.deleteDivingSpot(divingSpot.id)}
          />
        </StopPropagation>
      ),
    },
  ];

  return (
    <div>
      <DivingSpotDrawer
        handleDrawerClose={handleDrawerClose}
        visible={drawerContent && true}
        content={drawerContent}
        position={coordinates}
      />
      {isAdmin ? (
        <TablePagination
          loading={loading}
          columns={columns}
          handleTableChange={handleTableChange}
          data={data}
          meta={meta}
          color={color}
          onRow={(record) => ({
            onClick: () => {
              handleRowClick(record);
            },
          })}
        />
      ) : (
        <>
          <StyledTitle> {title} </StyledTitle>
          <TablePagination
            loading={loading}
            columns={columns}
            handleTableChange={handleTableChange}
            data={inViewData}
            meta={meta}
            onRow={(record) => ({
              onClick: () => {
                handleRowClick(record);
              },
            })}
          />
        </>
      )}
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
    deleteDivingSpot: (id) => dispatch(deleteDivingSpot(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setDivingSpotEdit: (data) => dispatch(setDivingSpotEdit(data)),
    addToUser: (data) => dispatch(addToUser(data)),
    removeFromUser: (data) => dispatch(removeFromUser(data)),
    updateDivingSpot: (id, data) => dispatch(updateDivingSpot(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
    user: state.auth.user,
    /* data: state.divingSpot.DivingSpotSelector, */
    meta: state.divingSpot.meta,
    loading: state.divingSpot.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DivingSpotTableContainer);
