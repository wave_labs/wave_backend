import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Container = styled.div`
  padding: 4px 0px;
  min-width: 120px;
`;

const Content = styled.p`
  font-weight: bold;
  margin: 0 0;
  span {
    font-weight: normal;
  }
`;

const DivingSpotPopUp = ({ diving_spot, viewMore }) => {
  return (
    <Container>
      <Content>
        Name: <span>{diving_spot.name}</span>
      </Content>
      <Content>
        Coordinates:{" "}
        <span>{diving_spot.latitude + ", " + diving_spot.longitude}</span>
      </Content>
      <Content>
        Protection: <span>{diving_spot.protection}</span>
      </Content>

      {viewMore && <Link to={`/dashboard/divingSpot`}>View More</Link>}
    </Container>
  );
};

export default DivingSpotPopUp;
