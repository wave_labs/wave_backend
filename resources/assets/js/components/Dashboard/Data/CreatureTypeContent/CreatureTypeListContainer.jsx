import React, { Component } from "react";
import {
  fetchCreatureTypes,
  deleteCreatureType,
  setCreatureTypeEdit,
  updateCreatureType,
} from "redux-modules/creatureType/actions";
import styled from "styled-components";
import { connect } from "react-redux";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { Row } from "antd";
import CardFooter from "../../Common/CardFooter";
import { Link } from "react-router-dom";
import { dimensions } from "helpers";

const Avatar = styled.img`
  width: 50%;
  margin: 5% auto;
  display: block;
  min-width: 100px;
`;

const Item = styled.div`
  width: 23%;
  min-width: 250px;
  margin: 1.5% 0;
  border: 1px solid lightgray;
  border-radius: 6px;
  box-sizing: border-box;

  @media (max-width: ${dimensions.md}) {
    margin: 1.5% auto;
  }
`;

const Title = styled.div`
  font-size: 1.2em;
  text-align: center;
  margin-bottom: 30px;
  color: black;
`;

class CreatureTypeListContainer extends Component {
  componentDidMount = () => {
    this.props.fetchCreatureTypes();
  };

  handleUpdate = (record) => {
    this.props.setCreatureTypeEdit(record);
    this.props.startEditing();
  };

  handleDelete = (record) => {
    this.props.deleteCreatureType(record.id);
  };

  render() {
    let { data, isAdmin } = this.props;

    return (
      <div>
        <Row type="flex" justify="space-between" align="middle">
          {data.map((record) => (
            <Item key={record.id}>
              <Link to={"/dashboard/creatures/" + record.id}>
                <Avatar
                  src={record.image ? record.image : "/placeholder.png"}
                />

                <Title>{record.taxa_category}</Title>
              </Link>
              {isAdmin && (
                <CardFooter
                  handleUpdate={() => this.handleUpdate(record)}
                  handleDelete={() => this.handleDelete(record)}
                />
              )}
            </Item>
          ))}
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCreatureTypes: (page, filters) =>
      dispatch(fetchCreatureTypes(page, filters)),
    deleteCreatureType: (id) => dispatch(deleteCreatureType(id)),
    activateCreatureType: (id) => dispatch(activateCreatureType(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setCreatureTypeEdit: (data) => dispatch(setCreatureTypeEdit(data)),
    updateCreatureType: (id, data) => dispatch(updateCreatureType(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
    data: state.creatureType.data,
    meta: state.creatureType.meta,
    loading: state.creatureType.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatureTypeListContainer);
