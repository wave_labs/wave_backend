import React from "react";
import CreatureTypeModalContainer from "./CreatureTypeModalContainer";
import CreatureTypeListContainer from "./CreatureTypeListContainer";
import CreatureTypeFilters from "./CreatureTypeFilters";
import { connect } from "react-redux";

const CreatureTypeContent = ({ isAdmin }) => {
  return (
    <div>
      {isAdmin != undefined && <CreatureTypeFilters />}
      <CreatureTypeModalContainer />
      <CreatureTypeListContainer />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(
  mapStateToProps,
  null
)(CreatureTypeContent);
