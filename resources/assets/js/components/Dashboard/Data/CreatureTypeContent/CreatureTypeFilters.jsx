import React, { Component } from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { fetchCreatureTypes } from "redux-modules/creatureType/actions";
import { startCreating } from "redux-modules/editCreateModal/actions";
import FilterRow from "../../Common/FilterRow";

const FilterContainer = styled.div`
  width: 100%;
  display: block;
  margin: 1rem auto;
`;

class CreatureTypeFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operations: ["reset"],
      filters: {
        search: undefined,
      },
    };
  }

  componentDidMount() {
    if (this.props.isAdmin) {
      let { operations } = this.state;
      operations.push("create");
      this.setState({ operations });
    }
  }

  handleFilterChange = (filter, value) => {
    var { filters } = this.state;
    filters[filter] = value;
    this.setState({ filters });
  };

  handleReset = () => {
    this.setState({
      filters: { search: undefined },
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state.filters) {
      this.props.fetchCreatureTypes(1, this.state.filters);
    }
  }

  render() {
    return (
      <FilterContainer>
        <FilterRow
          filters={["search"]}
          filterValues={this.state.filters}
          operations={this.state.operations}
          handleFilterChange={this.handleFilterChange}
          handleCreate={() => this.props.startCreating()}
          handleReset={() => this.handleReset()}
        />
      </FilterContainer>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCreatureTypes: (page, filters) =>
      dispatch(fetchCreatureTypes(page, filters)),
    startCreating: () => dispatch(startCreating()),
  };
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatureTypeFilters);
