import React, { Component } from "react";
import { Row, Col, Form, Input, Select } from "antd";
import { getBase64 } from "helpers";
import ImagePreview from "../../Common/ImagePreview";

const FormItem = Form.Item;
const Option = Select.Option;

class CreatureTypeForm extends Component {
  state = {};

  rules = {
    taxa_category: [
      {
        required: true,
        message: "Taxa category is required",
      },
    ],
    acoustic_category: [
      {
        required: true,
        message: "Acoustic Category is required",
      },
    ],
    acoustic_data: [
      {
        required: true,
        message: "Acoustic Data is required",
      },
    ],
  };

  handleChange = (info) => {
    getBase64(info.file.originFileObj, (imageUrl) =>
      this.props.handlePhoto(info.file.originFileObj, imageUrl)
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { initialData, previewImage } = this.props;

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24}>
            <FormItem hasFeedback label="Name">
              {getFieldDecorator("taxa_category", {
                initialValue: initialData.taxa_category,
                rules: this.rules.taxa_category,
              })(<Input placeholder="Name" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Acoustic category">
              {getFieldDecorator("acoustic_category", {
                initialValue: initialData.acoustic_category,
                rules: this.rules.acoustic_category,
              })(
                <Select placeholder="Acoustic">
                  <Option value={1}>Yes</Option>
                  <Option value={0}>No</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Acoustic data">
              {getFieldDecorator("acoustic_data", {
                initialValue: initialData.acoustic_data,
                rules: this.rules.acoustic_data,
              })(
                <Select placeholder="Acoustic data">
                  <Option value={1}>Yes</Option>
                  <Option value={0}>No</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col xs={24}>
            <FormItem>
              {getFieldDecorator("image", {})(
                <ImagePreview
                  previewImage={previewImage}
                  handleChange={this.handleChange}
                  tipText="Image in same style as existing"
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(CreatureTypeForm);
