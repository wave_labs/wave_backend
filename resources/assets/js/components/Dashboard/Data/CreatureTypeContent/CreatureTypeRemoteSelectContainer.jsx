import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/creatureType/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class CreatureTypeRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = search => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      CreatureTypeSelector,
      loading,
      value,
      onChange,
      mode,
      children
    } = this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Creature Type"
        mode={mode}
      >
        {children}
        {CreatureTypeSelector.map(d => (
          <Option value={d.id} key={d.id}>
            {d.taxa_category}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchSelector: filters => dispatch(fetchSelector(filters))
  };
};

const mapStateToProps = state => {
  return {
    CreatureTypeSelector: state.creatureType.CreatureTypeSelector,
    loading: state.creatureType.loading
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatureTypeRemoteSelectContainer);
