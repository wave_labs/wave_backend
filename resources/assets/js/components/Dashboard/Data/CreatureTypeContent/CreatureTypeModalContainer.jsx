import React, { Component } from "react";
import {
  createCreatureType,
  updateCreatureType,
  resetCreatureTypeEdit,
} from "redux-modules/creatureType/actions";
import { resetModal } from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../Common/EditCreateModal";
import CreatureTypeForm from "./CreatureTypeForm";
import { alertActions } from "redux-modules/alert/actions";
import { getErrorMessages } from "helpers";

class CreatureTypeModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
    fileList: [],
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, creatureType) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        this.props
          .updateCreatureType(this.props.editCreatureType.id, creatureType)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = getErrorMessages(error.response.data.errors);

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({
      confirmLoading: false,
      fileList: null,
      previewImage: null,
    });
    this.props.resetCreatureTypeEdit();
    this.props.resetModal();
    form.resetFields();
  };

  handlePhoto = (photo, preview) => {
    this.setState({ fileList: photo, previewImage: preview });
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, data) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        let formData = new FormData();
        formData.append("taxa_category", data.taxa_category);
        formData.append("acoustic_category", data.acoustic_category);
        formData.append("acoustic_data", data.acoustic_data);
        formData.append("image", this.state.fileList);

        this.props
          .createCreatureType(formData)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = getErrorMessages(error.response.data.errors);

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit"
        titleCreate="Create"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <CreatureTypeForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          initialData={this.props.editCreatureType}
          previewImage={this.state.previewImage}
          handlePhoto={this.handlePhoto}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetCreatureTypeEdit: () => dispatch(resetCreatureTypeEdit()),
    resetModal: () => dispatch(resetModal()),
    createCreatureType: (data) => dispatch(createCreatureType(data)),
    updateCreatureType: (data, id) => dispatch(updateCreatureType(data, id)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    editCreatureType: state.creatureType.editCreatureType,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatureTypeModalContainer);
