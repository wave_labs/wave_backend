import React, { Component } from "react";
import { connect } from "react-redux";
import SightingInfo from "./SightingInfo";
import SightingDataList from "./SightingDataList";
import SightingDataModalContainer from "./SightingDataModalContainer";
import { Row, Col } from "antd";
import {
  fetchSighting,
  fetchSightingNextAndPrior,
} from "redux-modules/sighting/actions";
import MapBoxMap from "../Common/MapBoxMap";
import MapPin from "../Common/MapPin";
import ArrowNavigation from "../Common/ArrowNavigation";
import { Marker } from "react-map-gl";
import { formatPosition } from "helpers";

class SightingPage extends Component {
  state = {
    position: {},
  };

  async componentDidMount() {
    await this.props.fetchSighting(this.props.match.params.id);
    await this.props.fetchSightingNextAndPrior(this.props.match.params.id);

    this.setState({
      position: formatPosition(this.props.sighting),
    });
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.match.params.id !== nextProps.match.params.id) {
      await this.props.fetchSighting(nextProps.match.params.id);
      await this.props.fetchSightingNextAndPrior(nextProps.match.params.id);

      this.setState({
        position: formatPosition(this.props.sighting),
      });
    }
  }

  render() {
    let { sighting, loading, prior, next } = this.props;
    const { position } = this.state;

    return (
      <div>
        <SightingDataModalContainer />
        <Row gutter={12}>
          <Col xs={24} sm={8}>
            <ArrowNavigation
              prior={prior}
              next={next}
              url={`/dashboard/sightings/`}
            />
            <SightingInfo loading={loading} sighting={sighting} />
          </Col>
          <Col xs={24} sm={16}>
            {position.lat && position.lng && (
              <MapBoxMap defaultCenter={position} fixedSize={true}>
                <Marker longitude={position.lng} latitude={position.lat}>
                  <MapPin />
                </Marker>
              </MapBoxMap>
            )}
          </Col>
        </Row>

        <SightingDataList />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSighting: (id) => dispatch(fetchSighting(id)),
    fetchSightingNextAndPrior: (id) => dispatch(fetchSightingNextAndPrior(id)),
  };
};

const mapStateToProps = (state) => {
  return {
    sighting: state.sighting.current,
    prior: state.sighting.prior,
    next: state.sighting.next,
    loading: state.sighting.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SightingPage);
