import React from "react";
import { Card } from "antd";
import styled from "styled-components";
import { NoDataMessage } from "helpers";

const StyledTitle = styled.b`
  font-weight: bold;
`;

const StyledIndent = styled.div`
  padding-left: 1.25rem;
`;

const SightingInfo = ({ loading, sighting }) => {
  return (
    <Card loading={loading} bordered={false}>
      <div>
        <StyledTitle> ID: </StyledTitle>
        <span> {sighting.id}</span>
      </div>
      <div>
        <StyledTitle> Capsule: </StyledTitle>
        {sighting.capsule_id ? (
          <span> {sighting.capsule_id}</span>
        ) : (
          <span> {NoDataMessage()} </span>
        )}
      </div>
      <div>
        <StyledTitle> User </StyledTitle>
        <StyledIndent>
          <div>
            <StyledTitle> ID: </StyledTitle>
            <span> {sighting.userable.id}</span>
          </div>
          <div>
            <StyledTitle> Name: </StyledTitle>
            <span> {sighting.userable.user.name}</span>
          </div>
        </StyledIndent>
      </div>
      <div>
        <StyledTitle> Creature </StyledTitle>
        <StyledIndent>
          <div>
            <StyledTitle> ID: </StyledTitle>
            <span> {sighting.creature.id}</span>
          </div>
          <div>
            <StyledTitle> Name: </StyledTitle>
            <span> {sighting.creature.name}</span>
          </div>
        </StyledIndent>
      </div>
      {sighting.beaufort_scale ? (
        <div>
          <StyledTitle> Beaufort Scale </StyledTitle>
          <StyledIndent>
            <div>
              <StyledTitle> Scale: </StyledTitle>
              <span> {sighting.beaufort_scale.scale}</span>
            </div>
            <div>
              <StyledTitle> Description: </StyledTitle>
              <span> {sighting.beaufort_scale.desc}</span>
            </div>
            <div>
              <StyledTitle> Sea_Conditions: </StyledTitle>
              <span> {sighting.beaufort_scale.sea_conditions}</span>
            </div>
            <div>
              <StyledTitle> Land Conditions: </StyledTitle>
              <span> {sighting.beaufort_scale.land_conditions}</span>
            </div>
          </StyledIndent>
        </div>
      ) : (
        <div>
          <StyledTitle> Beaufort Scale: </StyledTitle>
          <StyledIndent>
            <div>
              <StyledTitle> Scale: </StyledTitle>
              <span> {NoDataMessage()} </span>
            </div>
            <div>
              <StyledTitle> Description: </StyledTitle>
              <span> {NoDataMessage()} </span>
            </div>
            <div>
              <StyledTitle> Sea_Conditions: </StyledTitle>
              <span> {NoDataMessage()} </span>
            </div>
            <div>
              <StyledTitle> Land Conditions: </StyledTitle>
              <span> {NoDataMessage()} </span>
            </div>
          </StyledIndent>
        </div>
      )}

      <div>
        <StyledTitle> Date: </StyledTitle>
        <span> {sighting.date}</span>
      </div>
      <div>
        <StyledTitle> Coordinates: </StyledTitle>
        <span> {`(${sighting.latitude},${sighting.longitude})`}</span>
      </div>
      <div>
        <StyledTitle> Group Type: </StyledTitle>
        <span> {sighting.is_group_mixed ? "Mixed" : "Uniform"}</span>
      </div>
      <div>
        <StyledTitle> Group Size: </StyledTitle>
        {sighting.group_size ? (
          <span> {sighting.group_size}</span>
        ) : (
          <span> {NoDataMessage()} </span>
        )}
      </div>
      <div>
        <StyledTitle> Behaviour: </StyledTitle>
        {sighting.behaviour ? (
          <span> {sighting.behaviour.behaviour}</span>
        ) : (
          <span> {NoDataMessage()} </span>
        )}
      </div>
      <div>
        <StyledTitle> Calves: </StyledTitle>
        <span> {sighting.calves}</span>
      </div>
      <div>
        <StyledTitle> Boat Type: </StyledTitle>
        {sighting.vehicle ? (
          <span> {sighting.vehicle.vehicle}</span>
        ) : (
          <span> {NoDataMessage()} </span>
        )}
      </div>
      <div>
        <StyledTitle> Notes: </StyledTitle>
        {sighting.notes ? (
          <span> {sighting.notes}</span>
        ) : (
          <span> {NoDataMessage()} </span>
        )}
      </div>
      <div>
        <StyledTitle> Created at: </StyledTitle>
        <span> {sighting.created_at}</span>
      </div>
    </Card>
  );
};

export default SightingInfo;
