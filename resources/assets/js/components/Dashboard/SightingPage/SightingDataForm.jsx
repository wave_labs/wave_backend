import React, { Component } from "react";
import { Row, Col, Select, Form } from "antd";
import UploadFileFormItem from "../Common/UploadFileFormItem";

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 }
  }
};

class SightingDataForm extends Component {
  state = {
    type: this.props.sightingData.type || null
  };

  checkFileType = (rule, value, callback) => {
    const form = this.props.form;
    const allowedType = rule.allowedType();
    if (
      value &&
      !value.defaultFile &&
      allowedType &&
      !allowedType.includes(value.type)
    ) {
      callback("File type should be of the following type: " + allowedType);
    }
    callback();
  };

  getAllowedType = () => {
    switch (this.state.type) {
      case "Video":
        return ["video/mp4"];
      case "Acoustic":
        return ["audio/mpeg"];
      case "Image":
        return ["image/jpeg", "image/png", "image/jpg"];
    }
  };

  rules = {
    type: [
      {
        required: true,
        message: "Sighting data type  is required!"
      }
    ],
    underwater: [
      {
        required: true,
        message: "Underwater is required!"
      }
    ],
    upload: [
      {
        required: true,
        message: "File is required!"
      },
      {
        allowedType: this.getAllowedType,
        validator: this.checkFileType
      }
    ],
    acoustic_type: [
      {
        required: true,
        message: "Acoustic type is required!"
      }
    ]
  };

  onTypeChange = type => this.setState({ type: type });

  render() {
    const { sightingData, editing, creating } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.onSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Underwater">
              {getFieldDecorator("underwater", {
                initialValue: sightingData.underwater,
                rules: this.rules.underwater
              })(
                <Select placeholder="Underwater">
                  <Option value={1}>Yes</Option>
                  <Option value={0}>No</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Data Type">
              {getFieldDecorator("type", {
                initialValue: sightingData.type,
                rules: this.rules.type
              })(
                <Select
                  onSelect={this.onTypeChange}
                  disabled={editing}
                  placeholder="Data Type"
                >
                  <Option value="Image">Image</Option>
                  <Option value="Acoustic">Acoustic</Option>
                  <Option value="Video">Video</Option>
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        {this.state.type === "Acoustic" && (
          <FormItem hasFeedback label="Acoustic Type">
            {getFieldDecorator("acoustic_type", {
              initialValue: sightingData.acoustic
                ? sightingData.acoustic[0].acoustic_type
                : null,
              rules: this.rules.acoustic_type
            })(
              <Select placeholder="Acoustic Type">
                <Option value="Click">Click</Option>
                <Option value="Moan">Moan</Option>
                <Option value="Whistle">whistle</Option>
              </Select>
            )}
          </FormItem>
        )}
        <UploadFileFormItem
          form={this.props.form}
          rules={this.rules.upload}
          link={sightingData.link}
          key={sightingData.id}
        />
      </Form>
    );
  }
}

export default Form.create()(SightingDataForm);
