import React from "react";
import { List, Card, Icon, Avatar } from "antd";
import styled from "styled-components";

const StyledTitle = styled.b`
	font-weight: bold;
`;

const Styled = styled(List.Item)`
	height: 500px;
`;

const DataListItem = ({ data, onDeleteClick, onUpdateClick }) => {
	const chooseCover = data => {
		switch (data.type) {
			case "Video":
				return (
					<video controls>
						<source
							src={`${window.location.origin}/storage/${
								data.link
							}`}
							type="video/mp4"
						/>
					</video>
				);
			case "Acoustic":
				return (
					<audio controls>
						<source
							src={`${window.location.origin}/storage/${
								data.link
							}`}
							type="audio/mpeg"
						/>
					</audio>
				);

			case "Image":
				return (
					<img
						src={`${window.location.origin}/storage/${data.link}`}
					/>
				);
		}
	};

	return (
		<List.Item>
			<Card
				cover={chooseCover(data)}
				actions={[
					<Icon onClick={() => onUpdateClick(data)} type="edit" />,
					<Icon
						onClick={() => onDeleteClick(data.id)}
						type="delete"
					/>
				]}
			>
				<div>
					<StyledTitle> ID: </StyledTitle>
					<span> {data.id}</span>
				</div>
				<div>
					<StyledTitle> Type: </StyledTitle>
					<span> {data.type}</span>
				</div>
				<div>
					<StyledTitle> Underwater: </StyledTitle>
					<span> {data.underwater == 1 ? "Yes" : "No"}</span>
				</div>
				{data.acoustic && (
					<div>
						<div>
							<StyledTitle> Duration: </StyledTitle>
							<span> {data.acoustic[0].duration_s} s</span>
						</div>
						<div>
							<StyledTitle> Acoustic Type: </StyledTitle>
							<span> {data.acoustic[0].acoustic_type}</span>
						</div>
					</div>
				)}
			</Card>
		</List.Item>
	);
};

export default DataListItem;
