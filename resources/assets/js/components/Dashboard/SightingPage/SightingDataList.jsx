import React, { Component } from "react";
import { connect } from "react-redux";
import SightingInfo from "./SightingInfo";
import DataListItem from "./DataListItem";
import { Icon, Row, Col, Button, List } from "antd";
import {
	fetchSighting,
	deleteSightingData,
	setCurrentSightingData
} from "redux-modules/sighting/actions";
import {
	startEditing,
	startCreating
} from "redux-modules/editCreateModal/actions";

const SightingDataList = ({
	startEditing,
	deleteSightingData,
	setCurrentSightingData,
	startCreating,
	sightingData,
	loading
}) => {
	const onUpdateClick = data => {
		setCurrentSightingData(data);
		startEditing();
	};

	return (
		<div>
			<Button onClick={startCreating}> Create </Button>
			<List
				grid={{ gutter: 16, column: 4 }}
				dataSource={sightingData}
				renderItem={item => (
					<DataListItem
						onDeleteClick={deleteSightingData}
						onUpdateClick={item => onUpdateClick(item)}
						data={item}
					/>
				)}
			/>
		</div>
	);
};

export default connect(
	state => ({
		sightingData: state.sighting.sightingDataList,
		loading: state.sighting.loading
	}),
	{
		deleteSightingData,
		setCurrentSightingData,
		startEditing,
		startCreating
	}
)(SightingDataList);
