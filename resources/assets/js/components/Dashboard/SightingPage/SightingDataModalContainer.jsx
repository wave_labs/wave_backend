import React, { Component } from "react";
import {
  updateSightingData,
  createSightingData,
  resetCurrentSightingData,
} from "redux-modules/sighting/actions";
import {
  resetModal,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../Common/EditCreateModal";
import SightingDataForm from "./SightingDataForm";
import moment from "moment";
import { Modal } from "antd";

class SightingDataModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, sightingData) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        const data = this.createFormData(sightingData);
        this.props
          .updateSightingData(this.props.sightingData.id, data)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.resetModal();
    this.props.resetCurrentSightingData();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, sightingData) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });

        const data = this.createFormData(sightingData);
        this.props
          .createSightingData(data)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  createFormData = (sightingData) => {
    let data = new FormData();
    data.append("file", sightingData.file);
    data.append("underwater", sightingData.underwater);
    data.append("type", sightingData.type);
    data.append("download", 0);
    data.append(
      "sighting_id",
      sightingData.sighting_id || this.props.sightingData.sighting_id
    );
    data.append("acoustic_type", sightingData.acoustic_type);

    if (this.props.editing) {
      //Php doens't allow puts with formDatas for some reason, so we must spoof it with a post
      data.append("_method", "PUT");
    }
    return data;
  };

  handleFormChange = (changedFields) => {
    console.log(changedFields);
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    const { sightingData, editing, creating } = this.props;
    return (
      <EditCreateModal
        titleEdit="Edit Sighting Data"
        titleCreate="Create Sighting Data"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={editing}
        creating={creating}
      >
        <SightingDataForm
          creating={creating}
          editing={editing}
          wrappedComponentRef={this.saveFormRef}
          sightingData={sightingData}
          onChange={this.handleFormChange}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetCurrentSightingData: () => dispatch(resetCurrentSightingData()),
    resetModal: () => dispatch(resetModal()),
    updateSightingData: (data, id) => dispatch(updateSightingData(data, id)),
    createSightingData: (data) => dispatch(createSightingData(data)),
    updateSightingData: (id, data) => dispatch(updateSightingData(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    sightingData: state.sighting.editingSightingData,
    sighting: state.sighting.current,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SightingDataModalContainer);
