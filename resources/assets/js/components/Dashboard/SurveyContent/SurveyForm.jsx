import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Form, InputNumber, Select, DatePicker } from "antd";
import UserRemoteSelectContainer from "../UserContent/UserRemoteSelectContainer";
import CreatureRemoteSelectContainer from "../Data/CreatureContent/CreatureRemoteSelectContainer";
import DiveRemoteSelectContainer from "../DiveContent/DiveRemoteSelectContainer";
import moment from "moment";

const FormItem = Form.Item;
const Option = Select.Option;

const StyledInputNumber = styled(InputNumber)`
  min-width: 100% !important;
`;

class SurveyForm extends Component {
  state = {};

  rules = {
    userId: [
      {
        required: true,
        message: "User  is required",
      },
    ],
    current: [
      {
        required: true,
        message: "Creature Type is required",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { survey, editing } = this.props;
    const { creature, userable, dive } = survey;

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="User">
              {getFieldDecorator("user_id", {
                initialValue: userable.id,
                rules: this.rules.userId,
              })(
                <UserRemoteSelectContainer>
                  {editing && (
                    <Option value={userable.id} key={userable.id}>
                      {userable.user.name}
                    </Option>
                  )}
                </UserRemoteSelectContainer>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Creature ">
              {getFieldDecorator("creature_id", {
                initialValue: creature.id,
                rules: this.rules.creatureId,
              })(<CreatureRemoteSelectContainer />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={6}>
            <FormItem hasFeedback label="Dive">
              {getFieldDecorator("dive_id", {
                initialValue: dive.id,
                rules: this.rules.diveId,
              })(
                <DiveRemoteSelectContainer>
                  {editing && (
                    <Option value={dive.id} key={dive.id}>
                      {dive.id}
                    </Option>
                  )}
                </DiveRemoteSelectContainer>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={6}>
            <FormItem hasFeedback label="Abundance">
              {getFieldDecorator("abundance_value", {
                initialValue: survey.abundance_value,
                rules: this.rules.abundance_value,
              })(<StyledInputNumber placeholder="Abundance" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Date">
              {getFieldDecorator("date", {
                initialValue: moment(survey.date),
                rules: this.rules.date,
              })(<DatePicker style={{ width: "100%" }} />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(SurveyForm);
