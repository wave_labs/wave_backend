import React, { Component } from "react";
import {
	createSurvey,
	updateSurvey,
	resetSurveyEdit
} from "redux-modules/survey/actions";
import {
	resetModal,
	startEditing
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../Common/EditCreateModal";
import SurveyForm from "./SurveyForm";
import moment from "moment";

class SurveyModalContainer extends Component {
	state = {
		confirmLoading: false //modal button loading icon
	};

	onCancel = () => {
		this.resetModalForm();
	};

	onOkEditClick = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, survey) => {
			if (err) {
				return;
			} else {
				const formatedDate = moment(survey.date).format("YYYY-MM-DD");

				this.setState({ confirmLoading: true });
				this.props
					.updateSurvey(this.props.editSurvey.id, { ...survey, date: formatedDate })
					.then(data => {
						this.resetModalForm();
					})
					.catch(e => this.setState({ confirmLoading: false }));
			}
		});
	};

	resetModalForm = () => {
		const form = this.formRef.props.form;
		this.setState({ confirmLoading: false });
		this.props.resetSurveyEdit();
		this.props.resetModal();
		form.resetFields();
	};

	onOkCreateClick = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, survey) => {
			if (err) {
				return;
			} else {
				const formatedDate = moment(survey.date).format("YYYY-MM-DD");
				this.setState({ confirmLoading: true });

				this.props
					.createSurvey({
						...survey,
						date: formatedDate
					})
					.then(data => {
						this.resetModalForm();
					})
					.catch(e => this.setState({ confirmLoading: false }));
			}
		});
	};


	saveFormRef = formRef => {
		this.formRef = formRef;
	};

	render() {
		return (
			<EditCreateModal
				titleEdit="Edit Survey"
				titleCreate="Create Survey"
				onOkEditClick={() => this.onOkEditClick()}
				onOkCreateClick={() => this.onOkCreateClick()}
				confirmLoading={this.state.confirmLoading}
				onCancel={() => this.onCancel()}
				editing={this.props.editing}
				creating={this.props.creating}
			>
				<SurveyForm
					creating={this.props.creating}
					wrappedComponentRef={this.saveFormRef}
					survey={this.props.editSurvey}
				/>
			</EditCreateModal>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		resetSurveyEdit: () => dispatch(resetSurveyEdit()),
		resetModal: () => dispatch(resetModal()),
		createSurvey: data => dispatch(createSurvey(data)),
		updateSurvey: (data, id) => dispatch(updateSurvey(data, id))
	};
};

const mapStateToProps = state => {
	return {
		editSurvey: state.survey.editSurvey,
		creating: state.editCreateModal.creating,
		editing: state.editCreateModal.editing
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyModalContainer);
