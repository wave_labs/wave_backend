import React from 'react';
import SurveyModalContainer from './SurveyModalContainer'
import SurveyTableFilterContainer from './SurveyTableFilterContainer'

const SurveyContent = () => {
	return (
			<div>
				<SurveyModalContainer />
				<SurveyTableFilterContainer />
			</div>
	)
}

export default SurveyContent;