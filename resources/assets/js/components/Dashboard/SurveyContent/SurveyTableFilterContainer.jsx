import React, { Component } from "react";
import {
	fetchSurveys,
	deleteSurvey,
	setSurveyEdit,
	updateSurvey
} from "redux-modules/survey/actions";
import { connect } from "react-redux";
import { Row, Col, Button, Icon, Popconfirm, Checkbox, Input } from "antd";
import RowOperation from "../Common/RowOperation";
import TablePagination from "../Common/TablePagination";
import TableFilterRow from "../Common/TableFilterRow";
import {
	startCreating,
	startEditing
} from "redux-modules/editCreateModal/actions";

class SurveyTableFilterContainer extends Component {
	constructor(props) {
		super(props);
		this.filters = {};
		this.columns = [
			{
				title: "ID",
				dataIndex: "id",
				sorter: true,
				width: 75
			},
			{
				title: "User",
				dataIndex: "userable.user.name",
			},
			{
				title: "Creature",
				dataIndex: "creature.name",
			},
			{
				title: "Dive",
				dataIndex: "dive.id",
			},
			{
				title: "Abbundance",
				dataIndex: "abundance_value",
			},
			{
				title: "Date",
				dataIndex: "date",
			},
			{
				title: "",
				key: "",
				render: (text, survey) => (
					<RowOperation
						deleteRow
						updateRow
						onUpdateClick={() => this.onUpdateClick(survey)}
						onDeleteConfirm={() =>
							this.props.deleteSurvey(survey.id)
						}
					/>
				)
			}
		];
	}

	componentDidMount = () => {
		this.props.fetchSurveys();
	};

	onUpdateClick = survey => {
		this.props.setSurveyEdit(survey);
		this.props.startEditing();
	};

	onSearch = search => {
		this.filters = { ...this.filters, search };
		this.props.fetchSurveys(1, this.filters);
	};

	handleTableChange = (pagination, filters, sorter) => {
		this.filters = {
			...this.filters,
			...filters,
			order: [sorter.field, sorter.order]
		};
		this.props.fetchSurveys(pagination.current, this.filters);
	};
	render() {
		return (
			<div>
				<TableFilterRow
					onSearch={search => this.onSearch(search)}
					searchPlaceholder="Search Surveys"
					onCreateClick={() => this.props.startCreating()}
				/>
				<TablePagination
					loading={this.props.loading}
					columns={this.columns}
					handleTableChange={this.handleTableChange}
					data={this.props.data}
					meta={this.props.meta}
				/>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		fetchSurveys: (page, filters) =>
			dispatch(fetchSurveys(page, filters)),
		deleteSurvey: id => dispatch(deleteSurvey(id)),
		activateSurvey: id => dispatch(activateSurvey(id)),
		startCreating: () => dispatch(startCreating()),
		startEditing: () => dispatch(startEditing()),
		setSurveyEdit: data => dispatch(setSurveyEdit(data)),
		updateSurvey: (id, data) => dispatch(updateSurvey(id, data))
	};
};

const mapStateToProps = state => {
	return {
		data: state.survey.data,
		meta: state.survey.meta,
		loading: state.survey.loading
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SurveyTableFilterContainer);
