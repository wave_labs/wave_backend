import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSurveys } from "redux-modules/survey/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class SurveyRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = search => {
    this.props.fetchSurveys(1, { search });
  };

  render() {
    const { data, loading, value, onChange, mode, children } = this.props;
    console.log(value);
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Survey"
        mode={mode}
      >
        {children}
        {data.map(d => (
          <Option value={d.id} key={d.id}>
            {d.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchSurveys: (page, filters) => dispatch(fetchSurveys(page, filters))
  };
};

const mapStateToProps = state => {
  return {
    data: state.survey.data,
    loading: state.survey.loading
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SurveyRemoteSelectContainer);
