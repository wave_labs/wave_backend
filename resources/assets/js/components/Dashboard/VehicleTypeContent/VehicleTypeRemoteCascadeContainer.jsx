import React, { Component } from "react";
import { connect } from "react-redux";
import { Cascader } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/vehicleType/actions";

class VehicleTypeRemoteCascadeContainer extends Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const { VehicleTypeSelector, loading } = this.props;
    let options = [];
    let children = [];

    VehicleTypeSelector.forEach((vehicleType) => {
      vehicleType.vehicles.forEach((vehicle) => {
        children.push({
          value: vehicle.id,
          label: vehicle.vehicle,
        });
      });
      options.push({
        value: vehicleType.id,
        label: vehicleType.name,
        children: children,
      });
      children = [];
    });
    console.log(options);
    return (
      <Cascader
        options={options}
        onChange={this.props.onChange}
        allowClear={true}
        expandTrigger="hover"
      />
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    VehicleTypeSelector: state.vehicleType.VehicleTypeSelector,
    loading: state.vehicleType.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VehicleTypeRemoteCascadeContainer);
