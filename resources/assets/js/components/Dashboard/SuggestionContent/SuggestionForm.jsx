import React, { Component } from "react";
import { Row, Col, Form, Input, Select, Radio } from "antd";
import SuggestionTypeRemoteSelectContainer from "../SuggestionTypeContent/SuggestionTypeRemoteSelectContainer";
import SourceRemoteSelectContainer from "../SourceContent/SourceRemoteSelectContainer";
import { sourceConverter } from "helpers";
import styled from "styled-components";

const FormItem = Form.Item;
const Option = Select.Option;
const TextArea = Input.TextArea;

const StyledRow = styled(Row)`
  width: 100%;
`;

const StyledRadioGroup = styled(Radio.Group)`
  width: 100%;
`;

const StyledRadio = styled(Radio)``;

class SuggestionForm extends Component {
  state = {};

  rules = {
    message: [
      {
        required: true,
        message: "Message is required",
      },
    ],
    source: [
      {
        required: true,
        message: "Source is required",
      },
    ],
    suggestionType: [
      {
        required: true,
        message: "Suggestion type is required",
      },
    ],
    email: [
      {
        type: "email",
        message: "This is not a valid email!",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { suggestion, editing } = this.props;
    const { suggestionType, source, userable } = suggestion;

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={8}>
            <FormItem hasFeedback label="Source">
              {getFieldDecorator("source_id", {
                initialValue: source.id,
                rules: this.rules.source,
              })(<SourceRemoteSelectContainer />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={8}>
            <FormItem hasFeedback label="Section">
              {getFieldDecorator("suggestion_type_id", {
                initialValue: suggestionType.id,
                rules: this.rules.suggestionType,
              })(<SuggestionTypeRemoteSelectContainer />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={8}>
            <FormItem hasFeedback label="Email">
              {getFieldDecorator("email", {
                initialValue: suggestion.email,
                rules: this.rules.email,
              })(<Input placeholder="Email address" />)}
            </FormItem>
          </Col>
          {editing && (
            <Col xs={24} sm={24}>
              <FormItem label="Status">
                {getFieldDecorator("status", {
                  initialValue: suggestion.status,
                  rules: this.rules.status,
                })(
                  <StyledRadioGroup>
                    <StyledRow type="flex" justify="space-around">
                      <StyledRadio value="pending">Pending</StyledRadio>
                      <StyledRadio value="denied">Denied</StyledRadio>
                      <StyledRadio value="approved">Approved</StyledRadio>
                    </StyledRow>
                  </StyledRadioGroup>
                )}
              </FormItem>
            </Col>
          )}
        </Row>
        <Row>
          <FormItem hasFeedback label="Message">
            {getFieldDecorator("suggestion", {
              initialValue: suggestion.suggestion,
              rules: this.rules.message,
            })(
              <TextArea
                placeholder="Message"
                autosize={{ minRows: 4, maxRows: 6 }}
              />
            )}
          </FormItem>
        </Row>
        <FormItem>
          {getFieldDecorator("user_id", {
            initialValue: userable.user.id,
          })(<Input hidden />)}
        </FormItem>
      </Form>
    );
  }
}

export default Form.create()(SuggestionForm);
