import React, { Component } from "react";
import {
  fetchSuggestions,
  deleteSuggestion,
  setSuggestionEdit,
  updateSuggestion,
} from "redux-modules/suggestion/actions";
import { Badge, Row } from "antd";
import { connect } from "react-redux";
import RowOperation from "../Common/RowOperation";
import TablePagination from "../Common/TablePagination";
import TableFilterRow from "../Common/TableFilterRow";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { NoDataMessage, sourceConverter } from "helpers";

const statusConverter = {
  pending: "warning",
  approved: "success",
  denied: "error",
};

class SuggestionTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "User",
        dataIndex: "userable.user.name",
      },
      {
        title: "Email",
        dataIndex: "email",
      },
      {
        title: "Category",
        dataIndex: "suggestionType",
        filters: [
          { text: "Error", value: "error" },
          { text: "Help", value: "help" },
          { text: "Collaboration", value: "collaboration" },
          { text: "Suggestion", value: "suggestion" },
          { text: "Other", value: "other" },
        ],
        filterMultiple: false,
        render: (suggestionType) => suggestionType.name,
      },
      {
        title: "Message",
        dataIndex: "suggestion",
      },
      {
        title: "Source",
        dataIndex: "source",
        filters: [
          { text: "Whale Reporter", value: "WHALE_REPORTER" },
          { text: "Dive Reporter", value: "DIVE_REPORTER" },
          { text: "Litter Reporter", value: "LITTER_REPORTER" },
          { text: "Website", value: "WEB" },
        ],
        filterMultiple: false,
        render: (source) =>
          source ? `${sourceConverter[source.name]}` : NoDataMessage(),
      },
      {
        title: "Created At",
        dataIndex: "created_at",
      },
      {
        title: "Status",
        dataIndex: "status",
        filters: [
          { text: "Pending", value: "pending" },
          { text: "Denied", value: "denied" },
          { text: "Approved", value: "approved" },
        ],
        filterMultiple: false,
        render: (status) => (
          <Row type="flex" justify="space-around" middle="center">
            <Badge status={statusConverter[status]} text={status} />
          </Row>
        ),
      },
      {
        title: "",
        key: "",
        render: (text, suggestion) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(suggestion)}
            onDeleteConfirm={() => this.props.deleteSuggestion(suggestion.id)}
          />
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.props.fetchSuggestions();
  };

  onUpdateClick = (suggestion) => {
    this.props.setSuggestionEdit(suggestion);
    this.props.startEditing();
  };

  onSearch = (search) => {
    this.filters = { ...this.filters, search };
    this.props.fetchSuggestions(1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchSuggestions(pagination.current, this.filters);
  };
  render() {
    return (
      <div>
        <TableFilterRow
          onSearch={(search) => this.onSearch(search)}
          searchPlaceholder="Search"
          onCreateClick={() => this.props.startCreating()}
        />
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSuggestions: (page, filters) =>
      dispatch(fetchSuggestions(page, filters)),
    deleteSuggestion: (id) => dispatch(deleteSuggestion(id)),
    activateSuggestion: (id) => dispatch(activateSuggestion(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setSuggestionEdit: (data) => dispatch(setSuggestionEdit(data)),
    updateSuggestion: (id, data) => dispatch(updateSuggestion(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.suggestion.data,
    meta: state.suggestion.meta,
    loading: state.suggestion.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SuggestionTableFilterContainer);
