import React from 'react';
import SuggestionModalContainer from './SuggestionModalContainer'
import SuggestionTableFilterContainer from './SuggestionTableFilterContainer'

const SuggestionContent = () => {
	return (
			<div>
				<SuggestionModalContainer />
				<SuggestionTableFilterContainer />
			</div>
	)
}

export default SuggestionContent;