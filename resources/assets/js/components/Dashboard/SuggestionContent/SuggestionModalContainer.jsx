import React, { Component } from "react";
import {
	createSuggestion,
	updateSuggestion,
	resetSuggestionEdit
} from "redux-modules/suggestion/actions";
import {
	resetModal,
	startEditing
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../Common/EditCreateModal";
import SuggestionForm from "./SuggestionForm";

class SuggestionModalContainer extends Component {
	state = {
		confirmLoading: false //modal button loading icon
	};

	onCancel = () => {
		this.resetModalForm();
	};

	onOkEditClick = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, suggestion) => {
			if (err) {
				return;
			} else {
				this.setState({ confirmLoading: true });
				this.props
					.updateSuggestion(this.props.editSuggestion.id, suggestion)
					.then(data => {
						this.resetModalForm();
					})
					.catch(e => this.setState({ confirmLoading: false }));
			}
		});
	};

	resetModalForm = () => {
		const form = this.formRef.props.form;
		this.setState({ confirmLoading: false });
		this.props.resetSuggestionEdit();
		this.props.resetModal();
		form.resetFields();
	};

	onOkCreateClick = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, suggestion) => {
			if (err) {
				return;
			} else {
				this.setState({ confirmLoading: true });

				this.props
					.createSuggestion(suggestion)
					.then(data => {
						this.resetModalForm();
					})
					.catch(e => this.setState({ confirmLoading: false }));
			}
		});
	};


	saveFormRef = formRef => {
		this.formRef = formRef;
	};

	render() {
		return (
			<EditCreateModal
				titleEdit="Edit Suggestion"
				titleCreate="Create Suggestion"
				onOkEditClick={() => this.onOkEditClick()}
				onOkCreateClick={() => this.onOkCreateClick()}
				confirmLoading={this.state.confirmLoading}
				onCancel={() => this.onCancel()}
				editing={this.props.editing}
				creating={this.props.creating}
			>
				<SuggestionForm
					creating={this.props.creating}
					editing={this.props.editing}
					wrappedComponentRef={this.saveFormRef}
					suggestion={this.props.editSuggestion}
				/>
			</EditCreateModal>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		resetSuggestionEdit: () => dispatch(resetSuggestionEdit()),
		resetModal: () => dispatch(resetModal()),
		createSuggestion: data => dispatch(createSuggestion(data)),
		updateSuggestion: (data, id) => dispatch(updateSuggestion(data, id))
	};
};

const mapStateToProps = state => {
	return {
		editSuggestion: state.suggestion.editSuggestion,
		creating: state.editCreateModal.creating,
		editing: state.editCreateModal.editing
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SuggestionModalContainer);
