import React, { Component } from "react";
import {
  resetModal,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import InsectOccurrenceModal from "./InsectOccurrenceModal";
import InsectOccurrenceForm from "./InsectOccurrenceForm";
import {
  createInsectOccurrence,
  setInsectOccurrenceCurrent,
  updateInsectOccurrence,
} from "redux-modules/insectOccurrence/actions";

class InsectReferenceModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, record) => {
      if (err) {
        return;
      } else {
        console.log(record);
        this.setState({ confirmLoading: true });
        this.props
          .updateInsectOccurrence(this.props.current.id, record)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.setInsectOccurrenceCurrent();
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, record) => {
      if (err) {
        return;
      } else {
        record.project_id = this.props.projectId;
        this.setState({ confirmLoading: true });

        this.props
          .createInsectOccurrence(record)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <InsectOccurrenceModal
        titleEdit="Edit Occurrence"
        titleCreate="Create Occurrence"
        onOkEditClick={this.onOkEditClick}
        onOkCreateClick={this.onOkCreateClick}
        confirmLoading={this.state.confirmLoading}
        onCancel={this.onCancel}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <InsectOccurrenceForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          current={this.props.current}
          projectId={this.props.projectId}
        />
      </InsectOccurrenceModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createInsectOccurrence: (data) => dispatch(createInsectOccurrence(data)),
    updateInsectOccurrence: (id, data) =>
      dispatch(updateInsectOccurrence(id, data)),
    setInsectOccurrenceCurrent: () => dispatch(setInsectOccurrenceCurrent()),
    resetModal: () => dispatch(resetModal()),
  };
};

const mapStateToProps = (state) => {
  return {
    current: state.insectOccurrence.current,
    loading: state.insectOccurrence.loading,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectReferenceModalContainer);
