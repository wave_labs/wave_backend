import React, { Component } from "react";
import { Form, Icon, Popover, Collapse, Row } from "antd";
import EventForm from "./Forms/EventForm";
import IdentificationForm from "./Forms/IdentificationForm";
import LocationForm from "./Forms/LocationForm";
import CollectionForm from "./Forms/CollectionForm";
import DatasetForm from "./Forms/DatasetForm";
import IndividualsForm from "./Forms/IndividualsForm";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

const { Panel } = Collapse;

class InsectOccurrenceForm extends Component {
  state = {};

  render() {
    const { getFieldDecorator } = this.props.form;
    const { current } = this.props;

    return (
      <Form hideRequiredMark={false}>
        <Row gutter={16}>
          <EventForm
            current={current}
            getFieldDecorator={getFieldDecorator}
            form={this.props.form}
            projectId={this.props.projectId}
          />
          <IdentificationForm
            current={current}
            getFieldDecorator={getFieldDecorator}
            form={this.props.form}
          />
        </Row>
        <Collapse>
          <Panel
            header="Expand to fill locality and geography information"
            key="1"
          >
            <LocationForm
              current={current}
              getFieldDecorator={getFieldDecorator}
              form={this.props.form}
            />
          </Panel>
        </Collapse>
        <Collapse>
          <Panel
            header="Expand to fill methods of collection information"
            key="2"
          >
            <CollectionForm
              current={current}
              getFieldDecorator={getFieldDecorator}
              form={this.props.form}
            />
          </Panel>
        </Collapse>
        <Collapse>
          <Panel
            header="Expand to fill dataset and collection information"
            key="3"
          >
            <DatasetForm
              current={current}
              getFieldDecorator={getFieldDecorator}
              form={this.props.form}
            />
          </Panel>
        </Collapse>
        <Collapse>
          <Panel
            header="Expand to fill quantities, preparations and biological associations"
            key="4"
          >
            <IndividualsForm
              current={current}
              getFieldDecorator={getFieldDecorator}
              form={this.props.form}
            />
          </Panel>
        </Collapse>
      </Form>
    );
  }
}

export default Form.create()(InsectOccurrenceForm);
