import React from "react";
import { Modal, Button } from "antd";

const InsectOccurrenceModal = (props) => {
    return (
        <Modal
            destroyOnClose
            width={"90%"}
            maskStyle={{ backgroundColor: "rgba(55,55,55,.2)" }}
            centered
            title={props.editing ? props.titleEdit : props.titleCreate}
            visible={props.editing || props.creating}
            onOk={
                props.editing
                    ? () => props.onOkEditClick()
                    : () => props.onOkCreateClick()
            }
            onCancel={() => props.onCancel()}
            confirmLoading={props.confirmLoading}
            footer={props.footer == null && props.footer}
        >
            {props.children}
        </Modal>
    );
};

export default InsectOccurrenceModal;
