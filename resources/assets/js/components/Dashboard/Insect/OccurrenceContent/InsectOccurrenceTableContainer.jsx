import React from "react";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import { setInsectOccurrenceCurrent } from "redux-modules/insectOccurrence/actions";
import StopPropagation from "../../Common/StopPropagation";
import { Icon, Menu, Tag } from "antd";
import { deleteInsectReference } from "../../../../redux-modules/insectReference/actions";
import moment from "moment";
import styled from "styled-components";

function InsectOccurrenceTableContainer({
  loading,
  handleTableChange,
  data,
  meta,
  setInsectOccurrenceCurrent,
  onUpdateClick,
  setVisible,
}) {
  const getGeography = (record) => {
    var geography = "";
    record.continent ? (location = location + record.continent + " ") : "";
    record.islandGroup ? (location = location + record.islandGroup + " ") : "";
    record.island ? (location = location + record.island + " ") : "";
    record.country ? (location = location + record.country + " ") : "";
    record.region ? (location = location + record.region + " ") : "";
    record.province ? (location = location + record.province + " ") : "";
    record.county ? (location = location + record.county) : "";
    return geography;
  };

  const getLocality = (record) => {
    var location = "";
    record.decimalLatitude
      ? (location = location + "latitude: " + record.decimalLatitude + " ")
      : "";
    record.decimalLongitude
      ? (location = location + "longitude: " + record.decimalLongitude + " ")
      : "";
    return location;
  };

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
    },
    {
      title: "Basis of Record",
      dataIndex: "basisOfRecord",
    },
    {
      title: "Specimen",
      render: (text, row) => (
        <p key={row.id} style={{ margin: 0 }}>
          {row.specie ? row.specie.scientificName : ""}
          {row.subspecie ? row.subspecie.scientificName : ""}
        </p>
      ),
    },
    {
      title: "Previous Identifications",
      render: (text, row) =>
        row.previousIdentifications.map((record) => (
          <p key={row.id} style={{ margin: 0 }}>
            {record.species ? record.species.scientificName : ""}
            {record.subspecies ? record.subspecies.scientificName : ""}
            {record.dateIdentified
              ? ", identified at " + record.dateIdentified
              : ""}
            {record.author.length != 0 ? " by: " + record.author[0] : ""}
          </p>
        )),
    },
    /*
    {
      title: "Coordinates",
      render: (text, row) => (
        <p key={row.id} style={{ margin: 0 }}>
          {getLocality(row)}
        </p>
      ),
    },
    {
      title: "Specie",
      render: (text, row) => (
        <p key={row.id} style={{ margin: 0 }}>
          {row.specie ? row.specie.specific : ""}
          {row.subspecie ? row.subspecie.infraSpecific : ""}
        </p>
      ),
    }, */
    {
      title: "",
      key: "",
      render: (text, record) => (
        <StopPropagation>
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => onUpdateClick(record)}
          ></RowOperation>
        </StopPropagation>
      ),
    },
  ];

  return (
    <div>
      <TablePagination
        loading={loading}
        columns={columns}
        handleTableChange={handleTableChange}
        data={data}
        meta={meta}
        onRow={(record) => ({
          onClick: () => {
            setInsectOccurrenceCurrent(record);
            setVisible(true);
          },
        })}
      />
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    setInsectOccurrenceCurrent: (record) =>
      dispatch(setInsectOccurrenceCurrent(record)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.insectOccurrence.data,
    meta: state.insectOccurrence.meta,
    loading: state.insectOccurrence.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectOccurrenceTableContainer);
