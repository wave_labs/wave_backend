import { insectImportOccurrences } from "redux-modules/insectOccurrence/actions";
import { Upload, Modal, message, Progress, Button, Icon } from "antd";
import React, { useState } from "react";
import { connect } from "react-redux";
import styled from "styled-components";

const { Dragger } = Upload;

message.config({
  maxCount: 1,
});

const Container = styled.section`
  margin-bottom: 50px;
`;

const RoundButton = styled(Button)`
  margin: auto;
  width: 70px;
  height: 70px;
`;

function InsectOccurrenceImportModal({
  insectImportOccurrences,
  visible,
  setVisible,
  projectId,
}) {
  const [hasUploaded, setHasUploaded] = useState(0);
  const [created, setCreated] = useState(false);
  const [form, setForm] = useState({ file: undefined });
  const UploadProps = {
    name: "excel_file",
    multiple: false,
    showUploadList: true,
    action: `${window.location.origin}/api/success`,
    accept: ".xlsx",
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        console.log(info.file.percent);
      }
      if (status === "done") {
        setHasUploaded(1);
        message.success(`${info.file.name} is being uploaded to the database.`);
        handleCreate();
      } else if (status === "error") {
        console.log(info);
        setHasUploaded(2);
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    beforeUpload(currentFile) {
      setForm({ ...form, file: currentFile });
      return true;
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
  };

  const handleCreate = () => {
    console.log(form.file);
    var formData = new FormData();
    formData.append("excel_file", form.file);
    formData.append("projectId", projectId);

    insectImportOccurrences(formData)
      .then((data) => {
        message.success("Finished importing file");
        setCreated(true);
      })
      .catch((e) => {
        message.error(e);
      });
  };

  const onCancel = () => {
    setVisible(!visible);
    setHasUploaded(0);
    setCreated(false);
  };

  return (
    <Modal
      destroyOnClose
      width={"90%"}
      style={{ maxWidth: "900px", color: "black" }}
      maskStyle={{ backgroundColor: "rgba(55,55,55,.2)" }}
      centered
      title={"Upload occurrences"}
      visible={visible}
      footer={null}
      onCancel={() => onCancel()}
    >
      <p>
        The format that the occurrence file should have for import is the
        following
      </p>
      <a href="/public/download/NomenclatureFileFormat.xlsx" download>
        Click to download
      </a>
      <Dragger maxCount={1} style={{ width: "100%" }} {...UploadProps}>
        {hasUploaded ? (
          created ? (
            <>
              <Progress
                status={"success"}
                style={{ marginBottom: "20px" }}
                type="circle"
                percent={100}
              />
              <p className="ant-upload-text">File uploaded successfully</p>
            </>
          ) : (
            <>
              <Progress
                status={"active"}
                style={{ marginBottom: "20px" }}
                type="circle"
                percent={99}
              />
              <p className="ant-upload-text">Transfering file to database</p>
            </>
          )
        ) : (
          <>
            <p className="ant-upload-drag-icon">
              <Icon type="upload" />
            </p>
            <p className="ant-upload-text">Select an Excel file to upload</p>
            <p className="ant-upload-hint">or drag and drop it here</p>
          </>
        )}
      </Dragger>
    </Modal>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    insectImportOccurrences: (data) => dispatch(insectImportOccurrences(data)),
  };
};
const mapStateToProps = (state) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectOccurrenceImportModal);
