import React, { useState, useEffect } from "react";
import {
  startEditing,
  startCreating,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import styled from "styled-components";
import { Col, Row } from "antd";
import moment from "moment";
import InsectOccurrenceFilters from "./InsectOccurrenceFilters";
import InsectOccurrenceModalContainer from "./InsectOccurrenceModalContainer";
import InsectOccurrenceTableContainer from "./InsectOccurrenceTableContainer";
import {
  setInsectOccurrenceCurrent,
  fetchInsectOccurrences,
} from "redux-modules/insectOccurrence/actions";

const Container = styled.section`
  margin-top: 20px;
  /* display: flex;
    justify-content: space-between; */
`;

function InsectOccurrenceContent({
  setInsectOccurrenceCurrent,
  fetchInsectOccurrences,
  current,
  startEditing,
  projectId,
}) {
  const [visible, setVisible] = useState(false);
  const [filters, setFilters] = useState({
    search: undefined,
    project: projectId,
  });

  const handleFilters = (aFilters) => {
    setFilters({ ...filters, ...aFilters });
  };

  const handlePageChange = (pagination) => {
    fetchInsectOccurrences(pagination.current, filters);
  };

  useEffect(() => {
    fetchInsectOccurrences(1, filters);
  }, [filters]);

  const handleUpdate = (record) => {
    setInsectOccurrenceCurrent(record);
    startEditing();
  };

  return (
    <Container>
      <InsectOccurrenceModalContainer projectId={projectId} />
      <InsectOccurrenceFilters
        filters={filters}
        handleFilterChange={handleFilters}
        projectId={projectId}
      />
      <Row>
        <Col xs={24} lg={24}>
          <InsectOccurrenceTableContainer
            setVisible={setVisible}
            onUpdateClick={handleUpdate}
            handleTableChange={handlePageChange}
          />
        </Col>
      </Row>
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectOccurrences: (page, filters) =>
      dispatch(fetchInsectOccurrences(page, filters)),
    setInsectOccurrenceCurrent: (record) =>
      dispatch(setInsectOccurrenceCurrent(record)),
    startEditing: () => dispatch(startEditing()),
    startCreating: () => dispatch(startCreating()),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.insectOccurrence.data,
    current: state.insectOccurrence.current,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectOccurrenceContent);
