import { Col, Form, Input, Row, Popover, Icon } from "antd";
import React from "react";
import { connect } from "react-redux";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function IndividualsForm(props) {
  const { current, getFieldDecorator, form } = props;

  return (
    <Row gutter={16}>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Individual Count" content={""} />
        <Form.Item>
          {getFieldDecorator("individualCount", {
            initialValue: current.individualCount,
          })(<Input placeholder="Individual Count" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Male Count" content={""} />
        <Form.Item>
          {getFieldDecorator("maleCount", {
            initialValue: current.maleCount,
          })(<Input placeholder="Male Count" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Female Count" content={""} />
        <Form.Item>
          {getFieldDecorator("femaleCount", {
            initialValue: current.femaleCount,
          })(<Input placeholder="Female Count" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Imature Count" content={""} />
        <Form.Item>
          {getFieldDecorator("imatureCount", {
            initialValue: current.imatureCount,
          })(<Input placeholder="Imature Count" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Exuvias Count" content={""} />
        <Form.Item>
          {getFieldDecorator("exuvias", {
            initialValue: current.exuvias,
          })(<Input placeholder="Exuvias Count" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Organism Quantity" content={""} />
        <Form.Item>
          {getFieldDecorator("organismQuantity", {
            initialValue: current.organismQuantity,
          })(<Input placeholder="Organism Quantity" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Organism Quantity Type" content={""} />
        <Form.Item>
          {getFieldDecorator("organismQuantityType", {
            initialValue: current.organismQuantityType,
          })(<Input placeholder="Organism Quantity Type" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Establishment Means" content={""} />
        <Form.Item>
          {getFieldDecorator("establishmentMeans", {
            initialValue: current.establishmentMeans,
          })(<Input placeholder="Establishment Means" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Preparations" content={""} />
        <Form.Item>
          {getFieldDecorator("preparations", {
            initialValue: current.preparations,
          })(<Input placeholder="Preparations" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Preparation Type" content={""} />
        <Form.Item>
          {getFieldDecorator("preparationType", {
            initialValue: current.preparationType,
          })(<Input placeholder="Preparation Type" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Prepared by" content={""} />
        <Form.Item>
          {getFieldDecorator("preparedBy", {
            initialValue: current.preparedBy,
          })(<Input placeholder="Prepared by" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Preparation date" content={""} />
        <Form.Item>
          {getFieldDecorator("preparationDate", {
            initialValue: current.preparationDate,
          })(<Input placeholder="Preparation date" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Preparation remarks" content={""} />
        <Form.Item>
          {getFieldDecorator("preparationRemarks", {
            initialValue: current.preparationRemarks,
          })(<Input placeholder="Preparation remarks" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Habitat" content={""} />
        <Form.Item>
          {getFieldDecorator("habitat", {
            initialValue: current.habitat,
          })(<Input placeholder="Habitat" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Habitat Remarks" content={""} />
        <Form.Item>
          {getFieldDecorator("habitatRemarks", {
            initialValue: current.habitatRemarks,
          })(<Input placeholder="Habitat Remarks" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Microhabitats" content={""} />
        <Form.Item>
          {getFieldDecorator("microHabitats", {
            initialValue: current.microHabitats,
          })(<Input placeholder="Microhabitats" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Biological Associations" content={""} />
        <Form.Item>
          {getFieldDecorator("biologicalAssociation", {
            initialValue: current.biologicalAssociation,
          })(<Input placeholder="Biological Associations" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Biological Remarks" content={""} />
        <Form.Item>
          {getFieldDecorator("biologicalRemarks", {
            initialValue: current.biologicalRemarks,
          })(<Input placeholder="Biological Remarks" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Sampling Method" content={""} />
        <Form.Item>
          {getFieldDecorator("samplingMethod", {
            initialValue: current.samplingMethod,
          })(<Input placeholder="Sampling Method" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Type of Sampling" content={""} />
        <Form.Item>
          {getFieldDecorator("typeOfSampling", {
            initialValue: current.typeOfSampling,
          })(<Input placeholder="Type of Sampling" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Trip Code" content={""} />
        <Form.Item>
          {getFieldDecorator("tripCode", {
            initialValue: current.tripCode,
          })(<Input placeholder="Trip Code" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Data Attributes" content={""} />
        <Form.Item>
          {getFieldDecorator("dataAttributes", {
            initialValue: current.dataAttributes,
          })(<Input placeholder="Data Attributes" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Material Type" content={""} />
        <Form.Item>
          {getFieldDecorator("materialType", {
            initialValue: current.materialType,
          })(<Input placeholder="Material Type" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Catalogued By" content={""} />
        <Form.Item>
          {getFieldDecorator("cataloguedBy", {
            initialValue: current.cataloguedBy,
          })(<Input placeholder="Catalogued By" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Data Validation" content={""} />
        <Form.Item>
          {getFieldDecorator("dataValidation", {
            initialValue: current.dataValidation,
          })(<Input placeholder="Data Validation" />)}
        </Form.Item>
      </Col>
    </Row>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {};
};

const mapStateToProps = (state) => {};

export default connect(mapStateToProps, mapDispatchToProps)(IndividualsForm);
