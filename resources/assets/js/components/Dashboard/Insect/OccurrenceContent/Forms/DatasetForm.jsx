import { Col, Form, Input, Row, Popover, Icon, Select } from "antd";
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useCallback } from "react";
import { fetchReferenceAuthorsSelector } from "redux-modules/insectReference/actions";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function DatasetForm(props) {
  const {
    current,
    getFieldDecorator,
    form,
    fetchReferenceAuthorsSelector,
    authors,
  } = props;

  const [filters, setFilters] = useState({ search: "" });

  const onSearch = useCallback(
    _.debounce((search) => onSearchChange(search), 500),
    []
  );

  const onSearchChange = (search) => {
    setFilters({
      search: search,
    });
  };
  useEffect(() => {
    fetchReferenceAuthorsSelector(filters);
  }, [filters]);

  const handleChange = () => {
    setFilters({
      search: "",
    });
  };

  return (
    <Row gutter={16}>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Language" content={""} />
        <Form.Item>
          {getFieldDecorator("language", {
            initialValue: current.language,
          })(<Input placeholder="Language" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="License" content={""} />
        <Form.Item>
          {getFieldDecorator("license", {
            initialValue: current.license,
          })(<Input placeholder="License" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Instituion ID" content={""} />
        <Form.Item>
          {getFieldDecorator("institutionId", {
            initialValue: current.institutionId,
          })(<Input placeholder="Instituion ID" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Instituion Code" content={""} />
        <Form.Item>
          {getFieldDecorator("institutionCode", {
            initialValue: current.institutionCode,
          })(<Input placeholder="Instituion Code" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Collection Code" content={""} />
        <Form.Item>
          {getFieldDecorator("collectionCode", {
            initialValue: current.collectionCode,
          })(<Input placeholder="Collection Code" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Catalog Number" content={""} />
        <Form.Item>
          {getFieldDecorator("catalogNumber", {
            initialValue: current.catalogNumber,
          })(<Input placeholder="Catalog Number" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Recorded By" content={""} />
        <Form.Item>
          {getFieldDecorator("recordedBy", {
            initialValue: current.recordedBy,
          })(
            <Select
              mode="tags"
              onSearch={onSearch}
              style={{ width: "100%" }}
              placeholder="Authors"
              onChange={handleChange}
            >
              {authors.map((author) => (
                <Select.Option value={author.name} key={author.id}>
                  {author.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
      </Col>
    </Row>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchReferenceAuthorsSelector: (filters) =>
      dispatch(fetchReferenceAuthorsSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return { authors: state.insectReference.selectorAuthors };
};

export default connect(mapStateToProps, mapDispatchToProps)(DatasetForm);
