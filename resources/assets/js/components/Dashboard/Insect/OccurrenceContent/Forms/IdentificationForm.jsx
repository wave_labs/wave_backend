import {
  Col,
  Form,
  Input,
  Row,
  Select,
  DatePicker,
  Popover,
  Icon,
  Collapse,
} from "antd";
import moment from "moment";
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useCallback } from "react";
import { fetchReferenceAuthorsSelector } from "redux-modules/insectReference/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

const { Panel } = Collapse;

function IdentificationForm(props) {
  const {
    current,
    getFieldDecorator,
    form,
    fetchReferenceAuthorsSelector,
    authors,
  } = props;
  const [filters, setFilters] = useState({ search: "" });
  const [identifiedAuthor, setIdentifiedAuthor] = useState();

  useEffect(() => {
    if (current.identifiedBy) setIdentifiedAuthor(current.identifiedBy);
  }, [current]);

  const onSearch = useCallback(
    _.debounce((search) => onSearchChange(search), 500),
    []
  );

  const onSearchChange = (search) => {
    setFilters({
      search: search,
    });
  };
  useEffect(() => {
    fetchReferenceAuthorsSelector(filters);
  }, [filters]);

  useEffect(() => {
    form.setFieldsValue({ identifiedBy: identifiedAuthor });
  }, [identifiedAuthor]);

  const handleChange = (value) => {
    if (value.length > 1) {
      setIdentifiedAuthor(value[1]);
    } else {
      setIdentifiedAuthor(value[0]);
    }
    setFilters({
      search: "",
    });
  };

  const handleDateChange = (value) => {
    form.setFieldsValue({ dateIdentified: value });
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Identified By" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("identifiedBy", {
            initialValue: current.identifiedBy,
          })(
            <SelectSearch
              mode="tags"
              onSearch={onSearch}
              style={{ width: "100%" }}
              placeholder="Authors"
              onChange={handleChange}
            >
              {authors.map((author) => (
                <Select.Option value={author.name} key={author.id}>
                  {author.name}
                </Select.Option>
              ))}
            </SelectSearch>
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications
          label="Level of experience of the author"
          content={"Education level and which is the specialty of the author"}
        />
        <Form.Item hasFeedback>
          {getFieldDecorator("authorExperience", {
            initialValue: current.authorExperience,
          })(<Input placeholder="Level of experience of the author" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Identification Date" content={""} />
        <Form.Item>
          {getFieldDecorator("dateIdentified", {
            initialValue: current.dateIdentified
              ? moment(current.dateIdentified)
              : null,
          })(
            <DatePicker
              format={"YYYY-MM-DD"}
              onPanelChange={handleDateChange}
              placeholder="Identification Date"
            />
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Identification Remarks" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("identificationRemarks", {
            initialValue: current.identificationRemarks,
          })(<Input placeholder="Identification Remarks" />)}
        </Form.Item>
      </Col>
      {/* <Col xs={24} md={12} lg={8}>
        <Specifications label="Previous Identifications" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("previousIdentifications", {
            initialValue: current.previousIdentifications,
          })(<Input placeholder="Previous Identifications" />)}
        </Form.Item>
      </Col> */}
    </>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchReferenceAuthorsSelector: (filters) =>
      dispatch(fetchReferenceAuthorsSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return { authors: state.insectReference.selectorAuthors };
};

export default connect(mapStateToProps, mapDispatchToProps)(IdentificationForm);
