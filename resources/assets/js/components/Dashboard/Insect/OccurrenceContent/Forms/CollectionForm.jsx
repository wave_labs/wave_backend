import { Col, Form, Input, Row, Select, Popover, Icon } from "antd";
import React from "react";
import { connect } from "react-redux";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function CollectionForm(props) {
  const { current, getFieldDecorator, form } = props;

  return (
    <Row gutter={16}>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Type" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("type", {
            initialValue: current.type,
          })(
            <Select placeholder="Type">
              <Select.Option key="physicalObject" value="physicalObject">
                Physical Object
              </Select.Option>
              <Select.Option key="text" value="text">
                Text
              </Select.Option>
              <Select.Option key="sound" value="sound">
                Sound
              </Select.Option>
              <Select.Option key="photo" value="photo">
                Photo
              </Select.Option>
              <Select.Option key="video" value="video">
                Video
              </Select.Option>
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Numbers" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("numbers", {
            initialValue: current.numbers,
          })(<Input placeholder="Numbers" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Record number" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("recordNumber", {
            initialValue: current.recordNumber,
          })(<Input placeholder="Record number" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Machine Specifics" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("machineSpecifics", {
            initialValue: current.machineSpecifics,
          })(<Input placeholder="Machine Specifics" />)}
        </Form.Item>
      </Col>
    </Row>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {};
};

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(CollectionForm);
