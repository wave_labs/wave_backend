import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import {
  fetchInsectSpeciesSelector,
  fetchInsectSubspeciesSelector,
} from "redux-modules/insectOccurrence/actions";
import React, { useState } from "react";
import { useEffect } from "react";
import { connect } from "react-redux";
import { useCallback } from "react";
import SelectSearch from "components/Dashboard/Common/SelectSearch";
import InsectReferenceRemoteSelectContainer from "../../FamilyContent/InsectReferenceRemoteSelectContainer";

const { RangePicker, MonthPicker } = DatePicker;

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function EventForm(props) {
  const {
    current,
    getFieldDecorator,
    form,
    fetchInsectSpeciesSelector,
    fetchInsectSubspeciesSelector,
    taxonSelector,
    projectId,
  } = props;
  const [datepickerMode, setDatepickerMode] = useState("day");
  const [taxonLevel, setTaxonLevel] = useState("specie");
  const [filters, setFilters] = useState({ search: "" });

  useEffect(() => {
    setDatepickerMode(current.dateType);
    handleCurrentDate();
  }, []);

  useEffect(() => {
    if (current.specie) {
      setTaxonLevel("specie");
    }
    if (current.subspecie) {
      setTaxonLevel("subspecie");
    }
  }, [current]);

  const onSearch = useCallback(
    _.debounce((search) => onSearchChange(search), 500),
    []
  );

  const onSearchChange = (search) => {
    setFilters({
      search: search,
    });
  };

  useEffect(() => {
    if (taxonLevel == "specie") {
      fetchInsectSpeciesSelector(filters);
    } else {
      fetchInsectSubspeciesSelector(filters);
    }
  }, [taxonLevel, filters]);

  const handleChange = () => {
    setFilters({
      search: "",
    });
  };

  const dateFormat = {
    day: "YYYY-MM-DD",
    month: "YYYY-MM",
    year: "YYYY",
  };

  const handleDateTypeChange = (value) => {
    form.setFieldsValue({ eventDate: undefined });
    setDatepickerMode(value);
  };

  const handleDateChange = (value) => {
    form.setFieldsValue({ eventDate: value });
  };

  const handleTaxonLevel = (value) => {
    setTaxonLevel(value);
  };

  /* useEffect(() => {
    if (taxonLevel == "specie") {
      fetchInsectSpeciesSelector(filters);
    } else {
      fetchInsectSubspeciesSelector(filters);
    }
  }, [taxonLevel]); */

  const handleCurrentDate = () => {
    if (current.dateType == "range" && current.eventDate) {
      form.setFieldsValue({
        eventDate: [moment(current.eventDate[0]), moment(current.eventDate[1])],
      });
    }
    if (current.dateType != "range" && current.eventDate) {
      form.setFieldsValue({
        eventDate: moment(current.eventDate[0]),
      });
    }
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications
          label="Basis of Record"
          content={
            "The specific nature of the data record. Recommended best practice is to use this controlled vocabulary: http://rs.gbif.org/vocabulary/dwc/basis_of_record.xml"
          }
        />
        <Form.Item hasFeedback>
          {getFieldDecorator("basisOfRecord", {
            initialValue: current.basisOfRecord,
          })(
            <Select placeholder="Basis of Record">
              <Select.Option key="preservedSpecimen" value="preservedSpecimen">
                Preserved Specimen
              </Select.Option>
              <Select.Option key="fossilSpecimen" value="fossilSpecimen">
                Fossil Specimen
              </Select.Option>
              <Select.Option key="livingSpecimen" value="livingSpecimen">
                Living Specimen
              </Select.Option>
              <Select.Option key="materialSample" value="materialSample">
                Material Sample
              </Select.Option>
              <Select.Option key="humanObservation" value="humanObservation">
                Human Observation
              </Select.Option>
              <Select.Option
                key="machineObservation"
                value="machineObservation"
              >
                Machine Observation
              </Select.Option>
              <Select.Option key="materialCitation" value="materialCitation">
                Material Citation
              </Select.Option>
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Date Type" content={""} />
        <Form.Item>
          {getFieldDecorator("dateType", {
            initialValue: current.dateType ? current.dateType : "day",
          })(
            <Select
              placeholder="Date type"
              onChange={handleDateTypeChange}
              style={{ width: "100%" }}
            >
              <Select.Option value="range">Range</Select.Option>
              <Select.Option value="day">Day</Select.Option>
              <Select.Option value="month">Month</Select.Option>
              <Select.Option value="year">Year</Select.Option>
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Event Date" content={""} />
        <Form.Item>
          {getFieldDecorator("eventDate", {
            initialValue: null,
          })(
            datepickerMode == "range" ? (
              <RangePicker mode={datepickerMode} placeholder="Event Date" />
            ) : (
              <DatePicker
                format={dateFormat[datepickerMode]}
                onPanelChange={handleDateChange}
                mode={datepickerMode}
                placeholder="Event Date"
              />
            )
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="End Day of Year" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("endDayOfYear", {
            initialValue: current.endDayOfYear,
          })(<Input placeholder="End Day of Year" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Verbatim Event Date" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("verbatimEventDate", {
            initialValue: current.verbatimEventDate,
          })(<Input placeholder="Verbatim Event Date" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Event Remarks" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("eventRemarks", {
            initialValue: current.eventRemarks,
          })(<Input placeholder="Event Remarks" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={24} lg={8}>
        <Specifications label="Reference" content={""} />
        <Form.Item>
          {getFieldDecorator("reference_id", {
            initialValue: current.reference_id,
          })(<InsectReferenceRemoteSelectContainer projectId={projectId} />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Occurrence Remarks" content={""} />
        <Form.Item>
          {getFieldDecorator("occurrenceRemarks", {
            initialValue: current.occurrenceRemarks,
          })(<Input placeholder="Occurrence Remarks" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Taxon level" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("taxonLevel", {
            initialValue: taxonLevel,
          })(
            <Select placeholder="Taxon Level" onSelect={handleTaxonLevel}>
              <Select.Option key="specie" value="specie">
                Specie
              </Select.Option>
              <Select.Option key="subspecie" value="subspecie">
                Subspecie
              </Select.Option>
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Specimen" content={""} />
        <Form.Item>
          {getFieldDecorator(
            taxonLevel == "specie" ? "specific_id" : "infra_specific_id",
            {
              initialValue: current.specie
                ? current.specie.id
                : current.subspecie
                ? current.subspecie.id
                : null,
            }
          )(
            taxonLevel == "specie" ? (
              <SelectSearch
                placeholder="Specie"
                onSearch={onSearch}
                onChange={handleChange}
              >
                {taxonSelector.map((option) => (
                  <Select.Option key={option.id} value={option.id}>
                    {option.scientificName}
                  </Select.Option>
                ))}
              </SelectSearch>
            ) : (
              <SelectSearch
                placeholder="Subspecie"
                onSearch={onSearch}
                onChange={handleChange}
              >
                {taxonSelector.map((option) => (
                  <Select.Option key={option.id} value={option.id}>
                    {option.scientificName}
                  </Select.Option>
                ))}
              </SelectSearch>
            )
          )}
        </Form.Item>
      </Col>
    </>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectSpeciesSelector: (filters) =>
      dispatch(fetchInsectSpeciesSelector(filters)),
    fetchInsectSubspeciesSelector: (filters) =>
      dispatch(fetchInsectSubspeciesSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    taxonSelector: state.insectOccurrence.selector,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventForm);
