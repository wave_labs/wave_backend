import {
  Col,
  Form,
  Input,
  Row,
  DatePicker,
  Popover,
  Icon,
  Collapse,
} from "antd";
import moment from "moment";
import React from "react";
import { connect } from "react-redux";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

const { Panel } = Collapse;

function EventForm(props) {
  const { current, getFieldDecorator, form } = props;

  return (
    <Row gutter={16}>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Latitude" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("decimalLatitude", {
            initialValue: current.decimalLatitude,
          })(<Input placeholder="Latitude" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Longitude" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("decimalLongitude", {
            initialValue: current.decimalLongitude,
          })(<Input placeholder="Longitude" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Geodetic Datum" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("geodeticDatum", {
            initialValue: current.geodeticDatum,
          })(<Input placeholder="Geodetic Datum" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Coordinate Uncertainty (m)" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("coordinateUncertaintyInMeters", {
            initialValue: current.coordinateUncertaintyInMeters,
          })(<Input placeholder="Coordinate Uncertainty (m)" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Verbatim Coordinates" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("verbatimCoordinates", {
            initialValue: current.verbatimCoordinates,
          })(<Input placeholder="Verbatim Coordinates" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Verbatim Coordinate System" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("verbatimCoordinateSystem", {
            initialValue: current.verbatimCoordinateSystem,
          })(<Input placeholder="Verbatim Coordinate System" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Georeferenced By" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("georeferencedBy", {
            initialValue: current.georeferencedBy,
          })(<Input placeholder="Georeferenced By" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Georeferenced Date" content={""} />
        <Form.Item>
          {getFieldDecorator("georeferencedDate", {
            initialValue: current.georeferencedDate ? moment(current.georeferencedDate) : null,
          })(
            <DatePicker
              format={"YYYY-MM-DD"}
              /* onPanelChange={handleDateChange} */
              placeholder="Georeferenced Date"
            />
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Georeference Protocol" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("georeferenceProtocol", {
            initialValue: current.georeferenceProtocol,
          })(<Input placeholder="Georeference Protocol" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Georeference Sources" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("georeferenceSources", {
            initialValue: current.georeferenceSources,
          })(<Input placeholder="Georeference Sources" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Georeference Remarks" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("georeferenceRemarks", {
            initialValue: current.georeferenceRemarks,
          })(<Input placeholder="Georeference Remarks" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Georeference Verification Status" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("georeferenceVerificationStatus", {
            initialValue: current.georeferenceVerificationStatus,
          })(<Input placeholder="Georeference Verification Status" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Higher Geography" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("higherGeography", {
            initialValue: current.higherGeography,
          })(<Input placeholder="Higher Geography" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Continent" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("continent", {
            initialValue: current.continent,
          })(<Input placeholder="Continent" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Island Group" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("islandGroup", {
            initialValue: current.islandGroup,
          })(<Input placeholder="Island Group" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Island" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("island", {
            initialValue: current.island,
          })(<Input placeholder="Island" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Biogeographic Realm" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("biogeographicRealm", {
            initialValue: current.biogeographicRealm,
          })(<Input placeholder="Biogeographic Realm" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Country" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("country", {
            initialValue: current.country,
          })(<Input placeholder="Country" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Country Code" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("countryCode", {
            initialValue: current.countryCode,
          })(<Input placeholder="Country Code" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Region" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("region", {
            initialValue: current.region,
          })(<Input placeholder="Region" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Province" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("province", {
            initialValue: current.province,
          })(<Input placeholder="Province" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="County" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("county", {
            initialValue: current.county,
          })(<Input placeholder="County" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Geographic Remarks" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("geographicRemarks", {
            initialValue: current.geographicRemarks,
          })(<Input placeholder="Geographic Remarks" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Locality" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("locality", {
            initialValue: current.locality,
          })(<Input placeholder="Locality" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Verbatim Locality" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("verbatimLocality", {
            initialValue: current.verbatimLocality,
          })(<Input placeholder="Verbatim Locality" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Location According To" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("locationAccordingTo", {
            initialValue: current.locationAccordingTo,
          })(<Input placeholder="Location According To" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Locality Remarks" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("localityRemarks", {
            initialValue: current.localityRemarks,
          })(<Input placeholder="Locality Remarks" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Elevation Minimum" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("elevationMin", {
            initialValue: current.elevationMin,
          })(<Input placeholder="Elevation Minimum" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Elevation Maximum" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("elevationMax", {
            initialValue: current.elevationMax,
          })(<Input placeholder="Elevation Maximum" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Elevation Method" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("elevationMethod", {
            initialValue: current.elevationMethod,
          })(<Input placeholder="Elevation Method" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Elevation Accuracy" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("elevationAccuracy", {
            initialValue: current.elevationAccuracy,
          })(<Input placeholder="Elevation Accuracy" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Verbatim Elevation" content={""} />
        <Form.Item hasFeedback>
          {getFieldDecorator("verbatimElevation", {
            initialValue: current.verbatimElevation,
          })(<Input placeholder="Verbatim Elevation" />)}
        </Form.Item>
      </Col>
    </Row>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {};
};

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(EventForm);
