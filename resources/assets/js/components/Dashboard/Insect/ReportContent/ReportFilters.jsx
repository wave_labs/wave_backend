import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Row, message } from "antd";
import styled from "styled-components";
import moment from "moment";
import { startCreating } from "redux-modules/editCreateModal/actions";
import FilterRow from "../../Common/FilterRow";
import ExportMessage from "../../Common/ExportMessage";

message.config({
    maxCount: 1,
});

const Container = styled.section`
    margin-bottom: 50px;
`;



function ReportFilters({ handleFilterChange, filters, isAdmin, startCreating }) {
    const [operations, setOperations] = useState(["reset"]);
    const [messageStatus, setMessageStatus] = useState(null);

    useEffect(() => {
        if (isAdmin) {
            setOperations(["reset", "create"]);
        }
    }, [])


    const handleReset = () => {
        handleFilterChange({
            search: undefined,
            date: undefined,
        });
    };

    const handleFilters = (field, value) => {
        var object = {};

        object[field] = value;
        handleFilterChange(object);


    };

    return (
        <Container>
            <ExportMessage message={messageStatus} />
            <FilterRow
                filters={["search"]}
                filterValues={filters}
                operations={operations}
                handleFilterChange={handleFilters}
                handleCreate={startCreating}
                handleReset={handleReset}
            />
        </Container>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchInsectReports: (page, filters) => dispatch(fetchInsectReports(page, filters)),
        startCreating: () => dispatch(startCreating()),
        // exportReport: (filters) => dispatch(exportReport(filters)),
    };
};
const mapStateToProps = (state) => {
    return {
        isAdmin: state.auth.isAdmin,
        loadingExport: state.litter.loadingExport,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ReportFilters);
