import React, { useState, useEffect } from "react";
import { fetchInsectReports, setInsectReportCurrent } from "redux-modules/insectReport/actions"
import {
    startEditing
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import ReportTableContainer from "./ReportTableContainer";
import ReportMap from "./ReportMap";
import ReportDrawer from "./ReportDrawer";
import styled from "styled-components";
import { Col, Row } from "antd";
import ReportFilters from "./ReportFilters";
import moment from "moment";
import InsectReportModalContainer from "./InsectReportModalContainer";
import { fetchInsectReportsSelector } from "../../../../redux-modules/insectReport/actions";

const Container = styled.section`
    /* display: flex;
    justify-content: space-between; */
`;

function ReportContent({ fetchInsectReports, fetchInsectReportsSelector, setInsectReportCurrent, current, startEditing }) {
    const [filters, setFilters] = useState({});
    const [visible, setVisible] = useState(false);

    const handleFilters = (aFilters) => {
        setFilters({ ...filters, ...aFilters });

    }

    useEffect(() => {
        fetchInsectReportsSelector()
    }, [])


    useEffect(() => {
        var newFilters = { ...filters };

        if (newFilters.date) {
            newFilters.date = [
                moment(newFilters.date[0]).format("YYYY-MM-DD"),
                moment(newFilters.date[1]).format("YYYY-MM-DD"),
            ]
        }

        fetchInsectReports(1, newFilters);
    }, [filters])

    const handlePageChange = (pagination) => {
        fetchInsectReports(pagination.current, filters);
    }

    const handleDrawerClose = () => {
        setVisible(false);
        setInsectReportCurrent();
    }

    const handleUpdate = (record) => {
        setInsectReportCurrent(record);
        startEditing();
    }

    return (
        <Container>
            <ReportDrawer current={current} visible={visible} handleDrawerClose={handleDrawerClose} />
            <InsectReportModalContainer />
            <ReportFilters filters={filters} handleFilterChange={handleFilters} />
            <Row type="flex" gutter={24}>
                <Col xs={24} lg={12}>
                    <ReportMap viewMore />
                </Col>
                <Col xs={24} lg={12}>
                    <ReportTableContainer setVisible={setVisible} onUpdateClick={handleUpdate} handleTableChange={handlePageChange} />
                </Col>
            </Row>
        </Container>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchInsectReports: (page, filters) => dispatch(fetchInsectReports(page, filters)),
        setInsectReportCurrent: (record) => dispatch(setInsectReportCurrent(record)),
        startEditing: () => dispatch(startEditing()),
        fetchInsectReportsSelector: () => dispatch(fetchInsectReportsSelector()),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.insectReport.data,
        current: state.insectReport.current,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ReportContent);
