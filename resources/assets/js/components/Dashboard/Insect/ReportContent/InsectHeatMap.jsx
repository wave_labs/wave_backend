import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Layer, Source, Marker} from "react-map-gl";
import MapBoxMap from "./Common/MapBoxMap";

const StyledMap = styled.div`
  width: 96%;
  margin: auto;
  -webkit-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 28px -11px rgba(0, 0, 0, 0.75);
  margin-bottom: 5%;
  min-height: 600px;
`;

const StyledSubtitle = styled.div`
  font-size: 36px;
  text-align: center;
  margin-bottom: 1%;
  font-weight: 700;
  line-height: 1.3;

  @media screen and (max-width: 1000px) {
    font-size: 24px;
  }
`;

export default function InsectHeatmap() {

  const [coords, setCoords] = useState([]);
  const [size, setSize] = useState(1);

  useEffect(() => {
    //setCoords([32.4637, -16.7492]);
    setCoords([
      [32.4637, -16.7492],
      [32.4637, -16.8492],
      [32.4637, -16.9492],
      [32.4637, -17.0002],
      [32.4637, -17.2492],
    ]);
    setSize(600);
    //console.log(geojson);
  }, [])
  
  /* const geojson = {
    type: "FeatureCollection",
    features: [
      {
        type: "Feature",
        geometry: { type: "MultiPoint", coordinates: coords },
      },
    ],
  }; */

  return (
    <div>
      <StyledSubtitle>Insect Heatmap</StyledSubtitle>
        
      <StyledMap>
      
        <MapBoxMap size="600px" fixedSize zoom={5}>
          {
            coords.length && 
            coords.map((element, index) => (
              <Marker
              key={index}
              latitude={parseFloat(element[0])}
              longitude={parseFloat(element[1])}
              >
              <img src={`${window.location.origin}/storage/images/map-markers/litterHeat.png`}/>
              </Marker>
            ))
          /* geojson && console.log(geojson) && (
            <Source id="my-data" type="geojson" data={geojson}>
            <Layer
              id="point"
              type="heatmap"
              paint={{
                "heatmap-radius": 20,
                "heatmap-color": [
                  "interpolate",
                  ["linear"],
                  ["heatmap-density"],
                  0,
                  "rgba(0, 0, 255, 0)",
                  0.1,
                  "#ffffb2",
                  0.3,
                  "#feb24c",
                  0.5,
                  "#fd8d3c",
                  0.7,
                  "#fc4e2a",
                  1,
                  "#e31a1c",
                ],
              }}
            />
          </Source>) */
          }

        </MapBoxMap>
      
      </StyledMap>
    </div>
  )
}