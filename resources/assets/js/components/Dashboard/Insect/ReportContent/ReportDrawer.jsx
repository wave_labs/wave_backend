import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Drawer } from "antd";
import moment from "moment";

import { profileInfo } from "helpers";

import { Marker } from "react-map-gl";
import { updateDrawerDimensions } from "helpers";
import MapBoxMap from "../../Common/MapBoxMap";
import MapPin from "../../Common/MapPin";


const StyledDiv = styled.div`
  font-size: 14;
  line-height: 22px;
  margin-bottom: 10px;
  color: rgba(0, 0, 0, 0.65);
`;

const StyledParagraph = styled.span`
  margin-right: 8px;
  display: inline-block;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.85);
  &:after {
    content: ":";
  }
`;

const MapContainer = styled.div`
    width: 100%;
    margin-bottom: 30px;
    height: 400px;
`;

const DescriptionItem = ({ xs, md, title, content }) => (
    <Col xs={xs} md={md}>
        <StyledDiv>
            <StyledParagraph>{title}</StyledParagraph>
            {content}
        </StyledDiv>
    </Col>
);

function ReportDrawer({ handleDrawerClose, visible, current }) {
    return (
        <Drawer
            width={"50%"}
            placement="right"
            closable={false}
            onClose={handleDrawerClose}
            visible={visible}
            destroyOnClose
        >
            <div>
                {(current.latitude && current.longitude) && (
                    <MapContainer>
                        <MapBoxMap defaultCenter={{ lat: current.latitude, lng: current.longitude }}>
                            <Marker longitude={current.longitude} latitude={current.latitude}>
                                <MapPin />
                            </Marker>
                        </MapBoxMap>
                    </MapContainer>
                )}

                <DescriptionItem xs={12} md={8} title="ID" content={current.id} />
                <DescriptionItem xs={12} md={8} title="Verbatin coordinates" content={current.coordinates} />
                <DescriptionItem xs={12} md={8} title="Date" content={current.start + " / " + current.end} />

                <DescriptionItem xs={12} md={8} title="Region" content={current.zone ? current.zone.region : "---"} />
                <DescriptionItem xs={12} md={8} title="Province" content={current.zone ? current.zone.province : "---"} />
                <DescriptionItem xs={12} md={8} title="Locality" content={current.zone ? current.zone.locality : "---"} />

                <DescriptionItem xs={12} md={8} title="Males" content={current.n_male ? current.n_male : "---"} />
                <DescriptionItem xs={12} md={8} title="Females" content={current.n_female ? current.n_female : "---"} />
                <DescriptionItem xs={12} md={8} title="Children" content={current.n_children ? current.n_children : "---"} />

                <DescriptionItem xs={12} md={8} title="NAMEUNIT" content={current.zone ? current.zone.nameunit : "---"} />
                <DescriptionItem xs={12} md={8} title="Collector" content={current.userable ? current.userable.user.name : "unknown"} />
                <DescriptionItem xs={12} md={8} title="Reference" content={current.reference ? current.reference : "---"} />
            </div>
        </Drawer>
    )
}

export default ReportDrawer;
