import React, { Component } from "react";
import { Fragment } from "react";
import { Marker } from "react-map-gl";
import MapBoxMap from "../../../Common/MapBoxMap";

class CoordinatesForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            positions: [],
            activeOnClick: true,
        };
    }

    rules = {
        requiredField: [
            {
                required: true,
                message: "Field is required",
            },
        ],
    };


    handleMarkerClick = (e) => {
        if (!e.leftButton) {
            this.setState({ positions: [] });
        }
    };

    handleMapClick = (e) => {
        if (e.leftButton) {
            let { positions, activeOnClick } = this.state;
            if (activeOnClick) {
                positions = [e.lngLat[1], e.lngLat[0]];
                this.setState({ positions });
            }
        }
    };

    handleDragEnd = (event) => {
        let { positions } = this.state;

        positions = [event.lngLat[1], event.lngLat[0]];

        setTimeout(() => {
            this.setState({
                positions,
                activeOnClick: true,
            });
        }, 1);
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.positions != this.state.positions) {
            if (this.state.positions.length == 2) {
                var object = {};
                object.latitude = this.state.positions[0];
                object.longitude = this.state.positions[1];

                this.props.handlePositions(object)
            }
        }
    }



    render() {
        const { positions } = this.state;

        return (
            <MapBoxMap
                fixedSize
                size="300"
                zoom={3}
                onClick={(e) => this.handleMapClick(e)}
            >
                {positions.length ?
                    <Marker
                        latitude={positions[0]}
                        longitude={positions[1]}
                        draggable
                        onDragEnd={this.handleDragEnd}
                        onDragStart={() =>
                            this.setState({ activeOnClick: false })
                        }
                    >
                        <svg
                            onContextMenu={this.handleMarkerClick}
                            style={{
                                position: "absolute",
                                top: "-10px",
                                left: "-10px",
                                width: "20px",
                                height: "20px",
                                fill: "red",
                            }}
                        >
                            <circle cx="10" cy="10" r="10" stroke="none" />
                        </svg>
                    </Marker>

                    : <></>
                }
            </MapBoxMap>
        );
    }
}

export default CoordinatesForm;
