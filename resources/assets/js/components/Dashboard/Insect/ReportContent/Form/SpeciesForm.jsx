import { Col, Form, InputNumber, Input, Row, DatePicker, Select } from 'antd'
import React, { useState } from 'react'
import moment from "moment";
/* import InsectFamilyRemoteCascadeContainer from "../../FamilyContent/InsectFamilyRemoteCascadeContainer"; */
import CoordinatesForm from './CoordinatesForm';
import { connect } from 'react-redux';
const { RangePicker, MonthPicker } = DatePicker;

const rules = {
    region: [
        {
            required: true,
            message: "Region is required",
        },
    ],
    province: [
        {
            required: true,
            message: "Province is required",
        },
    ],
    locality: [
        {
            required: true,
            message: "Locality is required",
        },
    ],
    date: [
        {
            required: true,
            message: "Date range is required!",
        },
    ],
    integer: [
        {
            type: "integer",
            message: "Field must be an integer",
        },
    ],
    species: [
        {
            required: true,
            message: "Species is required!",
        },
    ],
    latitude: [
        {
            required: false,
            message: "Latitude is required!",
        },
    ],
    longitude: [
        {
            required: false,
            message: "Longitude is required!",
        },
    ],
};
function SpeciesForm(props) {
    const [datepickerMode, setDatepickerMode] = useState("range");
    const { getFieldDecorator, form } = props;

    const dateFormat = {
        day: "YYYY-MM-DD",
        month: "YYYY-MM",
        year: "YYYY",
    }


    const handleDateTypeChange = (value) => {

        var newValue = undefined;
        if (value == "range") {
            newValue = [undefined, undefined];
        }
        form.setFieldsValue({ date: undefined });
        setDatepickerMode(value);
    }

    const handleDateChange = (value) => {
        form.setFieldsValue({ date: value });
    }

    return (
        <Row gutter={16}>
            <Col span={24}>
                <CoordinatesForm handlePositions={(positions) => form.setFieldsValue(positions)} />
            </Col>
            <Col xs={24} md={12}>
                <Form.Item label="Latitude">
                    {getFieldDecorator("latitude", {
                        rules: rules.latitude,
                    })(<Input placeholder="Latitude" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={12}>
                <Form.Item label="Longitude">
                    {getFieldDecorator("longitude", {
                        rules: rules.longitude,
                    })(<Input placeholder="Longitude" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Region">
                    {getFieldDecorator("region", {
                        rules: rules.region,
                    })(<Input placeholder="Region" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Province">
                    {getFieldDecorator("province", {
                        rules: rules.province,
                    })(<Input placeholder="Province" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Locality">
                    {getFieldDecorator("locality", {
                        rules: rules.locality,
                    })(<Input placeholder="Locality" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Date type">
                    {getFieldDecorator("date_type", {
                        rules: rules.latitude,
                        initialValue: "range"
                    })(
                        <Select placeholder="Date type" onChange={handleDateTypeChange} style={{ width: "100%" }}>
                            <Select.Option value="range">Range</Select.Option>
                            <Select.Option value="day">Day</Select.Option>
                            <Select.Option value="month">Month</Select.Option>
                            <Select.Option value="year">Year</Select.Option>
                        </Select>
                    )}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Date">
                    {getFieldDecorator("date", {
                        rules: rules.date,
                    })(
                        datepickerMode == "range" ?
                            <RangePicker mode={datepickerMode} placeholder="Date" /> :
                            <DatePicker
                                format={dateFormat[datepickerMode]}
                                onPanelChange={handleDateChange}
                                mode={"year"}
                                placeholder="Date"
                            />
                    )}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Page">
                    {getFieldDecorator("page", {
                        rules: rules.form,
                    })(<Input placeholder="Ex. 5-7" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Nº Males">
                    {getFieldDecorator("n_male", {
                        rules: rules.integer,
                    })(<InputNumber style={{ width: "100%" }} placeholder="Males" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Nº Females">
                    {getFieldDecorator("n_female", {
                        rules: rules.integer,
                    })(<InputNumber style={{ width: "100%" }} placeholder="Females" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Nº Children">
                    {getFieldDecorator("n_children", {
                        rules: rules.integer,
                    })(<InputNumber style={{ width: "100%" }} placeholder="Children" />)}
                </Form.Item>
            </Col>


            {/* <Col xs={24} md={12}>
                <Form.Item label="Species">
                    {getFieldDecorator("species", {
                        rules: rules.species,
                    })(<InsectFamilyRemoteCascadeContainer />)}
                </Form.Item>
            </Col> */}

            <Col xs={24} md={12}>
                <Form.Item label="Referred as">
                    {getFieldDecorator("sinonimia", {
                        rules: rules.sinonimia,
                    })(<Input placeholder="Sinonimia" />)}
                </Form.Item>
            </Col>






        </Row>
    )
}

export default SpeciesForm;