import React, { Component } from "react";
import {
    createInsectReport,
    updateInsectReport,
    setInsectReportCurrent,
} from "redux-modules/insectReport/actions";
import {
    resetModal,
    startEditing
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../Common/EditCreateModal";
import InsectReportForm from "./InsectReportForm";

class InsectReportModalContainer extends Component {
    state = {
        confirmLoading: false //modal button loading icon
    };

    onCancel = () => {
        this.resetModalForm();
    };

    onOkEditClick = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, record) => {
            if (err) {
                return;
            } else {
                this.setState({ confirmLoading: true });
                this.props
                    .updateInsectReport(this.props.current.id, record)
                    .then(data => {
                        this.resetModalForm();
                    })
                    .catch(e => this.setState({ confirmLoading: false }));
            }
        });
    };

    resetModalForm = () => {
        const form = this.formRef.props.form;
        this.setState({ confirmLoading: false });
        this.props.setInsectReportCurrent();
        this.props.resetModal();
        form.resetFields();
    };

    onOkCreateClick = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, record) => {
            if (err) {
                return;
            } else {
                this.setState({ confirmLoading: true });

                this.props
                    .createInsectReference(record)
                    .then(data => {
                        this.resetModalForm();
                    })
                    .catch(e => this.setState({ confirmLoading: false }));
            }
        });
    };


    saveFormRef = formRef => {
        this.formRef = formRef;
    };

    render() {
        return (
            <EditCreateModal
                titleEdit="Edit Report"
                titleCreate="Create Report"
                onOkEditClick={this.onOkEditClick}
                onOkCreateClick={this.onOkCreateClick}
                confirmLoading={this.state.confirmLoading}
                onCancel={this.onCancel}
                editing={this.props.editing}
                creating={this.props.creating}
            >
                <InsectReportForm
                    creating={this.props.creating}
                    wrappedComponentRef={this.saveFormRef}
                    current={this.props.current}
                />
            </EditCreateModal>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setInsectReportCurrent: () => dispatch(setInsectReportCurrent()),
        resetModal: () => dispatch(resetModal()),
        createInsectReport: data => dispatch(createInsectReport(data)),
        updateInsectReport: (data, id) => dispatch(updateInsectReport(data, id))
    };
};

const mapStateToProps = state => {
    return {
        current: state.insectReport.current,
        loading: state.insectReport.loading,
        creating: state.editCreateModal.creating,
        editing: state.editCreateModal.editing
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(InsectReportModalContainer);
