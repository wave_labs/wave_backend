import React, { Component } from "react";
import { Form } from "antd";

import SpeciesForm from "./Form/SpeciesForm";

class InsectReportForm extends Component {
    state = {};


    render() {
        const { getFieldDecorator } = this.props.form;



        const handleCoordinateChange = (object) => {
            this.props.form.setFieldsValue(object);
        };

        return (
            <Form hideRequiredMark={true}>
                {/* <ReferenceForm current={current} getFieldDecorator={getFieldDecorator} /> */}
                <SpeciesForm handleCoordinateChange={this.handleCoordinateChange} form={this.props.form} getFieldDecorator={getFieldDecorator} />

            </Form>
        );
    }
}

export default Form.create()(InsectReportForm);
