import React from "react";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import { setInsectReportCurrent } from "redux-modules/insectReport/actions";
import StopPropagation from "../../Common/StopPropagation";



function ReportTableContainer({ loading, handleTableChange, data, meta, setInsectReportCurrent, onUpdateClick, setVisible }) {
    const columns = [
        {
            title: "ID",
            dataIndex: "id",
            sorter: true,
        },
        {
            title: "Species",
            dataIndex: "species",
            render: (element) => element.name
        },
        {
            title: "Localization",
            dataIndex: "zone",
            render: (element) => element.region + ", " + element.province + ", " + element.locality
        },
        {
            title: "Period",
            dataIndex: "start",
            render: (element, row) => element + " / " + row.end
        },
        {
            title: "Collector",
            dataIndex: "userable",
            render: (element) => element ? element.user.name : "unknown"
        },
        {
            title: "",
            key: "",
            render: (text, record) => (
                <StopPropagation>
                    <RowOperation
                        deleteRow
                        updateRow
                        onUpdateClick={() => onUpdateClick(record)}
                        onDeleteConfirm={() => this.props.deleteQuestion(record.id)}
                    />
                </StopPropagation>
            ),
        },
    ];
    return (
        <div>
            <TablePagination
                loading={loading}
                columns={columns}
                handleTableChange={handleTableChange}
                data={data}
                meta={meta}
                onRow={(record) => ({
                    onClick: () => {
                        setInsectReportCurrent(record);
                        setVisible(true);
                    },
                })}
            />
        </div>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        setInsectReportCurrent: (record) => dispatch(setInsectReportCurrent(record)),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.insectReport.data,
        meta: state.insectReport.meta,
        loading: state.insectReport.loading,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ReportTableContainer);
