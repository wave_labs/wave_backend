import React, { useState } from "react";
import { connect } from "react-redux";
import { Marker, Popup } from "react-map-gl";
import MapBoxMap from "../../Common/MapBoxMap";
import styled from "styled-components";

const PopUpContainer = styled.div`
    min-width: 180px;
    padding: 10px 0px;
    box-sizing: border-box;

    span {
        margin-bottom: 10px;
        opacity: .7;
        display: block;
    }

    p {
        margin: 0px;
    }
`;


function ReportMap({ data, loading }) {
    const [popUp, setPopUp] = useState({})

    return (
        <div className="map-container">
            <MapBoxMap zoom={3} defaultCenter={{ lat: 37.62046402187591, lng: -9.122915730460894 }}>
                {!loading && data.map((record) => (
                    <>
                        {(record.longitude && record.latitude) &&
                            <Marker
                                key={record.id}
                                longitude={record.longitude}
                                latitude={record.latitude}
                            >
                                <img
                                    onClick={() => setPopUp(record)}
                                    src="/storage/images/map-markers/19.png"
                                />
                            </Marker>}
                    </>

                ))}

                {Object.values(popUp).length &&
                    <Popup
                        tipSize={5}
                        longitude={popUp.longitude}
                        latitude={popUp.latitude}
                        closeOnClick={false}
                        onClose={() => setPopUp({})}
                    >
                        <PopUpContainer>
                            <p>Species</p>
                            <span>{popUp.species && popUp.species.name}</span>

                            <p>Period</p>
                            <span>{popUp.start} / {popUp.end}</span>
                        </PopUpContainer>

                    </Popup>
                }
            </MapBoxMap>
        </div>
    )
}





const mapStateToProps = (state) => {
    return {
        data: state.insectReport.selector,
        loading: state.insectReport.loading,
    };
};

export default connect(
    mapStateToProps,
    null
)(ReportMap);
