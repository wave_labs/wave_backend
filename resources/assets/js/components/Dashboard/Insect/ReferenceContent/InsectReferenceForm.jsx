import React, { Component } from "react";
import { Form } from "antd";

import ReferenceForm from "./Form/ReferenceForm";
import SpeciesForm from "./Form/SpeciesForm";

class InsectReferenceForm extends Component {
  state = {};

  render() {
    const { getFieldDecorator } = this.props.form;
    const { current } = this.props;

    return (
      <Form hideRequiredMark={false}>
        <ReferenceForm
          current={current}
          getFieldDecorator={getFieldDecorator}
          form={this.props.form}
        />
        {/* <SpeciesForm current={current} getFieldDecorator={getFieldDecorator} /> */}
      </Form>
    );
  }
}

export default Form.create()(InsectReferenceForm);
