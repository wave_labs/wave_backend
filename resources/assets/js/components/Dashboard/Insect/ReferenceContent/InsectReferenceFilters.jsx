import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Button, Input, Select, Upload, message } from "antd";
import styled from "styled-components";
import { startCreating } from "redux-modules/editCreateModal/actions";
import ExportMessage from "../../Common/ExportMessage";

message.config({
  maxCount: 1,
});

const Container = styled.section`
  margin-bottom: 50px;
  display: flex;
  align-items: center;
`;

const FilterContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 16px;
  padding-right: 150px;

  .filter {
    width: 33%;
  }
`;

const ButtonContainer = styled.div`
  width: 150px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const RoundButton = styled(Button)`
  margin: auto;
  width: 70px;
  height: 70px;
`;

function InsectReferenceFilters(props) {
  const [messageStatus, setMessageStatus] = useState(null);

  const handleReset = () => {
    props.handleFilterChange({
      search: undefined,
      sinonimia: undefined,
      author: undefined,
      tag: undefined,
      keyword: undefined,
      category: undefined,
    });
  };

  const tagOptions = [
    "Collection objects",
    "Taxon names",
    "Synonym",
    "Pylogenetic relationships",
    "Description",
    "Identification key",
    "Illustrations",
    "Diagnostic characters",
    "Female genitalia",
    "Inmatures",
    "Host plants",
    "Distributions",
    "Acoustic calls",
    "Behaviour",
    "Cytogenetics",
    "Parasitoids",
    "Vector of",
  ];

  const keywordOptions = [
    "Sourced processed complete",
    "Source unprocessed",
    "Source being processed",
    "Source processed biology",
    "Source processed distribution",
    "Source processed nomenclature",
    "Source processed food plants",
    "Source processed habitat",
    "Source processed description",
    "Pages needed",
    "Request data from authors",
    "Search suplementary material",
  ];

  const categoryOptions = [
    "Biology",
    "Distribution",
    "Nomenclature",
    "Food plants",
    "Habitat",
    "Description",
  ];

  return (
    <>
      <ExportMessage message={messageStatus} />

      <Container>
        <FilterContainer>
          <div className="filter">
            <Input
              value={props.filters.search}
              placeholder="Title, journal, editor or year"
              onChange={(value) =>
                props.handleFilterChange({ search: value.target.value })
              }
            />
          </div>
          <div className="filter">
            <Input.Search
              placeholder="Species"
              onSearch={(value) =>
                props.handleFilterChange({ sinonimia: value })
              }
            />
          </div>
          <div className="filter">
            <Input.Search
              placeholder="Author"
              onSearch={(value) => props.handleFilterChange({ author: value })}
            />
          </div>
          <div className="filter">
            <Select
              style={{ width: "100%" }}
              placeholder="Tags"
              onChange={(value) => props.handleFilterChange({ tag: value })}
            >
              <Option value="">Tag</Option>
              {tagOptions.map((option) => (
                <Option value={option}>{option}</Option>
              ))}
            </Select>
          </div>

          <div className="filter">
            <Select
              style={{ width: "100%" }}
              placeholder="Keyword"
              onChange={(value) => props.handleFilterChange({ keyword: value })}
            >
              <Option value="">Keyword</Option>
              {keywordOptions.map((keyword) => (
                <Option value={keyword}>{keyword}</Option>
              ))}
            </Select>
          </div>
          <div className="filter">
            <Select
              style={{ width: "100%" }}
              placeholder="Category"
              onChange={(value) =>
                props.handleFilterChange({ category: value })
              }
            >
              <Option value="">Category</Option>
              {categoryOptions.map((tag) => (
                <Option value={tag}>{tag}</Option>
              ))}
            </Select>
          </div>
        </FilterContainer>

        <ButtonContainer>
          {/* <RoundButton
                        onClick={props.exportReference}
                        type="primary"
                        shape="round"
                        icon="download"
                        htmlType="submit"
                    /> */}
          <RoundButton
            onClick={handleReset}
            type="default"
            shape="round"
            icon="redo"
            htmlType="submit"
          />
          <Upload
            accept=".jpg,.png,.jpeg"
            name="predictPic"
            showUploadList={false}
            customRequest={props.handleUpload}
          >
            <RoundButton type="default" shape="round" icon="upload" />
          </Upload>

          <RoundButton
            onClick={props.startCreating}
            type="primary"
            shape="round"
            icon="plus"
            htmlType="submit"
          />
        </ButtonContainer>
      </Container>
    </>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectReferences: (page, filters) =>
      dispatch(fetchInsectReferences(page, filters)),
    startCreating: () => dispatch(startCreating()),
    exportReference: (filters) => dispatch(exportReference(filters)),
  };
};
const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
    loadingExport: state.litter.loadingExport,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectReferenceFilters);
