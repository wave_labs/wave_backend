import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Drawer } from "antd";
import moment from "moment";

import { Marker } from "react-map-gl";
import MapBoxMap from "../../Common/MapBoxMap";
import MapPin from "../../Common/MapPin";

const StyledDiv = styled.div`
  font-size: 14;
  line-height: 22px;
  margin-bottom: 10px;
  color: rgba(0, 0, 0, 0.65);
`;

const StyledParagraph = styled.span`
  margin-right: 8px;
  display: inline-block;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.85);
  &:after {
    content: ":";
  }
`;

const MapContainer = styled.div`
  width: 100%;
  height: 350px;
`;

const DescriptionItem = ({ xs, md, title, content }) => (
  <Col xs={xs} md={md}>
    <StyledDiv>
      <StyledParagraph>{title}</StyledParagraph>
      {content}
    </StyledDiv>
  </Col>
);

function ReferenceDrawer({ handleDrawerClose, visible, current }) {
  const getAuthors = (authorList) => {
    var authors = "";
    if (authorList) {
      authorList.forEach((author) => (authors = authors + author + ", "));
    }
    return authors;
  };

  const getPublication = (
    description,
    journal,
    chapterTittle,
    date,
    identifier
  ) => {
    var publication = "";
    if (description) {
      publication = publication + description + ", ";
    }
    if (journal) {
      publication = publication + journal + ", ";
    }
    if (chapterTittle) {
      publication = publication + chapterTittle + ", ";
    }
    if (date) {
      publication = publication + date + ", ";
    }
    if (identifier) {
      publication = publication + identifier + ", ";
    }
    return publication;
  };

  return (
    <Drawer
      width={"60%"}
      placement="right"
      closable={false}
      onClose={handleDrawerClose}
      visible={visible}
      destroyOnClose
    >
      {console.log(current)}
      <div>
        <DescriptionItem span={24} title="Title" content={current.title} />
        <DescriptionItem
          span={24}
          title="Authors"
          content={getAuthors(current.authors)}
        />
        <DescriptionItem
          span={24}
          title="Publication"
          content={getPublication(
            current.description,
            current.journal,
            current.chapterTittle,
            current.date,
            current.identifier
          )}
        />

        {current.reports &&
          current.reports.map((report) => {
            var hasCoords = report.latitude && report.longitude;
            return (
              <div
                style={{
                  borderBottom: "1px solid #bbbbbb",
                  padding: "50px 0px 30px 0px",
                  boxSizing: "border-box",
                }}
              >
                <Row>
                  <Col xs={24} md={hasCoords ? 12 : 24}>
                    <DescriptionItem
                      xs={12}
                      md={hasCoords ? 12 : 8}
                      title="Date"
                      content={
                        report.date_type == "range"
                          ? report.start + " / " + report.end
                          : report.date
                      }
                    />
                    <DescriptionItem
                      xs={12}
                      md={hasCoords ? 12 : 8}
                      title="Page(s)"
                      content={report.page}
                    />

                    <DescriptionItem
                      xs={12}
                      md={hasCoords ? 12 : 8}
                      title="Species"
                      content={report.species ? report.species.name : "---"}
                    />
                    <DescriptionItem
                      xs={12}
                      md={hasCoords ? 12 : 8}
                      title="Referred as"
                      content={report.sinonimia ? report.sinonimia.name : "---"}
                    />

                    {hasCoords && (
                      <DescriptionItem
                        xs={12}
                        md={12}
                        title="Verbatin coordinates"
                        content={report.coordinates}
                      />
                    )}

                    <DescriptionItem
                      xs={12}
                      md={hasCoords ? 12 : 8}
                      title="Region"
                      content={report.zone ? report.zone.region : "---"}
                    />
                    <DescriptionItem
                      xs={12}
                      md={hasCoords ? 12 : 8}
                      title="Province"
                      content={report.zone ? report.zone.province : "---"}
                    />
                    <DescriptionItem
                      xs={12}
                      md={hasCoords ? 12 : 8}
                      title="Locality"
                      content={report.zone ? report.zone.locality : "---"}
                    />

                    <DescriptionItem
                      xs={12}
                      md={hasCoords ? 12 : 8}
                      title="Males"
                      content={report.n_male ? report.n_male : "---"}
                    />
                    <DescriptionItem
                      xs={12}
                      md={hasCoords ? 12 : 8}
                      title="Females"
                      content={report.n_female ? report.n_female : "---"}
                    />
                    <DescriptionItem
                      xs={12}
                      md={hasCoords ? 12 : 8}
                      title="Children"
                      content={report.n_children ? report.n_children : "---"}
                    />
                  </Col>

                  <Col xs={24} md={12}>
                    {hasCoords && (
                      <MapContainer>
                        <MapBoxMap
                          zoom={4}
                          defaultCenter={{
                            lat: report.latitude,
                            lng: report.longitude,
                          }}
                        >
                          <Marker
                            longitude={report.longitude}
                            latitude={report.latitude}
                          >
                            <MapPin />
                          </Marker>
                        </MapBoxMap>
                      </MapContainer>
                    )}
                  </Col>
                </Row>
                <br />
              </div>
            );
          })}
      </div>
    </Drawer>
  );
}

export default ReferenceDrawer;
