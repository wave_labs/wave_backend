import React, { Component } from "react";
import {
  createInsectReference,
  updateInsectReference,
  setInsectReferenceCurrent,
  fetchInsectReference,
} from "redux-modules/insectReference/actions";
import { createInsectReport } from "redux-modules/insectReport/actions";
import {
  resetModal,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import InsectModal from "./InsectModal";
import InsectReferenceForm from "./InsectReferenceForm";
import InsectReportForm from "../ReportContent/InsectReportForm";

class InsectReferenceModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
    this.props.setSpeciesForm(false);
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, record) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        console.log(this.props.current.id);
        console.log(record);
        this.props
          .updateInsectReference(this.props.current.id, record)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.setInsectReferenceCurrent();
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, record) => {
      if (err) {
        return;
      } else {
        record.project_id = this.props.projectId;
        this.setState({ confirmLoading: true });
        if (this.props.speciesForm) {
          this.props
            .createInsectReport({
              ...record,
              reference_id: this.props.current.id,
            })
            .then((data) => {
              this.props.fetchInsectReference(this.props.current.id);
              this.resetModalForm();
            })
            .catch((e) => this.setState({ confirmLoading: false }));
        } else {
          this.props
            .createInsectReference(record)
            .then((data) => {
              this.resetModalForm();
            })
            .catch((e) => this.setState({ confirmLoading: false }));
        }
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <InsectModal
        titleEdit="Edit Reference"
        titleCreate="Create Reference"
        onOkEditClick={this.onOkEditClick}
        onOkCreateClick={this.onOkCreateClick}
        confirmLoading={this.state.confirmLoading}
        onCancel={this.onCancel}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        {this.props.speciesForm ? (
          <InsectReportForm
            creating={this.props.creating}
            wrappedComponentRef={this.saveFormRef}
          />
        ) : (
          <InsectReferenceForm
            creating={this.props.creating}
            wrappedComponentRef={this.saveFormRef}
            current={this.props.current}
          />
        )}
      </InsectModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setInsectReferenceCurrent: () => dispatch(setInsectReferenceCurrent()),
    resetModal: () => dispatch(resetModal()),
    createInsectReference: (data) => dispatch(createInsectReference(data)),
    createInsectReport: (data) => dispatch(createInsectReport(data)),
    updateInsectReference: (id, data) =>
      dispatch(updateInsectReference(id, data)),
    fetchInsectReference: (id) => dispatch(fetchInsectReference(id)),
  };
};

const mapStateToProps = (state) => {
  return {
    current: state.insectReference.current,
    loading: state.insectReference.loading,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectReferenceModalContainer);
