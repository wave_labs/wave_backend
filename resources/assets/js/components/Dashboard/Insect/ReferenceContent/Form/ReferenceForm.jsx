import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon, Collapse } from "antd";
import moment from "moment";
import React, { useState } from "react";
import JornalArticleForm from "./JornalArticleForm";
import { fetchReferenceAuthorsSelector } from "redux-modules/insectReference/actions";
import BookForm from "./BookForm";
import BookChapterForm from "./BookChapterForm";
import ConferencePaperForm from "./ConferencePaperForm";
import ThesisForm from "./ThesisForm";
import WebpageForm from "./WebpageForm";
import CongressForm from "./CongressForm";
import { useEffect } from "react";
import { connect } from "react-redux";
import { useCallback } from "react";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

const { Panel } = Collapse;

function ReferenceForm(props) {
  const {
    current,
    getFieldDecorator,
    form,
    fetchReferenceAuthorsSelector,
    authors,
  } = props;
  const children = [];
  const [formType, setFormType] = useState(current.description);
  const [filters, setFilters] = useState({ search: "" });

  const handleDateChange = (value) => {
    form.setFieldsValue({ date: value });
  };

  const handleFormType = (value) => {
    setFormType(value);
  };

  useEffect(() => {
    fetchReferenceAuthorsSelector();
  }, []);

  const onSearch = useCallback(
    _.debounce((search) => onSearchChange(search), 500),
    []
  );

  const onSearchChange = (search) => {
    setFilters({
      search: search,
    });
  };

  useEffect(() => {
    fetchReferenceAuthorsSelector(filters);
  }, [filters]);

  const handleChange = () => {
    setFilters({
      search: "",
    });
  };

  for (let i = 10; i < 36; i++) {
    children.push(
      <Select.Option key={i.toString(36) + i}>
        {i.toString(36) + i}
      </Select.Option>
    );
  }

  //ToDo: put into remote select container

  const tagOptions = [
    "Collection objects",
    "Taxon names",
    "Synonym",
    "Pylogenetic relationships",
    "Description",
    "Identification key",
    "Illustrations",
    "Diagnostic characters",
    "Female genitalia",
    "Inmatures",
    "Host plants",
    "Distributions",
    "Acoustic calls",
    "Behaviour",
    "Cytogenetics",
    "Parasitoids",
    "Vector of",
  ];

  const keywordOptions = [
    "Source unprocessed biology",
    "Source being processed biology",
    "Source processed biology",
    "Source unprocessed distribution",
    "Source being processed distribution",
    "Source processed distribution",
    "Source unprocessed nomenclature",
    "Source being processed nomenclature",
    "Source processed nomenclature",
    "Source unprocessed food plants",
    "Source being processed food plants",
    "Source processed food plants",
    "Source unprocessed habitat",
    "Source being processed habitat",
    "Source processed habitat",
    "Source unprocessed description",
    "Source being processed description",
    "Source processed description",
    "Pages needed",
    "Request data from authors",
    "Search suplementary material",
  ];

  const explanations = {
    description: "Book, journal, journal article, webpage, etc",
    authorList: "The author or authors of the referenced work",
    identifier:
      "DOI, ISBN, URI, etc refering to the reference. This can be repeated in multiple rows to include multiple identifiers, e.g. a DOI and a URL pointing to a pdf of the article",
    year: "Date of publication, recommended ISO format YYYY",
    edition:
      "The name defining a special edition of a document. Normally its a literal value composed of a version number and words",
    language: "Language that the resource is in",
    subject:
      "Semicolon seperated list of keywords. Can include a resource qualifier that specifies the relation of this reference to the taxon, e.g namePublishedIn",
    taxonRemarks:
      "Annotation of taxon-specific information related to the referenced publication",
    type: "Used to assign a bibliographic reference to list of taxonomic or nomenclatural categories. Best practice is to use a controlled vocabulary",
    tags: "Tags related to the work",
    categories: "Categories present on the work",
    keywords: "State that the processing of the reference is in",
    validated:
      "This is to check or prove the validity or accuracy of the information processed",
    datasetName:
      "The name identifying the data set from which the record was derived",
    datasetID:
      "An identifier for the set of data. May be a global unique identifier or an identifier specific to a collection or institution",
    modified:
      "The most recent date-time on which the resource was changed. For Darwin Core, recommended best practice is to use an encoding scheme, such as ISO 8601:2004(E)",
    institutionID:
      "An identifier for the institution having custody of the object(s) or information referred to in the record",
    collectionID:
      "An identifier for the collection or dataset from which the record was derived. For physical specimens, the recommended best practice is to use the identifier in a collections registry such as the Biodiversity Collections Index",
    institutionCode:
      "The name (or acronym) in use by the institution having custody of the object(s) or information referred to in the record",
    collectionCode:
      "The name, acronym, coden, or initialism identifying the collection or data set from which the record was derived",
    ownerInstitutionCode:
      "The name (or acronym) in use by the institution having ownership of the object(s) or information referred to in the record",
  };

  return (
    <Row gutter={16}>
      <Col xs={24} md={12} lg={8}>
        <Specifications
          label="Publication type"
          content={explanations["description"]}
        />
        <Form.Item hasFeedback>
          {getFieldDecorator("description", {
            initialValue: current.description,
          })(
            <Select placeholder="Publication type" onChange={handleFormType}>
              <Option key="Conference Paper" value="Conference Paper">
                Conference Paper
              </Option>
              <Option key="Congress" value="Congress">
                Congress
              </Option>
              <Option key="Book Chapter" value="Book Chapter">
                Book Chapter
              </Option>
              <Option key="Book" value="Book">
                Book
              </Option>
              <Option key="Jornal Article" value="Jornal Article">
                Jornal Article
              </Option>
              <Option key="Thesis" value="Thesis">
                Thesis
              </Option>
              <Option key="Webpage" value="Webpage">
                Webpage
              </Option>
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={24} lg={16}>
        <Specifications
          label="Authors (after each author press enter)"
          content={explanations["authorList"]}
        />
        <Form.Item>
          {getFieldDecorator("authorList", {
            initialValue: current.authors,
          })(
            <Select
              mode="tags"
              onSearch={onSearch}
              style={{ width: "100%" }}
              placeholder="Authors"
              onChange={handleChange}
            >
              {authors.map((author) => (
                <Option value={author.name} key={author.id}>
                  {author.name}
                </Option>
              ))}
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications
          label="Identifier"
          content={explanations["identifier"]}
        />
        <Form.Item>
          {getFieldDecorator("identifier", {
            initialValue: current.identifier,
          })(<Input placeholder="Identifier (DOI, URL, etc)" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Year" content={explanations["year"]} />
        <Form.Item>
          {getFieldDecorator("date", {
            initialValue: current.date ? moment(current.date) : null,
          })(
            <DatePicker
              format={"YYYY"}
              onPanelChange={handleDateChange}
              mode={"year"}
              placeholder="Date"
            />
          )}
        </Form.Item>
      </Col>
      {/* Different form depending on the type of publication */}
      {formType == "Jornal Article" && (
        <JornalArticleForm
          current={current}
          getFieldDecorator={getFieldDecorator}
        />
      )}
      {formType == "Book" && (
        <BookForm current={current} getFieldDecorator={getFieldDecorator} />
      )}
      {formType == "Book Chapter" && (
        <BookChapterForm
          current={current}
          getFieldDecorator={getFieldDecorator}
        />
      )}
      {formType == "Conference Paper" && (
        <ConferencePaperForm
          current={current}
          getFieldDecorator={getFieldDecorator}
        />
      )}
      {formType == "Congress" && (
        <CongressForm current={current} getFieldDecorator={getFieldDecorator} />
      )}
      {formType == "Thesis" && (
        <ThesisForm current={current} getFieldDecorator={getFieldDecorator} />
      )}
      {formType == "Webpage" && (
        <WebpageForm current={current} getFieldDecorator={getFieldDecorator} />
      )}
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Language" content={explanations["language"]} />
        <Form.Item>
          {getFieldDecorator("language", {
            initialValue: current.language,
          })(<Input placeholder="Language" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Subject" content={explanations["subject"]} />
        <Form.Item>
          {getFieldDecorator("subject", {
            initialValue: current.subject,
          })(<Input placeholder="Subject" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications
          label="Taxon Remarks"
          content={explanations["taxonRemarks"]}
        />
        <Form.Item>
          {getFieldDecorator("taxonRemarks", {
            initialValue: current.taxonRemarks,
          })(<Input placeholder="Taxon remarks" />)}
        </Form.Item>
      </Col>
      {/* <Col xs={24} md={12} lg={8}>
        <Specifications
          label="Categories"
          content={explanations["categories"]}
        />
        <Form.Item hasFeedback>
          {getFieldDecorator("categories", {
            initialValue: current.categories,
          })(
            <Select placeholder="Categories" mode="multiple">
              {categoryOptions.map((tag) => (
                <Option value={tag}>{tag}</Option>
              ))}
            </Select>
          )}
        </Form.Item>
      </Col> */}
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Tags" content={explanations["tags"]} />
        <Form.Item hasFeedback>
          {getFieldDecorator("tags", {
            initialValue: current.tags,
          })(
            <Select placeholder="Tags" mode="multiple">
              {tagOptions.map((option) => (
                <Option value={option}>{option}</Option>
              ))}
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Type" content={explanations["type"]} />
        <Form.Item>
          {getFieldDecorator("type", {
            initialValue: current.type,
          })(<Input placeholder="Type" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="State" content={explanations["keywords"]} />
        <Form.Item hasFeedback>
          {getFieldDecorator("keywords", {
            initialValue: current.keywords,
          })(
            <Select placeholder="State" mode="multiple">
              {keywordOptions.map((keyword) => (
                <Option value={keyword}>{keyword}</Option>
              ))}
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Validated" content={explanations["validated"]} />
        <Form.Item hasFeedback>
          {getFieldDecorator("validated", {
            initialValue: current.validated,
          })(
            <Select placeholder="Validated">
              <Option value="Yes">Yes</Option>
              <Option value="No">No</Option>
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col xs={24}>
        <Collapse>
          <Panel
            header="Expand to fill dataset and collection information"
            key="1"
          >
            <Col xs={24} md={12} lg={8}>
              <Specifications
                label="Dataset Name"
                content={explanations["datasetName"]}
              />
              <Form.Item>
                {getFieldDecorator("datasetName", {
                  initialValue: current.datasetName,
                })(<Input placeholder="Dataset Name" />)}
              </Form.Item>
            </Col>
            <Col xs={24} md={12} lg={8}>
              <Specifications
                label="Dataset ID"
                content={explanations["datasetID"]}
              />
              <Form.Item>
                {getFieldDecorator("datasetID", {
                  initialValue: current.datasetID,
                })(<Input placeholder="Dataset ID" />)}
              </Form.Item>
            </Col>
            <Col xs={24} md={12} lg={8}>
              <Specifications
                label="Modified"
                content={explanations["modified"]}
              />
              <Form.Item>
                {getFieldDecorator("modified", {
                  initialValue: current.modified,
                })(<Input placeholder="Modified" />)}
              </Form.Item>
            </Col>
            <Col xs={24} md={12} lg={8}>
              <Specifications
                label="Institution ID"
                content={explanations["institutionID"]}
              />
              <Form.Item>
                {getFieldDecorator("institutionID", {
                  initialValue: current.institutionID,
                })(<Input placeholder="Institution ID" />)}
              </Form.Item>
            </Col>
            <Col xs={24} md={12} lg={8}>
              <Specifications
                label="Institution Code"
                content={explanations["institutionCode"]}
              />
              <Form.Item>
                {getFieldDecorator("institutionCode", {
                  initialValue: current.institutionCode,
                })(<Input placeholder="Institution Code" />)}
              </Form.Item>
            </Col>
            <Col xs={24} md={12} lg={8}>
              <Specifications
                label="Collection ID"
                content={explanations["collectionID"]}
              />
              <Form.Item>
                {getFieldDecorator("collectionID", {
                  initialValue: current.collectionID,
                })(<Input placeholder="Collection ID" />)}
              </Form.Item>
            </Col>
            <Col xs={24} md={12} lg={8}>
              <Specifications
                label="Collection Code"
                content={explanations["collectionCode"]}
              />
              <Form.Item>
                {getFieldDecorator("collectionCode", {
                  initialValue: current.collectionCode,
                })(<Input placeholder="Collection Code" />)}
              </Form.Item>
            </Col>
            <Col xs={24} md={12} lg={8}>
              <Specifications
                label="Owner Institution Code"
                content={explanations["ownerInstitutionCode"]}
              />
              <Form.Item>
                {getFieldDecorator("ownerInstitutionCode", {
                  initialValue: current.ownerInstitutionCode,
                })(<Input placeholder="Owner Institution Code" />)}
              </Form.Item>
            </Col>
          </Panel>
        </Collapse>
      </Col>
    </Row>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchReferenceAuthorsSelector: (filters) =>
      dispatch(fetchReferenceAuthorsSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    authors: state.insectReference.selectorAuthors,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReferenceForm);
