import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function CongressForm(props) {
  const { current, getFieldDecorator } = props;
  const children = [];

  const handleDateChange = (value) => {
    current.setFieldsValue({ date: value });
  };

  const handleAccessDate = (value) => {
    current.setFieldsValue({ accessDate: value });
  };

  for (let i = 10; i < 36; i++) {
    children.push(
      <Select.Option key={i.toString(36) + i}>
        {i.toString(36) + i}
      </Select.Option>
    );
  }

  const explanations = {
    tittle: "Title of  article",
    journal: "Title of the book",
    volume: "A volume number",
    chapter: "Title of the chapter",
    editor:
      "An ordered list of editors. Normally, this list is seen as a priority list that order editors by importance",
    series: "A series number",
    publisher: "Used to link a bibliographic item to its publisher",
    pageStart: "Starting page number within a continuous page range",
    pageEnd: "Ending page number within a continuous page range",
    pages:
      "A string of non-contiguous page spans that locate a Document within a Collection. Example: 23-25, 34, 54-56",
    localityName: "City of the publication",
    issue: "An issue number",
    degree: "Degree obtained from the thesis",
    university: "University responsible for the thesis",
    conferenceTittle: "Conference title where the publication occurred",
    accessDate: "Date that the conference occurred",
  };

  //all of them in remote select container

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Title" content={explanations["title"]} />
        <Form.Item>
          {getFieldDecorator("title", {
            initialValue: current.title,
          })(<Input placeholder="Title" />)}
        </Form.Item>
      </Col>

      <Col xs={24} md={12} lg={8}>
        <Specifications
          label="Congress tittle"
          content={explanations["conferenceTittle"]}
        />
        <Form.Item>
          {getFieldDecorator("conferenceTittle", {
            initialValue: current.conferenceTittle,
          })(<Input placeholder="Congress tittle" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Edition" content={explanations["edition"]} />
        <Form.Item>
          {getFieldDecorator("edition", {
            initialValue: current.edition,
          })(<Input placeholder="Edition" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="City" content={explanations["localityName"]} />
        <Form.Item>
          {getFieldDecorator("localityName", {
            initialValue: current.localityName,
          })(<Input placeholder="City" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications
          label="Date of congress"
          content={explanations["accessDate"]}
        />
        <Form.Item>
          {getFieldDecorator("accessDate", {
            initialValue: current.accessDate,
          })(
            <DatePicker
              format={"DD MMMM YYYY"}
              onPanelChange={handleAccessDate}
              mode={"date"}
              placeholder="Date of congress"
            />
          )}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Editor (s)" content={explanations["editor"]} />
        <Form.Item>
          {getFieldDecorator("editor", {
            initialValue: current.editor,
          })(<Input placeholder="Editor (s)" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Pages" content={explanations["pages"]} />
        <Form.Item>
          {getFieldDecorator("pages", {
            initialValue: current.pages,
          })(<Input placeholder="Pages" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default CongressForm;
