import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function BookForm(props) {
  const { current, getFieldDecorator } = props;
  const children = [];

  const handleDateChange = (value) => {
    current.setFieldsValue({ date: value });
  };

  const handleAccessDate = (value) => {
    current.setFieldsValue({ accessDate: value });
  };

  for (let i = 10; i < 36; i++) {
    children.push(
      <Select.Option key={i.toString(36) + i}>
        {i.toString(36) + i}
      </Select.Option>
    );
  }

  //ToDo: put into remote select container

  const tagOptions = [
    "Collection objects",
    "Taxon names",
    "Synonym",
    "Pylogenetic relationships",
    "Description",
    "Identification key",
    "Illustrations",
    "Diagnostic characters",
    "Female genitalia",
    "Inmatures",
    "Host plants",
    "Distributions",
    "Acoustic calls",
    "Behaviour",
    "Cytogenetics",
    "Parasitoids",
    "Vector of",
  ];

  const keywordOptions = [
    "Sourced processed complete",
    "Source unprocessed",
    "Source being processed",
    "Source processed biology",
    "Source processed distribution",
    "Source processed nomenclature",
    "Source processed food plants",
    "Source processed habitat",
    "Source processed description",
    "Pages needed",
    "Request data from authors",
    "Search suplementary material",
  ];

  const categoryOptions = [
    "Biology",
    "Distribution",
    "Nomenclature",
    "Food plants",
    "Habitat",
    "Description",
  ];

  const explanations = {
    tittle: "Title of  article",
    journal: "Title of the book",
    volume: "A volume number",
    chapter: "Title of the chapter",
    editor:
      "An ordered list of editors. Normally, this list is seen as a priority list that order editors by importance",
    series: "A series number",
    publisher: "Used to link a bibliographic item to its publisher",
    pageStart: "Starting page number within a continuous page range",
    pageEnd: "Ending page number within a continuous page range",
    pages:
      "A string of non-contiguous page spans that locate a Document within a Collection. Example: 23-25, 34, 54-56",
    localityName: "Locality of the publication",
    issue: "An issue number",
    degree: "Degree obtained from the thesis",
    university: "University responsible for the thesis",
    conferenceTittle: "Conference title where the publication occurred",
    accessDate: "Date that the website was accessed",
    edition:
      "The name defining a special edition of a document. Normally its a literal value composed of a version number and words",
  };

  //all of them in remote select container

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Book tittle" content={explanations["journal"]} />
        <Form.Item>
          {getFieldDecorator("journal", {
            initialValue: current.journal,
          })(<Input placeholder="Book title" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Edition" content={explanations["edition"]} />
        <Form.Item>
          {getFieldDecorator("edition", {
            initialValue: current.edition,
          })(<Input placeholder="Edition" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Volume" content={explanations["volume"]} />
        <Form.Item>
          {getFieldDecorator("volume", {
            initialValue: current.volume,
          })(<Input placeholder="Volume" />)}
        </Form.Item>
      </Col>

      <Col xs={24} md={12} lg={8}>
        <Specifications label="Series" content={explanations["series"]} />
        <Form.Item>
          {getFieldDecorator("series", {
            initialValue: current.series,
          })(<Input placeholder="Series" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Publisher" content={explanations["publisher"]} />
        <Form.Item>
          {getFieldDecorator("publisher", {
            initialValue: current.publisher,
          })(<Input placeholder="Publisher" />)}
        </Form.Item>
      </Col>

      <Col xs={24} md={12} lg={8}>
        <Specifications label="Pages" content={explanations["pages"]} />
        <Form.Item>
          {getFieldDecorator("pages", {
            initialValue: current.pages,
          })(<Input placeholder="Pages" />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="City" content={explanations["localityName"]} />
        <Form.Item>
          {getFieldDecorator("localityName", {
            initialValue: current.localityName,
          })(<Input placeholder="City" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default BookForm;
