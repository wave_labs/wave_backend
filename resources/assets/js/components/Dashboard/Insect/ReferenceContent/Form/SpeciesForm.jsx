import { Col, Form, InputNumber, Input, Row, DatePicker } from 'antd'
import React from 'react'
import moment from "moment";
/* import InsectFamilyRemoteCascadeContainer from "../../FamilyContent/InsectFamilyRemoteCascadeContainer"; */
const { RangePicker } = DatePicker;

const rules = {
    region: [
        {
            required: true,
            message: "Region is required",
        },
    ],
    province: [
        {
            required: true,
            message: "Province is required",
        },
    ],
    locality: [
        {
            required: true,
            message: "Locality is required",
        },
    ],
    date: [
        {
            required: true,
            message: "Date range is required!",
        },
    ],
    integer: [
        {
            type: "integer",
            message: "Field must be an integer",
        },
    ],
    species: [
        {
            required: true,
            message: "Species is required!",
        },
    ],
    latitude: [
        {
            required: false,
            message: "Latitude is required!",
        },
    ],
    longitude: [
        {
            required: false,
            message: "Longitude is required!",
        },
    ],
};
function SpeciesForm(props) {
    const { current, getFieldDecorator } = props;

    return (
        <Row gutter={16}>
            <Col xs={24} md={8}>
                <Form.Item label="Region">
                    {getFieldDecorator("region", {
                        initialValue: current.zone ? current.zone.region : undefined,
                        rules: rules.region,
                    })(<Input placeholder="Region" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Province">
                    {getFieldDecorator("province", {
                        initialValue: current.zone ? current.zone.province : undefined,
                        rules: rules.province,
                    })(<Input placeholder="Province" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Locality">
                    {getFieldDecorator("locality", {
                        initialValue: current.zone ? current.zone.locality : undefined,
                        rules: rules.locality,
                    })(<Input placeholder="Locality" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Date">
                    {getFieldDecorator("date", {
                        initialValue: [current.start ? moment(current.start) : undefined, current.end ? moment(current.end) : undefined],
                        rules: rules.date,
                    })(<RangePicker placeholder="Date" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Collector">
                    {getFieldDecorator("collector", {
                        initialValue: current.collector,
                        rules: rules.form,
                    })(<Input placeholder="Collector" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Males">
                    {getFieldDecorator("n_male", {
                        initialValue: current.n_male,
                        rules: rules.integer,
                    })(<InputNumber style={{ width: "100%" }} placeholder="Males" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Females">
                    {getFieldDecorator("n_female", {
                        initialValue: current.n_female,
                        rules: rules.integer,
                    })(<InputNumber style={{ width: "100%" }} placeholder="Females" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={8}>
                <Form.Item label="Children">
                    {getFieldDecorator("n_children", {
                        initialValue: current.n_children,
                        rules: rules.integer,
                    })(<InputNumber style={{ width: "100%" }} placeholder="Children" />)}
                </Form.Item>
            </Col>

           {/*  <Col xs={24} md={12}>
                <Form.Item label="Species">
                    {getFieldDecorator("species", {
                        initialValue: current.species ? [current.species.family_id, current.species.id] : undefined,
                        rules: rules.species,
                    })(<InsectFamilyRemoteCascadeContainer initialValue={current.species ? [current.species.family_id, current.species.id] : undefined} />)}
                </Form.Item>
            </Col> */}
            <Col xs={24} md={6}>
                <Form.Item label="Latitude">
                    {getFieldDecorator("latitude", {
                        initialValue: current.latitude,
                        rules: rules.latitude,
                    })(<Input placeholder="Latitude" />)}
                </Form.Item>
            </Col>
            <Col xs={24} md={6}>
                <Form.Item label="Longitude">
                    {getFieldDecorator("longitude", {
                        initialValue: current.longitude,
                        rules: rules.longitude,
                    })(<Input placeholder="Longitude" />)}
                </Form.Item>
            </Col>
        </Row>
    )
}

export default SpeciesForm