import React from "react";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import { setInsectReferenceCurrent } from "redux-modules/insectReference/actions";
import StopPropagation from "../../Common/StopPropagation";
import { Icon, Menu, Tag } from "antd";
import { deleteInsectReference } from "../../../../redux-modules/insectReference/actions";
import moment from "moment";
import styled from "styled-components";
import {
  CheckOutlined,
  CloseOutlined,
  CopyTwoTone,
  FilePdfTwoTone,
  FileExcelTwoTone,
} from "@ant-design/icons";

const Processed = styled.div`
  width: 15px;
  height: 15px;
  border-radius: 15px;
  margin: auto;
  cursor: pointer;
  border-style: solid;
  border-color: ${(props) => props.color};
  background-color: ${(props) => (props.exists ? props.color : "white")};
`;

const Unprocessed = styled.div`
  width: 15px;
  height: 15px;
  margin: auto;
  border-color: ${(props) => props.color};
  border-left: 7.5px solid white;
  border-right: 7.5px solid white;
  border-bottom: 15px solid ${(props) => props.color};
  background-color: ${(props) => (props.exists ? props.color : "white")};
`;

const Processing = styled.div`
  width: 15px;
  height: 15px;
  margin: auto;
  border-style: solid;
  transform: rotate(-45deg);
  border-color: ${(props) => props.color};
  background-color: ${(props) => (props.exists ? props.color : "white")};
`;

function InsectReferenceTableContainer({
  loading,
  deleteInsectReference,
  handleTableChange,
  data,
  meta,
  onReportClick,
  setInsectReferenceCurrent,
  onUpdateClick,
  setVisible,
}) {
  const columns = [
    {
      title: "Authors",
      dataIndex: "authors",
      sorter: (a, b) => {
        if (a.authors.lenght && b.authors.lenght) {
          return a.authors[0].localeCompare(b.authors[0]);
        } else if (a.authors.lenght == "undefined") {
          return -1;
        } else {
          return 1;
        }
      },
      render: (authors, index) =>
        authors.map((author) =>
          index != authors.length - 1 ? (
            <span>{author}, </span>
          ) : (
            <span>{author} </span>
          )
        ),
    },
    {
      title: "Year",
      dataIndex: "date",
      render: (text, row) => (
        <p key={row.id} style={{ margin: 0 }}>
          {moment(row.date).format("YYYY")}
        </p>
      ),
      sorter: (a, b) => moment(a.date) - moment(b.date),
    },
    {
      title: "Title",
      dataIndex: "title",
      sorter: (a, b) => a.title.localeCompare(b.title),
    },
    {
      title: "Journal/Book title",
      dataIndex: "journal",
    },
    {
      title: "Keywords",
      dataIndex: "keywords",
      render: (keywords) =>
        keywords.map((keyword) => (
          <>
            {keyword == "Pages needed" && <FileExcelTwoTone />}
            {keyword == "Request data from authors" && <FilePdfTwoTone />}
            {keyword == "Search suplementary material" && <CopyTwoTone />}
          </>
        )),
    },
    {
      title: "Biology",
      dataIndex: "keywords",
      render: (keywords) =>
        keywords.map((keyword) => (
          <>
            {keyword == "Source unprocessed biology" && (
              <Unprocessed exists={true} color={"yellow"} />
            )}
            {keyword == "Source being processed biology" && (
              <Processing exists={true} color={"yellow"} />
            )}
            {keyword == "Source processed biology" && (
              <Processed exists={true} color={"yellow"} />
            )}
          </>
        )),
    },
    {
      title: "Distribution",
      dataIndex: "keywords",
      render: (keywords) =>
        keywords.map((keyword) => (
          <>
            {keyword == "Source unprocessed distribution" && (
              <Unprocessed exists={true} color={"green"} />
            )}
            {keyword == "Source being processed distribution" && (
              <Processing exists={true} color={"green"} />
            )}
            {keyword == "Source processed distribution" && (
              <Processed exists={true} color={"green"} />
            )}
          </>
        )),
    },
    {
      title: "Nomenclature",
      dataIndex: "keywords",
      render: (keywords) =>
        keywords.map((keyword) => (
          <>
            {keyword == "Source unprocessed nomenclature" && (
              <Unprocessed exists={true} color={"blue"} />
            )}
            {keyword == "Source being processed nomenclature" && (
              <Processing exists={true} color={"blue"} />
            )}
            {keyword == "Source processed nomenclature" && (
              <Processed exists={true} color={"blue"} />
            )}
          </>
        )),
    },
    {
      title: "Food Plants",
      dataIndex: "keywords",
      render: (keywords) =>
        keywords.map((keyword) => (
          <>
            {keyword == "Source unprocessed food plants" && (
              <Unprocessed exists={true} color={"red"} />
            )}
            {keyword == "Source being processed food plants" && (
              <Processing exists={true} color={"red"} />
            )}
            {keyword == "Source processed food plants" && (
              <Processed exists={true} color={"red"} />
            )}
          </>
        )),
    },
    {
      title: "Habitat",
      dataIndex: "keywords",
      render: (keywords) =>
        keywords.map((keyword) => (
          <>
            {keyword == "Source unprocessed habitat" && (
              <Unprocessed exists={true} color={"orange"} />
            )}
            {keyword == "Source being processed habitat" && (
              <Processing exists={true} color={"orange"} />
            )}
            {keyword == "Source processed habitat" && (
              <Processed exists={true} color={"orange"} />
            )}
          </>
        )),
    },
    {
      title: "Description",
      dataIndex: "keywords",
      render: (keywords) =>
        keywords.map((keyword) => (
          <>
            {keyword == "Source unprocessed description" && (
              <Unprocessed exists={true} color={"purple"} />
            )}
            {keyword == "Source being processed description" && (
              <Processing exists={true} color={"purple"} />
            )}
            {keyword == "Source processed description" && (
              <Processed exists={true} color={"purple"} />
            )}
          </>
        )),
    },
    {
      title: "Validated",
      dataIndex: "validated",
      render: (validated) =>
        validated == "Yes" ? <CheckOutlined /> : <CloseOutlined />,
    },
    /*
    {
      title: "Species",
      dataIndex: "reports",
      render: (reports) =>
        getUniqueSpecies(reports).map((species) => <span>{species}, </span>),
    }, */
    {
      title: "",
      key: "",
      render: (text, record) => (
        <StopPropagation>
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => onUpdateClick(record)}
            onDeleteConfirm={() => deleteInsectReference(record.id)}
          >
            <Menu.Item onClick={() => onReportClick(record)}>
              <a href="javascript:;">
                <Icon type="edit" /> Add report
              </a>
            </Menu.Item>
          </RowOperation>
        </StopPropagation>
      ),
    },
  ];

  const getUniqueSpecies = (reports) => {
    var species = [];

    reports.map((report) => {
      if (
        !species.includes(report.species.name + " (p. " + report.page + ")")
      ) {
        species.push(report.species.name + " (p. " + report.page + ")");
      }
    });

    return species;
  };

  return (
    <div>
      <TablePagination
        loading={loading}
        columns={columns}
        handleTableChange={handleTableChange}
        data={data}
        meta={meta}
        onRow={(record) => ({
          onClick: () => {
            setInsectReferenceCurrent(record);
            setVisible(true);
          },
        })}
      />
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    setInsectReferenceCurrent: (record) =>
      dispatch(setInsectReferenceCurrent(record)),
    deleteInsectReference: (id) => dispatch(deleteInsectReference(id)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.insectReference.data,
    meta: state.insectReference.meta,
    loading: state.insectReference.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectReferenceTableContainer);
