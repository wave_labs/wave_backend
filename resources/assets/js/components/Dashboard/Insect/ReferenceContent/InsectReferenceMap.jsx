import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Source, Layer, HeatmapLayer } from "react-map-gl";
import MapBoxMap from "../../Common/MapBoxMap";
import styled from "styled-components";
import { fetchInsectReportsSelector } from "../../../../redux-modules/insectReport/actions";
const PopUpContainer = styled.div`
    min-width: 180px;
    padding: 10px 0px;
    box-sizing: border-box;

    span {
        margin-bottom: 10px;
        opacity: .7;
        display: block;
    }

    p {
        margin: 0px;
    }
`;


function ReportMap({ data, loading, fetchInsectReportsSelector }) {
    const [coordinates, setCoordinates] = useState([])

    useEffect(() => {
        fetchInsectReportsSelector();
    }, [])

    useEffect(() => {
        if (data.length) {
            var coords = [];
            data.forEach((element) => {
                coords.push([element.longitude, element.latitude]);
            });
            setCoordinates(coords);
        }
    }, [data])

    const geojson = {
        type: "FeatureCollection",
        features: [
            {
                type: "Feature",
                geometry: { type: "MultiPoint", coordinates: coordinates },
            },
        ],
    };

    const mapbox_terrain = {
        type: "vector",
        url: "mapbox://mapbox.mapbox-terrain-v2"
    }

/*    map.addSource("mapbox-terrain", {
        type: "vector",
        url: "mapbox://mapbox.mapbox-terrain-v2",
      })*/
    

    return (
        <div className="map-container">
            {/*coordinates.length &&*/
                <MapBoxMap zoom={2}>
                    <Source type="geojson" data={geojson}>
                        <Layer
                            id="heatmap"
                            type="heatmap"
                            paint={{
                                'heatmap-weight': ['interpolate', ['linear'], ['get', 'mag'], 0, 0, 6, 1],
                                "heatmap-radius": 15,
                                'heatmap-color': [
                                    'interpolate',
                                    ['linear'],
                                    ['heatmap-density'],
                                    0,
                                    'rgba(33,102,172,0)',
                                    0.2,
                                    'rgb(103,169,207)',
                                    0.4,
                                    'rgb(209,229,240)',
                                    0.6,
                                    'rgb(253,219,199)',
                                    0.8,
                                    'rgb(239,138,98)',
                                    0.9,
                                    'rgb(255,201,101)'
                                ],
                            }}
                        />
                    </Source>
                    <Source type="vector" url={mapbox_terrain.url}>
                        {/*<Layer
                            id="landcover"
                            source="mapbox_terrain"
                            source-layer="landcover"
                            type="fill"
                            paint={{
                                "fill-color": "rgb(251, 140, 66)",
                                "fill-outline-color": "rgba(66,100,251, 1)"
                            }}
                        />
                        <Layer
                            id="hillshade"
                            source="mapbox_terrain"
                            source-layer="hillshade"
                            type="fill"
                            paint={{
                                "fill-color": "rgb(208, 219, 55)",
                                "fill-outline-color": "rgba(66,100,251, 1)"
                            }}
                        />*/}
                        <Layer
                            id="contour"
                            source="mapbox_terrain"
                            source-layer="contour"
                            type="line"
                            paint={{
                                "line-color": "rgba(233, 18, 11, 1)",
                                "line-width": 2
                            }}
                        />
                        <Layer
                            id="contour_label"
                            type="symbol"
                            source="mapbox_terrain"
                            source-layer= "contour"
                            minzoom= {0}
                            maxzoom= {22}
                            
                            filter={ [
                                "any",
                                ["==", ["get", "index"], 10],
                                ["==", ["get", "index"], 5]
                            ]}
                            layout= {{
                              "symbol-placement": "line",
                              "text-field": "{ele}",
                              "text-font": ["Open Sans Regular,   Arial Unicode MS Regular"],
                              "text-letter-spacing": 0,
                              "text-line-height": 1.6,
                              "text-max-angle": 10,
                              "text-rotation-alignment": "map"
                            }}
                            
                            paint-contours={{
                                "text-opacity": 1,
                                "text-halo-blur": 0,
                                "text-size": 12,
                                "text-halo-width": 1,
                                "text-halo-color": "#333",
                                "text-color": "#00fcdc"
                            }}
                        />
                    </Source>
                </MapBoxMap>
            }
            {/* <MapBoxMap zoom={3} defaultCenter={{ lat: 37.62046402187591, lng: -9.122915730460894 }}>
                {!loading && data.map((record) => (
                    <>
                        {(record.longitude && record.latitude) &&
                            <Marker
                                key={record.id}
                                longitude={record.longitude}
                                latitude={record.latitude}
                            >
                                <img onClick={() => setPopUp(record)} src={`${window.location.origin}/storage/images/map-markers/sightingheat.png`} />
                            </Marker>}
                    </>

                ))}

                {Object.values(popUp).length &&
                    <Popup
                        tipSize={5}
                        longitude={popUp.longitude}
                        latitude={popUp.latitude}
                        closeOnClick={false}
                        onClose={() => setPopUp({})}
                    >
                        <PopUpContainer>
                            <p>Species</p>
                            <span>{popUp.species && popUp.species.name}</span>

                            <p>Period</p>
                            <span>{popUp.start} / {popUp.end}</span>
                        </PopUpContainer>

                    </Popup>
                }
            </MapBoxMap> */}
        </div>
    )
}



const mapDispatchToProps = (dispatch) => {
    return {
        fetchInsectReportsSelector: (filters) => dispatch(fetchInsectReportsSelector(filters)),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.insectReport.selector,
        loading: state.insectReport.loading,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ReportMap);
