import React, { useState, useEffect } from "react";
import {
  fetchInsectReferences,
  setInsectReferenceCurrent,
} from "redux-modules/insectReference/actions";
import {
  startEditing,
  startCreating,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import InsectReferenceTableContainer from "./InsectReferenceTableContainer";
import InsectReferenceMap from "./InsectReferenceMap";
import InsectReferenceDrawer from "./InsectReferenceDrawer";
import styled from "styled-components";
import { Col, Row } from "antd";
import InsectReferenceFilters from "./InsectReferenceFilters";
import moment from "moment";
import InsectReferenceModalContainer from "./InsectReferenceModalContainer";
import { parse } from "bibtex-bibjson";

const Container = styled.section`
  margin-top: 20px;
  /* display: flex;
    justify-content: space-between; */
`;

function ReferenceContent({
  location,
  fetchInsectReferences,
  startCreating,
  setInsectReferenceCurrent,
  current,
  startEditing,
  projectId,
}) {
  const [filters, setFilters] = useState({
    search: undefined,
    sinonimia: undefined,
    author: undefined,
    tag: undefined,
    keyword: undefined,
    category: undefined,
    project: projectId,
  });
  const [visible, setVisible] = useState(false);
  const [speciesForm, setSpeciesForm] = useState(false);

  useEffect(() => {
    /* var { search } = location;
    const params = new URLSearchParams(search);
    var aFilters = { ...filters };
    params.forEach((value, key) => {
      if (value) {
        aFilters[key] = value;
      }
    }); */
    /* var aFilters = { ...filters };
    setFilters(aFilters); */
    fetchInsectReferences(1, filters);
  }, []);

  const handleFilters = (aFilters) => {
    setFilters({ ...filters, ...aFilters });
  };

  const handlePageChange = (pagination) => {
    fetchInsectReferences(pagination.current, filters);
  };

  useEffect(() => {
    var newFilters = { ...filters };

    if (newFilters.date) {
      newFilters.date = [
        moment(newFilters.date[0]).format("YYYY-MM-DD"),
        moment(newFilters.date[1]).format("YYYY-MM-DD"),
      ];
    }

    fetchInsectReferences(1, newFilters);
  }, [filters]);

  const handleDrawerClose = () => {
    setVisible(false);
    setInsectReferenceCurrent();
  };

  const handleUpdate = (record) => {
    setInsectReferenceCurrent(record);
    startEditing();
  };

  const handleUpload = (record) => {
    let reader = new FileReader();
    reader.readAsText(record.file);
    reader.onload = function () {
      let bibJson = parse(reader.result);
      console.log(Object.values(bibJson)[0]);
      /* let remappedBib = {
                authors: bibJson.author,
                journal: bibJson.booktitle,
                chapterTitle: bibJson.chapter,
                edition: bibJson.edition,
                editor: bibJson.editor,
                institutionID: bibJson.institution,
                journal: bibJson.journal,
                subject: bibJson.note,
                pages: bibJson.pages,
                publisher: bibJson.publisher,
                series: bibJson.series,
                title: bibJson.title,
                type: bibJson.type,
                volume: bibJson.volume,
                date: bibJson.year
            } */
      setInsectReferenceCurrent(Object.values(bibJson)[0]);
      startCreating();
    };
  };

  const handleReport = (record) => {
    setInsectReferenceCurrent(record);
    setSpeciesForm(true);
    startCreating();
  };

  return (
    <Container>
      <InsectReferenceDrawer
        current={current}
        visible={visible}
        handleDrawerClose={handleDrawerClose}
      />
      <InsectReferenceModalContainer
        speciesForm={speciesForm}
        setSpeciesForm={setSpeciesForm}
        projectId={projectId}
      />
      <InsectReferenceFilters
        filters={filters}
        handleFilterChange={handleFilters}
        handleUpload={handleUpload}
      />
      <Row type="flex" gutter={24}>
        {/* <Col xs={24} lg={12}>
                    <InsectReferenceMap viewMore />
                </Col> */}
        <Col xs={24} lg={24}>
          <InsectReferenceTableContainer
            setSpeciesForm={setSpeciesForm}
            setVisible={setVisible}
            onReportClick={handleReport}
            onUpdateClick={handleUpdate}
            handleTableChange={handlePageChange}
          />
        </Col>
      </Row>
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectReferences: (page, filters) =>
      dispatch(fetchInsectReferences(page, filters)),
    setInsectReferenceCurrent: (record) =>
      dispatch(setInsectReferenceCurrent(record)),
    startEditing: () => dispatch(startEditing()),
    startCreating: () => dispatch(startCreating()),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.insectReference.data,
    current: state.insectReference.current,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReferenceContent);
