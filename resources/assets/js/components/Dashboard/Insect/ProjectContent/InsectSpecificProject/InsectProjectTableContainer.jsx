import React, { useEffect } from "react";
import { connect } from "react-redux";
import RowOperation from "../../../Common/RowOperation";
import TablePagination from "../../../Common/TablePagination";
import StopPropagation from "../../../Common/StopPropagation";
import { Icon, Menu, Table, Tag } from "antd";
import moment from "moment";
import styled from "styled-components";
import { removeMemberFromProject } from "redux-modules/insectProject/actions";

function InsectProjectTableContainer({ data, user, removeMemberFromProject }) {
  const columns = [
    {
      title: "Email",
      dataIndex: "email",
    },
    {
      title: "",
      render: (text, record) => (
        <StopPropagation>
          {user.id == data.owner.id ? (
            <RowOperation
              deleteRow
              onDeleteConfirm={() =>
                removeMemberFromProject(record.id, data.id)
              }
            ></RowOperation>
          ) : null}
        </StopPropagation>
      ),
    },
  ];

  return (
    <div>
      <Table
        columns={columns}
        dataSource={data.team}
        rowKey={(record) => record.id}
        pagination={false}
      />
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    removeMemberFromProject: (userId, projectId) =>
      dispatch(removeMemberFromProject(userId, projectId)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.insectProject.projectSelector,
    loading: state.insectProject.loading,
    meta: state.insectProject.meta,
    user: state.auth.user,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectProjectTableContainer);
