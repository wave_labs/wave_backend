import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Button, Input, Select, Upload, message, Icon } from "antd";
import styled from "styled-components";
import { startCreating } from "redux-modules/editCreateModal/actions";
/* import ExportMessage from "../../Common/ExportMessage"; */

message.config({
  maxCount: 1,
});

const Container = styled.section`
  margin-bottom: 50px;
  display: flex;
  align-items: center;
`;

const FilterContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 40px;
  padding-right: 150px;

  .filter {
    width: 100%;
    max-width: 500px;
  }
`;

const ButtonContainer = styled.div`
  width: auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 40px;
`;

const RoundButton = styled(Button)`
  margin: auto;
  width: auto;
  height: 70px;
`;

function InsectProjectFilters(props) {
  const handleForm = (type) => {
    props.handleFormType(type);
  };

  return (
    <>
      <Container>
        {/* <FilterContainer>
          <div className="filter">
            <Input
              value={props.filters.search}
              placeholder="Search"
              onChange={(value) => onSearchChange(value.target.value)}
            />
          </div>
        </FilterContainer> */}

        <ButtonContainer>
          <RoundButton
            onClick={() => {
              handleForm("team");
            }}
            type="primary"
            /* shape="round" */
            htmlType="submit"
          >
            <Icon type="team" style={{ stroke: "#ffffff" }} />
            Team
          </RoundButton>

          <RoundButton
            onClick={() => {
              handleForm("edit");
            }}
            type="primary"
            /* shape="round" */
            htmlType="submit"
          >
            <Icon type="edit" style={{ stroke: "#ffffff" }} />
            Edit project information
          </RoundButton>
        </ButtonContainer>
      </Container>
    </>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    startCreating: () => dispatch(startCreating()),
  };
};
const mapStateToProps = (state) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectProjectFilters);
