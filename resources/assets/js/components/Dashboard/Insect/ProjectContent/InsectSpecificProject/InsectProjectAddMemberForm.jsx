import React, { Component } from "react";
import { Button, Col, Form, Input, Row, Select } from "antd";

class InsectProjectAddMemberForm extends Component {
  state = {};

  rules = {
    email: [
      {
        type: "email",
        message: "This is not a valid email",
      },
      {
        required: true,
        message: "Please input the email of the new member",
      },
    ],
  };
  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form hideRequiredMark={true}>
        <Row gutter={16}>
          <Col xs={12}>
            <Form.Item label="Email">
              {getFieldDecorator("email", {
                rules: this.rules.email,
              })(<Input placeholder="Email" />)}
            </Form.Item>
          </Col>
          <Col xs={8}>
            <Form.Item label="Experience Level">
              {getFieldDecorator(
                "experienceLevel",
                {}
              )(
                <Select placeholder="Experience Level">
                  <Select.Option key="Supervisor" value="Supervisor">
                    Supervisor
                  </Select.Option>
                  <Select.Option key="Curator" value="Curator">
                    Curator
                  </Select.Option>
                  <Select.Option key="BSc Student" value="BSc Student">
                    BSc Student
                  </Select.Option>
                  <Select.Option key="MSc Student" value="MSc Student">
                    MSc Student
                  </Select.Option>
                  <Select.Option key="PhD Student" value="PhD Student">
                    PhD Student
                  </Select.Option>
                  <Select.Option key="Volunteer" value="Volunteer">
                    Volunteer
                  </Select.Option>
                  <Select.Option key="Biologist" value="Biologist">
                    Biologist
                  </Select.Option>
                  <Select.Option key="Researcher" value="Researcher">
                    Researcher
                  </Select.Option>
                  <Select.Option key="Other" value="Other">
                    Other
                  </Select.Option>
                </Select>
              )}
            </Form.Item>
          </Col>
          <Col xs={4}>
            <Form.Item label="&nbsp;">
              <Button
                key="submit"
                type="primary"
                loading={this.props.loading}
                onClick={this.props.onOkCreateClick}
              >
                Add
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(InsectProjectAddMemberForm);
