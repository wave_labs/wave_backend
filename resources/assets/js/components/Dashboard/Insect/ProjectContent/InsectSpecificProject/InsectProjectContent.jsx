import React, { useState, useEffect } from "react";
import {
  startEditing,
  startCreating,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import styled from "styled-components";
import { Button, Col, Row } from "antd";
import { fetchInsectProject } from "redux-modules/insectProject/actions";
import { dimensions } from "helpers";
import { history } from "routes";
import InsectProjectModalContainer from "./InsectProjectModalContainer";
import InsectProjectFilters from "./InsectProjectFilters";
import { WhaleReporter } from "customIcons";
import Icon from "@ant-design/icons";
import InsectReferenceContent from "../../ReferenceContent/InsectReferenceContent";
import InsectOccurrenceContent from "../../OccurrenceContent/InsectOccurrenceContent";
import { EnvironmentOutlined, FolderOpenOutlined } from "@ant-design/icons";

const Container = styled.section`
  /* margin-bottom: 50px;
  display: flex;
  align-items: center; */
`;

const FilterContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 40px;
  padding-right: 150px;

  .filter {
    width: 100%;
    max-width: 500px;
  }
`;

const RoundButton = styled(Button)`
  margin: auto;
  width: auto;
  height: 70px;
`;

const StyledTitle = styled.div`
  font-size: 68px;
  font-weight: bold;
  max-width: 96%;
  line-height: 1.1;

  @media screen and (max-width: 1000px) {
    font-size: 50px;
    text-align: center;
  }
`;

const StyledImage = styled.img`
  width: 100%;
  max-width: 500px;
  min-width: 100px;
  cursor: pointer;
  margin: auto;
  display: block;

  &:hover {
    filter: blur(1px) brightness(80%);
  }

  @media (max-width: ${dimensions.md}) {
    width: 100%;
  }
`;

const StyledDescription = styled.section`
  max-width: 96%;
  text-align: left;
  font-size: 19px;

  @media screen and (max-width: 1000px) {
    font-size: 16px;
    text-align: center;
  }
`;

function InsectProjectContent({
  fetchInsectProject,
  data,
  match,
  startCreating,
  startEditing,
}) {
  const [formType, setFormType] = useState("");
  const [contentType, setContentType] = useState(null);
  const [projectId, setProjectId] = useState(null);

  useEffect(() => {
    fetchInsectProject(match.params.id).catch((error) => {
      history.push("/dashboard");
    });
    setProjectId(match.params.id);
  }, [match.params.id]);

  const handleForm = (type) => {
    handleContent(null);
    setFormType(type);
    if (type == "team") {
      startCreating();
    } else {
      startEditing();
    }
  };

  const handleContent = (type) => {
    setContentType(type);
  };

  return (
    <Container>
      <InsectProjectModalContainer
        formType={formType}
        projectId={match.params.id}
        projectOwner={data.owner}
      />
      <Row type="flex" justify="space-around" gutter={[16, 16]}>
        <Col xs={24} lg={24}>
          <StyledTitle>{data.name}</StyledTitle>
        </Col>
        <Col xs={24} lg={24}>
          <StyledDescription>{data.description}</StyledDescription>
        </Col>
        <Col
          xs={24}
          lg={24}
          style={{ display: "flex", justifyContent: "flex-end" }}
        >
          <InsectProjectFilters handleFormType={handleForm} />
        </Col>
      </Row>
      <Row type="flex" justify="space-around" gutter={[16, 16]}>
        <Col xs={12} type="flex" align="middle">
          <Button
            onClick={() => {
              handleContent(1);
            }}
            type="primary"
            size="large"
          >
            <FolderOpenOutlined />
            References
          </Button>
        </Col>
        <Col xs={12} type="flex" align="middle">
          <Button
            onClick={() => {
              handleContent(2);
            }}
            type="primary"
            size="large"
          >
            <EnvironmentOutlined />
            Occurrences
          </Button>
        </Col>
      </Row>
      <Row type="flex" justify="space-around" gutter={[16, 16]}>
        {contentType == 1 && (
          <Col xs={24}>
            <InsectReferenceContent projectId={projectId} />
          </Col>
        )}
        {contentType == 2 && (
          <Col xs={24}>
            <InsectOccurrenceContent projectId={projectId} />
          </Col>
        )}
      </Row>
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectProject: (id) => dispatch(fetchInsectProject(id)),
    startEditing: () => dispatch(startEditing()),
    startCreating: () => dispatch(startCreating()),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.insectProject.projectSelector,
    current: state.insectProject.projectSelector,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectProjectContent);
