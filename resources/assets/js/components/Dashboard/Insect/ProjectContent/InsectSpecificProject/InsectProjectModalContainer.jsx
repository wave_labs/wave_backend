import React, { Component } from "react";
import {
  resetModal,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import InsectProjectModal from "./InsectProjectModal";
import {
  createInsectProject,
  addMemberToProject,
  updateInsectProject,
} from "redux-modules/insectProject/actions";
import InsectProjectTableContainer from "./InsectProjectTableContainer";
import { Button } from "antd";
import InsectProjectAddMemberForm from "./InsectProjectAddMemberForm";
import { alertActions } from "redux-modules/alert/actions";
import { getErrorMessages, handleArrayToFormData } from "helpers";
import InsectProjectForm from "../InsectProjects/InsectProjectForm";

class InsectProjectModalContainer extends Component {
  state = {
    confirmLoading: false,
    fileList: [],
    previewImage: this.props.current.imagePath,
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.previewImage !== this.state.previewImage) {
      this.setState({ previewImage: this.props.current.imagePath });
    }
  }

  onCancel = () => {
    this.resetModalForm();
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({
      confirmLoading: false,
      previewImage: this.props.current.imagePath,
    });
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, record) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        this.props
          .addMemberToProject(this.props.projectId, record)
          .then((data) => {
            /* this.resetModalForm(); */
          })
          .catch((e) => {
            this.props.errorAlert({
              description: e.response.data.message,
            });
          });
      }
    });
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    console.log("got into edit");
    form.validateFields((err, record) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        let formData = new FormData();

        Object.entries(record).map((entry) => {
          formData.append(`${entry[0]}`, entry[1]);
        });

        formData.append("image", this.state.fileList);

        formData.append("_method", "PATCH");
        this.props
          .updateInsectProject(this.props.projectId, formData)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  handlePhoto = (photo, preview) => {
    this.setState({ fileList: photo, previewImage: preview });
  };

  render() {
    return (
      /* Creating is for the team modal (when viewing/adding/removing members); Editing is for project information itself */
      <InsectProjectModal
        titleCreate="Team"
        titleEdit="Edit Project"
        onOkCreateClick={this.onOkCreateClick}
        onOkEditClick={this.onOkEditClick}
        confirmLoading={this.state.confirmLoading}
        onCancel={this.onCancel}
        creating={this.props.creating}
        editing={this.props.editing}
        footer={
          this.props.creating
            ? [null, null]
            : [
                <Button key="back" onClick={this.onCancel}>
                  Close
                </Button>,
                <Button
                  key="submit"
                  type="primary"
                  loading={this.props.loading}
                  onClick={this.onOkEditClick}
                >
                  Ok
                </Button>,
              ]
        }
      >
        {this.props.formType == "team" ? (
          <>
            {this.props.projectOwner.id == this.props.user.id && (
              <InsectProjectAddMemberForm
                creating={this.props.creating}
                wrappedComponentRef={this.saveFormRef}
                onOkCreateClick={this.onOkCreateClick}
                loading={this.props.loading}
              />
            )}

            <InsectProjectTableContainer />
          </>
        ) : (
          <InsectProjectForm
            editing={this.props.editing}
            wrappedComponentRef={this.saveFormRef}
            current={this.props.current}
            handlePhoto={this.handlePhoto}
            previewImage={this.state.previewImage}
          />
        )}
      </InsectProjectModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createInsectProject: (data) => dispatch(createInsectProject(data)),
    addMemberToProject: (projectId, data) =>
      dispatch(addMemberToProject(projectId, data)),
    updateInsectProject: (id, data) => dispatch(updateInsectProject(id, data)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
    resetModal: () => dispatch(resetModal()),
  };
};

const mapStateToProps = (state) => {
  return {
    current: state.insectProject.projectSelector,
    loading: state.insectProject.loading,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
    user: state.auth.user,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectProjectModalContainer);
