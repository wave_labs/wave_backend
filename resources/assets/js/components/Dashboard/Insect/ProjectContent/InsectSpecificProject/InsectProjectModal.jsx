import React from "react";
import { Modal, Button } from "antd";

const InsectProjectModal = (props) => {
  return (
    <Modal
      destroyOnClose
      width={"90%"}
      style={{ maxWidth: "900px", color: "black" }}
      maskStyle={{ backgroundColor: "rgba(55,55,55,.2)" }}
      centered
      title={props.editing ? props.titleEdit : props.titleCreate}
      visible={props.editing || props.creating}
      onOk={
        props.editing
          ? () => props.onOkEditClick()
          : () => props.onOkCreateClick()
      }
      onCancel={() => props.onCancel()}
      confirmLoading={props.confirmLoading}
      cancelButtonProps={{ display: false }}
      okButtonProps={{ display: false }}
      footer={props.footer}
    >
      {props.children}
    </Modal>
  );
};

export default InsectProjectModal;
