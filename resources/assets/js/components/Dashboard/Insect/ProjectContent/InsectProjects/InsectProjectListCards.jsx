import React from "react";
import { Card, Col, Divider, Row } from "antd";
import styled from "styled-components";
import { dimensions } from "helpers";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

const ItemContainer = styled.div`
  width: 100%;
  border: 1px solid lightgray;
  border-radius: 6px;
  box-sizing: border-box;
  padding: 30px;
  cursor: pointer;
  margin: 3% auto;

  &:hover {
    box-shadow: 0 5px 5px 0 rgba(0, 0, 0, 0.2);
  }

  img {
    max-width: 200px;
    width: 60%;
    margin: auto;
    display: block;
  }

  h1 {
    text-align: center;
    font-size: 1.5em;
    font-weight: bold;
    margin-top: 20px;
  }

  p {
    text-align: center;
    font-size: 1.1em;
    color: #777777;
    margin-bottom: 5%;
  }
`;

const ContainerLink = styled(Link)``;

const InsectProjectListCards = ({ loading, data }) => {
  const Item = ({ id, name, description, image }) => (
    <ContainerLink to={"/dashboard/insect/project/" + id}>
      <ItemContainer>
        <h1>{name}</h1>
        <p>{description}</p>
        <img
          src={
            image
              ? image
              : "/storage/uploaded/photo/insectProject/placeholder.png"
          }
          alt={name}
        />
      </ItemContainer>
    </ContainerLink>
  );

  return (
    <div>
      <Row type="flex" justify="start" gutter={16}>
        {data.map((record) => (
          <Col xs={24} md={12} lg={8}>
            <Item
              id={record.id}
              name={record.name}
              description={record.description}
              image={record.imagePath}
            />
          </Col>
        ))}
      </Row>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.insectProject.data,
    loading: state.insectProject.loading,
  };
};

export default connect(mapStateToProps, null)(InsectProjectListCards);
