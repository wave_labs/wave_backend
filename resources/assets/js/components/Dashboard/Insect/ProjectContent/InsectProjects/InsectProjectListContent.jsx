import React, { useState, useEffect } from "react";
import {
  startEditing,
  startCreating,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import styled from "styled-components";
import { Col, Row } from "antd";
import InsectProjectListFilters from "./InsectProjectListFilters";
import InsectProjectListModalContainer from "./InsectProjectListModalContainer";
import { fetchInsectProjects } from "redux-modules/insectProject/actions";
import InsectProjectListCards from "./InsectProjectListCards";
import { debounce } from "lodash";

const Container = styled.section`
  /* display: flex;
    justify-content: space-between; */
`;

function InsectProjectListContent({ fetchInsectProjects, data }) {
  const [filters, setFilters] = useState({
    search: undefined,
  });

  useEffect(() => {
    fetchInsectProjects();
  }, []);

  const handleFilters = (aFilters) => {
    setFilters({ ...filters, ...aFilters });
  };

  useEffect(() => {
    debounceFetch();
  }, [filters]);

  const debounceFetch = debounce(() => {
    fetchInsectProjects(filters);
  }, 500);

  return (
    <Container>
      <InsectProjectListModalContainer />
      <InsectProjectListFilters
        filters={filters}
        handleFilterChange={handleFilters}
      />
      <InsectProjectListCards />
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectProjects: (filters) => dispatch(fetchInsectProjects(filters)),
    startEditing: () => dispatch(startEditing()),
    startCreating: () => dispatch(startCreating()),
  };
};

const mapStateToProps = (state) => {
  return { data: state.insectProject.data };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectProjectListContent);
