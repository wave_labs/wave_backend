import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Button, Input, Select, Upload, message } from "antd";
import styled from "styled-components";
import { startCreating } from "redux-modules/editCreateModal/actions";
/* import ExportMessage from "../../Common/ExportMessage"; */

message.config({
  maxCount: 1,
});

const Container = styled.section`
  margin-bottom: 50px;
  display: flex;
  align-items: center;
`;

const FilterContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 16px;
  padding-right: 150px;

  .filter {
    width: 100%;
    max-width: 500px;
  }
`;

const ButtonContainer = styled.div`
  width: 150px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const RoundButton = styled(Button)`
  margin: auto;
  width: 70px;
  height: 70px;
`;

function InsectProjectListFilters(props) {
  const [messageStatus, setMessageStatus] = useState(null);

  const handleReset = () => {
    props.handleFilterChange({
      search: undefined,
    });
  };

  const onSearchChange = (value) => {
    console.log(value);
    props.handleFilterChange({ search: value });
  };

  return (
    <>
      <Container>
        <FilterContainer>
          <div className="filter">
            <Input
              value={props.filters.search}
              placeholder="Search"
              onChange={(value) => onSearchChange(value.target.value)}
            />
          </div>
        </FilterContainer>

        <ButtonContainer>
          <RoundButton
            onClick={handleReset}
            type="default"
            shape="round"
            icon="redo"
            htmlType="submit"
          />

          <RoundButton
            onClick={props.startCreating}
            type="primary"
            shape="round"
            icon="plus"
            htmlType="submit"
          />
        </ButtonContainer>
      </Container>
    </>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    startCreating: () => dispatch(startCreating()),
  };
};
const mapStateToProps = (state) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectProjectListFilters);
