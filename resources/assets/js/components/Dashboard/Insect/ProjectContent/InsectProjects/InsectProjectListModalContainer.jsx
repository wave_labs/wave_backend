import React, { Component } from "react";
import {
  resetModal,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import InsectProjectListModal from "./InsectProjectListModal";
import InsectProjectForm from "./InsectProjectForm";
import { createInsectProject } from "redux-modules/insectProject/actions";

class InsectProjectListModalContainer extends Component {
  state = {
    confirmLoading: false,
    fileList: [],
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, record) => {
      if (err) {
        return;
      } else {
        console.log(record);
        this.setState({ confirmLoading: true });
        /* this.props
          .updateInsectOccurrence(this.props.current.id, record)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false })); */
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.setState({
      fileList: null,
      confirmLoading: false,
      previewImage: null,
    });
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, record) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        let formData = new FormData();

        Object.entries(record).map((entry) => {
          formData.append(`${entry[0]}`, entry[1]);
        });

        formData.append("image", this.state.fileList);
        this.props
          .createInsectProject(formData)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  handlePhoto = (photo, preview) => {
    this.setState({ fileList: photo, previewImage: preview });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <InsectProjectListModal
        titleEdit="Edit Project"
        titleCreate="Create Project"
        onOkEditClick={this.onOkEditClick}
        onOkCreateClick={this.onOkCreateClick}
        confirmLoading={this.state.confirmLoading}
        onCancel={this.onCancel}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <InsectProjectForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          current={this.props.current}
          handlePhoto={this.handlePhoto}
          previewImage={this.state.previewImage}
        />
      </InsectProjectListModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createInsectProject: (data) => dispatch(createInsectProject(data)),
    resetModal: () => dispatch(resetModal()),
  };
};

const mapStateToProps = (state) => {
  return {
    current: state.insectProject.current,
    loading: state.insectProject.loading,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectProjectListModalContainer);
