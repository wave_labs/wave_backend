import React, { Component } from "react";
import styled from "styled-components";
import {
  Form,
  Icon,
  Popover,
  Collapse,
  Row,
  Upload,
  Col,
  Input,
  Button,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { getBase64, dummyRequest, dimensions } from "helpers";
import FileDragger from "../../../ENM/Common/FileDragger";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

const FormItem = Form.Item;

const StyledImage = styled.img`
  width: 50%;
  min-width: 100px;
  cursor: pointer;
  margin: auto;
  display: block;

  &:hover {
    filter: blur(1px) brightness(80%);
  }

  @media (max-width: ${dimensions.md}) {
    width: 100%;
  }
`;

const { Panel } = Collapse;

class InsectProjectForm extends Component {
  constructor() {
    super();
    this.state = {
      fileList: [],
      confirmLoading: false, //modal button loading icon
      removedSelf: false,
      fields: [],
      formData: {
        template: undefined,
      },
      disabled: true,
    };
  }

  rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
    email: [
      {
        type: "email",
        message: "This is not a valid email!",
      },
      {
        required: true,
        message: "Please input your E-mail!",
      },
    ],
  };

  handleChange = (info) => {
    console.log(info);
    getBase64(info.file.originFileObj, (imageUrl) =>
      this.props.handlePhoto(info.file.originFileObj, imageUrl)
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { current, previewImage } = this.props;

    return (
      <Form hideRequiredMark={false}>
        <Row gutter={16}>
          <Col span={24}>
            <Form.Item label="Project Name">
              {getFieldDecorator("name", {
                initialValue: current.name,
                rules: this.rules.requiredField,
              })(<Input placeholder="Project Name" />)}
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item label="Project Description">
              {getFieldDecorator("description", {
                initialValue: current.description,
                rules: this.rules.requiredField,
              })(<Input placeholder="Project Description" />)}
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item label="Mobile Contact">
              {getFieldDecorator("mobile_contact", {
                initialValue: current.mobile_contact
                  ? String(current.mobile_contact)
                  : null,
              })(<Input placeholder="Mobile Contact" />)}
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item label="Email">
              {getFieldDecorator("email_contact", {
                initialValue: current.email_contact,
                rules: this.rules.email,
              })(<Input placeholder="Email" />)}
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item label="Address">
              {getFieldDecorator("address", {
                initialValue: current.address,
              })(<Input placeholder="Address" />)}
            </Form.Item>
          </Col>
          <Col xs={24}>
            <Row type="flex" justify="center">
              <FormItem style={{ width: "60%" }}>
                <Upload
                  customRequest={dummyRequest}
                  onChange={this.handleChange}
                  style={{ width: "100%", display: "block" }}
                  fileList={false}
                >
                  {previewImage ? (
                    <StyledImage src={previewImage} />
                  ) : (
                    <Button style={{ width: "100%" }}>
                      <UploadOutlined /> Upload photo
                    </Button>
                  )}
                </Upload>
              </FormItem>
            </Row>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(InsectProjectForm);
