import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Button, Modal, Row, Select, Upload, message } from "antd";
import styled from "styled-components";
import { startCreating } from "redux-modules/editCreateModal/actions";
import FilterRow from "../../Common/FilterRow";
import ExportMessage from "../../Common/ExportMessage";
import InsectImportModal from "./InsectImportModal";

message.config({
  maxCount: 1,
});

const Container = styled.section`
  margin-bottom: 50px;
`;

const RoundButton = styled(Button)`
  margin: auto;
  width: 70px;
  height: 70px;
`;

function InsectFamilyFilters({
  handleFilterChange,
  filters,
  isAdmin,
  startCreating,
}) {
  const [operations, setOperations] = useState(["reset"]);
  const [messageStatus, setMessageStatus] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    if (isAdmin) {
      setOperations(["reset", "create"]);
    }
  }, []);

  const handleReset = () => {
    handleFilterChange({
      search: undefined,
      taxonRank: undefined,
      nomenclatureStatus: undefined,
      taxonomicStatus: undefined,
    });
  };

  const handleFilters = (field, value) => {
    var object = {};

    object[field] = value;
    handleFilterChange(object);
  };

  const taxonRankOptions = [
    "class",
    "subclass",
    "order",
    "infraorder",
    "family",
    "genus",
    "subgenus",
    "species",
    "subspecies",
  ];

  return (
    <Container>
      <ExportMessage message={messageStatus} />
      <FilterRow
        filters={["search"]}
        additionalFilters={[
          <Select
            style={{ width: "100%" }}
            placeholder="Taxon Rank"
            onChange={(value) => handleFilters("taxonRank", value)}
          >
            <Option value="">Taxon Rank</Option>
            {taxonRankOptions.map((option) => (
              <Option value={option}>{option}</Option>
            ))}
          </Select>,
          <RoundButton
            type="default"
            shape="round"
            icon="upload"
            onClick={() => setModalVisible(!modalVisible)}
          />,
        ]}
        filterValues={filters}
        operations={operations}
        handleFilterChange={handleFilters}
        handleCreate={startCreating}
        handleReset={handleReset}
      />
      <InsectImportModal visible={modalVisible} setVisible={setModalVisible} />
    </Container>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectReports: (page, filters) =>
      dispatch(fetchInsectReports(page, filters)),
    startCreating: () => dispatch(startCreating()),
    // exportReport: (filters) => dispatch(exportReport(filters)),
  };
};
const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
    loadingExport: state.litter.loadingExport,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectFamilyFilters);
