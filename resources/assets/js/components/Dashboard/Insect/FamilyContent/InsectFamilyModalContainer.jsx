import React, { Component } from "react";
import {
  createInsectSpecie,
  updateInsectSpecie,
  setInsectSpecieCurrent,
  fetchInsectSpecies,
  updateTaxon,
} from "redux-modules/insectSpecies/actions";

import {
  createInsectSinonimia,
  updateInsectSinonimia,
  setInsectSinonimiaCurrent,
} from "redux-modules/insectSinonimia/actions";
import {
  resetModal,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import InsectModal from "../ReferenceContent/InsectModal";
import InsectSpeciesForm from "./InsectSpeciesForm";
import InsectNewNameForm from "./InsectNewNameForm";

import { alertActions } from "redux-modules/alert/actions";

class InsectFamilyModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
    nSpecies: 0,
  };

  onCancel = () => {
    this.resetModalForm();
    this.props.setNameForm(false);
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, record) => {
      if (err) {
        return;
      } else {
        if (this.props.nameForm) {
          this.setState({ confirmLoading: true });
          this.props
            .updateInsectSinonimia(this.props.currentSinonimia.id, record)
            .then((data) => {
              this.resetModalForm();
              this.props.fetchInsectSpecies(1, {});
            })
            .catch((e) => this.setState({ confirmLoading: false }));
        } else {
          this.setState({ confirmLoading: true });
          this.props
            .updateTaxon(
              this.props.current.taxonRank,
              this.props.current.id,
              record
            )
            .then((data) => {
              this.resetModalForm();
              this.props.fetchInsectSpecies(1, {});
            })
            .catch((e) => this.setState({ confirmLoading: false }));
        }
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false, nSpecies: 0 });
    this.props.setNameForm(false);
    this.props.setInsectSpecieCurrent();
    this.props.setInsectSinonimiaCurrent();
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, record) => {
      if (err) {
        return;
      } else {
        if (this.props.nameForm) {
          this.setState({ confirmLoading: true });
          record.specieID = this.props.specieID;
          record.specieRank = this.props.nameForm;
          console.log(record);
          this.props
            .createInsectSinonimia(record)
            .then((data) => {
              this.resetModalForm();
              this.props.fetchInsectSpecies(1, {});
            })
            .catch((e) => this.setState({ confirmLoading: false }));
        } else {
          this.setState({ confirmLoading: true });
          this.props
            .createInsectSpecie(record)
            .then((data) => {
              this.resetModalForm();
              this.props.fetchInsectSpecies(1, {});
            })
            .catch((error) => {
              this.setState({ confirmLoading: false });
              let messages = [];

              Object.values(error.response.data.errors).map(function (message) {
                messages.push(message[0]);
              });
              console.log(messages);
              this.props.errorAlert({
                description: messages,
              });
            });
        }
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  /* componentDidUpdate(prevProps) {
    if (prevProps.current != this.props.current) {
      console.log(prevProps.current);
      console.log(this.props.current);
    }
  } */

  render() {
    return (
      <InsectModal
        titleEdit="Edit Species"
        titleCreate="Create Species"
        onOkEditClick={this.onOkEditClick}
        onOkCreateClick={this.onOkCreateClick}
        confirmLoading={this.state.confirmLoading}
        onCancel={this.onCancel}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        {this.props.nameForm ? (
          <InsectNewNameForm
            creating={this.props.creating}
            wrappedComponentRef={this.saveFormRef}
            current={this.props.currentSinonimia}
          />
        ) : (
          <InsectSpeciesForm
            creating={this.props.creating}
            wrappedComponentRef={this.saveFormRef}
            current={this.props.current}
          />
        )}
      </InsectModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectSpecies: (page, filters) =>
      dispatch(fetchInsectSpecies(page, filters)),
    setInsectSpecieCurrent: () => dispatch(setInsectSpecieCurrent()),
    resetModal: () => dispatch(resetModal()),
    createInsectSpecie: (data) => dispatch(createInsectSpecie(data)),
    updateInsectSpecie: (id, data) => dispatch(updateInsectSpecie(id, data)),
    updateTaxon: (taxonRank, id, data) =>
      dispatch(updateTaxon(taxonRank, id, data)),
    setInsectSinonimiaCurrent: () => dispatch(setInsectSinonimiaCurrent()),
    createInsectSinonimia: (data) => dispatch(createInsectSinonimia(data)),
    updateInsectSinonimia: (id, data) =>
      dispatch(updateInsectSinonimia(id, data)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    current: state.insectSpecies.current,
    currentSinonimia: state.insectSinonimia.current,
    loading: state.insectSpecies.loading,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectFamilyModalContainer);
