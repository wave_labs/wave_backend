import React from "react";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import {
  setInsectSpecieCurrent,
  deleteInsectSpecie,
  fetchInsectSpecies,
} from "redux-modules/insectSpecies/actions";
import {
  deleteInsectSinonimia,
  setInsectSinonimiaCurrent,
} from "redux-modules/insectSinonimia/actions";
import StopPropagation from "../../Common/StopPropagation";
import {
  startEditing,
  startCreating,
} from "redux-modules/editCreateModal/actions";
import { Icon, Menu, Table } from "antd";
import moment from "moment";

function InsectFamilyTableContainer(props) {
  const { data, loading, meta, setNameForm, setSpecieID } = props;

  const getAuthors = (authorList) => {
    var authors = "";
    authorList.forEach((author) => (authors = authors + author));
    return authors;
  };

  const getOriginalName = (originalName) => {};

  const columns = [
    {
      title: "Rank",
      dataIndex: "taxonRank",
    },
    {
      title: "Name",
      dataIndex: "name",
      render: (text, row) => (
        <p key={row.id} style={{ margin: 0 }}>
          {row.name}
        </p>
      ),
    },
    {
      title: "Parent",
      dataIndex: "",
      render: (text, row) => (
        <p key={row.id} style={{ margin: 0 }}>
          {getAuthors(row.reference.authors)}{" "}
          {row.reference.date && moment(row.reference.date).format("YYYY")}
        </p>
      ),
    },
    {
      title: "Source",
      dataIndex: "source",
    },
    {
      title: "Original Combination",
      dataIndex: "",
      render: (text, row) => (
        <p key={row.id} style={{ margin: 0 }}>
          {row.originalName && row.originalName.originalNameGenus
            ? row.originalName.originalNameGenus + " "
            : ""}
          {row.originalName && row.originalName.originalNameSubGenus
            ? row.originalName.originalNameSubGenus + " "
            : ""}
          {row.originalName && row.originalName.originalNameSpecificEpithet
            ? row.originalName.originalNameSpecificEpithet + " "
            : ""}
          {row.originalName && row.originalName.originalNameInfraSpecificEpithet
            ? row.originalName.originalNameInfraSpecificEpithet + " "
            : ""}
          {row.originalName && row.originalName.reference
            ? ", " + getAuthors(row.originalName.reference.authors) + " "
            : ""}
          {row.originalName && row.originalName.reference
            ? moment(row.originalName.reference.date).format("YYYY")
            : ""}
        </p>
      ),
    },
    {
      title: "Sinonimias/Combinations",
      dataIndex: "",
      render: (text, row) =>
        row.sinonimias &&
        row.sinonimias.map((sinonimia) => {
          return (
            <p key={row.id} style={{ margin: 0 }}>
              {sinonimia.scientificName}
              {sinonimia.reference &&
                ", " +
                  getAuthors(sinonimia.reference.authors) +
                  " " +
                  moment(sinonimia.reference.date).format("YYYY")}
              {", " +
                sinonimia.taxonomicStatus +
                ", " +
                sinonimia.nomenclatureStatus}
              <StopPropagation>
                <RowOperation
                  deleteRow
                  updateRow
                  onUpdateClick={() => onUpdateClick(sinonimia)}
                  onDeleteConfirm={() => onDeleteClick(sinonimia)}
                ></RowOperation>
              </StopPropagation>
            </p>
          );
        }),
    },
    /*  {
      title: "Taxonomic Status",
      dataIndex: "taxonomicStatus",
    },
    {
      title: "Nomenclature Status",
      dataIndex: "nomenclatureStatus",
    }, */
    {
      title: "",
      render: (text, record) => (
        <StopPropagation>
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => onUpdateClick(record)}
            onDeleteConfirm={() => onDeleteClick(record)}
          >
            {record.taxonRank == "Species" ||
            record.taxonRank == "Subspecies" ? (
              <Menu.Item onClick={() => onAddClick(record)}>
                <a href="javascript:;">
                  <Icon type="edit" /> Add New Name
                </a>
              </Menu.Item>
            ) : null}
          </RowOperation>
        </StopPropagation>
      ),
    },
  ];

  const onUpdateClick = (record) => {
    if (record.taxonomicStatus) {
      setNameForm(true);
      props.setInsectSinonimiaCurrent(record);
      props.startEditing();
    } else {
      props.setInsectSpecieCurrent(record);
      props.startEditing();
    }
  };

  const onAddClick = (record) => {
    if (record.taxonRank == "Subspecies") {
      setSpecieID(record.id);
      setNameForm("Subspecie");
    } else {
      setSpecieID(record.id);
      setNameForm("Specie");
    }
    props.startCreating();
  };

  const onDeleteClick = (record) => {
    if (record.taxonomicStatus) {
      props.deleteInsectSinonimia(record.id);
      props.fetchInsectSpecies(1, {});
    } else {
      props.deleteInsectSpecie(record);
      props.fetchInsectSpecies(1, {});
    }
  };

  return (
    <div>
      <Table
        loading={loading}
        columns={columns}
        handleTableChange={props.handleTableChange}
        dataSource={data}
        meta={meta}
        childrenColumnName="children"
        rowKey="uuid"
        /* onRow={(record) => ({
          onClick: () => {
            setInsectSpecieCurrent(record);
          },
        })} */
      />
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectSpecies: (page, filters) =>
      dispatch(fetchInsectSpecies(page, filters)),
    setInsectSpecieCurrent: (record) =>
      dispatch(setInsectSpecieCurrent(record)),
    setInsectSinonimiaCurrent: (record) =>
      dispatch(setInsectSinonimiaCurrent(record)),
    startEditing: () => dispatch(startEditing()),
    startCreating: () => dispatch(startCreating()),
    deleteInsectSpecie: (id) => dispatch(deleteInsectSpecie(id)),
    deleteInsectSinonimia: (id) => dispatch(deleteInsectSinonimia(id)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.insectSpecies.data,
    meta: state.insectSpecies.meta,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectFamilyTableContainer);
