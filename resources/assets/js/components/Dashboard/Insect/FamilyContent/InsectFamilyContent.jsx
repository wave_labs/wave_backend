import React, { useState, useEffect } from "react";
import { fetchInsectSpecies } from "redux-modules/insectSpecies/actions"
import { connect } from "react-redux";
import styled from "styled-components";
import moment from "moment";
import InsectFamilyFilters from "./InsectFamilyFilters";
import InsectFamilyTableContainer from "./InsectFamilyTableContainer";
import InsectFamilyModalContainer from "./InsectFamilyModalContainer";

const Container = styled.section`
    /* display: flex;
    justify-content: space-between; */
`;

function InsectFamilyContent({ fetchInsectSpecies, current }) {
    const [filters, setFilters] = useState({});
    const [visible, setVisible] = useState(false);
    const [nameForm, setNameForm] = useState(null);
    const [specieID, setSpecieID] = useState(0);

    const handleFilters = (aFilters) => {
        setFilters({ ...filters, ...aFilters });

    }

    useEffect(() => {
        if (Object.values(current).length) {
            setVisible(true);
        }
    }, [current])

    useEffect(() => {
        var newFilters = { ...filters };

        if (newFilters.date) {
            newFilters.date = [
                moment(newFilters.date[0]).format("YYYY-MM-DD"),
                moment(newFilters.date[1]).format("YYYY-MM-DD"),
            ]
        }

        fetchInsectSpecies(1, newFilters);
    }, [filters])

    const handlePageChange = (pagination) => {
        fetchInsectSpecies(pagination.current, filters);
    }

    return (
        <Container>
            <InsectFamilyFilters filters={filters} handleFilterChange={handleFilters} />
            <InsectFamilyModalContainer nameForm={nameForm} setNameForm={setNameForm} specieID={specieID}/>
            <InsectFamilyTableContainer handlePageChange={handlePageChange} setNameForm={setNameForm} setSpecieID={setSpecieID}/>

        </Container>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchInsectSpecies: (page, filters) => dispatch(fetchInsectSpecies(page, filters)),
        setInsectSpecieCurrent: () => dispatch(setInsectSpecieCurrent()),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.insectSpecies.data,
        current: state.insectSpecies.current,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(InsectFamilyContent);
