import React, { Component, useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectSuperFamilySelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

function InsectSuperFamilyRemoteSelectContainer(props) {
  const {
    insectSuperFamilies,
    loading,
    value,
    onChange,
    placeholder = "Superfamily",
    infraOrderId,
    fetchInsectSuperFamilySelector,
  } = props;

  const [filters, setFilters] = useState({
    search: "",
    infraOrder: infraOrderId,
  });

  useEffect(() => {
    setFilters({
      infraOrder: infraOrderId,
    });
  }, [infraOrderId]);

  useEffect(() => {
    fetchInsectSuperFamilySelector(filters);
  }, [filters]);

  const onSearch = useCallback(_.debounce(search => onSearchChange(search), 500), []);

  const onSearchChange = (search) => {
    setFilters({
      search: search,
      infraOrder: infraOrderId,
    });
  };

  return (
    <SelectSearch
      value={value}
      onChange={onChange}
      onSearch={onSearch}
      loading={loading}
      placeholder={placeholder}
    >
      {insectSuperFamilies && insectSuperFamilies.map((superFamily) => (
        <Option value={superFamily.id} key={superFamily.id}>
          {superFamily.superFamily}
        </Option>
      ))}
    </SelectSearch>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectSuperFamilySelector: (filters) =>
      dispatch(fetchInsectSuperFamilySelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectSuperFamilies: state.insectSpecies.superTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectSuperFamilyRemoteSelectContainer);
