import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectClassSelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class InsectClassRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchInsectClassSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchInsectClassSelector();
  };

  render() {
    const {
      insectClasses,
      loading,
      value,
      onChange,
      placeholder = "Class",
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
      >
        {insectClasses && insectClasses.map((insectClass) => (
          <Option value={insectClass.id} key={insectClass.id}>
            {insectClass.class}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectClassSelector: (filters) =>
      dispatch(fetchInsectClassSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectClasses: state.insectSpecies.basicTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectClassRemoteSelectContainer);
