import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectPhylumSelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class InsectPhylumRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchInsectPhylumSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchInsectPhylumSelector();
  };

  render() {
    const {
      insectPhylums,
      loading,
      value,
      onChange,
      placeholder = "Phylum",
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
      >
        {insectPhylums && insectPhylums.map((phylum) => (
          <Option value={phylum.id} key={phylum.id}>
            {phylum.phylum}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectPhylumSelector: (filters) =>
      dispatch(fetchInsectPhylumSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectPhylums: state.insectSpecies.basicTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectPhylumRemoteSelectContainer);
