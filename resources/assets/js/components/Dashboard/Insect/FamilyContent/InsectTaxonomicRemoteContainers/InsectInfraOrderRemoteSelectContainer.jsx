import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectInfraOrderSelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

function InsectInfraOrderRemoteSelectContainer(props) {
  const {
    insectInfraOrders,
    loading,
    value,
    onChange,
    placeholder = "Infraorder",
    subOrderId,
    fetchInsectInfraOrderSelector,
  } = props;

  const [filters, setFilters] = useState({ search: "", subOrder: subOrderId });

  useEffect(() => {
    setFilters({
      subOrder: subOrderId,
    });
    console.log(subOrderId);
  }, [subOrderId]);

  useEffect(() => {
    fetchInsectInfraOrderSelector(filters);
  }, [filters]);

  const onSearch = useCallback(_.debounce(search => onSearchChange(search), 500), []);

  const onSearchChange = (search) => {
    setFilters({
      search: search,
      order: subOrderId,
    });
  };
  return (
    <SelectSearch
      value={value}
      onChange={onChange}
      onSearch={onSearch}
      loading={loading}
      placeholder={placeholder}
    >
      {insectInfraOrders && insectInfraOrders.map((infraOrder) => (
        <Option value={infraOrder.id} key={infraOrder.id}>
          {infraOrder.infraOrder}
        </Option>
      ))}
    </SelectSearch>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectInfraOrderSelector: (filters) =>
      dispatch(fetchInsectInfraOrderSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectInfraOrders: state.insectSpecies.infraTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectInfraOrderRemoteSelectContainer);
