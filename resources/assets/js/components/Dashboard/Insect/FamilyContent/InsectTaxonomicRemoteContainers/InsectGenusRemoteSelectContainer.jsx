import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectGenusSelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

function InsectGenusRemoteSelectContainer(props) {
  const {
    insectGenuses,
    loading,
    value,
    onChange,
    placeholder = "Genus",
    fetchInsectGenusSelector,
  } = props;

  const [filters, setFilters] = useState({ search: "" });

  useEffect(() => {
    fetchInsectGenusSelector(filters);
  }, [filters]);

  useEffect(() => {
    fetchInsectGenusSelector(filters);
  }, []);

  const onSearch = useCallback(
    _.debounce((search) => onSearchChange(search), 500),
    []
  );

  const onSearchChange = (search) => {
    setFilters({
      search: search,
    });
  };

  return (
    <SelectSearch
      value={value}
      onChange={onChange}
      onSearch={onSearch}
      loading={loading}
      placeholder={placeholder}
    >
      {insectGenuses &&
        insectGenuses.map((genus) => (
          <Option value={genus.id} key={genus.id}>
            {genus.genus}
          </Option>
        ))}
    </SelectSearch>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectGenusSelector: (filters) =>
      dispatch(fetchInsectGenusSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectGenuses: state.insectSpecies.basicTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectGenusRemoteSelectContainer);
