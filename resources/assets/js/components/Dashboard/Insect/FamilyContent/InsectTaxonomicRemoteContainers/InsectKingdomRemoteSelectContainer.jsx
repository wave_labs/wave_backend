import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectKingdomSelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class InsectKingdomRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchInsectKingdomSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchInsectKingdomSelector();
  };

  render() {
    const {
      insectKingdoms,
      loading,
      value,
      onChange,
      placeholder = "Kingdom",
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
      >
        {insectKingdoms &&
          insectKingdoms.map((kingdom) => (
            <Option value={kingdom.id} key={kingdom.id}>
              {kingdom.kingdom}
            </Option>
          ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectKingdomSelector: (filters) =>
      dispatch(fetchInsectKingdomSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectKingdoms: state.insectSpecies.basicTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectKingdomRemoteSelectContainer);
