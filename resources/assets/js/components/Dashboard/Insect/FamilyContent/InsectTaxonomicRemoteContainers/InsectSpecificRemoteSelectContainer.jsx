import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectSpecificSelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

function InsectSpecificRemoteSelectContainer(props) {
  const {
    insectSpecifics,
    loading,
    value,
    onChange,
    placeholder = "Specie",
    fetchInsectSpecificSelector,
    genusId,
    subGenusId
  } = props;

  const [filters, setFilters] = useState({ search: "", genus: genusId, subGenus: subGenusId });

  useEffect(() =>{
    setFilters({
      genus: genusId,
      subGenus: subGenusId
    });
  }, [genusId, subGenusId]);

  useEffect(() => {
    fetchInsectSpecificSelector(filters);
  }, [filters]);

  useEffect(() => {
    fetchInsectSpecificSelector(filters);
  }, []);

  const onSearch = useCallback(
    _.debounce((search) => onSearchChange(search), 500),
    []
  );

  const onSearchChange = (search) => {
    setFilters({
      search: search,
      genus: genusId,
      subGenus: subGenusId
    });
  };

  return (
    <SelectSearch
      value={value}
      onChange={onChange}
      onSearch={onSearch}
      loading={loading}
      placeholder={placeholder}
    >
      {insectSpecifics &&
        insectSpecifics.map((specific) => (
          <Option value={specific.id} key={specific.id}>
            {specific.specific}
          </Option>
        ))}
    </SelectSearch>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectSpecificSelector: (filters) =>
      dispatch(fetchInsectSpecificSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectSpecifics: state.insectSpecies.specificSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectSpecificRemoteSelectContainer);
