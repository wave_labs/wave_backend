import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectSubOrderSelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

function InsectSubOrderRemoteSelectContainer(props) {
  const {
    insectSubOrders,
    loading,
    value,
    onChange,
    placeholder = "Suborder",
    orderId,
    fetchInsectSubOrderSelector,
  } = props;

  const [filters, setFilters] = useState({ search: "", order: orderId });

  useEffect(() => {
    setFilters({
      order: orderId,
    });
  }, [orderId]);

  useEffect(() => {
    fetchInsectSubOrderSelector(filters);
  }, [filters]);

  const onSearch = useCallback(_.debounce(search => onSearchChange(search), 500), []);

  const onSearchChange = (search) => {
    setFilters({
      search: search,
      order: orderId,
    });
  };

  return (
    <SelectSearch
      value={value}
      onChange={onChange}
      onSearch={onSearch}
      loading={loading}
      placeholder={placeholder}
    >
      {insectSubOrders && insectSubOrders.map((subOrder) => (
        <Option value={subOrder.id} key={subOrder.id}>
          {subOrder.subOrder}
        </Option>
      ))}
    </SelectSearch>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectSubOrderSelector: (filters) =>
      dispatch(fetchInsectSubOrderSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectSubOrders: state.insectSpecies.subTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectSubOrderRemoteSelectContainer);
