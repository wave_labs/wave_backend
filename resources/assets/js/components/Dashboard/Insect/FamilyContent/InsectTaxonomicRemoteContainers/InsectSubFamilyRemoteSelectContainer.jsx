import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectSubFamilySelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

function InsectSubFamilyRemoteSelectContainer(props) {
  const {
    insectSubFamilies,
    loading,
    value,
    onChange,
    placeholder = "Subfamily",
    familyId,
    fetchInsectSubFamilySelector,
  } = props;

  const [filters, setFilters] = useState({ search: "", family: familyId });

  useEffect(() => {
    setFilters({
      family: familyId,
    });
  }, [familyId]);

  useEffect(() => {
    fetchInsectSubFamilySelector(filters);
  }, [filters]);

  const onSearch = useCallback(_.debounce(search => onSearchChange(search), 500), []);

  const onSearchChange = (search) => {
    setFilters({
      search: search,
      family: familyId,
    });
  };

  return (
    <SelectSearch
      value={value}
      onChange={onChange}
      onSearch={onSearch}
      loading={loading}
      placeholder={placeholder}
    >
      {insectSubFamilies && insectSubFamilies.map((subFamily) => (
        <Option value={subFamily.id} key={subFamily.id}>
          {subFamily.subFamily}
        </Option>
      ))}
    </SelectSearch>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectSubFamilySelector: (filters) =>
      dispatch(fetchInsectSubFamilySelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectSubFamilies: state.insectSpecies.subTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectSubFamilyRemoteSelectContainer);
