import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectTribeSelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

function InsectTribeRemoteSelectContainer(props) {
  const {
    insectTribes,
    loading,
    value,
    onChange,
    placeholder = "Tribe",
    subFamilyId,
    fetchInsectTribeSelector,
  } = props;

  const [filters, setFilters] = useState({ search: "", subFamily: subFamilyId });

  useEffect(() => {
    setFilters({
      subFamily: subFamilyId,
    });
  }, [subFamilyId]);

  useEffect(() => {
    fetchInsectTribeSelector(filters);
  }, [filters]);

  const onSearch = useCallback(_.debounce(search => onSearchChange(search), 500), []);

  const onSearchChange = (search) => {
    setFilters({
      search: search,
      subFamily: subFamilyId,
    });
  };

  return (
    <SelectSearch
      value={value}
      onChange={onChange}
      onSearch={onSearch}
      loading={loading}
      placeholder={placeholder}
    >
      {insectTribes && insectTribes.map((tribe) => (
        <Option value={tribe.id} key={tribe.id}>
          {tribe.tribe}
        </Option>
      ))}
    </SelectSearch>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectTribeSelector: (filters) =>
      dispatch(fetchInsectTribeSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectTribes: state.insectSpecies.infraTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectTribeRemoteSelectContainer);
