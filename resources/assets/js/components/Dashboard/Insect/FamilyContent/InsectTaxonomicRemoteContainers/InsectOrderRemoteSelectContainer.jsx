import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectOrderSelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class InsectOrderRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchInsectOrderSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchInsectOrderSelector();
  };

  render() {
    const {
      insectOrders,
      loading,
      value,
      onChange,
      placeholder = "Order",
    } = this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
      >
        {insectOrders && insectOrders.map((order) => (
          <Option value={order.id} key={order.id}>
            {order.order}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectOrderSelector: (filters) =>
      dispatch(fetchInsectOrderSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectOrders: state.insectSpecies.basicTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectOrderRemoteSelectContainer);
