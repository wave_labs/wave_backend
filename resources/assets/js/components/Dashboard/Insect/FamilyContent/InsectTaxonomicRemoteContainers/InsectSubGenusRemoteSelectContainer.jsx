import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectSubGenusSelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

function InsectSubGenusRemoteSelectContainer(props) {
  const {
    insectSubGenus,
    loading,
    value,
    onChange,
    placeholder = "Subgenus",
    fetchInsectSubGenusSelector,
    genusId
  } = props;

  const [filters, setFilters] = useState({ search: "", genus: genusId});

  useEffect(() => {
    setFilters({
      genus: genusId
    });
  }, [genusId]);

  useEffect(() => {
    fetchInsectSubGenusSelector(filters);
  }, [filters]);

  useEffect(() => {
    fetchInsectSubGenusSelector(filters);
  }, []);



  const onSearch = useCallback(_.debounce(search => onSearchChange(search), 500), []);

  const onSearchChange = (search) => {
    setFilters({
      search: search,
      genus: genusId
    });
  };

  return (
    <SelectSearch
      value={value}
      onChange={onChange}
      onSearch={onSearch}
      loading={loading}
      placeholder={placeholder}
    >
      {insectSubGenus && insectSubGenus.map((subGenus) => (
        <Option value={subGenus.id} key={subGenus.id}>
          {subGenus.subGenus}
        </Option>
      ))}
    </SelectSearch>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectSubGenusSelector: (filters) =>
      dispatch(fetchInsectSubGenusSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectSubGenus: state.insectSpecies.subTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectSubGenusRemoteSelectContainer);
