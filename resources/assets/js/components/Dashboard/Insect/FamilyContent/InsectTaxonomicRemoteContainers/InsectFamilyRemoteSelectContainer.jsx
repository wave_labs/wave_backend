import React, { Component, useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectFamilySelector } from "redux-modules/insectSpecies/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

function InsectFamilyRemoteSelectContainer(props) {
  const {
    insectFamilies,
    loading,
    value,
    onChange,
    placeholder = "Family",
    fetchInsectFamilySelector,
  } = props;

  const [filters, setFilters] = useState({
    search: "",
  });

  useEffect(() => {
    fetchInsectFamilySelector(filters);
  }, [filters]);

  useEffect(() => {
    fetchInsectFamilySelector(filters);
  }, []);

  const onSearch = useCallback(
    _.debounce((search) => onSearchChange(search), 500),
    []
  );

  const onSearchChange = (search) => {
    setFilters({
      search: search,
    });
  };

  return (
    <SelectSearch
      value={value}
      onChange={onChange}
      onSearch={onSearch}
      loading={loading}
      placeholder={placeholder}
    >
      {insectFamilies &&
        insectFamilies.map((family) => (
          <Option value={family.id} key={family.id}>
            {family.family}
          </Option>
        ))}
    </SelectSearch>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectFamilySelector: (filters) =>
      dispatch(fetchInsectFamilySelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectFamilies: state.insectSpecies.basicTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectFamilyRemoteSelectContainer);
