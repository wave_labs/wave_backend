import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class InsectTaxonRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchTaxon({ search });
  };

  componentDidMount = () => {
    this.props.fetchTaxon();
  };

  render() {
    const { insectTaxons, field, loading, value, onChange, placeholder } =
      this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
      >
        {insectTaxons.map((taxon) => (
          <Option value={taxon.id} key={taxon.id}>
            {taxon[field]}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

export default InsectTaxonRemoteSelectContainer;
