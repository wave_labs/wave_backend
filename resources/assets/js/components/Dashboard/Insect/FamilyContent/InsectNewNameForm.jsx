import React, { Component } from "react";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  Popover,
  Select,
  Cascader,
  DatePicker,
  Collapse,
} from "antd";
import moment from "moment";
import InsectReferenceRemoteSelectContainer from "./InsectReferenceRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

const { Panel } = Collapse;

const StyledTitle = styled.div`
  font-size: 1.5rem;
  font-weight: bold;
`;

class InsectNewNameForm extends Component {
  state = {
    species: 0,
  };

  rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  constructor() {
    super();
    this.taxonomicStatusOptions = [
      {
        value: "Accepted",
        label: "Accepted",
      },
      {
        value: "Synonym",
        label: "Synonym",
        children: [
          {
            value: "Heterotypic Synonym",
            label: "Heterotypic Synonym",
          },
          {
            value: "Homotypic Synonym",
            label: "Homotypic Synonym",
          },
          {
            value: "Pro Parte Synonym",
            label: "Pro Parte Synonym",
          },
          {
            value: "Junior Synonym",
            label: "Junior Synonym",
          },
          {
            value: "Objective Synonym",
            label: "Objective Synonym",
          },
          {
            value: "Subjective Synonym",
            label: "Subjective Synonym",
          },
        ],
      },
      {
        value: "Homonym",
        label: "Homonym",
        children: [
          {
            value: "Senior Homonym",
            label: "Senior Homonym",
          },
          {
            value: "Junior Homonym",
            label: "Junior Homonym",
          },
          {
            value: "Para Homonym",
            label: "Para Homonym",
          },
        ],
      },
    ];

    this.nomenclatureStatusOptions = [
      {
        value: "Unavailable",
        label: "Unavailable",
      },
      {
        value: "Available",
        label: "Available",
        children: [
          {
            value: "Combination",
            label: "Combination",
          },
          {
            value: "Invalide",
            label: "Invalide",
          },
          {
            value: "Nudum",
            label: "Nudum",
          },
          {
            value: "Oblitum",
            label: "Oblitum",
          },
          {
            value: "Valid",
            label: "Valid",
          },
          {
            value: "Status Novus",
            label: "Status Novus",
          },
          {
            value: "Nomen Erratum",
            label: "Nomen Erratum",
          },
          {
            value: "Nomen Novum",
            label: "Nomen Novum",
          },
        ],
      },
      {
        value: "homonym",
        label: "Homonym",
        children: [
          {
            value: "seniorHomonym",
            label: "Senior Homonym",
          },
          {
            value: "juniorHomonym",
            label: "Junior Homonym",
          },
          {
            value: "parahomonym",
            label: "Para Homonym",
          },
        ],
      },
    ];

    this.explanations = {
      author:
        "The authorship information for the scientific name formatted according to the conventions of the applicable nomenclaturalCode",
      date: "Year that the scientific name was firstly used",
      reference:
        "The reference to the source in which the specific taxon concept circumscription is defined or implied",
      taxonRank:
        "The taxonomic rank of the most specific name in the scientificName. Recommended best practice is to use this controlled vocabulary: http://rs.gbif.org/vocabulary/gbif/rank_2015-04-24.xml E.g species, genus",
      taxonRemarks:
        "Comments or notes about the taxon or name, E.g Type consists of a skull and skeletal fragments",
      language:
        "The language of the parent resource. Recommended best practice is to use a controlled vocabulary such as ISO 693 e.g eng",
      rights:
        "Information about rights held in and over the resource. Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights",
      license:
        "A legal document giving official permission to do something with the resource",
      institutionID:
        "An identifier for the institution having custody of the object(s) or information referred to in the record",
      institutionCode:
        "The name (or acronym) in use by the institution having custody of the object(s) or information referred to in the record",
      rightSholder:
        "A person or organization owning or managing rights over the resource",
      accessRights:
        "Information about who can access the resource or an indication of its security status. Access Rights may include information regarding access or restrictions based on privacy, security, or other policies. E.g not-for-profit use only",
      source:
        "Used to link to an external representation of the data record such as in a source web database.A URI link or reference to the source of this record",
      datasetID:
        "An identifier for a (sub) dataset. Ideally globally unique, but any id allowed",
      datasetName:
        "The title of the (sub) dataset optionally also referenced via datasetID",
    };
    this.taxonRankOptions = [
      "Kingdom",
      "Phylum",
      "Class",
      "Order",
      "Suborder",
      "Infraorder",
      "Superfamily",
      "Family",
      "Subfamily",
      "Tribe",
      "Genus",
      "Subgenus",
      "Species",
      "Subspecies",
    ];
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { current } = this.props;
    console.log(current.nomenclatureStatus);

    /* var currentTaxonomicStatus = [];
    var currentNomenclatureStatus = [];
    if (current.taxonomicStatus) {
      currentTaxonomicStatus = current.taxonomicStatus.split(",");
    }
    if (current.nomenclatureStatus) {
      currentNomenclatureStatus = current.nomenclatureStatus.split(",");
    } */

    return (
      <Form>
        <Row gutter={16}>
          <Col xs={24} md={8} lg={6}>
            <Specifications
              label="Genus"
              content={this.explanations["genus"]}
            />
            <Form.Item>
              {getFieldDecorator("genus", {
                rules: this.rules.requiredField,
                initialValue: current.genus,
              })(<Input placeholder="Genus" />)}
            </Form.Item>
          </Col>
          <Col xs={24} md={8} lg={6}>
            <Specifications
              label="Subgenus"
              content={this.explanations["subgenus"]}
            />
            <Form.Item>
              {getFieldDecorator("subgenus", {
                initialValue: current.subgenus,
              })(<Input placeholder="Subgenus" />)}
            </Form.Item>
          </Col>

          <Col xs={24} md={8} lg={6}>
            <Specifications
              label="Specific Epithet"
              content={this.explanations["specificEpithet"]}
            />
            <Form.Item>
              {getFieldDecorator("specificEpithet", {
                rules: this.rules.requiredField,
                initialValue: current.specificEpithet,
              })(<Input placeholder="Specific Epithet" />)}
            </Form.Item>
          </Col>

          <Col xs={24} md={8} lg={6}>
            <Specifications
              label="Infra Specific Epithet"
              content={this.explanations["infraSpecificEpithet"]}
            />
            <Form.Item>
              {getFieldDecorator("infraSpecificEpithet", {
                initialValue: current.infraSpecificEpithet,
              })(<Input placeholder="Infra Specific Epithet" />)}
            </Form.Item>
          </Col>
          <Col xs={24} md={16} lg={12}>
            <Specifications
              label="Reference"
              content={this.explanations["reference"]}
            />
            <Form.Item>
              {getFieldDecorator("reference", {
                initialValue: current.reference ? current.reference.id : null,
              })(<InsectReferenceRemoteSelectContainer />)}
            </Form.Item>
          </Col>

          <Col xs={24} md={8} lg={6}>
            <Specifications
              label="Taxon Rank"
              content={this.explanations["taxonRank"]}
            />
            <Form.Item>
              {getFieldDecorator("taxonRank", {
                initialValue: current.taxonRank,
              })(
                <Select placeholder="Taxon Rank" onChange={this.handleFormType}>
                  {this.taxonRankOptions.map((option) => (
                    <Option value={option}>{option}</Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          </Col>

          <Col xs={24} md={8} lg={6}>
            <Specifications
              label="Taxon Remarks"
              content={this.explanations["taxonRemarks"]}
            />
            <Form.Item>
              {getFieldDecorator("taxonRemarks", {
                initialValue: current.taxonRemarks,
              })(<Input placeholder="Taxon Remarks" />)}
            </Form.Item>
          </Col>

          <Col xs={24} md={8} lg={6}>
            <Specifications
              label="Nomenclature Status"
              content={this.explanations["nomenclatureStatus"]}
            />
            <Form.Item>
              {getFieldDecorator("nomenclatureStatus", {
                rules: this.rules.requiredField,
                initialValue: current.nomenclatureStatus,
              })(
                <Cascader
                  options={this.nomenclatureStatusOptions}
                  expandTrigger="hover"
                  placeholder="Please select"
                />
              )}
            </Form.Item>
          </Col>

          <Col xs={24} md={8} lg={6}>
            <Specifications
              label="Taxonomic Status"
              content={this.explanations["taxonomicStatus"]}
            />
            <Form.Item>
              {getFieldDecorator("taxonomicStatus", {
                rules: this.rules.requiredField,
                initialValue: current.taxonomicStatus,
              })(
                <Cascader
                  options={this.taxonomicStatusOptions}
                  expandTrigger="hover"
                  placeholder="Please select"
                />
              )}
            </Form.Item>
          </Col>
          <Col xs={24}>
            <Collapse>
              <Panel
                header="Expand to fill dataset and collection information"
                key="1"
              >
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Language"
                    content={this.explanations["language"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("language", {
                      initialValue: current.language,
                    })(<Input placeholder="Language" />)}
                  </Form.Item>
                </Col>

                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Rights"
                    content={this.explanations["rights"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("rights", {
                      initialValue: current.rights,
                    })(<Input placeholder="Rights" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="License"
                    content={this.explanations["license"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("license", {
                      initialValue: current.license,
                    })(<Input placeholder="License" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Institution ID"
                    content={this.explanations["institutionID"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("institutionID", {
                      initialValue: current.institutionID,
                    })(<Input placeholder="Institution ID" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="institution Code"
                    content={this.explanations["institutionCode"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("institutionCode", {
                      initialValue: current.institutionCode,
                    })(<Input placeholder="institution Code" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Right Sholder"
                    content={this.explanations["rightSholder"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("rightSholder", {
                      initialValue: current.rightSholder,
                    })(<Input placeholder="Right Sholder" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Access Rights"
                    content={this.explanations["accessRights"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("accessRights", {
                      initialValue: current.accessRights,
                    })(<Input placeholder="Access Rights" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Source"
                    content={this.explanations["source"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("source", {
                      initialValue: current.source,
                    })(<Input placeholder="Source" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Dataset ID"
                    content={this.explanations["datasetID"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("datasetID", {
                      initialValue: current.datasetID,
                    })(<Input placeholder="Dataset ID" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Dataset Name"
                    content={this.explanations["datasetName"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("datasetName", {
                      initialValue: current.datasetName,
                    })(<Input placeholder="Dataset Name" />)}
                  </Form.Item>
                </Col>
              </Panel>
            </Collapse>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(InsectNewNameForm);
