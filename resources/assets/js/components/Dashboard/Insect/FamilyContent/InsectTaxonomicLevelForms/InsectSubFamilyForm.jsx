import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";
import InsectFamilyRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectFamilyRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectSubFamilyForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Family" />
        <Form.Item>
          {getFieldDecorator("family_id", {
            rules: rules.requiredField,
            initialValue: current.family && current.family.id,
          })(<InsectFamilyRemoteSelectContainer />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Subfamily" />
        <Form.Item>
          {getFieldDecorator("subFamily", {
            initialValue: current.name,
          })(<Input placeholder="Subfamily" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectSubFamilyForm;
