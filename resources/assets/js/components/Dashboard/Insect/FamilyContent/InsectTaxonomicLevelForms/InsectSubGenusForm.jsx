import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";
import InsectGenusRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectGenusRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectSubGenusForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Genus" />
        <Form.Item>
          {getFieldDecorator("genus_id", {
            rules: rules.requiredField,
            initialValue: current.genus && current.genus.id,
          })(<InsectGenusRemoteSelectContainer />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Subgenus" />
        <Form.Item>
          {getFieldDecorator("subGenus", {
            initialValue: current.name,
          })(<Input placeholder="Subgenus" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectSubGenusForm;
