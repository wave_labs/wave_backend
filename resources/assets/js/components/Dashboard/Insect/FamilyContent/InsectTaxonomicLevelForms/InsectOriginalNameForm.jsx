import React, { Component } from "react";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  Button,
  Popover,
  DatePicker,
  Select,
  Collapse,
} from "antd";
import moment from "moment";
import InsectReferenceRemoteSelectContainer from "../InsectReferenceRemoteSelectContainer";
import InsectGenusRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectGenusRemoteSelectContainer";
import InsectSubGenusRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectSubGenusRemoteSelectContainer";
import InsectSpecificRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectSpecificRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);
const { Panel } = Collapse;

const StyledTitle = styled.div`
  font-size: 1.5rem;
  font-weight: bold;
`;

class InsectOriginalNameForm extends Component {
  state = {
    species: 0,
    formType: "",
  };

  rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { current } = this.props;

    const explanations = {
      kingdom:
        "The full scientific name of the kingdom in which the taxon is classified. E.g Animalia, Plantae",
      author:
        "The authorship information for the scientific name formatted according to the conventions of the applicable nomenclaturalCode",
      date: "Year that the scientific name was firstly used",
      reference:
        "The reference to the source in which the specific taxon concept circumscription is defined or implied",
      taxonRank:
        "The taxonomic rank of the most specific name in the scientificName. Recommended best practice is to use this controlled vocabulary: http://rs.gbif.org/vocabulary/gbif/rank_2015-04-24.xml E.g species, genus",
      taxonRemarks:
        "Comments or notes about the taxon or name, E.g Type consists of a skull and skeletal fragments",
      language:
        "The language of the parent resource. Recommended best practice is to use a controlled vocabulary such as ISO 693 e.g eng",
      rights:
        "Information about rights held in and over the resource. Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights",
      license:
        "A legal document giving official permission to do something with the resource",
      institutionID:
        "An identifier for the institution having custody of the object(s) or information referred to in the record",
      institutionCode:
        "The name (or acronym) in use by the institution having custody of the object(s) or information referred to in the record",
      rightSholder:
        "A person or organization owning or managing rights over the resource",
      accessRights:
        "Information about who can access the resource or an indication of its security status. Access Rights may include information regarding access or restrictions based on privacy, security, or other policies. E.g not-for-profit use only",
      source:
        "Used to link to an external representation of the data record such as in a source web database.A URI link or reference to the source of this record",
      datasetID:
        "An identifier for a (sub) dataset. Ideally globally unique, but any id allowed",
      datasetName:
        "The title of the (sub) dataset optionally also referenced via datasetID",
    };

    const taxonRankOptions = [
      "Kingdom",
      "Phylum",
      "Class",
      "Order",
      "Suborder",
      "Infraorder",
      "Superfamily",
      "Family",
      "Subfamily",
      "Tribe",
      "Genus",
      "Subgenus",
      "Species",
      "Subspecies",
    ];

    return (
      <Row gutter={16}>
        <StyledTitle>Original Name</StyledTitle>
        <Col xs={24} md={8} lg={6}>
          <Specifications label="Genus" />
          <Form.Item>
            {getFieldDecorator("originalNameGenus", {
              initialValue: current.originalName
                ? current.originalName.originalNameGenus
                : "",
            })(<Input placeholder="Genus" />)}
          </Form.Item>
        </Col>
        <Col xs={24} md={8} lg={6}>
          <Specifications label="Subgenus" content={explanations["subgenus"]} />
          <Form.Item>
            {getFieldDecorator("originalNameSubGenus", {
              initialValue: current.originalName
                ? current.originalName.originalNameSubGenus
                : "",
            })(<Input placeholder="Subgenus" />)}
          </Form.Item>
        </Col>

        <Col xs={24} md={8} lg={6}>
          <Specifications
            label="Specific Epithet"
            content={explanations["specificEpithet"]}
          />
          <Form.Item>
            {getFieldDecorator("originalNameSpecificEpithet", {
              initialValue: current.originalName
                ? current.originalName.originalNameSpecificEpithet
                : "",
            })(<Input placeholder="Specific Epithet" />)}
          </Form.Item>
        </Col>

        <Col xs={24} md={8} lg={6}>
          <Specifications
            label="Infra Specific Epithet"
            content={explanations["infraSpecificEpithet"]}
          />
          <Form.Item>
            {getFieldDecorator("originalNameInfraSpecificEpithet", {
              initialValue: current.originalName
                ? current.originalName.originalNameInfraSpecificEpithet
                : "",
            })(<Input placeholder="Infra Specific Epithet" />)}
          </Form.Item>
        </Col>

        <Col xs={24} md={8} lg={6}>
          <Specifications label="Year" content={explanations["date"]} />
          <Form.Item>
            {getFieldDecorator("originalNameDate", {
              initialValue: current.originalName
                ? current.originalName.originalNameDate
                  ? moment(current.originalName.originalNameDate)
                  : null
                : "",
            })(
              <DatePicker
                format={"YYYY"}
                mode="year"
                onPanelChange={(v) => {
                  this.props.form.setFieldsValue({ originalNameDate: v });
                }}
              />
            )}
          </Form.Item>
        </Col>
        <Col xs={24} md={16} lg={12}>
          <Specifications
            label="Reference"
            content={explanations["reference"]}
          />
          <Form.Item>
            {getFieldDecorator("originalNameReference_id", {
              initialValue:
                current.originalName && current.originalName.reference
                  ? current.originalName.reference.id
                  : null,
            })(<InsectReferenceRemoteSelectContainer />)}
          </Form.Item>
        </Col>

        <Col xs={24} md={8} lg={6}>
          <Specifications
            label="Taxon Rank"
            content={explanations["taxonRank"]}
          />
          <Form.Item>
            {getFieldDecorator("originalNameTaxonRank", {
              initialValue:
                current.originalName &&
                current.originalName.originalNameTaxonRank,
            })(
              <Select placeholder="Taxon Rank">
                {taxonRankOptions.map((option) => (
                  <Option value={option}>{option}</Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>

        <Col xs={24} md={8} lg={6}>
          <Specifications
            label="Taxon Remarks"
            content={explanations["taxonRemarks"]}
          />
          <Form.Item>
            {getFieldDecorator("originalNameTaxonRemarks", {
              initialValue: current.originalName
                ? current.originalName.originalNameTaxonRemarks
                : "",
            })(<Input placeholder="Taxon Remarks" />)}
          </Form.Item>
        </Col>

        <Col xs={24}>
          <Collapse>
            <Panel
              header="Expand to fill dataset and collection information"
              key="1"
            >
              <Col xs={24} md={8} lg={6}>
                <Specifications
                  label="Language"
                  content={explanations["language"]}
                />
                <Form.Item>
                  {getFieldDecorator("originalNameLanguage", {
                    initialValue: current.originalName
                      ? current.originalName.originalNameLanguage
                      : "",
                  })(<Input placeholder="Language" />)}
                </Form.Item>
              </Col>
              <Col xs={24} md={8} lg={6}>
                <Specifications
                  label="Rights"
                  content={explanations["rights"]}
                />
                <Form.Item>
                  {getFieldDecorator("originalNameRights", {
                    initialValue: current.originalName
                      ? current.originalName.originalNameRights
                      : "",
                  })(<Input placeholder="Rights" />)}
                </Form.Item>
              </Col>
              <Col xs={24} md={8} lg={6}>
                <Specifications
                  label="License"
                  content={explanations["license"]}
                />
                <Form.Item>
                  {getFieldDecorator("originalNameLicense", {
                    initialValue: current.originalName
                      ? current.originalName.originalNameLicense
                      : "",
                  })(<Input placeholder="License" />)}
                </Form.Item>
              </Col>
              <Col xs={24} md={8} lg={6}>
                <Specifications
                  label="Institution ID"
                  content={explanations["institutionID"]}
                />
                <Form.Item>
                  {getFieldDecorator("originalNameInstitutionID", {
                    initialValue: current.originalName
                      ? current.originalName.originalNameInstitutionID
                      : "",
                  })(<Input placeholder="Institution ID" />)}
                </Form.Item>
              </Col>
              <Col xs={24} md={8} lg={6}>
                <Specifications
                  label="institution Code"
                  content={explanations["institutionCode"]}
                />
                <Form.Item>
                  {getFieldDecorator("originalNameInstitutionCode", {
                    initialValue: current.originalName
                      ? current.originalName.originalNameInstitutionCode
                      : "",
                  })(<Input placeholder="institution Code" />)}
                </Form.Item>
              </Col>
              <Col xs={24} md={8} lg={6}>
                <Specifications
                  label="Right Sholder"
                  content={explanations["rightSholder"]}
                />
                <Form.Item>
                  {getFieldDecorator("originalNameRightSholder", {
                    initialValue: current.originalName
                      ? current.originalName.originalNameRightSholder
                      : "",
                  })(<Input placeholder="Right Sholder" />)}
                </Form.Item>
              </Col>
              <Col xs={24} md={8} lg={6}>
                <Specifications
                  label="Access Rights"
                  content={explanations["accessRights"]}
                />
                <Form.Item>
                  {getFieldDecorator("originalNameAccessRights", {
                    initialValue: current.originalName
                      ? current.originalName.originalNameAccessRights
                      : "",
                  })(<Input placeholder="Access Rights" />)}
                </Form.Item>
              </Col>
              <Col xs={24} md={8} lg={6}>
                <Specifications
                  label="Source"
                  content={explanations["source"]}
                />
                <Form.Item>
                  {getFieldDecorator("originalNameSource", {
                    initialValue: current.originalName
                      ? current.originalName.originalNameSource
                      : "",
                  })(<Input placeholder="originalNameSource" />)}
                </Form.Item>
              </Col>
              <Col xs={24} md={8} lg={6}>
                <Specifications
                  label="Dataset ID"
                  content={explanations["datasetID"]}
                />
                <Form.Item>
                  {getFieldDecorator("originalNameDatasetID", {
                    initialValue: current.originalName
                      ? current.originalName.originalNameDatasetID
                      : "",
                  })(<Input placeholder="Dataset ID" />)}
                </Form.Item>
              </Col>
              <Col xs={24} md={8} lg={6}>
                <Specifications
                  label="Dataset Name"
                  content={explanations["datasetName"]}
                />
                <Form.Item>
                  {getFieldDecorator("originalNameDatasetName", {
                    initialValue: current.originalName
                      ? current.originalName.originalNameDatasetName
                      : "",
                  })(<Input placeholder="Dataset Name" />)}
                </Form.Item>
              </Col>
            </Panel>
          </Collapse>
        </Col>
      </Row>
    );
  }
}

export default InsectOriginalNameForm;
