import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";
import InsectOrderRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectOrderRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectSubOrderForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Order" />
        <Form.Item>
          {getFieldDecorator("order_id", {
            rules: rules.requiredField,
            initialValue: current.order && current.order.id,
          })(<InsectOrderRemoteSelectContainer />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Suborder" />
        <Form.Item>
          {getFieldDecorator("subOrder", {
            initialValue: current.name,
          })(<Input placeholder="Suborder" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectSubOrderForm;
