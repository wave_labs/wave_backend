import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";
import InsectKingdomRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectKingdomRemoteSelectContainer";
import { fetchInsectKingdomSelector } from "redux-modules/insectSpecies/actions";
import { connect } from "react-redux";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectPhylumForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Kingdom" />
        <Form.Item>
          {getFieldDecorator("kingdom_id", {
            rules: rules.requiredField,
            initialValue: current.kingdom && current.kingdom.id,
          })(<InsectKingdomRemoteSelectContainer />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Phylum" />
        <Form.Item>
          {getFieldDecorator("phylum", {
            rules: rules.requiredField,
            initialValue: current.name,
          })(<Input placeholder="Phylum" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectPhylumForm;

/* const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectKingdomSelector: (filters) =>
      dispatch(fetchInsectKingdomSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectKingdoms: state.insectSpecies.basicTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InsectPhylumForm);
 */
