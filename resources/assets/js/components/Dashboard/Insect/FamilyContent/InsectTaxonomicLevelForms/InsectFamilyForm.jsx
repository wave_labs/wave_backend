import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import InsectKingdomRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectKingdomRemoteSelectContainer";
import InsectOrderRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectOrderRemoteSelectContainer";
import InsectSubOrderRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectSubOrderRemoteSelectContainer";
import InsectInfraOrderRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectInfraOrderRemoteSelectContainer";
import InsectSuperFamilyRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectSuperFamilyRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectFamilyForm(props) {
  const { current, getFieldDecorator, form } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Order" />
        <Form.Item>
          {getFieldDecorator("order_id", {
            rules: rules.requiredField,
            initialValue: current.order && current.order.id,
          })(<InsectOrderRemoteSelectContainer />)}
        </Form.Item>
      </Col>

      <Col xs={24} md={12} lg={8}>
        <Specifications label="Suborder" />
        <Form.Item>
          {getFieldDecorator("sub_order_id", {
            initialValue: current.subOrder && current.subOrder.id,
          })(<InsectSubOrderRemoteSelectContainer orderId={form.getFieldValue('order_id')}/>)}
        </Form.Item>
      </Col>

      <Col xs={24} md={12} lg={8}>
        <Specifications label="Infraorder" />
        <Form.Item>
          {getFieldDecorator("infra_order_id", {
            initialValue: current.infraOrder && current.infraOrder.id,
          })(<InsectInfraOrderRemoteSelectContainer subOrderId={form.getFieldValue('sub_order_id')}/>)}
        </Form.Item>
      </Col>

      <Col xs={24} md={12} lg={8}>
        <Specifications label="Superfamily" />
        <Form.Item>
          {getFieldDecorator("super_family_id", {
            initialValue: current.superFamily && current.superFamily.id,
          })(<InsectSuperFamilyRemoteSelectContainer infraOrderId={form.getFieldValue('infra_order_id')}/>)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Family" />
        <Form.Item>
          {getFieldDecorator("family", {
            rules: rules.requiredField,
            initialValue: current.name,
          })(<Input placeholder="Family" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectFamilyForm;

/* const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectKingdomSelector: (filters) =>
      dispatch(fetchInsectKingdomSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectKingdoms: state.insectSpecies.basicTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InsectFamilyForm);
 */
