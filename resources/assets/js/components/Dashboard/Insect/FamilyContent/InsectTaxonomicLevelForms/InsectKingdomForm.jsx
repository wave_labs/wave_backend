import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React, { useState } from "react";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectKingdomForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Kingdom" />
        <Form.Item>
          {getFieldDecorator("kingdom", {
            rules: rules.requiredField,
            initialValue: current.name,
          })(<Input placeholder="Kingdom" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectKingdomForm;
