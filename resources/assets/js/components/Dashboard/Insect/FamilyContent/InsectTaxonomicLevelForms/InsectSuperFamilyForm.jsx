import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";
import InsectInfraOrderRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectInfraOrderRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectSuperFamilyForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Infraorder" />
        <Form.Item>
          {getFieldDecorator("infra_order_id", {
            rules: rules.requiredField,
            initialValue: current.infraOrder && current.infraOrder.id,
          })(<InsectInfraOrderRemoteSelectContainer />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Superfamily" />
        <Form.Item>
          {getFieldDecorator("superFamily", {
            initialValue: current.name,
          })(<Input placeholder="Superfamily" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectSuperFamilyForm;
