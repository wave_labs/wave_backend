import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import InsectGenusRemoteSelectContainer from"../InsectTaxonomicRemoteContainers/InsectGenusRemoteSelectContainer";
import InsectSubGenusRemoteSelectContainer from"../InsectTaxonomicRemoteContainers/InsectSubGenusRemoteSelectContainer";



const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectSpecificForm(props) {
  const { current, getFieldDecorator, form } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Genus" />
        <Form.Item>
          {getFieldDecorator("genus_id", {
            rules: rules.requiredField,
            initialValue: current.genus && current.genus.id,
          })(<InsectGenusRemoteSelectContainer />)}
        </Form.Item>
      </Col>

      <Col xs={24} md={12} lg={8}>
        <Specifications label="Subgenus" />
        <Form.Item>
          {getFieldDecorator("sub_genus_id", {
            initialValue: current.subGenus && current.subGenus.id,
          })(<InsectSubGenusRemoteSelectContainer genusId={form.getFieldValue('genus_id')}/>)}
        </Form.Item>
      </Col>


      <Col xs={24} md={12} lg={8}>
        <Specifications label="Specific" />
        <Form.Item>
          {getFieldDecorator("specific", {
            rules: rules.requiredField,
            initialValue: current.name,
          })(<Input placeholder="Specific" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectSpecificForm;

/* const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectKingdomSelector: (filters) =>
      dispatch(fetchInsectKingdomSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectKingdoms: state.insectSpecies.basicTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InsectSpecificForm);
 */
