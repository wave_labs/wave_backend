import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";
import InsectClassRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectClassRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectOrderForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Class" />
        <Form.Item>
          {getFieldDecorator("class_id", {
            rules: rules.requiredField,
            initialValue: current.class && current.class.id,
          })(<InsectClassRemoteSelectContainer />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Order" />
        <Form.Item>
          {getFieldDecorator("order", {
            initialValue: current.name,
          })(<Input placeholder="Order" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectOrderForm;
