import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import InsectFamilyRemoteSelectContainer from"../InsectTaxonomicRemoteContainers/InsectFamilyRemoteSelectContainer";
import InsectSubFamilyRemoteSelectContainer from"../InsectTaxonomicRemoteContainers/InsectSubFamilyRemoteSelectContainer";
import InsectTribeRemoteSelectContainer from"../InsectTaxonomicRemoteContainers/InsectTribeRemoteSelectContainer";



const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectGenusForm(props) {
  const { current, getFieldDecorator, form } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Family" />
        <Form.Item>
          {getFieldDecorator("family_id", {
            rules: rules.requiredField,
            initialValue: current.family && current.family.id,
          })(<InsectFamilyRemoteSelectContainer />)}
        </Form.Item>
      </Col>

      <Col xs={24} md={12} lg={8}>
        <Specifications label="Subfamily" />
        <Form.Item>
          {getFieldDecorator("sub_family_id", {
            initialValue: current.subFamily && current.subFamily.id,
          })(<InsectSubFamilyRemoteSelectContainer familyId={form.getFieldValue('family_id')}/>)}
        </Form.Item>
      </Col>

      <Col xs={24} md={12} lg={8}>
        <Specifications label="Tribe" />
        <Form.Item>
          {getFieldDecorator("tribe_id", {
            initialValue: current.tribe && current.tribe.id,
          })(<InsectTribeRemoteSelectContainer subFamilyId={form.getFieldValue('sub_family_id')}/>)}
        </Form.Item>
      </Col>

      <Col xs={24} md={12} lg={8}>
        <Specifications label="Genus" />
        <Form.Item>
          {getFieldDecorator("genus", {
            rules: rules.requiredField,
            initialValue: current.name,
          })(<Input placeholder="Genus" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectGenusForm;

/* const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectKingdomSelector: (filters) =>
      dispatch(fetchInsectKingdomSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    insectKingdoms: state.insectSpecies.basicTaxonSelector,
    loading: state.insectSpecies.loading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InsectGenusForm);
 */
