import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";
import InsectPhylumRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectPhylumRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectClassForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Phylum" />
        <Form.Item>
          {getFieldDecorator("phylum_id", {
            rules: rules.requiredField,
            initialValue: current.phylum && current.phylum.id,
          })(<InsectPhylumRemoteSelectContainer />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Class" />
        <Form.Item>
          {getFieldDecorator("class", {
            rules: rules.requiredField,
            initialValue: current.name,
          })(<Input placeholder="Class" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectClassForm;
