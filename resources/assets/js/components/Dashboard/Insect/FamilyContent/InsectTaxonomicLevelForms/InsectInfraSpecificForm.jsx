import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";
import InsectSpecificRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectSpecificRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectClassForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Specie" />
        <Form.Item>
          {getFieldDecorator("specific_id", {
            rules: rules.requiredField,
            initialValue: current.specific && current.specific.id,
          })(<InsectSpecificRemoteSelectContainer />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Subspecie" />
        <Form.Item>
          {getFieldDecorator("infraSpecific", {
            rules: rules.requiredField,
            initialValue: current.name,
          })(<Input placeholder="Subspecie" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectClassForm;
