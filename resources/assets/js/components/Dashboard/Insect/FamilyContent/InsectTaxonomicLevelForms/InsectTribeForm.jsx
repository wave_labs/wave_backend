import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";
import InsectFamilyRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectFamilyRemoteSelectContainer";
import InsectSubFamilyRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectSubFamilyRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectTribeForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Subfamily" />
        <Form.Item>
          {getFieldDecorator("sub_family_id", {
            rules: rules.requiredField,
            initialValue: current.subFamily && current.subFamily.id,
          })(<InsectSubFamilyRemoteSelectContainer />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Tribe" />
        <Form.Item>
          {getFieldDecorator("tribe", {
            initialValue: current.name,
          })(<Input placeholder="Tribe" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectTribeForm;
