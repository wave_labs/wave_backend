import { Col, Form, Input, Row, Select, DatePicker, Popover, Icon } from "antd";
import moment from "moment";
import React from "react";
import InsectSubOrderRemoteSelectContainer from "../InsectTaxonomicRemoteContainers/InsectSubOrderRemoteSelectContainer";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

function InsectInfraOrderForm(props) {
  const { current, getFieldDecorator } = props;

  const rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  return (
    <>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Suborder" />
        <Form.Item>
          {getFieldDecorator("sub_order_id", {
            rules: rules.requiredField,
            initialValue: current.subOrder && current.subOrder.id,
          })(<InsectSubOrderRemoteSelectContainer />)}
        </Form.Item>
      </Col>
      <Col xs={24} md={12} lg={8}>
        <Specifications label="Infraorder" />
        <Form.Item>
          {getFieldDecorator("infraOrder", {
            initialValue: current.name,
          })(<Input placeholder="Infraorder" />)}
        </Form.Item>
      </Col>
    </>
  );
}

export default InsectInfraOrderForm;
