import React, { Component } from "react";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  Button,
  Popover,
  DatePicker,
  Select,
  Collapse,
} from "antd";
import moment from "moment";
import InsectReferenceRemoteSelectContainer from "./InsectReferenceRemoteSelectContainer";
import InsectKingdomForm from "./InsectTaxonomicLevelForms/InsectKingdomForm";
import InsectPhylumForm from "./InsectTaxonomicLevelForms/InsectPhylumForm";
import InsectClassForm from "./InsectTaxonomicLevelForms/InsectClassForm";
import InsectOrderForm from "./InsectTaxonomicLevelForms/InsectOrderForm";
import InsectSubOrderForm from "./InsectTaxonomicLevelForms/InsectSubOrderForm";
import InsectFamilyForm from "./InsectTaxonomicLevelForms/InsectFamilyForm";
import InsectOriginalNameForm from "./InsectTaxonomicLevelForms/InsectOriginalNameForm";
import InsectInfraOrderForm from "./InsectTaxonomicLevelForms/InsectInfraOrderForm";
import InsectSuperFamilyForm from "./InsectTaxonomicLevelForms/InsectSuperFamilyForm";
import InsectSubFamilyForm from "./InsectTaxonomicLevelForms/InsectSubFamilyForm";
import InsectTribeForm from "./InsectTaxonomicLevelForms/InsectTribeForm";
import InsectGenusForm from "./InsectTaxonomicLevelForms/InsectGenusForm";
import InsectSubGenusForm from "./InsectTaxonomicLevelForms/InsectSubGenusForm";
import InsectSpecificForm from "./InsectTaxonomicLevelForms/InsectSpecificForm";
import InsectInfraSpecificForm from "./InsectTaxonomicLevelForms/InsectInfraSpecificForm";

const Specifications = ({ label, content }) => (
  <div>
    {label}
    <Popover content={content} trigger="hover">
      <Icon type="question-circle" />
    </Popover>
  </div>
);

const { Panel } = Collapse;

const StyledTitle = styled.div`
  font-size: 1.5rem;
  font-weight: bold;
`;

class InsectSpeciesForm extends Component {
  state = {
    formType: "",
  };

  rules = {
    requiredField: [
      {
        required: true,
        message: "Field is required",
      },
    ],
  };

  handleFormType = (value) => {
    this.setState({formType: value});
  };

  componentDidMount = () => {
    console.log(this.props.current);
    if (this.props.current) {
      this.handleFormType(this.props.current.taxonRank);
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { current } = this.props;

    const explanations = {
      kingdom:
        "The full scientific name of the kingdom in which the taxon is classified. E.g Animalia, Plantae",
      author:
        "The authorship information for the scientific name formatted according to the conventions of the applicable nomenclaturalCode",
      date: "Year that the scientific name was firstly used",
      reference:
        "The reference to the source in which the specific taxon concept circumscription is defined or implied",
      taxonRank:
        "The taxonomic rank of the most specific name in the scientificName. Recommended best practice is to use this controlled vocabulary: http://rs.gbif.org/vocabulary/gbif/rank_2015-04-24.xml E.g species, genus",
      taxonRemarks:
        "Comments or notes about the taxon or name, E.g Type consists of a skull and skeletal fragments",
      language:
        "The language of the parent resource. Recommended best practice is to use a controlled vocabulary such as ISO 693 e.g eng",
      rights:
        "Information about rights held in and over the resource. Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights",
      license:
        "A legal document giving official permission to do something with the resource",
      institutionID:
        "An identifier for the institution having custody of the object(s) or information referred to in the record",
      institutionCode:
        "The name (or acronym) in use by the institution having custody of the object(s) or information referred to in the record",
      rightSholder:
        "A person or organization owning or managing rights over the resource",
      accessRights:
        "Information about who can access the resource or an indication of its security status. Access Rights may include information regarding access or restrictions based on privacy, security, or other policies. E.g not-for-profit use only",
      source:
        "Used to link to an external representation of the data record such as in a source web database.A URI link or reference to the source of this record",
      datasetID:
        "An identifier for a (sub) dataset. Ideally globally unique, but any id allowed",
      datasetName:
        "The title of the (sub) dataset optionally also referenced via datasetID",
    };

    const taxonRankOptions = [
      "Kingdom",
      "Phylum",
      "Class",
      "Order",
      "Suborder",
      "Infraorder",
      "Superfamily",
      "Family",
      "Subfamily",
      "Tribe",
      "Genus",
      "Subgenus",
      "Species",
      "Subspecies",
    ];

    return (
      <Form>
        <Row gutter={16}>
          {/* valid name form */}
          <StyledTitle>Valid Name</StyledTitle>
          <Col xs={24} md={8} lg={6}>
            <Specifications
              label="Taxon Rank"
              content={explanations["taxonRank"]}
            />
            <Form.Item>
              {getFieldDecorator("taxonRank", {
                rules: this.rules.requiredField,
                initialValue: current.taxonRank,
              })(
                <Select placeholder="Taxon Rank" onChange={this.handleFormType}>
                  {taxonRankOptions.map((option) => (
                    <Option value={option}>{option}</Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          </Col>
          {(() => {
            switch (this.state.formType) {
              case "Kingdom":
                return (
                  <InsectKingdomForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              case "Phylum":
                return (
                  <InsectPhylumForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              case "Class":
                return (
                  <InsectClassForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              case "Order":
                return (
                  <InsectOrderForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              case "Suborder":
                return (
                  <InsectSubOrderForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              case "Infraorder":
                return (
                  <InsectInfraOrderForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              case "Superfamily":
                return (
                  <InsectSuperFamilyForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              case "Family":
                return (
                  <InsectFamilyForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                    form={this.props.form}
                  />
                );
              case "Subfamily":
                return (
                  <InsectSubFamilyForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              case "Tribe":
                return (
                  <InsectTribeForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              case "Genus":
                return (
                  <InsectGenusForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                    form={this.props.form}
                  />
                );
              case "Subgenus":
                return (
                  <InsectSubGenusForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              case "Species":
                return (
                  <InsectSpecificForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                    form={this.props.form}
                  />
                );
              case "Subspecies":
                return (
                  <InsectInfraSpecificForm
                    current={current}
                    getFieldDecorator={getFieldDecorator}
                  />
                );
              default:
                return null;
            }
          })()}
          {/* <Col xs={24} md={8} lg={6}>
            <Specifications label="Year" content={explanations["date"]} />
            <Form.Item>
              {getFieldDecorator("date", {
                initialValue: current.year ? moment(current.year) : null,
              })(
                <DatePicker
                  format={"YYYY"}
                  mode="year"
                  onPanelChange={(v) => {
                    this.props.form.setFieldsValue({ date: v });
                  }}
                />
              )}
            </Form.Item>
          </Col> */}
          <Col xs={24} md={16} lg={12}>
            <Specifications
              label="Reference"
              content={explanations["reference"]}
            />
            <Form.Item>
              {getFieldDecorator("reference_id", {
                rules: this.rules.requiredField,
                initialValue: current.reference && current.reference.id,
              })(<InsectReferenceRemoteSelectContainer />)}
            </Form.Item>
          </Col>

          <Col xs={24} md={8} lg={6}>
            <Specifications
              label="Taxon Remarks"
              content={explanations["taxonRemarks"]}
            />
            <Form.Item>
              {getFieldDecorator("taxonRemarks", {
                initialValue: current.taxonRemarks,
              })(<Input placeholder="Taxon Remarks" />)}
            </Form.Item>
          </Col>

          <Col xs={24}>
            <Collapse>
              <Panel
                header="Expand to fill dataset and collection information"
                key="1"
              >
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Language"
                    content={explanations["language"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("language", {
                      initialValue: current.language,
                    })(<Input placeholder="Language" />)}
                  </Form.Item>
                </Col>

                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Rights"
                    content={explanations["rights"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("rights", {
                      initialValue: current.rights,
                    })(<Input placeholder="Rights" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="License"
                    content={explanations["license"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("license", {
                      initialValue: current.license,
                    })(<Input placeholder="License" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Institution ID"
                    content={explanations["institutionID"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("institutionID", {
                      initialValue: current.institutionID,
                    })(<Input placeholder="Institution ID" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="institution Code"
                    content={explanations["institutionCode"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("institutionCode", {
                      initialValue: current.institutionCode,
                    })(<Input placeholder="institution Code" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Right Sholder"
                    content={explanations["rightSholder"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("rightSholder", {
                      initialValue: current.rightSholder,
                    })(<Input placeholder="Right Sholder" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Access Rights"
                    content={explanations["accessRights"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("accessRights", {
                      initialValue: current.accessRights,
                    })(<Input placeholder="Access Rights" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Source"
                    content={explanations["source"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("source", {
                      initialValue: current.source,
                    })(<Input placeholder="Source" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Dataset ID"
                    content={explanations["datasetID"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("datasetID", {
                      initialValue: current.datasetID,
                    })(<Input placeholder="Dataset ID" />)}
                  </Form.Item>
                </Col>
                <Col xs={24} md={8} lg={6}>
                  <Specifications
                    label="Dataset Name"
                    content={explanations["datasetName"]}
                  />
                  <Form.Item>
                    {getFieldDecorator("datasetName", {
                      initialValue: current.datasetName,
                    })(<Input placeholder="Dataset Name" />)}
                  </Form.Item>
                </Col>
              </Panel>
            </Collapse>
          </Col>
        </Row>

        {(() => {
          switch (this.state.formType) {
            case "Species":
            case "Subspecies":
              return (
                <InsectOriginalNameForm
                  current={current}
                  form={this.props.form}
                />
              );
            default:
              return null;
          }
        })()}
      </Form>
    );
  }
}

export default Form.create()(InsectSpeciesForm);
