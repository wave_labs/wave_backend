import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchInsectReferencesSelector } from "redux-modules/insectReference/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";
import moment from "moment";

const Option = Select.Option;

class InsectReferenceRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchInsectReferencesSelector({
      search: search,
      project: this.props.projectId,
    });
  };

  componentDidMount = () => {
    this.props.fetchInsectReferencesSelector({ project: this.props.projectId });
  };

  getAuthors = (authorList) => {
    var authors = "";
    if (authorList) {
      authorList.forEach((author) => (authors = authors + author + ", "));
    }
    return authors;
  };

  getPublication = (
    title,
    conferenceTittle,
    journal,
    chapterTittle,
    date,
    authorList
  ) => {
    var publication = "";
    if (title) {
      publication = publication + title + ", ";
    }
    if (conferenceTittle) {
      publication = publication + conferenceTittle + ", ";
    }
    if (journal) {
      publication = publication + journal + ", ";
    }
    if (chapterTittle) {
      publication = publication + chapterTittle + ", ";
    }
    publication = publication + this.getAuthors(authorList);
    if (date) {
      publication = publication + moment(date).format("YYYY");
    }
    return publication;
  };

  render() {
    const {
      InsectReferenceSelector,
      loading,
      value,
      onChange,
      placeholder = "Reference",
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
      >
        {console.log(value)}
        {InsectReferenceSelector.map((reference) => (
          <Option value={reference.id} key={reference.id}>
            {this.getPublication(
              reference.title,
              reference.conferenceTittle,
              reference.journal,
              reference.chapterTittle,
              reference.date,
              reference.authors
            )}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchInsectReferencesSelector: (filters) =>
      dispatch(fetchInsectReferencesSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    InsectReferenceSelector: state.insectReference.selectorReferences,
    loading: state.insectReference.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InsectReferenceRemoteSelectContainer);
