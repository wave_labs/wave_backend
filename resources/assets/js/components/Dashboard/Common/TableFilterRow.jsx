import React from "react";
import { Row, Col, Button, Input } from "antd";

const Search = Input.Search;

let TableFilterRow = ({
  onSearch,
  searchPlaceholder,
  onCreateClick,
  children,
}) => {
  return (
    <Row type="flex" gutter={16}>
      {onSearch && (
        <Col>
          <Search
            placeholder={searchPlaceholder}
            onSearch={(search) => onSearch(search)}
          />
        </Col>
      )}
      {children}
      {onCreateClick && (
        <Col style={{ marginLeft: "auto" }}>
          <Button onClick={() => onCreateClick()}> Create </Button>
        </Col>
      )}
    </Row>
  );
};

export default TableFilterRow;
