import React from "react";
import { Table } from "antd";
import styled from "styled-components";

const StyledTable = styled(Table)`
  padding: 16px 0;
  width: 100%;
  & td {
    white-space: pre-wrap;
  }
`;

const TablePagination = ({
    handleTableChange,
    meta,
    loading,
    columns,
    data,
    expandedRowRender,
    onRow,
    showSizeChanger = false,
    showQuickJumper = true,
    childrenColumnName = "children"
}) => {
    // PAGINATION OPTIONS
    const pagination = {
        showSizeChanger: showSizeChanger,
        showQuickJumper: showQuickJumper,
        total: meta.total,
        showTotal: (total, range) => `${range[0]} to ${range[1]} of ${total}`,
        total: meta.total,
        current: meta.current_page,
        pageSize: meta.per_page,

    };
    return (
        <StyledTable
            rowClassName="table-row"
            expandedRowRender={expandedRowRender}
            onRow={onRow}
            scroll={{ x: true }}
            rowKey={(record) => record.id}
            loading={loading}
            columns={columns}
            dataSource={data}
            pagination={pagination}
            onChange={handleTableChange}
            size="middle"
            childrenColumnName={childrenColumnName}
        />
    );
};

export default TablePagination;
