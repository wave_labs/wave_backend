import React from "react";
import { Select, Spin } from "antd";

const SelectSearch = ({
  loading,
  onSearch,
  children,
  value,
  onChange,
  placeholder,
  mode,
  dropdownRender,
  onSelect,
  disabled = false
}) => {
  return (
    <Select
      value={value}
      onChange={onChange}
      showSearch
      placeholder={placeholder}
      notFoundContent={loading ? <Spin size="small" /> : null}
      filterOption={false}
      showArrow={true}
      onSearch={onSearch}
      style={{ width: "100%" }}
      mode={mode ? mode : "default"}
      dropdownRender={dropdownRender}
      onSelect={onSelect}
      disabled={disabled}
    >
      {children}
    </Select>
  );
};

export default SelectSearch;
