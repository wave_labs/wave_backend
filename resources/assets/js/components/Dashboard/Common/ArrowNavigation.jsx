import React, { Component } from "react";
import { LeftCircleOutlined, RightCircleOutlined } from "@ant-design/icons";
import styled from "styled-components";
import { Link } from "react-router-dom";

const NavigationContainer = styled.div`
  width: 90%;
  margin: 2% auto;
  display: block;
  position: relative;
  height: 26px;
`;
const StyledLeftCircleOutlined = styled(LeftCircleOutlined)`
  position: absolute;
  left: 0;
`;
const StyledRightCircleOutlined = styled(RightCircleOutlined)`
  right: 0;
  position: absolute;
`;

class ArrowNavigation extends Component {
  render() {
    const { prior, next, url } = this.props;

    return (
      <NavigationContainer>
        {Object.keys(prior).length !== 0 && (
          <Link to={`${url}${prior.id}`}>
            <StyledLeftCircleOutlined style={{ fontSize: "26px" }} />
          </Link>
        )}
        {Object.keys(next).length !== 0 && (
          <Link to={`${url}${next.id}`}>
            <StyledRightCircleOutlined style={{ fontSize: "26px" }} />
          </Link>
        )}
      </NavigationContainer>
    );
  }
}

export default ArrowNavigation;
