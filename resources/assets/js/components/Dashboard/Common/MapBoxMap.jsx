import React, { Component } from "react";
import ReactMapGL, { NavigationControl } from "react-map-gl";
import { Spin } from "antd";
import "mapbox-gl/dist/mapbox-gl.css";
import { debounce } from "lodash";

const TOKEN =
  "pk.eyJ1IjoidGlnZXJ3aGFsZSIsImEiOiJjanBncmNscnAwMWx3M3ZxdDF2cW8xYWZvIn0.LVgciVtYclOed_hZ9oXY2g";

const navStyle = {
  position: "absolute",
  top: 0,
  left: 0,
  padding: "10px",
};

const bounds = [
  [32.625052, -17.260608], // Southwest coordinates
  [32.882379, -16.62892], // Northeast coordinates
];

class MapBoxMap extends Component {
  constructor(props) {
    super(props);
    this.mapRef = React.createRef();
    this.getMapBoundsDebounced = debounce(this.getMapBounds, 800);
    this.state = {
      style: "mapbox://styles/tigerwhale/cjpgrt1sccjs92sqjfnuixnxc",
      viewport: {
        width: "100%",
        height: `${this.props.size}px`,
        latitude: this.props.defaultCenter
          ? this.props.defaultCenter.lat
          : 32.6600150717693,
        longitude: this.props.defaultCenter
          ? this.props.defaultCenter.lng
          : -16.92564526551496,
        zoom: this.props.zoom ? this.props.zoom : 15,
      },
      settings: {
        dragPan: true,
        dragRotate: true,
        scrollZoom: true,
        touchZoom: true,
        touchRotate: true,
        keyboard: true,
        doubleClickZoom: true,
        minZoom: 0,
        maxZoom: 20,
        minPitch: 0,
        maxPitch: 85,
        maxBounds: this.props.bounds && bounds, // Sets bounds as max
        scrollZoom: this.props.removeZoom ? false : true,
        boxZoom: this.props.removeZoom ? false : true,
        doubleClickZoom: this.props.removeZoom ? false : true,
      },
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.defaultCenter !== this.props.defaultCenter) {
      this.setState({
        viewport: {
          width: "100%",
          height: "100%",
          latitude: this.props.defaultCenter
            ? this.props.defaultCenter.lat
            : 32.6600150717693,
          longitude: this.props.defaultCenter
            ? this.props.defaultCenter.lng
            : -16.92564526551496,
          zoom: this.props.zoom ? this.props.zoom : 10,
        },
      });
    }
  }

  onViewportChange = (viewport) => {
    this.setState({ viewport });
  };

  getMapBounds = () => {
    let bounds = [];
    if (this.mapRef.current) {
      const mapGL = this.mapRef.current.getMap();
      bounds = mapGL.getBounds();
    }
    if (this.props.setMapBounds) {
      this.props.setMapBounds(bounds)
    }
    return bounds;
  };

  getMapZoom = () => {
    let zoom = 0;
    if (this.mapRef.current) {
      const mapGL = this.mapRef.current.getMap();
      zoom = mapGL.getZoom();
    }

    return zoom;
  };

  render() {
    const { fixedSize, style } = this.props;
    this.getMapBoundsDebounced();
    return (
      <div
        id={fixedSize ? "nullable-map-container" : "map-container"}
        className={fixedSize ? "nullable-map-container" : "map-container"}
        style={{ height: this.props.size ? this.props.size : "600px" }}
      >
        <ReactMapGL
          {...this.state.viewport}
          {...this.state.settings}
          mapboxApiAccessToken={TOKEN}
          mapStyle={style ? style : this.state.style}
          onViewportChange={this.onViewportChange}
          onClick={this.props.onClick}
          style={{ cursor: "pointer !important" }}
          ref={this.mapRef}
        >
          {this.props.loading && (
            <Spin
              className={"map-loader"}
              style={{
                minWidth: "100%",
                minHeight: "100%",
                textAlign: "center",
                background: "#E4E6E8",
                zIndex: "10001",
                position: "absolute",
              }}
            />
          )}
          <div className="nav" style={navStyle}>
            <NavigationControl onViewportChange={this.onViewportChange} />
          </div>
          {this.props.children}
        </ReactMapGL>
      </div>
    );
  }
}

export default MapBoxMap;
