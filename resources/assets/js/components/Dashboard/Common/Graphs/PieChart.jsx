import React, { Component } from "react";
import { Pie } from "react-chartjs-2";
import { Empty, Row, DatePicker, Spin } from "antd";
import styled from "styled-components";
import { LeftCircleOutlined, RightCircleOutlined } from "@ant-design/icons";
import moment from "moment";

const StyledSpan = styled.span`
  color: #777777;
`;
const StyledTitle = styled.h1`
  font-size: 2em;
  text-align: center;
`;

const PieChart = ({
  handleDate,
  labelsChartPie,
  datasetChartPie,
  hasData,
  loading,
  aDate,
  title,
  yLabel,
  xLabel,
}) => {
  const options = {
    legend: {
      display: true,
      position: "right",
    },
  };

  const data = {
    labels: labelsChartPie,
    datasets: [
      {
        data: datasetChartPie,
        backgroundColor: [
          "#359811",
          "#8f85a9",
          "#f3ed12",
          "#c55bd6",
          "#97dcda",
          "#b08d6a",
          "#5451a1",
          "#e9040b",
          "#7945c3",
          "#fca89e",
          "#58f8b8",
          "#b71639",
          "#f085af",
          "#44c335",
          "#36A2EB",
          "#FF6384",
          "#4BC0C0",
          "#9e3a47",
          "#3ced0c",
          "#43c4aa",
          "#FF6384",
          "#4BC0C0",
          "#FFCE56",
          "#E7E9ED",
          "#36A2EB",
          "#3c30a5",
          "#c19a12",
          "#7520f3",
          "#ebdc0a",
          "#621725",
          "#a41bc5",
          "#c7d247",
          "#b3ddc8",
          "#2d3ae1",
          "#d599d0",
        ],
      },
    ],
  };

  function handleSpecificDateChange(e) {
    handleDate(moment(e));
  }

  function handleDateChange(next) {
    let date = moment(aDate);
    let newDate = next ? date.add(1, "M") : date.subtract(1, "M");
    handleDate(newDate);
  }

  return (
    <Row style={{ width: "100%" }}>
      <StyledTitle>{title}</StyledTitle>
      <Row
        gutter={24}
        type="flex"
        justify="space-around"
        align="middle"
        style={{ margin: "20px auto", width: "70%", minWidth: "200px" }}
      >
        <LeftCircleOutlined
          style={{ fontSize: "26px" }}
          onClick={() => handleDateChange(false)}
        />

        <DatePicker.MonthPicker
          value={moment(aDate, "YYYY-MM")}
          placeholder="Select month"
          onChange={(e) => handleSpecificDateChange(e)}
          style={{ width: "50%" }}
        />

        <RightCircleOutlined
          style={{ fontSize: "26px" }}
          onClick={() => handleDateChange(true)}
        />
      </Row>
      {loading ? (
        <Row
          type="flex"
          justify="space-around"
          align="middle"
          style={{ minHeight: "300px" }}
        >
          <Spin size="large" />
        </Row>
      ) : (
        <Row
          type="flex"
          justify="center"
          align="middle"
          style={{ width: "80%", margin: "auto", minHeight: "200px" }}
        >
          {hasData ? (
            <Pie id="chartPie" data={data} options={options} />
          ) : (
            <Empty
              image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
              imageStyle={{
                height: 60,
              }}
              description={
                <StyledSpan>You have no reports this month.</StyledSpan>
              }
            />
          )}
        </Row>
      )}
    </Row>
  );
};

export default PieChart;
