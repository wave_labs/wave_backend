import React from "react";
import { Line } from "react-chartjs-2";
import { Spin, Row } from "antd";
import styled from "styled-components";

const StyledTitle = styled.h1`
  font-size: 2em;
  text-align: center;
`;

const LineChart = ({ xAxis, yAxis, loading, title, yLabel, xLabel }) => {
  const options = {
    maintainAspectRatio: true,
    legend: {
      display: false,
    },
    scales: {
      yAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: yLabel,
            fontStyle: "bold",
          },
        },
      ],
      xAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: xLabel,
            fontStyle: "bold",
          },
        },
      ],
    },
  };

  const data = {
    labels: xAxis,
    datasets: [
      {
        fill: true,
        lineTension: 0.1,
        backgroundColor: "rgba(75,192,192,0.4)",
        borderColor: "rgba(75,192,192,1)",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgba(75,192,192,1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 5,
        pointRadius: 2,
        pointHitRadius: 10,
        data: yAxis,
      },
    ],
  };

  return (
    <Row>
      <StyledTitle>{title}</StyledTitle>
      <Row
        type="flex"
        justify="center"
        align="middle"
        style={{ minHeight: "200px" }}
      >
        {loading ? (
          <Row
            type="flex"
            justify="space-around"
            align="middle"
            style={{ minHeight: "300px" }}
          >
            <Spin size="large" />
          </Row>
        ) : (
          <Line data={data} options={options} />
        )}
      </Row>
    </Row>
  );
};

export default LineChart;
