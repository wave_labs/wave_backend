import React from "react";
import { EditOutlined, DeleteOutlined, CopyOutlined } from "@ant-design/icons";
import { Popconfirm } from "antd";
import styled from "styled-components";

const Footer = styled.div`
  width: 100%;
  height: 60px;
  border-top: 1px solid lightgray;
  display: flex;
  justify-content: space-around;
  align-items: center;
  background: rgb(250, 250, 250);
`;

const IconStyle = {
  fontSize: "22px",
  cursor: "pointer",
  color: "black",
};

const CardFooter = ({ handleDelete, handleUpdate, handleDuplicate }) => {
  return (
    <Footer>
      {handleUpdate && (
        <EditOutlined onClick={() => handleUpdate()} style={IconStyle} />
      )}

      {handleDuplicate && (
        <CopyOutlined onClick={() => handleDuplicate()} style={IconStyle} />
      )}
      <Popconfirm
        title="Do you want to delete?"
        okText="Yes"
        cancelText="No"
        onConfirm={() => handleDelete()}
      >
        <DeleteOutlined style={IconStyle} />
      </Popconfirm>
    </Footer>
  );
};

export default CardFooter;
