import React, { Component } from "react";
import { Row, Col, Button, Input, DatePicker } from "antd";
import styled from "styled-components";
import CreatureRemoteSelectContainer from "../Data/CreatureContent/CreatureRemoteSelectContainer";
import ActivityRemoteSelectContainer from "../ActivityContent/ActivityRemoteSelectContainer";
import DivingSpotRemoteSelectContainer from "../Data/DivingSpotContent/DivingSpotRemoteSelectContainer";
import moment from "moment";

const Search = Input.Search;
const RangePicker = DatePicker.RangePicker;

const RoundButton = styled(Button)`
  margin: auto;
`;

class FilterRow extends Component {
    state = {
        filterCount: 1,
        operationCount: 1,
    };

    componentDidMount() {
        let addFilters = this.props.additionalFilters
            ? this.props.additionalFilters.length
            : 0;

        this.setState({
            filterCount: this.props.filters.length + addFilters,
            operationCount: this.props.operations.length,
        });
    }

    handleFilterChange = (filter, value) => {
        this.props.handleFilterChange(filter, value);
    };

    hasFilter(filter) {
        return this.props.filters.includes(filter);
    }

    hasOperation(operation) {
        return this.props.operations.includes(operation);
    }

    render() {
        const {
            handleCreate,
            handleReset,
            handleDownload,
            filterValues,
            createIcon ="plus",
            createText =""
        } = this.props;
        let { filterCount } = this.state;

        const FilterSection = ({ active, children }) =>
            active && (
                <Col
                    xs={{
                        span: 12,
                    }}
                    lg={{
                        span: 20 / filterCount > 12 ? 12 : Math.floor(20 / filterCount),
                    }}
                >
                    {children}
                </Col>
            );

        const OperationSection = ({ active, handleOperation, icon, type, children }) => (
            <div>
                {active && (
                    <RoundButton
                        onClick={handleOperation}
                        type={type}
                        shape="round"
                        icon={icon}
                        htmlType="submit"
                    >
                        {children}
                    </RoundButton>
                )}
            </div>
        );

        return (
            <Row type="flex" gutter={8}>
                <FilterSection active={this.hasFilter("search")}>
                    <Search
                        placeholder="Search"
                        onSearch={(value) => this.handleFilterChange("search", value)}
                    />
                </FilterSection>

                <FilterSection active={this.hasFilter("creature")}>
                    <CreatureRemoteSelectContainer
                        value={filterValues.creature}
                        onChange={(value) => this.handleFilterChange("creature", value)}
                    />
                </FilterSection>

                <FilterSection active={this.hasFilter("divingSpot")}>
                    <DivingSpotRemoteSelectContainer
                        value={filterValues.divingSpot}
                        onChange={(value) => this.handleFilterChange("divingSpot", value)}
                    />
                </FilterSection>

                <FilterSection active={this.hasFilter("activity")}>
                    <ActivityRemoteSelectContainer
                        value={filterValues.activity}
                        onChange={(value) => this.handleFilterChange("activity", value)}
                    />
                </FilterSection>

                <FilterSection active={this.hasFilter("date")}>
                    <RangePicker
                        value={filterValues.date}
                        ranges={{
                            Today: [moment(), moment()],
                            "This Week": [moment().startOf("week"), moment().endOf("week")],
                            "This Month": [
                                moment().startOf("month"),
                                moment().endOf("month"),
                            ],
                            "This Year": [moment().startOf("year"), moment().endOf("year")],
                        }}
                        format="YYYY/MM/DD"
                        onChange={(value) => this.handleFilterChange("date", value)}
                    />
                </FilterSection>

                {this.props.additionalFilters &&
                    this.props.additionalFilters.map((filter, index) => (
                        <FilterSection key={index} active={true}>
                            {filter}
                        </FilterSection>
                    ))}

                <Col
                    style={{ marginLeft: "auto", maxWidth: "150px" }}
                    lg={{ span: 4 }}
                    xs={{ span: 12 }}
                >
                    <Row
                        type="flex"
                        justify={
                            this.props.operations.length <= 1 ? "end" : "space-between"
                        }
                        align="middle"
                    >
                        <OperationSection
                            active={this.hasOperation("reset")}
                            handleOperation={handleReset}
                            type="default"
                            icon="redo"
                        />
                        <OperationSection
                            active={this.hasOperation("download")}
                            handleOperation={handleDownload}
                            type="default"
                            icon="download"
                        />
                        <OperationSection
                            active={this.hasOperation("create")}
                            handleOperation={handleCreate}
                            type="primary"
                            icon={createIcon}
                        >
                            {createText}
                        </OperationSection>
                    </Row>
                </Col>
            </Row>
        );
    }
}

export default FilterRow;
