import React from "react";
import { Progress, Row } from "antd";
import styled from "styled-components";
import { roleConverter } from "helpers";

const StyledTitle = styled.div`
  font-size: 1.5rem;
  text-align: center;
`;

const StyledSubTitle = styled(StyledTitle)`
  font-size: 1rem;
  color: #777777;
`;

const StyledImageContainer = styled.div`
  width: 100%;
`;

const Container = styled(Row)`
  height: 100%;
`;

const StyledImage = styled.img`
  width: 120px;
  height: 120px;
  border-radius: 50%;
  margin: 5% auto;
  display: block;
`;

const ProfileContainer = ({ content }) => {
  return (
    <Container type="flex" align="middle" justify="center">
      <div>
        <StyledImageContainer>
          <StyledImage src={content.photo} />
        </StyledImageContainer>
        <StyledTitle>{content.userable.user.name}</StyledTitle>
        <StyledSubTitle>{roleConverter[content.roles[0].name]}</StyledSubTitle>

        <Row type="flex" justify="space-between" style={{ marginTop: "30px" }}>
          <StyledSubTitle>Level {content.rating.level}</StyledSubTitle>
          <StyledSubTitle>
            {content.rating.points}
            {content.rating.next_level && "/ " + content.rating.next_level}
          </StyledSubTitle>
        </Row>

        <Progress
          strokeColor={{
            "0%": "darkblue",
            "100%": "lightblue",
          }}
          percent={(content.rating.points * 100) / content.rating.next_level}
          showInfo={false}
        />
      </div>
    </Container>
  );
};

export default ProfileContainer;
