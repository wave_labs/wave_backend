import React from "react";
import { Drawer } from "antd";

const DrawerContainer = ({
  handleDrawerClose,
  visible,
  width,
  children,
}) => {
  return (
    <Drawer
      width={width ? width : "400px"}
      placement="right"
      closable={false}
      onClose={handleDrawerClose}
      visible={visible}
    >
      {children}
    </Drawer>
  );
};

export default DrawerContainer;
