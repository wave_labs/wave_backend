import React from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import CardFooter from "../Common/CardFooter";
import { Link } from "react-router-dom";
import { dimensions } from "helpers";

const ItemContainer = styled.div`
  padding: 20px;
  width: 25%;
  min-width: 210px;

  @media (max-width: ${dimensions.lg}) {
    width: 48%;
  }

  @media (max-width: ${dimensions.sm}) {
    width: 100%;
  }
`;

const Item = styled.div`
  border: 1px solid lightgray;
  border-radius: 6px;
  width: 100%;
`;

const ContainerLink = styled(Link)`
  height: 300px;
  display: flex;
  align-items: center;
  justify-content: center;

  div {
    h3 {
      font-size: 1.2em;
      text-align: center;
      color: black;
      margin: 5px auto;
    }
  }
`;

const CardImage = styled.div`
  display: block;
  position: relative;
  width: 200px;
  height: 200px;
  margin: auto;
  border-radius: 50%;
  background-color: #eeeeee;
  background-image: url(${(props) => props.background});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

const ActiveIndicator = styled.div`
  position: absolute;
  width: 15px;
  height: 15px;
  box-shadow: 0px 0px 10px 0px rgba(138, 138, 138, .3);
  top: 5px;
  right: 5px;
  border-radius: 50%;
  background-color: ${(props) => (props.active == 1 ? "green" : "red")};
`;

const CardListContainer = ({
  data,
  handleUpdate,
  handleDelete,
  handleDuplicate,
  defaultPath = "",
}) => {
  return (
    <div>
      <Row type="flex" justify="start">
        {data.map((record) => (
          <ItemContainer key={record.id}>
            <Item >
              <ContainerLink to={defaultPath + record.id}>
                <div>
                  <CardImage
                    background={`${window.location.origin}/` + record.img}
                  />
                  <h3>{record.name}</h3>
                </div>
              </ContainerLink>

              <CardFooter
                handleUpdate={() => handleUpdate(record)}
                handleDelete={() => handleDelete(record)}
                handleDuplicate={() => handleDuplicate(record)}
              />
            </Item>
          </ItemContainer>
        ))}
      </Row>
    </div>
  );
};

export default CardListContainer;
