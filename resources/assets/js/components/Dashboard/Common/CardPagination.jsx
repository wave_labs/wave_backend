import React from "react";
import { Col, Row, Menu, Card, Table, Pagination, Icon, Popconfirm  } from "antd";

const { Meta } = Card;

const CardPagination = ({
  meta,
  data,
  cardOptions,
  handleChange
}) => {
    const cards = 
    <Row style={{ 
        margin: '17px 0px'}}
    >
        {data.map((device) =>  
            <Col 
                key={device.id}
                style={{ marginBottom: 15 }}
                xs={24} 
                sm={12} 
                md={8} 
                lg={8} 
                xl={6}
            >
                <Card
                    style={{ 
                        maxWidth: '90%', }}
                    cover={
                    <img
                        style={{display: 'block', maxWidth: '500px', maxHeight: '500px'}}
                        alt={device.name}
                        src={ device.images.length ? `${window.location.origin}/api/photo/iotDevice/` + device.images[0].path : `${window.location.origin}/api/photo/iotDevice/no-image-icon.png`}
                    />
                    }
                    actions={cardOptions(device)}
                >
                    <Meta
                    title={device.name}
                    description= {`Device Id: ${device.id}`}
                    />
                </Card>
            </Col>
        )};
    </Row>

    return (
        <div>
            {cards}
            <Pagination 
                showSizeChanger = { false}
                showQuickJumper = { false}
                total = { meta.total}
                showTotal = { (total, range) => `${range[0]} to ${range[1]} of ${total}`}
                total = { meta.total}
                current = { meta.current_page}
                pageSize = { meta.per_page} 
                style={{float: 'right'}}
                onChange={handleChange}
            />
        </div>
  );
};

export default CardPagination;
