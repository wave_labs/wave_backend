import React from "react";
import { Card, Icon, List } from "antd";
import styled from "styled-components";
import RowOperation from "./RowOperation";

const { Meta } = Card;

const StyledCard = styled(Card)`
  width: 100%;
  min-width: 200px;
  border-top: 3px solid blue;
  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.15);
  border-radius: 6px !important;
  height: 100%;
  min-width: 250px;
  border-top: 4px solid #002140 !important;
`;

const StyledImg = styled.img`
  width: 150px;
`;

const InnerContainer = styled.div`
  height: 50vh;
  width: 100%;
  overflow: auto;
`;

const StyledHeader = styled.h1`
  display: inline;
`;

const TablePagination = ({
  handleTableChange,
  meta,
  loading,
  columns,
  data,
  expandedRowRender,
  title,
}) => {
  return (
    <StyledCard
      title={<StyledHeader>{title}</StyledHeader>}
      extra={
        <Icon
          onClick={() => onCreateClick()}
          type="plus-circle"
          theme="filled"
        />
      }
    >
      <InnerContainer>
        <List
          itemLayout="horizontal"
          dataSource={data}
          renderItem={(item) => (
            <List.Item
              actions={
                <RowOperation
                  onUpdateClick={() => this.onUpdateClick(sighting)}
                  onDeleteConfirm={() => this.props.deleteSighting(sighting.id)}
                />
              }
            >
              {item.image ? (
                <List.Item.Meta
                  avatar={<StyledImg src={item.image} />}
                  title={item.title}
                  description={item.description}
                />
              ) : (
                <List.Item.Meta
                  title={item.title}
                  description={item.description}
                />
              )}
            </List.Item>
          )}
        />
      </InnerContainer>
    </StyledCard>
  );
};

export default TablePagination;
