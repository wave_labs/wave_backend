import React, { useEffect, useState } from "react";
import { message } from "antd";

message.config({
  maxCount: 1,
});

function ExportMessage(messageStatus) {
  const [status, setStatus] = useState(null);

  useEffect(() => {
    if (status != messageStatus.message) {
      setStatus(messageStatus.message);
      console.log(status);
      switch (messageStatus.message) {
        case "success":
          return message.success("Download successful");
        case "error":
          return message.error("Download failed, please try again");
        case "loading":
          return message.loading("Gathering data...", 0);
        default:
          return;
      }
    }
  });

  return <div />;
}

export default ExportMessage;
