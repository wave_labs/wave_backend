import React, {
  useState,
  forwardRef,
  useRef,
  useImperativeHandle,
} from "react";

const StepsContainer = forwardRef((props, ref) => {
  const [currentStep, setCurrentStep] = useState(0);

  useImperativeHandle(ref, () => ({
    next(nextStep = currentStep + 1) {
      if (nextStep < props.steps.length) {
        setCurrentStep(nextStep);
      }
    },
    prev(prevStep = currentStep - 1) {
      if (prevStep >= 0) {
        setCurrentStep(prevStep);
      }
    },
    getCurrentStep() {
      return currentStep;
    },
    setCurrentStep(step) {
      setCurrentStep(step);
    },
    isLastStep() {
      return currentStep == props.steps.length - 1;
    },
    isFirstStep() {
      return currentStep == 0;
    },
  }));

  return <div>{props.steps[currentStep]}</div>;
});

export default StepsContainer;
