import React, { Component } from "react";
import { message } from "antd";
import { connect } from "react-redux";
import { isEmpty } from "underscore";
import { alertActions } from "redux-modules/alert/actions";

class DashboardMessage extends Component {
  handleClose = () => {
    this.props.clearAlert();
  };

  render() {
    const { alert } = this.props;

    return (
      <div>
        {!isEmpty(alert) && alert.type == "error"
          ? message.error(
              Array.isArray(alert.description)
                ? alert.description.map((description, index) =>
                    index == 0 ? (
                      <span key={index}>{description}</span>
                    ) : (
                      <p key={index}>{description}</p>
                    )
                  )
                : alert.description,
              2,
              this.handleClose
            )
          : alert.type == "success" &&
            message.success(alert.description, 2, this.handleClose)}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clearAlert: () => dispatch(alertActions.clear()),
  };
};

const mapStateToProps = (state) => {
  return {
    alert: state.alert,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardMessage);
