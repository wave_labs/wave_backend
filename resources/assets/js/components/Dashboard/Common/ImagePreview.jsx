import React from "react";
import { Row, Upload } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import styled from "styled-components";
import { dummyRequest } from "helpers";

const StyledImage = styled.img`
  width: 100%;
  cursor: pointer;
`;

const StyledUploadOutlined = styled(UploadOutlined)`
  font-size: 22px;
`;

const Tip = styled.p`
  flex: 0 0 100%;
  color: #777777;
  text-align: center;
`;

const ImagePreview = ({
  previewImage,
  handleChange,
  tipText = "Image in same style as existing",
  uploadText = "Upload",
}) => {
  return (
    <Row type="flex" justify="center">
      <Upload
        listType="picture-card"
        className="avatar-uploader"
        customRequest={dummyRequest}
        style={{ width: "100%" }}
        fileList={false}
        onChange={handleChange}
      >
        {previewImage ? (
          <StyledImage src={previewImage} />
        ) : (
          <div>
            <StyledUploadOutlined />
            <div className="ant-upload-text">{uploadText}</div>
          </div>
        )}
      </Upload>
      <Tip>{tipText}</Tip>
    </Row>
  );
};

export default ImagePreview;
