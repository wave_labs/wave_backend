import React, { Component } from "react";
import { Marker, InfoWindow } from "react-google-maps";
import { Form, Button, Upload, Icon } from "antd";

const FormItem = Form.Item;

//This component always needs to have key prop set, because of its derived state
class UploadFileFormItem extends Component {
	createDefaultFile = link => {
		return {
			uid: "-1",
			name: link,
			status: "done",
			defaultFile: true,
			url: `${window.location.origin}/storage/${link}`
		};
	};

	state = {
		fileList: this.props.link
			? [this.createDefaultFile(this.props.link)]
			: []
	};

	normFile = info => {
		this.setState({ fileList: [info.file] });
		return info.file;
	};
	beforeUpload = file => false;

	onFileRemove = file => this.setState({ fileList: [] });

	render() {
		const { getFieldDecorator } = this.props.form;

		return (
			<FormItem label="Upload File">
				{getFieldDecorator("file", {
					initialValue: this.state.fileList[0],
					getValueFromEvent: this.normFile,
					rules: this.props.rules
				})(
					<Upload
						fileList={this.state.fileList}
						onRemove={this.onFileRemove}
						beforeUpload={this.beforeUpload}
					>
						<Button>
							<Icon type="upload" /> Click to Upload
						</Button>
					</Upload>
				)}
			</FormItem>
		);
	}
}

export default UploadFileFormItem;
