import React, { Component } from "react";
import { Row, DatePicker, Col, Button, Form, Input } from "antd";

const { RangePicker } = DatePicker;
const Search = Input.Search;
const FormItem = Form.Item;

class TableFilterRowWithDatesWithReport extends Component {
  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      onSearchReport,
      handleDate,
      hasMarginBottom,
      hasMarginTop,
      handleSubmit,
    } = this.props;

    return (
      <div>
        <Form layout="inline" onSubmit={handleSubmit} hideRequiredMark={true}>
          <Row
            style={{
              marginBottom: hasMarginBottom ? "15px" : "",
              marginTop: hasMarginTop ? "15px" : "",
            }}
            type="flex"
            gutter={16}
          >
            <Col>
              <FormItem>
                {getFieldDecorator("searchReport", {})(
                  <Search
                    placeholder="Report ID"
                    onSearch={(search) => onSearchReport(search)}
                  />
                )}
              </FormItem>
            </Col>
            <Col>
              <FormItem>
                {getFieldDecorator("range", {})(
                  <RangePicker onChange={(date) => handleDate(date)} />
                )}
              </FormItem>
            </Col>
            <Col>
              <Button
                type="primary"
                shape="round"
                icon="download"
                htmlType="submit"
              />
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

export default Form.create()(TableFilterRowWithDatesWithReport);
