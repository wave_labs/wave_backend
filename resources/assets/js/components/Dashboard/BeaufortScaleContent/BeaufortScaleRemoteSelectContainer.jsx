import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/beaufortScale/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class BeaufortScaleRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = search => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      BeaufortScaleSelector,
      loading,
      value,
      onChange,
      mode,
      children
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Beaufort Scale"
        mode={mode}
      >
        {children}
        {BeaufortScaleSelector.map(b => (
          <Option value={b.id} key={b.id}>
            {b.desc} ({b.scale})
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchSelector: filters => dispatch(fetchSelector(filters))
  };
};

const mapStateToProps = state => {
  return {
    BeaufortScaleSelector: state.beaufortScale.BeaufortScaleSelector,
    loading: state.beaufortScale.loading
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BeaufortScaleRemoteSelectContainer);
