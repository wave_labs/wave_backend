import React, { Component } from "react";
import { connect } from "react-redux";
import WhatsNewForm from "./WhatsNewForm";
import SectionContainer from "./SectionContainer";
import Form from "./WhatsNewForm";
import { Row, Col, Divider } from "antd";

import styled from "styled-components";

const Container = styled.div``;

class SettingsContent extends Component {
  render() {
    return (
      <Container>
        <Form />
      </Container>
    );
  }
}

export default SettingsContent;
