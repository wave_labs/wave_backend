import React, { Component } from "react";
import styled from "styled-components";
import axios from "axios";
import { Row, Col, Form, Input, Button, Upload, Icon } from "antd";
import DynamicFields from "./DynamicFields";

const FormItem = Form.Item;
const TextArea = Input.TextArea;

const dummyRequest = ({ file, onSuccess }) => {
  setTimeout(() => {
    onSuccess("ok");
  }, 0);
};

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

class WhatsNewForm extends Component {
  state = {
    loading: false,
    fileList: [],
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        let formData = new FormData();

        formData.append("image", this.state.fileList[0].originFileObj);
        formData.append("title", values.title);
        formData.append("description", values.description);
        if (values.update) {
          formData.append("update", values.update);
        }
        if (values.research) {
          formData.append("research", values.research);
        }
        if (values.event) {
          formData.append("event", values.event);
        }

        console.log(formData);

        axios.post(`${window.location.origin}/api/whats-new`, formData);
      }
    });
  };

  handleChange = (info) => {
    if (info.file.status === "uploading") {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === "done") {
      getBase64(info.file.originFileObj, (imageUrl) =>
        this.setState({
          imageUrl,
          loading: false,
          fileList: info.fileList,
        })
      );
    }
  };

  rules = {
    title: [
      {
        required: true,
        message: "Subject is required",
      },
    ],
    description: [
      {
        required: true,
        message: "Section is required",
      },
    ],
  };

  render() {
    const { form } = this.props;
    const { getFieldDecorator } = this.props.form;

    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? "loading" : "plus"} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const { imageUrl } = this.state;

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row gutter={24}>
          <Col xs={24}>
            <FormItem hasFeedback label="Title">
              {getFieldDecorator("title", {
                rules: this.rules.title,
              })(<Input placeholder="Title" />)}
            </FormItem>
          </Col>

          <Col xs={24}>
            <FormItem hasFeedback label="Description">
              {getFieldDecorator("description", {
                rules: this.rules.description,
              })(
                <TextArea
                  placeholder="New's description"
                  autosize={{ minRows: 4, maxRows: 6 }}
                />
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={8}>
            <DynamicFields
              {...form}
              name="update"
              fields={{
                name: "update",
                label: "Updates",
              }}
            />
          </Col>

          <Col xs={24} sm={8}>
            <DynamicFields
              {...form}
              name="event"
              fields={{
                name: "event",
                label: "Events",
              }}
            />
          </Col>

          <Col xs={24} sm={8}>
            <DynamicFields
              {...form}
              name="research"
              fields={{
                name: "research",
                label: "Research",
              }}
            />
          </Col>

          <Col xs={24}>
            <center>
              <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                customRequest={dummyRequest}
                showUploadList={false}
                onChange={this.handleChange}
              >
                {imageUrl ? (
                  <img src={imageUrl} alt="avatar" style={{ width: "100%" }} />
                ) : (
                  uploadButton
                )}
              </Upload>
            </center>
          </Col>
        </Row>

        <FormItem>
          <center>
            <Button ghost="true" size="large" type="primary" htmlType="submit">
              CREATE POST
            </Button>
          </center>
        </FormItem>
      </Form>
    );
  }
}

export default Form.create()(WhatsNewForm);
