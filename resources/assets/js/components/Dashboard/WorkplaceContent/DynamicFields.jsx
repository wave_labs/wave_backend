import React, { Component, Fragment } from "react";
import { Form, Icon, Button, Input } from "antd";

const FormItem = Form.Item;

class DynamicFields extends Component {
  id = 0;

  add = () => {
    const { getFieldValue, setFieldsValue, name } = this.props,
      keys = getFieldValue(`${name}List`),
      nextKeys = keys.concat(this.id++);

    setFieldsValue({
      [`${name}List`]: nextKeys
    });
  };

  remove = k => () => {
    const { getFieldValue, setFieldsValue, name } = this.props,
      keys = getFieldValue(`${name}List`);

    setFieldsValue({
      [`${name}List`]: keys.filter(key => key !== k)
    });
  };

  defaultValidation = name => ({
    validateTrigger: ["onChange", "onBlur"],
    rules: [
      {
        required: true,
        message: `Please input ${name}.`
      }
    ]
  });

  render() {
    const { getFieldDecorator, getFieldValue, fields: obj, name } = this.props;
    getFieldDecorator(`${name}List`, { initialValue: [] });
    const fieldCounter = getFieldValue(`${name}List`);
    return (
      <Fragment>
        {fieldCounter.map(k => (
          <FormItem key={k}>
            {getFieldDecorator(
              `${name}[${k}]`,
              obj.validation || this.defaultValidation(name)
            )(
              <Input
                placeholder={obj.label}
                style={{ width: "100%" }}
                suffix={
                  <Icon
                    className="dynamic-delete-button"
                    type="minus-circle-o"
                    onClick={this.remove(k)}
                  />
                }
              />
            )}
          </FormItem>
        ))}

        <FormItem>
          <Button type="dashed" onClick={this.add} style={{ width: "100%" }}>
            <Icon type="plus" /> Add &nbsp; {name}
          </Button>
        </FormItem>
      </Fragment>
    );
  }
}

export default DynamicFields;
