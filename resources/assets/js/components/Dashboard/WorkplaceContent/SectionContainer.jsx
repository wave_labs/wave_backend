import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import CardList from "../Common/CardList";

const Container = styled.div`
  height: 100%;
  width: 100%;
  margin: auto;
  display: block;
`;

const NewsData = [
  {
    title: "Card title",
    description: "This is the description",
    image: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
  },
  {
    title: "Card title",
    description: "This is the description",
    image: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
  },
  {
    title: "Card title",
    description: "This is the description",
    image: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
  },
  {
    title: "Card title",
    description: "This is the description",
    image: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
  },
  {
    title: "Card title",
    description: "This is the description",
    image: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
  },
  {
    title: "Card title",
    description: "This is the description",
    image: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
  }
];

const FAQData = [
  {
    title: "Card title",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum impedit dignissimos unde ea asperiores quae sapiente eos molestiae, ex necessitatibus nostrum dicta sunt, nisi tempora optio eius vitae repellat similique?"
  },
  {
    title: "Card title",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum impedit dignissimos unde ea asperiores quae sapiente eos molestiae, ex necessitatibus nostrum dicta sunt, nisi tempora optio eius vitae repellat similique?"
  },
  {
    title: "Card title",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum impedit dignissimos unde ea asperiores quae sapiente eos molestiae, ex necessitatibus nostrum dicta sunt, nisi tempora optio eius vitae repellat similique?"
  },
  {
    title: "Card title",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum impedit dignissimos unde ea asperiores quae sapiente eos molestiae, ex necessitatibus nostrum dicta sunt, nisi tempora optio eius vitae repellat similique?"
  },
  {
    title: "Card title",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum impedit dignissimos unde ea asperiores quae sapiente eos molestiae, ex necessitatibus nostrum dicta sunt, nisi tempora optio eius vitae repellat similique?"
  },
  {
    title: "Card title",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum impedit dignissimos unde ea asperiores quae sapiente eos molestiae, ex necessitatibus nostrum dicta sunt, nisi tempora optio eius vitae repellat similique?"
  }
];

class SurveyTableFilterContainer extends Component {
  render() {
    return (
      <Container>
        <Row type="flex" justify="space-between" align="top" gutter={64}>
          <Col span={8}>
            <CardList title="News" data={NewsData} />
          </Col>
          <Col span={8}>
            <CardList title="FAQ" data={FAQData} />
          </Col>
          <Col span={8}>
            <CardList title="Vacancies" data={FAQData} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default SurveyTableFilterContainer;
