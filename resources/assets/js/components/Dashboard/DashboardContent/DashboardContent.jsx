import React from 'react';
import DashboardHeaderContainer from './DashboardHeaderContainer'
import DashboardCardsContainer from './DashboardCardsContainer'

const DashboardContent = () => {
	return (
			<div>
				<DashboardHeaderContainer/>
				<DashboardCardsContainer />
			</div>
	)
}

export default DashboardContent;