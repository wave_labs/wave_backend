import React, { Component } from "react";
import styled from "styled-components";

const StyledTitle = styled.div`
  font-size: 2rem;
  margin-bottom: 2rem;
  font-weight: bold;
  font-variant: small-caps;
`;

class DashboardHeaderContainer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        #ID <StyledTitle> Welcome User to Wave Dashboard </StyledTitle>
      </div>
    );
  }
}

export default DashboardHeaderContainer;
