import React, { Component } from "react";
import { Divider, Card, Row, Col, Icon } from "antd";
import styled from "styled-components";

const StyledTitle = styled.div`
  font-size: 1.2rem;
  margin-bottom: 2rem;
  text-align: center;
  font-weight: bold;
  font-variant: small-caps;
`;
const StyledThreeWayDescription = styled.div`
  font-size: 1.8rem;
  display: block;
  margin: 0;
  top: 10%;
  -ms-transform: translateY(-10%);
  transform: translateY(-10%);
  font-weight: bold;
  font-variant: small-caps;
`;
const StyledDescription = styled.div`
  font-size: 2.3rem;
  display: inline-block;
  font-weight: bold;
  font-variant: small-caps;
  margin: 0;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
`;

class DashboardCardContainer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Divider orientation="left">Overview</Divider>
        <div style={{ background: "#fff", padding: "20px" }}>
          <Row gutter={40}>
            <Col xs={24} md={12}>
              <Card>
                <StyledTitle> Applications </StyledTitle>
                <Row gutter={16}>
                  <Col xs={24} md={12}>
                    <center>
                      <Icon
                        type="mobile"
                        style={{
                          fontSize: "140px",
                          float: "right",
                          padding: "10px"
                        }}
                      />
                    </center>
                  </Col>
                  <Col xs={24} md={12}>
                    <StyledThreeWayDescription>
                      Whale Reporter
                    </StyledThreeWayDescription>
                    <StyledThreeWayDescription>
                      Litter Reporter
                    </StyledThreeWayDescription>
                    <StyledThreeWayDescription>
                      Dive Reporter
                    </StyledThreeWayDescription>
                  </Col>
                </Row>
              </Card>
            </Col>
            <Col xs={24} md={12}>
              <Card>
                <StyledTitle> Reports </StyledTitle>

                <center>
                  <Icon
                    type="file-done"
                    style={{ fontSize: "140px", padding: "10px" }}
                  />{" "}
                  <StyledDescription> +700 </StyledDescription>
                </center>
              </Card>
            </Col>
          </Row>
        </div>
        <div style={{ background: "#fff", padding: "20px" }}>
          <Row gutter={40}>
            <Col xs={24} md={12}>
              <Card>
                <StyledTitle> Website Visits </StyledTitle>
                <center>
                  <Icon
                    type="eye"
                    style={{ fontSize: "140px", padding: "10px" }}
                  />{" "}
                  <StyledDescription> +15 000 </StyledDescription>
                </center>
              </Card>
            </Col>
            <Col span={12}>
              <Card>
                <StyledTitle> Users </StyledTitle>
                <center>
                  <Icon
                    type="usergroup-add"
                    style={{ fontSize: "140px", padding: "10px" }}
                  />{" "}
                  <StyledDescription> +1000 </StyledDescription>
                </center>
              </Card>
            </Col>
          </Row>
        </div>
        ,
      </div>
    );
  }
}

const gridStyle = {
  textAlign: "center",
  padding: "30px"
};

export default DashboardCardContainer;
