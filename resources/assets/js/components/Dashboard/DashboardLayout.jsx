import React, { Component } from "react";
import { connect } from "react-redux";
import { Layout } from "antd";
import { dimensions } from "helpers";
import { Route, Switch, withRouter } from "react-router-dom";
import styled from "styled-components";
import PrivateRoute from "components/Common/PrivateRoute";
import SideMenu from "./SideMenu";
import Breadcrumb from "./Breadcrumb";
import UserContent from "./UserContent/UserContent";
import CapsuleContent from "./CapsuleContent/CapsuleContent";

import AppsContent from "./Apps/AppsContent";
import SightingContent from "./Apps/SightingContent/SightingContent";
import DiveContent from "./Apps/DiveContent/DiveContent";
import LitterContent from "./Apps/LitterContent/LitterContent";

import SightingDataContent from "./SightingDataContent/SightingDataContent";
import CreatureContent from "./Data/CreatureContent/CreatureContent";
import CreatureTypeContent from "./Data/CreatureTypeContent/CreatureTypeContent";
import DivingSpotContent from "./Data/DivingSpotContent/DivingSpotContent";
import LitterCategoryContent from "./Data/LitterCategoryContent/LitterCategoryContent";
import LitterSubCategoryContent from "./Data/LitterSubCategoryContent/LitterSubCategoryContent";

import SuggestionContent from "./SuggestionContent/SuggestionContent";
import FormContent from "./FormContent/FormContent";
import QuestionContent from "./QuestionContent/QuestionContent";
import FeedbackContent from "./FeedbackContent/FeedbackContent";
import ResultContent from "./ResultContent/ResultContent";

import WorkplaceContent from "./WorkplaceContent/WorkplaceContent";
import SettingsContent from "./AccountContent/SettingsContent";
const { Content, Footer } = Layout;

import IotDeviceContent from "./IoT/DeviceContent/IotDeviceContent";
import IotReportContent from "./IoT/IotReportContent/IotReportContent";
import IotContent from "./IoT/IotContent";

import StudiesContent from "./Studies/StudiesContent";
import MarkerContent from "./Studies/MarkerContent/MarkerContent";
import FreshwaterPinContent from "./Studies/FreshwaterPinContent/FreshwaterPinContent";
import TroutContent from "./Studies/TroutContent/TroutContent";
import DiademaContent from "./Studies/DiademaContent/DiademaContent";
import ClimarestContent from "./Studies/ClimarestContent/ClimarestContent";
import StopwatchContent from "./Studies/StopwatchContent/StopwatchContent";
import StopwatchTimeline from "./Studies/StopwatchContent/StopwatchTimeline";
import BoundingBoxContent from "./Studies/BoundingBoxContent/BoundingBoxContent";

import DashboardMessage from "./Common/DashboardMessage";
import ENMContent from "./ENM/ENMContent";

import AiCategoryContent from "./AI/AiContent/AiContent";
import AiContent from "./AI/AiContent";

import ReportContent from "./Insect/ReportContent/ReportContent";
import InsectFamilyContent from "./Insect/FamilyContent/InsectFamilyContent";
import InsectReferenceContent from "./Insect/ReferenceContent/InsectReferenceContent";
import InsectOccurrenceContent from "./Insect/OccurrenceContent/InsectOccurrenceContent";
import InsectProjectListContent from "./Insect/ProjectContent/InsectProjects/InsectProjectListContent";
import InsectProjectContent from "./Insect/ProjectContent/InsectSpecificProject/InsectProjectContent";
import SoftCoralContent from "./Studies/SoftCoralContent/SoftCoralContent";

const Container = styled.div`
    padding: 24px;
    background: #fff;
    min-height: 360;

    @media (max-width: ${dimensions.md}) {
        padding: 0px;
    }
`;

class DashboardLayout extends Component {
    render() {
        return (
            <Layout style={{ minHeight: "100vh" }}>
                {this.props.user.userable.type_name !=
                    "App\\UserDiveCenter" && <SideMenu />}

                <Layout style={{ background: "white" }}>
                    <DashboardMessage />
                    <Content style={{ margin: "0 12px" }}>
                        <Breadcrumb />
                        <Container>
                            <Switch>
                                <Route
                                    path="/dashboard/apps/litterReporter"
                                    component={LitterContent}
                                />
                                <Route
                                    path="/dashboard/apps/whaleReporter/:id?"
                                    component={SightingContent}
                                />

                                <Route
                                    path="/dashboard/apps/diveReporter"
                                    component={DiveContent}
                                />
                                <Route
                                    path="/dashboard/apps"
                                    component={withRouter(AppsContent)}
                                />

                                <PrivateRoute
                                    path="/dashboard/users"
                                    component={UserContent}
                                    route="/dashboard"
                                    source="dashboard"
                                />
                                <PrivateRoute
                                    path="/dashboard/suggestions"
                                    component={SuggestionContent}
                                    route="/dashboard"
                                    source="dashboard"
                                />
                                <PrivateRoute
                                    path="/dashboard/form/structure/:form"
                                    component={QuestionContent}
                                    route="/dashboard"
                                    source="dashboard"
                                />

                                <PrivateRoute
                                    path="/dashboard/form/results/:form/:id"
                                    component={ResultContent}
                                    route="/dashboard"
                                    source="dashboard"
                                />
                                <PrivateRoute
                                    path="/dashboard/form/results/:form"
                                    component={FeedbackContent}
                                    route="/dashboard"
                                    source="dashboard"
                                />
                                <PrivateRoute
                                    path="/dashboard/form"
                                    component={FormContent}
                                    route="/dashboard"
                                    source="dashboard"
                                />
                                <PrivateRoute
                                    path="/dashboard/workplace"
                                    component={WorkplaceContent}
                                    route="/dashboard"
                                    source="dashboard"
                                />

                                <Route
                                    path="/dashboard/capsules"
                                    component={CapsuleContent}
                                />

                                <Route
                                    path="/dashboard/sighting-data"
                                    component={SightingDataContent}
                                />
                                <Route
                                    path="/dashboard/creatures/:id"
                                    component={CreatureContent}
                                />
                                <Route
                                    path="/dashboard/creatures"
                                    component={CreatureTypeContent}
                                />

                                <Route
                                    path="/dashboard/divingSpot"
                                    component={DivingSpotContent}
                                />

                                <Route
                                    path="/dashboard/litter/categories/:id"
                                    component={LitterSubCategoryContent}
                                />
                                <Route
                                    path="/dashboard/litter/categories"
                                    component={LitterCategoryContent}
                                />

                                <Route
                                    path="/dashboard/settings"
                                    component={SettingsContent}
                                />

                                <Route
                                    path="/dashboard/iot/:deviceType/:device"
                                    component={withRouter(IotReportContent)}
                                />
                                <Route
                                    path="/dashboard/iot/:deviceType"
                                    component={withRouter(IotDeviceContent)}
                                />
                                <Route
                                    path="/dashboard/iot"
                                    component={withRouter(IotContent)}
                                />

                                <Route
                                    path="/dashboard/insect/references"
                                    component={InsectReferenceContent}
                                />
                                <PrivateRoute
                                    path="/dashboard/insect/species"
                                    component={InsectFamilyContent}
                                    route="/dashboard"
                                    source="dashboard"
                                />
                                <Route
                                    path="/dashboard/insect/occurrences"
                                    component={InsectOccurrenceContent}
                                />

                                <Route
                                    path="/dashboard/insect/projects"
                                    component={InsectProjectListContent}
                                />

                                <Route
                                    path="/dashboard/insect/project/:id"
                                    component={withRouter(InsectProjectContent)}
                                />

                                <Route
                                    path="/dashboard/enm"
                                    component={ENMContent}
                                />
                                <Route
                                    path="/dashboard/ai/:aiCategory"
                                    component={withRouter(AiCategoryContent)}
                                />
                                <Route
                                    path="/dashboard/ai"
                                    component={AiContent}
                                />

                                <Route
                                    path="/dashboard/studies/boundingBox"
                                    component={BoundingBoxContent}
                                />

                                <Route
                                    path="/dashboard/studies/markers"
                                    component={MarkerContent}
                                />
                                <Route
                                    path="/dashboard/studies/stopwatch/:id"
                                    component={withRouter(StopwatchTimeline)}
                                />
                                <Route
                                    path="/dashboard/studies/stopwatch"
                                    component={withRouter(StopwatchContent)}
                                />
                                <Route
                                    path="/dashboard/studies/freshwaterPin"
                                    component={FreshwaterPinContent}
                                />
                                <Route
                                    path="/dashboard/studies/trout"
                                    component={TroutContent}
                                />
                                <Route
                                    path="/dashboard/studies/diadema"
                                    component={DiademaContent}
                                />
                                <Route
                                    path="/dashboard/studies/climarest"
                                    component={ClimarestContent}
                                />
                                <Route
                                    path="/dashboard/studies/softCoral"
                                    component={SoftCoralContent}
                                />
                                <Route
                                    path="/dashboard/studies"
                                    component={withRouter(StudiesContent)}
                                />

                                <Route
                                    path="/dashboard/litter/categories"
                                    component={LitterCategoryContent}
                                />

                                <Route
                                    path="/dashboard"
                                    component={SettingsContent}
                                />
                            </Switch>
                        </Container>
                    </Content>
                    <Footer
                        style={{ background: "white", textAlign: "center" }}
                    >
                        Wave Labs ©{new Date().getFullYear()}
                    </Footer>
                </Layout>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.auth.user,
        isAuthenticated: state.auth.isAuthenticated,
        token: state.auth.token,
    };
};

export default connect(mapStateToProps)(DashboardLayout);
