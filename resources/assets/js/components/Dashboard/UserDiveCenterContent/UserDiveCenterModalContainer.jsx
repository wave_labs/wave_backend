import React, { Component } from "react";
import {
  createUserDiveCenter,
  updateUserDiveCenter,
  resetUserDiveCenterEdit,
} from "redux-modules/userDiveCenter/actions";
import {
  resetModal,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../Common/EditCreateModal";
import UserDiveCenterForm from "./UserDiveCenterForm";
import moment from "moment";

class UserDiveCenterModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, userDiveCenter) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        this.props
          .updateUserDiveCenter(
            this.props.editUserDiveCenter.id,
            userDiveCenter
          )
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.resetUserDiveCenterEdit();
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, userDiveCenter) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });

        this.props
          .createUserDiveCenter(userDiveCenter)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit UserDiveCenter"
        titleCreate="Create DiveCenter"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <UserDiveCenterForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          userDiveCenter={this.props.editUserDiveCenter}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetUserDiveCenterEdit: () => dispatch(resetUserDiveCenterEdit()),
    resetModal: () => dispatch(resetModal()),
    createUserDiveCenter: (data) => dispatch(createUserDiveCenter(data)),
    updateUserDiveCenter: (data, id) =>
      dispatch(updateUserDiveCenter(data, id)),
  };
};

const mapStateToProps = (state) => {
  return {
    editUserDiveCenter: state.userDiveCenter.editUserDiveCenter,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDiveCenterModalContainer);
