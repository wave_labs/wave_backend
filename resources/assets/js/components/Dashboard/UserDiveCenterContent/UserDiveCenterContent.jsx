import React from 'react';
import UserDiveCenterModalContainer from './UserDiveCenterModalContainer'
import UserDiveCenterTableFilterContainer from './UserDiveCenterTableFilterContainer'

const UserDiveCenterContent = () => {
	return (
			<div>
				<UserDiveCenterModalContainer />
				<UserDiveCenterTableFilterContainer />
			</div>
	)
}

export default UserDiveCenterContent;