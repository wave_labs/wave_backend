import React, { Component } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { formatPosition } from "helpers";

const StyledImg = styled.img`
  max-width: 200px !important;
  margin: 0.75rem;
`;

const StyledBold = styled.b`
  font-weight: bold;
`;

const LitterPopUp = ({ dive_center, viewMore }) => {
  return (
    <div>
      <div>
        <StyledBold>Name: </StyledBold>
        <span>{dive_center.name}</span>
      </div>
      <div>
        <StyledBold>Address: </StyledBold>
        <span>{dive_center.address}</span>
      </div>

      <Link to={`/dashboard/dive_center/${dive_center.id}`}>View More</Link>
    </div>
  );
};

export default LitterPopUp;
