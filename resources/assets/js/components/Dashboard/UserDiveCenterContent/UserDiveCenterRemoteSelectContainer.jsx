import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchUserDiveCenters } from "redux-modules/userDiveCenter/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class UserDiveCenterRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchUserDiveCenters(1, { search });
  };

  render() {
    const { data, loading, value, onChange, mode, children } = this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="UserDiveCenter"
        mode={mode}
      >
        {children}
        {data.map((d) => (
          <Option value={d.id} key={d.id}>
            {d.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUserDiveCenters: (page, filters) =>
      dispatch(fetchUserDiveCenters(page, filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.userDiveCenter.data,
    loading: state.userDiveCenter.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDiveCenterRemoteSelectContainer);
