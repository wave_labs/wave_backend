import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Form, Input, InputNumber, Select, Checkbox } from "antd";

const FormItem = Form.Item;
const TextArea = Input.TextArea;
const Option = Select.Option;

const StyledHelp = styled.a`
  color: #777777;
  text-align: center;
  display: block;
  margin: 20px;

  &:hover {
    color: black;
  }
`;

class UserDiveCenterForm extends Component {
  state = {
    confirmDirty: false,
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;

    if (value && value !== form.getFieldValue("password")) {
      callback("The passwords don't match!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;

    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  rules = {
    name: [
      {
        required: true,
        message: "Name  is required",
      },
    ],
    phone: [
      {
        required: true,
        message: "Please input your phone number!",
      },
      {
        max: 16,
        message: "Phone number too long!",
      },
    ],
    confirmPassword: [
      {
        required: this.props.creating,
        message: "Please confirm your password!",
      },
      {
        validator: this.compareToFirstPassword,
      },
    ],
    password: [
      {
        required: this.props.creating, //password is only required when creating users
        message: "Please input your password!",
      },
      {
        min: 6,
        message: "Your password must have atleast 6 characters",
      },
      {
        validator: this.validateToNextPassword,
      },
    ],
    email: [
      {
        type: "email",
        message: "This is not a valid email!",
      },
      {
        required: true,
        message: "Please input your E-mail!",
      },
    ],
    latitude: [
      {
        required: true,
        message: "Latitude  is required",
      },
    ],
    longitude: [
      {
        required: true,
        message: "Longitude  is required",
      },
    ],
    address: [
      {
        required: true,
        message: "Address  is required",
      },
    ],
    role: [
      {
        required: true,
        message: "Please input your role!",
      },
    ],
    note: [
      {
        required: true,
        message: "Please input some notes!",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { userDiveCenter, editing } = this.props;
    console.log(this.props);

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Name">
              {getFieldDecorator("name", {
                initialValue: userDiveCenter.name,
                rules: this.rules.name,
              })(<Input placeholder="Name" />)}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Address">
              {getFieldDecorator("address", {
                initialValue: userDiveCenter.address,
                rules: this.rules.address,
              })(<Input placeholder="Address" />)}
            </FormItem>
          </Col>

          <StyledHelp href="https://gps-coordinates.org/" target="_blank">
            Don't know your coordinates? Learn how
          </StyledHelp>

          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Latitude">
              {getFieldDecorator("latitude", {
                initialValue: userDiveCenter.latitude,
                rules: this.rules.latitude,
              })(<Input placeholder="Latitude" />)}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Longitude">
              {getFieldDecorator("longitude", {
                initialValue: userDiveCenter.longitude,
                rules: this.rules.longitude,
              })(<Input placeholder="Longitude" />)}
            </FormItem>
          </Col>
        </Row>

        <FormItem>
          {getFieldDecorator("userable_type", {
            initialValue: "UserDiveCenter",
          })(<Input disabled type="hidden" />)}
        </FormItem>
      </Form>
    );
  }
}

export default Form.create()(UserDiveCenterForm);
