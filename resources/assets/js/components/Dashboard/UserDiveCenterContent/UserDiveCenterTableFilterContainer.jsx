import React, { Component } from "react";
import {
  fetchUserDiveCenters,
  deleteUserDiveCenter,
  setUserDiveCenterEdit,
  updateUserDiveCenter,
} from "redux-modules/userDiveCenter/actions";
import { connect } from "react-redux";
import { Row, Col, Button, Icon, Popconfirm, Checkbox, Input } from "antd";
import RowOperation from "../Common/RowOperation";
import TablePagination from "../Common/TablePagination";
import TableFilterRow from "../Common/TableFilterRow";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";

class UserDiveCenterTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
        width: 75,
      },
      {
        title: "Name",
        dataIndex: "name",
      },
      {
        title: "Coordinates",
        dataIndex: "latitude",
        render: (latitude, row) => `(${latitude},${row.longitude})`,
      },
      {
        title: "Address",
        dataIndex: "address",
      },
      {
        title: "",
        key: "",
        render: (text, userDiveCenter) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(userDiveCenter)}
            onDeleteConfirm={() =>
              this.props.deleteUserDiveCenter(userDiveCenter.id)
            }
          />
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.props.fetchUserDiveCenters();
  };

  onUpdateClick = (userDiveCenter) => {
    this.props.setUserDiveCenterEdit(userDiveCenter);
    this.props.startEditing();
  };

  onSearch = (search) => {
    this.filters = { ...this.filters, search };
    this.props.fetchuserDiveCenters(1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchUserDiveCenters(pagination.current, this.filters);
  };
  render() {
    return (
      <div>
        <TableFilterRow
          onSearch={(search) => this.onSearch(search)}
          searchPlaceholder="Search Dive Center"
        />
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUserDiveCenters: (page, filters) =>
      dispatch(fetchUserDiveCenters(page, filters)),
    deleteUserDiveCenter: (id) => dispatch(deleteUserDiveCenter(id)),
    activateUserDiveCenter: (id) => dispatch(activateUserDiveCenter(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setUserDiveCenterEdit: (data) => dispatch(setUserDiveCenterEdit(data)),
    updateUserDiveCenter: (id, data) =>
      dispatch(updateUserDiveCenter(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.userDiveCenter.data,
    meta: state.userDiveCenter.meta,
    loading: state.userDiveCenter.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDiveCenterTableFilterContainer);
