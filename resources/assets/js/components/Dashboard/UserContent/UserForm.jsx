import React, { Component } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Icon,
  InputNumber,
  Button,
  Checkbox,
  Input,
  DatePicker,
  Select,
  Radio,
} from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import CountrySelect from "../../Common/CountrySelect";
import moment from "moment";

const FormItem = Form.Item;
const TextArea = Input.TextArea;
const Option = Select.Option;

const StyledInputNumber = styled(InputNumber)`
  width: 250px !important;
`;

const StyledRadio = styled(Radio.Button)`
  width: 100%;
`;

const StyledRadioGroup = styled(Radio.Group)`
  width: 100%;
`;

class UserForm extends Component {
  state = {
    confirmDirty: false,
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;

    if (value && value !== form.getFieldValue("password")) {
      callback("The passwords don't match!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;

    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  handleUserTypeChange = (e) => {
    this.props.handleUserTypeChange(e);
  };

  rules = {
    name: [
      {
        required: true,
        message: "Please input your name!",
      },
    ],
    confirmPassword: [
      {
        required: this.props.creating,
        message: "Please confirm your password!",
      },
      {
        validator: this.compareToFirstPassword,
      },
    ],
    password: [
      {
        required: this.props.creating, //password is only required when creating users
        message: "Please input your password!",
      },
      {
        min: 6,
        message: "Your password must have atleast 6 characters",
      },
      {
        validator: this.validateToNextPassword,
      },
    ],
    email: [
      {
        type: "email",
        message: "This is not a valid email!",
      },
      {
        required: true,
        message: "Please input your E-mail!",
      },
    ],
    role: [
      {
        required: true,
        message: "Please input your role!",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    let { userType, user } = this.props;

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={16}>
          {this.props.creating ? (
            <Col lg={24} xl={12}>
              <FormItem hasFeedback label="E-mail">
                {getFieldDecorator("email", {
                  initialValue: user.email,
                  rules: this.rules.email,
                })(<Input placeholder="E-mail" />)}
              </FormItem>
            </Col>
          ) : (
            <Col lg={24}>
              <FormItem hasFeedback label="E-mail">
                {getFieldDecorator("email", {
                  initialValue: user.email,
                  rules: this.rules.email,
                })(<Input placeholder="E-mail" />)}
              </FormItem>
            </Col>
          )}

          {this.props.creating && (
            <Col lg={24} xl={12}>
              <FormItem hasFeedback label="Name">
                {getFieldDecorator("name", {
                  initialValue: user.name,
                  rules: this.rules.name,
                })(<Input placeholder="Name" />)}
              </FormItem>
            </Col>
          )}

          {this.props.creating && (
            <React.Fragment>
              <Col lg={24} xl={12}>
                <FormItem hasFeedback label="Password">
                  {getFieldDecorator("password", {
                    rules: this.rules.password,
                  })(<Input type="password" placeholder="Password" />)}
                </FormItem>
              </Col>
              <Col lg={24} xl={12}>
                <FormItem hasFeedback label="Confirm Password">
                  {getFieldDecorator("password_confirmation", {
                    rules: this.rules.confirmPassword,
                  })(
                    <Input
                      type="password"
                      placeholder="Confirm Password"
                      onBlur={this.handleConfirmBlur}
                    />
                  )}
                </FormItem>
              </Col>
            </React.Fragment>
          )}
          {this.props.creating ? (
            <Col lg={24}>
              <Form.Item label="What type of user are you?">
                {getFieldDecorator("userable_type", {
                  rules: this.rules.userable_type,
                })(
                  <StyledRadioGroup
                    size="large"
                    buttonStyle="outline"
                    onChange={this.handleUserTypeChange}
                  >
                    <Row gutter={12}>
                      <Col xs={24} sm={12}>
                        <StyledRadio value="UserPerson">Person</StyledRadio>
                      </Col>
                      <Col xs={24} sm={12}>
                        <StyledRadio value="UserDiveCenter">
                          Dive Center
                        </StyledRadio>
                      </Col>
                    </Row>
                  </StyledRadioGroup>
                )}
              </Form.Item>
            </Col>
          ) : (
            <FormItem>
              {getFieldDecorator("userable_type", {
                initialValue: user.userable && user.userable.type_name,
              })(<Input type="hidden" />)}
            </FormItem>
          )}

          {this.props.creating && userType === "UserPerson" && (
            <React.Fragment>
              <Col lg={24} xl={12}>
                <FormItem hasFeedback label="Birthday">
                  {getFieldDecorator("b_day", {
                    initialValue: user.b_day ? moment(user.b_day) : null,
                    rules: this.rules.birthday,
                  })(<DatePicker style={{ width: "100%" }} />)}
                </FormItem>
              </Col>
              <Col lg={24} xl={12}>
                <FormItem hasFeedback label="Gender">
                  {getFieldDecorator("gender", {
                    initialValue: user.gender,
                    rules: this.rules.gender,
                  })(
                    <Select placeholder="Select your gender">
                      <Option value="m">Male</Option>
                      <Option value="f">Female</Option>
                    </Select>
                  )}
                </FormItem>
              </Col>
              <Col lg={24}>
                <FormItem hasFeedback label="Country">
                  {getFieldDecorator("country", {
                    initialValue: user.country,
                    rules: this.rules.country,
                  })(<CountrySelect />)}
                </FormItem>
              </Col>
            </React.Fragment>
          )}
          {this.props.creating && userType === "UserDiveCenter" && (
            <React.Fragment>
              <Col xs={24}>
                <FormItem hasFeedback label="Address">
                  {getFieldDecorator("address", {
                    initialValue: user.address,
                    rules: this.rules.address,
                  })(<Input placeholder="Address" />)}
                </FormItem>
              </Col>

              <Col xs={24} sm={12}>
                <FormItem hasFeedback label="Latitude">
                  {getFieldDecorator("latitude", {
                    initialValue: user.latitude,
                    rules: this.rules.latitude,
                  })(<Input placeholder="Latitude" />)}
                </FormItem>
              </Col>

              <Col xs={24} sm={12}>
                <FormItem hasFeedback label="Longitude">
                  {getFieldDecorator("longitude", {
                    initialValue: user.longitude,
                    rules: this.rules.longitude,
                  })(<Input placeholder="Longitude" />)}
                </FormItem>
              </Col>
            </React.Fragment>
          )}
          <Col xs={24}>
            <Row type="flex" justify="space-around" gutter={64}>
              <FormItem>
                {getFieldDecorator("active", {
                  initialValue: user.active,
                  valuePropName: "checked",
                })(<Checkbox> Active</Checkbox>)}
              </FormItem>
              <FormItem>
                {getFieldDecorator("admin", {
                  initialValue: user.admin,
                  valuePropName: "checked",
                })(<Checkbox> Admin</Checkbox>)}
              </FormItem>
              <FormItem>
                {getFieldDecorator("is_verified", {
                  initialValue: user.is_verified,
                  valuePropName: "checked",
                })(<Checkbox> Verified</Checkbox>)}
              </FormItem>
            </Row>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(
  connect(
    null,
    null,
    null,
    { withRef: true }
  )(UserForm)
);
