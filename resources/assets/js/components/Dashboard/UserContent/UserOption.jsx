import React, { Component } from "react";
import { Select } from "antd";

const Option = Select.Option;

const UserOption = ({ user }) => {
  return (
    <Option value={user.id} key={user.id}>
      {user.userable.user.name}
    </Option>
  );
};

export default UserOption;
