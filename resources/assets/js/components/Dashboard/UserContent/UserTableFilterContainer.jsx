import React, { Component } from "react";
import {
    fetchUsers,
    deleteUser,
    activateUser,
    setUserEdit,
    sendRecoverPasswordEmail,
} from "redux-modules/user/actions";
import { connect } from "react-redux";
import { Checkbox, Icon, Menu } from "antd";
import RowOperation from "../Common/RowOperation";
import TablePagination from "../Common/TablePagination";
import TableFilterRow from "../Common/TableFilterRow";
import {
    startCreating,
    startEditing,
} from "redux-modules/editCreateModal/actions";
import { NoDataMessage } from "helpers";
import UserDrawer from "./UserDrawer";
import StopPropagation from "../Common/StopPropagation";

class UserTableFilterContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drawerContent: null,
        };
        this.filters = {};
        this.columns = [
            {
                title: "ID",
                dataIndex: "id",
                sorter: true,
            },
            {
                title: "Type",
                dataIndex: "userable.type_name",
            },
            {
                title: "Email",
                dataIndex: "email",
            },
            {
                title: "Points",
                dataIndex: "rating.points",
            },
            {
                title: "Level",
                dataIndex: "rating.level",
            },
            {
                title: "Active",
                dataIndex: "active",
                filters: [
                    { text: "Active", value: 1 },
                    { text: "Unactive", value: 0 },
                ],
                filterMultiple: false,
                render: (active, user) => (
                    <Checkbox
                        onChange={() => this.props.activateUser(user.id)}
                        checked={active}
                    />
                ),
            },
            {
                title: "Verified",
                dataIndex: "is_verified",
                render: (is_verified, row) => (is_verified ? "Yes" : "No"),
                filters: [
                    { text: "Yes", value: 1 },
                    { text: "No", value: 0 },
                ],
                filterMultiple: false,
            },
            {
                title: "Role",
                dataIndex: "roles",
                render: (roles) =>
                    roles
                        ? `${roles[0].name}`
                        : NoDataMessage("Not categorized"),
            },
            {
                title: "Note",
                dataIndex: "note",
            },
            {
                title: "",
                dataIndex: "",
                render: (user) => (
                    <StopPropagation>
                        <RowOperation
                            updateRow
                            deleteRow
                            onUpdateClick={() => this.onUpdateClick(user)}
                            onDeleteConfirm={() =>
                                this.props.deleteUser(user.id)
                            }
                        >
                            <Menu.Item
                                onClick={() =>
                                    this.props.sendRecoverPasswordEmail(user.id)
                                }
                            >
                                <a href="javascript:;">
                                    <Icon type="mail" /> Send Recover Email
                                </a>
                            </Menu.Item>
                        </RowOperation>
                    </StopPropagation>
                ),
            },
        ];
    }

    componentDidMount = () => {
        this.props.fetchUsers();
    };

    onUpdateClick = (user) => {
        this.props.setUserEdit(user);
        this.props.startEditing();
    };

    onSearch = (search) => {
        this.filters = { ...this.filters, search };
        this.props.fetchUsers(1, this.filters);
    };

    handleTableChange = (pagination, filters, sorter) => {
        this.filters = {
            ...this.filters,
            ...filters,
            order: [sorter.field, sorter.order],
        };
        this.props.fetchUsers(pagination.current, this.filters);
    };

    handleRowClick = (record) => {
        this.setState({
            drawerContent: record,
        });
    };

    handleDrawerClose = () => {
        this.setState({
            drawerContent: null,
        });
    };

    render() {
        let { drawerContent } = this.state;
        return (
            <div>
                <UserDrawer
                    handleDrawerClose={this.handleDrawerClose}
                    visible={drawerContent && true}
                    content={drawerContent}
                />
                <TableFilterRow
                    search
                    onSearch={(search) => this.onSearch(search)}
                    searchPlaceholder="Search Users"
                    onCreateClick={() => this.props.startCreating()}
                />
                <TablePagination
                    loading={this.props.loading}
                    columns={this.columns}
                    handleTableChange={this.handleTableChange}
                    data={this.props.data}
                    meta={this.props.meta}
                    onRow={(record) => ({
                        onClick: () => {
                            this.handleRowClick(record);
                        },
                    })}
                />
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUsers: (page, filters) => dispatch(fetchUsers(page, filters)),
        deleteUser: (id) => dispatch(deleteUser(id)),
        activateUser: (id) => dispatch(activateUser(id)),
        startCreating: () => dispatch(startCreating()),
        startEditing: () => dispatch(startEditing()),
        setUserEdit: (data) => dispatch(setUserEdit(data)),
        sendRecoverPasswordEmail: (id) =>
            dispatch(sendRecoverPasswordEmail(id)),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.user.data,
        meta: state.user.meta,
        loading: state.user.loading,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserTableFilterContainer);
