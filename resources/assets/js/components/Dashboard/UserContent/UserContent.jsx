import React from "react";
import UserModalContainer from "./UserModalContainer";
import UserTableFilterContainer from "./UserTableFilterContainer";

const UserContent = () => {
  return (
    <div>
      <UserModalContainer />
      <UserTableFilterContainer />
    </div>
  );
};

export default UserContent;
