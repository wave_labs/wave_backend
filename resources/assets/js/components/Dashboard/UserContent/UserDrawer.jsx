import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import moment from "moment";
import { genderConverter, profileInfo } from "helpers";
import DrawerContainer from "../Common/DrawerContainer";
import ProfileContainer from "../Common/ProfileContainer";

const ProfileImageContainer = styled(Row)`
  margin: 50px auto;
`;

const StyledDiv = styled.div`
  font-size: 14;
  line-height: 22px;
  margin-bottom: 7px;
  color: rgba(0, 0, 0, 0.65);
`;

const StyledParagraph = styled.p`
  margin-right: 8px;
  display: inline-block;
  color: rgba(0, 0, 0, 0.85);
  &:after {
    content: ":";
  }
`;

const DescriptionItem = ({ span, title, content }) => (
  <Col span={span}>
    <StyledDiv>
      <StyledParagraph>{title}</StyledParagraph>
      {content}
    </StyledDiv>
  </Col>
);

class UserDrawer extends Component {
  state = {
    drawerWidth: "400px",
  };
  updateDimensions() {
    if (window.innerWidth > 1300) {
      this.setState({ drawerWidth: "30%" });
    } else if (window.innerWidth > 768 && window.innerWidth < 1300) {
      this.setState({ drawerWidth: "50%" });
    } else if (window.innerWidth < 768) {
      this.setState({ drawerWidth: "80%" });
    }
  }

  componentDidMount = () => {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  render() {
    let { content } = this.props;

    return (
      <DrawerContainer
        handleDrawerClose={this.props.handleDrawerClose}
        visible={this.props.visible}
        width={this.state.drawerWidth}
      >
        {content && (
          <React.Fragment>
            <ProfileImageContainer>
              <ProfileContainer content={content} />
            </ProfileImageContainer>

            <Row>
              <DescriptionItem
                span={12}
                title="Name"
                content={content.userable.user.name}
              />

              <DescriptionItem
                span={12}
                title="Account"
                content={content.email}
              />

              {content.userable.type_name == "App\\UserPerson" ? (
                <React.Fragment>
                  <DescriptionItem
                    span={12}
                    title="Birthday"
                    content={profileInfo(
                      content.userable.user.b_day &&
                        moment(content.userable.user.b_day).format(
                          "dddd, MMMM Do YYYY"
                        )
                    )}
                  />
                  <DescriptionItem
                    span={12}
                    title="Country"
                    content={profileInfo(content.userable.user.country)}
                  />
                  <DescriptionItem
                    span={12}
                    title="Gender"
                    content={profileInfo(
                      genderConverter[content.userable.user.gender]
                    )}
                  />
                  <DescriptionItem
                    span={12}
                    title="Occupation"
                    content={profileInfo(
                      content.occupation && content.occupation.name
                    )}
                  />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <DescriptionItem
                    span={12}
                    title="Address"
                    content={profileInfo(content.userable.user.adress)}
                  />

                  <DescriptionItem
                    span={12}
                    title="Coordinates"
                    content={
                      content.userable.user.latitude +
                      ", " +
                      content.userable.user.longitude
                    }
                  />
                </React.Fragment>
              )}

              <DescriptionItem
                span={24}
                title="About"
                content={profileInfo(content.note)}
              />
            </Row>
          </React.Fragment>
        )}
      </DrawerContainer>
    );
  }
}

export default UserDrawer;
