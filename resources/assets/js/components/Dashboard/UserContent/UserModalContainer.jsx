import React, { Component } from "react";
import {
  updateUser,
  createUser,
  resetUserEdit,
} from "redux-modules/user/actions";
import {
  resetModal,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../Common/EditCreateModal";
import UserForm from "./UserForm";
import moment from "moment";

class UserModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
    userType: null,
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, user) => {
      if (err) {
        return;
      } else {
        const formatedDate = moment(user.b_day).format("YYYY-MM-DD");

        this.setState({ confirmLoading: true });
        this.props
          .updateUser(this.props.editUser.id, {
            ...user,
            b_day: formatedDate,
          })
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false, userType: null });
    this.props.resetUserEdit();
    this.props.resetModal();
    form.resetFields();
  };

  handleUserTypeChange = (e) => {
    this.setState({
      userType: e.target.value,
    });
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, user) => {
      if (err) {
        return;
      } else {
        const formatedDate = user.b_day
          ? moment(userPerson.b_day).format("YYYY-MM-DD")
          : null;
        this.setState({ confirmLoading: true });
        this.props
          .createUser({
            ...user,
            b_day: formatedDate,
            is_verified: true,
          })
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit User"
        titleCreate="Create User"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <UserForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          user={this.props.editUser}
          userType={this.state.userType}
          handleUserTypeChange={this.handleUserTypeChange}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetUserEdit: () => dispatch(resetUserEdit()),
    resetModal: () => dispatch(resetModal()),
    updateUser: (data, id) => dispatch(updateUser(data, id)),
    createUser: (data) => dispatch(createUser(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    editUser: state.user.editUser,
    editing: state.editCreateModal.editing,
    creating: state.editCreateModal.creating,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserModalContainer);
