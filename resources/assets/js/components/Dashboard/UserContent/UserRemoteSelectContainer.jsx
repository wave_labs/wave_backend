import React from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/user/actions";
import SelectSearch from "../Common/SelectSearch";

const Option = Select.Option;

class UserRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      UserSelector,
      loading,
      onChange,
      value,
      children,
      placeholder = "User",
    } = this.props;

    return (
      <SelectSearch
        mode="multiple"
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
      >
        {children}
        {UserSelector.map((user) => (
          <Option value={user.id} key={user.id}>
            {user.userable.user.name} ({user.email})
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    UserSelector: state.user.UserSelector,
    loading: state.user.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserRemoteSelectContainer);
