import React from "react";
import { connect } from "react-redux";
import { Row } from "antd";
import { setUserView } from "redux-modules/user/actions";
import { views } from "redux-modules/user/types";
import styled from "styled-components";

const StyledB = styled.b`
  margin: 1rem;
  font-weight: ${({ active }) => (active ? "bold" : "normal")};
`;

const UserViewChooser = ({ view, setUserView }) => {
  return (
    <Row gutter={16} type="flex" justify="center" align="middle">
      <StyledB
        active={views.USER_VIEW == view ? 1 : 0}
        onClick={() => setUserView(views.USER_VIEW)}
      >
        ALL
      </StyledB>
      <StyledB
        active={views.DIVE_VIEW == view ? 1 : 0}
        onClick={() => setUserView(views.DIVE_VIEW)}
      >
        DIVE CENTER
      </StyledB>
      <StyledB
        active={views.PERSON_VIEW == view ? 1 : 0}
        onClick={() => setUserView(views.PERSON_VIEW)}
      >
        PERSON
      </StyledB>
    </Row>
  );
};

export default connect(
  state => ({
    view: state.user.view
  }),
  { setUserView }
)(UserViewChooser);
