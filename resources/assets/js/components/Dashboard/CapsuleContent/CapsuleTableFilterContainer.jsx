import React, { Component } from "react";
import {
  fetchCapsules,
  deleteCapsule,
  setCapsuleEdit,
  updateCapsule
} from "redux-modules/capsule/actions";
import { connect } from "react-redux";
import { Avatar, Checkbox } from "antd";
import RowOperation from "../Common/RowOperation";
import TablePagination from "../Common/TablePagination";
import TableFilterRow from "../Common/TableFilterRow";
import {
  startCreating,
  startEditing
} from "redux-modules/editCreateModal/actions";

class CapsuleTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
        width: 75
      },
      {
        title: "Photo",
        dataIndex: "image_link",
        render: image_link => (
          <a
            target="_blank"
            rel="noopener noreferrer"
            href={`${window.location.origin}/storage/${image_link}`}
          >
            <Avatar src={`${window.location.origin}/storage/${image_link}`} />
          </a>
        )
      },
      {
        title: "User",
        dataIndex: "userable.user.name"
      },
      {
        title: "Coordinates",
        dataIndex: "latitude",
        render: (latitude, row) => `(${latitude}, ${row.longitude})`
      },
      {
        title: "Temperature (C)",
        dataIndex: "temperature_c",
        sorter: true,
        width: 175
      },
      {
        title: "Ram (Mb)",
        dataIndex: "ram_mb",
        sorter: true,
        width: 125
      },
      {
        title: "Disk Space (Gb)",
        dataIndex: "disk_space_gb",
        sorter: true,
        width: 150
      },
      {
        title: "Volts",
        dataIndex: "volts",
        sorter: true
      },
      {
        title: "Current",
        dataIndex: "current",
        sorter: true,
        width: 125
      },
      {
        title: "Power",
        dataIndex: "power",
        sorter: true,
        width: 125
      },
      {
        title: "Assigned",
        dataIndex: "assigned",
        filters: [
          { text: "Assigned", value: 1 },
          { text: "Unassigned", value: 0 }
        ],
        filterMultiple: false,
        witdh: 175,
        render: (assigned, capsule) => (
          <Checkbox
            onChange={e => {
              this.props.updateCapsule(capsule.id, {
                assigned: e.target.checked
              });
            }}
            checked={assigned}
          />
        )
      },
      {
        title: "Description",
        dataIndex: "description"
      },
      {
        title: "",
        key: "",
        render: (text, capsule) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(capsule)}
            onDeleteConfirm={() => this.props.deleteCapsule(capsule.id)}
          />
        )
      }
    ];
  }

  componentDidMount = () => {
    this.props.fetchCapsules();
  };

  onUpdateClick = capsule => {
    this.props.setCapsuleEdit(capsule);
    this.props.startEditing();
  };

  onSearch = search => {
    this.filters = { ...this.filters, search };
    this.props.fetchCapsules(1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order]
    };
    this.props.fetchCapsules(pagination.current, this.filters);
  };
  render() {
    return (
      <div>
        <TableFilterRow
          onSearch={search => this.onSearch(search)}
          searchPlaceholder="Search Capsules"
          onCreateClick={() => this.props.startCreating()}
        />
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchCapsules: (page, filters) => dispatch(fetchCapsules(page, filters)),
    deleteCapsule: id => dispatch(deleteCapsule(id)),
    activateCapsule: id => dispatch(activateCapsule(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setCapsuleEdit: data => dispatch(setCapsuleEdit(data)),
    updateCapsule: (id, data) => dispatch(updateCapsule(id, data))
  };
};

const mapStateToProps = state => {
  return {
    data: state.capsule.data,
    meta: state.capsule.meta,
    loading: state.capsule.loading
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CapsuleTableFilterContainer);
