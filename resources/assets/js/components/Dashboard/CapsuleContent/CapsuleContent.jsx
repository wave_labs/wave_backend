import React from 'react';
import CapsuleModalContainer from './CapsuleModalContainer'
import CapsuleTableFilterContainer from './CapsuleTableFilterContainer'

const CapsuleContent = () => {
	return (
			<div>
				<CapsuleModalContainer />
				<CapsuleTableFilterContainer />
			</div>
	)
}

export default CapsuleContent;