import React, { Component } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  InputNumber,
  Button,
  Checkbox,
  Select
} from "antd";
import { Link } from "react-router-dom";
import UserRemoteSelectContainer from "../UserContent/UserRemoteSelectContainer";
import UploadFileFormItem from "../Common/UploadFileFormItem";

const FormItem = Form.Item;
const TextArea = Input.TextArea;
const Option = Select.Option;

const StyledInputNumber = styled(InputNumber)`
  min-width: 100% !important;
`;

class CapsuleForm extends Component {
  createDefaultFile = link => {
    return {
      uid: "-1",
      name: link,
      status: "done",
      defaultFile: true,
      url: `${window.location.origin}/storage/${link}`
    };
  };

  state = {};

  rules = {
    userId: [
      {
        required: true,
        message: "User  is required"
      }
    ],
    ram: [
      {
        required: true,
        message: "Ram is required"
      }
    ],
    diskSpace: [
      {
        required: true,
        message: "Disk space is required"
      }
    ],
    volts: [
      {
        required: true,
        message: "Volts is required"
      }
    ],
    current: [
      {
        required: true,
        message: "Current is required"
      }
    ]
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { capsule, editing } = this.props;
    const { user } = capsule;

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="User">
              {getFieldDecorator("user_id", {
                initialValue: user.id,
                rules: this.rules.userId
              })(
                <UserRemoteSelectContainer>
                  {editing && (
                    <Option value={user.id} key={user.id}>
                      {user.first_name} {user.last_name}
                    </Option>
                  )}
                </UserRemoteSelectContainer>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Disk Space">
              {getFieldDecorator("disk_space_gb", {
                initialValue: capsule.disk_space_gb,
                rules: this.rules.diskSpace
              })(<StyledInputNumber placeholder="Disk Space" />)}
            </FormItem>
          </Col>
        </Row>

        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Volts">
              {getFieldDecorator("volts", {
                initialValue: capsule.volts,
                rules: this.rules.volts
              })(<StyledInputNumber placeholder="Volts" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Current">
              {getFieldDecorator("current", {
                initialValue: capsule.current,
                rules: this.rules.current
              })(<StyledInputNumber placeholder="Current" />)}
            </FormItem>
          </Col>
        </Row>

        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <Row type="flex" gutter={8}>
              <Col xs={24} sm={12}>
                <FormItem hasFeedback label="Latitude">
                  {getFieldDecorator("latitude", {
                    initialValue: capsule.latitude
                  })(<StyledInputNumber placeholder="Latitude" />)}
                </FormItem>
              </Col>
              <Col xs={24} sm={12}>
                <FormItem hasFeedback label="Longitude">
                  {getFieldDecorator("longitude", {
                    initialValue: capsule.longitude
                  })(<StyledInputNumber placeholder="Longitude" />)}
                </FormItem>
              </Col>
            </Row>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Ram">
              {getFieldDecorator("ram_mb", {
                initialValue: capsule.ram_mb,
                rules: this.rules.ram
              })(<StyledInputNumber placeholder="Ram" />)}
            </FormItem>
          </Col>
        </Row>
        <FormItem hasFeedback label="Description">
          {getFieldDecorator("description", {
            initialValue: capsule.description
          })(
            <TextArea
              placeholder="Notes"
              autosize={{ minRows: 2, maxRows: 6 }}
            />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator("assigned", {
            initialValue: capsule.assigned,
            valuePropName: "checked"
          })(<Checkbox> Assigned Capsule</Checkbox>)}
        </FormItem>
        <UploadFileFormItem
          form={this.props.form}
          rules={null}
          link={capsule.image_link}
          key={capsule.id}
        />
      </Form>
    );
  }
}

export default Form.create()(CapsuleForm);
