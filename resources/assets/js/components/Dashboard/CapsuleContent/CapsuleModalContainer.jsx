import React, { Component } from "react";
import {
	createCapsule,
	updateCapsule,
	resetCapsuleEdit
} from "redux-modules/capsule/actions";
import {
	resetModal,
	startEditing
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../Common/EditCreateModal";
import CapsuleForm from "./CapsuleForm";

class CapsuleModalContainer extends Component {
	state = {
		confirmLoading: false //modal button loading icon
	};

	onCancel = () => {
		this.resetModalForm();
	};

	onOkEditClick = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, capsule) => {
			if (err) {
				return;
			} else {
				this.setState({ confirmLoading: true });
				const data = this.createFormData(capsule);

				this.props
					.updateCapsule(this.props.editCapsule.id, data)
					.then(data => {
						this.resetModalForm();
					}).catch(e => this.setState({ confirmLoading: false }));
			}
		});
	};

	resetModalForm = () => {
		const form = this.formRef.props.form;
		this.setState({ confirmLoading: false });
		this.props.resetCapsuleEdit();
		this.props.resetModal();
		form.resetFields();
	};

	onOkCreateClick = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, capsule) => {
			if (err) {
				return;
			} else {
				this.setState({ confirmLoading: true });
				const data = this.createFormData(capsule);

				this.props.createCapsule(data).then(data => {
					this.resetModalForm();
				}).catch(e => this.setState({ confirmLoading: false }));
			}
		});
	};

	createFormData = capsule => {
		let data = new FormData();
		data.append("file", capsule.file);
		data.append("current", capsule.current);
		data.append("disk_space_gb", capsule.disk_space_gb);
		data.append("latitude", capsule.latitude);
		data.append("longitude", capsule.longitude);
		data.append("ram_mb", capsule.ram_mb);
		data.append("volts", capsule.volts);
		data.append("user_id", capsule.user_id);
		data.append("description", capsule.description);

		if (capsule.assigned) {
			data.append("assigned", capsule.assigned ? 1 : 0);
		}

		if (this.props.editing) {
			//Php doens't allow puts with formDatas for some reason, so we must spoof it with a post
			data.append("_method", "PUT");
		}
		return data;
	};

	saveFormRef = formRef => {
		this.formRef = formRef;
	};

	render() {
		return (
			<EditCreateModal
				titleEdit="Edit Capsule"
				titleCreate="Create Capsule"
				onOkEditClick={() => this.onOkEditClick()}
				onOkCreateClick={() => this.onOkCreateClick()}
				confirmLoading={this.state.confirmLoading}
				onCancel={() => this.onCancel()}
				editing={this.props.editing}
				creating={this.props.creating}
			>
				<CapsuleForm
					creating={this.props.creating}
					editing={this.props.editing}
					wrappedComponentRef={this.saveFormRef}
					capsule={this.props.editCapsule}
				/>
			</EditCreateModal>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		resetCapsuleEdit: () => dispatch(resetCapsuleEdit()),
		resetModal: () => dispatch(resetModal()),
		createCapsule: data => dispatch(createCapsule(data)),
		updateCapsule: (data, id) => dispatch(updateCapsule(data, id))
	};
};

const mapStateToProps = state => {
	return {
		editCapsule: state.capsule.editCapsule,
		creating: state.editCreateModal.creating,
		editing: state.editCreateModal.editing
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CapsuleModalContainer);
