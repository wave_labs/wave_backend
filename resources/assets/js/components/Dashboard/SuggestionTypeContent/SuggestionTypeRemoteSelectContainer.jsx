import React from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/suggestionType/actions";
import SelectSearch from "../Common/SelectSearch";

const Option = Select.Option;

class SuggestionTypeRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      SuggestionTypeSelector,
      loading,
      onChange,
      value,
      children,
    } = this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Suggestion Type"
      >
        {children}
        {SuggestionTypeSelector.map((suggestionType) => (
          <Option value={suggestionType.id} key={suggestionType.id}>
            {suggestionType.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    SuggestionTypeSelector: state.suggestionType.SuggestionTypeSelector,
    loading: state.suggestionType.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SuggestionTypeRemoteSelectContainer);
