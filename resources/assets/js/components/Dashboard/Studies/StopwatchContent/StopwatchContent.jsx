import React, { Component } from "react";
import { connect } from "react-redux";
import StopwatchTableContainer from "./StopwatchTableContainer.jsx";
import StopwatchFilters from "./StopwatchFilters.jsx";

class StopwatchContent extends Component {
  render() {
    return (
      <div>
        <StopwatchFilters />
        <StopwatchTableContainer history={this.props.history} />
      </div>
    );
  }
}

export default StopwatchContent;