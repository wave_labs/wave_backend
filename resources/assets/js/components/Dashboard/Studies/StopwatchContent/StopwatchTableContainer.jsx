import React, { Component } from "react";
import {
  fetchStopwatchResults,
  deleteStopwatchResult,
  setCurrentStopwatchResult,
} from "redux-modules/stopwatch/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import StopPropagation from "../../Common/StopPropagation";

class StopwatchTableContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activities: [],
    };
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "Category",
        dataIndex: "video.category",
      },
      {
        title: "Video",
        dataIndex: "video.id",
      },
      {
        title: "User",
        dataIndex: "occupation",
      },
      {
        title: "Classifications",
        dataIndex: "timestamps",
        render: (timestamps) => (
          <span>
            {this.handleClassifications(timestamps).map((category, index) => (
              <span key={index}>
                {category}
                {index != timestamps.length - 1 && ", "}
              </span>
            ))}
          </span>
        ),
      },
      {
        title: "Date",
        dataIndex: "created_at",
      },
      {
        title: "",
        key: "",
        render: (text, record) => (
          <StopPropagation>
            <RowOperation
              deleteRow
              onDeleteConfirm={() =>
                this.props.deleteStopwatchResult(record.id)
              }
            />
          </StopPropagation>
        ),
      },
    ];
  }

  handleClassifications = (timestamps) => {
    let classes = [];
    timestamps.map((category) => {
      !classes.includes(category.class.name) &&
        classes.push(category.class.name);
    });

    return classes;
  };

  componentDidMount = () => {
    this.props.fetchStopwatchResults();
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchStopwatchResults(pagination.current, this.filters);
  };

  handleRowClick = (record) => {
    this.props.setCurrentStopwatchResult(record);
    this.props.history.push("stopwatch/" + record.id);
  };

  render() {
    return (
      <div>
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          onRow={(record) => ({
            onClick: () => {
              this.handleRowClick(record);
            },
          })}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchStopwatchResults: (page, filters) =>
      dispatch(fetchStopwatchResults(page, filters)),
    setCurrentStopwatchResult: (record) =>
      dispatch(setCurrentStopwatchResult(record)),
    deleteStopwatchResult: (id) => dispatch(deleteStopwatchResult(id)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.stopwatch.results,
    meta: state.stopwatch.meta,
    loading: state.stopwatch.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StopwatchTableContainer);
