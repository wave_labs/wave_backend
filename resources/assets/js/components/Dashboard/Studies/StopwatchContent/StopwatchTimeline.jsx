import React, { Component } from "react";
import { HorizontalBar } from "react-chartjs-2";
import { fetchStopwatchResult } from "redux-modules/stopwatch/actions";
import { connect } from "react-redux";

const colors = [
  "#00253b",
  "#004672",
  "#0062a0",
  "#16a6ff",
  "#94d6ff",
  "#e0f5ff",
];

class StopwatchTimeline extends Component {
  state = {
    data: {
      labels: [],
      datasets: [],
    },
    options: {
      scales: {
        yAxes: [
          {
            stacked: true,
          },
        ],
      },
      responsive: true,
      legend: {
        display: false,
      },
      tooltips: {
        callbacks: {
          label: function(tooltipItem) {
            return tooltipItem.xLabel;
          },
        },
      },
    },
  };
  componentDidMount() {
    if (Object.keys(this.props.currentResult).length === 0) {
      this.props.fetchStopwatchResult(this.props.match.params.id);
    }
    setTimeout(() => {
      this.handleData();
    }, 1);
  }

  handleData() {
    let { data } = this.state;
    let { currentResult } = this.props;
    let datasets = [];
    if (Object.keys(currentResult).length > 0) {
      currentResult.timestamps.map((element, index) => {
        element.video;
        let data = [];
        for (let i = 0; i < 5; i++) {
          if (i == element.class.order) {
            data[i] = [element.start_time, element.end_time];
          } else {
            data[i] = 0;
          }
        }

        datasets.push({
          label: index,
          data: data,
          backgroundColor: colors[element.class.order],
          borderWidth: 0,
        });
      });
      data.datasets = datasets;
      data.labels =
        currentResult.video.category == "biodiversity"
          ? ["Dolphin", "Whale", "Seal", "Seabird", "Sea Turtle"]
          : ["Plastic", "Rubber", "Metal", "Cloth / Textile"];
      this.setState({ data });
    }
  }
  render() {
    console.log(this.state.data);
    return (
      <HorizontalBar data={this.state.data} options={this.state.options} />
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchStopwatchResult: (id) => dispatch(fetchStopwatchResult(id)),
  };
};

const mapStateToProps = (state) => {
  return {
    currentResult: state.stopwatch.currentResult,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StopwatchTimeline);
