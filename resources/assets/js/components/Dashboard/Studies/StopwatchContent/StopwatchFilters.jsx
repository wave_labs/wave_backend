import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Select } from "antd";
import styled from "styled-components";
import moment from "moment";
import { fetchStopwatchResults } from "redux-modules/stopwatch/actions";
import FilterRow from "../../Common/FilterRow";

const { Option } = Select;
const StyledRow = styled(Row)`
  margin: 1rem;
`;

class StopwatchFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {
        video: undefined,
        category: undefined,
        date: undefined,
        search: undefined,
      },
    };

    this.initialState = this.state;
  }

  handleFilterChange = (filter, value) => {
    var { filters } = this.state;
    filters[filter] = value;
    this.setState({ filters });
  };

  handleReset = () => {
    this.setState({
      filters: { video: undefined, category: undefined, date: undefined },
    });
  };

  handleFilters = () => {
    var { video, category, date, search } = this.state.filters;
    let newDate = date ? [...date] : [];

    return {
      video: video,
      category: category,
      search: search,
      date:
        newDate.length > 0
          ? [
              moment(newDate[0]).format("YYYY-MM-DD"),
              moment(newDate[1]).format("YYYY-MM-DD"),
            ]
          : newDate,
    };
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state.filters) {
      var filters = this.handleFilters();

      this.props.fetchStopwatchResults(1, filters);
    }
  }

  render() {
    return (
      <StyledRow>
        <FilterRow
          filters={["date", "search"]}
          filterValues={this.state.filters}
          operations={["reset"]}
          handleFilterChange={this.handleFilterChange}
          handleReset={() => this.handleReset()}
          additionalFilters={[
            <Select
              placeholder="Category"
              value={this.state.filters.category}
              style={{ width: "100%" }}
              onChange={(value) => this.handleFilterChange("category", value)}
            >
              <Option value={1}>Biodiversity</Option>
              <Option value={2}>Litter</Option>
            </Select>,
          ]}
        />
      </StyledRow>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchStopwatchResults: (page, filters) =>
      dispatch(fetchStopwatchResults(page, filters)),
  };
};

export default connect(
  null,
  mapDispatchToProps
)(StopwatchFilters);
