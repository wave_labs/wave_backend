import { Row, Col } from "antd";
import React from "react";
import { connect } from "react-redux";
import SoftCoralFilters from "./SoftCoralFilters";
import SoftCoralMap from "./SoftCoralMap";
import TroutTableContainer from "./SoftCoralTableContainer";

function SoftCoralContent({ isAdmin }) {
    return (
        <div>
            <SoftCoralFilters />

            <Row type="flex" gutter={24}>
                <Col xs={24} lg={12}>
                    <SoftCoralMap isAdmin={isAdmin} />
                </Col>
                <Col xs={24} lg={12}>
                    <TroutTableContainer isAdmin={isAdmin} />
                </Col>
            </Row>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        isAdmin: state.auth.isAdmin,
    };
};

export default connect(mapStateToProps, null)(SoftCoralContent);
