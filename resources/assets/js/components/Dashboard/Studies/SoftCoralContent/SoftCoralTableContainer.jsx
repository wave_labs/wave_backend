import React, { Component } from "react";
import {
    fetchSoftCorals,
    deleteSoftCoral,
} from "redux-modules/softCoral/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import { startCreating } from "redux-modules/editCreateModal/actions";
import { NoDataMessage } from "helpers";
import styled from "styled-components";

const Pin = styled.p`
    margin: 0;

    span {
        text-transform: lowercase;
    }
`;

class SoftCoralTableContainer extends Component {
    constructor(props) {
        super(props);

        this.filters = {};
        this.columns = [
            {
                title: "ID",
                dataIndex: "id",
                sorter: true,
            },
            {
                title: "Date",
                dataIndex: "date",
            },
            {
                title: "Occurrences",
                dataIndex: "coordinates",
                render: (coordinates) => (
                    <Pin key={index}>{coordinates.length}</Pin>
                ),
            },
            {
                title: "",
                key: "",
                render: (text, record) => (
                    <RowOperation
                        deleteRow
                        updateRow
                        onDeleteConfirm={() =>
                            this.props.deleteSoftCoral(record.id)
                        }
                    />
                ),
            },
        ];
    }

    componentDidMount = () => {
        this.props.fetchSoftCorals();
    };

    onSearch = (search) => {
        this.filters = { ...this.filters, search };
        this.props.fetchSoftCorals(1, this.filters);
    };

    handleTableChange = (pagination, filters, sorter) => {
        this.filters = {
            ...this.filters,
            ...filters,
            order: [sorter.field, sorter.order],
        };
        this.props.fetchSoftCorals(pagination.current, this.filters);
    };

    render() {
        return (
            <div>
                <TablePagination
                    loading={this.props.loading}
                    columns={this.columns}
                    handleTableChange={this.handleTableChange}
                    data={this.props.data}
                    meta={this.props.meta}
                />
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSoftCorals: (page, filters) => dispatch(fetchSoftCorals(page, filters)),
        deleteSoftCoral: (id) => dispatch(deleteSoftCoral(id)),
        startCreating: () => dispatch(startCreating()),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.softCoral.data,
        meta: state.softCoral.meta,
        loading: state.softCoral.loading,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SoftCoralTableContainer);
