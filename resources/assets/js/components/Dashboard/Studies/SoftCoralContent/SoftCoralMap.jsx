import React, { Component, PureComponent } from "react";
import { Marker } from "react-map-gl";
import { connect } from "react-redux";
import { fetchSoftCoralCoordinates } from "redux-modules/softCoral/actions";
import MapBoxMap from "../../Common/MapBoxMap";

// PureComponent ensures that the markers are only rerendered when data changes
class Markers extends PureComponent {
    render() {
        return this.props.children;
    }
}

class SoftCoralMap extends Component {
    constructor(props) {
        super(props);
        this.filters = {};
        this.state = {
            popUp: null,
        };
    }

    componentDidMount() {
        this.props.fetchSoftCoralCoordinates(this.filters);
    }

    render() {
        var { data } = this.props;

        return (
            <div className="map-container">
                <MapBoxMap zoom={9}>
                    <Markers>
                        {data.map((record) =>
                            record.coordinates.map((coordinates) => {
                                return (
                                    <Marker
                                        key={coordinates.id}
                                        longitude={parseFloat(
                                            coordinates.longitude
                                        )}
                                        latitude={parseFloat(
                                            coordinates.latitude
                                        )}
                                    >
                                        <svg
                                            style={{
                                                position: "absolute",
                                                top: "-10px",
                                                left: "-10px",
                                                width: "20px",
                                                height: "20px",
                                                fill: "red",
                                            }}
                                        >
                                            <circle
                                                cx="10"
                                                cy="10"
                                                r="10"
                                                stroke="none"
                                            />
                                        </svg>
                                    </Marker>
                                );
                            })
                        )}
                    </Markers>
                </MapBoxMap>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSoftCoralCoordinates: (filters) =>
            dispatch(fetchSoftCoralCoordinates(filters)),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.softCoral.coordinates,
        loading: state.softCoral.loadingCoordinates,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SoftCoralMap);
