import { Row, Col } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import FreshwaterPinFilters from "./FreshwaterPinFilters";
import FreshwaterPinMap from "./FreshwaterPinMap";
import FreshwaterPinTableContainer from "./FreshwaterPinTableContainer";

class FreshwaterPinContent extends Component {
  render() {
    return (
      <div>
        <FreshwaterPinFilters />

        <Row type="flex" gutter={24}>
          <Col xs={24} lg={12}>
            <FreshwaterPinMap isAdmin={this.props.isAdmin} />
          </Col>
          <Col xs={24} lg={12}>
            <FreshwaterPinTableContainer isAdmin={this.props.isAdmin} />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {};
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FreshwaterPinContent);
