import React, { Component } from "react";
import {
  fetchFreshwaterPins,
  deleteFreshwaterPin,
} from "redux-modules/freshwaterPin/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import { startCreating } from "redux-modules/editCreateModal/actions";
import { NoDataMessage } from "helpers";
import styled from "styled-components";

const Pin = styled.p`
  margin: 0;

  span {
    text-transform: lowercase;
  }
`;

const getSpecies = {
  "Yes, the A": "A",
  "Yes, the B": "B",
  "Yes, A and B": "A and B",
}

class FreshwaterPinTableContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activities: [],
    };
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "Date",
        dataIndex: "date",
      },
      {
        title: "Species",
        dataIndex: "form",
        render: (form) => <span>{getSpecies[form[3].answer]}</span>,
      },
      {
        title: "Occurrences",
        dataIndex: "coordinates",
        render: (coordinates) => (
          <Pin key={index}>
            {coordinates.length}
          </Pin>
        ),
      },
      {
        title: "",
        key: "",
        render: (text, record) => (
          <RowOperation
            deleteRow
            updateRow
            onDeleteConfirm={() => this.props.deleteFreshwaterPin(record.id)}
          />
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.props.fetchFreshwaterPins();
  };

  onSearch = (search) => {
    this.filters = { ...this.filters, search };
    this.props.fetchFreshwaterPins(1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchFreshwaterPins(pagination.current, this.filters);
  };

  render() {
    return (
      <div>
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchFreshwaterPins: (page, filters) =>
      dispatch(fetchFreshwaterPins(page, filters)),
    deleteFreshwaterPin: (id) => dispatch(deleteFreshwaterPin(id)),
    startCreating: () => dispatch(startCreating()),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.freshwaterPin.data,
    meta: state.freshwaterPin.meta,
    loading: state.freshwaterPin.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FreshwaterPinTableContainer);
