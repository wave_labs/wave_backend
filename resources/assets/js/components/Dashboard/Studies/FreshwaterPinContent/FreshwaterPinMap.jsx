import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import MapBoxMap from "../../Common/MapBoxMap";
import { Marker, Popup } from "react-map-gl";
import { fetchFreshwaterPinCoordinates } from "redux-modules/freshwaterPin/actions";
import PopUp from "./PopUp";
import styled from "styled-components";

const Pin = styled.img`
  cursor: pointer;
  width: 18px;
  opacity: 0.7;
`;

const LabelContainer = styled.div`
  position: absolute;
  right: 5px;
  bottom: 5px;
  background: #ffffffc3;
  padding: 5px;
`;

const Label = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;

  span {
    width: 70px;
    text-align: center;
    vertical-align: middle;
    font-size: .8em;
  }
  div {
    width: 70px;
    height: 12px;
    margin: 0 8px;
    background-image: linear-gradient(
      to right,
      ${(props) => props.colors[0]},
      ${(props) => props.colors[1]}
    );
  }
`;

const Subtitle = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;

  span {
    width: 70px;
    text-align: center;
    vertical-align: middle;
    font-size: .8em;
  }
  img { 
    width: 18px;
  }

  div {
    width: 70px;
    margin: 0 8px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;

// PureComponent ensures that the markers are only rerendered when data changes
class Markers extends PureComponent {
  render() {
    return this.props.children;
  }
}

class FreshwaterPinMap extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.state = {
      popUp: null,
    };
  }

  componentDidMount() {
    this.props.fetchFreshwaterPinCoordinates(this.filters);
  }

  showLitter = () => {
    const { popUp } = this.state;

    return (
      popUp && (
        <Popup
          longitude={parseFloat(popUp.longitude)}
          latitude={parseFloat(popUp.latitude)}
          onClose={() => this.setState({ popUp: null })}
        >
          <PopUp data={popUp} />
        </Popup>
      )
    );
  };

  handleClick = (record, coordinates) => {
    this.props.isAdmin &&
      this.setState({
        popUp: {
          date: record.date,
          timeframe: coordinates.timeframe,
          specie: coordinates.image,
          id: coordinates.id,
          longitude: coordinates.longitude,
          latitude: coordinates.latitude,
        },
      });
  };

  getImageName = (coordinates) => {
    return coordinates.image == "A" ?
      coordinates.timeframe == "In the last 5 years" ? "A1" : "A2"
      : coordinates.timeframe == "In the last 5 years" ? "B1" : "B2";
  };

  render() {
    var { data } = this.props;

    return (
      <div className="map-container">
        <MapBoxMap zoom={9}>
          <Markers>
            {data.map((record) =>
              record.coordinates.map((coordinates) => (
                <Marker
                  key={coordinates.id}
                  longitude={parseFloat(coordinates.longitude)}
                  latitude={parseFloat(coordinates.latitude)}
                >
                  <Pin
                    src={"/images/studies/freshwater-pin/icon" + this.getImageName(coordinates) + ".svg"}
                    onClick={() => this.handleClick(record, coordinates)}
                  >
                  </Pin>
                </Marker>
              ))
            )}
          </Markers>

          {this.showLitter()}
          <LabelContainer>
            <Subtitle >
              <span>A</span>
              <div>
                <img src="/images/studies/freshwater-pin/iconA.svg" />
                <img src="/images/studies/freshwater-pin/iconB.svg" />
              </div >
              <span>B</span>
            </Subtitle>
            <Label colors={["#3d81ff", "#001138"]}>
              <span>In the last 5 years</span>
              <div />
              <span>Over 5 years ago</span>
            </Label>

          </LabelContainer>
        </MapBoxMap>
      </div >
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchFreshwaterPinCoordinates: (filters) =>
      dispatch(fetchFreshwaterPinCoordinates(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.freshwaterPin.coordinates,
    loading: state.freshwaterPin.loadingCoordinates,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FreshwaterPinMap);
