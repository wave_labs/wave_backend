import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Select } from "antd";
import styled from "styled-components";
import moment from "moment";
import {
  fetchFreshwaterPinCoordinates,
  fetchFreshwaterPins,
  exportFreshwaterPin,
} from "redux-modules/freshwaterPin/actions";
import FilterRow from "../../Common/FilterRow";

const { Option } = Select;

const StyledRow = styled(Row)`
  margin: 1rem;
`;

class FreshwaterPinFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {
        date: undefined,
      },
    };

    this.initialState = this.state;
  }

  handleFilterChange = (filter, value) => {
    var { filters } = this.state;
    filters[filter] = value;
    this.setState({ filters });
  };

  handleReset = () => {
    this.setState({
      filters: { date: undefined },
    });
  };

  handleDownload = () => {
    var filters = this.handleFilters();

    this.props.exportFreshwaterPin(filters);
  };

  handleFilters = () => {
    var { date } = this.state.filters;
    let newDate = date ? [...date] : [];

    return {

      date:
        newDate.length > 0
          ? [
            moment(newDate[0]).format("YYYY-MM-DD"),
            moment(newDate[1]).format("YYYY-MM-DD"),
          ]
          : newDate,
    };
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state.filters) {
      var filters = this.handleFilters();

      this.props.fetchFreshwaterPinCoordinates(filters);
      this.props.fetchFreshwaterPins(1, filters);
    }
  }

  render() {
    let { filters } = this.state;
    return (
      <StyledRow>
        <FilterRow
          filters={["date"]}
          filterValues={this.state.filters}
          operations={["reset", "download"]}
          handleFilterChange={this.handleFilterChange}
          handleDownload={() => this.handleDownload()}
          handleReset={() => this.handleReset()}

        />
      </StyledRow>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchFreshwaterPinCoordinates: (filters) =>
      dispatch(fetchFreshwaterPinCoordinates(filters)),
    fetchFreshwaterPins: (page, filters) =>
      dispatch(fetchFreshwaterPins(page, filters)),
    exportFreshwaterPin: (filters) => dispatch(exportFreshwaterPin(filters)),
  };
};

export default connect(
  null,
  mapDispatchToProps
)(FreshwaterPinFilters);
