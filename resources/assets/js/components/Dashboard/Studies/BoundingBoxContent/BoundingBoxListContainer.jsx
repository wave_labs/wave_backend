import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import styled from "styled-components";
import { Row, Tag, Col, Popconfirm, Avatar } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
import {
  fetchBoundingBoxResults,
  deleteBoundingBoxResult,
} from "redux-modules/boundingBox/actions";
import { dimensions } from "helpers";
import NoDataContainer from "../../../Common/NoDataContainer";

const radius = "6px";

const Item = styled.div`
  border: 1px solid lightgray;
  border-radius: ${radius};
  margin: 1% 0;
  width: 24%;
  min-width: 210px;

  @media (max-width: ${dimensions.lg}) {
    width: 48%;
  }

  @media (max-width: ${dimensions.sm}) {
    width: 100%;
  }
`;

const CardSquareImage = styled.img`
  display: block;
  width: 100%;
  height: auto;
  border-top-left-radius: ${radius};
  border-top-right-radius: ${radius};
`;

const Footer = styled.div`
  width: 100%;
  min-height: 60px;
  background: rgb(253, 253, 253);
  border-bottom-left-radius: ${radius};
  border-bottom-right-radius: ${radius};
  padding: 10px;
`;

const BlueTag = ({ box }) => <Tag color="blue">{box.class}</Tag>;

class BoundingBoxListContainer extends Component {
  componentDidMount() {
    this.props.fetchBoundingBoxResults();
  }

  handleDelete(e) {
    console.log(e);
  }

  render() {
    let { data } = this.props;
    return (
      <NoDataContainer hasData={data.length > 0}>
        <Row type="flex" justify="space-between" align="top">
          {data.map((record) => {
            return (
              <Item key={record.id}>
                <CardSquareImage
                  src={`${window.location.origin}/` + record.img}
                />
                <Footer>
                  <Row type="flex" gutter={16} style={{ margin: 0 }}>
                    {record.boxes.map((box) => (
                      <BlueTag key={box.id} box={box} />
                    ))}
                  </Row>
                  <Row
                    style={{ margin: "15px 0" }}
                    type="flex"
                    justify="space-between"
                    align="middle"
                  >
                    <Col span={20}>
                      <Row type="flex" align="middle">
                        <Avatar
                          src={record.userable.photo}
                          style={{ marginRight: "8px" }}
                        />

                        <div>
                          <div>{record.userable.user.name}</div>
                          <div>
                            {moment(record.created_at).format(
                              "Do MMM YYYY, HH:mm:ss"
                            )}
                          </div>
                        </div>
                      </Row>
                    </Col>

                    <Popconfirm
                      title="Do you want to delete?"
                      okText="Yes"
                      cancelText="No"
                      onConfirm={() =>
                        this.props.deleteBoundingBoxResult(record.id)
                      }
                    >
                      <DeleteOutlined
                        style={{
                          fontSize: "24px",
                          cursor: "pointer",
                          color: "black",
                        }}
                      />
                    </Popconfirm>
                  </Row>
                </Footer>
              </Item>
            );
          })}
        </Row>
      </NoDataContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  data: state.boundingBox.results,
});

const mapDispatchToProps = (dispatch) => {
  return {
    fetchBoundingBoxResults: (page, filters) =>
      dispatch(fetchBoundingBoxResults(page, filters)),
    deleteBoundingBoxResult: (id) => dispatch(deleteBoundingBoxResult(id)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BoundingBoxListContainer);
