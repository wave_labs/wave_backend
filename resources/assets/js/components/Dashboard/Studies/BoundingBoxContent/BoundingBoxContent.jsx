import React, { Component } from "react";
import BoundingBoxFilters from "./BoundingBoxFilters";
import BoundingBoxListContainer from "./BoundingBoxListContainer";

class BoundingBoxContent extends Component {
  render() {
    return (
      <div>
        <BoundingBoxFilters />
        <BoundingBoxListContainer />
      </div>
    );
  }
}

export default BoundingBoxContent;
