import React, { Component } from "react";
import { connect } from "react-redux";
import { Row } from "antd";
import styled from "styled-components";
import moment from "moment";
import {
  fetchMarkerCoordinates,
  fetchMarkers,
  exportMarkers,
} from "redux-modules/marker/actions";
import FilterRow from "../../Common/FilterRow";

const StyledRow = styled(Row)`
  margin: 1rem;
`;

class MarkerFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {
        creature: undefined,
        activity: undefined,
        date: undefined,
      },
    };

    this.initialState = this.state;
  }

  handleFilterChange = (filter, value) => {
    var { filters } = this.state;
    filters[filter] = value;
    this.setState({ filters });
  };

  handleReset = () => {
    this.setState({
      filters: { creature: undefined, activity: undefined, date: undefined },
    });
  };

  handleDownload = () => {
    var filters = this.handleFilters();

    this.props.exportMarkers(filters);
  };

  handleFilters = () => {
    var { creature, activity, date } = this.state.filters;
    let newDate = date ? [...date] : [];

    return {
      creature: creature,
      activity: activity,
      date:
        newDate.length > 0
          ? [
              moment(newDate[0]).format("YYYY-MM-DD"),
              moment(newDate[1]).format("YYYY-MM-DD"),
            ]
          : newDate,
    };
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state.filters) {
      var filters = this.handleFilters();

      this.props.fetchMarkerCoordinates(filters);
      this.props.fetchMarkers(1, filters);
    }
  }

  render() {
    return (
      <StyledRow>
        <FilterRow
          filters={["activity", "creature", "date"]}
          filterValues={this.state.filters}
          operations={["reset", "download"]}
          handleFilterChange={this.handleFilterChange}
          handleDownload={() => this.handleDownload()}
          handleReset={() => this.handleReset()}
        />
      </StyledRow>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMarkerCoordinates: (filters) =>
      dispatch(fetchMarkerCoordinates(filters)),
    fetchMarkers: (page, filters) => dispatch(fetchMarkers(page, filters)),
    exportMarkers: (filters) => dispatch(exportMarkers(filters)),
  };
};

export default connect(
  null,
  mapDispatchToProps
)(MarkerFilters);
