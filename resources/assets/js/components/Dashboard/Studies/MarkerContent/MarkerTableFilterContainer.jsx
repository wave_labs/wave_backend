import React, { Component } from "react";
import {
  fetchMarkers,
  deleteMarker,
  setCurrentMarker,
} from "redux-modules/marker/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import { startCreating } from "redux-modules/editCreateModal/actions";
import { NoDataMessage } from "helpers";


class MarkerTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activities: [],
    };
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "Creature",
        dataIndex: "creature.name",
      },
      {
        title: "Activities",
        dataIndex: "activities",
        render: (activities) => (
          <span>
            {activities.length > 0
              ? activities.map((activity, index) => (
                  <span key={index}>
                    {activity.activity}
                    {index != activities.length - 1 && ", "}
                  </span>
                ))
              : NoDataMessage()}
          </span>
        ),
      },
      {
        title: "",
        key: "",
        render: (text, record) => (
          <RowOperation
            deleteRow
            updateRow
            onDeleteConfirm={() => this.props.deleteMarker(record.id)}
          />
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.props.isAdmin && this.props.fetchMarkers();
  };

  onSearch = (search) => {
    this.filters = { ...this.filters, search };
    this.props.fetchMarkers(1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchMarkers(pagination.current, this.filters);
  };

  render() {
    return (
      <div>
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMarkers: (page, filters) => dispatch(fetchMarkers(page, filters)),
    deleteMarker: (id) => dispatch(deleteMarker(id)),
    startCreating: () => dispatch(startCreating()),
    setCurrentMarker: (data) => dispatch(setCurrentMarker(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.marker.data,
    meta: state.marker.meta,
    loading: state.marker.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MarkerTableFilterContainer);
