import React from "react";
import styled from "styled-components";

const StyledImg = styled.img`
  width: 100%;
`;

const StyledContainer = styled.div`
  max-width: 200px;
`;

const Activity = styled.span`
  text-transform: capitalize;
  display: inline-block;
`;

const StyledBold = styled(Activity)`
  font-weight: bold;
`;

const StyledDate = styled.div`
  font-size: 0.8em;
`;

const MarkerPopUp = ({ data }) => {
  return (
    <StyledContainer>
      <StyledImg
        src={`${window.location.origin}/api/image/creature/${data.creature.id}`}
      />

      <br />

      <Activity>
        <StyledBold>Activity:</StyledBold> {data.activity}
      </Activity>

      <StyledDate>{data.date}</StyledDate>
    </StyledContainer>
  );
};

export default MarkerPopUp;
