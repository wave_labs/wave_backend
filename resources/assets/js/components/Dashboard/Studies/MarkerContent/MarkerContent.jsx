import { Row, Col } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import MarkerFilters from "./MarkerFilters";
import MarkerMapList from "./MarkerMapList";
import MarkerTableFilterContainer from "./MarkerTableFilterContainer";

class MarkerContent extends Component {
  render() {
    return (
      <div>
        {this.props.isAdmin && <MarkerFilters />}

        <Row type="flex" gutter={24}>
          <Col xs={24} lg={12}>
            <MarkerMapList isAdmin={this.props.isAdmin} />
          </Col>
          <Col xs={24} lg={12}>
            <MarkerTableFilterContainer isAdmin={this.props.isAdmin} />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {};
};

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MarkerContent);
