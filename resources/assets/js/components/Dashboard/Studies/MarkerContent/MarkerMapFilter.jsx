import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col } from "antd";
import styled from "styled-components";
import { fetchMarkerCoordinates } from "redux-modules/marker/actions";
import CreatureRemoteSelectContainer from "../../Data/CreatureContent/CreatureRemoteSelectContainer";
import ActivityRemoteSelectContainer from "../../ActivityContent/ActivityRemoteSelectContainer";

const StyledRow = styled(Row)`
  margin: 1rem;
`;

class MarkerMapFilter extends Component {
  state = {
    filters: {
      creature: undefined,
      activity: undefined,
    },
  };

  handleCreatureChange = (value) => {
    this.setState({ filters: { ...this.state.filters, creature: value } });
  };

  handleActivityChange = (value) => {
    this.setState({ filters: { ...this.state.filters, activity: value } });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state.filters) {
      this.props.fetchMarkerCoordinates({
        ...this.state.filters,
      });
    }
  }

  render() {
    var { creature, activity } = this.state.filters;
    return (
      <StyledRow type="flex" gutter={16}>
        <Col span={12}>
          <CreatureRemoteSelectContainer
            value={creature}
            onChange={this.handleCreatureChange}
          />
        </Col>
        <Col span={12}>
          <ActivityRemoteSelectContainer
            value={activity}
            onChange={this.handleActivityChange}
          />
        </Col>
      </StyledRow>
    );
  }
}

export default connect(
  null,
  { fetchMarkerCoordinates }
)(MarkerMapFilter);
