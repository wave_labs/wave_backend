import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import MapBoxMap from "../../Common/MapBoxMap";
import { Marker, Popup } from "react-map-gl";
import { fetchMarkerCoordinates } from "redux-modules/marker/actions";
import MarkerPopUp from "./MarkerPopUp";
import styled from "styled-components";
import { ICON } from "helpers";

const MarkerPin = styled.svg`
  cursor: pointer;
`;

// PureComponent ensures that the markers are only rerendered when data changes
class Markers extends PureComponent {
  render() {
    return this.props.children;
  }
}

class MarkerMapList extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.state = {
      popUp: null,
    };
  }

  componentDidMount() {
    this.props.fetchMarkerCoordinates(this.filters);
  }

  showLitter = () => {
    const { popUp } = this.state;

    return (
      popUp && (
        <Popup
          longitude={parseFloat(popUp.longitude)}
          latitude={parseFloat(popUp.latitude)}
          onClose={() => this.setState({ popUp: null })}
        >
          <MarkerPopUp data={popUp} />
        </Popup>
      )
    );
  };

  handleMarkerClick = (record, activities, coordinates) => {
    this.props.isAdmin &&
      this.setState({
        popUp: {
          creature: record.creature,
          date: record.date,
          activity: activities.activity,
          id: coordinates.id,
          longitude: coordinates.longitude,
          latitude: coordinates.latitude,
        },
      });
  };

  render() {
    var { data } = this.props;

    return (
      <div className="map-container">
        <MapBoxMap zoom={9}>
          <Markers>
            {data.map((record) =>
              record.activities.map((activities) =>
                activities.coordinates.map((coordinates) => (
                  <Marker
                    key={coordinates.id}
                    longitude={parseFloat(coordinates.longitude)}
                    latitude={parseFloat(coordinates.latitude)}
                  >
                    <MarkerPin
                      onClick={() =>
                        this.handleMarkerClick(record, activities, coordinates)
                      }
                      height="25"
                      viewBox="0 0 24 24"
                      style={{
                        fill: activities.color,
                        stroke: "none",
                      }}
                    >
                      <path d={ICON} />
                    </MarkerPin>
                  </Marker>
                ))
              )
            )}
          </Markers>

          {this.showLitter()}
        </MapBoxMap>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMarkerCoordinates: (filters) =>
      dispatch(fetchMarkerCoordinates(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.marker.coordinates,
    loading: state.marker.loadingCoordinates,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MarkerMapList);
