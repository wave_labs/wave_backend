import { Row, Col } from "antd";
import React from "react";
import { connect } from "react-redux";
import TroutFilters from "./TroutFilters";
import TroutMap from "./TroutMap";
import TroutTableContainer from "./TroutTableContainer";

function TroutContent({ isAdmin }) {
    return (
        <div>
            <TroutFilters />

            <Row type="flex" gutter={24}>
                <Col xs={24} lg={12}>
                    <TroutMap isAdmin={isAdmin} />
                </Col>
                <Col xs={24} lg={12}>
                    <TroutTableContainer isAdmin={isAdmin} />
                </Col>
            </Row>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        isAdmin: state.auth.isAdmin,
    };
};

export default connect(
    mapStateToProps,
    null
)(TroutContent);
