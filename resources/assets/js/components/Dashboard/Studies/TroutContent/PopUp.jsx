import React from "react";
import styled from "styled-components";

const Container = styled.div`
  width: 200px;
  padding: 10px;

  img {
    width: 100%;
    margin: 5px auto;
  }

  p {
    margin: 0;
  }
`;

const Info = styled.p`
  font-size: 1em;
`;

const Date = styled.p`
  font-size: 0.8em;
`;

const PopUp = ({ data }) => {
    return (
        <Container>
            <img
                src={`/storage/images/studies/trout/${data.specie}.jpg`}
            />
            <Info>

            </Info>
            <Date>{data.date}</Date>
        </Container>
    );
};

export default PopUp;
