import React, { Component } from "react";
import {
    fetchDiademas,
    deleteDiadema,
} from "redux-modules/diadema/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import { startCreating } from "redux-modules/editCreateModal/actions";
import { NoDataMessage } from "helpers";
import styled from "styled-components";

const Pin = styled.p`
  margin: 0;

  span {
    text-transform: lowercase;
  }
`;

const getSpecies = {
    "Yes, the A": "A",
    "Yes, the B": "B",
    "Yes, A and B": "A and B",
}

class DiademaTableContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activities: [],
        };
        this.filters = {};
        this.columns = [
            {
                title: "ID",
                dataIndex: "id",
                sorter: true,
            },
            {
                title: "Date",
                dataIndex: "created_at",
            },
            {
                title: "Percentage of dead sightings",
                dataIndex: "dead",
            },
            {
                title: "Percentage of sick sightings",
                dataIndex: "sick",
            },
            {
                title: "",
                key: "",
                render: (text, record) => (
                    <RowOperation
                        deleteRow
                        updateRow
                        onDeleteConfirm={() => this.props.deleteDiadema(record.id)}
                    />
                ),
            },
        ];
    }

    componentDidMount = () => {
        this.props.fetchDiademas();
    };

    onSearch = (search) => {
        this.filters = { ...this.filters, search };
        this.props.fetchDiademas(1, this.filters);
    };

    handleTableChange = (pagination, filters, sorter) => {
        this.filters = {
            ...this.filters,
            ...filters,
            order: [sorter.field, sorter.order],
        };
        this.props.fetchDiademas(pagination.current, this.filters);
    };

    render() {
        return (
            <div>
                <TablePagination
                    loading={this.props.loading}
                    columns={this.columns}
                    handleTableChange={this.handleTableChange}
                    data={this.props.data}
                    meta={this.props.meta}
                />
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchDiademas: (page, filters) =>
            dispatch(fetchDiademas(page, filters)),
        deleteDiadema: (id) => dispatch(deleteDiadema(id)),
        startCreating: () => dispatch(startCreating()),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.diadema.data,
        meta: state.diadema.meta,
        loading: state.diadema.loading,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DiademaTableContainer);
