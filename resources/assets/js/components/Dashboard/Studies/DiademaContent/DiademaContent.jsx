import { Row, Col } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import DiademaFilters from "./DiademaFilters";
import DiademaMap from "./DiademaMap";
import DiademaTableContainer from "./DiademaTableContainer";

class DiademaContent extends Component {
    render() {
        return (
            <div>
                <DiademaFilters />

                <Row type="flex" gutter={24}>
                    <Col xs={24} lg={12}>
                        <DiademaMap isAdmin={this.props.isAdmin} />
                    </Col>
                    <Col xs={24} lg={12}>
                        <DiademaTableContainer isAdmin={this.props.isAdmin} />
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {};
};

const mapStateToProps = (state) => {
    return {
        isAdmin: state.auth.isAdmin,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DiademaContent);
