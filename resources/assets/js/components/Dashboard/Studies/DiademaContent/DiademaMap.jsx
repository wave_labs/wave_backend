import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";
import MapBoxMap from "../../Common/MapBoxMap";
import { Marker } from "react-map-gl";
import { fetchDiademaCoordinates } from "redux-modules/diadema/actions";
import styled from "styled-components";

const Pin = styled.img`
  cursor: pointer;
  width: 18px;
  opacity: 0.7;
`;


class Markers extends PureComponent {
    render() {
        return this.props.children;
    }
}

class DiademaMap extends Component {
    constructor(props) {
        super(props);
        this.filters = {};

    }

    componentDidMount() {
        this.props.fetchDiademaCoordinates(this.filters);
    }

    render() {
        var { data } = this.props;

        return (
            <div className="map-container">
                <MapBoxMap zoom={9}>
                    <Markers>
                        {data.length && data.map((record) => (

                            <Marker
                                key={record.id}
                                longitude={parseFloat(record.longitude)}
                                latitude={parseFloat(record.latitude)}
                            >
                                <Pin
                                    src={"/images/studies/diadema/icon.svg"}
                                >
                                </Pin>
                            </Marker>

                        ))}
                    </Markers>
                </MapBoxMap>
            </div >
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchDiademaCoordinates: (filters) =>
            dispatch(fetchDiademaCoordinates(filters)),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.diadema.coordinates,
        loading: state.diadema.loadingCoordinates,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DiademaMap);
