import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Select } from "antd";
import styled from "styled-components";
import moment from "moment";
import {
    fetchDiademaCoordinates,
    fetchDiademas,
    exportDiadema,
} from "redux-modules/diadema/actions";
import FilterRow from "../../Common/FilterRow";

const { Option } = Select;

const StyledRow = styled(Row)`
  margin: 1rem;
`;

class DiademaFilters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filters: {
                date: undefined,
            },
        };

        this.initialState = this.state;
    }

    handleFilterChange = (filter, value) => {
        var { filters } = this.state;
        filters[filter] = value;
        this.setState({ filters });
    };

    handleReset = () => {
        this.setState({
            filters: { date: undefined },
        });
    };

    handleDownload = () => {
        var filters = this.handleFilters();

        this.props.exportDiadema(filters);
    };

    handleFilters = () => {
        var { date } = this.state.filters;
        let newDate = date ? [...date] : [];

        return {

            date:
                newDate.length > 0
                    ? [
                        moment(newDate[0]).format("YYYY-MM-DD"),
                        moment(newDate[1]).format("YYYY-MM-DD"),
                    ]
                    : newDate,
        };
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState !== this.state.filters) {
            var filters = this.handleFilters();

            this.props.fetchDiademaCoordinates(filters);
            this.props.fetchDiademas(1, filters);
        }
    }

    render() {
        let { filters } = this.state;
        return (
            <StyledRow>
                <FilterRow
                    filters={["date"]}
                    filterValues={this.state.filters}
                    operations={["reset", "download"]}
                    handleFilterChange={this.handleFilterChange}
                    handleDownload={() => this.handleDownload()}
                    handleReset={() => this.handleReset()}

                />
            </StyledRow>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchDiademaCoordinates: (filters) =>
            dispatch(fetchDiademaCoordinates(filters)),
        fetchDiademas: (page, filters) =>
            dispatch(fetchDiademas(page, filters)),
        exportDiadema: (filters) => dispatch(exportDiadema(filters)),
    };
};

export default connect(
    null,
    mapDispatchToProps
)(DiademaFilters);
