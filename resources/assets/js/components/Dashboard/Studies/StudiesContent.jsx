import React from "react";
import TopicListContainer from "../../Common/TopicListContainer";

const StudiesContent = ({ history }) => {
    const content = [
        {
            to: "studies/markers",
            image: "/storage/images/logo/markers.svg",
            name: "Markers",
            description:
                "Evaluating presence, abundance and distribution of biodiversity in the aquatic coast land of Arquipélago da Madeira.",
        },
        {
            to: "studies/stopwatch",
            image: "/storage/images/logo/stopwatch.svg",
            name: "Stopwatch",
            description:
                "Web-based stopwatch of marine occurrences in various videos, providing comparison metrics with AI generated detections.",
        },
        {
            to: "studies/boundingBox",
            image: "/storage/images/logo/bounding-box.svg",
            name: "Bounding Box",
            description:
                "Imagery labeling of marine taxa with various classifications, providing comparison metrics with AI generated detections.",
        },
        {
            to: "studies/freshwaterPin",
            image: "/storage/images/logo/freshwater-pin.svg",
            name: "Freshwater Pin",
            description:
                "Mapping species of the freshwater ecosystems at Madeira and Porto Santo islands, evaluating distributions across timeframes.",
        },
        {
            to: "studies/diadema",
            image: "/storage/images/logo/diadema.svg",
            name: "Diadema",
            description:
                "Assessment on the distribution of the longthorned urchins (Diadema africanum) mortality in the coastal waters of the Madeira.",
        },
        {
            to: "studies/trout",
            image: "/storage/images/logo/trout.svg",
            name: "Trout",
            description:
                "There's an ongoing research project at MARE-Madeira focused on freshwater animal species present in Madeira's streams.",
        },
        {
            to: "studies/climarest",
            image: "/storage/images/logo/climarest.png",
            name: "Climarest stakeholders",
            description:
                "On going research about stakeholders for the Climarest project.",
        },
        {
            to: "studies/softCoral",
            image: "/storage/images/logo/coral.svg",
            name: "Soft Corals",
            description:
                "Within the scope of scientific activities and research, MARE-Madeira is assessing this soft coral's presence and distribution in the Madeira Archipelago's coastal waters.",
        },
    ];
    return (
        <div>
            <TopicListContainer history={history} topic="Studies" content={content} />
        </div>
    );
};

export default StudiesContent;
