import { Row, Col } from "antd";
import React from "react";
import { connect } from "react-redux";
import ClimarestFilters from "./ClimarestFilters";
import ClimarestTableContainer from "./ClimarestTableContainer";

function ClimarestContent({ isAdmin }) {
    return (
        <div>
            <ClimarestFilters />

            <Row type="flex" gutter={24}>
                <Col xs={24} lg={24}>
                    <ClimarestTableContainer />
                </Col>
            </Row>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        isAdmin: state.auth.isAdmin,
    };
};

export default connect(
    mapStateToProps,
    null
)(ClimarestContent);
