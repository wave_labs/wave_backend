import React, { Component } from "react";
import styled from "styled-components";
import { Col, Row } from "antd";
import MapBoxMap from "../../Common/MapBoxMap";
import MapPin from "../../Common/MapPin";
import { Marker } from "react-map-gl";

import { updateDrawerDimensions } from "helpers";

import DrawerContainer from "../../Common/DrawerContainer";

const StyledDiv = styled.div`
  font-size: 14;
  line-height: 22px;
  margin-bottom: 10px;
  color: rgba(0, 0, 0, 0.65);
`;

const StyledRow = styled(Row)`
  flex-direction: column;
`;

const MapContainer = styled.div`
  width: 100%;
  margin-bottom: 30px;
  height: 400px;
`;

const StyledParagraph = styled.span`
  margin-right: 8px;
  display: inline-block;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.85);
  &:after {
    content: ":";
  }
`;

const DescriptionItem = ({ span, title, content }) => (
  <Col span={span}>
    <StyledDiv>
      <StyledParagraph>{title}</StyledParagraph>
      {content}
    </StyledDiv>
  </Col>
);

class ClimarestDrawer extends Component {
  state = {
    drawerWidth: "400px",
  };

  updateDimensions() {
    this.setState({ drawerWidth: updateDrawerDimensions(window) });
  }

  componentDidMount = () => {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  render() {
    let { content } = this.props;
    let { drawerWidth } = this.state;

    return (
      <DrawerContainer
        handleDrawerClose={this.props.handleDrawerClose}
        visible={this.props.visible}
        width={drawerWidth}
      >
        {content && (
          <StyledRow type="flex">
            
            <Row type="flex" align="top">
              {console.log(content)}
              <DescriptionItem
                span={24}
                title="Which restoration area is this?"
                content={content.typeArea}
              />
              <DescriptionItem
                span={24}
                title="Is the location inside a protected area?"
                content={content.protectedArea}
              />
              <DescriptionItem
                span={24}
                title="Describe the restoration area’s size (km2)"
                content={content.restorationArea}
              />
              <DescriptionItem
                span={24}
                title="What is the distance from shore – closest point, furthest point (km)"
                content={content.distanceShore}
              />
              <DescriptionItem
                span={24}
                title="Who has tenure / ownership / management responsibility of the marine space?"
                content={content.ownership}
              />
              <DescriptionItem
                span={24}
                title="Who has tenure / ownership of the adjacent land?"
                content={content.adjacentOwnership}
              />
              <DescriptionItem
                span={24}
                title="Who currently has marine resource access rights (such as permits, quotas, etc.) within the area of intervention?"
                content={content.marineRights}
              />
              <DescriptionItem
                span={24}
                title="Will the intervention result in restrictions or changes to access and resource use?"
                content={content.resultRestrictions}
              />
              <DescriptionItem
                span={24}
                title="What other activities (e.g. recreational) are currently conducted within the area of intervention?"
                content={content.areaActivities}
              />
              <DescriptionItem
                span={24}
                title="Will the intervention result in restrictions or changes to these activities?"
                content={content.activitiesRestrictions}
              />
              <DescriptionItem
                span={24}
                title="Describe the aspects of the intervention that involve technological solutions."
                content={content.technologicSolutions}
              />
              <DescriptionItem
                span={24}
                title="Describe the aspects of the intervention that involve nature-based solutions."
                content={content.natureSolutions}
              />
              <DescriptionItem
                span={24}
                title="How will the restoration decrease climate vulnerability?"
                content={content.climateVulnerability}
              />
              <DescriptionItem
                span={24}
                title="Have there been any conflicts over resource use, conservation, or restoration at this site before?"
                content={content.conflicts}
              />
              <DescriptionItem
                span={24}
                title="Are any of the methods that you plan to use controversial? In what way/why? If so, which stakeholders have been involved?"
                content={content.contreversialMethods}
              />
              <DescriptionItem
                span={24}
                title="Name and institution"
                content={content.name}
              />
            </Row>
          </StyledRow>
        )}
      </DrawerContainer>
    );
  }
}

export default ClimarestDrawer;
