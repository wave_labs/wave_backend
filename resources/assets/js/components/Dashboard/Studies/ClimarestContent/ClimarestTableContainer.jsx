import React, { Component } from "react";
import { fetchClimarestAreas } from "redux-modules/climarest/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import { NoDataMessage } from "helpers";
import styled from "styled-components";

import ClimarestDrawer from "./ClimarestDrawer";

class ClimarestTableContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      drawerContent: null,
    };

    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "Date",
        dataIndex: "created_at",
        sorter: true,
      },
      {
        title: "Location",
        dataIndex: "typeArea",
      },
      {
        title: "Vulnerability",
        dataIndex: "climateVulnerability",
      },
      {
        title: "Ownership",
        dataIndex: "ownership",
      },
    ];
  }

  componentDidMount = () => {
    this.props.fetchClimarestAreas();
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchClimarestAreas(pagination.current, this.filters);
  };

  handleRowClick = (record) => {
    this.setState({
      drawerContent: record,
    });
  };

  handleDrawerClose = () => {
    this.setState({
      drawerContent: null,
    });
  };

  render() {
    let { drawerContent } = this.state;
    return (
      <div>
        <ClimarestDrawer
          handleDrawerClose={this.handleDrawerClose}
          visible={drawerContent && true}
          content={drawerContent}
        />
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
          onRow={(record) => ({
            onClick: () => {
              this.handleRowClick(record);
            },
          })}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchClimarestAreas: (page, filters) =>
      dispatch(fetchClimarestAreas(page, filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.climarest.data,
    meta: state.climarest.meta,
    loading: state.climarest.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClimarestTableContainer);
