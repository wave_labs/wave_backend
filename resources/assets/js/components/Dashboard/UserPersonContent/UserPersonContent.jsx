import React from 'react';
import UserPersonModalContainer from './UserPersonModalContainer'
import UserPersonTableFilterContainer from './UserPersonTableFilterContainer'

const UserPersonContent = () => {
	return (
			<div>
				<UserPersonModalContainer />
				<UserPersonTableFilterContainer />
			</div>
	)
}

export default UserPersonContent;