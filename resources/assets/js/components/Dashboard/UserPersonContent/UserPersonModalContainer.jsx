import React, { Component } from "react";
import {
  createUserPerson,
  updateUserPerson,
  resetUserPersonEdit,
} from "redux-modules/userPerson/actions";
import {
  resetModal,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../Common/EditCreateModal";
import UserPersonForm from "./UserPersonForm";
import moment from "moment";

class UserPersonModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, userPerson) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        const formatedDate = userPerson.b_day
          ? moment(userPerson.b_day).format("YYYY-MM-DD")
          : null;

        this.props
          .updateUserPerson(this.props.editUserPerson.id, {
            ...userPerson,
            b_day: formatedDate,
          })
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.resetUserPersonEdit();
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, userPerson) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });

        this.props
          .createUserPerson(userPerson)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((e) => this.setState({ confirmLoading: false }));
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit User Person"
        titleCreate="Create User Person"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <UserPersonForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          userPerson={this.props.editUserPerson}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetUserPersonEdit: () => dispatch(resetUserPersonEdit()),
    resetModal: () => dispatch(resetModal()),
    createUserPerson: (data) => dispatch(createUserPerson(data)),
    updateUserPerson: (data, id) => dispatch(updateUserPerson(data, id)),
  };
};

const mapStateToProps = (state) => {
  return {
    editUserPerson: state.userPerson.editUserPerson,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPersonModalContainer);
