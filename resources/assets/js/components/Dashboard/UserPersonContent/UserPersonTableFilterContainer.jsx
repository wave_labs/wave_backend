import React, { Component } from "react";
import {
  fetchUserPerson,
  deleteUserPerson,
  setUserPersonEdit,
  updateUserPerson,
} from "redux-modules/userPerson/actions";
import { connect } from "react-redux";
import RowOperation from "../Common/RowOperation";
import TablePagination from "../Common/TablePagination";
import TableFilterRow from "../Common/TableFilterRow";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { NoDataMessage } from "helpers";

const genderConverter = {
  m: "Male",
  f: "Female",
};

class UserPersonTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "User",
        dataIndex: "name",
      },
      {
        title: "Age",
        dataIndex: "age",
        render: (age, row) => row.b_day && `${age}`,
      },
      {
        title: "Gender",
        dataIndex: "gender",
        render: (gender) => genderConverter[gender],
      },
      {
        title: "Birthday",
        dataIndex: "b_day",
      },
      {
        title: "Country",
        dataIndex: "country",
      },
      {
        title: "Number Dives",
        dataIndex: "number_dives",
        render: (number_dives) =>
          number_dives ? `${number_dives}` : NoDataMessage(),
      },
      {
        title: "",
        key: "",
        render: (text, userPerson) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(userPerson)}
            onDeleteConfirm={() => this.props.deleteUserPerson(userPerson.id)}
          />
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.props.fetchUserPerson();
  };

  onUpdateClick = (userPerson) => {
    this.props.setUserPersonEdit(userPerson);
    this.props.startEditing();
  };

  onSearch = (search) => {
    this.filters = { ...this.filters, search };
    this.props.fetchUserPerson(1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchUserPerson(pagination.current, this.filters);
  };
  render() {
    return (
      <div>
        <TableFilterRow
          onSearch={(search) => this.onSearch(search)}
          searchPlaceholder="Search User Person"
        />
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUserPerson: (page, filters) =>
      dispatch(fetchUserPerson(page, filters)),
    deleteUserPerson: (id) => dispatch(deleteUserPerson(id)),
    activateUserPerson: (id) => dispatch(activateUserPerson(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setUserPersonEdit: (data) => dispatch(setUserPersonEdit(data)),
    updateUserPerson: (id, data) => dispatch(updateUserPerson(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.userPerson.data,
    meta: state.userPerson.meta,
    loading: state.userPerson.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPersonTableFilterContainer);
