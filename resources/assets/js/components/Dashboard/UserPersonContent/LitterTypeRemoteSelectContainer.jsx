import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchUserPerson } from "redux-modules/userPerson/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class UserPersonRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = search => {
    this.props.fetchUserPerson(1, { search });
  };

  render() {
    const { data, loading, value, onChange, mode, children } = this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="User Person"
        mode={mode}
      >
        {children}
        {data.map(d => (
          <Option value={d.id} key={d.id}>
            {d.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchUserPerson: (page, filters) => dispatch(fetchUserPerson(page, filters))
  };
};

const mapStateToProps = state => {
  return {
    data: state.userPerson.data,
    loading: state.userPerson.loading
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPersonRemoteSelectContainer);
