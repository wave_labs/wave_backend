import React, { Component } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Icon,
  InputNumber,
  Upload,
  Input,
  DatePicker,
  Select
} from "antd";
import { Link } from "react-router-dom";
import CountrySelect from "../../Common/CountrySelect";
import moment from "moment";

const FormItem = Form.Item;
const TextArea = Input.TextArea;
const Option = Select.Option;

const StyledInputNumber = styled(InputNumber)`
  width: 250px !important;
`;

class UserPersonForm extends Component {
  state = {
    confirmDirty: false
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;

    if (value && value !== form.getFieldValue("password")) {
      callback("The passwords don't match!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;

    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  rules = {
    birthday: [
      {
        required: false,
        type: "object",
        message: "Please select your birthday!"
      }
    ],
    name: [
      {
        required: false,
        message: "Please input your name!"
      }
    ],
    country: [
      {
        required: false,
        message: "Please input your country!"
      }
    ],
    gender: [
      {
        required: false,
        message: "Please input your gender!"
      }
    ]
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={16}>
          <Col lg={24}>
            <FormItem hasFeedback label="Name">
              {getFieldDecorator("name", {
                initialValue: this.props.userPerson.name,
                rules: this.rules.name
              })(<Input placeholder="Name" />)}
            </FormItem>
          </Col>
          <Col lg={24} xl={12}>
            <FormItem hasFeedback label="Birthday">
              {getFieldDecorator("b_day", {
                initialValue: this.props.userPerson.b_day ? moment(this.props.userPerson.b_day) : null,
                rules: this.rules.birthday
              })(<DatePicker style={{ width: "100%" }} />)}
            </FormItem>
          </Col>
          <Col lg={24} xl={12}>
            <FormItem hasFeedback label="Gender">
              {getFieldDecorator("gender", {
                initialValue: this.props.userPerson.gender,
                rules: this.rules.gender
              })(
                <Select placeholder="Select your gender">
                  <Option value="m">Male</Option>
                  <Option value="f">Female</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col lg={24} xl={12}>
            <FormItem hasFeedback label="Country">
              {getFieldDecorator("country", {
                initialValue: this.props.userPerson.country,
                rules: this.rules.country
              })(<CountrySelect />)}
            </FormItem>
          </Col>
          <Col lg={24} xl={12}>
            <FormItem hasFeedback label="Number of dives">
              {getFieldDecorator("number_dives", {
                rules: this.rules.number_dives,
                initialValue: this.props.userPerson.number_dives
              })(
                <Select placeholder="Select your range of dives">
                  <Option value="0-10">0-10</Option>
                  <Option value="11-20">11-20</Option>
                  <Option value="21-30">21-30</Option>
                  <Option value="30+">30+</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col lg={24}>
            <FormItem label="Certificates">
              <div className="dropbox">
                {getFieldDecorator("certificates", {
                  valuePropName: "certificates",
                  getValueFromEvent: this.normFile,
                  initialValue: this.props.userPerson.certificates
                })(
                  <Upload.Dragger name="files" action="/upload.do">
                    <p className="ant-upload-drag-icon">
                      <Icon type="inbox" />
                    </p>
                    <p>Click or drag file to this area to upload</p>
                  </Upload.Dragger>
                )}
              </div>
            </FormItem>
          </Col>
          <FormItem>
            {getFieldDecorator("userable_type", {
              initialValue: "UserPerson"
            })(<Input disabled type="hidden" />)}
          </FormItem>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(UserPersonForm);
