import React from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/source/actions";
import SelectSearch from "../Common/SelectSearch";
import { sourceConverter } from "helpers";

const Option = Select.Option;

class SourceRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const { SourceSelector, loading, onChange, value, children } = this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Source"
      >
        {children}
        {SourceSelector.map((source) => (
          <Option value={source.id} key={source.id}>
            {sourceConverter[source.name]}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    SourceSelector: state.source.SourceSelector,
    loading: state.source.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SourceRemoteSelectContainer);
