import React from "react";
import { connect } from "react-redux";
import debounce from "lodash/debounce";
import {
  fetchSelector,
  createUserOccupation,
} from "redux-modules/userOccupation/actions";
import { Select, Divider, Input, Row } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import styled from "styled-components";

const Option = Select.Option;

let index = 0;

const StyledOptionContent = styled.span`
  text-transform: capitalize;
`;

const StyledRow = styled(Row)`
  padding: 8px;
`;

class UserOccupationRemoteSelectContainer extends React.Component {
  lock = null;
  constructor(props) {
    super(props);

    this.onSearch = debounce(this.onSearch, 800);
  }

  state = {
    name: "",
    open: false,
  };

  lockClose = (e) => {
    clearTimeout(this.lock);
    this.lock = setTimeout(() => {
      this.lock = null;
    }, 100);
  };

  setRef = (select) => {
    this.select = select;
  };

  onDropdownVisibleChange = (open) => {
    if (this.lock) {
      this.select.focus();
      return;
    }
    this.setState({ open });
  };

  onNameChange = (event) => {
    this.setState({
      name: event.target.value,
    });
  };

  addItem = async () => {
    const { name } = this.state;

    await this.props.createUserOccupation({ name });
  };

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      UserOccupationSelector,
      loading,
      value,
      onChange,
      mode,
      children,
    } = this.props;
    const { name, open } = this.state;
    return (
      <Select
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        onDropdownVisibleChange={this.onDropdownVisibleChange}
        loading={loading}
        placeholder="Select you occupation"
        mode={mode}
        dropdownRender={(menu) => (
          <div>
            {menu}
            <Divider style={{ margin: "4px 0" }} />
            <StyledRow
              onMouseDown={this.lockClose}
              onMouseUp={this.lockClose}
              type="flex"
              justify="space-around"
              align="middle"
            >
              <Input
                style={{ width: "90%" }}
                value={name}
                onChange={this.onNameChange}
              />
              <PlusOutlined onClick={this.addItem} />
            </StyledRow>
          </div>
        )}
      >
        {children}
        {UserOccupationSelector.map((item) => (
          <Option key={item.id} value={item.id}>
            <StyledOptionContent>{item.name}</StyledOptionContent>
          </Option>
        ))}
      </Select>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
    createUserOccupation: (data) => dispatch(createUserOccupation(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    UserOccupationSelector: state.userOccupation.UserOccupationSelector,
    loading: state.userOccupation.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserOccupationRemoteSelectContainer);
