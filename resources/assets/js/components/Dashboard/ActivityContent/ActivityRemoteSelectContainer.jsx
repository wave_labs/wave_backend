import React from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import { fetchSelector } from "redux-modules/activity/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";
import styled from "styled-components";

const StyledOption = styled.div`
  text-transform: capitalize;
`;

class ActivityRemoteSelectContainer extends React.Component {
  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      data,
      loading,
      value,
      onChange,
      mode,
      placeholder = "Activity",
    } = this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
        mode={mode}
      >
        {data.map((record) => (
          <Select.Option value={record.id} key={record.id}>
            <StyledOption>{record.name}</StyledOption>
          </Select.Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.activity.selector,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivityRemoteSelectContainer);
