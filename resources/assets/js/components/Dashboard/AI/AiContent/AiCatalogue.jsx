import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchAiDetections } from "redux-modules/trackerCatalogue/actions";
import { fetchAiDetectionImages } from "redux-modules/ai/detectionImage/actions";
import styled from "styled-components";
import Lightbox from "react-image-lightbox";
import Carousel from "react-multi-carousel";
import AiFilters from "./AiFilters";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { getCarouselBreakpoints } from "helpers";
import NoDataContainer from "../../../Common/NoDataContainer";

const imagePath = `${window.location.origin}/python/images/`;

const StepContainer = styled.div`
  margin: 50px 0;
`;

const ImageContainer = styled.div`
  width: 11%;
  aspect-ratio: 1 / 1;
  display: inline-block;
  padding: 5px;
  position: relative;
`;

const Image = styled.div`
  margin: auto;
  display: block;
  opacity: 0.8;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: ${(props) => (props.percent * 70) / 100 + 30 + "%"};
  height: ${(props) => (props.percent * 70) / 100 + 30 + "%"};
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  background: url(${(props) => props.background});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  cursor: pointer;
  transition: 0.2s ease-in-out;

  &:hover {
    opacity: 1;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.5);
    width: 100%;
    height: 100%;
  }
`;

const Container = styled.div`
  margin: 50px 0;
  .react-multi-carousel-list {
    padding: 20px 0;
  }
`;

const Directory = styled.div`
  padding: 20px;

  &:hover {
    .folder-text-container {
      opacity: 1;
    }
  }

  span {
    font-weight: bold;
  }
  p {
    margin: 2px 0;
  }
`;

const FolderImage = styled.div`
  width: 180px;
  height: 180px;
  cursor: pointer;
  background: url(${(props) => props.background});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  cursor: pointer;
  position: relative;
`;

const FolderTextContainer = styled.div`
  position: absolute;
  bottom: 40px;
  right: 0;
  width: 150px;
  padding: 8px 5px;
  transition: 0.3s ease-in;
  background: rgba(0, 0, 0, 0.4);
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
  color: white;
  opacity: 0;
`;

const FolderText = styled.div`
  width: 60px;
  display: inline-block;
`;

const FolderTextLength = styled(FolderText)`
  font-size: 40px;
  text-align: right;
  margin-right: 5px;
`;

const BackButton = styled(ArrowLeftOutlined)`
  font-size: 30px;
  cursor: pointer;
  z-index: 99;
  margin-bottom: 20px;
`;

class AiCatalogue extends Component {
  state = {
    isOpen: false,
    currentImage: 0,
    paginatedData: [],
    currentStep: 0,
    lightbox: {
      current: 0,
      next: 0,
      prev: 0,
    },
  };

  componentDidMount() {
    this.props.fetchAiDetections(1, {
      category: this.props.aiCategory,
      self: true,
    });
  }

  getAverageAccuracy = (classifications) => {
    let count = 0;
    let total = 0;

    classifications.map((c) => {
      count++;
      total += parseInt(c.accuracy);
    });
    return total / count;
  };

  next = (detection) => {
    this.props
      .fetchAiDetectionImages(1, {
        detection: detection,
        category: this.props.aiCategory,
      })
      .then(() => {
        let { paginatedData } = this.state;
        const iterations = Math.ceil(this.props.data.length / 18);

        for (let index = 1; index <= iterations; index++) {
          this.props.data.map((image, i) => {
            if (Math.ceil((i + 1) / 18) == index) {
              !(index - 1 in paginatedData) && (paginatedData[index - 1] = []);
              paginatedData[index - 1].push(image);
            }
          });
        }

        this.setState({ paginatedData });
      });
    const currentStep = this.state.currentStep + 1;
    this.setState({ currentStep });
  };

  prev = () => {
    const currentStep = this.state.currentStep - 1;
    this.setState({ currentStep, paginatedData: [] });
  };

  handleImageClick = (index) => {
    let { data } = this.props;
    this.setState({
      lightbox: {
        current: index,
        next: (index + 1) % data.length,
        prev: (index + data.length - 1) % data.length,
      },
      isOpen: true,
    });
  };

  render() {
    let { detections, data } = this.props;
    const { isOpen, lightbox, currentStep, paginatedData } = this.state;
    const { current, prev, next } = lightbox;
    const DirectoryContainer = ({ model, date, length, detection }) => (
      <Directory onClick={() => this.next(detection)}>
        <FolderImage background="/images/ai/folder.png">
          <FolderTextContainer className="folder-text-container">
            <FolderTextLength>{length}</FolderTextLength>
            <FolderText>view items</FolderText>
          </FolderTextContainer>
        </FolderImage>
        <p>
          <span>Model: </span> {model}
        </p>
        <p>
          <span>Date: </span> {date}
        </p>
      </Directory>
    );

    const steps = [
      <div>
        <AiFilters
          models={this.props.models}
          aiCategory={this.props.aiCategory}
        />
        <NoDataContainer hasData={detections.length > 0}>
          <Carousel responsive={getCarouselBreakpoints([1, 2, 3, 4, 6])}>
            {detections.map((detection, index) => (
              <DirectoryContainer
                detection={detection.id}
                key={index}
                model={detection.model.name}
                date={detection.created_at}
                length={detection.images.length}
              />
            ))}
          </Carousel>
        </NoDataContainer>
      </div>,
      <Container>
        <BackButton onClick={this.prev} />
        <NoDataContainer hasData={data.length > 0}>
          <Carousel responsive={getCarouselBreakpoints([1, 1, 1, 1, 1])}>
            {paginatedData.map(
              (page, index) =>
                page.length > 0 && (
                  <div key={index}>
                    {page.map((image, i) => (
                      <ImageContainer key={i}>
                        <Image
                          onClick={() => this.handleImageClick(i + index * 18)}
                          background={imagePath + "/" + image.image}
                          percent={this.getAverageAccuracy(
                            image.classifications
                          )}
                        />
                      </ImageContainer>
                    ))}
                  </div>
                )
            )}
          </Carousel>
        </NoDataContainer>
      </Container>,
    ];

    return (
      <div>
        {isOpen && (
          <Lightbox
            mainSrc={imagePath + data[current].image}
            nextSrc={imagePath + data[next].image}
            prevSrc={imagePath + data[prev].image}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() => this.handleImageClick(prev)}
            onMoveNextRequest={() => this.handleImageClick(next)}
            imageCaption={
              <div>
                {data[current].classifications.map((c) => (
                  <p>
                    {c.accuracy != 0
                      ? `Classified as ${c.label.name} with ${c.accuracy}%`
                      : "No classification on this image"}
                  </p>
                ))}
              </div>
            }
          />
        )}

        <StepContainer>{steps[currentStep]}</StepContainer>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAiDetectionImages: (page, filters) =>
      dispatch(fetchAiDetectionImages(page, filters)),
    fetchAiDetections: (page, filters) =>
      dispatch(fetchAiDetections(page, filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    meta: state.aiDetectionImage.meta,
    data: state.aiDetectionImage.data,
    loading: state.aiDetectionImage.loading,
    detections: state.aiDetection.data,
    loadingD: state.aiDetection.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AiCatalogue);
