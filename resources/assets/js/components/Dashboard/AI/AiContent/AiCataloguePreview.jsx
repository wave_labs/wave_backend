import React, { Component } from "react";
import { Modal, Button, Row, Upload, Col, Spin } from "antd";
import styled from "styled-components";
import { connect } from "react-redux";
import { PlusOutlined } from "@ant-design/icons";
import {
  deleteAiDetection,
  createAiDetection,
} from "redux-modules/trackerCatalogue/actions";
import { getBase64 } from "helpers";
import {
  LoadingOutlined,
  UploadOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import { alertActions } from "redux-modules/alert/actions";
import { getErrorMessages, handleArrayToFormData } from "helpers";

const StyledHint = styled.p`
  @media screen and (max-width: 768px) {
    display: none;
  }
`;

const StyledUploadText = styled.p`
  @media screen and (max-width: 768px) {
    font-size: 0.9em !important;
  }
`;

const SectionContainer = styled(Row)`
  margin: 5% auto;
`;

const ImageControlRow = styled(Row)`
  margin-top: 2%;
  button {
    margin: 0px 5px;
  }
`;
const ImageContainer = styled.div`
  position: relative;
`;
const StyledSpin = styled(Spin)`
  position: absolute !important;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
const StyledImage = styled.img`
  width: auto;
  max-height: 300px;
  margin: auto;
  max-width: 1200px;
  box-shadow: 0 10px 0 0 rgba(black, 0.3);
  filter: ${(props) => props.filter};
`;

const StyledImageTwo = styled.img`
  width: 90%;
  margin: auto;
  display: block;
`;

const StyledMaintenanceImage = styled.img`
  width: 60%;
  margin: auto;
  display: block;
`;

const StyledParagraph = styled.p`
  text-align: center;
`;

const StyledParagraphTitle = styled(StyledParagraph)`
  font-weight: bold;
  font-size: 3em;
`;

const FilterContainer = styled.div`
  width: 20%;

  min-width: 60px;
  text-align: center;
  margin: 20px auto;
  margin-bottom: 50px;
`;
const ImageSubtitle = styled.div`
  font-size: 1.1vw;
  color: #777777;
  font-weight: bold;

  @media (max-width: 1000px) {
    display: none;
  }
`;

const SelectAiImage = styled.img`
  width: 100%;
  max-width: 150px;
  margin: auto;
  height: auto;
  opacity: ${(props) => (props.active ? 1 : 0.5)};
  cursor: pointer;
`;

class AiCataloguePreview extends Component {
  state = {
    file: null,
    loading: false,
    visible: false,
    hasResponse: false,
    detection: false,
    uploadDisplay: "block",
    imageDisplay: "none",
    imageUrl: null,
    filter: "blur(2px) brightness(80%)",
    model: null,
  };

  handleReset = () => {
    this.setState({
      detection: false,
      uploadDisplay: "block",
      imageDisplay: "none",
      visible: false,
      filter: "blur(2px) brightness(80%)",
    });
  };

  handleOk = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 100);
  };

  handleFilterChange = (e) => {
    this.setState({
      model: e.target.id,
    });
  };

  handleUpload = (options) => {
    const { file } = options;

    if (!this.state.model) {
      this.props.errorAlert({
        message: "Identification failed",
        description: "Please select a model before uploading a file",
      });
    } else if (this.state.model == "AcousticNet") {
      this.setState({ visible: true, imageUrl: "/images/ai/AcousticNet.jpg" });
    } else {
      getBase64(file, (imageUrl) =>
        this.setState({
          hasResponse: false,
          imageUrl,
          imageDisplay: "flex",
          uploadDisplay: "none",
          filter: "blur(2px) brightness(80%)",
        })
      );

      const formData = new FormData();
      handleArrayToFormData(formData, [file], "directory");
      formData.append("model_id", this.state.model);

      this.props.createAiDetection(formData).then(
        (response) => {
          this.setState({
            imageUrl:
              "/python/images/" + response.value.data.data.images[0].image,
            hasResponse: true,
            filter: "blur(0px)",
          });

          this.props.deleteAiDetection(response.value.data.data.id);
        },
        (error) => {
          this.setState({
            hasResponse: true,
            filter: "blur(0px)",
          });
          this.props.errorAlert({
            message: "Identification failed",
            description: getErrorMessages(error.response.data.errors),
          });
        }
      );
    }
  };

  render() {
    const {
      visible,
      hasResponse,
      uploadDisplay,
      imageDisplay,
      imageUrl,
      filter,
      model,
    } = this.state;

    return (
      <div>
        <SectionContainer type="flex" justify="space-around" align="middle">
          <FilterContainer>
            <SelectAiImage
              active={model && model == 4 && true}
              id={4}
              onClick={this.handleFilterChange}
              src={`${window.location.origin}/storage/images/logo/biodiversitynet.svg`}
            />
            <ImageSubtitle> BiodiversityNet </ImageSubtitle>
          </FilterContainer>
          <FilterContainer>
            <SelectAiImage
              active={model && model == 14 && true}
              id={14}
              onClick={this.handleFilterChange}
              src={`${window.location.origin}/storage/images/logo/litternet.svg`}
            />
            <ImageSubtitle> LitterNet </ImageSubtitle>
          </FilterContainer>
          <FilterContainer>
            <SelectAiImage
              active={model && model == "AcousticNet" && true}
              id="AcousticNet"
              onClick={this.handleFilterChange}
              src={`${window.location.origin}/storage/images/logo/acousticnet.svg`}
            />
            <ImageSubtitle> AcousticNet </ImageSubtitle>
          </FilterContainer>
        </SectionContainer>

        <ImageContainer style={{ display: imageDisplay }}>
          <StyledImage filter={filter} src={imageUrl} alt="specie" />
          <StyledSpin
            spinning={!hasResponse}
            indicator={
              <LoadingOutlined style={{ fontSize: 100, color: "white" }} />
            }
          />
        </ImageContainer>

        {hasResponse && (
          <ImageControlRow
            type="flex"
            justify="end"
            align="middle"
            gutter={16}
            style={{ display: imageDisplay }}
          >
            <Button size="large" onClick={this.handleReset}>
              <ReloadOutlined />
              Reset
            </Button>
            <Upload
              accept=".jpg,.png,.jpeg"
              name="predictPic"
              showUploadList={false}
              customRequest={this.handleUpload}
            >
              <Button size="large" type="primary">
                <UploadOutlined />
                New file
              </Button>
            </Upload>
          </ImageControlRow>
        )}

        <div style={{ display: uploadDisplay }}>
          <Upload.Dragger
            accept=".jpg,.png,.jpeg"
            name="predictPic"
            showUploadList={false}
            customRequest={this.handleUpload}
          >
            <p className="ant-upload-drag-icon">
              <PlusOutlined />
            </p>
            <StyledUploadText className="ant-upload-text">
              "Click or drag a file to this area to upload"
            </StyledUploadText>
            <StyledHint className="ant-upload-hint">
              Supports png, jpeg and jpg files
            </StyledHint>
          </Upload.Dragger>{" "}
        </div>

        <Modal
          visible={visible}
          onOk={this.handleOk}
          title="AI Wave"
          onCancel={this.handleReset}
          footer={[
            <Button key="back" onClick={this.handleReset}>
              Close
            </Button>,
            null,
          ]}
          width={"900px"}
        >
          <Row type="flex" align="middle" justify="space-between">
            <Col span={12}>
              <StyledMaintenanceImage
                src="/images/ai/maintenance.png"
                alt="maintenance"
              />
              <StyledParagraphTitle>Wave AI</StyledParagraphTitle>
              <StyledParagraph>Head's up! AI in progress...</StyledParagraph>
              <StyledParagraph>
                While we are collecting image dataset for our model training,
                capable in discriminating acoustic imagery.
              </StyledParagraph>
              <StyledParagraph>
                The image on the right is a preview of what our machine learning
                identifies.
              </StyledParagraph>
              <StyledParagraph>
                Our acoustic machine learning pipeline will be online soon!
              </StyledParagraph>
            </Col>
            <Col span={12}>
              <StyledImageTwo src={imageUrl} alt="image" />
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createAiDetection: (data) => dispatch(createAiDetection(data)),
    deleteAiDetection: (id) => dispatch(deleteAiDetection(id)),
    errorAlert: (message) => dispatch(alertActions.error(message)),
  };
};

export default connect(null, mapDispatchToProps)(AiCataloguePreview);
