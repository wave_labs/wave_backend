import { Col, Row, message } from "antd";
import React, { Component } from "react";
import styled from "styled-components";
import AiCatalogue from "./AiCatalogue";
import AIGraphContainer from "./AIGraphContainer";
import EditCreateModal from "../../Common/EditCreateModal";
import AiDetectionForm from "./AiDetectionForm";
import { connect } from "react-redux";
import { fetchAiCategory } from "redux-modules/ai/category/actions";
import { alertActions } from "redux-modules/alert/actions";
import { createAiDetection } from "redux-modules/trackerCatalogue/actions";
import { handleArrayToFormData, getErrorMessages } from "helpers";
import { resetModal } from "redux-modules/editCreateModal/actions";

message.config({
  maxCount: 1,
});

const StyledImage = styled.img`
  width: 60%;
  min-width: 150px;
  display: block;
  margin: auto;
`;

const Title = styled.h1`
  font-weight: bold;
  font-size: 1.6em;
`;

const InformationContainer = styled(Col)`
  p {
    margin-bottom: 15px;
  }

  h3 {
    font-size: 1.4em;
  }
`;

const Model = styled(Col)`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

class AiContent extends Component {
  state = {
    directory: [],
    uploadedFiles: false,
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  componentDidMount() {
    this.props.fetchAiCategory(this.props.match.params.aiCategory);
  }

  handleImageUpload = (file) => {
    let { directory } = this.state;
    directory.push(file);
    this.setState({ directory });
  };

  setUploadedFiles = () => {
    this.setState({ uploadedFiles: true });
  };

  handleCreate = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, data) => {
      if (err) {
        return;
      } else {
        let formData = new FormData();
        handleArrayToFormData(formData, this.state.directory, "directory");
        formData.append("model_id", data.model_id);
        message.loading("Action in progress..", 0);
        this.props
          .createAiDetection(formData)
          .then((response) => {
            message.success(
              "Prediction has finished, please confirm results of the output.",
              5
            );
            this.handleClose();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = getErrorMessages(error.response.data.errors);

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  handleClose = () => {
    const form = this.formRef.props.form;
    this.setState({
      directory: [],
      uploadedFiles: false,
    });
    this.props.resetModal();
    form.resetFields();
  };

  render() {
    let { aiCategory, loading } = this.props;

    return (
      <div>
        {!loading && (
          <Row type="flex" align="middle" justify="space-between">
            <Col sm={24} md={8}>
              <StyledImage src={"/storage/images/logo/" + aiCategory.image} />
            </Col>
            <InformationContainer sm={24} md={16}>
              <Title>{aiCategory.name}</Title>
              <p>{aiCategory.description}</p>
              <h3>Models</h3>
              <Row type="flex" justify="space-between">
                {aiCategory.models.map((element, index) => (
                  <Model key={index} xs={24} md={12} lg={8}>
                    <span>{element.name}</span>
                  </Model>
                ))}
              </Row>
            </InformationContainer>
          </Row>
        )}

        <EditCreateModal
          titleCreate="Upload Catalogue"
          onOkCreateClick={() => this.handleCreate()}
          onCancel={() => this.handleClose()}
          creating={this.props.creating}
        >
          <AiDetectionForm
            wrappedComponentRef={this.saveFormRef}
            models={aiCategory.models}
            handleImageUpload={this.handleImageUpload}
            setUploadedFiles={this.setUploadedFiles}
            directory={this.state.directory}
            uploadedFiles={this.state.uploadedFiles}
          />
        </EditCreateModal>

        <AiCatalogue
          labels={aiCategory.labels}
          models={aiCategory.models}
          aiCategory={this.props.match.params.aiCategory}
        />

        <AIGraphContainer />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAiCategory: (id) => dispatch(fetchAiCategory(id)),
    createAiDetection: (data) => dispatch(createAiDetection(data)),
    errorAlert: (message) => dispatch(alertActions.error(message)),
    resetModal: () => dispatch(resetModal()),
  };
};

const mapStateToProps = (state) => {
  return {
    aiCategory: state.aiCategory.current,
    loading: state.aiCategory.loading,
    creating: state.editCreateModal.creating,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AiContent);
