import { Col, Row } from "antd";
import React, { Component } from "react";
import { Bar, HorizontalBar } from "react-chartjs-2";

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

class AiGraph extends Component {
  state = {
    timeMonths: [],
    timeFrequency: [],
    specieName: [],
    specieValue: [],
  };

  getOptions = (yLabel, xLabel) => {
    return {
      maintainAspectRatio: true,
      legend: {
        display: false,
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              stepSize: 10,
            },
            scaleLabel: {
              display: true,
              labelString: yLabel,
              fontStyle: "bold",
              fontSize: "16",
            },
          },
        ],
        xAxes: [
          {
            ticks: {
              maxRotation: 0,
              minRotation: 0,
            },
            scaleLabel: {
              display: true,
              labelString: xLabel,
              labelAngle: 45,
              fontStyle: "bold",
              fontSize: "16",
            },
          },
        ],
      },
    };
  };

  getData = (label, data) => {
    return {
      labels: label,
      datasets: [
        {
          backgroundColor: "rgba(24,144,255,0.2)",
          borderColor: "rgba(24,144,255,1)",
          borderWidth: 1,
          hoverBackgroundColor: "rgba(24,144,255,0.4)",
          hoverBorderColor: "rgba(24,144,255,1)",
          data: data,
        },
      ],
    };
  };

  handleData = () => {
    let timeMonths = [],
      timeFrequency = [],
      specieName = [],
      specieValue = [];

    Object.entries(this.props.time).map((element) => {
      timeMonths.push(months[element[0] - 1]);
      timeFrequency.push(element[1]);
    });

    Object.entries(this.props.specie).map((element) => {
      specieName.push(element[0]);
      specieValue.push(element[1]);
    });

    this.setState({
      timeMonths: timeMonths,
      timeFrequency: timeFrequency,
      specieName: specieName,
      specieValue: specieValue,
    });
  };

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      this.handleData();
    }
  }

  componentDidMount() {
    this.handleData();
  }

  render() {
    const { timeMonths, timeFrequency, specieName, specieValue } = this.state;

    return (
      <Row>
        <Col md={24} lg={12}>
          <Bar
            data={this.getData(specieName, specieValue)}
            options={this.getOptions("Accuracy", "Species")}
          />
        </Col>
        <Col md={24} lg={12}>
          <HorizontalBar
            data={this.getData(timeMonths, timeFrequency)}
            options={this.getOptions("Time", "Frequency")}
          />
        </Col>
      </Row>
    );
  }
}

export default AiGraph;
