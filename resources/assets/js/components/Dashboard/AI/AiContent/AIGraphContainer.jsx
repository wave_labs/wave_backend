import React, { Component } from "react";
import AiGraph from "./AiGraph.jsx";
import { connect } from "react-redux";
import {
  specieHistory,
  timeHistory,
} from "redux-modules/trackerHistory/actions";

class AIGraphContainer extends Component {
  componentDidMount() {
    this.props.timeHistory();
    this.props.specieHistory();
  }

  render() {
    const { dataTime, dataSpecie, loadingTime, loadingSpecie } = this.props;

    return (
      <div>
        {!loadingTime && !loadingSpecie && (
          <AiGraph time={dataTime} specie={dataSpecie} loading={false} />
        )}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    specieHistory: () => dispatch(specieHistory()),
    timeHistory: () => dispatch(timeHistory()),
  };
};

const mapStateToProps = (state) => {
  return {
    dataTime: state.trackerHistory.dataTime,
    dataSpecie: state.trackerHistory.dataSpecie,
    loadingTime: state.trackerHistory.loadingTime,
    loadingSpecie: state.trackerHistory.loadingSpecie,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AIGraphContainer);
