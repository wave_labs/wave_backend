import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Select } from "antd";
import styled from "styled-components";
import moment from "moment";
import {
  fetchDives,
  fetchDiveCoords,
  exportDiveCsv,
} from "redux-modules/dive/actions";
import { startCreating } from "redux-modules/editCreateModal/actions";
import FilterRow from "../../Common/FilterRow";
import { fetchAiDetections } from "redux-modules/trackerCatalogue/actions";
const { Option } = Select;

const StyledRow = styled(Row)`
  margin: 1rem;
`;

class AiFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operations: ["reset", "create"],
      filters: {
        self: true,
        date: undefined,
        model: undefined,
      },
    };
  }

  handleFilterChange = (filter, value) => {
    var { filters } = this.state;
    filters[filter] = value;
    this.setState({ filters });
  };

  handleReset = () => {
    this.setState({
      filters: {
        date: undefined,
        self: true,
        model: undefined,
      },
    });
  };

  handleFilters = () => {
    var { date, model } = this.state.filters;
    let newDate = date ? [...date] : [];

    return {
      self: true,
      model: model,
      date:
        newDate.length > 0
          ? [
              moment(newDate[0]).format("YYYY-MM-DD"),
              moment(newDate[1]).format("YYYY-MM-DD"),
            ]
          : newDate,
    };
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state) {
      var filters = this.handleFilters();

      this.props.fetchAiDetections(1, filters);
    }
  }

  render() {
    return (
      <StyledRow>
        <FilterRow
          filters={["date"]}
          filterValues={this.state.filters}
          operations={this.state.operations}
          handleFilterChange={this.handleFilterChange}
          handleCreate={() => this.props.startCreating()}
          handleReset={() => this.handleReset()}
          additionalFilters={[
            <Select
              placeholder="Model"
              value={this.state.filters.model}
              style={{ width: "100%" }}
              onChange={(value) => this.handleFilterChange("model", value)}
            >
              {this.props.models.map((model, index) => (
                <Option key={index} value={model.id}>
                  {model.name}
                </Option>
              ))}
            </Select>,
          ]}
        />
      </StyledRow>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchDiveCoords: (filters) => dispatch(fetchDiveCoords(filters)),
    fetchDives: (page, filters) => dispatch(fetchDives(page, filters)),
    exportDiveCsv: (filters) => dispatch(exportDiveCsv(filters)),
    startCreating: () => dispatch(startCreating()),
    fetchAiDetections: (page, filters) =>
      dispatch(fetchAiDetections(page, filters)),
  };
};
const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AiFilters);
