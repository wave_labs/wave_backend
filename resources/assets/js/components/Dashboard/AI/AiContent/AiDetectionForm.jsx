import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Form, Upload, Radio } from "antd";
import { PlusOutlined, CheckOutlined } from "@ant-design/icons";

const FormItem = Form.Item;

const StyledHint = styled.p`
  @media screen and (max-width: 768px) {
    display: none;
  }
`;
const Info = styled.p`
  color: #777;
  font-size: 0.8em;
  text-align: center;
`;

const ModelOptions = styled(Radio)`
  width: 50%;
  margin: 5px 0 !important;
`;

const StyledUploadText = styled.div`
  @media screen and (max-width: 768px) {
    font-size: 0.9em !important;
  }
`;

class AiDetectionForm extends Component {
  rules = {
    model: [
      {
        required: true,
        message: "Model is required",
      },
    ],
    directory: [
      {
        required: true,
        message: "Directory with images is required",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { models, directory, uploadedFiles } = this.props;

    return (
      <Form hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col span={24}>
            <FormItem label="Model">
              {getFieldDecorator("model_id", {
                rules: this.rules.model,
              })(
                <Radio.Group style={{ width: "100%", marginBottom: 20 }}>
                  <Row type="flex" justify="space-between">
                    {models.map((model, index) => (
                      <ModelOptions key={index} value={model.id}>
                        {model.name}
                      </ModelOptions>
                    ))}
                  </Row>
                </Radio.Group>
              )}
            </FormItem>
          </Col>

          <Col span={24}>
            <FormItem>
              {getFieldDecorator("directory", {
                rules: this.rules.directory,
              })(
                <Upload.Dragger
                  directory
                  showUploadList={false}
                  beforeUpload={(file, fileList) => {
                    if (file.uid === fileList[0].uid) {
                      fileList = fileList.slice(0, 50);

                      fileList.map((newFile) => {
                        (newFile.type === "image/png" ||
                          newFile.type === "image/jpeg" ||
                          newFile.type === "image/jpg") &&
                          this.props.handleImageUpload(newFile);

                        if (newFile.uid === fileList[fileList.length - 1].uid) {
                          this.props.setUploadedFiles();
                        }
                      });
                    }

                    return false;
                  }}
                >
                  <p className="ant-upload-drag-icon">
                    {uploadedFiles ? <CheckOutlined /> : <PlusOutlined />}
                  </p>
                  <StyledUploadText className="ant-upload-text">
                    {uploadedFiles ? (
                      directory.map((file, index) => (
                        <p key={index}>
                          {index < 5
                            ? file.name
                            : index == 5 && <span>...</span>}
                        </p>
                      ))
                    ) : (
                      <span>
                        Click or drag a directory to this area to upload
                      </span>
                    )}
                  </StyledUploadText>
                  <StyledHint className="ant-upload-hint">
                    {!uploadedFiles && (
                      <span>Supports png, jpeg and jpg files</span>
                    )}
                  </StyledHint>
                </Upload.Dragger>
              )}
            </FormItem>
          </Col>
          <Info>
            Wave supports directories up to 50 files, if you have larger
            repositories please contact us so we can run the detections directly
            on the server.
          </Info>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(AiDetectionForm);
