import React from "react";
import TopicListContainer from "../../Common/TopicListContainer";
import { AiData } from "../../MainPage/Kits/kitsHelper";

const AiContent = ({ history }) => {
  return <TopicListContainer history={history} topic="AI" content={AiData} />;
};

export default AiContent;
