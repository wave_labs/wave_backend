import React, { Component } from "react";
import {
	createQuestion,
	updateQuestion,
	resetQuestionEdit
} from "redux-modules/question/actions";
import {
	resetModal,
	startEditing
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../Common/EditCreateModal";
import QuestionForm from "./QuestionForm";

class QuestionModalContainer extends Component {
	state = {
		confirmLoading: false //modal button loading icon
	};

	onCancel = () => {
		this.resetModalForm();
	};

	onOkEditClick = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, question) => {
			if (err) {
				return;
			} else {
				this.setState({ confirmLoading: true });
				this.props
					.updateQuestion(this.props.editQuestion.id, question)
					.then(data => {
						this.resetModalForm();
					})
					.catch(e => this.setState({ confirmLoading: false }));
			}
		});
	};

	resetModalForm = () => {
		const form = this.formRef.props.form;
		this.setState({ confirmLoading: false });
		this.props.resetQuestionEdit();
		this.props.resetModal();
		form.resetFields();
	};

	onOkCreateClick = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, question) => {
			if (err) {
				return;
			} else {
				this.setState({ confirmLoading: true });

				this.props
					.createQuestion(question)
					.then(data => {
						this.resetModalForm();
					})
					.catch(e => this.setState({ confirmLoading: false }));
			}
		});
	};


	saveFormRef = formRef => {
		this.formRef = formRef;
	};

	render() {
		return (
			<EditCreateModal
				titleEdit="Edit  Question"
				titleCreate="Create  Question"
				onOkEditClick={() => this.onOkEditClick()}
				onOkCreateClick={() => this.onOkCreateClick()}
				confirmLoading={this.state.confirmLoading}
				onCancel={() => this.onCancel()}
				editing={this.props.editing}
				creating={this.props.creating}
			>
				<QuestionForm
					creating={this.props.creating}
					wrappedComponentRef={this.saveFormRef}
					question={this.props.editQuestion}
				/>
			</EditCreateModal>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		resetQuestionEdit: () => dispatch(resetQuestionEdit()),
		resetModal: () => dispatch(resetModal()),
		createQuestion: data => dispatch(createQuestion(data)),
		updateQuestion: (data, id) => dispatch(updateQuestion(data, id))
	};
};

const mapStateToProps = state => {
	return {
		editQuestion: state.question.editQuestion,
		creating: state.editCreateModal.creating,
		editing: state.editCreateModal.editing
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(QuestionModalContainer);
