import React, { Component } from "react";
import {
  fetchQuestion,
  deleteQuestion,
  setQuestionEdit,
  updateQuestion,
} from "redux-modules/question/actions";
import { connect } from "react-redux";
import RowOperation from "../Common/RowOperation";
import TablePagination from "../Common/TablePagination";
import TableFilterRow from "../Common/TableFilterRow";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";

class QuestionTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "Scale",
        dataIndex: "scale",
      },
      {
        title: "Question",
        dataIndex: "question",
      },
      {
        title: "",
        key: "",
        render: (text, question) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(question)}
            onDeleteConfirm={() => this.props.deleteQuestion(question.id)}
          />
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.props.fetchQuestion(this.props.question);

    console.log(this.props);
  };

  onUpdateClick = (question) => {
    this.props.setQuestionEdit(question);
    this.props.startEditing();
  };

  onSearch = (search) => {
    this.filters = { ...this.filters, search };
    this.props.fetchQuestion(this.props.question, 1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchQuestion(
      this.props.question,
      pagination.current,
      this.filters
    );
  };

  render() {
    return (
      <div>
        <TableFilterRow
          onSearch={(search) => this.onSearch(search)}
          searchPlaceholder="Search  Question"
        />

        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchQuestion: (form, page, filters) =>
      dispatch(fetchQuestion(form, page, filters)),
    deleteQuestion: (id) => dispatch(deleteQuestion(id)),
    activateQuestion: (id) => dispatch(activateQuestion(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setQuestionEdit: (data) => dispatch(setQuestionEdit(data)),
    updateQuestion: (id, data) => dispatch(updateQuestion(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.question.data,
    meta: state.question.meta,
    loading: state.question.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestionTableFilterContainer);
