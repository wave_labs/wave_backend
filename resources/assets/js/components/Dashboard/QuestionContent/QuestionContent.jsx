import React, { Component } from "react";
import { connect } from "react-redux";
import QuestionModalContainer from "./QuestionModalContainer";
import QuestionTableFilterContainer from "./QuestionTableFilterContainer";

class QuestionContent extends Component {
  render() {
    const { form } = this.props.match.params;

    return (
      <div>
        <QuestionModalContainer />
        <QuestionTableFilterContainer question={form} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchQuestion: (form) => dispatch(fetchQuestion(form)),
  };
};

const mapStateToProps = (state) => {
  return {
    question: state.question.current,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestionContent);
