import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Form, Input, Slider } from "antd";

const FormItem = Form.Item;

const StyledCol = styled(Col)`
  display: flex !important;
  justify-content: center !important;
`;

class QuestionForm extends Component {
  state = {};

  rules = {
    question: [
      {
        required: true,
        message: "Question is required",
      },
    ],
    scale: [
      {
        required: true,
        message: "Please input a scale!",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { question } = this.props;
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row gutter={16}>
          <Col xs={24}>
            <FormItem hasQuestion label="Name">
              {getFieldDecorator("form", {
                initialValue: question.question,
                rules: this.rules.form,
              })(<Input placeholder="Name" />)}
            </FormItem>
          </Col>
          <Col xs={24}>
            <FormItem hasQuestion label="Scale">
              <StyledCol xs={2}>2</StyledCol>
              <Col xs={20}>
                {getFieldDecorator("scale", {
                  initialValue: question.scale,
                  rules: this.rules.scale,
                })(<Slider min={2} max={20} />)}
              </Col>
              <StyledCol xs={2}>20</StyledCol>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(QuestionForm);
