import React, { Component } from "react";
import { fetchSightingData } from "redux-modules/sightingData/actions";
import {
	startEditing
} from "redux-modules/editCreateModal/actions";
import { deleteSightingData, setCurrentSightingData } from "redux-modules/sighting/actions";
import { connect } from "react-redux";
import {
	Table,
	Row,
	Col,
	Button,
	Icon,
	Popconfirm,
	Checkbox,
	Input
} from "antd";
import TablePagination from "../Common/TablePagination";
import RowOperation from "../Common/RowOperation";
import TableFilterRow from "../Common/TableFilterRow";
import { Link } from "react-router-dom";
import styled from "styled-components";

const StyledTitle = styled.b`
	font-weight: bold;
`;

class SightingDataTableFilterContainer extends Component {
	expandedRowRender = record => {
		if (record.type == "Acoustic") {
			return (
				<div>
					<div>
						<StyledTitle> Duration: </StyledTitle>
						<span> {record.acoustic[0].duration_s} s</span>
					</div>
					<div>
						<StyledTitle> Acoustic Type: </StyledTitle>
						<span> {record.acoustic[0].acoustic_type}</span>
					</div>
				</div>
			);
		} else {
			return <span> No extra information </span>;
		}
	};

	constructor(props) {
		super(props);
		this.filters = {};

		this.columns = [
			{
				title: "ID",
				dataIndex: "id",
				sorter: true
			},
			{
				title: "Sighting ID",
				dataIndex: "sighting_id",
				sorter: true,
				render: id => (
					<Link to={`/dashboard/sightings/${id}`}>{id}</Link>
				)
			},
			{
				title: "Link",
				dataIndex: "link",
				render: link => (
					<a
						target="_blank"
						rel="noopener noreferrer"
						href={`${window.location.origin}/storage/${link}`}
					>
						Link
					</a>
				)
			},
			{
				title: "Underwater",
				dataIndex: "underwater",
				filters: [
					{ text: "Underwater", value: 1 },
					{ text: "Surface", value: 0 }
				],
				filterMultiple: false,
				render: underwater => (underwater ? "Underwater" : "Surface")
			},
			{
				title: "Type",
				dataIndex: "type",
				filters: [
					{ text: "Image", value: "Image" },
					{ text: "Acoustic", value: "Acoustic" },
					{ text: "Video", value: "Video" }
				]
			},
			{
				title: "",
				key: "",
				render: (text, sightingData) => (
					<RowOperation
						onUpdateClick={() => this.onUpdateClick(sightingData)}
						onDeleteConfirm={() =>
							this.props.deleteSightingData(sightingData.id)
						}
					/>
				)
			}
		];
	}

	componentDidMount = () => {
		this.props.fetchSightingData();
	};

	onUpdateClick = sightingData => {
		this.props.setCurrentSightingData(sightingData);
		this.props.startEditing();
	};

	handleTableChange = (pagination, filters, sorter) => {
		this.filters = {
			...this.filters,
			...filters,
			order: [sorter.field, sorter.order]
		};
		this.props.fetchSightingData(pagination.current, this.filters);
	};

	render() {
		return (
			<div>
				<TablePagination
					loading={this.props.loading}
					columns={this.columns}
					handleTableChange={this.handleTableChange}
					data={this.props.data}
					expandedRowRender={this.expandedRowRender}
					meta={this.props.meta}
				/>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		fetchSightingData: (page, filters) =>
			dispatch(fetchSightingData(page, filters)),
		deleteSightingData: id => dispatch(deleteSightingData(id)),
				startCreating: () => dispatch(startCreating()),
		startEditing: () => dispatch(startEditing()),
		setCurrentSightingData: (data) => dispatch(setCurrentSightingData(data))
	};
};

const mapStateToProps = state => {
	return {
		data: state.sightingData.data,
		meta: state.sightingData.meta,
		loading: state.sightingData.loading
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SightingDataTableFilterContainer);
