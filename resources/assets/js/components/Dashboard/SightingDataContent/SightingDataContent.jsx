import React from "react";
import SightingDataModalContainer from "../SightingPage/SightingDataModalContainer";
import SightingDataTableFilterContainer from "./SightingDataTableFilterContainer";

const SightingDataContent = () => {
	return (
		<div>
			<SightingDataModalContainer />
			<SightingDataTableFilterContainer />
		</div>
	);
};

export default SightingDataContent;
