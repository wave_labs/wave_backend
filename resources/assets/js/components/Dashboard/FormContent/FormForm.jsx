import React, { Component } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  Button,
} from "antd";
import { Link } from "react-router-dom";

const FormItem = Form.Item;
const TextArea = Input.TextArea;

const StyledIcon = styled(Icon)`
 padding: 8px;
`;

let id = 0;

class FormForm extends Component {

  remove = k => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }

    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  };

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys,
    });
  };

  rules = {
    form_name: [
      {
        required: true,
        message: "Form name is required"
      }
    ],
    description: [
      {
        required: true,
        message: "Please input a description!"
      }
    ],
    question: [
      {
        required: true,
        message: "Please input a question or delete this field!"
      }
    ],
  };

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { forms } = this.props;
    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');

    const formItems = keys.map((k, index) => (
      <FormItem
        key={k}
      >
        {getFieldDecorator(`questions[${k}]`, {
          validateTrigger: ['onChange', 'onBlur'],
          initialValue: forms.questions,
          rules: this.rules.question
        })(<Input placeholder="Question" style={{ width: '93%' }} />)}
        {keys.length > 1 ? (
          <StyledIcon
            className="dynamic-delete-button"
            type="minus-circle-o"
            onClick={() => this.remove(k)}
          />
        ) : null}
      </FormItem>
    ));

    return (
      <div>
        <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
          <Row>
              <FormItem hasForm label="Name">
                {getFieldDecorator("form_name", {
                  initialValue: forms.form_name,
                  rules: this.rules.form_name
                })(<Input placeholder="Name" />)}
              </FormItem>
          </Row>

          <Row>
            <FormItem hasForm label="Description">
              {getFieldDecorator("description", {
                initialValue: forms.description,
                rules: this.rules.description
              })(
                <TextArea
                  placeholder="Description"
                  autosize={{ minRows: 4, maxRows: 10 }}
                />
              )}
            </FormItem>
          </Row>

          <Row>
          <FormItem hasForm label="Questions">
          </FormItem>
            {formItems}
          </Row>

          <Row>
            <FormItem>
              <Button type="dashed" onClick={this.add} style={{ width: '100%' }}>
                <Icon type="plus" /> Add Question
              </Button>
            </FormItem>
          </Row>

        </Form>
      </div>
    );
  }
}

export default Form.create()(FormForm);
