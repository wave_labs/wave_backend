import React, { Component } from "react";
import {
  fetchForm,
  deleteForm,
  setFormEdit,
  updateForm,
} from "redux-modules/form/actions";
import { connect } from "react-redux";
import { Divider } from "antd";
import RowOperation from "../Common/RowOperation";
import TablePagination from "../Common/TablePagination";
import TableFilterRow from "../Common/TableFilterRow";
import DateRange from "./DateRange";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { Link } from "react-router-dom";

class FormTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {};

    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "Form",
        dataIndex: "form",
      },
      {
        title: "Description",
        dataIndex: "desc",
        width: 1100,
      },
      {
        title: "Actions",
        width: 200,
        render: (forms) => (
          <span>
            <Link to={`/dashboard/form/structure/${forms.form}`}>
              Structure
            </Link>

            <Divider type="vertical" />

            <Link to={`/dashboard/form/results/${forms.form}`}>Results</Link>
          </span>
        ),
      },
      {
        title: "",
        key: "",
        render: (text, forms) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(forms)}
            onDeleteConfirm={() => this.props.deleteForm(forms.id)}
          />
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.props.fetchForm();
  };

  onUpdateClick = (forms) => {
    this.props.setFormEdit(forms);
    this.props.startEditing();
  };

  onSearch = (search) => {
    this.filters = { ...this.filters, search };
    this.props.fetchForm(1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchForm(pagination.current, this.filters);
  };
  render() {
    return (
      <div>
        <TableFilterRow
          onSearch={(search) => this.onSearch(search)}
          searchPlaceholder="Search Form"
          onCreateClick={() => this.props.startCreating()}
        />

        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
        <DateRange />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchForm: (page, filters) => dispatch(fetchForm(page, filters)),
    deleteForm: (id) => dispatch(deleteForm(id)),
    activateForm: (id) => dispatch(activateForm(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setFormEdit: (data) => dispatch(setFormEdit(data)),
    updateForm: (id, data) => dispatch(updateForm(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.forms.data,
    meta: state.forms.meta,
    loading: state.forms.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FormTableFilterContainer);
