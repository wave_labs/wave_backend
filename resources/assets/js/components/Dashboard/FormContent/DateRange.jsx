import React, { Component } from "react";
import { Row, Col, Form, Button, DatePicker, message } from "antd";
import moment from "moment";
import axios from "axios";

const RangePicker = DatePicker.RangePicker;
const FormItem = Form.Item;

message.config({
  maxCount: 1,
});

class FormForm extends Component {
  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      // Should format date value before submit.
      const rangeValue = fieldsValue["range"];
      const values = {
        ...fieldsValue,
        range: [
          rangeValue[0].format("YYYY-MM-DD"),
          rangeValue[1].format("YYYY-MM-DD"),
        ],
      };

      message.loading("Action in progress..", 0);

      axios({
        url: `${window.location.origin}/api/export/${rangeValue[0].format(
          "YYYY-MM-DD"
        )}/${rangeValue[1].format("YYYY-MM-DD")}`,
        method: "GET",
        responseType: "blob", // important
      }).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute(
          "download",
          `${rangeValue[0].format("YYYY-MM-DD")}_${rangeValue[1].format(
            "YYYY-MM-DD"
          )}.csv`
        );
        document.body.appendChild(link);
        link.click();

        if (response) {
          message.success("Export finished", 3);
        } else {
          message.error("Export failed", 3);
        }
      });
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Form
          layout="inline"
          onSubmit={this.handleSubmit}
          hideRequiredMark={true}
        >
          <Row type="flex" gutter={16}>
            <Col style={{ marginLeft: "auto" }}>
              <FormItem>
                {getFieldDecorator("range", {})(
                  <RangePicker
                    ranges={{
                      Today: [moment(), moment()],
                      "This Week": [
                        moment().startOf("week"),
                        moment().endOf("week"),
                      ],
                      "This Month": [
                        moment().startOf("month"),
                        moment().endOf("month"),
                      ],
                      "This Year": [
                        moment().startOf("year"),
                        moment().endOf("year"),
                      ],
                    }}
                    format="YYYY/MM/DD"
                    onChange={this.onDateChange}
                  />
                )}
              </FormItem>

              <Button
                type="primary"
                shape="round"
                icon="download"
                htmlType="submit"
              >
                Export CSV
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

export default Form.create()(FormForm);
