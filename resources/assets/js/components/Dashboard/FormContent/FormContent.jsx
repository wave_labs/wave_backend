import React from 'react';
import FormModalContainer from './FormModalContainer'
import FormTableFilterContainer from './FormTableFilterContainer'

const FormContent = () => {
	return (
			<div>
				<FormModalContainer />
				<FormTableFilterContainer />
			</div>
	)
}

export default FormContent;