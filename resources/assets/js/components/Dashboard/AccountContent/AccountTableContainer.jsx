import React, { Component } from "react";
import { Table } from "antd";
import moment from "moment";

const columns = [
  {
    title: "Date",
    dataIndex: "date",
    render: (date) => moment(date).format("YYYY-MM-DD"),
    sorter: true,
  },
  {
    title: "Category",
    dataIndex: "category",
    key: "category",
    render: (category) => (
      <div>
        {Array.isArray(category)
          ? Object.values(category).map(function(c, index) {
              return <div key={index}>{c.name}</div>;
            })
          : category}
      </div>
    ),
  },
  {
    title: "Quantity",
    dataIndex: "quantity",
    key: "quantity",
    render: (quantity, row) => (
      <div>
        {Array.isArray(row.category)
          ? Object.values(row.category).map(function(c, index) {
              console.log(c);
              return <div key={index}>{c.quantity}</div>;
            })
          : quantity}
      </div>
    ),
  },

  {
    title: "Dive spot",
    dataIndex: "source",
    key: "source",
  },
];

class AccountTableContainer extends Component {
  render() {
    let { activity } = this.props;

    return (
      <div>
        {activity && (
          <Table
            columns={columns}
            dataSource={activity}
            rowKey={(record) => record.id}
            pagination={{
              total: activity.length,
              pageSize: 4,
              hideOnSinglePage: true,
            }}
          />
        )}
      </div>
    );
  }
}

export default AccountTableContainer;
