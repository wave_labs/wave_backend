import React, { useEffect, useState } from "react";
import { Row, Col } from "antd";
import { fetchSelector } from "redux-modules/divingSpot/actions";
import { connect } from "react-redux";
import DivingSpotMapList from "./DivingSpotMapList";

function DivingSpotContent(props) {
  const { data, account } = props;
  const [filters, setFilters] = useState({});

  useEffect(() => {
    props.fetchSelector(filters);
  }, []);

  return (
    <div>
      <Row type="flex" gutter={24}>
        <Col lg={24} xs={24}>
          <DivingSpotMapList data={data} user={account} />
        </Col>
      </Row>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.divingSpot.DivingSpotSelector,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DivingSpotContent);
