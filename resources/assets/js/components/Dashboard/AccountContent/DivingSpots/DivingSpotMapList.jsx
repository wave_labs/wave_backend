import React, { useState, useEffect } from "react";
import MapBoxMap from "../../Common/MapBoxMap";
import { Marker, Popup } from "react-map-gl";
import DivingSpotPopUp from "./DivingSpotPopUp";

function DivingSpotMapList(props) {
  const { data, user } = props;

  const [divingSpotPopUp, setDivingSpotPopUp] = useState(null);

  const showSpot = () => {
    return (
      divingSpotPopUp && (
        <Popup
          tipSize={5}
          longitude={parseFloat(divingSpotPopUp.longitude)}
          latitude={parseFloat(divingSpotPopUp.latitude)}
          closeOnClick={false}
          onClose={() => setDivingSpotPopUp(null)}
        >
          <DivingSpotPopUp  spot={divingSpotPopUp} />
        </Popup>
      )
    );
  };

  const getMarkerLink = () =>
    `${window.location.origin}/storage/images/map-markers/19.png`;

  const hasDivingSpot = (users) => {
    var hasUser = false;
    users.map((element) => {
      if (element.id == user.id) {
        hasUser = true;
      }
    });
    return hasUser;
  };


  return (
    <div className="map-container">
      <MapBoxMap zoom={7} fixedSize >
        {data.map((record) =>
          hasDivingSpot(record.users) ? (
            <Marker
              key={record.id}
              longitude={parseFloat(record.longitude)}
              latitude={parseFloat(record.latitude)}
            >
              <img
                src={getMarkerLink()}
                onClick={() => setDivingSpotPopUp(record)}
              />
            </Marker>
          ) : (
            <></>
          )
        )}
        {showSpot()}
      </MapBoxMap>
    </div>
  );
}

export default DivingSpotMapList;
