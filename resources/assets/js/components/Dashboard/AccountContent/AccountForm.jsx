import React, { Component } from "react";
import { Row, Col, Form, Input, DatePicker, Select, Button, Modal } from "antd";
import CountrySelect from "../../Common/CountrySelect";
import { connect } from "react-redux";
import moment from "moment";
import styled from "styled-components";
import UserOccupationRemoteSelectContainer from "../UserOccupationContent/UserOccupationRemoteSelectContainer";
import AccountModalContainer from "./AccountModalContainer";
import { EditOutlined } from "@ant-design/icons";

const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;

const StyledSection = styled.div`
  padding: 25px !important;
  flex: 1;
`;

const MembersContainer = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: 85%;
  margin: auto 5px;
  display: flex;
`;

const MemberContainer = styled.div`
  display: flex;
  align-items: center;
  margin-right: 3%;
  margin-top: 5px;
  min-width: 100px;
`;

const StyledSubtitle = styled.div`
  color: #777777;
`;

const StyledFormContainer = styled(Row)`
  display: flex;
  flex-direction: column;
`;

const StyledImage = styled.img`
  width: 28px;
  height: 28px;
  margin-right: 5px;
`;

class AccountForm extends Component {
  state = {
    visible: false,
    userPerson: null,
    members: [],
    modalVisibility: false,
    loading: true,
  };

  showModal = () => {
    this.setState({ modalVisibility: true });
  };

  handleCancel = () => {
    this.setState({ modalVisibility: false });
  };

  handleAddMember = (members) => {
    this.setState({ members: members });
    this.props.form.setFieldsValue({
      members: members,
    });

    this.handleCancel();
  };

  hide = () => {
    this.setState({
      visible: false,
    });
  };

  handleVisibleChange = (visible) => {
    this.setState({ visible });
  };

  componentDidMount() {
    const { user } = this.props;
    let members = [];
    let userAssociates,
      userPerson = null;

    if (user.userable.type_name == "App\\UserPerson") {
      userPerson = true;
      userAssociates = user.userable.user.companies;
    } else {
      userPerson = false;
      userAssociates = user.userable.user.persons;
    }

    userAssociates.map((el) => {
      members.push(el.email);
    });

    this.setState({
      userPerson: userPerson,
      members: members,
      loading: false,
    });
  }

  rules = {
    name: [
      {
        min: 2,
        message: "Name has a minimum of two characters",
      },
    ],
    phone: [
      {
        required: true,
        message: "Please input your phone number!",
      },
      {
        max: 16,
        message: "Phone number too long!",
      },
    ],
    email: [
      {
        type: "email",
        message: "This is not a valid email!",
      },
    ],
    role: [
      {
        required: true,
        message: "Please input your role!",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { user } = this.props;
    const { userPerson, members, loading } = this.state;

    console.log(loading);

    return (
      <StyledFormContainer>
        <StyledSection style={{ borderBottom: "1px solid rgba(5, 5, 5, 0.1)" }}>
          <StyledSubtitle>Account Overview</StyledSubtitle>
          <Form layout="inline" className="custom-form">
            <Row type="flex" justify="space-between" align="middle">
              <Col xs={24} lg={12}>
                <FormItem label="Email">
                  {getFieldDecorator("email", {
                    initialValue: user.email,
                    rules: this.rules.email,
                  })(<Input type="email" />)}
                </FormItem>
              </Col>
              <Col xs={24} lg={12}>
                <FormItem label="Name">
                  {getFieldDecorator("name", {
                    initialValue: user.userable.user.name,
                    rules: this.rules.name,
                  })(<Input />)}
                </FormItem>
              </Col>
            </Row>
          </Form>
        </StyledSection>
        <StyledSection style={{ borderBottom: "1px solid rgba(5, 5, 5, 0.1)" }}>
          <StyledSubtitle>
            {userPerson ? "Demographics" : "Additional Information"}
          </StyledSubtitle>

          <Form layout="inline" className="custom-form">
            <Row type="flex" justify="space-between" align="middle">
              {userPerson ? (
                <React.Fragment>
                  <Col xs={24} lg={12}>
                    <FormItem label="Gender">
                      {getFieldDecorator("gender", {
                        initialValue: user.userable.user.gender
                          ? user.userable.user.gender
                          : undefined,
                      })(
                        <Select placeholder="Select your gender">
                          <Option value="m">Male</Option>
                          <Option value="f">Female</Option>
                        </Select>
                      )}
                    </FormItem>
                  </Col>
                  <Col xs={24} lg={12}>
                    <FormItem label="Country">
                      {getFieldDecorator("country", {
                        initialValue: user.userable.user.country
                          ? user.userable.user.country
                          : undefined,
                      })(<CountrySelect />)}
                    </FormItem>
                  </Col>
                  <Col xs={24} lg={12}>
                    <FormItem label="Occupation">
                      {getFieldDecorator("user_occupation_id", {
                        initialValue: user.occupation
                          ? user.occupation.id
                          : undefined,
                      })(<UserOccupationRemoteSelectContainer />)}
                    </FormItem>
                  </Col>
                  <Col xs={24} lg={12}>
                    <FormItem label="Birthday">
                      {getFieldDecorator("b_day", {
                        initialValue:
                          user.userable.user.b_day &&
                          moment(user.userable.user.b_day, "YYYY/MM/DD"),
                      })(<DatePicker style={{ width: "100%" }} />)}
                    </FormItem>
                  </Col>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <Col xs={24} lg={8}>
                    <FormItem label="Address">
                      {getFieldDecorator("address", {
                        initialValue: user.userable.user.address,
                      })(
                        <Input
                          placeholder="Input your address"
                          style={{ width: "100%" }}
                        />
                      )}
                    </FormItem>
                  </Col>
                  <Col xs={24} lg={12}>
                    <FormItem label="About">
                      {getFieldDecorator("note", {
                        initialValue: user.note,
                      })(
                        <Input
                          placeholder="Write about yourself..."
                          style={{ width: "100%" }}
                        />
                      )}
                    </FormItem>
                  </Col>
                </React.Fragment>
              )}
            </Row>
          </Form>
        </StyledSection>
        <StyledSection style={{ borderBottom: "1px solid rgba(5, 5, 5, 0.1)" }}>
          <StyledSubtitle>{userPerson ? "About" : "Members"}</StyledSubtitle>
          <Row
            type="flex"
            justify="space-between"
            align="middle"
            className="custom-form"
          >
            <Col span={24}>
              <Form className="custom-form">
                {userPerson ? (
                  <FormItem>
                    {getFieldDecorator("note", {
                      initialValue: user.note,
                    })(
                      <TextArea
                        placeholder="Write about yourself..."
                        rows={1}
                        style={{ width: "100%" }}
                      />
                    )}
                  </FormItem>
                ) : (
                  <FormItem>
                    {getFieldDecorator("members", {
                      initialValue: members,
                    })(
                      <React.Fragment>
                        <Row type="flex" justify="space-between" align="middle">
                          {!loading && (
                            <AccountModalContainer
                              initialInputs={members}
                              visible={this.state.modalVisibility}
                              onCancel={this.handleCancel}
                              onCreate={this.handleAddMember}
                            />
                          )}
                        </Row>
                        <Row type="flex" justify="space-between" align="middle">
                          <MembersContainer>
                            {members.length > 0 ? (
                              members.map((el, index) => {
                                return (
                                  <MemberContainer key={index}>
                                    <StyledImage src="https://www.jf-amareleja.pt/wp-content/uploads/2017/10/default-user-300x284.png" />
                                    {el}
                                  </MemberContainer>
                                );
                              })
                            ) : (
                              <span>You don't have members associated yet</span>
                            )}
                          </MembersContainer>

                          <Button type="primary" onClick={this.showModal}>
                            <EditOutlined />
                          </Button>
                        </Row>
                      </React.Fragment>
                    )}
                  </FormItem>
                )}
              </Form>
            </Col>
          </Row>
        </StyledSection>
        <StyledSection>
          <StyledSubtitle>Settings</StyledSubtitle>
          <Row
            type="flex"
            justify="space-between"
            align="middle"
            className="custom-form"
          >
            <Col span={20}>
              <label className="custom-label" htmlFor="X-location">
                Language:
              </label>
              <Select
                id="X-location"
                placeholder="Language"
                style={{ minWidth: "100px" }}
                defaultValue="en"
              >
                <Option value="en">English</Option>
              </Select>
            </Col>
            <Col span={4}>
              <Button type="primary" onClick={this.props.handleSubmit}>
                Save Changes
              </Button>
            </Col>
          </Row>
        </StyledSection>
      </StyledFormContainer>
    );
  }
}

export default Form.create()(AccountForm);
