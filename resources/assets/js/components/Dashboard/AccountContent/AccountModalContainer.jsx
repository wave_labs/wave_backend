import React, { Component } from "react";
import { Modal, Input, Button, Row } from "antd";
import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import styled from "styled-components";
import { validateEmail } from "helpers";

const StyledDelete = styled(DeleteOutlined)`
  font-size: 26px;
  cursor: pointer;
`;

class AccountModalContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: [],
      input: null,
      initialState: [],
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.initialInputs !== this.props.initialInputs) {
      const { initialInputs } = this.props;

      this.setState({
        inputs: [...initialInputs],
      });
    }
  }

  componentDidMount() {
    const { initialInputs } = this.props;

    this.setState({
      inputs: [...initialInputs],
    });
  }

  handleDeleteInput = (index) => {
    let { inputs } = this.state;

    let newInput = inputs.filter((element) => element !== index);
    this.setState({ inputs: newInput });
  };

  handleNewInput = () => {
    let { inputs, input } = this.state;
    validateEmail(input)
      ? this.setState({ inputs: [...inputs, input], input: null })
      : this.setState({ input: null });
  };

  handleCancel = () => {
    let { initialInputs } = this.props;
    this.setState({ inputs: [...initialInputs] });
    this.props.onCancel();
  };

  render() {
    let { onCreate, visible, initialInputs } = this.props;
    let { inputs, input } = this.state;

    return (
      <Modal
        visible={visible}
        title="Edit company's members"
        onCancel={this.handleCancel}
        onOk={() => onCreate(inputs)}
        maskStyle={{ backgroundColor: "rgba(55,55,55,.2)" }}
      >
        <Row type="flex" justify="space-between" align="middle" gutter={8}>
          <Input
            placeholder="Insert new member's email address"
            onChange={(e) => this.setState({ input: e.target.value })}
            value={input}
            style={{ width: "85%" }}
          />
          <Button type="primary" onClick={this.handleNewInput}>
            <PlusOutlined />
          </Button>
        </Row>
        {inputs.map((element, index) => {
          return (
            <Row
              type="flex"
              justify="space-between"
              align="middle"
              key={index}
              placeholder="Insert new member's email address"
              style={{ margin: "26px auto" }}
            >
              {element}
              <StyledDelete onClick={() => this.handleDeleteInput(element)} />
            </Row>
          );
        })}
      </Modal>
    );
  }
}
export default AccountModalContainer;
