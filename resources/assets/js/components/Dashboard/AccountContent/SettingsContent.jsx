import React, { Component } from "react";
import { connect } from "react-redux";
import AcountOverview from "./AcountOverview";
import AccountTableContainer from "./AccountTableContainer";
import { Row, Divider, Col } from "antd";
import { fetchHistory, fetchMe } from "redux-modules/account/actions";
import styled from "styled-components";
import { Link } from "react-router-dom";
import DivingSpotContent from "../Data/DivingSpotContent/DivingSpotContent";
import DiveContent from "../Apps/DiveContent/DiveContent";

const StyledTitle = styled.div`
  font-size: 2rem;
`;

const StyledSubTitle = styled.div`
  font-size: 1rem;
`;

const FilterContainer = styled.div`
  width: 20%;

  min-width: 60px;
  text-align: center;
  margin: 20px auto;
  margin-bottom: 50px;
`;
const ImageSubtitle = styled.div`
  font-size: 1.1vw;
  color: #777777;
  font-weight: bold;

  @media (max-width: 1000px) {
    display: none;
  }
`;

const StyledImage = styled.img`
  width: 100%;
  max-width: 150px;
  margin: auto;
  height: auto;
  border-radius: 50%;
  opacity: ${(props) => (props.active ? 1 : 0.5)};
  cursor: pointer;
`;

const placeholderImage =
  "https://lh3.googleusercontent.com/proxy/p2t528e-SUnvuw4gN4ADK3dPSAnRxItdJwPJmR9ozKJz_x9i_SCqh493LgJh-8DyjFKIlxSt-oua7e5BYXb6eDMPO8LNBYnT";

class SettingsContent extends Component {
  constructor(props) {
    super(props);
    this.filters = {
      litter: false,
      dive: true,
      sighting: false,
    };
    this.state = {
      filters: {
        litter: false,
        dive: false,
        sighting: false,
      },
      active: true,
      currentActive: "dive-reporter",
    };
  }

  onFilterChange = async (filters) => {
    await this.handleFilterChange(filters);

    this.props.fetchHistory({
      ...this.filters,
    });
  };

  handleFilterChangeteste(filters) {
    let source = [];
    let hasFilters;

    Object.values(filters).map((element) => {
      element.length > 0 ? (hasFilters = true) : (hasFilters = false);
      element.forEach((e) => {
        source[e] = true;
      });
    });

    if (!hasFilters) {
      this.filters = this.state.filters;
    } else {
      this.filters = {
        ...source,
        source,
      };
    }

    /*if (e.target.value == "litter")
      this.filters = {
        ...this.state.filters,
        litter: !this.state.filters.litter,
      };
    else if (e.target.value == "sighting")
      this.filters = {
        ...this.state.filters,
        sighting: !this.state.filters.sighting,
      };
    if (e.target.value == "dive")
      this.filters = {
        ...this.state.filters,
        dive: !this.state.filters.dive,
      };*/
  }

  componentDidMount() {
    this.props.fetchHistory({
      ...this.filters,
    });
  }

  handleFilterChange = (e) => {
    this.setState({
      active: true,
      currentActive: e.target.id,
    });

    this.filters = this.state.filters;

    if (e.target.id == "litter-reporter")
      this.filters = {
        ...this.filters,
        litter: true,
      };
    else if (e.target.id == "whale-reporter")
      this.filters = {
        ...this.filters,
        sighting: true,
      };
    if (e.target.id == "dive-reporter")
      this.filters = {
        ...this.filters,
        dive: true,
      };

    console.log(this.filters);

    this.props.fetchHistory({
      ...this.filters,
    });
  };

  render() {
    const { loading, history, user } = this.props;
    const { active, currentActive } = this.state;

    return (
      <div>
        {user.userable.type_name == "App\\UserDiveCenter" && (
          <DiveContent/>
        )}
        {user.userable.type_name == "App\\UserDiveCenter" && (
          <>
            <DivingSpotContent/>
          </>
        )}
        {user.userable.type_name == "App\\UserDiveCenter" ? (
          <Row>
            <Col xs={23}>
              <Divider orientation="left">
                <StyledTitle> My Profile </StyledTitle>
              </Divider>
            </Col>
            <Col xs={1}>
              <img
                src="/storage/images/logo/dive-reporter"
                width="100%"
              />
            </Col>
          </Row>
        ) : (
          <Row>
            <Divider orientation="left">
              <StyledTitle> My Profile </StyledTitle>
            </Divider>
          </Row>
        )}
        <Row>
          <AcountOverview loading={loading} account={user} />
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMe: () => dispatch(fetchMe()),
    fetchHistory: (filters) => dispatch(fetchHistory(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    history: state.me.history,
    loading: state.auth.loading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsContent);
