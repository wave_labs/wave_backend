import React, { Component } from "react";
import { Row, Col } from "antd";
import { updateMe } from "redux-modules/auth/actions";
import { alertActions } from "redux-modules/alert/actions";
import { connect } from "react-redux";
import styled from "styled-components";
import AccountForm from "./AccountForm";
import moment from "moment";
import ProfileContainer from "../Common/ProfileContainer";

const Container = styled(Row)`
  position: relative;
  margin: 50px auto;
  border-radius: 8px;
  border: none;
  min-height: 300px;
  -webkit-box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.1);
  -moz-box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.1);
  box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.1);

  @media screen and (max-width: 768px) {
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
  }
`;

const StyledCol = styled(Col)`
  border-right: 1px solid rgba(5, 5, 5, 0.1);

  @media screen and (max-width: 768px) {
    border: none;
  }
`;

const StyledImageContainer = styled.div`
  width: 100%;
`;

const StyledImage = styled.img`
  width: 30%;
  height: auto;
  border-radius: 50%;
  min-width: 104px;
  max-width: 204px;
  margin: 5% auto;
  display: block;
`;

const StyledTitle = styled.div`
  font-size: 1.5rem;
  text-align: center;
`;

const StyledSubTitle = styled(StyledTitle)`
  font-size: 1rem;
  color: #777777;
`;

class AccountOverview extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  handleSubmit = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, user) => {
      if (err) {
        return;
      } else {
        const formatedDate = user.b_day
          ? moment(user.b_day).format("YYYY-MM-DD")
          : null;
        this.setState({ confirmLoading: true });
        this.props.updateMe({
          ...user,
          b_day: formatedDate,
        });
      }
    });
  };

  render() {
    let { account, loading } = this.props;

    return (
      <Container type="flex">
        <StyledCol xs={24} lg={6}>
          <ProfileContainer content={account} />
        </StyledCol>
        <Col xs={24} lg={18}>
          {!loading && (
            <AccountForm
              wrappedComponentRef={this.saveFormRef}
              user={this.props.me}
              handleSubmit={this.handleSubmit}
            />
          )}
        </Col>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateMe: (data) => dispatch(updateMe(data)),
    error: (data) => dispatch(alertActions.error(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    me: state.auth.user,
    loading: state.auth.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountOverview);
