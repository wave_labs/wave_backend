import React, { Component } from "react";
import {
  fetchResult,
  deleteResult,
  setResultEdit,
  updateResult
} from "redux-modules/result/actions";
import { connect } from "react-redux";
import {
  Row,
  Col,
  Button,
  Icon,
  Popconfirm,
  Checkbox,
  Input,
  Badge,
  Dropdown,
  Table
} from "antd";
import RowOperation from "../Common/RowOperation";
import TablePagination from "../Common/TablePagination";
import TableFilterRow from "../Common/TableFilterRow";
import {
  startCreating,
  startEditing
} from "redux-modules/editCreateModal/actions";
import { Link } from "react-router-dom";

class ResultTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true
      },
      {
        title: "Question",
        dataIndex: "question"
      },
      {
        title: "Rating",
        dataIndex: "rating"
      },
      {
        title: "",
        key: "",
        render: (text, result) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(result)}
            onDeleteConfirm={() => this.props.deleteResult(result.id)}
          />
        )
      }
    ];
  }

  componentDidMount = () => {
    this.props.fetchResult(this.props.result);
  };

  onUpdateClick = result => {
    this.props.setResultEdit(result);
    this.props.startEditing();
  };

  onSearch = search => {
    this.filters = { ...this.filters, search };
    this.props.fetchResult(this.props.result, 1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order]
    };
    this.props.fetchResult(this.props.result, pagination.current, this.filters);
  };
  render() {
    return (
      <div>
        <TableFilterRow
          onSearch={search => this.onSearch(search)}
          searchPlaceholder="Search Result"
        />

        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchResult: (id, page, filters) =>
      dispatch(fetchResult(id, page, filters)),
    deleteResult: id => dispatch(deleteResult(id)),
    activateResult: id => dispatch(activateResult(id)),
    startEditing: () => dispatch(startEditing()),
    setResultEdit: data => dispatch(setResultEdit(data)),
    updateResult: (id, data) => dispatch(updateResult(id, data))
  };
};

const mapStateToProps = state => {
  return {
    data: state.result.data,
    meta: state.result.meta,
    loading: state.result.loading
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResultTableFilterContainer);
