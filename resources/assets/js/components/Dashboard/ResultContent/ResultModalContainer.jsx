import React, { Component } from "react";
import {
  createResult,
  updateResult,
  resetResultEdit
} from "redux-modules/result/actions";
import {
  resetModal,
  startEditing
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../Common/EditCreateModal";
import ResultForm from "./ResultForm";

class ResultModalContainer extends Component {
  state = {
    confirmLoading: false //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, result) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        this.props
          .updateResult(this.props.editResult.id, result)
          .then(data => {
            this.resetModalForm();
          })
          .catch(e => this.setState({ confirmLoading: false }));
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.resetResultEdit();
    this.props.resetModal();
    form.resetFields();
  };

  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit Result"
        onOkEditClick={() => this.onOkEditClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
      >
        <ResultForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          result={this.props.editResult}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    resetResultEdit: () => dispatch(resetResultEdit()),
    resetModal: () => dispatch(resetModal()),
    createResult: data => dispatch(createResult(data)),
    updateResult: (data, id) => dispatch(updateResult(data, id))
  };
};

const mapStateToProps = state => {
  return {
    editResult: state.result.editResult,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResultModalContainer);
