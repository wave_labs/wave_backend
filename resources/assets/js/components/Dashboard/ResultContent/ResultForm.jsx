import React, { Component } from "react";
import { Row, Col, Form, Input } from "antd";

const FormItem = Form.Item;

class ResultForm extends Component {
  state = {};

  rules = {
    question: [
      {
        required: true,
        message: "Question is required",
      },
    ],
    rating: [
      {
        required: true,
        message: "Please input a rating!",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { result } = this.props;
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row>
          <Col>
            <FormItem hasResult label="Question">
              {getFieldDecorator("question", {
                initialValue: result.question,
                rules: this.rules.question,
              })(<Input placeholder="Question" />)}
            </FormItem>
          </Col>

          <Col>
            <FormItem hasResult label="Rating">
              {getFieldDecorator("question", {
                initialValue: result.rating,
                rules: this.rules.rating,
              })(<Input placeholder="Rating" />)}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(ResultForm);
