import React, { Component } from 'react';
import { connect } from "react-redux";
import ResultModalContainer from './ResultModalContainer'
import ResultTableFilterContainer from './ResultTableFilterContainer'

class ResultContent extends Component {
	render() {
		const { result } = this.props;
		return (
			<div>
				<ResultModalContainer />
				<ResultTableFilterContainer result={this.props.match.params.id}/>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		fetchResult: form => dispatch(fetchResult(id))
	};
};

const mapStateToProps = state => {
	return {
		result: state.result.current,
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ResultContent);

