import React, { Fragment } from "react";
import { Col, Row, Select, DatePicker } from "antd";
import CreatureRemoteSelectContainer from "../../Data/CreatureContent/CreatureRemoteSelectContainer";
import styled from "styled-components";
import FormItem from "../Common/FormItem";

const { Option } = Select;
const { MonthPicker } = DatePicker;

const StyledMonthPicker = styled(MonthPicker)`
  width: 100%;
`;

const WaveDataForm = ({ queryDataForm, handleQueryFormChange }) => {
  return (
    <Fragment>
      <Row gutter={16}>
        <Col md={24} lg={12}>
          <FormItem label="Data Source">
            <Select
              style={{ width: "100%" }}
              placeholder="Data Source"
              value={queryDataForm.source}
              onChange={(field) => {
                handleQueryFormChange({ source: field });
              }}
            >
              <Option value="whaleReporter">Whale Reporter</Option>
            </Select>
          </FormItem>
        </Col>
        <Col md={24} lg={12}>
          <FormItem label="Creature">
            <CreatureRemoteSelectContainer
              value={queryDataForm.creature}
              onChange={(field) => {
                handleQueryFormChange({ creature: field });
              }}
            />
          </FormItem>
        </Col>

        <Col md={24} lg={12}>
          <FormItem label="Start Date">
            <StyledMonthPicker
              disabledDate={(date) => {
                if (!date || !queryDataForm.endDate) {
                  return false;
                }
                return date.valueOf() > queryDataForm.endDate.valueOf();
              }}
              format="YYYY-MM"
              value={queryDataForm.startDate}
              placeholder="Start"
              onChange={(field) => handleQueryFormChange({ startDate: field })}
            />
          </FormItem>
        </Col>
        <Col md={24} lg={12}>
          <FormItem label="End Date">
            <StyledMonthPicker
              disabledDate={(date) => {
                if (!date || !queryDataForm.startDate) {
                  return false;
                }
                return date.valueOf() <= queryDataForm.startDate.valueOf();
              }}
              format="YYYY-MM"
              value={queryDataForm.endDate}
              placeholder="End"
              onChange={(field) => handleQueryFormChange({ endDate: field })}
            />
          </FormItem>
        </Col>
      </Row>
    </Fragment>
  );
};

export default WaveDataForm;
