import React, { Fragment } from "react";
import { Col, Checkbox, Slider, Row } from "antd";
import FormItem from "../Common/FormItem";

const CommonForm = ({ commonForm, handleCommonFormChange, canCrop }) => {
  return (
    <Fragment>
      <Col md={24} lg={12}>
        <FormItem label="Limit ocurrence number">
          <Slider
            style={{ width: "98%" }}
            step={null}
            marks={{
              0: "10",
              20: "200",
              50: "1000",
              100: "2000",
            }}
            value={commonForm.size}
            tipFormatter={null}
            onChange={(e) => handleCommonFormChange({ size: e })}
          />
        </FormItem>
      </Col>
      <Col md={24} lg={12}>
        <FormItem label="Data pre-processing with data cleaning">
          <Col sm={24} lg={12}>
            <Checkbox
              checked={commonForm.duplicates}
              onChange={(e) => handleCommonFormChange({ duplicates: true })}
            >
              Remove duplicates
            </Checkbox>
          </Col>
          <Col sm={24} lg={12}>
            <Checkbox
              disabled={!canCrop}
              checked={commonForm.crop}
              onChange={(e) => handleCommonFormChange({ crop: true })}
            >
              Crop to current map extent
            </Checkbox>
          </Col>
        </FormItem>
      </Col>
    </Fragment>
  );
};

export default CommonForm;
