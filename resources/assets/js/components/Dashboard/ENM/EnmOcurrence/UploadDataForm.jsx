import React, { Fragment } from "react";
import { Col, Row, Radio, Input, Button } from "antd";
import FormItem from "../Common/FormItem";
import FileDragger from "../Common/FileDragger";

const UploadDataForm = ({
  uploadDataForm,
  handleUploadFormChange,
  isUploadReady,
  createOcurrenceData,
  children,
}) => {
  return (
    <Fragment>
      <Row type="flex" justify="space-between">
        <Col md={24} lg={8}>
          <Col span={24}>
            <FileDragger
              file={uploadDataForm.file}
              beforeUpload={(file) => handleUploadFormChange({ file: [file] })}
              onRemove={() => handleUploadFormChange({ file: [] })}
              instructions="Support for single CSV file"
              accept="application/vnd.ms-excel,.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            />
          </Col>
          <Col span={24}>
            <FormItem label="Csv Separator">
              <Radio.Group
                options={[
                  { label: "Tab", value: "tab" },
                  { label: "Comma", value: "comma" },
                  { label: "Semicolon", value: "semicolon" },
                  { label: "Space", value: "space" },
                ]}
                onChange={(e) =>
                  handleUploadFormChange({ separator: e.target.value })
                }
                value={uploadDataForm.separator}
              />
            </FormItem>
          </Col>
        </Col>
        <Col md={24} lg={15}>
          <Row gutter={16}>
            <Col md={24} lg={12}>
              <FormItem label="Column name that contains latitude data points">
                <Input
                  onChange={(e) =>
                    handleUploadFormChange({ latColumn: e.target.value })
                  }
                  value={uploadDataForm.latColumn}
                  placeholder="Latitude"
                />
              </FormItem>
            </Col>
            <Col md={24} lg={12}>
              <FormItem label="Column name that contains longitude data points">
                <Input
                  onChange={(e) =>
                    handleUploadFormChange({ lonColumn: e.target.value })
                  }
                  value={uploadDataForm.lonColumn}
                  placeholder="Longitude"
                />
              </FormItem>
            </Col>

            {children}
          </Row>
        </Col>
        <Col span={24}>
          <Row type="flex" justify="end">
            <Button disabled={!isUploadReady} onClick={createOcurrenceData}>
              Start Upload
            </Button>
          </Row>
        </Col>
      </Row>
    </Fragment>
  );
};

export default UploadDataForm;
