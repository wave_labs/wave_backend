import React, { Fragment } from "react";
import { Col, Row, Select } from "antd";
import CreatureRemoteSelectContainer from "../../Data/CreatureContent/CreatureRemoteSelectContainer";
import FormItem from "../Common/FormItem";
const { Option } = Select;

const PipelineDataForm = ({ source, creature, handlePipelineFormChange }) => {
  return (
    <Fragment>
      <Row gutter={16}>
        <Col md={24} lg={12}>
          <FormItem label="Data Source">
            <Select
              style={{ width: "100%" }}
              placeholder="Data Source"
              value={source}
              onChange={(field) => {
                handlePipelineFormChange({ source: field });
              }}
            >
              <Option value="gbif">
                Global Biodiversity Information Facility (GBIF)
              </Option>
              <Option value="bison">
                Biodiversity Information Serving Our Nation (BISON)
              </Option>
              <Option value="obis">
                Ocean Biogeographic Information System (OBIS)
              </Option>
            </Select>
          </FormItem>
        </Col>
        <Col md={24} lg={12}>
          <FormItem label="Creature">
            <CreatureRemoteSelectContainer
              value={creature}
              onChange={(field) => {
                handlePipelineFormChange({ creature: field });
              }}
            />
          </FormItem>
        </Col>
      </Row>
    </Fragment>
  );
};

export default PipelineDataForm;
