import styled, { css } from "styled-components";
import React from "react";
import { Select, InputNumber, } from "antd";

export const radioStyle = {
  display: "block",
  height: "30px",
  lineHeight: "30px",
};

export const statusToIndicator = {
  "pending": "warning",
  "failed": "error",
  "success": "success",
};

export const ocurrenceLimitConverter = {
  0: 10,
  20: 200,
  50: 1000,
  100: 2000,
};

const fieldSize = css`
  width: 20% !important;
  min-width: 120px !important;
`;

export const StyledSelect = styled(Select)`
  ${fieldSize};
`;

export const InputSize = styled.div`
width: 40% !important;
  ${fieldSize};
`;

export const StyledInput = styled(InputNumber)`
  ${fieldSize};
`;


export const bioclimVariables = [
  'Annual Mean Temperature',
  'Mean Diurnal Range',
  'Isothermality',
  'Temperature Seasonality',
  'Max Temperature of Warmest Month',
  'Min Temperature of Coldest Month',
  'Temperature Annual Range',
  'Mean Temperature of Wettest Quarter',
  'Mean Temperature of Driest Quarter',
  'Mean Temperature of Warmest Quarter',
  'Mean Temperature of Coldest Quarter',
  'Annual Precipitation',
  'Precipitation of Wettest Month',
  'Precipitation of Driest Month',
  'Precipitation Seasonality',
  'Precipitation of Wettest Quarter',
  'Precipitation of Driest Quarter',
  'Precipitation of Warmest Quarter',
  'Precipitation of Coldest Quarter',
]


export const OcurrenceText = () => {
  return (
    <div>
      <p>
        Predictions require occurrence/absence data points of
        cetaceans which may be provided by the user or through the
        system's existing data.
                  </p>
      <p>
        The data collected from the system refers to the
        applications section in the Wave platform, which gathers
        data with citizen scientists. Whale Reporter builds on the
        iconic value of whales and the economic impact of whale
        watching as a form of ecotourism, through a mobile
        application for on- and off-shore whale watching. Dive
        reporter is a tablet application with scuba divers as
        citizen scientists for post-dive abundance estimations, used
        for longitudinal studies necessary to marine biologists.
                  </p>
      <p>
        Users may provide data throught csv files, supplying
        coordinate points of a specific taxa.
                  </p>
    </div>
  )
}
