import React, { Fragment } from "react";
import { Col, Row, Select, DatePicker } from "antd";
import SectionInformation from "../Common/SectionInformation";
import FormItem from "../Common/FormItem";
import CopernicusRemoteSelectContainer from "../../CopernicusContent/CopernicusRemoteSelectContainer";
const { Option } = Select;

const CopernicusForm = ({ data, handleFormChange }) => {
  return (
    <Fragment>
      <SectionInformation information="The Copernicus Marine Environment Monitoring Service (CMEMS) provides regular and systematic reference information on the physical and biogeochemical state, variability and dynamics of the ocean and marine ecosystems for the global ocean and the European regional seas. Many of the data delivered by the service (e.g. temperature, salinity, sea level, currents, wind and sea ice) also play a crucial role in the domain of weather, climate and seasonal forecasting." />
      <Row gutter={16}>
        <Col md={24} lg={12}>
          <FormItem label="Model">
            <CopernicusRemoteSelectContainer
              placeholder="Corpernicus ocean product"
              value={data.product}
              onChange={(field) => {
                handleFormChange({ product: field });
              }}
            />
          </FormItem>
        </Col>
        <Col md={24} lg={12}>
          <FormItem label="Date">
            <DatePicker.MonthPicker
              placeholder="Date"
              value={data.date}
              style={{ width: "100%" }}
              onChange={(field) => {
                handleFormChange({ date: field });
              }}
            />
          </FormItem>
        </Col>
      </Row>
    </Fragment>
  );
};

export default CopernicusForm;
