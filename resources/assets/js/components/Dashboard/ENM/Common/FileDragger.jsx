import { Upload } from "antd";
import React from "react";
import { UploadOutlined, FileDoneOutlined } from "@ant-design/icons";

const { Dragger } = Upload;

const FileDragger = ({
  beforeUpload,
  onRemove,
  file,
  accept,
  instructions,
  directory = false,
}) => {
  return (
    <Dragger
      directory={directory}
      multiple={false}
      showUploadList={false}
      accept={accept}
      beforeUpload={(file) => {
        beforeUpload(file);
        return false;
      }}
      onRemove={() => {
        onRemove();
        return true;
      }}
    >
      {file.length == 0 ? (
        <div>
          <p className="ant-upload-drag-icon">
            <UploadOutlined />
          </p>
          <p className="ant-upload-hint">{instructions}</p>
        </div>
      ) : (
        <div>
          <p className="ant-upload-drag-icon">
            <FileDoneOutlined />
          </p>
          {file.map((e, i) => (
            <p key={i} className="ant-upload-hint">
              {e.name}
            </p>
          ))}
        </div>
      )}
    </Dragger>
  );
};

export default FileDragger;
