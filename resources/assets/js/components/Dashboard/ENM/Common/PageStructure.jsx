import { Col, Row } from "antd";
import React from "react";
import styled from "styled-components";

const customGrey = "#f3f3f3";

const Container = styled(Row)`
  margin: 50px auto;
`;

const DataContainer = styled(Col)`
  margin-top: 50px;
`;

const FormContent = styled.div`
  min-height: 200px;
  border: 4px solid ${customGrey};

  .title {
    background: ${customGrey};
    font-weight: bold;
    font-size: 1.2em;
    padding: 15px;
  }

  .form-container {
    padding: 20px;
  }
`;

const FormContainer = ({ title, children }) => {
  return (
    <FormContent>
      <div className="title">{title}</div>
      <div className="form-container">{children}</div>
    </FormContent>
  );
};

const PageStructure = ({ form, visualize, title }) => {
  return (
    <Container>
      <Col span={24}>
        {title && <FormContainer title={title}>{form}</FormContainer>}
      </Col>
      <DataContainer span={24}>
        <Row type="flex" gutter={24}>
          {visualize}
        </Row>
      </DataContainer>
    </Container>
  );
};

export default PageStructure;
