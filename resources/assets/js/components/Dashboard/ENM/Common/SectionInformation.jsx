import React from "react";
import styled from "styled-components";
import { Fragment } from "react";

const Title = styled.h3`
  font-weight: bold;
`;

const SectionInformation = ({ information, title = "Information" }) => {
  return (
    <Fragment>
      <Title>{title}</Title>
      <div>{information}</div>
    </Fragment>
  );
};

export default SectionInformation;
