import { Col, Table } from "antd";
import TablePagination from "../../Common/TablePagination";
import React from "react";

const TableView = ({ data, columns, loading }) => {
  return (
    <Col xs={24} lg={8}>
      <TablePagination
        data={data}
        meta={[]}
        columns={columns}
        loading={loading}
      />
    </Col>
  );
};

export default TableView;
