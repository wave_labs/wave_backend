import { Col, Popconfirm } from "antd";
import MapBoxMap from "../../Common/MapBoxMap";
import React from "react";
import { Marker } from "react-map-gl";

let MapView = ({ data, defaultCenter, loading, deleteOcurrenceDataRecord }) => {
  return (
    <Col xs={24} lg={16}>
      <div className="map-container">
        <MapBoxMap
          loading={loading}
          zoom={5}
          defaultCenter={defaultCenter.lat && defaultCenter}
        >
          {data.map((record) => (
            <Marker
              key={record.id}
              longitude={parseFloat(record.longitude)}
              latitude={parseFloat(record.latitude)}
            >
              <Popconfirm
                title="Are you sure to remove this record?"
                okText="Yes"
                cancelText="No"
                onConfirm={() => deleteOcurrenceDataRecord(record.id)}
              >
                <a href="javascript:;">
                  <img
                    src={`${
                      window.location.origin
                    }/storage/images/map-markers/32.png`}
                  />
                </a>
              </Popconfirm>
            </Marker>
          ))}
        </MapBoxMap>
      </div>
    </Col>
  );
};

export default MapView;
