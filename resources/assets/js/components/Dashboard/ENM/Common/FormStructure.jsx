import { Row, Col } from "antd";
import React from "react";
import styled from "styled-components";
import { dimensions } from "helpers";

const RadioGroup = styled(Col)`
  border-right: 2px solid grey;

  @media (max-width: ${dimensions.md}) {
    border: none;
    border-bottom: 2px solid grey;
    padding-bottom: 15px;
    margin-bottom: 15px;
  }
`;

const Space = styled(Col)`
  @media (max-width: ${dimensions.md}) {
    display: none;
  }
`;

const FormStructure = ({ leftContent, rightContent }) => {
  return (
    <Row>
      <RadioGroup sm={24} md={24} lg={3}>
        {leftContent}
      </RadioGroup>
      <Space span={1} />
      <Col sm={24} md={24} lg={20}>
        {rightContent}
      </Col>
    </Row>
  );
};

export default FormStructure;
