import React from "react";
import styled from "styled-components";

const Label = styled.p`
  margin-top: 10px;

  &:after {
    content: ": ";
  }
`;

const FormItem = ({ label, children }) => {
  return (
    <div>
      <Label>{label}</Label>
      {children}
    </div>
  );
};

export default FormItem;
