import { Col, Row } from "antd";
import React, { Component } from "react";
import styled from "styled-components";
import { createPrediction } from "redux-modules/enm/actions";
import { connect } from "react-redux";
import PageStructure from "./Common/PageStructure";
import LoadingLogo from "../../Common/LoadingLogo";
import { dimensions } from "helpers";

const PlotContainer = styled.div`
  width: 18%;
  min-width: 250px;

  img {
    width: 100%;
  }
  h2,
  h3 {
    margin-left: 5px;
  }

  h2 {
    font-weight: bold;
    text-transform: capitalize;
  }

  h3 {
    font-size: 1.2em;
  }
`;

const LoadingContainer = styled.div`
  text-align: center;
  h1 {
    font-weight: bold;
    font-size: 1.2em;
  }
  h2 {
    font-size: 1em;
    color: #777;
  }
`;

const ErrorContainer = styled.div`
  width: 60%;
  min-width: 300px;

  h3 {
    font-weight: bold;
    font-size: 1.4em;
  }

  img {
    width: 80%;
    margin: auto;
    display: block;
    @media (max-width: ${dimensions.sm}) {
      width: 100%;
    }
  }

  p,
  ul {
    color: #777;
  }
`;

class ENMResult extends Component {
  componentDidMount() {
    if (this.props.results.length == 0) {
      let formData = new FormData();
      formData.append("bioclimVars", this.props.variables.bioclim);
      formData.append("resolution", this.props.variables.resolution);
      formData.append("kfold", this.props.variables.kfold);
      for (var i = 0; i < this.props.models.length; i++) {
        formData.append(`${"models"}[]`, this.props.models[i]);
      }
      this.props.createPrediction(this.props.enm, formData);
    } else {
      this.props.setStepReady();
    }
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.predictions != this.props.predictions ||
      this.props.hasFailed
    ) {
      this.props.setStepReady();
    }
  }

  render() {
    const ErrorScreen = () => {
      return (
        <ErrorContainer>
          <Col xs={24} md={12}>
            <img src="/images/error.svg" alt="error" />
          </Col>
          <Col xs={24} md={12}>
            <h3>Unexpected error</h3>
            <p>
              Unfortunately, the modeling process has failed and predictions for
              selected models could not be generated.
            </p>
            <p>The process has failed due to:</p>
            <ul>
              {this.props.alerts.map((alert, index) => (
                <li key={index}>{alert}</li>
              ))}
            </ul>
          </Col>
        </ErrorContainer>
      );
    };
    const ResultsScreen = ({ data }) => {
      return (
        <Row
          type="flex"
          justify="space-between"
          align="bottom"
          style={{ width: "100%" }}
        >
          {data.map((prediction, index) => (
            <PlotContainer key={index}>
              {(index % 5 == 0 || index == 0) && <h2>{prediction.model}</h2>}

              <div>
                <h3>{prediction.predictionType}</h3>
                <img
                  src={`${
                    window.location.origin
                  }/api/enm/getPredictionImage?image=${prediction.path}`}
                  alt={prediction.model + "-" + prediction.predictionType}
                />
              </div>
            </PlotContainer>
          ))}
        </Row>
      );
    };
    return (
      <PageStructure
        visualize={
          <Row
            type="flex"
            justify="center"
            align="middle"
            style={{ width: "100%", minHeight: "300px" }}
          >
            {this.props.loading ? (
              <LoadingContainer>
                <LoadingLogo />
                <h1>Running selected model(s)...</h1>
                <h2>
                  Process may take a few minutes depending on the number of
                  models
                </h2>
              </LoadingContainer>
            ) : this.props.hasFailed ? (
              <ErrorScreen />
            ) : (
              <ResultsScreen
                data={
                  this.props.results.length > 0
                    ? this.props.results
                    : this.props.predictions
                }
              />
            )}
          </Row>
        }
      />
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createPrediction: (id, data) => dispatch(createPrediction(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    enm: state.enm.enm,
    alerts: state.enm.alerts,
    loading: state.enm.loadingPrediction,
    variables: state.enm.variables,
    predictions: state.enm.predictions,
    hasFailed: state.enm.hasFailed,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ENMResult);
