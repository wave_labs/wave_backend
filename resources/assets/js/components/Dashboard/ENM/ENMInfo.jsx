import { Col, Row } from "antd";
import React, { Component } from "react";
import styled from "styled-components";
import EnmTableContainer from "./EnmInfo/EnmTableContainer";

const StyledTitle = styled.h1`
  font-size: 2.5em;
`;

const Container = styled(Row)`
  margin: 50px auto;
`;

class ENMInfo extends Component {
  render() {
    return (
      <Container type="flex" justify="space-between">
        <Col md={24} lg={6}>
          <StyledTitle>Workflow</StyledTitle>
          <ul>
            <li>Select Ocurrence Data</li>
            <ul>
              <li>Query system</li>
              <li>Scientific Repositories</li>
              <li>Upload Data</li>
            </ul>
            <li>Select Environmental Covariates</li>
            <ul>
              <li>Bioclim</li>
              <li>Copernicus Marine Services</li>
              <li>Upload Data</li>
            </ul>
            <li>Select and Run Model(s)</li>
            <ul>
              <li>DOMAIN</li>
              <li>BIOCLIM</li>
              <li>Maxent</li>
            </ul>
            <li>Visualize Prediction Output</li>
            <ul>
              <li>Response Plot</li>
              <li>Raster Object</li>
              <li>Receiver operating characteristic</li>
              <li>True Positive Rate</li>
              <li>Presence / Absence</li>
            </ul>
          </ul>
        </Col>
        <Col md={24} lg={16}>
          <StyledTitle>Previous interactions</StyledTitle>
          <EnmTableContainer handleEditClick={this.props.handleEditClick} />
        </Col>
      </Container>
    );
  }
}

export default ENMInfo;
