import React, { Component } from "react";
import { fetchEnms, deleteEnm } from "redux-modules/enm/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import styled from "styled-components";
import { isFieldNull } from "helpers";
import { statusToIndicator } from "../enmHelper";
import { Badge } from "antd";
import StopPropagation from "../../Common/StopPropagation";

const TableListItem = styled.p`
  margin: 2px 0px;
`;
const Status = styled.span`
  text-transform: capitalize;
`;

class EnmTableContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {
      self: true,
    };

    this.columns = [
      {
        title: "Status",
        dataIndex: "status",
        render: (element) => (
          <Badge
            status={statusToIndicator[element]}
            text={<Status>{element}</Status>}
          />
        ),
      },
      {
        title: "Creature",
        dataIndex: "creature",
        render: (element) => isFieldNull(element),
      },
      {
        title: "Ocurrence Source",
        dataIndex: "ocurrenceData",
        render: (element) => isFieldNull(element),
      },
      {
        title: "Environmental Source",
        dataIndex: "environmentalData",
        render: (element) => isFieldNull(element),
      },
      {
        title: "Models",
        dataIndex: "models",
        render: (models) => (
          <span>
            {models.length > 0
              ? models.map((model) => (
                  <div key={model.id}>
                    <TableListItem> {model.model} </TableListItem>
                  </div>
                ))
              : isFieldNull()}
          </span>
        ),
      },
      {
        title: "Predictions",
        dataIndex: "predictions",
        render: (predictions) => {
          let newPredictions = this.handlePredictions(predictions);
          let content = (
            <span>
              {newPredictions.length > 0
                ? newPredictions.map((prediction, index) => (
                    <div key={index}>
                      <TableListItem>{prediction}</TableListItem>
                    </div>
                  ))
                : isFieldNull()}
            </span>
          );

          const obj = {
            children: content,
            props: {},
          };
          return obj;
        },
      },
      {
        title: "Created at",
        dataIndex: "created_at",
      },
      {
        title: "",
        key: "",
        render: (text, element) => (
          <StopPropagation>
            <RowOperation
              deleteRow
              onDeleteConfirm={() => this.props.deleteEnm(element.id)}
            />
          </StopPropagation>
        ),
      },
    ];
  }

  handlePredictions = (predictions) => {
    let array = [];
    predictions.map((prediction, index) => {
      if (!array.includes(prediction.predictionType)) {
        array.push(prediction.predictionType);
      }
    });

    return array;
  };

  componentDidMount = () => {
    this.props.fetchEnms(1, this.filters);
  };

  onUpdateClick = (e) => {
    console.log(e);
  };

  handleTableChange = (pagination) => {
    this.props.fetchEnms(pagination.current, this.props.filters);
  };

  render() {
    return (
      <div>
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
          onRow={(record) => ({
            onClick: () => {
              this.props.handleEditClick(record);
            },
          })}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchEnms: (page, filters) => dispatch(fetchEnms(page, filters)),
    deleteEnm: (id) => dispatch(deleteEnm(id)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.enm.data,
    meta: state.enm.meta,
    loading: state.enm.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnmTableContainer);
