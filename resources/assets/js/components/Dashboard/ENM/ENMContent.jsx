import React, { Component } from "react";
import { Steps, Button, Row, message } from "antd";
import ENMInfo from "./ENMInfo";
import ENMOcurrenceData from "./ENMOcurrenceData";
import ENMEnvData from "./ENMEnvData";
import ENMModel from "./ENMModel";
import ENMResult from "./ENMResult";
import { resetEnm, setEnm } from "redux-modules/enm/actions";
import { connect } from "react-redux";

const { Step } = Steps;

message.config({
  maxCount: 5,
  duration: 5,
});

export class ENMContent extends Component {
  state = {
    current: 0,
    ready: false,
    finished: false,
    models: [],
    predictions: [],
  };

  componentDidUpdate(prevProps) {
    if (this.state.finished) {
      this.props.resetEnm();
      this.setState({ finished: false, models: [], predictions: [], kfold: 5 });
    }

    if (prevProps.alerts != this.props.alerts && this.props.alerts.length > 0) {
      this.props.alerts.map((alert) => message.error(alert));
    }

    if (prevProps.hasFailed != this.props.hasFailed) {
      this.resetVariables();
    }
  }

  setReady = (ready = true) => {
    this.setState({
      ready: ready,
    });
  };

  next = () => {
    this.setState({
      current: this.state.current + 1,
      ready: false,
    });
  };

  prev = () => {
    this.setState({
      current: this.state.current - 1,
    });
  };

  resetVariables = () => {
    this.setState({
      current: 0,
      finished: true,
      models: [],
      predictions: [],
      kfold: 5,
    });
  };

  setModels = (data) => {
    this.setState({ ...data });
  };

  handleEditClick = (row) => {
    if (row.status === "pending") {
      this.props.setEnm(row.id);
      if (!row.ocurrenceData) {
        this.setState({ current: 1 });
      } else if (!row.environmentalData) {
        this.setState({ current: 2 });
      } else this.setState({ current: 3 });
    } else if (row.status === "success") {
      this.setState({ current: 4, predictions: row.predictions });
    }
  };

  render() {
    let { current, ready } = this.state;

    const steps = [
      {
        title: "Information",
        content: <ENMInfo handleEditClick={this.handleEditClick} />,
      },
      {
        title: "Ocurrence Data",
        content: <ENMOcurrenceData setStepReady={this.setReady} />,
      },
      {
        title: "Environment Data",
        content: <ENMEnvData setStepReady={this.setReady} />,
      },
      {
        title: "Models",
        content: (
          <ENMModel setModels={this.setModels} setStepReady={this.setReady} />
        ),
      },
      {
        title: "Results",
        content: (
          <ENMResult
            results={this.state.predictions}
            models={this.state.models}
            kfold={this.state.kfold}
            setStepReady={this.setReady}
          />
        ),
      },
    ];

    return (
      <>
        <Steps current={current}>
          {steps.map((item, index) => (
            <Step key={index} title={item.title} />
          ))}
        </Steps>
        <div className="steps-content">{steps[current].content}</div>
        <div className="steps-action">
          <Row type="flex" justify="end">
            {current > 0 && current !== steps.length - 1 && (
              <Button style={{ margin: "0 8px" }} onClick={this.prev}>
                Previous
              </Button>
            )}
            {current == 0 ? (
              <Button type="primary" onClick={this.next}>
                Start
              </Button>
            ) : (
              current < steps.length - 1 && (
                <Button type="primary" onClick={this.next} disabled={!ready}>
                  Next
                </Button>
              )
            )}
            {current === steps.length - 1 && (
              <Button
                type="primary"
                disabled={!ready}
                onClick={() => this.resetVariables()}
              >
                Done
              </Button>
            )}
          </Row>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    alerts: state.enm.alerts,
    enm: state.enm.enm,
    hasFailed: state.enm.hasFailed,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    resetEnm: () => dispatch(resetEnm()),
    setEnm: (id) => dispatch(setEnm(id)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ENMContent);
