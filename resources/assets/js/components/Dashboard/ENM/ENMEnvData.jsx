import { Col, Row, Radio, Select, Checkbox } from "antd";
import React, { Component, Fragment } from "react";
import styled from "styled-components";
import PageStructure from "./Common/PageStructure";
import { radioStyle, bioclimVariables } from "./enmHelper";
import {
  setBioclimVars,
  setAlerts,
  getCopernicusData,
  uploadEnvironmentalData,
} from "redux-modules/enm/actions";
import { connect } from "react-redux";
import LoadingContainer from "../../Common/LoadingContainer";
import { dimensions, handleArrayToFormData } from "helpers";
import FileDragger from "./Common/FileDragger";
import CopernicusForm from "./Environmental/CopernicusForm";
import SectionInformation from "./Common/SectionInformation";
import FormItem from "./Common/FormItem";

const ImageContainer = styled.div`
  width: 23%;
  min-width: 180px;
  margin: 20px auto;

  @media (max-width: ${dimensions.lg}) {
    width: 30%;
  }

  @media (max-width: ${dimensions.md}) {
    width: 50%;
  }

  img {
    width: 100%;
  }
  h3 {
    text-align: center;
    @media (max-width: ${dimensions.md}) {
      font-size: 1em;
    }
  }
`;

const CustomCheckbox = styled(Checkbox)`
  margin: 5px 0 !important;

  @media (max-width: ${dimensions.md}) {
    font-size: 0.8em;
  }
`;

const { Option } = Select;

class ENMEnvData extends Component {
  state = {
    source: undefined,
    resolution: undefined,
    file: [],
    copernicusForm: {
      product: undefined,
      date: undefined,
    },
    bioclim: [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
    ],
  };

  componentWillUnmount() {
    let { bioclim, resolution, source } = this.state;
    if (source == 1) {
      this.uploadData();
    } else if (source == 2) {
      let bioVars = [];
      bioclim.map((element, index) => {
        if (element) {
          bioVars.push(index + 1);
        }
      });

      this.props.setBioclimVars({ bioclim: bioVars, resolution: resolution });
    } else if (source == 3) {
      this.getCopernicusData();
    }
  }

  handleBioclimVariable = (index) => {
    var variable = this.state.bioclim;
    variable[index] = !variable[index];
    this.setState({ bioclim: variable }, this.isStepReady());
  };

  uploadData = () => {
    let formData = new FormData();
    formData = handleArrayToFormData(formData, this.state.file, "files");

    this.props.uploadEnvironmentalData(formData, this.props.enm);
  };

  getCopernicusData = () => {
    let { copernicusForm } = this.state;

    this.props.getCopernicusData(this.props.enm, {
      date: copernicusForm.date,
      copernicusProduct: copernicusForm.product,
    });
  };

  isStepReady = () => {
    if (
      (this.state.resolution && this.state.bioclim.includes(true)) ||
      this.state.file.length > 0 ||
      (this.state.copernicusForm.product && this.state.copernicusForm.date)
    )
      this.props.setStepReady();
    else this.props.setStepReady(false);
  };

  handleSourceChange = (field) => {
    this.setState({ source: field.target.value }, this.isStepReady());
  };

  handleResolutionChange = (field) => {
    this.setState({ resolution: field }, () => this.isStepReady());
  };

  handleDirectoryUpload = (newfile) => {
    let { file } = this.state;
    let filename = newfile.name.split(".");
    if (
      filename[1] == "tif" ||
      filename[1] == "asc" ||
      filename[1] == "nc" ||
      filename[1] == "netcdf"
    ) {
      file.push(newfile);
    }

    this.setState({ file }, this.isStepReady);
  };

  handleCopernicusForm = (field) => {
    this.setState(
      {
        copernicusForm: { ...this.state.copernicusForm, ...field },
      },
      () => this.isStepReady()
    );
  };

  render() {
    const UploadForm = () => {
      return (
        <Row type="flex" justify="space-around" align="middle">
          <Col sm={24} md={12}>
            <FileDragger
              directory
              file={this.state.file}
              beforeUpload={(file) => this.handleDirectoryUpload(file)}
              onRemove={() => this.setState({ file: undefined })}
              instructions="Support for directory of tif and/or asc files"
              accept=".tif, .asc, .nc"
            />
          </Col>
        </Row>
      );
    };

    const Bioclim = () => {
      return (
        <div>
          <SectionInformation
            information="Bioclimatic variables are derived variables from the monthly mean,
            max, mean temperature, and mean precipitation values. They are
            developed for species distribution modeling and related ecological
            applications. They represent annual trends (e.g., mean annual
            temperature, annual precipitation) seasonality (e.g., annual range
            in temperature and precipitation) and extreme or limiting
            environmental factors (e.g., temperature of the coldest and warmest
            month, and precipitation of the wet and dry quarters). A quarter is
            defined as a period of three months (1/4 of the year)."
          />

          <FormItem label="Resolution">
            <Select
              style={{ width: "100%" }}
              placeholder="Select WordClim bioclimatic data resolution"
              value={this.state.resolution}
              onChange={this.handleResolutionChange}
            >
              <Option value={2.5}>2.5</Option>
              <Option value={5}>5</Option>
              <Option value={10}>10</Option>
            </Select>
          </FormItem>

          <FormItem label="Variables">
            <Row type="flex" align="top">
              {bioclimVariables.map((variable, index) => (
                <Col xs={12} md={8} key={index}>
                  <CustomCheckbox
                    onClick={() => this.handleBioclimVariable(index)}
                    checked={this.state.bioclim[index]}
                  >
                    {variable}
                  </CustomCheckbox>
                </Col>
              ))}
            </Row>
          </FormItem>
        </div>
      );
    };
    let { bioclim } = this.state;
    let { loading } = this.props;
    return (
      <PageStructure
        title="Select Environmental Data"
        form={
          <Row type="flex" justify="space-between">
            <Col s={24} lg={3} style={{ borderRight: "2px solid grey" }}>
              <Radio.Group
                onChange={(field) => this.handleSourceChange(field)}
                value={this.state.source}
              >
                <Radio style={radioStyle} value={1}>
                  Upload Data
                </Radio>
                <Radio style={radioStyle} value={2}>
                  Bioclim
                </Radio>
                <Radio style={radioStyle} value={3}>
                  Copernicus
                </Radio>
              </Radio.Group>
            </Col>
            <Col s={24} lg={20}>
              {this.state.source == 1 ? (
                <UploadForm />
              ) : this.state.source == 2 ? (
                <Bioclim />
              ) : this.state.source == 3 ? (
                <CopernicusForm
                  data={this.state.copernicusForm}
                  handleFormChange={this.handleCopernicusForm}
                />
              ) : (
                <SectionInformation
                  information={
                    <div>
                      <p>
                        System employs correlative species distribution
                        modelling, which means that the prediction is a result
                        of observed distribution of a species as a function of
                        environmental conditions.
                      </p>
                      <p>
                        Currently system supports user uploaded data, where you
                        can select a directory of environmental data directly
                        from your computer. The directory may include TIF
                        (Tagged Image Format), ASC (armored ASCII file) and NC
                        (ASCII text file) files.
                      </p>
                      <p>
                        Additionally you can query either public available
                        BIOCLIM or Copernicus Marine Services data.
                      </p>
                    </div>
                  }
                />
              )}
            </Col>
          </Row>
        }
        visualize={
          this.state.source == 2 && (
            <LoadingContainer loading={loading}>
              <Row type="flex" justify="space-around">
                {bioclim.map((element, index) => (
                  <Fragment key={index}>
                    {element && (
                      <ImageContainer>
                        <h3>{bioclimVariables[index]}</h3>
                        <img
                          src={`${
                            window.location.origin
                          }/api/enm/getEnvImage?image=${index + 1}`}
                        />
                      </ImageContainer>
                    )}
                  </Fragment>
                ))}
              </Row>
            </LoadingContainer>
          )
        }
      />
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setBioclimVars: (array) => dispatch(setBioclimVars(array)),
    setAlerts: (array) => dispatch(setAlerts(array)),
    uploadEnvironmentalData: (data, enm) =>
      dispatch(uploadEnvironmentalData(data, enm)),
    getCopernicusData: (enm, filters) =>
      dispatch(getCopernicusData(enm, filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    enm: state.enm.enm,
    loading: state.enm.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ENMEnvData);
