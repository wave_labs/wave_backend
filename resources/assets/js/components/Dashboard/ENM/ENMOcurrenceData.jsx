import { Col, Popconfirm, Radio, Row } from "antd";
import React, { Fragment, Component } from "react";
import PageStructure from "./Common/PageStructure";
import { DeleteOutlined } from "@ant-design/icons";
import TableView from "./Common/TableView";
import { Marker } from "react-map-gl";
import MapBoxMap from "../Common/MapBoxMap";
import { alertActions } from "redux-modules/alert/actions";
import moment from "moment";
import {
  createOcurrenceData,
  deleteOcurrenceDataDuplicates,
  queryOcurrenceData,
  limitOcurrenceData,
  deleteOcurrenceDataRecord,
  getOcurrenceDataFromPipeline,
  cropOcurrenceDataGeographicExtent,
} from "redux-modules/enm/actions";
import { connect } from "react-redux";
import {
  radioStyle,
  ocurrenceLimitConverter,
  OcurrenceText,
} from "./enmHelper";
import WaveDataForm from "./EnmOcurrence/WaveDataForm";
import UploadDataForm from "./EnmOcurrence/UploadDataForm";
import CommonForm from "./EnmOcurrence/CommonForm";
import FormStructure from "./Common/FormStructure";
import PipelineDataForm from "./EnmOcurrence/PipelineDataForm";

class ENMOcurrenceData extends Component {
  constructor(props) {
    super(props);

    this.mapContainerRef = React.createRef();

    this.state = {
      source: undefined,
      isUploadReady: false,
      queryDataForm: {
        startDate: null,
        endDate: null,
        source: undefined,
        creature: undefined,
      },
      uploadDataForm: {
        separator: undefined,
        latColumn: undefined,
        lonColumn: undefined,
        file: [],
      },
      commonForm: {
        crop: false,
        size: 100,
        duplicates: false,
      },
      pipelineForm: {
        source: undefined,
        creature: undefined,
      },
    };
  }

  cropData = () => {
    if (this.props.enm) {
      let bounds = this.mapContainerRef.current.getMapBounds();

      this.props.cropOcurrenceDataGeographicExtent(this.props.enm, {
        latMin: bounds._sw.lat,
        lonMin: bounds._sw.lng,
        latMax: bounds._ne.lat,
        lonMax: bounds._ne.lng,
      });
    }
  };

  createOcurrenceData = () => {
    let { uploadDataForm } = this.state;
    let formData = new FormData();
    formData.append("file", uploadDataForm.file[0]);
    this.props
      .createOcurrenceData(formData, {
        separator: uploadDataForm.separator,
        latColumn: uploadDataForm.latColumn,
        lonColumn: uploadDataForm.lonColumn,
        enm: this.props.enm,
      })
      .then(() => {
        this.handleProcessData();
      });
  };

  queryOcurrenceData = () => {
    let { startDate, endDate, source, creature } = this.state.queryDataForm;

    if (source && creature) {
      this.props
        .queryOcurrenceData({
          creature: creature,
          source: source,
          enm: this.props.enm,
          date: [
            startDate ? moment(startDate).format("YYYY-MM") : null,
            endDate ? moment(endDate).format("YYYY-MM") : null,
          ],
        })
        .then(() => {
          this.handleProcessData();
        });
    }
  };

  handleProcessData = () => {
    let { commonForm } = this.state;
    if (this.props.enm) {
      if (commonForm.size != 100) {
        this.limitData();
      }
      if (commonForm.duplicates) {
        this.removeDups();
      }
      if (commonForm.crop) {
        this.cropData();
      }
    }
  };

  componentWillUnmount() {
    this.setState({
      source: undefined,
      queryDataForm: {
        startDate: null,
        endDate: null,
        source: undefined,
        creature: undefined,
      },
      commonForm: {
        crop: false,
        size: 100,
        duplicates: false,
      },
      uploadDataForm: {
        separator: undefined,
        latColumn: undefined,
        lonColumn: undefined,
        file: undefined,
      },
      isUploadReady: false,
      pipelineForm: {
        source: undefined,
        creature: undefined,
      },
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.enm != this.props.enm) {
      this.props.setStepReady();
    }
  }

  limitData = () => {
    if (this.props.enm) {
      this.props.limitOcurrenceData(this.props.enm, {
        limit: ocurrenceLimitConverter[this.state.commonForm.size],
      });
    }
  };

  removeDups = () => {
    if (this.props.enm) {
      this.props.deleteOcurrenceDataDuplicates(this.props.enm);
    }
  };

  deleteOcurrenceDataRecord = (record) => {
    this.props.deleteOcurrenceDataRecord(this.props.enm, record + 1);
  };

  handleUploadReady = () => {
    let { lonColumn, latColumn, file, separator } = this.state.uploadDataForm;

    if (lonColumn && latColumn && file && separator) {
      this.setState({ isUploadReady: true });
    }
  };

  handleCommonFormChange = (field) => {
    Object.keys(field).map((key) => {
      switch (key) {
        case "duplicates":
          this.removeDups();
          break;
        case "size":
          this.limitData();
          break;
        case "crop":
          this.cropData();
          break;
        default:
          break;
      }
    });
    this.setState({
      commonForm: { ...this.state.commonForm, ...field },
    });
  };

  handleQueryFormChange = (field) => {
    this.setState(
      {
        queryDataForm: { ...this.state.queryDataForm, ...field },
      },
      () => this.queryOcurrenceData()
    );
  };

  handleUploadFormChange = (field) => {
    this.setState(
      {
        uploadDataForm: { ...this.state.uploadDataForm, ...field },
      },
      this.handleUploadReady()
    );
  };

  handlePipelineFormChange = (element) => {
    this.setState(
      {
        pipelineForm: { ...this.state.pipelineForm, ...element },
      },
      () => {
        if (
          this.state.pipelineForm.source &&
          this.state.pipelineForm.creature
        ) {
          this.props
            .getOcurrenceDataFromPipeline({
              creature: this.state.pipelineForm.creature,
              source: this.state.pipelineForm.source,
              enm: this.props.enm,
            })
            .then(() => {
              this.handleProcessData();
            });
        }
      }
    );
  };

  render() {
    return (
      <PageStructure
        title="Select Ocurrence Data"
        form={
          <FormStructure
            leftContent={
              <Radio.Group
                onChange={(field) =>
                  this.setState({
                    source: field.target.value,
                    commonForm: {
                      size: 100,
                      duplicates: false,
                      crop: false,
                    },
                  })
                }
                value={this.state.source}
              >
                <Radio style={radioStyle} value={1}>
                  Upload Data
                </Radio>
                <Radio style={radioStyle} value={2}>
                  Wave's Data
                </Radio>
                <Radio style={radioStyle} value={3}>
                  Scientific Repositories
                </Radio>
              </Radio.Group>
            }
            rightContent={
              <Fragment>
                {this.state.source == 1 ? (
                  <Fragment>
                    <UploadDataForm
                      uploadDataForm={this.state.uploadDataForm}
                      isUploadReady={this.state.isUploadReady}
                      handleUploadFormChange={this.handleUploadFormChange}
                      createOcurrenceData={this.createOcurrenceData}
                    >
                      <CommonForm
                        canCrop={this.props.ocurrenceData.length && true}
                        commonForm={this.state.commonForm}
                        handleCommonFormChange={this.handleCommonFormChange}
                      />
                    </UploadDataForm>
                  </Fragment>
                ) : this.state.source == 2 ? (
                  <Fragment>
                    <WaveDataForm
                      handleQueryFormChange={this.handleQueryFormChange}
                      queryDataForm={this.state.queryDataForm}
                    />
                    <Row gutter={16}>
                      <CommonForm
                        canCrop={this.props.ocurrenceData.length && true}
                        commonForm={this.state.commonForm}
                        handleCommonFormChange={this.handleCommonFormChange}
                      />
                    </Row>
                  </Fragment>
                ) : this.state.source == 3 ? (
                  <Fragment>
                    <PipelineDataForm
                      source={this.state.pipelineForm.source}
                      creature={this.state.pipelineForm.creature}
                      handlePipelineFormChange={this.handlePipelineFormChange}
                    />
                    <Row gutter={16}>
                      <CommonForm
                        canCrop={this.props.ocurrenceData.length && true}
                        commonForm={this.state.commonForm}
                        handleCommonFormChange={this.handleCommonFormChange}
                      />
                    </Row>
                  </Fragment>
                ) : (
                  <OcurrenceText />
                )}
              </Fragment>
            }
          />
        }
        visualize={
          <Fragment>
            <Col xs={24} lg={16}>
              <div className="map-container">
                <MapBoxMap
                  loading={this.props.loading}
                  zoom={5}
                  defaultCenter={
                    this.props.ocurrenceCenter.lat && this.props.ocurrenceCenter
                  }
                  ref={this.mapContainerRef}
                >
                  {this.props.ocurrenceData.map((record) => (
                    <Marker
                      key={record.id}
                      longitude={parseFloat(record.longitude)}
                      latitude={parseFloat(record.latitude)}
                    >
                      <Popconfirm
                        title="Are you sure to remove this record?"
                        okText="Yes"
                        cancelText="No"
                        onConfirm={() =>
                          this.deleteOcurrenceDataRecord(record.id)
                        }
                      >
                        <a href="javascript:;">
                          <img
                            src={`${
                              window.location.origin
                            }/storage/images/map-markers/32.png`}
                          />
                        </a>
                      </Popconfirm>
                    </Marker>
                  ))}
                </MapBoxMap>
              </div>
            </Col>

            <TableView
              loading={this.props.loading}
              data={this.props.ocurrenceData}
              columns={[
                {
                  title: "#",
                  dataIndex: "id",
                },
                {
                  title: "Coordinates",
                  dataIndex: "latitude",
                  render: (latitude, row) => (
                    <span>
                      {latitude} , {row.longitude}
                    </span>
                  ),
                },
                {
                  title: "Action",
                  key: "",
                  render: (key, record) => (
                    <Popconfirm
                      title="Are you sure to remove this record?"
                      okText="Yes"
                      cancelText="No"
                      onConfirm={() =>
                        this.deleteOcurrenceDataRecord(record.id)
                      }
                    >
                      <a href="javascript:;">
                        <DeleteOutlined /> Delete
                      </a>
                    </Popconfirm>
                  ),
                },
              ]}
            />
          </Fragment>
        }
      />
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createOcurrenceData: (data, filters) =>
      dispatch(createOcurrenceData(data, filters)),
    getOcurrenceDataFromPipeline: (filters) =>
      dispatch(getOcurrenceDataFromPipeline(filters)),
    deleteOcurrenceDataDuplicates: (id) =>
      dispatch(deleteOcurrenceDataDuplicates(id)),
    deleteOcurrenceDataRecord: (id, record) =>
      dispatch(deleteOcurrenceDataRecord(id, record)),
    cropOcurrenceDataGeographicExtent: (id, data) =>
      dispatch(cropOcurrenceDataGeographicExtent(id, data)),
    queryOcurrenceData: (filters) => dispatch(queryOcurrenceData(filters)),
    limitOcurrenceData: (id, limit) => dispatch(limitOcurrenceData(id, limit)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    enm: state.enm.enm,
    ocurrenceData: state.enm.ocurrenceData,
    ocurrenceCenter: state.enm.ocurrenceCenter,
    loading: state.enm.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ENMOcurrenceData);
