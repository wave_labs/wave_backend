import { Row, Checkbox, Slider, Col } from "antd";
import React, { Component } from "react";
import PageStructure from "./Common/PageStructure";
import SectionInformation from "./Common/SectionInformation";
import FormItem from "./Common/FormItem";

class ENMModel extends Component {
  state = {
    models: [],
    bioclim: false,
    domain: false,
    maxent: false,
    kfold: 37.5,
  };

  handleModels() {
    this.props.setModels({
      models: this.state.models,
      kfold: this.convertKfold(),
    });

    this.props.setStepReady();
  }

  convertKfold() {
    let { kfold } = this.state;

    // Takes this scale converter formula
    // max.new − min.new / max.old − min.old * (x − max.old) + max.new = kfold
    // Replaces new values to scale 0-100 and old values to scale 2-10
    // x = (kfold - 100) / 12.5 + 10;

    return (kfold - 100) / 12.5 + 10;
  }

  handleModelChange = (model) => {
    let { models } = this.state;
    const index = models.indexOf(model);

    if (index > -1) models.splice(index, 1);
    else models.push(model);

    this.setState({ models }, () => this.handleModels());
  };

  render() {
    let { models, kfold } = this.state;

    return (
      <PageStructure
        title="Select Model"
        form={
          <div>
            <SectionInformation
              information="
                  There are a variety of mathematical methods that can be used
                  for fitting, selecting, and evaluating correlative ENMs.
                  Algorithms include profile methods, which are simple
                  statistical techniques that use e.g. environmental distance to
                  known sites of occurrence such as BIOCLIM and DOMAIN;
                  regression methods (e.g. forms of generalized linear models);
                  and machine learning methods such as maximum entropy (MAXENT).
                  Current included algorithms used in ENM can be seen in the
                  list below.
                "
            />

            <FormItem
              label="k-fold partitioning of a data set for model testing purposes.
                  Each record in a matrix (or similar data structure) is
                  randomly assigned to a group"
            >
              <Row type="flex" justify="center">
                <Col xs={24} lg={12}>
                  <Slider
                    step={null}
                    marks={{
                      0: "2",
                      12.5: "3",
                      25: "4",
                      37.5: "5",
                      50: "6",
                      62.5: "7",
                      75: "8",
                      87.5: "9",
                      100: "10",
                    }}
                    tipFormatter={null}
                    value={kfold}
                    onChange={(e) =>
                      this.setState({ kfold: e }, () =>
                        this.props.setModels({
                          kfold: this.convertKfold(e),
                        })
                      )
                    }
                  />
                </Col>
              </Row>
            </FormItem>

            <FormItem label="Model(s) / Algorithm(s)">
              <Row type="flex" justify="space-around">
                <Checkbox
                  checked={models.includes(1)}
                  onClick={() => this.handleModelChange(1)}
                >
                  BIOCLIM
                </Checkbox>
                <Checkbox
                  checked={models.includes(2)}
                  onClick={() => this.handleModelChange(2)}
                >
                  DOMAIN
                </Checkbox>
                <Checkbox
                  checked={models.includes(3)}
                  onClick={() => this.handleModelChange(3)}
                >
                  Maximum Entropy (MAXENT)
                </Checkbox>
              </Row>
            </FormItem>
          </div>
        }
        visualize={<div />}
      />
    );
  }
}

export default ENMModel;
