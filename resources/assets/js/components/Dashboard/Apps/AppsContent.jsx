import React from "react";
import TopicListContainer from "../../Common/TopicListContainer";
import { AppsData } from "../../MainPage/Kits/kitsHelper";

const AppsContent = ({ history }) => {
  return (
    <TopicListContainer history={history} topic="Apps" content={AppsData} />
  );
};

export default AppsContent;
