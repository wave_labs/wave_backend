import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import moment from "moment";
import DrawerContainer from "../../Common/DrawerContainer";
import { profileInfo } from "helpers";
import MapBoxMap from "../../Common/MapBoxMap";
import MapPin from "../../Common/MapPin";
import { Marker } from "react-map-gl";
import { updateDrawerDimensions } from "helpers";
import LitterGallery from "./LitterGallery";
import ModalImage from "../../../Common/ModalImage";

const MapContainer = styled.div`
  width: 100%;
  margin-bottom: 30px;
  height: 400px;
`;

const StyledRow = styled(Row)`
  flex-direction: column;
`;

const StyledDiv = styled.div`
  font-size: 14;
  line-height: 22px;
  margin-bottom: 10px;
  color: rgba(0, 0, 0, 0.65);
`;

const StyledParagraph = styled.span`
  margin-right: 8px;
  display: inline-block;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.85);
  &:after {
    content: ":";
  }
`;

const DescriptionItem = ({ span, title, content }) => (
  <Col span={span}>
    <StyledDiv>
      <StyledParagraph>{title}</StyledParagraph>
      {content}
    </StyledDiv>
  </Col>
);

class LitterDrawer extends Component {
  state = {
    drawerWidth: "400px",
    visibleModal: false,
  };

  updateDimensions() {
    this.setState({ drawerWidth: updateDrawerDimensions(window) });
  }

  componentDidMount = () => {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  onUpdateClick = (sighting) => {
    this.props.setCurrentSighting(sighting);
    this.props.startEditing();
  };

  handleModal = (image = null) => {
    this.setState({
      visibleModal: image && true,
      currentImage: image ? image : null,
    });
  };

  render() {
    let { content, position } = this.props;
    let { visibleModal, currentImage, drawerWidth } = this.state;

    return (
      <DrawerContainer
        handleDrawerClose={this.props.handleDrawerClose}
        visible={this.props.visible}
        width={drawerWidth}
      >
        <ModalImage
          src={currentImage}
          visible={visibleModal}
          handleModal={this.handleModal}
        />

        {content && (
          <StyledRow type="flex">
            {position.lat && position.lng && (
              <MapContainer>
                <MapBoxMap defaultCenter={position} fxeidSize={true}>
                  <Marker longitude={position.lng} latitude={position.lat}>
                    <MapPin />
                  </Marker>
                </MapBoxMap>
              </MapContainer>
            )}

            <Row type="flex" align="top">
              <DescriptionItem span={12} title="Id" content={content.id} />

              <DescriptionItem
                span={12}
                title="User"
                content={profileInfo(content.userable.user.name)}
              />

              <DescriptionItem
                span={12}
                title="Category"
                content={
                  content.litter_category.length > 0 ? (
                    <ul>
                      {Object.values(content.litter_category).map(function(
                        element,
                        index
                      ) {
                        return (
                          <li key={index}>
                            <span>
                              {element.name ? element.name : "Not attributed"}
                            </span>
                          </li>
                        );
                      })}
                    </ul>
                  ) : (
                    "-----"
                  )
                }
              />

              <DescriptionItem
                span={12}
                title="Litter | OSPAR | UNEP"
                content={
                  content.litter_sub_category.length > 0 ? (
                    <ul>
                      {Object.values(content.litter_sub_category).map(function(
                        element,
                        index
                      ) {
                        return (
                          <li key={index}>
                            <span>{element.name}</span>
                            <span>
                              {element.OSPAR_code
                                ? " | " + element.OSPAR_code + " | "
                                : " | Not attributted | "}
                            </span>
                            <span>
                              {element.UNEP_code
                                ? element.UNEP_code
                                : "Not attributted"}
                            </span>
                          </li>
                        );
                      })}
                    </ul>
                  ) : (
                    "-----"
                  )
                }
              />

              <DescriptionItem
                span={12}
                title="Quantity"
                content={profileInfo(content.quantity)}
              />

              <DescriptionItem
                span={12}
                title="Radius"
                content={profileInfo(content.radius)}
              />

              <DescriptionItem
                span={12}
                title="Source"
                content={profileInfo(content.source)}
              />

              <DescriptionItem
                span={12}
                title="Date"
                content={profileInfo(
                  moment(content.date).format("dddd, MMMM Do YYYY")
                )}
              />
            </Row>

            <LitterGallery
              handleModal={this.handleModal}
              gallery={content.photo}
            />
          </StyledRow>
        )}
      </DrawerContainer>
    );
  }
}

export default LitterDrawer;
