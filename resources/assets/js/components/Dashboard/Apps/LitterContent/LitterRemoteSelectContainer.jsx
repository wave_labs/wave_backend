import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchLitter } from "redux-modules/litter/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class LitterRemoteSelectContainer extends Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchLitters(1, { search });
  };

  render() {
    const { data, loading, value, onChange, mode, children } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Litter"
        mode={mode}
      >
        {children}
        {data.map((d) => (
          <Option value={d.id} key={d.id}>
            {d.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchLitter: (page, filters) => dispatch(fetchLitter(page, filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.litter.data,
    loading: state.litter.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LitterRemoteSelectContainer);
