import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Select, message } from "antd";
import styled from "styled-components";
import moment from "moment";
import {
    fetchLittersCoord,
    fetchLitter,
    exportLitter,
} from "redux-modules/litter/actions";
import { startCreating } from "redux-modules/editCreateModal/actions";
import FilterRow from "../../Common/FilterRow";
import ExportMessage from "../../Common/ExportMessage";

const { Option } = Select;
message.config({
    maxCount: 1,
});

const StyledRow = styled(Row)`
  margin: 1rem;
`;

class LitterFilters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            operations: ["reset"],
            messageStatus: null,
            filters: {
                self: true,
                search: undefined,
                source: undefined,
                date: undefined,
            },
        };
    }

    componentDidMount() {
        if (this.props.isAdmin) {
            let { operations } = this.state;
            operations.push("create", "download");
            this.setState({ operations });
        }
    }

    handleFilterChange = (filter, value) => {
        var { filters } = this.state;
        filters[filter] = value;
        this.setState({ filters });
    };

    handleReset = () => {
        this.setState({
            filters: {
                search: undefined,
                date: undefined,
                self: true,
                source: undefined,
            },
        });
    };

    handleDownload = () => {
        this.setState({ messageStatus: "loading" });
        var filters = this.handleFilters();

        this.props.exportLitter(filters).then(() => {
            this.setState({ messageStatus: "success" });
        });
    };

    handleFilters = () => {
        var { search, date, source } = this.state.filters;
        let newDate = date ? [...date] : [];

        return {
            self: true,
            search: search,
            source: source,
            date:
                newDate.length > 0
                    ? [
                        moment(newDate[0]).format("YYYY-MM-DD"),
                        moment(newDate[1]).format("YYYY-MM-DD"),
                    ]
                    : newDate,
        };
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState !== this.state) {
            var filters = this.handleFilters();

            this.props.fetchLittersCoord(filters);
            this.props.fetchLitter(1, filters);
            this.props.handleFilterChange(filters);
        }
    }

    render() {
        var { messageStatus, filters, operations } = this.state;

        return (
            <StyledRow>
                <ExportMessage message={messageStatus} />
                <FilterRow
                    filters={["search", "date"]}
                    filterValues={filters}
                    operations={operations}
                    handleFilterChange={this.handleFilterChange}
                    handleCreate={() => this.props.startCreating()}
                    handleReset={() => this.handleReset()}
                    handleDownload={() => this.handleDownload()}
                    additionalFilters={[
                        <Select
                            placeholder="Source"
                            value={filters.source}
                            style={{ width: "100%" }}
                            onChange={(value) => this.handleFilterChange("source", value)}
                        >
                            <Option value="core">Core</Option>
                            <Option value="beach">Beach</Option>
                            <Option value="seafloor">Seafloor</Option>
                            <Option value="floating">Floating</Option>
                            <Option value="micro">Micro</Option>
                            <Option value="biota">Biota</Option>
                            <Option value="dive">Dive</Option>
                        </Select>,
                    ]}
                />
            </StyledRow>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchLittersCoord: (filters) => dispatch(fetchLittersCoord(filters)),
        fetchLitter: (page, filters) => dispatch(fetchLitter(page, filters)),
        startCreating: () => dispatch(startCreating()),
        exportLitter: (filters) => dispatch(exportLitter(filters)),
    };
};
const mapStateToProps = (state) => {
    return {
        isAdmin: state.auth.isAdmin,
        loadingExport: state.litter.loadingExport,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LitterFilters);
