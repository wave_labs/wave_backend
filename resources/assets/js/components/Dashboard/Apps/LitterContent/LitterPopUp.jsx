import React, { Component } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { formatPosition } from "helpers";

const StyledImg = styled.img`
  width: 100%;
`;

const StyledContainer = styled.div`
  max-width: 200px;
`;

const StyledBold = styled.b`
  font-weight: bold;
`;
const StyledDate = styled.div`
  font-size: 0.8em;
`;

const LitterPopUp = ({ litter, viewMore }) => {
  return (
    <StyledContainer>
      <div>
        {litter.photo[0] && (
          <div>
            <StyledImg
              src={`${window.location.origin}/storage/uploaded/photo/litter/${
                litter.photo[0].url
              }`}
            />
          </div>
        )}
      </div>
      <br />
      {Object.values(litter.litter_category).map(function(element, index) {
        return (
          <React.Fragment key={index}>
            <StyledBold>{element.name}</StyledBold>
            {index != litter.litter_category.length - 1 && (
              <StyledBold>, </StyledBold>
            )}
          </React.Fragment>
        );
      })}
      <div />

      <StyledDate>{litter.date}</StyledDate>
    </StyledContainer>
  );
};

export default LitterPopUp;
