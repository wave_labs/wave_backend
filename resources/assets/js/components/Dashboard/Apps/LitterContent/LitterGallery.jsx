import React from "react";
import styled from "styled-components";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { getCarouselBreakpoints } from "helpers";

const GalleryContainer = styled.div`
  margin: auto;
  margin-top: 30px;
  width: 100%;
`;

const StyledImage = styled.img`
  height: auto;
  width: 90%;
  cursor: pointer;
`;

const imagePath = `${window.location.origin}/storage/uploaded/photo/litter/`;

const LitterGallery = ({ gallery, handleModal }) => {
  function handleImageClick(e) {
    handleModal(e.target.src);
  }

  return (
    <GalleryContainer>
      <Carousel responsive={getCarouselBreakpoints([1, 2, 2, 2, 2])}>
        {Object.values(gallery).map(function(element, index) {
          return (
            <StyledImage
              src={imagePath + element.url}
              key={index}
              onClick={handleImageClick}
            />
          );
        })}
      </Carousel>
    </GalleryContainer>
  );
};

export default LitterGallery;
