import React, { Component } from "react";
import {
  fetchLitter,
  deleteLitter,
  setCurrentLitter,
} from "redux-modules/litter/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { NoDataMessage, formatPosition } from "helpers";
import moment from "moment";
import StopPropagation from "../../Common/StopPropagation";
import LitterDrawer from "./LitterDrawer";

class LitterTableContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      drawerContent: null,
      mapCoordinates: null,
    };
    this.filters = {
      self: true,
    };
    this.columns = [
      {
        title: "Category",
        dataIndex: "litter_category",
        width: "40%",
        render: (litter_category) => (
          <span>
            {litter_category.length > 0
              ? litter_category.map((c, index) => (
                  <span key={"litter-" + index + "-category-" + c.id}>
                    {c.name}
                    {index != litter_category.length - 1 && ", "}
                  </span>
                ))
              : "Not categorized"}
          </span>
        ),
      },
      {
        title: "User",
        dataIndex: "userable.user.name",
      },
      {
        title: "Date",
        dataIndex: "date",
        render: (date) => moment(date).format("YYYY-MM-DD"),
      },
      {
        title: "Quantity",
        dataIndex: "quantity",
        render: (element) => (element ? element : NoDataMessage()),
      },
      {
        title: "Source",
        dataIndex: "source",
      },
      {
        title: "",
        key: "",
        render: (text, litter) => (
          <StopPropagation>
            <RowOperation
              deleteRow
              updateRow
              onUpdateClick={() => this.onUpdateClick(litter)}
              onDeleteConfirm={() => this.props.deleteLitter(litter.id)}
            />
          </StopPropagation>
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.props.fetchLitter(1, this.filters);
  };

  onUpdateClick = (litter) => {
    this.props.setCurrentLitter(litter);
    this.props.startEditing();
  };

  handleRowClick = (record) => {
    this.setState({
      drawerContent: record,
      mapCoordinates: formatPosition(record),
    });
  };

  handleDrawerClose = () => {
    this.setState({
      drawerContent: null,
      mapCoordinates: null,
    });
  };

  handleTableChange = (pagination) => {
    this.props.fetchLitter(pagination.current, this.props.filters);
  };

  render() {
    let { drawerContent, mapCoordinates } = this.state;

    return (
      <div>
        <LitterDrawer
          handleDrawerClose={this.handleDrawerClose}
          visible={drawerContent && true}
          content={drawerContent}
          position={mapCoordinates}
        />

        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          data={this.props.data}
          meta={this.props.meta}
          handleTableChange={this.handleTableChange}
          onRow={(record) => ({
            onClick: () => {
              this.handleRowClick(record);
            },
          })}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchLitter: (page, filters) => dispatch(fetchLitter(page, filters)),
    deleteLitter: (id) => dispatch(deleteLitter(id)),
    activateLitter: (id) => dispatch(activateLitter(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setCurrentLitter: (data) => dispatch(setCurrentLitter(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.litter.data,
    meta: state.litter.meta,
    loading: state.litter.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LitterTableContainer);
