import React, { Component } from "react";
import {
  createLitter,
  updateLitter,
  resetCurrentLitter,
} from "redux-modules/litter/actions";
import { fetchSelector } from "redux-modules/litterCategory/actions";
import { resetModal } from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../Common/EditCreateModal";
import LitterForm from "./LitterForm.jsx";
import moment from "moment";
import { alertActions } from "redux-modules/alert/actions";
import { getErrorMessages } from "helpers";

class LitterModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
    fileList: [],
    sourceType: null,
    options: [],
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, litter) => {
      if (err) {
        return;
      } else {
        let formData = new FormData();
        let categories = this.handleCategories(litter);

        litter = {
          ...litter,
          date: moment(litter.date).format("YYYY-MM-DD"),
          collected: litter.collected == true ? 1 : 0,
          multiple: litter.multiple == true ? 1 : 0,
        };

        if (litter.source != "dive") {
          litter = {
            ...litter,
            photo: this.state.fileList,
          };
        }

        formData = this.handleArrayToFormData(
          formData,
          categories[0],
          "litter_category"
        );

        formData = this.handleArrayToFormData(
          formData,
          categories[1],
          "litter_sub_category"
        );

        Object.entries(litter).map((entry) => {
          if (
            entry[0] != "categories" ||
            entry[0] != "keys" ||
            entry[0] != "photo"
          ) {
            formData.append(`${entry[0]}`, entry[1]);
          }
        });

        formData.append("_method", "PATCH");
        //https://github.com/laravel/framework/issues/13457

        this.setState({ confirmLoading: true });
        this.props
          .updateLitter(this.props.currentLitter.id, formData)
          .then((response) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = getErrorMessages(error.response.data.errors);

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({
      confirmLoading: false,
      sourceType: null,
      fileList: null,
      litter_sub_category: [],
      litter_category: [],
      previewImage: null,
    });
    this.props.resetCurrentLitter();
    this.props.resetModal();
    form.resetFields();
  };

  toggleChildMenu = (e) => {
    this.setState({
      sourceType: e,
    });
  };

  handleCategories = (litter) => {
    let litter_sub_category = [],
      litter_category = [];

    if (litter.keys.length > 0) {
      for (var i = 0; i < litter.keys.length; i++) {
        if (litter.categories[litter.keys[i]]) {
          if (!litter_category.includes(litter.categories[litter.keys[i]][0])) {
            litter_category.push(litter.categories[litter.keys[i]][0]);
          }
          if (!litter_sub_category.includes(litter.keys[i][1])) {
            litter_sub_category.push(litter.categories[litter.keys[i]][1]);
          }
        }
      }
    }

    return [litter_category, litter_sub_category];
  };

  handleArrayToFormData = (formData, array, field) => {
    for (var i = 0; i < array.length; i++) {
      formData.append(`${field}[]`, array[i]);
    }

    return formData;
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, litter) => {
      if (err) {
        return;
      } else {
        let formData = new FormData();
        let categories = this.handleCategories(litter);

        litter = {
          ...litter,
          date: moment(litter.date).format("YYYY-MM-DD"),
          collected: litter.collected == true ? 1 : 0,
          multiple: litter.multiple == true ? 1 : 0,
        };

        if (litter.source != "dive") {
          litter = {
            ...litter,
            photo: this.state.fileList,
          };
        }

        formData = this.handleArrayToFormData(
          formData,
          categories[0],
          "litter_category"
        );

        formData = this.handleArrayToFormData(
          formData,
          categories[1],
          "litter_sub_category"
        );

        Object.entries(litter).map((entry) => {
          if (
            entry[0] != "categories" ||
            entry[0] != "keys" ||
            entry[0] != "photo"
          ) {
            formData.append(`${entry[0]}`, entry[1]);
          }
        });

        this.setState({ confirmLoading: true });
        this.props
          .createLitter(formData)
          .then((response) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = getErrorMessages(error.response.data.errors);

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  handlePhoto = (photo, preview) => {
    this.setState({ fileList: photo, previewImage: preview });
  };

  handleOptions = async () => {
    const { LitterCategorySelector } = this.props;
    let options = [],
      children = [];

    await LitterCategorySelector.forEach((litterCategory) => {
      litterCategory.sub_category.forEach((litterSubCat) => {
        children.push({
          value: litterSubCat.id,
          label: litterSubCat.name,
        });
      });

      options.push({
        value: litterCategory.id,
        label: litterCategory.name,
        children: children,
      });
      children = [];
    });

    this.setState({
      options: options,
    });
  };

  componentDidMount = async () => {
    let source =
      this.props.currentLitter.source == "beach"
        ? "Beach"
        : this.props.currentLitter.source == "floating"
        ? "Floating"
        : null;
    await this.props.fetchSelector({ source });
    this.handleOptions();
  };

  componentDidUpdate(prevProps) {
    if (prevProps.currentLitter != this.props.currentLitter) {
      this.setState({
        previewImage:
          this.props.currentLitter.photo.length > 0
            ? "/storage/uploaded/photo/litter/" +
              this.props.currentLitter.photo[0].url
            : null,
      });
    }
  }

  render() {
    const { editing, creating } = this.props;

    return (
      <EditCreateModal
        titleEdit="Litter"
        titleCreate="Litter"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={editing}
        creating={creating}
      >
        <LitterForm
          creating={creating}
          editing={editing}
          wrappedComponentRef={this.saveFormRef}
          litter={this.props.currentLitter}
          handlePhoto={this.handlePhoto}
          toggleChildMenu={this.toggleChildMenu}
          options={this.state.options}
          previewImage={this.state.previewImage}
          sourceType={
            this.state.sourceType
              ? this.state.sourceType
              : this.props.currentLitter.source
          }
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetCurrentLitter: () => dispatch(resetCurrentLitter()),
    resetModal: () => dispatch(resetModal()),
    createLitter: (data) => dispatch(createLitter(data)),
    updateLitter: (data, id) => dispatch(updateLitter(data, id)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    currentLitter: state.litter.current,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
    LitterCategorySelector: state.litterCategory.LitterCategorySelector,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LitterModalContainer);
