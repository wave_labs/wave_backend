import React, { Component } from "react";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Upload,
  InputNumber,
  Select,
  Checkbox,
  DatePicker,
  Button,
  Icon,
  Cascader,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import moment from "moment";
import StopPropagation from "../../Common/StopPropagation";
import { getBase64, dummyRequest } from "helpers";

const FormItem = Form.Item;
const Option = Select.Option;

const StyledImage = styled.img`
  width: 100%;
  cursor: pointer;

  &:hover {
    filter: blur(1px) brightness(80%);
  }
`;

const StyledInputNumber = styled(InputNumber)`
  min-width: 100% !important;
`;

const StyledFormItem = styled(FormItem)`
  position: relative;
`;
const StyledIcon = styled(Icon)`
  right: 8px;
  top: 4px;
  position: absolute;
`;

class LitterForm extends Component {
  state = {
    loading: false,
    keys: [],
    categories: [],
    id: 0,
  };

  remove = (k) => {
    const { form } = this.props;
    const keys = form.getFieldValue("keys");
    this.setState({ keys: keys.filter((key) => key !== k) });
    form.setFieldsValue({
      keys: keys.filter((key) => key !== k),
    });
  };

  handleChange = (info) => {
    getBase64(info.file.originFileObj, (imageUrl) =>
      this.props.handlePhoto(info.file.originFileObj, imageUrl)
    );
  };

  handleSource = (e) => {
    this.props.toggleChildMenu(e);
  };

  add = () => {
    const { form } = this.props;
    const { id } = this.state;
    const keys = form.getFieldValue("keys");
    const nextKeys = keys.concat(id + 1);
    this.setState({ id: id + 1, keys: nextKeys });
    form.setFieldsValue({
      keys: nextKeys,
    });
  };

  handleEditOpen(litter) {
    let categories = [],
      keys = [];

    Object.values(litter.litter_sub_category).map((element, index) => {
      keys.push(index);
      categories.push([element.category, element.id]);
    });

    this.setState({
      keys: keys,
      categories: categories,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      if (!nextProps.editing && !nextProps.creating) {
        this.setState({
          keys: [],
          categories: [],
          id: 0,
        });
      }

      nextProps.editing && this.handleEditOpen(nextProps.litter);
    }
  }

  componentDidMount() {
    this.props.editing && this.handleEditOpen(this.props.litter);
    let size = this.props.litter.litter_sub_category.length;

    this.setState({
      id: size ? size : 0,
    });
  }

  rules = {
    date: [
      {
        required: true,
        message: "Date is required",
      },
    ],
    source: [
      {
        required: true,
        message: "Source is required",
      },
    ],
    latitude: [
      {
        required: true,
        message: "Latitude is required",
      },
    ],
    longitude: [
      {
        required: true,
        message: "Longitude is required",
      },
    ],
  };

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { litter, sourceType, options, previewImage } = this.props;

    getFieldDecorator("keys", { initialValue: this.state.keys });
    const keys = getFieldValue("keys");

    const litterSubCategories = keys.map((k) => (
      <StyledFormItem key={"litter-" + k}>
        {getFieldDecorator(`categories[${k}]`, {
          validateTrigger: ["onChange", "onBlur"],
          initialValue: this.state.categories[k],
        })(
          <Cascader
            options={options}
            allowClear={false}
            expandTrigger="hover"
            suffixIcon={<div />}
          />
        )}
        {
          <StopPropagation>
            <StyledIcon
              key={k}
              className="dynamic-delete-button"
              type="minus-circle-o"
              onClick={() => this.remove(k)}
            />
          </StopPropagation>
        }
      </StyledFormItem>
    ));

    return (
      <Form hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Source">
              {getFieldDecorator("source", {
                initialValue: litter.source,
                rules: this.rules.source,
              })(
                <Select placeholder="Source" onChange={this.handleSource}>
                  <Option value="beach">Beach</Option>
                  <Option value="floating">Floating</Option>
                  <Option value="dive">Dive</Option>
                </Select>
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Date">
              {getFieldDecorator("date", {
                rules: this.rules.date,
                initialValue: litter.date ? moment(litter.date) : null,
              })(<DatePicker style={{ width: "100%" }} />)}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Latitude">
              {getFieldDecorator("latitude", {
                initialValue: litter.latitude,
                rules: this.rules.latitude,
              })(<StyledInputNumber placeholder="Latitude" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Longitude">
              {getFieldDecorator("longitude", {
                initialValue: litter.longitude,
                rules: this.rules.longitude,
              })(<StyledInputNumber placeholder="Longitude" />)}
            </FormItem>
          </Col>

          {(sourceType == "beach" || sourceType == "floating") && (
            <Col xs={24}>
              <Row type="flex" justify="center">
                <FormItem style={{ width: "60%" }}>
                  <Upload
                    customRequest={dummyRequest}
                    onChange={this.handleChange}
                    style={{ width: "100%", display: "block" }}
                    fileList={false}
                  >
                    {previewImage ? (
                      <StyledImage src={previewImage} />
                    ) : (
                      <Button style={{ width: "100%" }}>
                        <UploadOutlined /> Upload photo
                      </Button>
                    )}
                  </Upload>
                </FormItem>
              </Row>
            </Col>
          )}

          {sourceType == "beach" && (
            <React.Fragment>
              <Col xs={24} sm={12}>
                <FormItem hasFeedback label="Quantity">
                  {getFieldDecorator("quantity", {
                    initialValue: litter.quantity,
                    rules: this.rules.name,
                  })(
                    <Select placeholder="Quantity of litter">
                      <Option value="1-5">1-5</Option>
                      <Option value="5-10">5-10</Option>
                      <Option value="10-50">10-50</Option>
                      <Option value=">50">{">"}50</Option>
                    </Select>
                  )}
                </FormItem>
              </Col>

              <Col xs={24} sm={12}>
                <FormItem hasFeedback label="Radius">
                  {getFieldDecorator("radius", {
                    initialValue: litter.radius,
                    rules: this.rules.radius,
                  })(
                    <Select placeholder="Radius of litter">
                      <Option value="<1">{"<"}1</Option>
                      <Option value="1-5">1-5</Option>
                      <Option value="5-20">5-20</Option>
                      <Option value=">20">{">"}20</Option>
                    </Select>
                  )}
                </FormItem>
              </Col>
            </React.Fragment>
          )}

          {sourceType && (
            <React.Fragment>
              <Col xs={24}>{litterSubCategories}</Col>

              <Col xs={24}>
                <FormItem>
                  <Button
                    type="dashed"
                    onClick={this.add}
                    style={{ width: "100%" }}
                  >
                    <Icon type="plus" /> Categorize
                  </Button>
                </FormItem>
              </Col>
            </React.Fragment>
          )}
        </Row>
        <FormItem>
          {getFieldDecorator("collected", {
            initialValue: litter.collected,
            valuePropName: "checked",
          })(<Checkbox> Collected </Checkbox>)}
        </FormItem>

        {sourceType == "floating" && (
          <FormItem>
            {getFieldDecorator("multiple", {
              initialValue: litter.multiple,
              valuePropName: "checked",
            })(<Checkbox> Multiple objects </Checkbox>)}
          </FormItem>
        )}
      </Form>
    );
  }
}

export default Form.create()(LitterForm);
