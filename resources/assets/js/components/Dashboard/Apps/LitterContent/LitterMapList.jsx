import React, { Component } from "react";
import { connect } from "react-redux";
import MapBoxMap from "../../Common/MapBoxMap";
import { Marker, Popup } from "react-map-gl";
import { fetchLittersCoord } from "redux-modules/litter/actions";
import LitterPopUp from "./LitterPopUp";

class LittersMapList extends Component {
  constructor(props) {
    super(props);
    this.filters = {
      self: true,
    };
  }
  state = {
    litterPopUp: null,
  };
  componentDidMount() {
    this.props.fetchLittersCoord(this.filters);
  }

  showLitter = () => {
    const { litterPopUp } = this.state;

    return (
      litterPopUp && (
        <Popup
          tipSize={5}
          longitude={parseFloat(litterPopUp.longitude)}
          latitude={parseFloat(litterPopUp.latitude)}
          closeOnClick={false}
          onClose={() => this.setState({ litterPopUp: null })}
        >
          <LitterPopUp viewMore={this.props.viewMore} litter={litterPopUp} />
        </Popup>
      )
    );
  };

  getMarkerLink = () =>
    `${window.location.origin}/storage/images/map-markers/19.png`;

  render() {
    return (
      <div className="map-container">
        <MapBoxMap zoom={9}>
          {this.props.littersByCoord.map((litter) => (
            <Marker
              key={litter.id}
              longitude={parseFloat(litter.longitude)}
              latitude={parseFloat(litter.latitude)}
            >
              <img
                onClick={() => this.setState({ litterPopUp: litter })}
                src={this.getMarkerLink()}
              />
            </Marker>
          ))}

          {this.showLitter()}
        </MapBoxMap>
      </div>
    );
  }
}

export default connect(
  (state) => ({ littersByCoord: state.litter.littersByCoord }),
  { fetchLittersCoord }
)(LittersMapList);
