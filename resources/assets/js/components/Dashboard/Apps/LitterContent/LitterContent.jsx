import React, { Component } from "react";
import LitterMapList from "./LitterMapList";
import LitterModalContainer from "./LitterModalContainer";
import LitterTableContainer from "./LitterTableContainer";
import LitterFilters from "./LitterFilters";
import LitterGraphContainer from "./LitterGraphContainer";
import { Row, Col } from "antd";
import { connect } from "react-redux";

class LitterContent extends Component {
    state = {
        filters: [],
    };

    handleFilterChange = (newFilters) => {
        this.setState({ filters: newFilters });
    };

    render() {
        return (
            <div>
                <LitterModalContainer />
                {this.props.isAdmin != undefined && (
                    <LitterFilters
                        handleFilterChange={(newFilters) =>
                            this.handleFilterChange(newFilters)
                        }
                    />
                )}

                <Row type="flex" gutter={24}>
                    <Col xs={24} lg={12}>
                        <LitterMapList viewMore />
                    </Col>
                    <Col xs={24} lg={12}>
                        <LitterTableContainer filters={this.state.filters} />
                        <LitterGraphContainer />
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isAdmin: state.auth.isAdmin,
    };
};

export default connect(
    mapStateToProps,
    null
)(LitterContent);
