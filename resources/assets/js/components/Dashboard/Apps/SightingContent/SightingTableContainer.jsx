import React, { Component } from "react";
import {
    fetchSightings,
    deleteSighting,
    fetchSighting,
    setCurrentSighting,
    resetCurrentSighting,
} from "redux-modules/sighting/actions";
import { connect } from "react-redux";
import TablePagination from "../../Common/TablePagination";
import RowOperation from "../../Common/RowOperation";
import {
    startCreating,
    startEditing,
} from "redux-modules/editCreateModal/actions";
import { withRouter } from "react-router-dom";
import { NoDataMessage, formatPosition } from "helpers";
import moment from "moment";
import SightingDrawer from "./SightingDrawer";
import StopPropagation from "../../Common/StopPropagation";

class SightingTableFilterContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drawerContent: null,
            mapCoordinates: null,
        };
        this.filters = {
            self: true,
        };
        this.columns = [
            {
                title: "Creature",
                dataIndex: "creature",
                render: (creature) => `${creature.name}`,
            },
            {
                title: "Date",
                dataIndex: "date",
                render: (date) => moment(date).format("YYYY-MM-DD"),
            },
            {
                title: "Behaviour",
                dataIndex: "behaviour",
                render: (behaviour) =>
                    behaviour ? `${behaviour.behaviour.en}` : NoDataMessage(),
            },
            {
                title: "Boat",
                dataIndex: "vehicle",
                render: (vehicle) => (vehicle ? `${vehicle.vehicle.en}` : NoDataMessage()),
            },
            {
                title: "",
                key: "",
                render: (sighting) => (
                    <StopPropagation>
                        <RowOperation
                            onUpdateClick={() => this.onUpdateClick(sighting)}
                            onDeleteConfirm={() => this.props.deleteSighting(sighting.id)}
                        />
                    </StopPropagation>
                ),
            },
        ];
    }

    componentDidMount = () => {
        this.props.fetchSightings(1, this.filters);
    };

    componentDidUpdate(prevProps) {
        if (prevProps.activeDrawer != this.props.activeDrawer) {
            this.props.fetchSighting(this.props.activeDrawer).then((response) => {
                this.handleDrawerContent(response.action.payload.data.data[0]);
            });
        }
    }

    onUpdateClick = (sighting) => {
        this.props.setCurrentSighting(sighting);
        this.props.startEditing();
    };

    handleTableChange = (pagination) => {
        this.props.fetchSightings(pagination.current, this.props.filters);
    };

    handleDrawerContent = (record) => {
        this.setState({
            drawerContent: record,
            mapCoordinates: formatPosition(record),
        });
    };

    handleDrawerClose = () => {
        this.props.resetCurrentSighting();
        this.setState({
            drawerContent: null,
            mapCoordinates: null,
        });
    };

    render() {
        let { drawerContent, mapCoordinates } = this.state;

        return (
            <div>
                <SightingDrawer
                    handleDrawerClose={this.handleDrawerClose}
                    visible={drawerContent && true}
                    content={drawerContent}
                    position={mapCoordinates}
                />
                <TablePagination
                    loading={this.props.loading}
                    columns={this.columns}
                    handleTableChange={this.handleTableChange}
                    data={this.props.data}
                    meta={this.props.meta}
                    onRow={(record) => ({
                        onClick: () => {
                            this.handleDrawerContent(record);
                        },
                    })}
                />
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSightings: (page, filters) => dispatch(fetchSightings(page, filters)),
        deleteSighting: (id) => dispatch(deleteSighting(id)),
        fetchSighting: (id) => dispatch(fetchSighting(id)),
        startCreating: () => dispatch(startCreating()),
        startEditing: () => dispatch(startEditing()),
        setCurrentSighting: (data) => dispatch(setCurrentSighting(data)),
        resetCurrentSighting: () => dispatch(resetCurrentSighting()),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.sighting.data,
        meta: state.sighting.meta,
        loading: state.sighting.loading,
        user: state.auth.user,
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(SightingTableFilterContainer)
);
