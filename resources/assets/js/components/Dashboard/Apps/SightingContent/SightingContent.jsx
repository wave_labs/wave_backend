import React, { Component } from "react";
import SightingModalContainer from "./SightingModalContainer";
import SightingTableContainer from "./SightingTableContainer";
import SightingGraphContainer from "./SightingGraphContainer";
import SightingMapList from "./SightingMapList";
import SightingFilters from "./SightingFilters";
import { Row, Col } from "antd";
import { connect } from "react-redux";

class SightingContent extends Component {
  state = {
    filters: [],
    activeDrawer: null,
  };

  handleFilterChange = (newFilters) => {
    this.setState({ filters: newFilters });
  };

  componentDidMount() {
    console.log(this.props.match.params.id);
    this.setState({ activeDrawer: this.props.match.params.id });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params != this.props.match.params) {
      this.setState({ activeDrawer: this.props.match.params.id });
    }
  }

  render() {
    return (
      <div>
        <SightingModalContainer />
        {this.props.isAdmin != undefined && (
          <SightingFilters
            handleFilterChange={(newFilters) =>
              this.handleFilterChange(newFilters)
            }
          />
        )}

        <Row type="flex" gutter={24}>
          <Col xs={24} lg={12}>
            <SightingMapList viewMore />
          </Col>
          <Col xs={24} lg={12}>
            <SightingTableContainer
              activeDrawer={this.state.activeDrawer}
              filters={this.state.filters}
            />
            <SightingGraphContainer />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(
  mapStateToProps,
  null
)(SightingContent);
