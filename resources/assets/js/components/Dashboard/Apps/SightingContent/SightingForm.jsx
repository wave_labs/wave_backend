import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Select, Form, InputNumber, DatePicker } from "antd";
import moment from "moment";
import CreatureRemoteSelectContainer from "../../Data/CreatureContent/CreatureRemoteSelectContainer";
import BeaufortScaleRemoteSelectContainer from "../../BeaufortScaleContent/BeaufortScaleRemoteSelectContainer";
import VehicleTypeRemoteCascadeContainer from "../../VehicleTypeContent/VehicleTypeRemoteCascadeContainer";
import SightingBehaviourRemoteSelectContainer from "../../SightingBehaviourContent/SightingBehaviourRemoteSelectContainer";

const FormItem = Form.Item;
const Option = Select.Option;

const StyledInputNumber = styled(InputNumber)`
  min-width: 100% !important;
`;

class SightingForm extends Component {
  rules = {
    user: [
      {
        required: true,
        message: "User is required",
      },
    ],
    creatureId: [
      {
        required: true,
        message: "Creature  is required",
      },
    ],
    beaufortScaleId: [
      {
        required: true,
        message: "Beaufort scale is required",
      },
    ],
    date: [
      {
        required: true,
        message: "Date is required",
      },
    ],
    latitude: [
      {
        required: true,
        message: "Latitude is required",
      },
    ],
    longitude: [
      {
        required: true,
        message: "Longitude is required",
      },
    ],
  };

  render() {
    const { sighting, editing } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { creature, vehicle } = sighting;

    return (
      <Form hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Creature">
              {getFieldDecorator("creature_id", {
                initialValue: creature.id,
                rules: this.rules.creatureId,
              })(
                <CreatureRemoteSelectContainer>
                  {editing && (
                    <Option value={creature.id} key={creature.id}>
                      {creature.name}
                    </Option>
                  )}
                </CreatureRemoteSelectContainer>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Beaufort Scale">
              {getFieldDecorator("beaufort_scale_id", {
                initialValue: sighting.beaufort_scale.id,
                rules: this.rules.beaufortScaleId,
              })(
                <BeaufortScaleRemoteSelectContainer>
                  {editing && (
                    <Option
                      value={sighting.beaufort_scale.id}
                      key={sighting.beaufort_scale.id}
                    >
                      {sighting.beaufort_scale.desc}
                    </Option>
                  )}
                </BeaufortScaleRemoteSelectContainer>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Group Type">
              {getFieldDecorator("is_group_mixed", {
                initialValue: sighting.is_group_mixed,
              })(
                <Select placeholder="Group Type">
                  <Option value={0}>Uniform</Option>
                  <Option value={1}>Mixed</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Vehicle">
              {getFieldDecorator("vehicle_id", {
                initialValue: [vehicle.vehicle_type_id, vehicle.id],
              })(<VehicleTypeRemoteCascadeContainer />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Report Source">
              {getFieldDecorator("report_source", {
                initialValue: sighting.report_source,
              })(
                <Select placeholder="Report Source">
                  <Option value="land">Land</Option>
                  <Option value="aerial">Aerial</Option>
                  <Option value="underwater">Underwater</Option>
                  <Option value="surface">Surface</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Date">
              {getFieldDecorator("date", {
                initialValue: sighting.date ? moment(sighting.date) : null,
                rules: this.rules.date,
              })(<DatePicker style={{ width: "100%" }} />)}
            </FormItem>
          </Col>
        </Row>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Behaviour">
              {getFieldDecorator("behaviour_id", {
                initialValue: sighting.behaviour.id,
                rules: this.rules.beaufortScaleId,
              })(
                <SightingBehaviourRemoteSelectContainer>
                  {editing && (
                    <Option
                      value={sighting.behaviour.id}
                      key={sighting.behaviour.id}
                    >
                      {sighting.behaviour.behaviour}
                    </Option>
                  )}
                </SightingBehaviourRemoteSelectContainer>
              )}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <Col xs={24} sm={12}>
              <FormItem hasFeedback label="Latitude">
                {getFieldDecorator("latitude", {
                  initialValue: sighting.latitude,
                  rules: this.rules.latitude,
                })(<StyledInputNumber placeholder="Latitude" />)}
              </FormItem>
            </Col>
            <Col xs={24} sm={12}>
              <FormItem hasFeedback label="Longitude">
                {getFieldDecorator("longitude", {
                  initialValue: sighting.longitude,
                  rules: this.rules.longitude,
                })(<StyledInputNumber placeholder="Longitude" />)}
              </FormItem>
            </Col>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Group Size">
              {getFieldDecorator("group_size", {
                initialValue: sighting.group_size,
              })(<StyledInputNumber placeholder="Group size" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Calves">
              {getFieldDecorator("calves", {
                initialValue: sighting.calves,
              })(
                <StyledInputNumber
                  style={{ width: "100%" }}
                  placeholder="Calves"
                />
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(SightingForm);
