import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const StyledImg = styled.img`
  max-width: 200px !important;
  margin: 0.75rem;
`;

const StyledBold = styled.b`
  font-weight: bold;
`;

const SightingPopUp = ({ sighting, viewMore }) => {
  return (
    <div>
      <div>
        <StyledBold>Creature: </StyledBold>
        <span>{sighting.creature.name}</span>
      </div>
      <div>
        <StyledBold>Sighted at: </StyledBold>
        <span>{sighting.date}</span>
      </div>

      <Link to={`/dashboard/apps/whaleReporter/${sighting.id}`}>View More</Link>
    </div>
  );
};

export default SightingPopUp;
