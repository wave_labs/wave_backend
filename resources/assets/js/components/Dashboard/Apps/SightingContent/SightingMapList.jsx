import React, { Component } from "react";
import { connect } from "react-redux";
import MapBoxMap from "../../Common/MapBoxMap";
import { Marker, Popup } from "react-map-gl";
import { fetchSightingsCoord } from "redux-modules/sighting/actions";
import SightingPopUp from "./SightingPopUp";

class SightingMapList extends Component {
  constructor(props) {
    super(props);
    this.filters = {
      self: true,
    };
  }
  state = {
    sightingPopUp: null,
  };
  componentDidMount() {
    this.props.fetchSightingsCoord(this.filters);
  }

  showSighting = () => {
    const { sightingPopUp } = this.state;

    return (
      sightingPopUp && (
        <Popup
          tipSize={5}
          longitude={parseFloat(sightingPopUp.longitude)}
          latitude={parseFloat(sightingPopUp.latitude)}
          closeOnClick={false}
          onClose={() => this.setState({ sightingPopUp: null })}
        >
          <SightingPopUp
            viewMore={this.props.viewMore}
            sighting={sightingPopUp}
          />
        </Popup>
      )
    );
  };
  getMarkerLink = (id) =>
    `${window.location.origin}/storage/images/map-markers/32.png`;

  render() {
    return (
      <div className="map-container">
        <MapBoxMap zoom={9}>
          {this.props.sightingsByCoord.map((sighting) => (
            <Marker
              key={sighting.id}
              longitude={parseFloat(sighting.longitude)}
              latitude={parseFloat(sighting.latitude)}
            >
              <img
                onClick={() => this.setState({ sightingPopUp: sighting })}
                src={this.getMarkerLink(sighting.creature.id)}
              />
            </Marker>
          ))}

          {this.showSighting()}
        </MapBoxMap>
      </div>
    );
  }
}

export default connect(
  (state) => ({ sightingsByCoord: state.sighting.sightingsByCoord }),
  { fetchSightingsCoord }
)(SightingMapList);
