import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchAnalyticSighting,
  fetchAnalyticSightingCreature,
} from "redux-modules/analytic/actions";
import moment from "moment";
import { Row } from "antd";
import styled from "styled-components";
import LineChart from "../../Common/Graphs/LineChart";
import PieChart from "../../Common/Graphs/PieChart";

const GraphContainer = styled(Row)`
  display: flex;
  justify-content: space-around;
  align-items: center;
  min-height: 300px;
  margin: 50px auto;
`;

class SightingGraphContainer extends Component {
  state = {
    yAxisChartLine: null,
    xAxisChartLine: null,
    loadingAnalyticSighting: true,

    labelsChartPie: null,
    datasetChartPie: null,
    loadingAnalyticSightingCreature: true,
    hasData: false,
  };

  filters = {
    date: moment().format("YYYY-MM"),
    self: true,
  };

  handleArrayPush(element, ignoreNull) {
    let xAxis = [];
    let yAxis = [];

    Object.entries(element).map((e) => {
      if (ignoreNull || e[1] > 0) {
        xAxis.push(e[0]);
        yAxis.push(e[1]);
      }
    });

    return [xAxis, yAxis, false, yAxis.length > 0 && true];
  }

  handleDate = (newDate) => {
    let chartPieData = [];

    this.filters = {
      ...this.filters,
      date: newDate.format("YYYY-MM"),
    };

    this.setState({
      labelsChartPie: null,
      datasetChartPie: null,
      loadingAnalyticSightingCreature: true,
    });

    this.props.fetchAnalyticSightingCreature(this.filters).then((response) => {
      Object.values(response.action.payload.data.data.xAxis).map((element) => {
        chartPieData = this.handleArrayPush(element, false);
      });

      this.setState({
        labelsChartPie: chartPieData[0],
        datasetChartPie: chartPieData[1],
        loadingAnalyticSightingCreature: chartPieData[2],
        hasData: chartPieData[3],
      });
    });
  };

  componentDidMount() {
    this.filters = {
      ...this.filters,
      date: moment().format("YYYY-MM"),
    };

    let chartPieData = [];
    let chartLineData = [];

    this.props.fetchAnalyticSightingCreature(this.filters).then((response) => {
      Object.values(response.action.payload.data.data.xAxis).map((element) => {
        chartPieData = this.handleArrayPush(element, false);
      });
    });

    this.props.fetchAnalyticSighting(this.filters).then((response) => {
      chartLineData = this.handleArrayPush(
        response.action.payload.data.data.xAxis,
        true
      );

      this.setState({
        labelsChartPie: chartPieData[0],
        datasetChartPie: chartPieData[1],
        loadingAnalyticSightingCreature: chartPieData[2],
        hasData: chartPieData[3],

        xAxisChartLine: chartLineData[0],
        yAxisChartLine: chartLineData[1],
        loadingAnalyticSighting: chartLineData[2],
      });
    });
  }

  render() {
    let {
      xAxisChartLine,
      yAxisChartLine,
      loadingAnalyticSighting,
      labelsChartPie,
      datasetChartPie,
      loadingAnalyticSightingCreature,
      hasData,
    } = this.state;

    return (
      <Row>
        <GraphContainer style={{ marginTop: "10px" }}>
          <PieChart
            loading={loadingAnalyticSightingCreature}
            labelsChartPie={labelsChartPie}
            datasetChartPie={datasetChartPie}
            hasData={hasData}
            handleDate={this.handleDate}
            aDate={this.filters.date}
            title="Number of reports per creature"
          />
        </GraphContainer>

        <GraphContainer>
          <LineChart
            xAxis={xAxisChartLine}
            yAxis={yAxisChartLine}
            loading={loadingAnalyticSighting}
            title="Number of reports per month"
            yLabel="Reports Frequency"
            xLabel="Months"
          />
        </GraphContainer>
      </Row>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAnalyticSighting: (filters) =>
      dispatch(fetchAnalyticSighting(filters)),
    fetchAnalyticSightingCreature: (filters) =>
      dispatch(fetchAnalyticSightingCreature(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    analyticSighting: state.analytic.analyticSighting,
    analyticSightingCreature: state.analytic.analyticSightingCreature,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SightingGraphContainer);
