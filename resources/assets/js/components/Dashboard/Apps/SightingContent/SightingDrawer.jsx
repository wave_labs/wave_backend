import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import moment from "moment";
import DrawerContainer from "../../Common/DrawerContainer";
import { profileInfo } from "helpers";
import MapBoxMap from "../../Common/MapBoxMap";
import MapPin from "../../Common/MapPin";
import { Marker } from "react-map-gl";
import { updateDrawerDimensions } from "helpers";

const MapContainer = styled.div`
  width: 100%;
  margin-bottom: 30px;
  height: 400px;
`;

const StyledRow = styled(Row)`
  flex-direction: column;
`;

const StyledDiv = styled.div`
  font-size: 14;
  line-height: 22px;
  margin-bottom: 10px;
  color: rgba(0, 0, 0, 0.65);
`;

const StyledParagraph = styled.span`
  margin-right: 8px;
  display: inline-block;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.85);
  &:after {
    content: ":";
  }
`;

const DescriptionItem = ({ span, title, content }) => (
    <Col span={span}>
        <StyledDiv>
            <StyledParagraph>{title}</StyledParagraph>
            {content}
        </StyledDiv>
    </Col>
);

class SightingDrawer extends Component {
    state = {
        drawerWidth: "400px",
    };

    updateDimensions() {
        this.setState({ drawerWidth: updateDrawerDimensions(window) });
    }

    componentDidMount = () => {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }

    onUpdateClick = (sighting) => {
        this.props.setCurrentSighting(sighting);
        this.props.startEditing();
    };

    render() {
        let { content, position } = this.props;

        return (
            <DrawerContainer
                handleDrawerClose={this.props.handleDrawerClose}
                visible={this.props.visible}
                width={this.state.drawerWidth}
            >
                {content && (
                    <StyledRow type="flex">
                        {position.lat && position.lng && (
                            <MapContainer>
                                <MapBoxMap defaultCenter={position} fxeidSize={true}>
                                    <Marker longitude={position.lng} latitude={position.lat}>
                                        <MapPin />
                                    </Marker>
                                </MapBoxMap>
                            </MapContainer>
                        )}

                        <Row type="flex" align="top">
                            <DescriptionItem span={12} title="Id" content={content.id} />

                            <DescriptionItem
                                span={12}
                                title="Capsule"
                                content={profileInfo(content.capsule_id)}
                            />

                            <DescriptionItem
                                span={12}
                                title="User"
                                content={profileInfo(content.userable.user.name)}
                            />

                            <DescriptionItem
                                span={12}
                                title="Creature"
                                content={profileInfo(content.creature.name)}
                            />

                            <DescriptionItem
                                span={12}
                                title="Beaufort Scale"
                                content={profileInfo(
                                    content.beaufort_scale &&
                                    content.beaufort_scale.scale +
                                    ", " +
                                    content.beaufort_scale.desc
                                )}
                            />
                            <DescriptionItem
                                span={12}
                                title="Date"
                                content={profileInfo(
                                    moment(content.date).format("dddd, MMMM Do YYYY")
                                )}
                            />

                            <DescriptionItem
                                span={12}
                                title="Sea conditions"
                                content={profileInfo(
                                    content.beaufort_scale &&
                                    content.beaufort_scale.sea_conditions
                                )}
                            />

                            <DescriptionItem
                                span={12}
                                title="Land conditions"
                                content={profileInfo(
                                    content.beaufort_scale &&
                                    content.beaufort_scale.land_conditions
                                )}
                            />

                            <DescriptionItem
                                span={12}
                                title="Coordinates"
                                content={profileInfo(
                                    content.latitude + ", " + content.longitude
                                )}
                            />

                            <DescriptionItem
                                span={12}
                                title="Group"
                                content={profileInfo(
                                    content.is_group_mixed ? "Mixed" : "Uniform"
                                )}
                            />

                            <DescriptionItem
                                span={12}
                                title="Group size"
                                content={profileInfo(content.group_size)}
                            />

                            <DescriptionItem
                                span={12}
                                title="Behaviour"
                                content={profileInfo(content.behaviour.behaviour.en)}
                            />

                            <DescriptionItem
                                span={12}
                                title="Calves"
                                content={profileInfo(content.calves)}
                            />

                            <DescriptionItem
                                span={12}
                                title="Boat Type"
                                content={profileInfo(
                                    content.vehicle && content.vehicle.vehicle.en
                                )}
                            />
                        </Row>
                    </StyledRow>
                )}
            </DrawerContainer>
        );
    }
}

export default SightingDrawer;
