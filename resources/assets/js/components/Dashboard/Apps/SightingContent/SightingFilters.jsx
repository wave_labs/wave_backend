import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, message  } from "antd";
import styled from "styled-components";
import moment from "moment";
import {
  fetchSightingsCoord,
  fetchSightings,
  exportSightingCsv,
} from "redux-modules/sighting/actions";
import { startCreating } from "redux-modules/editCreateModal/actions";
import FilterRow from "../../Common/FilterRow";

message.config({
  maxCount: 1,
});

const StyledRow = styled(Row)`
  margin: 1rem;
`;

class SightingFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operations: ["reset"],
      filters: {
        self: true,
        search: undefined,
        source: undefined,
        creature: undefined,
      },
    };
  }

  componentDidMount() {
    if (this.props.isAdmin) {
      let { operations } = this.state;
      operations.push("create", "download");
      this.setState({ operations });
    }
  }

  handleFilterChange = (filter, value) => {
    var { filters } = this.state;
    filters[filter] = value;
    this.setState({ filters });
  };

  handleDownload = () => {
    var filters = this.handleFilters();
    message.loading("Action in progress...", 0);
    this.props
      .exportSightingCsv(filters)
      .then(() => message.success("Export has finished with success", 5));
  };

  handleReset = () => {
    this.setState({
      filters: {
        search: undefined,
        date: undefined,
        self: true,
        creature: undefined,
      },
    });
  };

  handleFilters = () => {
    var { search, date, creature } = this.state.filters;
    let newDate = date ? [...date] : [];

    return {
      self: true,
      search: search,
      creature: creature,
      date:
        newDate.length > 0
          ? [
              moment(newDate[0]).format("YYYY-MM-DD"),
              moment(newDate[1]).format("YYYY-MM-DD"),
            ]
          : newDate,
    };
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state) {
      var filters = this.handleFilters();

      this.props.fetchSightingsCoord(filters);
      this.props.fetchSightings(1, filters);
      this.props.handleFilterChange(filters);
    }
  }

  render() {
    return (
      <StyledRow>
        <FilterRow
          filters={["search", "date", "creature"]}
          filterValues={this.state.filters}
          operations={this.state.operations}
          handleFilterChange={this.handleFilterChange}
          handleCreate={() => this.props.startCreating()}
          handleReset={() => this.handleReset()}
          handleDownload={() => this.handleDownload()}
        />
      </StyledRow>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSightingsCoord: (filters) => dispatch(fetchSightingsCoord(filters)),
    exportSightingCsv: (filters) => dispatch(exportSightingCsv(filters)),
    fetchSightings: (page, filters) => dispatch(fetchSightings(page, filters)),
    startCreating: () => dispatch(startCreating()),
  };
};
const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SightingFilters);
