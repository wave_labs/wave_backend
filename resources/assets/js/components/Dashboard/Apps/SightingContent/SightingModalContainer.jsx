import React, { Component } from "react";
import {
  createSighting,
  updateSighting,
  resetCurrentSighting,
} from "redux-modules/sighting/actions";
import { resetModal } from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../Common/EditCreateModal";
import SightingForm from "./SightingForm";
import moment from "moment";
import { alertActions } from "redux-modules/alert/actions";
import { getErrorMessages } from "helpers";

class SightingModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, sighting) => {
      if (err) {
        return;
      } else {
        const formatedDate = moment(sighting.date).format("YYYY-MM-DD");
        const vehicle_id = sighting.vehicle_id[1] && sighting.vehicle_id[1];

        this.setState({ confirmLoading: true });
        this.props
          .updateSighting(this.props.currentSigthing.id, {
            ...sighting,
            date: formatedDate,
            vehicle_id: vehicle_id,
          })
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function(message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.resetModal();
    this.props.resetCurrentSighting();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, sighting) => {
      if (err) {
        return;
      } else {
        const formatedDate = moment(sighting.date).format("YYYY-MM-DD");

        const vehicle_id = sighting.vehicle_id[1] ? sighting.vehicle_id[1] : 1;
        this.setState({ confirmLoading: true });

        this.props
          .createSighting({
            ...sighting,
            date: formatedDate,
            vehicle_id: vehicle_id,
          })
          .then(() => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = getErrorMessages(error.response.data.errors);

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    const { editing, creating } = this.props;
    return (
      <EditCreateModal
        titleEdit="Edit Report"
        titleCreate="Create Report"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={editing}
        creating={creating}
      >
        <SightingForm
          creating={creating}
          editing={editing}
          wrappedComponentRef={this.saveFormRef}
          sighting={this.props.currentSigthing}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetCurrentSighting: () => dispatch(resetCurrentSighting()),
    resetModal: () => dispatch(resetModal()),
    createSighting: (data) => dispatch(createSighting(data)),
    updateSighting: (data, id) => dispatch(updateSighting(data, id)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    currentSigthing: state.sighting.current,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SightingModalContainer);
