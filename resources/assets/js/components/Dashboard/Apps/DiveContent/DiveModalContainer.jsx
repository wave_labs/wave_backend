import React, { Component } from "react";
import {
    createDive,
    updateDive,
    resetDiveEdit,
} from "redux-modules/dive/actions";
import { resetModal } from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../Common/EditCreateModal";
import DiveForm from "./DiveForm";
import moment from "moment";
import { alertActions } from "redux-modules/alert/actions";

class DiveModalContainer extends Component {
    state = {
        confirmLoading: false, //modal button loading icon
    };

    onCancel = () => {
        this.resetModalForm();
    };

    onOkEditClick = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, dive) => {
            if (err) {
                return;
            } else {
                const formatedDate = moment(dive.date).format("YYYY-MM-DD");
                let abundance = [];
                let creatures = [];
                for (var i = 0; i < dive.keys.length; i++) {
                    if (dive.abundance[dive.keys[i]]) {
                        abundance.push(dive.abundance[dive.keys[i]]);
                    }
                    if (dive.creatures[dive.keys[i]]) {
                        creatures.push(dive.creatures[dive.keys[i]]);
                    }
                }
                dive.abundance = abundance;
                dive.creatures = creatures;
                this.setState({ confirmLoading: true });
                this.props
                    .updateDive(this.props.editDive.id, { ...dive, date: formatedDate })
                    .then((data) => {
                        this.resetModalForm();
                    })
                    .catch((error) => {
                        this.setState({ confirmLoading: false });
                        let messages = [];

                        Object.values(error.response.data.errors).map(function (message) {
                            messages.push(message[0]);
                        });

                        this.props.errorAlert({
                            description: messages,
                        });
                    });
            }
        });
    };

    resetModalForm = () => {
        const form = this.formRef.props.form;
        this.setState({ confirmLoading: false });
        this.props.resetDiveEdit();
        this.props.resetModal();
        form.resetFields();
    };

    onOkCreateClick = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, dive) => {
            if (err) {
                return;
            } else {
                const formatedDate = moment(dive.date).format("YYYY-MM-DD");
                let abundance = [];
                let creatures = [];
                for (var i = 0; i < dive.keys.length; i++) {
                    if (dive.abundance[dive.keys[i]]) {
                        abundance.push(dive.abundance[dive.keys[i]]);
                    }
                    if (dive.creatures[dive.keys[i]]) {
                        creatures.push(dive.creatures[dive.keys[i]]);
                    }
                }
                dive.abundance = abundance;
                dive.creatures = creatures;
                this.setState({ confirmLoading: true });

                this.props
                    .createDive({ ...dive, date: formatedDate })
                    .then((data) => {
                        this.resetModalForm();
                    })
                    .catch((error) => {
                        this.setState({ confirmLoading: false });
                        let messages = [];

                        Object.values(error.response.data.errors).map(function (message) {
                            messages.push(message[0]);
                        });

                        this.props.errorAlert({
                            description: messages,
                        });
                    });
            }
        });
    };

    saveFormRef = (formRef) => {
        this.formRef = formRef;
    };

    render() {
        return (
            <EditCreateModal
                titleEdit="Edit Dive"
                titleCreate="Create Dive"
                onOkEditClick={this.onOkEditClick}
                onOkCreateClick={this.resetModalForm}
                confirmLoading={this.state.confirmLoading}
                onCancel={this.onCancel}
                editing={this.props.editing}
                creating={this.props.creating}
                footer={null}
            >
                <DiveForm
                    creating={this.props.creating}
                    editing={this.props.editing}
                    wrappedComponentRef={this.saveFormRef}
                    dive={this.props.editDive}
                />
            </EditCreateModal>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        resetDiveEdit: () => dispatch(resetDiveEdit()),
        resetModal: () => dispatch(resetModal()),
        createDive: (data) => dispatch(createDive(data)),
        updateDive: (data, id) => dispatch(updateDive(data, id)),
        errorAlert: (data) => dispatch(alertActions.error(data)),
    };
};

const mapStateToProps = (state) => {
    return {
        editDive: state.dive.editDive,
        creating: state.editCreateModal.creating,
        editing: state.editCreateModal.editing,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DiveModalContainer);
