import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchDives } from "redux-modules/dive/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class DiveRemoteSelectContainer extends Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchDives(1, { search });
  };

  componentDidMount = () => {
    this.props.fetchDives();
  };

  render() {
    const { data, loading, value, onChange, mode, children } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Dive"
        mode={mode}
      >
        {children}
        {data.map((d) => (
          <Option value={d.id} key={d.id}>
            {d.id}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchDives: (page, filters) => dispatch(fetchDives(page, filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.dive.data,
    loading: state.dive.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DiveRemoteSelectContainer);
