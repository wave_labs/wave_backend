import { Row, Col, Divider } from "antd";
import DiveModalContainer from "./DiveModalContainer";
import DiveTableContainer from "./DiveTableContainer";
import React, { Component } from "react";
import { connect } from "react-redux";
import DiveFilters from "./DiveFilters";
import DiveMapList from "./DiveMapList";
import styled from "styled-components";
import DiveMontlyGraph from "./DiveMontlyGraph";
import DiveMostReported from "./DiveMostReported";

const StyledTitle = styled.div`
  font-size: 2rem;
`;

class DiveContent extends Component {
  state = {
    filters: [],
  };

  handleFilterChange = (newFilters) => {
    this.setState({ filters: newFilters });
  };

  render() {
    const account = this.props;

    return (
      <div>
        <Row>
          <Divider orientation="left">
            <StyledTitle> My dives </StyledTitle>
          </Divider>
        </Row>
        <DiveModalContainer />
        <Row type="flex" gutter={24}>
          <Col xs={24} lg={12}>
            <DiveMontlyGraph user={account} />
          </Col>
          <Col xs={24} lg={12}>
            <DiveMostReported user={account} />
          </Col>
        </Row>
        <StyledTitle> Latest reports </StyledTitle>
        <Row type="flex" gutter={24}>
          {this.props.isAdmin != undefined && (
            <Col xs={24} lg={24}>
              <DiveFilters
                handleFilterChange={(newFilters) =>
                  this.handleFilterChange(newFilters)
                }
              />
            </Col>
          )}
        </Row>
        <Row type="flex" gutter={24}>
          <Col xs={24} lg={12}>
            <DiveTableContainer />
          </Col>
          <Col xs={24} lg={12}>
            <DiveMapList />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(mapStateToProps, null)(DiveContent);
