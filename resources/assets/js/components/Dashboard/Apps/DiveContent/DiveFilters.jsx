import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, message, Select } from "antd";
import styled from "styled-components";
import moment from "moment";
import {
  fetchDives,
  fetchDiveCoords,
  exportDiveCsv,
} from "redux-modules/dive/actions";
import { startCreating } from "redux-modules/editCreateModal/actions";
import FilterRow from "../../Common/FilterRow";

message.config({
  maxCount: 1,
});

const StyledRow = styled(Row)`
  margin: 1rem;
`;

class DiveFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operations: ["reset"],
      filters: {
        self: true,
        search: undefined,
        divingSpot: undefined,
        creature: undefined,
        date: undefined,
      },
    };
  }

  componentDidMount() {
    if (this.props.isAdmin) {
      let { operations } = this.state;
      operations.push("create", "download");
      this.setState({ operations });
    }
  }

  handleFilterChange = (filter, value) => {
    var { filters } = this.state;
    filters[filter] = value;
    this.setState({ filters });
  };

  handleReset = () => {
    this.setState({
      filters: {
        search: undefined,
        date: undefined,
        self: true,
        divingSpot: undefined,
        creature: undefined,
      },
    });
  };

  handleDownload = () => {
    var filters = this.handleFilters();
    message.loading("Action in progress...", 0);
    this.props
      .exportDiveCsv(filters)
      .then(() => message.success("Export has finished with success", 5));
  };

  handleFilters = () => {
    var { search, date, divingSpot, creature } = this.state.filters;
    let newDate = date ? [...date] : [];

    return {
      self: true,
      search: search,
      divingSpot: divingSpot,
      creature: creature,
      date:
        newDate.length > 0
          ? [
              moment(newDate[0]).format("YYYY-MM-DD"),
              moment(newDate[1]).format("YYYY-MM-DD"),
            ]
          : newDate,
    };
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state) {
      var filters = this.handleFilters();

      this.props.fetchDiveCoords(filters);
      this.props.fetchDives(1, filters);
      this.props.handleFilterChange(filters);
    }
  }

  render() {
    return (
      <StyledRow>
        <FilterRow
          additionalFilters={[
            <Select
              mode="tags"
              value={this.state.filters.search}
              style={{ width: "100%" }}
              placeholder="Search"
              onChange={(value) => this.handleFilterChange("search", value)}
            ></Select>,
          ]}
          filters={["date", "divingSpot", "creature"]}
          filterValues={this.state.filters}
          operations={this.state.operations}
          handleFilterChange={this.handleFilterChange}
          handleCreate={() => this.props.startCreating()}
          handleReset={() => this.handleReset()}
          handleDownload={() => this.handleDownload()}
        />
      </StyledRow>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchDiveCoords: (filters) => dispatch(fetchDiveCoords(filters)),
    fetchDives: (page, filters) => dispatch(fetchDives(page, filters)),
    exportDiveCsv: (filters) => dispatch(exportDiveCsv(filters)),
    startCreating: () => dispatch(startCreating()),
  };
};
const mapStateToProps = (state) => {
  return {
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DiveFilters);
