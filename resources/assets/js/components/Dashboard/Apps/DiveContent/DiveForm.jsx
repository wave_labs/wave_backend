import React, { Component } from "react";
import styled from "styled-components";
import { Form, Row } from "antd";
import DiveInfoForm from "./FormComponents/DiveInfoForm";
import DiveCreatureListForm from "./FormComponents/DiveCreatureListForm";
import DiveAbundaceSelectorForm from "./FormComponents/DiveAbundaceSelectorForm";
import DiveThanks from "./FormComponents/DiveThanks";
import { ArrowRightOutlined, ArrowLeftOutlined } from "@ant-design/icons";

const Next = styled.button`
  border-radius: 50px;
  width: 50px;
  height: 50px;
  background-color: #2b8ccc;
  margin: auto;
  display: block;
  cursor: pointer;
  margin-top: 20px;
  box-shadow: 0px;
  border: 0px;
`;

const buttonRow = styled.div`
  height: 50px;
  width: 100%;
  margin: auto;
`;

class DiveForm extends Component {
  state = {
    currentCreature: {},
    creatures: [],
    id: 0,
    step: 0,
  };

  rules = {
    max_depth: [
      {
        required: false,
        message: "Max Depth is required",
      },
    ],
    dive_time: [
      {
        required: true,
        message: "Dive Time is required",
      },
    ],
    diving_spot: [
      {
        required: true,
        message: "Diving spot is required",
      },
    ],
    user: [
      {
        required: true,
        message: "User is required",
      },
    ],
    date: [
      {
        type: "object",
        whitespace: true,
        required: true,
        message: "Date field is required",
      },
    ],
    number_diver: [
      {
        required: true,
        message: "Number of Divers is required",
      },
    ],
  };

  handleCreatureSelect = (aCreature) => {
    this.setState({ step: 3, currentCreature: aCreature });
  };

  handleBackButton = () => {
    this.setState({ step: 1});
  };

  handleAbundanceSelect = (aObject) => {
    var { creatures } = this.state;
    creatures.push(aObject);
    this.setState({ step: 1, creatures });
  };

  render() {
    const { dive, editing } = this.props;
    console.log(this.state.creatures);
    const steps = [
      {
        key: 0,
        content: (
          <DiveInfoForm
            form={this.props.form}
            dive={dive}
            editing={editing}
            rules={this.rules}
          />
        ),
      },
      {
        key: 1,
        content: (
          <DiveCreatureListForm
            handleCreatureSelect={this.handleCreatureSelect}
            selected={this.state.creatures}
          />
        ),
      },
      {
        key: 2,
        content: <DiveThanks />,
      },
      {
        key: 3,
        content: (
          <DiveAbundaceSelectorForm
            handleAbundanceSelect={this.handleAbundanceSelect}
            current={this.state.currentCreature}
            handleBack={this.handleBackButton}
          />
        ),
      },
    ];

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark>
        {steps[this.state.step].content}


        <Row type="flex" justify="center" gutter={16}>
          {this.state.step > 0 && this.state.step < 2 && (
            <Next onClick={() => this.setState({ step: this.state.step - 1 })}>
              <ArrowLeftOutlined style={{ color: "white" }} />
            </Next>
          )}
          {this.state.step < 2 && (
            <Next onClick={() => this.setState({ step: this.state.step + 1 })}>
              <ArrowRightOutlined style={{ color: "white" }} />
            </Next>
          )}
        </Row>
      </Form>
    );
  }
}

export default Form.create()(DiveForm);
