import React, { useEffect } from 'react'
import styled from "styled-components";
import { connect } from "react-redux";
import { fetchSelector } from "redux-modules/creature/actions";

const Container = styled.div`
    display: flex;
    flex-wrap: wrap;

    div {
        position: relative;
        width: 33%;
    }

    p {
        position: absolute;
        color: white;
        font-size: 22px;
        font-weight: bold;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    img {
        width: 100%;
        transition: all .3s ease;
        cursor: pointer;

        &:hover {
            filter: brightness(.6);
        }
    }
`;

const ImageContainer = styled.div`
    position: relative;
    width: 33%;

    p {
        position: absolute;
        color: white;
        font-size: 22px;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    img {
        width: 100%;
        transition: all .3s ease;
        cursor: pointer;
        filter: ${props => props.isSelected ? "brightness(.6)" : "brightness(1)"} ;

        &:hover {
            filter: brightness(.6);
        }
    }
`;

function DiveCreatureListForm(props) {
    useEffect(() => {
        props.fetchSelector({ source: "DIVE_REPORTER" })
    }, [])

    const hasAbundance = (currentCreature) => {
        var content = "";
        props.selected.map((current) => {
            if (current.creature_id == currentCreature) {
                content = current.abundance
            }
        })

        return content;
    }

    return (
        <Container>
            {props.creatures.map((creature) => (
                <ImageContainer isSelected={hasAbundance(creature.id) != ""}>
                    <img onClick={() => props.handleCreatureSelect(creature)} key={creature.id} src={window.location.origin + "/api/" + creature.photos[0].link} alt="" />
                    <p>{hasAbundance(creature.id)}</p>
                </ImageContainer>
            ))}
        </Container>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSelector: (filters) => dispatch(fetchSelector(filters)),
    };
};

const mapStateToProps = (state) => {
    return {
        creatures: state.creature.CreatureSelector,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DiveCreatureListForm);