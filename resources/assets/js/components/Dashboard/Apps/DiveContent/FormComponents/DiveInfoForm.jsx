import { Col, Form, Row, InputNumber, DatePicker } from 'antd'
import React from 'react'
import DivingSpotRemoteSelectContainer from '../../../Data/DivingSpotContent/DivingSpotRemoteSelectContainer'
import styled from "styled-components";

const StyledInputNumber = styled(InputNumber)`
  min-width: 100% !important;
`;

function DiveInfoForm(props) {
    const { getFieldDecorator, getFieldValue } = props.form;
    const { dive, editing, rules } = props;

    return (
        <Row type="flex" gutter={8}>
            <Col xs={24} sm={12}>
                <Form.Item hasFeedback label="Diving Spot">
                    {getFieldDecorator("diving_spot_id", {
                        initialValue: dive.diving_spot.id,
                        rules: rules.diving_spot,
                    })(
                        <DivingSpotRemoteSelectContainer />
                    )}
                </Form.Item>
            </Col>
            <Col xs={24} sm={12}>
                <Form.Item hasFeedback label="Date">
                    {getFieldDecorator("date", {
                        initialValue: dive.date ? moment(dive.date) : null,
                        rules: rules.date,
                    })(<DatePicker style={{ width: "100%" }} />)}
                </Form.Item>
            </Col>

            <Col xs={24} sm={12}>
                <Form.Item hasFeedback label="Dive Time">
                    {getFieldDecorator("dive_time", {
                        initialValue: dive.dive_time,
                        rules: rules.dive_time,
                    })(<StyledInputNumber placeholder="Dive Time (m)" />)}
                </Form.Item>
            </Col>
            <Col xs={24} sm={12}>
                <Form.Item hasFeedback label="Number of Divers">
                    {getFieldDecorator("number_diver", {
                        initialValue: dive.number_diver,
                        rules: rules.number_diver,
                    })(<StyledInputNumber placeholder="Number of Divers" />)}
                </Form.Item>
            </Col>
        </Row>
    )
}

export default DiveInfoForm