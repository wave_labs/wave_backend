import React from "react";
import styled from "styled-components";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { Row } from "antd";

const Container = styled.div`
  img {
    width: 100%;
    display: block;
    margin: auto;
  }

  h3 {
    margin: 30px auto;
    text-align: center;
    font-size: clamp(18px, 2vw, 22px);
    font-weight: bold;
  }

  h4 {
    text-align: center;
    margin: 30px auto;
  }
`;

const AbundanceContainer = styled.div`
  display: flex;
  justify-content: space-around;
  margin: 20px 0px 50px 0px;

  button {
    border-radius: 60px;
    width: 60px;
    height: 60px;
    background-color: #2b8ccc;
    margin: auto;
    display: block;
    cursor: pointer;
    color: white;
  }
`;

const Next = styled.button`
  border-radius: 50px;
  width: 50px;
  height: 50px;
  background-color: #2b8ccc;
  display: block;
  cursor: pointer;
  box-shadow: 0px;
  border: 0px;
`;

function DiveAbundaceSelectorForm(props) {
  const handleAbundanceSelect = (value) => {
    props.handleAbundanceSelect({
      creature_id: props.current.id,
      abundance: value,
    });
  };

  const handleBack = (value) => {
    props.handleBack();
  };

  return (
    <Container>
      <Next onClick={() => handleBack()}>
        <ArrowLeftOutlined style={{ color: "white" }} />
      </Next>
      <img
        src={window.location.origin + "/api/" + props.current.photos[0].link}
        alt=""
      />
      <h3>{props.current.name}</h3>

      <h4>Select the abundance (meadow size)</h4>
      <AbundanceContainer>
        <button
          onClick={() => handleAbundanceSelect(props.current.abundance.level_1)}
        >
          {props.current.abundance.level_1}
        </button>
        <button
          onClick={() => handleAbundanceSelect(props.current.abundance.level_2)}
        >
          {props.current.abundance.level_2}
        </button>
        <button
          onClick={() => handleAbundanceSelect(props.current.abundance.level_3)}
        >
          {props.current.abundance.level_3}
        </button>
        <button
          onClick={() => handleAbundanceSelect(props.current.abundance.level_4)}
        >
          {props.current.abundance.level_4}
        </button>
      </AbundanceContainer>
    </Container>
  );
}

export default DiveAbundaceSelectorForm;
