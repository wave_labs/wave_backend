import React from 'react'

function DiveThanks() {
    return (
        <div>
            <h1 style={{ textAlign: "center", fontSize: "26px" }}>Thank you</h1>
            <p style={{ textAlign: "center" }}>Your report has been saved succesfully, if you wish to contact us, please send an email to team@wave-labs.org</p>
        </div>
    )
}

export default DiveThanks