import React, { Component } from "react";
import { connect } from "react-redux";
import { Layer, Source } from "react-map-gl";
import MapBoxMap from "../../Common/MapBoxMap";
import { fetchDiveCoords } from "redux-modules/dive/actions";

class DiveMapList extends Component {
  state = {
    coordinates: [],
    filters: {
      self: true,
    },
  };

  componentDidMount() {
    this.props.fetchDiveCoords(this.state.filters);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.coordinates != this.props.coordinates) {
      let coordinates = [];

      this.props.coordinates.forEach((element) => {
        coordinates.push([element.longitude, element.latitude]);
      });

      this.setState({ coordinates });
    }
  }

  render() {
    let { loading } = this.props;
    let { coordinates } = this.state;

    const geojson = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          geometry: { type: "MultiPoint", coordinates: coordinates },
        },
      ],
    };

    return (
      <div className="map-container">
        {!loading && (
          <MapBoxMap zoom={9}>
            <Source id="my-data" type="geojson" data={geojson}>
              <Layer
                id="point"
                type="heatmap"
                paint={{
                  "heatmap-radius": 15,
                  "heatmap-color": [
                    "interpolate",
                    ["linear"],
                    ["heatmap-density"],
                    0,
                    "rgba(0, 0, 255, 0)",
                    0.1,
                    "rgba(130, 130, 255, 0.4)",

                    0.5,
                    "rgba(180, 180, 255, 0.8)",

                    1,
                    "rgba(230, 230, 255, .9)",
                  ],
                }}
              />
            </Source>
          </MapBoxMap>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchDiveCoords: (filters) => dispatch(fetchDiveCoords(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    coordinates: state.dive.coordinates,
    loading: state.dive.loadingCoordinates,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DiveMapList);
