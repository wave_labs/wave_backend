import React, { Component } from "react";
import {
  fetchDives,
  deleteDive,
  setDiveEdit,
} from "redux-modules/dive/actions";
import { connect } from "react-redux";
import { Badge, message } from "antd";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import { startEditing } from "redux-modules/editCreateModal/actions";
import styled from "styled-components";
import { NoDataMessage } from "helpers";

const StyledSpan = styled.span`
  margin-left: 1px;
`;

message.config({
  maxCount: 1,
});

class DiveTableContainer extends Component {
  constructor(props) {
    super(props);
    this.isRegulator = false;
    this.filters = {
      self: true,
    };
    this.exportFilters = {};
    this.columns = [
      {
        title: "Diving spot",
        dataIndex: "diving_spot.name",
      },
      {
        title: "User",
        dataIndex: "userable.user.name",
      },
      {
        title: "Creatures",
        dataIndex: "creatures",
        render: (creatures) => (
          <span>
            {creatures.length
              ? creatures.map((c) => (
                  <div key={c.id}>
                    <Badge
                      count={c.abundance_value}
                      style={{
                        backgroundColor: "#fff",
                        color: "#999",
                        boxShadow: "0 0 0 1px #d9d9d9 inset",
                      }}
                    />

                    <StyledSpan> {c.creature.name} </StyledSpan>
                  </div>
                ))
              : NoDataMessage("No sightings in record")}
          </span>
        ),
      },
      {
        title: "Duration (min)",
        dataIndex: "dive_time",
      },
      {
        title: "Divers",
        dataIndex: "number_diver",
      },
      {
        title: "",
        key: "",
        render: (text, dive) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(dive)}
            onDeleteConfirm={() => this.props.deleteDive(dive.id)}
          />
        ),
      },
    ];
    this.columnsRegulator = [
      {
        title: "Diving spot",
        dataIndex: "diving_spot.name",
      },
      {
        title: "Creatures",
        dataIndex: "creatures",
        render: (creatures) => (
          <span>
            {creatures.length
              ? creatures.map((c) => (
                  <div key={c.id}>
                    <Badge
                      count={c.abundance_value}
                      style={{
                        backgroundColor: "#fff",
                        color: "#999",
                        boxShadow: "0 0 0 1px #d9d9d9 inset",
                      }}
                    />

                    <StyledSpan> {c.creature.name} </StyledSpan>
                  </div>
                ))
              : NoDataMessage("No sightings in record")}
          </span>
        ),
      },
      {
        title: "Effort (min)",
        render: (record) => (
          <span>{record.number_diver * record.dive_time}</span>
        ),
      },
      {
        title: "Date",
        dataIndex: "date",
      },
    ];
  }

  componentDidMount = () => {
    this.props.user.roles.forEach((element) => {
      if (element.name == `regulator`) {
        this.isRegulator = true;
      }
    });
    this.props.fetchDives(1, this.filters);
  };

  componentDidUpdate = () => {
    this.props.user.roles.forEach((element) => {
      if (element.name == `regulator`) {
        this.isRegulator = true;
      }
    });
  };

  onUpdateClick = (dive) => {
    this.props.setDiveEdit(dive);
    this.props.startEditing();
  };

  handleTableChange = (pagination) => {
    this.props.fetchDives(pagination.current, this.props.filters);
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <div>
        <TablePagination
          loading={this.props.loading}
          columns={this.isRegulator ? this.columnsRegulator : this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchDives: (page, filters) => dispatch(fetchDives(page, filters)),
    deleteDive: (id) => dispatch(deleteDive(id)),
    startEditing: () => dispatch(startEditing()),
    setDiveEdit: (data) => dispatch(setDiveEdit(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    data: state.dive.data,
    meta: state.dive.meta,
    loading: state.dive.loading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DiveTableContainer);
