import React, { Component, useEffect } from "react";
import {
  fetchMostReportedSpecies,
  deleteDive,
  setDiveEdit,
} from "redux-modules/dive/actions";
import { connect } from "react-redux";
import { startEditing } from "redux-modules/editCreateModal/actions";
import styled from "styled-components";
import TablePagination from "../../Common/TablePagination";
import { Bar } from "react-chartjs-2";
import { useState } from "react";

const StyledSpan = styled.span`
  margin-left: 1px;
`;

const StyledTitle = styled.div`
  font-size: 2rem;
`;

function DiveMostReported(props) {
  const { data, loading, meta } = props;
  const [info, setInfo] = useState(null);

  var labels = [];
  var occurrences = [];

  useEffect(() => {
    props.fetchMostReportedSpecies();
  }, []);

  useEffect(() => {
    data.forEach((element) => {
      labels.push(element["name"]["en"]);
      occurrences.push(element["dives_count"]);
    });
    setInfo({
      labels,
      datasets: [
        {
          label: "Number of occurrences",
          data: occurrences,
          borderColor: "rgb(0, 0, 255)",
          backgroundColor: "rgba(0, 0, 255, 0.5)",
        },
      ],
    });
  }, [data]);

  const options = {
    plugins: {
      legend: {
        position: "top",
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    maintainAspectRatio: false,
  };

  return (
    <div style={{ maxHeight: "650px", marginBottom: "5em" }}>
      <StyledTitle> Most reported species </StyledTitle>
      <Bar options={options} data={info} />
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMostReportedSpecies: () => dispatch(fetchMostReportedSpecies()),
    deleteDive: (id) => dispatch(deleteDive(id)),
    startEditing: () => dispatch(startEditing()),
    setDiveEdit: (data) => dispatch(setDiveEdit(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.dive.mostReportedData,
    meta: state.dive.meta,
    loading: state.dive.loading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DiveMostReported);
