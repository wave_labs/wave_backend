import React from "react";
import { withRouter, Link } from "react-router-dom";
import { Breadcrumb, Icon } from "antd";
import pathToRegexp from "path-to-regexp";
import styled from "styled-components";
import {
    EnvironmentOutlined,
    FileAddOutlined,
    HomeOutlined,
    FileSearchOutlined,
    FolderOpenOutlined,
    PushpinOutlined,
    QqOutlined,
    UserOutlined,
    ShakeOutlined,
    FieldTimeOutlined,
    BookOutlined,
    ApiOutlined,
} from "@ant-design/icons";
import {
    WhaleReporter,
    DiveReporter,
    LitterReporter,
    Poseidon,
    Seamote,
    Triton,
    Oceanus,
    MegafaunaNet,
    MarineLitterNet,
    AcousticNet,
    BoundingBox,
    FreshwaterPin,
    Diadema,
    Intertagua
} from "customIcons";

const StyledBreadCrumb = styled(Breadcrumb)`
  span:last-child a {
    color: rgba(0, 0, 0, 0.65);
  }
`;

const breadcrumbNameMap = {
    "/dashboard": () => (
        <span>
            <HomeOutlined />
            <span> Dashboard</span>
        </span>
    ),
    "/dashboard/apps": () => (
        <span>
            <ShakeOutlined />
            <span> Apps </span>
        </span>
    ),
    "/dashboard/apps/litterReporter": () => (
        <span>
            <Icon component={LitterReporter} />
            <span> Litter Reporter </span>
        </span>
    ),
    "/dashboard/apps/whaleReporter": () => (
        <span>
            <Icon component={WhaleReporter} style={{ stroke: "#000000" }} />
            <span>Whale Reporter</span>
        </span>
    ),
    "/dashboard/apps/diveReporter": () => (
        <span>
            <Icon component={DiveReporter} style={{ stroke: "#000000" }} />
            <span> Dive Reporter </span>
        </span>
    ),

    "/dashboard/users": () => (
        <span>
            <UserOutlined />
            <span> Users</span>
        </span>
    ),
    "/dashboard/workplace": () => (
        <span>
            <FileAddOutlined />
            <span> Workplace </span>
        </span>
    ),

    "/dashboard/creatures": () => (
        <span>
            <FolderOpenOutlined />
            <span> Categories </span>
        </span>
    ),
    "/dashboard/creatures/:id": () => (
        <span>
            <QqOutlined />
            <span> Creatures </span>
        </span>
    ),

    "/dashboard/divingSpot": () => (
        <span>
            <PushpinOutlined />
            <span> Diving Spots </span>
        </span>
    ),

    "/dashboard/suggestions": () => (
        <span>
            <Icon type="question" />
            <span> Suggestions </span>
        </span>
    ),
    "/dashboard/form": () => (
        <span>
            <Icon type="monitor" />
            <span> Surveys </span>
        </span>
    ),

    "/dashboard/litter/categories": () => (
        <span>
            <FolderOpenOutlined />
            <span> Litter Categories </span>
        </span>
    ),

    "/dashboard/litter/categories/:id": () => (
        <span>
            <FileSearchOutlined />
            <span> Litter </span>
        </span>
    ),

    "/dashboard/settings": () => (
        <span>
            <Icon type="setting" />
            <span> Settings </span>
        </span>
    ),
    "/dashboard/iotReports": () => (
        <span>
            <Icon type="file" />
            <span> Reports </span>
        </span>
    ),

    "/dashboard/iotUnits": () => (
        <span>
            <Icon type="setting" />
            <span> Units </span>
        </span>
    ),
    "/dashboard/iotDataType": () => (
        <span>
            <Icon type="setting" />
            <span> Data Type </span>
        </span>
    ),
    "/dashboard/iot": () => (
        <span>
            <ApiOutlined />
            <span> IoT </span>
        </span>
    ),
    "/dashboard/iot/:deviceType": (_, deviceType) => (
        <span>
            <Icon
                component={
                    deviceType == "poseidon"
                        ? Poseidon
                        : deviceType == "triton"
                            ? Triton
                            : deviceType == "seamote"
                                ? Seamote
                                : Intertagua
                }
                style={{ stroke: "#000000" }}
            />
            <span style={{ textTransform: "capitalize" }}> {deviceType} </span>
        </span>
    ),
    "/dashboard/iot/:deviceType/:id": () => (
        <span>
            <BookOutlined />
            <span> Reports </span>
        </span>
    ),
    "/dashboard/ai/1": () => (
        <span>
            <Icon component={MegafaunaNet} style={{ stroke: "#000000" }} />
            <span> BiodiversityNet </span>
        </span>
    ),
    "/dashboard/ai/2": () => (
        <span>
            <Icon component={MarineLitterNet} style={{ stroke: "#000000" }} />
            <span> LitterNet </span>
        </span>
    ),
    "/dashboard/ai/3": () => (
        <span>
            <Icon component={AcousticNet} style={{ stroke: "#000000" }} />
            <span> AcousticNet </span>
        </span>
    ),
    "/dashboard/ai": () => (
        <span>
            <Icon type="deployment-unit" />
            <span> AI </span>
        </span>
    ),
    "/dashboard/form/structure/:form": (_, form) => (
        <span>
            <Icon type="book" />
            <span> {form} </span>
        </span>
    ),
    "/dashboard/form/results/:form/:id": () => (
        <span>
            <Icon type="book" />
            <span> Results </span>
        </span>
    ),
    "/dashboard/form/results/:form": (_, form) => (
        <span>
            <Icon type="book" />
            <span> {form} </span>
        </span>
    ),
    "/dashboard/studies/markers": () => (
        <span>
            <EnvironmentOutlined />
            <span> Markers </span>
        </span>
    ),
    "/dashboard/studies/boundingBox": () => (
        <span>
            <Icon component={BoundingBox} />
            <span> Bounding Box </span>
        </span>
    ),
    "/dashboard/studies/freshwaterPin": () => (
        <span>
            <Icon component={FreshwaterPin} />
            <span> Freshwater Pin </span>
        </span>
    ),
    "/dashboard/studies/diadema": () => (
        <span>
            <Icon component={Diadema} />
            <span> Diadema </span>
        </span>
    ),
    "/dashboard/studies/stopwatch/:id": () => (
        <span>
            <BookOutlined />
            <span> Results </span>
        </span>
    ),
    "/dashboard/studies/stopwatch": () => (
        <span>
            <FieldTimeOutlined />
            <span> Stopwatch </span>
        </span>
    ),
    "/dashboard/studies": () => (
        <span>
            <BookOutlined />
            <span> Studies </span>
        </span>
    ),
    "/dashboard/sightings/:id": (_, id) => `Report ${id}`,
};

const BreadcrumbRouter = (props) => {
    const renderBreadcrumbItems = () => {
        const { location } = props;
        const pathSnippets = location.pathname.split("/").filter((i) => i);
        const breadcrumbItems = [];

        pathSnippets.forEach((_, index) => {
            const currentUrl = `/${pathSnippets.slice(0, index + 1).join("/")}`;

            Object.keys(breadcrumbNameMap).forEach((mapUrl) => {
                const pathMatches = pathToRegexp(mapUrl).exec(currentUrl);
                if (pathMatches) {
                    breadcrumbItems.push(
                        <Breadcrumb.Item key={currentUrl}>
                            <Link to={currentUrl}>
                                {breadcrumbNameMap[mapUrl](...pathMatches)}
                            </Link>
                        </Breadcrumb.Item>
                    );
                }
            });
        });

        return breadcrumbItems;
    };

    return (
        <StyledBreadCrumb style={{ margin: "16px 0" }}>
            {renderBreadcrumbItems()}
        </StyledBreadCrumb>
    );
};

export default withRouter(BreadcrumbRouter);
