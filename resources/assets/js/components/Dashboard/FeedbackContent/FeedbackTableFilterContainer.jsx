import React, { Component } from "react";
import {
  fetchFeedback,
  deleteFeedback,
  setFeedbackEdit,
  updateFeedback,
} from "redux-modules/feedback/actions";
import { connect } from "react-redux";
import RowOperation from "../Common/RowOperation";
import TablePagination from "../Common/TablePagination";
import TableFilterRow from "../Common/TableFilterRow";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import { Link } from "react-router-dom";

const sourceConverter = {
  WHALE_REPORTER: "Whale Reporter",
  DIVE_REPORTER: "Dive Reporter",
  LITTER_REPORTER: "Litter Reporter",
};

class FeedbackTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "User",
        dataIndex: "userable.user.name",
      },
      {
        title: "Date",
        dataIndex: "created_at",
      },
      {
        title: "Source",
        dataIndex: "source.form",
        render: (source) => sourceConverter[source],
      },
      {
        title: "Average score",
        dataIndex: "score",
      },
      {
        title: "Actions",
        width: 170,
        render: (row) => (
          <span>
            <Link to={`/dashboard/form/results/${row.form.form}/${row.id}`}>
              Results
            </Link>
          </span>
        ),
      },
      {
        title: "",
        key: "",
        render: (text, feedback) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(feedback)}
            onDeleteConfirm={() => this.props.deleteFeedback(feedback.id)}
          />
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.props.fetchFeedback(this.props.feedback);

    console.log(this.props);
  };

  onUpdateClick = (feedback) => {
    this.props.setFeedbackEdit(feedback);
    this.props.startEditing();
  };

  onSearch = (search) => {
    this.filters = { ...this.filters, search };
    this.props.fetchFeedback(this.props.feedback, 1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchFeedback(
      this.props.feedback,
      pagination.current,
      this.filters
    );
  };

  render() {
    return (
      <div>
        <TableFilterRow
          onSearch={(search) => this.onSearch(search)}
          searchPlaceholder="Search Feedback"
        />

        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchFeedback: (form, page, filters) =>
      dispatch(fetchFeedback(form, page, filters)),
    deleteFeedback: (id) => dispatch(deleteFeedback(id)),
    activateFeedback: (id) => dispatch(activateFeedback(id)),
    startEditing: () => dispatch(startEditing()),
    setFeedbackEdit: (data) => dispatch(setFeedbackEdit(data)),
    updateFeedback: (id, data) => dispatch(updateFeedback(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.feedback.data,
    meta: state.feedback.meta,
    loading: state.feedback.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FeedbackTableFilterContainer);
