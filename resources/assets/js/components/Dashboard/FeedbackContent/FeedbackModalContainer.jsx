import React, { Component } from "react";
import {
  createFeedback,
  updateFeedback,
  resetFeedbackEdit
} from "redux-modules/feedback/actions";
import {
  resetModal,
  startEditing
} from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../Common/EditCreateModal";
import FeedbackForm from "./FeedbackForm";

class FeedbackModalContainer extends Component {
  state = {
    confirmLoading: false //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, feedback) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        this.props
          .updateFeedback(this.props.editFeedback.id, feedback)
          .then(data => {
            this.resetModalForm();
          })
          .catch(e => this.setState({ confirmLoading: false }));
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.resetFeedbackEdit();
    this.props.resetModal();
    form.resetFields();
  };

  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit Feedback"
        onOkEditClick={() => this.onOkEditClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
      >
        <FeedbackForm
          wrappedComponentRef={this.saveFormRef}
          feedback={this.props.editFeedback}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    resetFeedbackEdit: () => dispatch(resetFeedbackEdit()),
    resetModal: () => dispatch(resetModal()),
    createFeedback: data => dispatch(createFeedback(data)),
    updateFeedback: (data, id) => dispatch(updateFeedback(data, id))
  };
};

const mapStateToProps = state => {
  return {
    editFeedback: state.feedback.editFeedback,
    editing: state.editCreateModal.editing
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FeedbackModalContainer);
