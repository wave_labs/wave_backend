import React, { Component } from "react";
import { connect } from "react-redux";
import FeedbackModalContainer from "./FeedbackModalContainer";
import FeedbackTableFilterContainer from "./FeedbackTableFilterContainer";

class FeedbackContent extends Component {
  render() {
    const { feedback } = this.props;
    return (
      <div>
        <FeedbackModalContainer />
        <FeedbackTableFilterContainer feedback={this.props.match.params.form} />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchFeedback: form => dispatch(fetchFeedback(form))
  };
};

const mapStateToProps = state => {
  return {
    feedback: state.feedback.current
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FeedbackContent);
