import React, { Component } from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  InputNumber,
  Button,
  Select,
  Checkbox
} from "antd";
import { Link } from "react-router-dom";
import UploadFileFormItem from "../Common/UploadFileFormItem";

const FormItem = Form.Item;
const TextArea = Input.TextArea;
const Option = Select.Option;

class FeedbackForm extends Component {
  state = {};

  rules = {
    form: [
      {
        required: true,
        message: "Form name is required"
      }
    ],
    desc: [
      {
        required: true,
        message: "Please input a description!"
      }
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { feedback, editing } = this.props;
    const { user } = feedback;
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row>
            <FormItem hasFeedback label="Name">
              {getFieldDecorator("form", {
                initialValue: feedback.form,
                rules: this.rules.form
              })(<Input placeholder="Name" />)}
            </FormItem>
        </Row>
        <FormItem hasFeedback label="Description">
          {getFieldDecorator("desc", {
            initialValue: feedback.desc,
            rules: this.rules.desc
          })(
            <TextArea
              placeholder="Description"
              autosize={{ minRows: 4, maxRows: 10 }}
            />
          )}
        </FormItem>
      </Form>
    );
  }
}

export default Form.create()(FeedbackForm);
