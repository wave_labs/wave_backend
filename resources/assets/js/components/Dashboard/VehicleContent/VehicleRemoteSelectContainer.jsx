import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/vehicle/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class VehicleRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      VehicleSelector,
      loading,
      value,
      onChange,
      mode,
      children,
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Vehicle"
        mode={mode}
      >
        {children}
        {VehicleSelector.map((b) => (
          <Option value={b.id} key={b.id}>
            {b.vehicle}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    VehicleSelector: state.vehicle.VehicleSelector,
    loading: state.vehicle.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VehicleRemoteSelectContainer);
