import { Col, Row } from 'antd'
import React from 'react'
import LoadingContainer from '../../../Common/LoadingContainer'
import IotDeviceInfo from '../IotReportContent/IotDeviceInfo'
import IotReportDrawerGraph from '../IotReportContent/IotReportDrawerGraph'
import MapContainer from './Common/MapContainer'
import TableContainer from './Common/TableContainer'

function MinimalistMap({ iotDevice }) {
    return (
        <LoadingContainer hasData={Object.keys(iotDevice).length}>
            <IotDeviceInfo iotDevice={iotDevice} />
            <IotReportDrawerGraph
                fields={iotDevice.fields}
                device={iotDevice.id}
            />
            <Row type='flex' gutter={32}>
                <div className='map-container'>
                    <MapContainer device={iotDevice} fixedSize />
                </div>
            </Row>

            <br />
            <TableContainer device={iotDevice.id} deviceType={iotDevice.type} deviceName={iotDevice.name} />
        </LoadingContainer>
    )
}

export default MinimalistMap;
