import React, { Fragment } from "react";
import { Bar } from "react-chartjs-2";

const VerticalBarChart = ({ value, labels, title, displayGrid = false }) => {
  const data = {
    labels: labels,
    datasets: [
      {
        fill: true,
        lineTension: 0.1,
        backgroundColor: "rgba(42, 116, 252, 0.6)",
        data: value,
      },
    ],
  };
  const options = {
    legend: {
      display: false,
    },
    animation: {
      duration: 0,
    },
    showXLabels: 5,
    scales: {
      yAxes: [
        {
          scaleLabel: {
            display: false,
          },
        },
      ],
      xAxes: [
        {
          ticks: {
            callback: (aValue, index) => {
              return value.length > 20
                ? index % 10 != 0
                  ? ""
                  : aValue
                : aValue;
            },
          },
          scaleLabel: {
            display: false,
          },
          gridLines: {
            display: displayGrid,
          },
        },
      ],
    },
  };

  return (

    <Fragment>
      <h2>{title}</h2>
      <Bar data={data} options={options} />
    </Fragment>


  );
};

export default VerticalBarChart;
