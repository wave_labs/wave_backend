import React from "react";
import { Doughnut } from "react-chartjs-2";
import { Fragment } from "react";
import styled from "styled-components";

const DoughnutContainer = styled.div`
  position: relative;

  .title {
    text-align: center;
    position: absolute;
    bottom: 10px;
    left: 0;
    right: 0;
    margin: 0 auto;
    font-weight: bold;
    width: 300px;
    font-size: 12px;
  }
`;

const Gauge = ({ value, title, range, unit }) => {
    const data = {
        labels: [""],
        datasets: [
            {
                label: "",
                data: [
                    (100 / (range[1] - range[0])) * (parseFloat(value) > parseFloat(range[1]) ? range[1] : value) - range[0],
                    100 - (100 / (range[1] - range[0])) * (parseFloat(value) > parseFloat(range[1]) ? range[1] : value) - range[0],
                ],
                backgroundColor: ["rgb(78, 136, 245)", "rgba(0, 89, 255, 0)"],
                borderColor: ["rgba(0, 89, 255, 0)", "rgb(78, 136, 245)"],
                borderWidth: 1,
            },
        ],
    };
    const options = {
        rotation: 0.8 * Math.PI,
        circumference: 1.4 * Math.PI,
        legend: {
            display: false,
        },
        animation: {
            duration: 0,
        },
        tooltips: {
            custom: function (tooltipModel) {
                tooltipModel.opacity = 0;
            },
        },
        cutoutPercentage: 90,
    };

    const handleDoughnutPlugins = (chart, range, value) => {
        var ctx = chart.chart.ctx;
        var cw = chart.chart.canvas.offsetWidth;
        var ch = chart.chart.canvas.offsetHeight;

        ctx.textAlign = "center";
        ctx.textBaseline = "middle";

        ctx.font = "20px B612";
        ctx.fontWeight = "bold";
        ctx.fillText(value[0], cw / 2, ch / 2);

        ctx.font = "14px B612";
        ctx.fontWeight = "normal";
        ctx.fillText(value[1], cw / 2, ch / 1.7);

        ctx.font = "12px B612";
        ctx.fillText(range[0], cw / 3.3, ch - 4);
        ctx.fillText(range[1], cw / 1.45, ch - 4);
    };

    return (

        <DoughnutContainer>
            <div className="title">{title}</div>

            <Doughnut
                data={data}
                options={options}
                plugins={[
                    {
                        afterDraw: (chart) =>
                            handleDoughnutPlugins(
                                chart,
                                [range[0], range[1]],
                                [value, unit]
                            ),
                    },
                ]}
            />
        </DoughnutContainer>

    );
};

export default Gauge;
