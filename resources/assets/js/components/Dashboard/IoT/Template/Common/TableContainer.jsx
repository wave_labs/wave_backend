import React, { Component } from "react";
import {
    fetchIotReports,
    deleteIotReport,
    fetchIotReportsCoordForLineDraw
} from "redux-modules/iotReport/actions";
import { connect } from "react-redux";
import { Col, Row, Button, DatePicker, Input } from "antd";
import styled from "styled-components";
import { formatPosition } from "helpers";
import StopPropagation from "../../../Common/StopPropagation";
import RowOperation from "../../../Common/RowOperation";
import TablePagination from "../../../Common/TablePagination";
import moment from "moment";
import Drawer from "./Drawer";

const RangePicker = DatePicker.RangePicker;
const variables = { spacing: 8, width: 250 };

const DateTitle = styled.p`
  font-weight: bold;

  span {
    font-weight: normal;
  }
`;

const Field = styled.p`
    margin: 1px 0px;
  span {
    font-weight: bold;
  }
`;


class TableContainer extends Component {
    state = {
        drawerContent: null,
        mapCoordinates: null,
        filters: {
            value: undefined,
            date: undefined,
            device: this.props.device
        },
    };

    handleFilterChange = (filter, value) => {
        var { filters } = this.state;
        filters[filter] = value;
        this.setState({ filters });
    };

    handleFilters = () => {
        var { date } = this.state.filters;
        let newDate = date ? [...date] : [];

        if (newDate.length) {
            newDate = [
                moment(newDate[0]).format("YYYY-MM-DD"),
                moment(newDate[1]).format("YYYY-MM-DD"),
            ]
        }

        this.props.fetchIotReports(1, { ...this.state.filters, date: newDate });
    };

    handleTableChange = (pagination, sorter) => {
        this.props.fetchIotReports(pagination.current, {
            ...this.state.filters,
            pageSize: pagination.pageSize,
            order: [sorter.field, sorter.order],
        });
    };

    handleRowClick = (record) => {
        this.setState({
            drawerContent: record,
            mapCoordinates: formatPosition(record),
        });
    };

    handleDrawerClose = () => {
        this.setState({
            drawerContent: null,
            mapCoordinates: null,
        });
    };

    componentDidMount() {
        this.props.fetchIotReports(1, this.state.filters);
        this.props.fetchIotReportsCoordForLineDraw(this.state.filters)
    }

    componentDidUpdate(prevProps) {
        if (prevProps.device != this.props.device) {
            var { filters } = this.state;
            filters = {
                value: undefined,
                date: undefined,
                device: this.props.device
            };
            this.props.fetchIotReports(1, filters);
            this.props.fetchIotReportsCoordForLineDraw(filters)
        }
    }


    render() {
        let { drawerContent, mapCoordinates } = this.state;

        const FilterContainer = ({ children }) => (
            <div style={{ padding: variables.spacing }}>
                {children}
                <Button
                    type="primary" icon="search" style={{ width: variables.width }}
                    onClick={this.handleFilters}
                >
                    Search
                </Button>
            </div>
        )

        const columns = [
            {
                title: "ID",
                dataIndex: "id",
                sorter: true,
            },
            {
                title: "Date",
                dataIndex: "created_at",
                filterDropdown: () => (
                    <FilterContainer >
                        <RangePicker
                            value={this.state.filters.date}
                            format="YYYY/MM/DD"
                            style={{ width: variables.width, marginBottom: variables.spacing, display: 'block' }}
                            onChange={(value) => this.handleFilterChange("date", value)}
                        />
                    </FilterContainer>
                ),
                render: (created_at) => (
                    <div>
                        <DateTitle>
                            Created At: <span>{created_at}</span>
                        </DateTitle>
                    </div>
                ),

            },
            {
                title: "Fields",
                dataIndex: "fields",
                filterDropdown: () => (
                    <FilterContainer >
                        <Input
                            allowClear
                            value={this.state.filters.value}
                            placeholder="Specify a value for any of the fields"
                            style={{ width: variables.width, marginBottom: variables.spacing, display: 'block' }}
                            onChange={(element) => this.handleFilterChange("value", element.target.value)}
                        />
                    </FilterContainer>
                ),
                render: (fields) => (
                    <Row type="flex">
                        {fields.map((element, index) => (
                            <Col xs={24} md={12} lg={8} key={index}>
                                <Field>
                                    <span>{element.key + ": "}</span> {element.value}
                                </Field>
                            </Col>
                        ))}
                    </Row>
                ),
            },
            {
                title: "",
                key: "",
                render: (text, iotReport) => (
                    <StopPropagation>
                        <RowOperation
                            deleteRow
                            onDeleteConfirm={() => this.props.deleteIotReport(iotReport.id)}
                        />
                    </StopPropagation>
                ),
            },
        ];

        return (
            <div style={{ width: "100%", marginTop: "30px" }}>
                <Drawer
                    handleDrawerClose={this.handleDrawerClose}
                    visible={drawerContent && true}
                    content={drawerContent}
                    position={mapCoordinates}
                    device={{ type: this.props.deviceType, name: this.props.deviceName }}
                />
                {/* {this.props.deviceType.id == 5 && <img style={{ width: "200px" }} src="/images/partners/mac-interreg.png" />} */}

                <TablePagination
                    showSizeChanger
                    loading={this.props.loading}
                    columns={columns}
                    data={this.props.data}
                    meta={this.props.meta}
                    handleTableChange={this.handleTableChange}
                    onRow={(record) => ({
                        onClick: () => {
                            this.handleRowClick(record);
                        },
                    })}
                />
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchIotReports: (page, filters) =>
            dispatch(fetchIotReports(page, filters)),
        deleteIotReport: (id) => dispatch(deleteIotReport(id)),
        fetchIotReportsCoordForLineDraw: (id) => dispatch(fetchIotReportsCoordForLineDraw(id)),

    };
};

const mapStateToProps = (state) => {
    return {
        data: state.iotReport.data,
        meta: state.iotReport.meta,
        loading: state.iotReport.loading,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TableContainer);
