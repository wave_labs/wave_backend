import React from "react";
import { Doughnut } from "react-chartjs-2";
import { Fragment } from "react";

const Compass = ({ value = 90 }) => {
  const data = {
    labels: [""],
    datasets: [
      {
        label: "",
        data: [360],
        needleValue: value,
        backgroundColor: ["rgb(112, 162, 255)"],
        borderWidth: 0,
      },
    ],
  };
  const options = {
    legend: {
      display: false,
    },
    tooltips: {
      custom: function (tooltipModel) {
        tooltipModel.opacity = 0;
      },
    },
    cutoutPercentage: 90,
    animation: {
      duration: 0,
    },
  };

  return (
    <Fragment>
      <Doughnut
        data={data}
        options={options}
        plugins={[
          {
            afterDraw: (chart) => {
              var needleValue = parseInt(value) + 90;
              var angle = needleValue * (Math.PI / 180);

              var ctx = chart.chart.ctx;
              var cw = chart.chart.canvas.offsetWidth;
              var ch = chart.chart.canvas.offsetHeight;
              var cx = cw / 2;
              var cy = ch - ch / 2;

              ctx.beginPath();
              ctx.fillStyle = "rgba(112, 162, 255, 0.1)";
              ctx.arc(cx, cy, cx / 4, 0, Math.PI * 2);
              ctx.fill();

              ctx.translate(cx, cy);
              ctx.rotate(angle);
              ctx.beginPath();
              ctx.moveTo(-10, -(cw / 13));
              ctx.lineTo(-cx / 4, 0);
              ctx.lineTo(-10, cw / 13);
              ctx.fillStyle = "rgb(112, 162, 255)";
              ctx.fill();

              ctx.rotate(-angle);
              ctx.translate(-cx, -cy);
              ctx.beginPath();
              ctx.arc(cx, cy, cx / 5, 0, Math.PI * 2);
              ctx.fill();

              ctx.fillStyle = "rgb(255, 255, 255)";
              ctx.textAlign = "center";
              ctx.textBaseline = "middle";

              ctx.font = "16px B612";
              ctx.fontWeight = "bold";
              ctx.fillText(value + "º", cw / 2, ch / 2);

              ctx.fillStyle = "rgb(41, 41, 41)";
              ctx.font = "12px B612";
              ctx.fillText("N", cw / 2, ch / 6);
              ctx.fillText("E", cw / 1.5, ch / 2);
              ctx.fillText("S", cw / 2, ch / 1.2);
              ctx.fillText("W", cw / 3, ch / 2);
              ctx.fillText("NW", cw / 2.7, ch / 3.5);
              ctx.fillText("NE", cw / 1.6, ch / 3.5);
              ctx.fillText("SW", cw / 2.7, ch / 1.35);
              ctx.fillText("SE", cw / 1.6, ch / 1.35);
            },
          },
        ]}
      />
    </Fragment>
  );
};

export default Compass;
