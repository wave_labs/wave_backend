import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import MapBoxMap from "../../../Common/MapBoxMap";
import { Marker, Popup } from "react-map-gl";
import {
    exportIotReportCsv,
} from "redux-modules/iotReport/actions";
import IotReportPopUp from "../../IotReportContent/IotReportPopUp";
import { Switch } from "antd";
import LineDrawer from "../../IotReportContent/LineDrawer";
import MapPin from "../../../Common/MapPin";
import { colors } from "../../helper";
import styled from "styled-components";

const MapLineSwitch = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  z-index: 99;
`;

const LinePin = styled.div`
  width: 6px;
  height: 6px;
  transform: translate(-3px, -3px);
  background-color: ${(props) => props.color};
  border-radius: 50%;
`;

class IotReportMapList extends Component {
    state = {
        iotReportPopUp: null,
        isMapLineVisible: true,
    };

    showIotReport = () => {
        const { iotReportPopUp } = this.state;

        return (
            iotReportPopUp && (
                <Popup
                    tipSize={5}
                    longitude={parseFloat(iotReportPopUp.longitude)}
                    latitude={parseFloat(iotReportPopUp.latitude)}
                    closeOnClick={false}
                    onClose={() => this.setState({ iotReportPopUp: null })}
                >
                    <IotReportPopUp iotReport={iotReportPopUp} />
                </Popup>
            )
        );
    };

    render() {
        var { device } = this.props;
        const Start = () => (
            <svg
                height="100"
                viewBox="0 0 24 24"
                style={{
                    fill: "BROWN",
                    stroke: "none",
                    transform: `translate(-11px,-10px)`,
                }}
            >
                <path d="M2 1 h1 v1 h1 v1 h-1 v1 h-1 v-1 h-1 v-1 h1 z" />
            </svg>
        );

        return (

            <MapBoxMap fixedSize={this.props.fixedSize} size={450} zoom={8}>
                <MapLineSwitch>
                    <Switch
                        checkedChildren="Lines ON"
                        unCheckedChildren="Lines OFF"
                        defaultChecked={this.state.isMapLineVisible}
                        onChange={(e) =>
                            this.setState({
                                isMapLineVisible: e,
                            })
                        }
                    />
                </MapLineSwitch>

                {this.state.isMapLineVisible && this.props.iotReportsByCoordForLineDraw.length > 1 && (
                    <LineDrawer color="BROWN" points={this.props.iotReportsByCoordForLineDraw} />
                )}

                {this.props.iotReportsByCoordForLineDraw.map((coordinates, index) => (

                    <Marker
                        key={coordinates.report}
                        latitude={parseFloat(coordinates.latitude)}
                        longitude={parseFloat(coordinates.longitude)}
                    >
                        <div onClick={() => this.setState({ iotReportPopUp: coordinates })}>
                            {index == 0 ? (
                                <Start />
                            ) : coordinates.length === index + 1 ? (
                                <MapPin color={colors[index]} />
                            ) : (
                                <LinePin color="BROWN" />
                            )}
                        </div>
                    </Marker>


                ))}

                {
                    (device.category.latitude && device.category.longitude) &&
                    <Marker
                        longitude={parseFloat(device.category.longitude)}
                        latitude={parseFloat(device.category.latitude)}
                    >
                        <MapPin color="GREEN" />
                    </Marker>
                }


                {this.showIotReport()}
            </MapBoxMap>

        );
    }
}

export default connect(
    (state) => ({
        loadingExport: state.iotReport.loadingExport,
        iotReportsByCoordForLineDraw: state.iotReport.iotReportsByCoordForLineDraw,
    }),
    { exportIotReportCsv }
)(IotReportMapList);
