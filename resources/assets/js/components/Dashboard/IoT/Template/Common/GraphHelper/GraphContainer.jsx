import React from "react";
import styled from "styled-components";


const Container = styled.div`
  border: 1px solid #e6e6e6;
  border-radius: 6px;
  width: 100%;
  padding: 40px;
  background-color: rgb(253, 253, 253);
  box-shadow: 0px 0 20px 0px rgba(180, 180, 180, 0.2);

  h1 {
    font-size: 1.4em;
    font-weight: bold;
  }

  h2 {
    font-size: 1em;
    font-weight: bold;
    text-align: center;
    margin-top: 20px;
  }
`;

function GraphContainer({ title, children }) {
  return (
    <Container>
      <h1>{title}</h1>
      <div>{children}</div>
    </Container>

  );
}

export default GraphContainer;
