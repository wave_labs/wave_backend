import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import DrawerContainer from "../../../Common/DrawerContainer";
import MapBoxMap from "../../../Common/MapBoxMap";
import MapPin from "../../../Common/MapPin";
import { Marker } from "react-map-gl";
import { updateDrawerDimensions } from "helpers";
import { connect } from "react-redux";
import { fetchGpsGatewaySatellites, fetchGpsTagSatellites, resetGpsData } from "redux-modules/gps/actions";

const MapContainer = styled.div`
  width: 100%;
  margin-bottom: 30px;
  height: 400px;
`;

const More = styled.div`
  cursor: pointer;
  font-weight: bold;
  color: #78BCED;
  display: flex;
  justify-content: flex-end;
  transition: color .3s ease;

  &:hover {
    color: #5b95bf;
  }
`;


const StyledDiv = styled.div`
  font-size: 14;
  line-height: 22px;
  margin-bottom: 10px;
  color: rgba(0, 0, 0, 0.65);
`;

const StyledParagraph = styled.span`
  margin-right: 8px;
  display: inline-block;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.85);
  
  &:after {
    content: ":";
  }
`;

const DescriptionItem = ({ span, title, content }) => (
    <Col span={span}>
        <StyledDiv>
            <StyledParagraph>{title}</StyledParagraph>
            {content}
        </StyledDiv>
    </Col>
);

class Drawer extends Component {
    state = {
        drawerWidth: "400px",
    };

    updateDimensions() {
        this.setState({ drawerWidth: updateDrawerDimensions(window) });
    }

    componentDidMount = () => {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));

    }

    componentDidUpdate(prevProps) {
        if (prevProps.visible != this.props.visible) {
            if (!this.props.visible) {
                this.props.resetGpsData();
            }
        }
    }

    handleMore = () => {
        let { device, content } = this.props;
        if (device.name.includes("gateway") || device.name.includes("Gateway")) {
            this.props.fetchGpsGatewaySatellites({ report_id: content.id })
        } else if (device.name.includes("tag") || device.name.includes("Tag")) {
            this.props.fetchGpsTagSatellites({ report_id: content.id })
        }
    }

    render() {
        let { content, position, device, gpsData } = this.props;
        let { drawerWidth } = this.state;

        return (
            <DrawerContainer
                handleDrawerClose={this.props.handleDrawerClose}
                visible={this.props.visible}
                width={drawerWidth}
            >
                {content && (
                    <Row type="flex">
                        {position.lat != 0 && position.lng != 0 && (
                            <MapContainer>
                                <MapBoxMap defaultCenter={position}>
                                    <Marker longitude={position.lng} latitude={position.lat}>
                                        <MapPin />
                                    </Marker>
                                </MapBoxMap>
                            </MapContainer>
                        )}

                        <DescriptionItem span={12} title="ID" content={content.id} />
                        <DescriptionItem span={12} title="Date" content={content.collected_at} />

                        <DescriptionItem
                            span={24}
                            title="Fields"
                            content={
                                content.fields.length ? (
                                    <ul>
                                        {Object.values(content.fields).map((element) => (
                                            <li key={element.id}>
                                                <span>
                                                    {element.key}: {element.value}
                                                </span>
                                            </li>
                                        ))}
                                    </ul>
                                ) : (
                                    "-----"
                                )
                            }
                        />

                        {gpsData.length ?
                            <DescriptionItem
                                span={24}
                                title="Gps Data"
                                content={
                                    <Row>
                                        {gpsData.map((element) => {
                                            return (
                                                <Col span={12}>
                                                    <ul>
                                                        {
                                                            Object.entries(element).map((value) => (
                                                                <li>
                                                                    <span>{value[0]}</span>: <span>{value[1]}</span>
                                                                </li>
                                                            ))
                                                        }
                                                    </ul>
                                                </Col>
                                            )

                                        })}
                                    </Row>
                                }
                            /> : <></>}



                    </Row>

                )}


            </DrawerContainer>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        fetchGpsGatewaySatellites: (filters) =>
            dispatch(fetchGpsGatewaySatellites(filters)),
        fetchGpsTagSatellites: (filters) => dispatch(fetchGpsTagSatellites(filters)),
        resetGpsData: () => dispatch(resetGpsData()),
    };
};

const mapStateToProps = (state) => {
    return {
        gpsData: state.gps.data,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Drawer);
