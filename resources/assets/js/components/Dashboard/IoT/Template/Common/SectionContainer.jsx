import React from 'react';
import GraphContainer from './GraphHelper/GraphContainer'
import { connect } from "react-redux";
import { Col, Row } from 'antd';
import Gauge from './GraphHelper/Gauge';
import SingleHorizontalBar from './GraphHelper/SingleHorizontalBar'
import Compass from './GraphHelper/Compass';
import LineChart from './GraphHelper/LineChart';
import VerticalBarChart from './GraphHelper/VerticalBarChart';


function SectionContainer({ latestReport, sections, features, title, fieldsTimeline }) {

    function getValue(element) {
        var response = {};

        latestReport.fields.map((field, i) => {
            if (element.field_key == field.key) {
                response.value = latestReport.fields[i].value
            }
        });

        if (element.parameters.length) {
            element.parameters.map((parameter) => {
                response[parameter.name] = parameter.pivot.value;
            })
        }

        return response;
    }

    function getColumnSpan(element) {
        var feature = features.find((e) => {
            return element.feature_id == e.id;
        })
        var span = 12;

        switch (feature.name) {
            case "single horizontal bar":
                span = 24
                break;
            default:
                break;
        }

        return span;
    }

    function generateGraph(element) {
        var feature = features.find((e) => {
            return element.feature_id == e.id;
        })
        var content = <div></div>;
        if (!element.has_timeline) {
            var data = getValue(element);
        }

        switch (feature.name) {
            case "gauge":
                content = (
                    <Gauge key={element.id}
                        title={element.field_key}
                        range={[data.min, data.max]}
                        value={data.value}
                        unit={data.unit}
                    />
                );
                break;
            case "single horizontal bar":
                content = (
                    <SingleHorizontalBar
                        title={element.field_key}
                        range={[parseFloat(data.min), parseFloat(data.max)]}
                        value={parseFloat(data.value)}
                    />
                );
                break;
            case "compass chart":
                content = (
                    <Compass
                        value={data.value}
                    />
                );
                break;
            case "line chart":
                if (fieldsTimeline.values[element.field_key]) {
                    content = (
                        <LineChart
                            title={element.field_key}
                            labels={fieldsTimeline.labels}
                            displayGrid={true}
                            value={fieldsTimeline.values[element.field_key]}
                        />
                    );
                }

                break;
            case "bar chart":
                if (fieldsTimeline.values[element.field_key]) {
                    content = (
                        <VerticalBarChart
                            title={element.field_key}
                            labels={fieldsTimeline.labels}
                            displayGrid={true}
                            value={fieldsTimeline.values[element.field_key]}
                        />
                    );
                }
                break;
            default:
                break;
        }

        return content;
    }

    return (
        <GraphContainer title={title} display>
            {(latestReport.id && features.length) &&
                (
                    <Row type="flex" justify='space-around'>
                        {sections.map((element, index) => (
                            <Col md={24} lg={getColumnSpan(element)} key={index}>
                                {generateGraph(element)}
                            </Col>

                        ))}
                    </Row>
                )
            }
        </GraphContainer>
    );
}

const mapStateToProps = (state) => {
    return {
        latestReport: state.iotReport.latestReport,
        hasReportChanged: state.iotReport.hasReportChanged,
        features: state.iotFeature.selector,
        fieldsTimeline: state.iotReport.fieldsTimeline,
    };
};

export default connect(
    mapStateToProps,
    null
)(SectionContainer);
