import React from "react";
import { HorizontalBar } from "react-chartjs-2";
import { Fragment } from "react";

const SingleHorizontalBar = ({ value, title, range }) => {
  const options = {
    showScale: true,
    scales: {
      yAxes: [
        {
          stacked: true,

          gridLines: {
            display: false,
          },
        },
      ],
      xAxes: [
        {
          ticks: {
            beginAtZero: false,
            min: range[0],
            max: range[1],
            stepSize: (Math.abs(range[0]) - Math.abs(range[1])) / 5,
          },
          gridLines: {
            color: "#c5c5c556",
          },
        },
      ],
    },
    responsive: true,
    legend: {
      display: false,
    },
    tooltips: {
      enabled: false,
    },
    animation: {
      duration: 0,
    },
  };

  const getData = (canvas) => {
    const gradient = canvas.getContext("2d").createLinearGradient(0, 0, 300, 0);
    gradient.addColorStop(0, '#415683');
    gradient.addColorStop(1, '#a6ccfa');

    return {
      labels: [""],
      datasets: [
        {
          label: "",
          data:
            parseInt(value) < range[1] && parseInt(value) > range[0]
              ? [[range[0], value]]
              : parseInt(value) > range[1] && [[range[0], range[1]]],
          backgroundColor: gradient,
          borderWidth: 0,
        },
      ],
    }

  };

  return (
    <Fragment>
      <h2>{title}</h2>
      <HorizontalBar data={getData} options={options} height={30} />
    </Fragment>
  );
};

export default SingleHorizontalBar;
