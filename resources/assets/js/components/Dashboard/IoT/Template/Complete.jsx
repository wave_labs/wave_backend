import { Col, Row } from 'antd'
import React, { useEffect } from 'react'
import LoadingContainer from '../../../Common/LoadingContainer'
import IotDeviceInfo from '../IotReportContent/IotDeviceInfo'
import IotReportDrawerGraph from '../IotReportContent/IotReportDrawerGraph'
import MapContainer from './Common/MapContainer'
import TableContainer from './Common/TableContainer'
import SectionContainer from './Common/SectionContainer'
import { fetchSelector } from "redux-modules/iotFeature/actions";
import { connect } from "react-redux";
import styled from "styled-components";

const GraphSectionContainer = styled.div`
    &>div {
        margin-top: 30px;
    }

    div:nth-child(1){
        margin-top: 0px;
    }
`;


function Complete({ iotDevice, fetchSelector }) {
    useEffect(() => {
        fetchSelector();
    }, [iotDevice.id]);

    return (
        <LoadingContainer hasData={Object.keys(iotDevice).length}>
            <IotDeviceInfo iotDevice={iotDevice} />
            <IotReportDrawerGraph
                fields={iotDevice.fields}
                device={iotDevice.id}
            />

            <Row type='flex' gutter={32}>
                <Col xs={24} lg={12}>
                    <GraphSectionContainer>
                        <SectionContainer title={iotDevice.sections['2'].title} sections={iotDevice.sections['2'].content} />
                        <SectionContainer title={iotDevice.sections['3'].title} sections={iotDevice.sections['3'].content} />
                        <SectionContainer title={iotDevice.sections['4'].title} sections={iotDevice.sections['4'].content} />
                    </GraphSectionContainer>
                </Col>
                <Col xs={24} lg={12}>
                    <div className='map-container'>
                        <MapContainer device={iotDevice} />
                    </div>
                </Col>
            </Row>
            <Row type='flex'>
                <SectionContainer title={iotDevice.sections['5'].title} sections={iotDevice.sections['5'].content} />
            </Row>
            <TableContainer device={iotDevice.id} />

        </LoadingContainer>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSelector: () => dispatch(fetchSelector())
    };
};

export default connect(
    null,
    mapDispatchToProps
)(Complete);
