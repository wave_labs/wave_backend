import React from 'react'
import LoadingContainer from '../../../Common/LoadingContainer'
import IotDeviceInfo from '../IotReportContent/IotDeviceInfo'
import IotReportDrawerGraph from '../IotReportContent/IotReportDrawerGraph'
import TableContainer from './Common/TableContainer'

function Table({ iotDevice }) {
    return (
        <LoadingContainer hasData={Object.keys(iotDevice).length}>
            <IotDeviceInfo iotDevice={iotDevice} />
            {iotDevice.id == 5 && <img style={{ width: "200px" }} src="/images/partners/mac-interreg.png" />}
            <IotReportDrawerGraph
                fields={iotDevice.fields}
                device={iotDevice.id}
            />

            <TableContainer device={iotDevice.id} deviceType={iotDevice.type} deviceName={iotDevice.name} />
        </LoadingContainer>
    )
}

export default Table;
