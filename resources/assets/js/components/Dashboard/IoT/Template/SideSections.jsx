import { Col, Row } from 'antd'
import React, { Fragment, useEffect } from 'react'
import LoadingContainer from '../../../Common/LoadingContainer'
import IotDeviceInfo from '../IotReportContent/IotDeviceInfo'
import IotReportDrawerGraph from '../IotReportContent/IotReportDrawerGraph'
import MapContainer from './Common/MapContainer'
import TableContainer from './Common/TableContainer'
import SectionContainer from './Common/SectionContainer'
import { fetchSelector } from "redux-modules/iotFeature/actions";
import { connect } from "react-redux";
import styled from "styled-components";

const GraphSectionContainer = styled.div`
    &>div {
        margin-top: 30px;
    }

    div:nth-child(1){
        margin-top: 0px;
    }
`;


function SideSections({ iotDevice, fetchSelector }) {
    useEffect(() => {
        fetchSelector();
    }, [iotDevice]);

    return (
        <LoadingContainer hasData={Object.keys(iotDevice).length}>
            <IotDeviceInfo iotDevice={iotDevice} />
            <IotReportDrawerGraph
                fields={iotDevice.fields}
                device={iotDevice.id}
            />

            <Row type='flex' gutter={32}>
                <Col xs={24} lg={12}>
                    <GraphSectionContainer>
                        {iotDevice.id && Object.values(iotDevice.sections).map((section, index) => (
                            <Fragment key={index}>
                                {section.title &&
                                    <SectionContainer
                                        title={section.title}
                                        sections={section.content} />
                                }
                            </Fragment>
                        ))}
                    </GraphSectionContainer>
                </Col>
                <Col xs={24} lg={12}>
                    <div className='map-container'>
                        <MapContainer device={iotDevice} />
                    </div>
                </Col>
            </Row>

            <TableContainer device={iotDevice.id} />

        </LoadingContainer>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchIotDevice: (id) => dispatch(fetchIotDevice(id)),
        fetchSelector: () => dispatch(fetchSelector())
    };
};

export default connect(
    null,
    mapDispatchToProps
)(SideSections);
