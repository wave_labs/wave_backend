import React from "react";
import IotDeviceTypeModalContainer from "./IotDeviceTypeModalContainer";
import IotDeviceTypeTableFilterContainer from "./IotDeviceTypeTableFilterContainer";

const IotDeviceTypeContent = () => {
    return (
        <div>
            <IotDeviceTypeModalContainer />
            <IotDeviceTypeTableFilterContainer />
        </div>
    );
};

export default IotDeviceTypeContent;
