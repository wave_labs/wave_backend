import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/iotDeviceType/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class IotDeviceTypeRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      IotDeviceTypeSelector,
      loading,
      value,
      onChange,
      mode,
      children,
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Device Type"
        mode={mode}
      >
        {children}
        {IotDeviceTypeSelector.map((iotDeviceType) => (
          <Option value={iotDeviceType.id} key={iotDeviceType.id}>
            {iotDeviceType.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    IotDeviceTypeSelector: state.iotDeviceType.IotDeviceTypeSelector,
    loading: state.iotDeviceType.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotDeviceTypeRemoteSelectContainer);
