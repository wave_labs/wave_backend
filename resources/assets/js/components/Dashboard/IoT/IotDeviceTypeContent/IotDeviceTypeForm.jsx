import React, { Component } from "react";
import { Row, Col, Form, Input } from "antd";
import IotDataTypeRemoteCheckboxesContainer from "../../IotDataTypeContent/IotDataTypeRemoteCheckboxesContainer";

const FormItem = Form.Item;
const TextArea = Input.TextArea;

class IotDeviceTypeForm extends Component {
  state = {};

  rules = {
    name: [
      {
        required: true,
        message: "Name is required",
      },
    ],
    description: [
      {
        required: true,
        message: "Description is required",
      },
    ],
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { iotDeviceType, editing, form } = this.props;

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24}>
            <FormItem hasFeedback label="Name">
              {getFieldDecorator("name", {
                initialValue: iotDeviceType.name,
                rules: this.rules.name,
              })(<Input placeholder="Name" />)}
            </FormItem>
          </Col>
          <Col xs={24}>
            <FormItem hasFeedback label="Description">
              {getFieldDecorator("description", {
                initialValue: iotDeviceType.description,
                rules: this.rules.description,
              })(<TextArea placeholder="Description" />)}
            </FormItem>
          </Col>
          <Col xs={24}>
            <IotDataTypeRemoteCheckboxesContainer
              formItemTitle="Default fields"
              defaultFields={iotDeviceType.defaultFields}
              form={form}
            >
              {editing}
            </IotDataTypeRemoteCheckboxesContainer>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(IotDeviceTypeForm);
