import React, { Component } from "react";
import {
  fetchIotDeviceTypes,
  deleteIotDeviceType,
  setIotDeviceTypeEdit,
  updateIotDeviceType,
} from "redux-modules/iotDeviceType/actions";
import { connect } from "react-redux";
import RowOperation from "../../Common/RowOperation";
import TablePagination from "../../Common/TablePagination";
import TableFilterRow from "../../Common/TableFilterRow";
import styled from "styled-components";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";

const StyledEllipsis = styled.div`
  overflow-y: auto;
  text-overflow: ellipsis;
  width: 220px;
  max-height: 180px;
`;

class IotDeviceTypeTableFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.filters = {};
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "Name",
        dataIndex: "name",
      },
      {
        title: "Description",
        dataIndex: "description",
        render: (description) => <StyledEllipsis>{description}</StyledEllipsis>,
      },
      {
        title: "",
        key: "",
        render: (text, iotDeviceType) => (
          <RowOperation
            deleteRow
            updateRow
            onUpdateClick={() => this.onUpdateClick(iotDeviceType)}
            onDeleteConfirm={() =>
              this.props.deleteIotDeviceType(iotDeviceType.id)
            }
          />
        ),
      },
    ];
  }

  componentDidMount = () => {
    this.props.fetchIotDeviceTypes();
  };

  onUpdateClick = (iotDeviceType) => {
    this.props.setIotDeviceTypeEdit(iotDeviceType);
    this.props.startEditing();
  };

  onSearch = (search) => {
    this.filters = { ...this.filters, search };
    this.props.fetchIotDeviceTypes(1, this.filters);
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.filters = {
      ...this.filters,
      ...filters,
      order: [sorter.field, sorter.order],
    };
    this.props.fetchIotDeviceTypes(pagination.current, this.filters);
  };
  render() {
    return (
      <div>
        <TableFilterRow
          onSearch={(search) => this.onSearch(search)}
          searchPlaceholder="Search Device Types"
          onCreateClick={() => this.props.startCreating()}
        />
        <TablePagination
          loading={this.props.loading}
          columns={this.columns}
          handleTableChange={this.handleTableChange}
          data={this.props.data}
          meta={this.props.meta}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchIotDeviceTypes: (page, filters) =>
      dispatch(fetchIotDeviceTypes(page, filters)),
    deleteIotDeviceType: (id) => dispatch(deleteIotDeviceType(id)),
    activateIotDeviceType: (id) => dispatch(activateIotDeviceType(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setIotDeviceTypeEdit: (data) => dispatch(setIotDeviceTypeEdit(data)),
    updateIotDeviceType: (id, data) => dispatch(updateIotDeviceType(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.iotDeviceType.data,
    meta: state.iotDeviceType.meta,
    loading: state.iotDeviceType.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotDeviceTypeTableFilterContainer);
