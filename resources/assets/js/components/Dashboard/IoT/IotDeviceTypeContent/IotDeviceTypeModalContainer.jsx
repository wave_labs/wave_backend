import React, { Component } from "react";
import {
  createIotDeviceType,
  updateIotDeviceType,
  resetIotDeviceTypeEdit,
} from "redux-modules/iotDeviceType/actions";
import { resetModal } from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../Common/EditCreateModal";
import IotDeviceTypeForm from "./IotDeviceTypeForm";
import { alertActions } from "redux-modules/alert/actions";

class IotDeviceTypeModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, iotDeviceType) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });
        this.props
          .updateIotDeviceType(this.props.editIotDeviceType.id, iotDeviceType)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function(message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({ confirmLoading: false });
    this.props.resetIotDeviceTypeEdit();
    this.props.resetModal();
    form.resetFields();
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, iotDeviceType) => {
      if (err) {
        return;
      } else {
        this.setState({ confirmLoading: true });

        this.props
          .createIotDeviceType(iotDeviceType)
          .then((data) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function(message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  render() {
    return (
      <EditCreateModal
        titleEdit="Edit Device Type"
        titleCreate="Create Device Type"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={this.props.editing}
        creating={this.props.creating}
      >
        <IotDeviceTypeForm
          creating={this.props.creating}
          wrappedComponentRef={this.saveFormRef}
          iotDeviceType={this.props.editIotDeviceType}
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetIotDeviceTypeEdit: () => dispatch(resetIotDeviceTypeEdit()),
    resetModal: () => dispatch(resetModal()),
    createIotDeviceType: (data) => dispatch(createIotDeviceType(data)),
    updateIotDeviceType: (data, id) => dispatch(updateIotDeviceType(data, id)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    editIotDeviceType: state.iotDeviceType.editIotDeviceType,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotDeviceTypeModalContainer);
