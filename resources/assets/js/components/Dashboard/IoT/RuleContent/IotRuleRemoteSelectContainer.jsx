import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/iotRule/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class IotRuleRemoteSelectContainer extends Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  handleRemove = (e, index) => {
    e.stopPropagation();
    let { selectedOptions } = this.state;
    selectedOptions.splice(index, 1);
    this.setState({ selectedOptions });
  };

  render() {
    const { data, loading, value, onChange, disabled, placeholder = "Rules" } = this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
        allowClear
        disabled={disabled}
        style={{ width: "100%" }}
      >
        {data.map((element) => (
          <Option value={element.id} key={element.id}>
            {element.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.iotRule.selector,
    loading: state.iotRule.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotRuleRemoteSelectContainer);
