import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/iotFeature/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class IotFeatureRemoteSelectContainer extends Component {
  constructor(props) {
    super(props);

    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  handleRemove = (e, index) => {
    e.stopPropagation();
    let { selectedOptions } = this.state;
    selectedOptions.splice(index, 1);
    this.setState({ selectedOptions });
  };

  render() {
    const { data, loading, value, onChange, disabled } = this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Features"
        multiple
        disabled={disabled}
        style={{ width: "100%" }}
      >
        {data.map((element) => (
          <Option value={element.id} key={element.id}>
            {element.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.iotFeature.selector,
    loading: state.iotFeature.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotFeatureRemoteSelectContainer);
