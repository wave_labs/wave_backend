import React from "react";
import { Row } from "antd";
import styled from "styled-components";
import { Fragment } from "react";

const StyledCol = styled.div`
  border: 1px solid #e6e6e6;
  border-radius: 6px;
  width: 100%;
  padding: 10px 10px 50px 10px;
  background-color: rgb(253, 253, 253);
  box-shadow: 0px 0 10px 0px rgba(180, 180, 180, 0.1);

  h1 {
    font-size: 1.4em;
    font-weight: bold;
  }

  h2 {
    font-size: 1em;
    font-weight: bold;
    text-align: center;
    margin-top: 20px;
  }
`;

function GraphContainer({ title, children, firstContainer, display }) {
  return (
    <Fragment>
      {display && (
        <StyledCol style={{ marginTop: firstContainer ? "0px" : "20px" }}>
          <h1>{title}</h1>
          <Row>{children}</Row>
        </StyledCol>
      )}
    </Fragment>
  );
}

export default GraphContainer;
