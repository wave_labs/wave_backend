import React from "react";
import { Line } from "react-chartjs-2";
import { Fragment } from "react";

const WavePeriodGraph = ({ value, title, labels, displayGrid = false }) => {
  const data = {
    labels: labels,
    datasets: [
      {
        fill: false,
        label: "Average",
        borderColor: "#011c57",
        pointBackgroundColor: "#011c57",
        pointBorderWidth: 0,
        pointHoverRadius: 4,
        borderWidth: 2,
        data: value[0],
      },
      {
        fill: false,
        label: "Max",
        borderColor: "#2c76ff",
        pointBackgroundColor: "#2c76ff",
        pointBorderWidth: 0,
        pointHoverRadius: 4,
        borderWidth: 2,
        data: value[1],
      },

      {
        fill: false,
        label: "Significant",
        borderColor: "#a1cbff",
        pointBackgroundColor: "#a1cbff",
        pointBorderWidth: 0,
        pointHoverRadius: 4,
        borderWidth: 2,
        data: value[2],
      },
    ],
  };

  const options = {
    legend: {
      display: true,
      labels: {
        fontColor: "gray",
        boxBorder: 0,
      },
    },
    animation: {
      duration: 0,
    },
    scales: {
      yAxes: [
        {
          scaleLabel: {
            display: false,
          },
        },
      ],
      xAxes: [
        {
          ticks: {
            callback: (aValue, index) => {
              return value[0].length > 20
                ? index % 10 != 0
                  ? ""
                  : aValue
                : aValue;
            },
          },
          scaleLabel: {
            display: false,
          },
          gridLines: {
            display: displayGrid,
          },
        },
      ],
    },
  };

  return (
    <Fragment>
      <h2>{title}</h2>
      <Line data={data} options={options} />
    </Fragment>
  );
};

export default WavePeriodGraph;
