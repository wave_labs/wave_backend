import React from "react";
import { Line } from "react-chartjs-2";
import { Fragment } from "react";
import styled from "styled-components";

const DoughnutContainer = styled.div`
  position: relative;

  img {
    position: absolute;
    bottom: 0;
    left: 50%;
    margin-left: -20px;
    width: 40px;
  }
`;

const WavePeriodGraph = ({ value, title, labels, displayGrid = false }) => {
  const data = {
    labels: labels,
    datasets: [
      {
        fill: true,
        lineTension: 0.1,
        backgroundColor: "rgba(112, 162, 255, 0.3)",
        borderColor: "rgb(112, 162, 255)",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgb(112, 162, 255)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointRadius: 2,
        pointHitRadius: 10,
        data: value,
      },
    ],
  };
  const options = {
    legend: {
      display: false,
    },
    animation: {
      duration: 0,
    },
    showXLabels: 5,
    scales: {
      yAxes: [
        {
          scaleLabel: {
            display: false,
          },
        },
      ],
      xAxes: [
        {
          ticks: {
            callback: (aValue, index) => {
              return value.length > 20
                ? index % 10 != 0
                  ? ""
                  : aValue
                : aValue;
            },
          },
          scaleLabel: {
            display: false,
          },
          gridLines: {
            display: displayGrid,
          },
        },
      ],
    },
  };

  return (
    <Fragment>
      <h2>{title}</h2>
      <Line data={data} options={options} />
    </Fragment>
  );
};

export default WavePeriodGraph;
