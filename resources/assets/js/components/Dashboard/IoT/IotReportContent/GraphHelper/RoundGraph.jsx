import React from "react";
import { Doughnut } from "react-chartjs-2";
import { Fragment } from "react";
import styled from "styled-components";

const DoughnutContainer = styled.div`
  position: relative;

  img {
    position: absolute;
    bottom: 0;
    left: 50%;
    margin-left: -15px;
    width: 30px;
  }
`;

const RoundGraph = ({ value, title, icon, range, unit }) => {
  const data = {
    labels: [""],
    datasets: [
      {
        label: "",
        data: [
          (100 / (range[1] - range[0])) * value - range[0],
          100 - (100 / (range[1] - range[0])) * value - range[0],
        ],
        backgroundColor: ["rgb(112, 162, 255)", "rgba(0, 89, 255, 0)"],
        borderColor: ["rgba(0, 89, 255, 0)", "rgb(112, 162, 255)"],
        borderWidth: 1,
      },
    ],
  };
  const options = {
    rotation: 0.8 * Math.PI,
    circumference: 1.4 * Math.PI,
    legend: {
      display: false,
    },
    animation: {
      duration: 0,
    },
    tooltips: {
      custom: function(tooltipModel) {
        tooltipModel.opacity = 0;
      },
    },
    cutoutPercentage: 90,
  };

  const handleDoughnutPlugins = (chart, range, value) => {
    var ctx = chart.chart.ctx;
    var cw = chart.chart.canvas.offsetWidth;
    var ch = chart.chart.canvas.offsetHeight;

    ctx.textAlign = "center";
    ctx.textBaseline = "middle";

    ctx.font = "18px B612";
    ctx.fontWeight = "bold";
    ctx.fillText(value[0], cw / 2, ch / 2);

    ctx.font = "12px B612";
    ctx.fontWeight = "normal";
    ctx.fillText(value[1], cw / 2, ch / 1.7);

    ctx.font = "10px B612";
    ctx.fillText(range[0], cw / 3.3, ch - 4);
    ctx.fillText(range[1], cw / 1.45, ch - 4);
  };

  return (
    <Fragment>
      <h2>{title}</h2>
      <DoughnutContainer>
        <img src={icon} />

        <Doughnut
          data={data}
          options={options}
          plugins={[
            {
              afterDraw: (chart) =>
                handleDoughnutPlugins(
                  chart,
                  [range[0], range[1]],
                  [value, unit]
                ),
            },
          ]}
        />
      </DoughnutContainer>
    </Fragment>
  );
};

export default RoundGraph;
