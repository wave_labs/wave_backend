import React from "react";
import { HorizontalBar } from "react-chartjs-2";
import { Fragment } from "react";

const ConnectivityGraph = ({ value, title, range }) => {
  const options = {
    showScale: true,
    scales: {
      yAxes: [
        {
          stacked: true,

          gridLines: {
            display: false,
          },
        },
      ],
      xAxes: [
        {
          ticks: {
            beginAtZero: false,
            min: range[0],
            max: range[3],
            stepSize: (Math.abs(range[0]) - Math.abs(range[0])) / 10,
          },
          gridLines: {
            color: "#c5c5c556",
          },
        },
      ],
    },
    responsive: true,
    legend: {
      display: false,
    },
    tooltips: {
      enabled: false,
    },
    animation: {
      duration: 0,
    },
  };

  const data = {
    labels: [""],
    datasets: [
      {
        label: "",
        data:
          parseInt(value) < range[1] && parseInt(value) > range[0]
            ? [[range[0], value]]
            : parseInt(value) > range[1] && [[range[0], range[1]]],
        backgroundColor: ["#596fa0"],
        borderWidth: 0,
      },
      {
        label: "1",
        data:
          parseInt(value) < range[2] && parseInt(value) > range[1]
            ? [[range[1], value]]
            : parseInt(value) > range[2] && [[range[1], range[2]]],
        backgroundColor: ["#70a2ff"],
        borderWidth: 0,
      },
      {
        label: "2",
        data: parseInt(value) < range[3] &&
          parseInt(value) > range[2] && [[range[2], value]],
        backgroundColor: ["#d7e9ff"],
        borderWidth: 0,
      },
    ],
  };
  return (
    <Fragment>
      <h2>{title}</h2>
      <HorizontalBar data={data} options={options} height={45} />
    </Fragment>
  );
};

export default ConnectivityGraph;
