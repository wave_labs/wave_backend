import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchIotReports } from "redux-modules/iotReport/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class IotReportRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchIotReports(1, { search });
  };

  render() {
    const { data, loading, value, onChange, mode, children } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="IotReport"
        mode={mode}
      >
        {children}
        {data.map((d) => (
          <Option value={d.id} key={d.id}>
            {d.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchIotReports: (page, filters) => dispatch(fetchIotReports(page, filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.litter.data,
    loading: state.litter.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotReportRemoteSelectContainer);
