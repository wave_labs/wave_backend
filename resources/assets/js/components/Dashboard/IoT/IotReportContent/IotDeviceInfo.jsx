import React from "react";
import { dimensions } from "helpers";
import { Row, Col } from "antd";
import styled from "styled-components";
import { Fragment } from "react";

const StyledTitle = styled.div`
  display: inline-block;
  margin: 2px 0px;
  margin-right: 2px;
  color: black;
`;

const InfoContainer = styled(Col)`
  color: #777;
`;

const SectionTitle = styled.h3`
  font-size: 1.2em;
  margin: 10px 0px;
  font-weight: bold;
`;

const DeviceName = styled.h2`
  font-weight: bold;
  font-size: 1.6em;
  color: black;
`;

const Image = styled.img`
  width: 70%;

  @media (max-width: ${dimensions.xs}) {
    width: 96%;
    margin-bottom: 20px;
  }
`;

const ImageContainer = styled(Col)`
  display: flex !important;
  align-items: center;
  justify-content: center !important;
`;

const ItemColumn = styled(Col)`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const IotDeviceInfo = ({ iotDevice }) => {
    const SectionItem = ({ name, value }) => (
        <ItemColumn lg={8} md={12} xs={24}>
            <StyledTitle> {name}: </StyledTitle>
            {value}
        </ItemColumn>
    );

    const Section = ({ name, children }) => (
        <Fragment>
            <SectionTitle> {name} </SectionTitle>
            {children}
        </Fragment>
    );

    return (
        <Row type="flex" justify="space-between" style={{ marginBottom: "20px" }}>
            <ImageContainer xs={24} md={10} lg={6}>
                <Image
                    alt={iotDevice.name}
                    src={`${window.location.origin}/` + iotDevice.img}
                />
            </ImageContainer>

            <InfoContainer xs={24} md={14} lg={18}>
                <DeviceName>{iotDevice.name}</DeviceName>

                <Section name="Device Information">
                    <Row>
                        <SectionItem name="ID" value={iotDevice.id} />
                        <SectionItem name="Serial Number" value={iotDevice.serial_number} />
                        <SectionItem name="Device Type" value={iotDevice.type.name} />
                        <SectionItem
                            name="Environment"
                            value={iotDevice.environment.name}
                        />
                        <SectionItem name="State" value={iotDevice.state.name} />
                        <SectionItem name="Date" value={iotDevice.created_at} />
                    </Row>
                </Section>
                <Section name="Device Users">
                    <Row>
                        {Object.values(iotDevice.users).map(function (element, index) {
                            return (
                                <ItemColumn key={index} lg={8} md={12} xs={24}>
                                    {element.email}
                                </ItemColumn>
                            );
                        })}
                    </Row>
                </Section>

                <Section name="Device Fields">
                    <Row>
                        {Object.values(iotDevice.fields).map(function (element, index) {
                            return (
                                <ItemColumn key={index} md={8} xs={12}>
                                    {element.key}
                                </ItemColumn>
                            );
                        })}
                    </Row>
                </Section>
            </InfoContainer>
        </Row>
    );
};

export default IotDeviceInfo;
