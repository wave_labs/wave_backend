import { Col, Row, Switch } from "antd";
import React, { Component } from "react";
import {
  latestIotReport,
  fetchWaveMeasurements,
  pushToWaveMeasurements,
} from "redux-modules/iotReport/actions";
import { fetchIotGateway } from "redux-modules/iotDevice/actions";
import IotDeviceInfo from "./IotDeviceInfo";
import styled from "styled-components";
import { connect } from "react-redux";
import MapBoxMap from "../../Common/MapBoxMap";
import ConnectivityGraph from "./GraphHelper/ConnectivityGraph";
import RoundGraph from "./GraphHelper/RoundGraph";
import DirectionGraph from "./GraphHelper/DirectionGraph";
import WavePeriodGraph from "./GraphHelper/WavePeriodGraph";
import WaveHeightGraph from "./GraphHelper/WaveHeightGraph";
import IotReportDrawerGraph from "./IotReportDrawerGraph";
import { Marker } from "react-map-gl";
import moment from "moment";
import { Fragment } from "react";

const PowerIndicator = styled.div`
  border-radius: 50%;
  display: inline-block;
  width: 10px;
  height: 10px;
  background-color: ${(props) => (props.ON ? "green" : "red")};
  margin-right: 3px;
`;
const DateTime = styled.div`
  color: #777;
  font-size: 0.8em;
  margin: 0 0 5px 0;
`;

const StyledCol = styled.div`
  border: 1px solid #e6e6e6;
  border-radius: 6px;
  width: 100%;
  padding: 10px;
  background-color: rgb(253, 253, 253);
  box-shadow: 0px 0 0 5px rgba(240, 240, 240, 0.1);

  h1 {
    font-size: 1.4em;
    font-weight: bold;
  }

  h2 {
    font-size: 1em;
    font-weight: bold;
    text-align: center;
    margin-top: 20px;
  }
`;

const MapIcon = styled.img`
  width: 22px;
  position: absolute;
  top: -11px;
`;

class IotReportBuoyTemplate extends Component {
  state = {
    displayGrid: false,
    recentData: {
      WaveDirection: undefined,
      RSSI: undefined,
      SNR: undefined,
      BatteryVoltage: undefined,
      BMP280_Temperature: undefined,
      BMP280_Pressure: undefined,
      Latitude: undefined,
      Longitude: undefined,
      Gateway: undefined,
    },
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.hasReportChanged != this.props.hasReportChanged) {
      let { recentData } = this.state;
      let labels = [],
        max = [],
        significant = [],
        period = [],
        average = [];

      labels.push(moment(this.props.data.date).format("HH:mm:ss"));

      this.props.data.fields.map((element) => {
        switch (element.type) {
          case "Height_Max":
            max.push(element.value);
            break;
          case "Height_Significant":
            significant.push(element.value);
            break;
          case "Height_Average":
            average.push(element.value);
            break;
          case "WavePeriod":
            period.push(element.value);
            break;
          default:
            break;
        }

        recentData[element.type] = element.value;
      });
      prevState.recentData.RSSI !== undefined &&
        this.props.pushToWaveMeasurements({
          labels: labels,
          max: max,
          average: average,
          significant: significant,
          period: period,
        });
      this.props.fetchIotGateway(recentData["Gateway"]);
      this.setState({ recentData });
    }
  }

  componentDidMount() {
    this.props.latestIotReport({ device: this.props.device });
    this.props.fetchWaveMeasurements({ device: this.props.device });

    let intervalFunction = setInterval(() => {
      this.props.latestIotReport({ device: this.props.device });
    }, 5000);

    this.setState({ intervalFunction: intervalFunction });
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalFunction);
  }

  handleDisplayGrid = (e) => {
    this.setState({ displayGrid: e });
  };

  render() {
    return (
      <div>
        {Object.keys(this.props.iotDevice).length > 0 && (
          <IotDeviceInfo iotDevice={this.props.iotDevice} />
        )}
        <br />

        <IotReportDrawerGraph
          fields={[
            { value: "Battery Voltage", name: "Battery" },
            { value: "Temperature", name: "Temperature" },
            { value: "Pressure", name: "Pressure" },
            { value: "Wave Direction", name: "Wave Direction" },
            { value: "Wave Period", name: "Wave Period" },
            {
              value: "Wave Height Significant",
              name: "Wave Height Significant",
            },
            { value: "Wave Height Max", name: "Wave Height Max" },
            { value: "Wave Height Average", name: "Wave Height Average" },
          ]}
          device={this.props.device}
        />

        <Row type="flex" gutter={16}>
          <Col md={24} xs={24} lg={12}>
            <StyledCol style={{ marginTop: 0 }}>
              <h1 style={{ marginBottom: 0 }}>Connectivity</h1>
              <DateTime>{this.props.data.date}</DateTime>
              <Row>
                <Row>
                  {this.props.currentGateway.state.id == 1 && (
                    <Fragment>
                      <PowerIndicator ON style={{ marginLeft: 10 }} />
                      <span>{this.props.currentGateway.name}</span>
                    </Fragment>
                  )}

                  <PowerIndicator
                    style={{ marginLeft: 10 }}
                    ON={this.props.currentDevice.state.id == 1 && true}
                  />
                  <span>Buoy</span>
                </Row>

                <Row type="flex" justify="space-between" align="middle">
                  <Col xs={24} lg={12}>
                    <ConnectivityGraph
                      title="RSSI"
                      range={[-120, -90, -50, 0]}
                      value={this.state.recentData.RSSI}
                      key={this.state.recentData.RSSI}
                    />
                    <ConnectivityGraph
                      title="SNR"
                      range={[-20, -10, 0, 10]}
                      value={this.state.recentData.SNR}
                      key={this.state.recentData.SNR}
                    />
                  </Col>
                  <Col xs={24} lg={12}>
                    <RoundGraph
                      title="Battery"
                      icon="/images/iot/battery.svg"
                      range={[0, 4.2]}
                      value={this.state.recentData.BatteryVoltage}
                      unit="Volts"
                      key={this.state.recentData.BatteryVoltage}
                    />
                  </Col>
                </Row>
              </Row>
            </StyledCol>

            <StyledCol style={{ marginTop: "20px" }}>
              <h1>BMP280</h1>
              <Row>
                <Col xs={24} lg={12}>
                  <RoundGraph
                    title="Temperature"
                    icon="/images/iot/temperature.svg"
                    range={[-40, 85]}
                    value={this.state.recentData.BMP280_Temperature}
                    unit="Celsius"
                    key={this.state.recentData.BMP280_Temperature}
                  />
                </Col>
                <Col xs={24} lg={12}>
                  <RoundGraph
                    title="Pressure"
                    icon="/images/iot/pressure.svg"
                    range={[300, 1200]}
                    value={this.state.recentData.BMP280_Pressure}
                    unit="hPa"
                    key={this.state.recentData.BMP280_Pressure}
                  />
                </Col>
              </Row>
            </StyledCol>

            <StyledCol style={{ marginTop: "20px" }}>
              <h1>Wave Direction</h1>
              <Row type="flex" justify="center">
                <Col xs={24} lg={14}>
                  <DirectionGraph
                    value={this.state.recentData.WaveDirection}
                    key={this.state.recentData.WaveDirection}
                  />
                </Col>
              </Row>
            </StyledCol>
          </Col>

          <Col xs={24} md={24} lg={12}>
            <div className="map-container">
              <MapBoxMap zoom={10}>
                <Marker
                  longitude={parseFloat(
                    this.props.currentGateway.category.position.longitude
                  )}
                  latitude={parseFloat(
                    this.props.currentGateway.category.position.latitude
                  )}
                >
                  <MapIcon src="/images/iot/gateway.svg" />
                </Marker>

                {this.state.recentData.Latitude && (
                  <Marker
                    longitude={parseFloat(this.state.recentData.Longitude)}
                    latitude={parseFloat(this.state.recentData.Latitude)}
                  >
                    <MapIcon src="/images/iot/buoy.svg" />
                  </Marker>
                )}
              </MapBoxMap>
            </div>
          </Col>
          <StyledCol style={{ marginTop: "20px" }}>
            <h1>Wave Measurements</h1>
            <Row>
              <Row type="flex" justify="end">
                <Switch
                  checkedChildren="Hide Grid"
                  unCheckedChildren="Display Grid"
                  defaultChecked={this.state.displayGrid}
                  onChange={this.handleDisplayGrid}
                />
              </Row>
              <Row type="flex" justify="space-between" align="top">
                <Col xs={24} md={12}>
                  <WavePeriodGraph
                    title="Period"
                    labels={this.props.waveMeasurements.labels}
                    displayGrid={this.state.displayGrid}
                    value={this.props.waveMeasurements.period}
                  />
                </Col>

                <Col xs={24} md={12}>
                  <WaveHeightGraph
                    title="Height"
                    labels={this.props.waveMeasurements.labels}
                    displayGrid={this.state.displayGrid}
                    value={[
                      this.props.waveMeasurements.average,
                      this.props.waveMeasurements.max,
                      this.props.waveMeasurements.significant,
                    ]}
                  />
                </Col>
              </Row>
            </Row>
          </StyledCol>
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    latestIotReport: (filters) => dispatch(latestIotReport(filters)),
    fetchWaveMeasurements: (filters) =>
      dispatch(fetchWaveMeasurements(filters)),
    pushToWaveMeasurements: (data) => dispatch(pushToWaveMeasurements(data)),
    fetchIotGateway: (id) => dispatch(fetchIotGateway(id)),
  };
};
const mapStateToProps = (state) => {
  return {
    data: state.iotReport.latestReport,
    hasReportChanged: state.iotReport.hasReportChanged,
    waveMeasurements: state.iotReport.waveMeasurements,
    currentDevice: state.iotDevice.current,
    currentGateway: state.iotDevice.currentGateway,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotReportBuoyTemplate);
