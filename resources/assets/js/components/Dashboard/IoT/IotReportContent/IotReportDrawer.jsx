import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import DrawerContainer from "../../Common/DrawerContainer";
import MapBoxMap from "../../Common/MapBoxMap";
import MapPin from "../../Common/MapPin";
import { Marker } from "react-map-gl";
import { updateDrawerDimensions } from "helpers";

const MapContainer = styled.div`
  width: 100%;
  margin-bottom: 30px;
  height: 400px;
`;

const StyledDiv = styled.div`
  font-size: 14;
  line-height: 22px;
  margin-bottom: 10px;
  color: rgba(0, 0, 0, 0.65);
`;

const StyledParagraph = styled.span`
  margin-right: 8px;
  display: inline-block;
  font-weight: bold;
  color: rgba(0, 0, 0, 0.85);
  &:after {
    content: ":";
  }
`;

const DescriptionItem = ({ span, title, content }) => (
  <Col span={span}>
    <StyledDiv>
      <StyledParagraph>{title}</StyledParagraph>
      {content}
    </StyledDiv>
  </Col>
);

class IotReportDrawer extends Component {
  state = {
    drawerWidth: "400px",
  };

  updateDimensions() {
    this.setState({ drawerWidth: updateDrawerDimensions(window) });
  }

  componentDidMount = () => {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  };

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  onUpdateClick = (sighting) => {
    this.props.setCurrentSighting(sighting);
    this.props.startEditing();
  };

  render() {
    let { content, position } = this.props;
    let { drawerWidth } = this.state;

    return (
      <DrawerContainer
        handleDrawerClose={this.props.handleDrawerClose}
        visible={this.props.visible}
        width={drawerWidth}
      >
        {content && (
          <Row type="flex">
            {position.lat != 0 && position.lng != 0 && (
              <MapContainer>
                <MapBoxMap defaultCenter={position} fxeidSize={true}>
                  <Marker longitude={position.lng} latitude={position.lat}>
                    <MapPin />
                  </Marker>
                </MapBoxMap>
              </MapContainer>
            )}

            <DescriptionItem span={12} title="Id" content={content.id} />
            <DescriptionItem span={12} title="Date" content={content.date} />

            <DescriptionItem
              span={24}
              title="Fields"
              content={
                content.fields.length > 0 ? (
                  <ul>
                    {Object.values(content.fields).map(function(
                      element,
                      index
                    ) {
                      return (
                        <li key={index}>
                          <span>
                            {element.type}: {element.value}
                          </span>
                        </li>
                      );
                    })}
                  </ul>
                ) : (
                  "-----"
                )
              }
            />
          </Row>
        )}
      </DrawerContainer>
    );
  }
}

export default IotReportDrawer;
