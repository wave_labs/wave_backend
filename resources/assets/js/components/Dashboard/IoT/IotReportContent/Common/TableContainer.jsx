import React, { Component } from "react";
import {
  fetchIotReports,
  deleteIotReport,
} from "redux-modules/iotReport/actions";
import { connect } from "react-redux";
import { Col, message, Row } from "antd";
import RowOperation from "../../../Common/RowOperation";
import styled from "styled-components";
import TablePagination from "../../../Common/TablePagination";
import { startCreating } from "redux-modules/editCreateModal/actions";
import { formatPosition } from "helpers";
import StopPropagation from "../../../Common/StopPropagation";
import IotReportDrawer from "../IotReportDrawer";

message.config({
  maxCount: 1,
});

const DateTitle = styled.p`
  font-weight: bold;

  span {
    font-weight: normal;
  }
`;

class TableContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      drawerContent: null,
      mapCoordinates: null,
    };
    this.columns = [
      {
        title: "ID",
        dataIndex: "id",
        sorter: true,
      },
      {
        title: "Date",
        dataIndex: "created_at",
        render: (fields, iotReport) => (
          <div>
            <DateTitle>
              Created At:{" "}
              <span>
                {iotReport.created_at ? iotReport.created_at : "Not specified"}
              </span>
            </DateTitle>
            <DateTitle>
              Collected At: <span>{iotReport.date}</span>
            </DateTitle>
          </div>
        ),
      },
      {
        title: "Fields",
        dataIndex: "fields",
        render: (fields, iotReport) => (
          <Row type="flex">
            {this.getFieldsArray(iotReport.fields).map((element, index) => (
              <Col xs={24} lg={24 / element.length} key={index}>
                {element.map((e, i) => (
                  <p style={{ margin: "1px 0px" }} key={i}>
                    {e.key + ": " + e.value}
                  </p>
                ))}
              </Col>
            ))}
          </Row>
        ),
      },
      {
        title: "",
        key: "",
        render: (text, iotReport) => (
          <StopPropagation>
            <RowOperation
              deleteRow
              onDeleteConfirm={() => this.props.deleteIotReport(iotReport.id)}
            />
          </StopPropagation>
        ),
      },
    ];
  }

  getFieldsArray = (fields) => {
    let size = fields.length;
    let response = [];

    var colNumber = size > 10 ? 3 : size > 5 ? 2 : 1; //Number of columns
    var itemsPerCol = size / colNumber; //Number of columns
    var start = 0,
      end = itemsPerCol;

    for (let index = 0; index < colNumber; index++) {
      response[index] = fields.slice(start, end);
      start = end;
      end += itemsPerCol;
    }
    return response;
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.props.fetchIotReports(pagination.current, {
      ...this.props.filters,
      pageSize: pagination.pageSize,
      order: [sorter.field, sorter.order],
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  handleRowClick = (record) => {
    this.setState({
      drawerContent: record,
      mapCoordinates: formatPosition(record),
    });
  };

  handleDrawerClose = () => {
    this.setState({
      drawerContent: null,
      mapCoordinates: null,
    });
  };

  onSizeChange = (current, pageSize) => {
    console.log(pageSize);
  };

  render() {
    let { drawerContent, mapCoordinates } = this.state;

    return (
      <div style={{ width: "100%" }}>
        <IotReportDrawer
          handleDrawerClose={this.handleDrawerClose}
          visible={drawerContent && true}
          content={drawerContent}
          position={mapCoordinates}
        />

        <TablePagination
          showSizeChanger={true}
          loading={this.props.loading}
          columns={this.columns}
          data={this.props.data}
          meta={this.props.meta}
          handleTableChange={this.handleTableChange}
          onRow={(record) => ({
            onClick: () => {
              this.handleRowClick(record);
            },
          })}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchIotReports: (page, filters) =>
      dispatch(fetchIotReports(page, filters)),
    deleteIotReport: (id) => dispatch(deleteIotReport(id)),
    startCreating: () => dispatch(startCreating()),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.iotReport.data,
    meta: state.iotReport.meta,
    loading: state.iotReport.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TableContainer);
