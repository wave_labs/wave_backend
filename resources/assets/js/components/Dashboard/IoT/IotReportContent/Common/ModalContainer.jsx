import React, { Component } from "react";
import {
  createIotReport,
  updateIotReport,
  resetCurrentIotReport,
} from "redux-modules/iotReport/actions";
import { resetModal } from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import EditCreateModal from "../../../Common/EditCreateModal";
import IotReportForm from "../IotReportForm.jsx";
import moment from "moment";
import { alertActions } from "redux-modules/alert/actions";

class ModalContainer extends Component {
  state = {
    confirmLoading: false, //modal button loading icon
    fileList: [],
    sourceType: null,
  };

  onCancel = () => {
    this.resetModalForm();
  };

  onOkEditClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, iotReport) => {
      if (err) {
        return;
      } else {
        const formatedDate = moment(iotReport.date).format("YYYY-MM-DD");
        this.setState({ confirmLoading: true });
        this.props
          .updateIotReport(this.props.currentIotReport.id, {
            ...iotReport,
            date: formatedDate,
          })
          .then((response) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function(message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  resetModalForm = () => {
    const form = this.formRef.props.form;
    this.setState({
      confirmLoading: false,
      sourceType: null,
      fileList: null,
      iotReport_sub_category: [],
      iotReport_category: [],
    });
    this.props.resetCurrentIotReport();
    this.props.resetModal();
    form.resetFields();
  };

  toggleChildMenu = (e) => {
    this.setState({
      sourceType: e,
    });
  };

  onOkCreateClick = () => {
    const form = this.formRef.props.form;
    form.validateFields((err, iotReport) => {
      if (err) {
        return;
      } else {
        let formData = new FormData();

        if (iotReport.source != "dive") {
          formData.append("photo", this.state.fileList);
        }

        formData.append("date", moment(iotReport.date).format("YYYY-MM-DD"));
        formData.append("source", iotReport.source);
        formData.append("latitude", iotReport.latitude);
        formData.append("longitude", iotReport.longitude);
        formData.append("user_id", iotReport.user_id);

        if (iotReport.collected == true) formData.append("collected", 1);
        else formData.append("collected", 0);
        if (iotReport.multiple == true) formData.append("multiple", 1);
        else formData.append("multiple", 0);

        for (var i = 0; i < iotReport.keys.length; i++) {
          if (iotReport.CategoriesArrays[iotReport.keys[i]][1]) {
            if (
              formData.get("iotReport_sub_category[]") !=
              iotReport.CategoriesArrays[iotReport.keys[i]][1]
            ) {
              formData.append(
                "iotReport_sub_category[]",
                iotReport.CategoriesArrays[iotReport.keys[i]][1]
              );
            }
          }

          if (
            formData.get("iotReport_category[]") !=
            iotReport.CategoriesArrays[iotReport.keys[i]][0]
          ) {
            formData.append(
              "iotReport_category[]",
              iotReport.CategoriesArrays[iotReport.keys[i]][0]
            );
          }
        }

        if (iotReport.source == "beach") {
          formData.append("radius", iotReport.radius);
          formData.append("quantity", iotReport.quantity);
        }

        this.setState({ confirmLoading: true });
        this.props
          .createIotReport(formData)
          .then((response) => {
            this.resetModalForm();
          })
          .catch((error) => {
            this.setState({ confirmLoading: false });
            let messages = [];

            Object.values(error.response.data.errors).map(function(message) {
              messages.push(message[0]);
            });

            this.props.errorAlert({
              description: messages,
            });
          });
      }
    });
  };

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };

  handlePhoto = (photo) => {
    this.setState({ fileList: photo });
  };

  render() {
    const { editing, creating } = this.props;
    return (
      <EditCreateModal
        titleEdit="IotReport"
        titleCreate="IotReport"
        onOkEditClick={() => this.onOkEditClick()}
        onOkCreateClick={() => this.onOkCreateClick()}
        confirmLoading={this.state.confirmLoading}
        onCancel={() => this.onCancel()}
        editing={editing}
        creating={creating}
      >
        <IotReportForm
          creating={creating}
          editing={editing}
          wrappedComponentRef={this.saveFormRef}
          iotReport={this.props.currentIotReport}
          handlePhoto={this.handlePhoto}
          toggleChildMenu={this.toggleChildMenu}
          sourceType={
            this.state.sourceType
              ? this.state.sourceType
              : this.props.currentIotReport.source
          }
        />
      </EditCreateModal>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetCurrentIotReport: () => dispatch(resetCurrentIotReport()),
    resetModal: () => dispatch(resetModal()),
    createIotReport: (data) => dispatch(createIotReport(data)),
    updateIotReport: (data, id) => dispatch(updateIotReport(data, id)),
    errorAlert: (data) => dispatch(alertActions.error(data)),
  };
};

const mapStateToProps = (state) => {
  return {
    currentIotReport: state.iotReport.current,
    creating: state.editCreateModal.creating,
    editing: state.editCreateModal.editing,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalContainer);
