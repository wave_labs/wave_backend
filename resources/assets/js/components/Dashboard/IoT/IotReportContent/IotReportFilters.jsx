import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, message, Select, Input } from "antd";
import styled from "styled-components";
import moment from "moment";
import debounce from "lodash/debounce";
import {
    fetchIotReports,
    exportIotReportCsv,
    fetchIotReportsCoordForLineDraw,
} from "redux-modules/iotReport/actions";
import FilterRow from "../../Common/FilterRow";
import ExportMessage from "../../Common/ExportMessage";

message.config({
    maxCount: 1,
});

const StyledRow = styled(Row)`
  margin: 1rem;
`;

class IotReportFilters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            operations: ["reset", "download"],
            messageStatus: null,
            filters: {
                field: undefined,
                value: undefined,
                date: undefined,
                device: null,
            },
        };
    }

    componentDidMount() {
        this.props.fetchIotReports(1, {
            ...this.state.filters,
            device: this.props.device,
        });
        this.handleFilterChange("device", this.props.device);
    }

    handleFilterChange = (filter, value) => {
        var { filters } = this.state;
        filters[filter] = value;
        this.setState({ filters });
    };

    handleReset = () => {
        this.setState({
            filters: {
                search: undefined,
                date: undefined,
                device: this.props.device,
            },
        });
    };

    handleDownload = () => {
        this.setState({ messageStatus: "loading" });
        var filters = this.handleFilters();

        this.props.exportIotReportCsv(filters).then(() => {
            this.setState({ messageStatus: "success" });
        });
    };

    handleFilters = () => {
        var { value, field, date, device } = this.state.filters;
        let newDate = date ? [...date] : [];

        return {
            value: value,
            field: field,
            device: device,
            date:
                newDate.length > 0
                    ? [
                        moment(newDate[0]).format("YYYY-MM-DD"),
                        moment(newDate[1]).format("YYYY-MM-DD"),
                    ]
                    : newDate,
        };
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState !== this.state) {
            var filters = this.handleFilters();

            this.props.handleFilterChange(filters);
            this.props.fetchIotReports(1, filters);
            this.props.fetchIotReportsCoordForLineDraw(filters);
        }
    }

    render() {
        var { messageStatus, filters, operations } = this.state;

        return (
            <StyledRow>
                <ExportMessage message={messageStatus} />
                <FilterRow
                    filters={["date"]}
                    filterValues={filters}
                    operations={operations}
                    handleFilterChange={this.handleFilterChange}
                    handleReset={() => this.handleReset()}
                    handleDownload={() => this.handleDownload()}
                    additionalFilters={[
                        <Select
                            placeholder="Select a field that the request must contain"
                            value={this.state.filters.field}
                            style={{ width: "100%" }}
                            onChange={(value) => this.handleFilterChange("field", value)}
                        >
                            {this.props.currentDevice.fields.map((field, index) => (
                                <Select.Option key={index} value={field.key}>
                                    {field.name}
                                </Select.Option>
                            ))}
                        </Select>,
                        <Input.Search
                            placeholder="Specify a value for any of the fields"
                            onSearch={(value) => this.handleFilterChange("value", value)}
                        />,
                    ]}
                />
            </StyledRow>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchIotReports: (page, filters) =>
            dispatch(fetchIotReports(page, filters)),
        exportIotReportCsv: (filters) => dispatch(exportIotReportCsv(filters)),
        fetchIotReportsCoordForLineDraw: (filters) =>
            dispatch(fetchIotReportsCoordForLineDraw(filters)),
    };
};
const mapStateToProps = (state) => {
    return {
        isAdmin: state.auth.isAdmin,
        currentDevice: state.iotDevice.current,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IotReportFilters);
