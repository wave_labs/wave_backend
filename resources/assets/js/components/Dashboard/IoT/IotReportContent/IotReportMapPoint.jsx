import React, { Component } from "react";
import MapBoxMap from "../../Common/MapBoxMap";
import { Marker } from "react-map-gl";
import MapPin from "../../Common/MapPin";
import { colors } from "../helper";

class IotReportMapPoint extends Component {
  render() {
    const { iotDevice } = this.props;

    return (
      <div className="map-container">
        <MapBoxMap
          defaultCenter={{
            lat: parseFloat(iotDevice.category.position.latitude),
            lng: parseFloat(iotDevice.category.position.longitude),
          }}
          zoom={9}
        >
          <Marker
            longitude={parseFloat(iotDevice.category.position.longitude)}
            latitude={parseFloat(iotDevice.category.position.latitude)}
          >
            <div onClick={() => this.setState({ popUp: true })}>
              <MapPin color={colors[1]} />
            </div>
          </Marker>
        </MapBoxMap>
      </div>
    );
  }
}

export default IotReportMapPoint;
