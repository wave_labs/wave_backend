import React from "react";
import styled from "styled-components";

const Container = styled.div`
  max-width: 200px;
`;

const Content = styled.div`
  font-size: 0.8em;
`;

const Title = styled(Content)`
  font-weight: bold;
`;

const IotReportPopUp = ({ iotReport }) => {
  return (
    <Container>
      <Title>Report Nº {iotReport.report}</Title>
      <Content>Date: {iotReport.date}</Content>
      <Content>Latitude: {iotReport.latitude}</Content>
      <Content>Longitude: {iotReport.longitude}</Content>
    </Container>
  );
};

export default IotReportPopUp;
