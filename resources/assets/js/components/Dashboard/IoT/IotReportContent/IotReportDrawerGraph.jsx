import React, { Component } from "react";
import { Row, DatePicker, Select, Button, Spin, Col, message } from "antd";
import { Line } from "react-chartjs-2";
import DrawerContainer from "../../Common/DrawerContainer";
import moment from "moment";
import { generateCustomGraph, exportIotReportCsv } from "redux-modules/iotReport/actions";
import { connect } from "react-redux";

const RangePicker = DatePicker.RangePicker;
const { Option } = Select;

class IotReportDrawerGraph extends Component {
    state = {
        visibility: false,
        filters: {
            graph: undefined,
            date: undefined,
            device: this.props.device,
        },
    };

    handleDrawerClose = () => {
        this.setState({
            visibility: false,
        });
    };

    handleDrawerOpen = () => {
        var { date, graph, device } = this.state.filters;
        let newDate = date ? [...date] : [];

        let newFilters = {
            graph: graph,
            device: device,
            date:
                newDate.length > 0
                    ? [
                        moment(newDate[0]).format("YYYY-MM-DD HH:mm"),
                        moment(newDate[1]).format("YYYY-MM-DD HH:mm"),
                    ]
                    : newDate,
        };

        this.props.generateCustomGraph(newFilters);
        this.setState({
            visibility: true,
        });
    };

    handleFilterChange = (filter, value) => {

        var { filters } = this.state;
        filters[filter] = value;
        this.setState({ filters });
    };

    render() {
        let { filters, visibility } = this.state;
        let { customGraph, loadingCustomGraph } = this.props;
        const data = {
            labels: customGraph.labels,
            datasets: [
                {
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: "rgba(112, 162, 255, 0.3)",
                    borderColor: "rgb(112, 162, 255)",
                    borderCapStyle: "butt",
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: "miter",
                    pointBorderColor: "rgb(112, 162, 255)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointRadius: 2,
                    pointHitRadius: 10,
                    data: customGraph.values,
                },
            ],
        };
        const options = {
            legend: {
                display: false,
            },
            animation: {
                duration: 0,
            },
            onClick: (evt, element) => {
                if (element.length > 0) {
                    var index = element[0]._index;
                    navigator.clipboard.writeText(customGraph.values[index]).then(() => {
                        message.info("Value copied to clipboard");
                    });
                }
            },
            onHover: (evt, element) => {
                if (element.length > 0) {
                    document.body.style.cursor = "pointer";
                }
            },
            showXLabels: 5,
            scales: {
                yAxes: [
                    {
                        scaleLabel: {
                            display: false,
                        },
                    },
                ],
                xAxes: [
                    {
                        ticks: {
                            callback: function (value, index) {
                                return index % 10 ? "" : value;
                            },
                        },
                        scaleLabel: {
                            display: false,
                        },
                        gridLines: {
                            display: false,
                        },
                    },
                ],
            },
        };

        const info = () => {
            message.loading('Generating CSV export, please remain on this page.', 0);
        };
        console.log(this.props.loadingExport);
        return (
            <div>
                {
                    this.props.loadingExport &&
                    info()
                }

                <Row style={{ margin: "50px 0" }} type="flex" justify="space-around" align="middle" gutter={16}>
                    <Col xs={12} sm={6}>
                        <Select
                            placeholder="Data Source"
                            value={filters.graph}
                            style={{ width: "100%" }}
                            onChange={(value) => this.handleFilterChange("graph", value)}
                        >
                            {this.props.fields.map((field, index) => (
                                <Option key={index} value={field.id}>
                                    {field.key}
                                </Option>
                            ))}
                        </Select>
                    </Col>
                    <Col xs={12} sm={6}>
                        <RangePicker
                            style={{ width: "100%" }}
                            value={filters.date}
                            ranges={{
                                Today: [moment().startOf("day"), moment()],
                                "This Week": [
                                    moment().startOf("week"),
                                    moment().endOf("week"),
                                ],
                            }}
                            showTime={{ format: "HH:mm" }}
                            format="YYYY-MM-DD HH:mm"
                            onChange={(value) => this.handleFilterChange("date", value)}
                        />
                    </Col>
                    <Col xs={12} sm={12}>
                        <Row type="flex" justify="end" gutter={16}>
                            <Button
                                onClick={this.handleDrawerOpen}
                                type="primary"
                                style={{ marginRight: "10px" }}
                                disabled={filters.graph == undefined || filters.date == undefined}
                            >
                                Generate Graph
                            </Button>
                            <Button
                                onClick={() => this.props.exportIotReportCsv({ device: this.props.device })}
                                type="primary"
                            >
                                Download data
                            </Button>
                        </Row>
                    </Col>
                </Row>
                {this.props.device.type && this.props.device.type.name == "intertagua" && <More onClick={this.handleMore}>check more</More>}
                <DrawerContainer
                    handleDrawerClose={this.handleDrawerClose}
                    visible={visibility}
                    width={"60%"}
                >
                    <h1>{filters.graph}</h1>
                    <Row type="flex" justify="center" align="middle">
                        {loadingCustomGraph ? (
                            <Spin size="large" />
                        ) : (
                            <Line
                                loading={loadingCustomGraph}
                                data={data}
                                options={options}
                            />
                        )}
                    </Row>
                </DrawerContainer>
            </div >
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        generateCustomGraph: (filters) => dispatch(generateCustomGraph(filters)),
        exportIotReportCsv: (filters) => dispatch(exportIotReportCsv(filters)),
    };
};
const mapStateToProps = (state) => {
    return {
        customGraph: state.iotReport.customGraph,
        loadingCustomGraph: state.iotReport.loadingCustomGraph,
        loadingExport: state.iotReport.loadingExport,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IotReportDrawerGraph);
