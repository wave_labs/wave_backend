import React, { useEffect, useState, Fragment } from "react";
import { connect } from "react-redux";
import { fetchIotDevice } from "redux-modules/iotDevice/actions";
import { fetchFieldsTimeline, latestIotReport } from "redux-modules/iotReport/actions";
import MinimalistMap from "../Template/MinimalistMap";
import LoadingContainer from "../../../Common/LoadingContainer";
import Table from "../Template/Table";
import SideSections from "../Template/SideSections";
import Complete from "../Template/Complete";

export const IotReportContent = ({ fetchIotDevice, fetchFieldsTimeline, latestIotReport, match, currentIotDevice, loadingTimeline, loading }) => {
    const [timestamp, setTimestamp] = useState(null); //hack'sh solution to make set interval get access to lastest props values

    useEffect(() => {
        setTimestamp(Date.now());
        fetchIotDevice(match.params.device);
    }, []);

    useEffect(() => {
        var interval = setInterval(() => {
            setTimestamp(Date.now());
            latestIotReport({ device: match.params.device });
        }, 3000);

        return () => clearInterval(interval);
    }, [timestamp]);

    useEffect(() => {
        fetchIotDevice(match.params.device).then((response) => {
            if (response.value.data.data.template != 1) {
                fetchFieldsTimeline({
                    device: response.value.data.data.id,
                    fields: response.value.data.data.timeline_fields,
                });
            }


            latestIotReport({ device: response.value.data.data.id });
        });
    }, [match.params.device]);

    return (
        <LoadingContainer hasData={!loading || !loadingTimeline}>
            {currentIotDevice.template &&
                <Fragment>
                    {
                        (currentIotDevice.template.id == 1) ?
                            <Table iotDevice={currentIotDevice} /> :
                            (currentIotDevice.template.id == 2) ?
                                <MinimalistMap iotDevice={currentIotDevice} /> :
                                (currentIotDevice.template.id == 3 || currentIotDevice.template.id == 4 || currentIotDevice.template.id == 5) ?
                                    <SideSections iotDevice={currentIotDevice} /> :
                                    <Complete iotDevice={currentIotDevice} />
                    }
                </Fragment>
            }


        </LoadingContainer>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchIotDevice: (id) => dispatch(fetchIotDevice(id)),
        fetchFieldsTimeline: (filters) => dispatch(fetchFieldsTimeline(filters)),
        latestIotReport: (filters) => dispatch(latestIotReport(filters)),
    };
};

const mapStateToProps = (state) => {
    return {
        currentIotDevice: state.iotDevice.current,
        loading: state.iotDevice.loading,
        loadingTimeline: state.iotReport.loading,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(IotReportContent);


