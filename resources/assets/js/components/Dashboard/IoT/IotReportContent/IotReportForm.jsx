import React, { Component } from "react";
import styled from "styled-components";
import {
  Row,
  Col,
  Form,
  Upload,
  InputNumber,
  Select,
  Checkbox,
  DatePicker,
  Button,
  Icon,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import LitterCategoryRemoteCascadeContainer from "../../Data/LitterCategoryContent/LitterCategoryRemoteCascadeContainer";
import moment from "moment";

const FormItem = Form.Item;
const Option = Select.Option;

const StyledInputNumber = styled(InputNumber)`
  min-width: 100% !important;
`;

const StyledFormItem = styled(FormItem)`
  position: relative;
`;
const StyledIcon = styled(Icon)`
  right: 8px;
  top: 4px;
  position: absolute;
`;

let id = 0;
class IotReportForm extends Component {
  state = {
    loading: false,
  };

  remove = (k) => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue("keys");

    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter((key) => key !== k),
    });
  };

  dummyRequest = ({ file, onSuccess }) => {
    setTimeout(() => {
      onSuccess("ok");
    }, 0);
  };

  handleChange = (info) => {
    if (info.file.status === "uploading") {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === "done") {
      this.setState({
        loading: false,
      });

      this.props.handlePhoto(info.fileList[0].originFileObj);
      info.fileList.shift();
    }
  };

  handleSource = (e) => {
    this.props.toggleChildMenu(e);
  };

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue("keys");
    const nextKeys = keys.concat(id++);
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys,
    });
  };

  rules = {
    date: [
      {
        required: true,
        message: "Date is required",
      },
    ],
    source: [
      {
        required: true,
        message: "Source is required",
      },
    ],
    latitude: [
      {
        required: true,
        message: "Latitude is required",
      },
    ],
    longitude: [
      {
        required: true,
        message: "Longitude is required",
      },
    ],
  };

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { iotReport, editing, sourceType } = this.props;
    const { userable } = iotReport;

    getFieldDecorator("keys", { initialValue: [] });
    const keys = getFieldValue("keys");

    const iotReportSubCategories = keys.map((k) => (
      <StyledFormItem key={"iotReport-" + k}>
        {getFieldDecorator(`CategoriesArrays[${k}]`, {
          validateTrigger: ["onChange", "onBlur"],
        })(
          <LitterCategoryRemoteCascadeContainer
            source={
              this.props.form.getFieldValue("source") == "beach"
                ? "Beach"
                : this.props.form.getFieldValue("source") == "floating"
                ? "Floating"
                : null
            }
          />
        )}
        {
          <StyledIcon
            key={k}
            className="dynamic-delete-button"
            type="minus-circle-o"
            onClick={() => this.remove(k)}
          />
        }
      </StyledFormItem>
    ));

    return (
      <Form hideRequiredMark={true}>
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Source">
              {getFieldDecorator("source", {
                initialValue: iotReport.source,
                rules: this.rules.source,
              })(
                <Select placeholder="Source" onChange={this.handleSource}>
                  <Option value="beach">Beach</Option>
                  <Option value="floating">Floating</Option>
                  <Option value="dive">Dive</Option>
                </Select>
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Date">
              {getFieldDecorator("date", {
                rules: this.rules.date,
                initialValue: iotReport.date ? moment(iotReport.date) : null,
              })(<DatePicker style={{ width: "100%" }} />)}
            </FormItem>
          </Col>

          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Latitude">
              {getFieldDecorator("latitude", {
                initialValue: iotReport.latitude,
                rules: this.rules.latitude,
              })(<StyledInputNumber placeholder="Latitude" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem hasFeedback label="Longitude">
              {getFieldDecorator("longitude", {
                initialValue: iotReport.longitude,
                rules: this.rules.longitude,
              })(<StyledInputNumber placeholder="Longitude" />)}
            </FormItem>
          </Col>

          <Col xs={24}>
            <Row type="flex" justify="center">
              <FormItem style={{ width: "60%" }}>
                <Upload
                  customRequest={this.dummyRequest}
                  onChange={this.handleChange}
                  style={{ width: "100%", display: "block" }}
                >
                  <Button style={{ width: "100%" }}>
                    <UploadOutlined /> Upload photo
                  </Button>
                </Upload>
              </FormItem>
            </Row>
          </Col>

          {sourceType == "beach" && (
            <Col xs={24} sm={12}>
              <FormItem hasFeedback label="Quantity">
                {getFieldDecorator("quantity", {
                  initialValue: iotReport.quantity,
                  rules: this.rules.name,
                })(
                  <Select placeholder="Quantity of iotReport">
                    <Option value="1-5">1-5</Option>
                    <Option value="5-10">5-10</Option>
                    <Option value="10-50">10-50</Option>
                    <Option value=">50">{">"}50</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
          )}

          {sourceType == "beach" && (
            <Col xs={24} sm={12}>
              <FormItem hasFeedback label="Radius">
                {getFieldDecorator("radius", {
                  initialValue: iotReport.radius,
                  rules: this.rules.radius,
                })(
                  <Select placeholder="Radius of iotReport">
                    <Option value="<1">{"<"}1</Option>
                    <Option value="1-5">1-5</Option>
                    <Option value="5-20">5-20</Option>
                    <Option value=">20">{">"}20</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
          )}

          {!editing &&
            (sourceType == "beach" ||
              sourceType == "floating" ||
              sourceType == "dive") && (
              <React.Fragment>
                <Col xs={24}>{iotReportSubCategories}</Col>

                <Col xs={24}>
                  <FormItem>
                    <Button
                      type="dashed"
                      onClick={this.add}
                      style={{ width: "100%" }}
                    >
                      <Icon type="plus" /> Add Litter
                    </Button>
                  </FormItem>
                </Col>
              </React.Fragment>
            )}
        </Row>
        <FormItem>
          {getFieldDecorator("collected", {
            initialValue: iotReport.collected,
            valuePropName: "checked",
          })(<Checkbox> Collected </Checkbox>)}
        </FormItem>

        {sourceType == "floating" && (
          <FormItem>
            {getFieldDecorator("multiple", {
              initialValue: iotReport.multiple,
              valuePropName: "checked",
            })(<Checkbox> Multiple objects </Checkbox>)}
          </FormItem>
        )}
      </Form>
    );
  }
}

export default Form.create()(IotReportForm);
