import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import MapBoxMap from "../../Common/MapBoxMap";
import { Marker, Popup } from "react-map-gl";
import {
  exportIotReportCsv,
} from "redux-modules/iotReport/actions";
import IotReportPopUp from "./IotReportPopUp";
import { Switch } from "antd";
import LineDrawer from "./LineDrawer";
import MapPin from "../../Common/MapPin";
import { colors } from "../helper";
import styled from "styled-components";

const MapLineSwitch = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  z-index: 99;
`;

const LinePin = styled.div`
  width: 6px;
  height: 6px;
  transform: translate(-3px, -3px);
  background-color: ${(props) => props.color};
  border-radius: 50%;
`;

class IotReportMapList extends Component {
  state = {
    iotReportPopUp: null,
    isMapLineVisible: true,
  };

  showIotReport = () => {
    const { iotReportPopUp } = this.state;

    return (
      iotReportPopUp && (
        <Popup
          tipSize={5}
          longitude={parseFloat(iotReportPopUp.longitude)}
          latitude={parseFloat(iotReportPopUp.latitude)}
          closeOnClick={false}
          onClose={() => this.setState({ iotReportPopUp: null })}
        >
          <IotReportPopUp iotReport={iotReportPopUp} />
        </Popup>
      )
    );
  };

  render() {
    const Start = ({ color }) => (
      <svg
        height="100"
        viewBox="0 0 24 24"
        style={{
          fill: color,
          stroke: "none",
          transform: `translate(-11px,-10px)`,
        }}
      >
        <path d="M2 1 h1 v1 h1 v1 h-1 v1 h-1 v-1 h-1 v-1 h1 z" />
      </svg>
    );

    return (
      <div className="map-container">
        <MapBoxMap zoom={9}>
          <MapLineSwitch>
            <Switch
              checkedChildren="Lines ON"
              unCheckedChildren="Lines OFF"
              defaultChecked={this.state.isMapLineVisible}
              onChange={(e) =>
                this.setState({
                  isMapLineVisible: e,
                })
              }
            />
          </MapLineSwitch>

          {this.props.iotReportsByCoordForLineDraw.map((iotReports, index) => (
            <Fragment key={index}>
              {this.state.isMapLineVisible && iotReports.length > 1 && (
                <LineDrawer color={colors[index]} points={iotReports} />
              )}

              {iotReports.map((coords, indexCoord) => (
                <Marker
                  key={indexCoord}
                  longitude={parseFloat(coords[0])}
                  latitude={parseFloat(coords[1])}
                >
                  <div
                    onClick={() => this.setState({ iotReportPopUp: coords[2] })}
                  >
                    {indexCoord == 0 ? (
                      <Start color={colors[index]} />
                    ) : iotReports.length === indexCoord + 1 ? (
                      <MapPin color={colors[index]} />
                    ) : (
                      <LinePin color={colors[index]} />
                    )}
                  </div>
                </Marker>
              ))}
            </Fragment>
          ))}

          {this.showIotReport()}
        </MapBoxMap>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    loadingExport: state.iotReport.loadingExport,
    iotReportsByCoordForLineDraw: state.iotReport.iotReportsByCoordForLineDraw,
  }),
  { exportIotReportCsv }
)(IotReportMapList);
