import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/iotEnvironmentType/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class IotEnvironmentTypeRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      IotEnvironmentTypeSelector,
      loading,
      value,
      onChange,
      mode,
      children,
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Environment Type"
        mode={mode}
      >
        {children}
        {IotEnvironmentTypeSelector.map((iotEnvironmentType) => (
          <Option value={iotEnvironmentType.id} key={iotEnvironmentType.id}>
            {iotEnvironmentType.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    IotEnvironmentTypeSelector: state.iotEnvironmentType.IotEnvironmentTypeSelector,
    loading: state.iotEnvironmentType.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotEnvironmentTypeRemoteSelectContainer);
