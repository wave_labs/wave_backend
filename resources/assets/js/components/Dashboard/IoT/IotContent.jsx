import React, { Component } from "react";
import TopicListContainer from "../../Common/TopicListContainer";

import { IotData } from "../../MainPage/Kits/kitsHelper";

class IotContent extends Component {
  render() {
    return (
      <TopicListContainer
        history={this.props.history}
        topic="IoT"
        content={IotData}
      />
    );
  }
}

export default IotContent;
