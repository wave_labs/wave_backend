import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/iotDeviceState/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class IotDeviceStateRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      IotDeviceStateSelector,
      loading,
      value,
      onChange,
      mode,
      children,
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder="Device State"
        mode={mode}
      >
        {children}
        {IotDeviceStateSelector.map((iotDeviceState) => (
          <Option value={iotDeviceState.id} key={iotDeviceState.id}>
            {iotDeviceState.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    IotDeviceStateSelector: state.iotDeviceState.IotDeviceStateSelector,
    loading: state.iotDeviceState.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotDeviceStateRemoteSelectContainer);
