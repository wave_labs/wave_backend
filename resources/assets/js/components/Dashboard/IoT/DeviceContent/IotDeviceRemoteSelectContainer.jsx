import React, { Component } from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import debounce from "lodash/debounce";
import { fetchSelector } from "redux-modules/iotDevice/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";

const Option = Select.Option;

class IotDeviceRemoteSelectContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onSearch = debounce(this.onSearch, 800);
  }

  onSearch = (search) => {
    this.props.fetchSelector({ search });
  };

  componentDidMount = () => {
    this.props.fetchSelector();
  };

  render() {
    const {
      IotDeviceSelector,
      loading,
      value,
      onChange,
      mode,
      children,
      placeholder = "Iot Device",
    } = this.props;
    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
        mode="multiple"
        style={{ width: "100%" }}
      >
        {IotDeviceSelector.map((iotDevice) => (
          <Option value={iotDevice.id} key={iotDevice.id}>
            {iotDevice.name}
          </Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSelector: (filters) => dispatch(fetchSelector(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    IotDeviceSelector: state.iotDevice.IotDeviceSelector,
    loading: state.iotDevice.loading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotDeviceRemoteSelectContainer);
