import React, { Component } from "react";
import {
    createIotDevice,
    updateIotDevice,
    resetIotDeviceEdit,
} from "redux-modules/iotDevice/actions";
import {
    fetchIotFeatures,
} from "redux-modules/iotFeature/actions";
import { resetModal } from "redux-modules/editCreateModal/actions";
import { connect } from "react-redux";
import DeviceForm from "./Form/DeviceForm";
import { alertActions } from "redux-modules/alert/actions";
import { Modal, Button } from "antd";
import StepsContainer from "../../Common/StepsContainer";

import TemplateForm from "./Form/TemplateForm";
import FieldForm from "./Form/FieldForm";
import StructureForm from "./Form/StructureForm";

class IotDeviceModalContainer extends Component {
    constructor() {
        super();
        this.state = {
            confirmLoading: false, //modal button loading icon
            fileList: [],
            removedSelf: false,
            fields: [],
            formData: {
                template: undefined
            },
            disabled: true
        };
    }


    onCancel = () => {
        this.resetModalForm();
    };

    onOkEditClick = () => {
        var { formData } = this.state;

        formData.template_id = formData.template.id;
        console.log("update");
        console.log(formData);
        this.props
            .updateIotDevice(this.props.editIotDevice.id, formData)
            .then(() => {
                this.resetModalForm();
            })
            .catch((error) => {
                this.setState({ confirmLoading: false });
                let messages = [];

                Object.values(error.response.data.errors).map(function (message) {
                    messages.push(message[0]);
                });

                this.props.errorAlert({
                    description: messages,
                });
            });
    };

    handleArrayToFormData = (formData, array, field) => {
        for (var i = 0; i < array.length; i++) {
            formData.append(`${field}[]`, array[i]);
        }

        return formData;
    };

    resetModalForm = () => {
        this.setState({
            confirmLoading: false,
            fileList: null,
            removedSelf: false,
            fields: [],
        });

        this.props.resetIotDeviceEdit();
        this.props.resetModal();

        this.deviceFormRef && this.deviceFormRef.props.form.resetFields();
        this.fieldFormRef && this.fieldFormRef.props.form.resetFields();
        this.templateFormRef && this.templateFormRef.props.form.resetFields();
        this.structureFormRef && this.structureFormRef.props.form.resetFields();

        this.stepsRef.setCurrentStep(0);
    };

    handlePhoto = (photo, preview) => {
        this.setState({ fileList: photo, previewImage: preview });
    };

    onOkCreateClick = () => {
        var { formData } = this.state;

        formData.template_id = formData.template.id;
        console.log("create");
        console.log(formData);
        this.props
            .createIotDevice(formData)
            .then(() => {
                this.resetModalForm();
            })
            .catch((error) => {
                this.setState({ confirmLoading: false });
                let messages = [];

                Object.values(error.response.data.errors).map(function (message) {
                    messages.push(message[0]);
                });

                this.props.errorAlert({
                    description: messages,
                });
            });
    };

    saveRef = (ref, form = "steps") => {
        switch (form) {
            case "device":
                this.deviceFormRef = ref;
                break;
            case "field":
                this.fieldFormRef = ref;
                break;
            case "structure":
                this.structureFormRef = ref;
                break;
            default:
                this.stepsRef = ref;
                break;
        }
    };

    componentDidUpdate(prevProps) {
        if (prevProps.editIotDevice != this.props.editIotDevice) {
            this.setState({
                previewImage: this.props.editing
                    ? "/" + this.props.editIotDevice.img
                    : null,
            });
        }
    }

    cleanArray(data) {
        Object.entries(data).map((value) => {
            if (Array.isArray(value[1])) {
                var newArray = value[1].filter((index) => {
                    return index !== null && typeof index !== 'undefined';
                });

                data[value[0]] = newArray;
            }

            if (
                typeof value[1] === 'object' &&
                !Array.isArray(value[1]) &&
                value[1] !== null
            ) {
                data[value[0]] = this.cleanArray(value[1]);
            }

        });

        return data;
    }

    handleFormData = (form) => {
        return new Promise((resolve, reject) => {
            form.validateFields((err, data) => {
                if (!err) {
                    data = this.cleanArray(data);
                    var { formData } = this.state;
                    formData = { ...formData, ...data };
                    this.setState({ formData });

                    resolve(data);
                } else {
                    reject(err);
                }
            });
        })

    };


    onNext = () => {
        if (this.stepsRef.isLastStep()) {
            this.handleFormData(this.structureFormRef.props.form).then(() => {
                this.props.editing ? this.onOkEditClick() : this.onOkCreateClick();
            });
        } else if (this.stepsRef.getCurrentStep() == 1) {
            this.handleFormData(this.fieldFormRef.props.form).then((data) => {
                this.setState({
                    fields: data.fields,
                }, () => this.stepsRef.next());
                this.handleDisabled(false);
            });
        } else if (this.stepsRef.getCurrentStep() == 2) {
            this.stepsRef.next();
            this.handleDisabled(false);
        } else {
            this.handleFormData(this.deviceFormRef.props.form).then(() => {
                this.stepsRef.next();
            }).catch(() => {
                this.handleDisabled(true);
            });
        }
    };

    handleDisabled = (aDisabled) => {
        this.setState({
            disabled: aDisabled
        })
    }

    componentDidMount() {
        this.props.fetchIotFeatures();
    }

    render() {
        return (
            <Modal
                width={720}
                centered
                title={this.props.editing ? "Edit Device" : "Create Device"}
                visible={this.props.editing || this.props.creating}
                onOk={this.props.editing ? this.onOkEditClick : this.onOkCreateClick}
                onCancel={this.onCancel}
                confirmLoading={this.state.confirmLoading}
                footer={[
                    <Button key="back" onClick={() => this.stepsRef.prev()}>
                        Previous
                    </Button>,
                    <Button
                        disabled={this.props.editing ? false : this.state.disabled}
                        key="submit"
                        type="primary"
                        onClick={this.onNext}
                    >
                        Ok
                    </Button>,
                ]}
            >
                <StepsContainer
                    ref={this.saveRef}
                    steps={[
                        <DeviceForm
                            user={this.props.user}
                            editing={this.props.editing}
                            creating={this.props.creating}
                            wrappedComponentRef={(ref) => this.saveRef(ref, "device")}
                            handlePhoto={this.handlePhoto}
                            previewImage={this.state.previewImage}
                            iotDevice={this.props.editIotDevice}
                            handleDisabled={this.handleDisabled}
                        />,
                        <FieldForm
                            wrappedComponentRef={(ref) => this.saveRef(ref, "field")}
                            iotDevice={this.props.editIotDevice}
                            editing={this.props.editing}
                            creating={this.props.creating}
                            handleDisabled={this.handleDisabled}
                        />,
                        <TemplateForm
                            editing={this.props.editing}
                            creating={this.props.creating}
                            handleDisabled={this.handleDisabled}
                            handleFormField={(data) => this.setState({ formData: { ...this.state.formData, ...data } })}
                            template={this.state.formData.template}
                            iotDevice={this.props.editIotDevice}
                        />,
                        <StructureForm
                            wrappedComponentRef={(ref) => this.saveRef(ref, "structure")}
                            iotDevice={this.props.editIotDevice}
                            fields={this.state.fields}
                            editing={this.props.editing}
                            creating={this.props.creating}
                            template={this.state.formData.template}
                            features={this.props.features}
                        />,
                    ]}
                />
            </Modal>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        resetIotDeviceEdit: () => dispatch(resetIotDeviceEdit()),
        resetModal: () => dispatch(resetModal()),
        createIotDevice: (data) => dispatch(createIotDevice(data)),
        updateIotDevice: (id, data) => dispatch(updateIotDevice(id, data)),
        errorAlert: (data) => dispatch(alertActions.error(data)),
        fetchIotFeatures: () => dispatch(fetchIotFeatures()),

    };
};

const mapStateToProps = (state) => {
    return {
        editIotDevice: state.iotDevice.editIotDevice,
        creating: state.editCreateModal.creating,
        editing: state.editCreateModal.editing,
        user: state.auth.user,
        features: state.iotFeature.data,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IotDeviceModalContainer);
