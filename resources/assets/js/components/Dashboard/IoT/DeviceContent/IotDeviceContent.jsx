import React, { Component } from "react";
import IotDeviceModalContainer from "./IotDeviceModalContainer";
import IotDeviceListContainer from "./IotDeviceListContainer";
import IotDeviceFilters from "./IotDeviceFilters";

class IotDeviceContent extends Component {
    render() {
        return (
            <div>
                <IotDeviceModalContainer />
                <IotDeviceFilters
                    deviceType={this.props.match.params.deviceType}
                />

                <IotDeviceListContainer
                    deviceType={this.props.match.params.deviceType}
                />
            </div>
        );
    }
}

export default IotDeviceContent;
