import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Row } from "antd";
import styled from "styled-components";
import { fetchIotDevices } from "redux-modules/iotDevice/actions";
import { startCreating } from "redux-modules/editCreateModal/actions";
import FilterRow from "../../Common/FilterRow";

const StyledRow = styled(Row)`
  margin: 1rem;
`;


function IotDeviceFilters({ fetchIotDevices, startCreating, deviceType }) {
    const [operations, setOperations] = useState(["reset", "create"]);
    const [filters, setFilters] = useState({
        self: true,
        search: undefined,
        deviceType: deviceType,
    });

    useEffect(() => {
        fetchIotDevices(filters);
    }, [filters])

    function handleFilterChange(filter, value) {
        var newFilters = { ...filters };
        newFilters[filter] = value;

        setFilters(newFilters);
    };

    function handleReset() {
        setFilters({
            search: undefined,
            self: true,
            deviceType: deviceType,
        });
    };

    return (
        <StyledRow>
            <FilterRow
                filters={["search"]}
                filterValues={filters}
                operations={operations}
                handleFilterChange={handleFilterChange}
                handleReset={handleReset}
                handleCreate={startCreating}
            />
        </StyledRow>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchIotDevices: (filters) => dispatch(fetchIotDevices(filters)),
        startCreating: () => dispatch(startCreating()),
    };
};


export default connect(
    null,
    mapDispatchToProps
)(IotDeviceFilters);



