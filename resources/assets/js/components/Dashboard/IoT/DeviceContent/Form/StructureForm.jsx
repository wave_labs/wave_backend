import React, { Component, Fragment } from "react";
import styled from "styled-components";
import { Row, Col, Form, Input, Button, Select } from "antd";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import IotFeatureRemoteSelectContainer from "../../FeatureContent/IotFeatureRemoteSelectContainer";
import Information from "../Common/Information";

const FormItem = Form.Item;
const Option = Select.Option;

const Section = styled.div`
  width: 100%;
  padding: 20px;
  box-shadow: 0 0 3px 0 rgba(136, 136, 136, 0.3);
  border-radius: 6px;
  background: #fefefe;
  margin: 20px auto;
`;

const TemplateImage = styled.img`
  width: 20%;
  min-width: 80px;
  margin: auto;
  display: block;
`;

class StructureForm extends Component {
  state = {
    keys: [],
    counter: [],
  };

  remove = (k, index) => {
    const { form } = this.props;
    const keys = form.getFieldValue("keys");

    let nextKeys = keys;
    nextKeys[index] = keys[index].filter((key) => key !== k);

    form.setFieldsValue({
      keys: nextKeys,
    });
  };

  add = (index) => {
    const { form } = this.props;
    let { counter } = this.state;
    const keys = form.getFieldValue("keys");
    let nextKeys = keys;
    nextKeys[index] = keys[index].concat(counter[index] + 1);
    counter[index] = counter[index] + 1;
    this.setState({ counter });
    form.setFieldsValue({
      keys: nextKeys,
    });
  };

  rules = {
    keys: [
      {
        required: true,
        whitespace: true,
        message: "Type the key or delete this line.",
      },
    ],
  };

  componentDidMount() {
    let { keys, counter } = this.state;
    this.props.template.sections.map(() => {
      keys.push([]);
      counter.push(0);
    })

    this.setState({ keys, counter });
  }

  fieldKey = (fieldName, label = "") => {
    const { getFieldDecorator } = this.props.form;
    const { fields } = this.props;
    return (
      <FormItem label={label}>
        {getFieldDecorator(fieldName, {
          validateTrigger: ["onChange", "onBlur"],
          rules: [
            {
              required: true,
              message: "Field is required",
            },
          ],
        })(
          <Select placeholder="Field">
            {fields.map((option, index) => (
              <Option value={option.key} key={index}>
                {option.key}
              </Option>
            ))}
          </Select>
        )}
      </FormItem>
    );
  };

  fieldFeature = (index, fieldName) => {
    const { getFieldDecorator } = this.props.form;

    return (
      <FormItem key={index}>
        {getFieldDecorator(fieldName, {})(<IotFeatureRemoteSelectContainer />)}
      </FormItem>
    );
  };

  sectionContainer = (title, keys, keysIndex) => {
    const { getFieldDecorator } = this.props.form;

    return (
      <Section key={keysIndex}>
        <h2>{title}</h2>
        <Row>
          <FormItem label="Title">
            {getFieldDecorator(`section_title[${title}]`, {})(
              <Input placeholder="Section identifier, it will be visible as title of each section" />
            )}
          </FormItem>
        </Row>

        {keys.map((k, index) => {
          var feature_id = this.props.form.getFieldValue(`sections[${title}][${index}].feature`);
          if (feature_id) {
            var currentFeature = this.props.features.filter((feature) => {
              return feature.id == this.props.form.getFieldValue(`sections[${title}][${index}].feature`);
            });
            currentFeature = currentFeature[0];

          }

          var columnSize = !feature_id ? 12
            : currentFeature.parameters.length ?
              parseInt((12 / (currentFeature.parameters.length))) : 12

          return (
            <Row key={index} type="flex" >
              <Col span={23}>
                <Row type="flex" gutter={16} justify="space-between">
                  <Col xs={columnSize != 12 ? 6 : columnSize}>
                    {this.fieldKey(`sections[${title}][${index}].field`)}
                  </Col>
                  <Col xs={columnSize != 12 ? 6 : columnSize}>
                    {this.fieldFeature(index, `sections[${title}][${index}].feature`)}
                  </Col>
                  {currentFeature && currentFeature.parameters.map((parameter) => (
                    <Col xs={columnSize}>
                      <FormItem >
                        {getFieldDecorator(`sections[${title}][${index}][${parameter.name}]`)(
                          <Input style={{ width: '100%' }} placeholder={parameter.name} />
                        )}
                      </FormItem>
                    </Col>
                  ))}
                </Row>
              </Col>
              <Col span={1}>
                <Row type="flex" align="middle" justify="center">
                  {this.deleteField(k, index, keysIndex)}
                </Row>
              </Col>
            </Row>
          )
        })}

        <Row>
          <FormItem>
            <Button
              type="dashed"
              onClick={() => this.add(keysIndex)}
              style={{ width: "100%" }}
            >
              <PlusOutlined /> Add Field
            </Button>
          </FormItem>
        </Row>
      </Section>
    );
  };

  deleteField = (k, index, keysIndex) => {
    return (
      <MinusCircleOutlined
        key={index}
        onClick={() => this.remove(k, keysIndex)}
      />
    );
  };

  MapSection = () => {
    const { getFieldDecorator } = this.props.form;
    var formItem = `sections[Map]`;

    return (
      <Section key={index}>
        <h2>Map</h2>
        <Row type="flex" gutter={16} align="bottom">
          <Col xs={12}>
            {this.fieldKey(formItem + `[${0}].field`, "latitude")}
          </Col>
          <Col xs={0}>
            <FormItem>
              {getFieldDecorator(formItem + `[${0}].feature`, {
                initialValue: 1,
              })(
                <IotFeatureRemoteSelectContainer disabled />
              )}
            </FormItem>
          </Col>
          <Col xs={12}>
            {this.fieldKey(formItem + `[${1}].field`, "longitude")}
          </Col>
          <Col xs={0}>
            <FormItem>
              {getFieldDecorator(formItem + `[${1}].feature`, {
                initialValue: 2,
              })(
                <IotFeatureRemoteSelectContainer disabled />
              )}
            </FormItem>
          </Col>
        </Row>
      </Section>
    )
  };

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { template } = this.props;

    getFieldDecorator(`keys`, { initialValue: this.state.keys });
    const keys = getFieldValue(`keys`);

    return (
      <Form hideRequiredMark={true}>

        <Information
          title="Add fields to sections"
          subtitle="Fill available sections with selected fields according to template visible on the image that follows."
        />

        <TemplateImage
          src={template.image}
          alt="template"
        />
        {keys.length &&
          <Fragment>
            {
              template.sections.map((section, index) => (
                section.name == "Map" ? this.MapSection() :
                  this.sectionContainer(section.name, keys[index], index)
              ))
            }
          </Fragment>
        }


      </Form>
    );
  }
}

export default Form.create()(StructureForm);
