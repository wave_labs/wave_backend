import React, { Component } from 'react'
import { Row } from 'antd';
import { connect } from "react-redux";
import {
    fetchSelector,
} from "redux-modules/iot/iotTemplate/actions";
import styled from "styled-components";
import Information from '../Common/Information';

const Template = styled.div`
  width: 30%;
  cursor: pointer;
  margin: auto;
  display: block;
  padding: 15px 20px;
  border-radius: 6px;
  box-shadow: ${props => props.active ? "0 0 10px rgba(0,0,0,0.2)" : "0px"} ;

  &:hover{
      box-shadow: 0 0 10px 0 rgba(0,0,0,0.1);
  }

  img {
    width: 100%;
  }
`;


class TemplateForm extends Component {
    componentDidMount() {
        this.props.fetchSelector();
        this.props.handleDisabled(true);
        if (this.props.editing) {
            this.props.handleFormField({ template: this.props.iotDevice.template });
        }
    }

    handleClick = (aTemplate) => {
        this.props.handleDisabled(false);
        this.props.handleFormField({ template: aTemplate });
    }

    render() {
        let { template } = this.props;

        return (
            <div>
                <Information
                    title="Select a template from the list below"
                    subtitle="Templates demonstrate how the device's page will look, namely map, sections, and table placement"
                />

                <Row type="flex" justify="space-around">
                    {this.props.data.map((element) => (
                        <Template
                            active={template && (element.id == template.id)}
                            onClick={() => this.handleClick(element)}
                            key={element.id}
                        >
                            <img src={element.image} alt={element.name} />
                        </Template>
                    ))
                    }
                </Row>
            </div>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        fetchSelector: () => dispatch(fetchSelector()),
    };
};

const mapStateToProps = (state) => {
    return {
        data: state.iotTemplate.selector,
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TemplateForm);
