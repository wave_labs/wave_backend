import React, { Component } from "react";
import { Row, Col, Form, Input, Button } from "antd";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import Information from '../Common/Information';
import IotRuleRemoteSelectContainer from '../../RuleContent/IotRuleRemoteSelectContainer';


const FormItem = Form.Item;

class FieldForm extends Component {
    state = {
        keys: [],
        id: 0,
    };

    remove = (k) => {
        const { form } = this.props;

        const keys = form.getFieldValue("keys");

        form.setFieldsValue({
            keys: keys.filter((key) => key !== k),
        });
    };

    add = () => {
        this.props.handleDisabled(false);
        const { form } = this.props;
        let { id } = this.state;
        const keys = form.getFieldValue("keys");
        const nextKeys = keys.concat(id + 1);
        this.setState({ id: this.state.id + 1 });
        form.setFieldsValue({
            keys: nextKeys,
        });
    };

    componentDidMount() {
        let keysInitialValue = [];
        let counter = 0;

        this.props.iotDevice.fields.forEach(() => {
            keysInitialValue.push(counter);
            counter++;
        });

        this.setState({
            keys: keysInitialValue,
            id: counter,
            category: this.props.iotDevice.category.id,
        });
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.editing != this.props.editing ||
            prevProps.creating != this.props.creating
        ) {
            if (this.props.editing || this.props.creating) {
                let keysInitialValue = [];
                let counter = 0;

                this.props.iotDevice.fields.forEach(() => {
                    keysInitialValue.push(counter);
                    counter++;
                });

                this.setState({
                    keys: keysInitialValue,
                    id: counter,
                    category: this.props.iotDevice.category.id,
                });
            }
        }
    }


    render() {
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const { iotDevice } = this.props;
        console.log(iotDevice);
        getFieldDecorator("keys", { initialValue: this.state.keys });
        const keys = getFieldValue("keys");

        return (
            <Form>
                <Information
                    title="Select device fields"
                    subtitle="Selected all fields that the system will receive from this device by providing keys and defining their validation rules if any"
                />
                {keys.map((k, index) => (
                    <Row key={index} type="flex" align="top" gutter={8}>
                        <Col span={22}>
                            <Col span={16}>
                                <FormItem key={"key-" + k} label="Field Name">
                                    {getFieldDecorator(`fields[${k}].key`, {
                                        validateTrigger: ["onChange", "onBlur"],
                                        rules: [{
                                            required: true,
                                            message: "Type the key or delete this line.",
                                        }],
                                        initialValue: iotDevice.fields[k]
                                            ? iotDevice.fields[k].key
                                            : undefined,
                                    })(<Input placeholder="Key that will be sent from the device" />)}
                                </FormItem>
                            </Col>
                            <Col span={8}>
                                <FormItem key={"key-" + k} label="Validate as">
                                    {getFieldDecorator(`fields[${k}].rule`, {
                                        validateTrigger: ["onChange", "onBlur"],
                                        initialValue: iotDevice.fields[k] && iotDevice.fields[k].rules[0]
                                            ? iotDevice.fields[k].rules[0].id
                                            : undefined,
                                    })(<IotRuleRemoteSelectContainer placeholder="Rules" />)}
                                </FormItem>
                            </Col>
                        </Col>
                        <Col span={2}>
                            <MinusCircleOutlined key={index} onClick={() => this.remove(k)} />
                        </Col>
                    </Row>
                ))}

                <Row type="flex" align="top">

                    <Button type="dashed" onClick={this.add} style={{ width: "80%", margin: "auto", display: "block" }}>
                        <PlusOutlined /> Add Field
                    </Button>

                </Row>
            </Form>
        );
    }
}

export default Form.create()(FieldForm);
