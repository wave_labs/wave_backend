import React, { Component } from "react";
import styled from "styled-components";
import { Row, Col, Form, Input, Button, Upload, Modal, Select, InputNumber } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import IotDeviceTypeRemoteSelectContainer from "../../IotDeviceTypeContent/IotDeviceTypeRemoteSelectContainer";
import IotEnvironmentTypeRemoteSelectContainer from "../../IotEnvironmentTypeContent/IotEnvironmentTypeRemoteSelectContainer";
import IotDeviceStateRemoteSelectContainer from "../../IotDeviceStateContent/IotDeviceStateRemoteSelectContainer";
import UserRemoteSelectContainer from "../../../UserContent/UserRemoteSelectContainer";
import { getBase64, dummyRequest, dimensions } from "helpers";
import IotDeviceRemoteSelectContainer from "../IotDeviceRemoteSelectContainer";
import debounce from "lodash/debounce";

const Option = Select.Option;
const { confirm } = Modal;
const FormItem = Form.Item;

const StyledInputNumber = styled(InputNumber)`
  min-width: 100% !important;
`;

const StyledImage = styled.img`
  width: 50%;
  min-width: 100px;
  cursor: pointer;
  margin: auto;
  display: block;

  &:hover {
    filter: blur(1px) brightness(80%);
  }

  @media (max-width: ${dimensions.md}) {
    width: 100%;
  }
`;

class DeviceForm extends Component {
  constructor(props) {
    super(props);
    this.validateFields = debounce(this.validateFields, 800);

    this.state = {
      category: null,
    };
  }

  handleCategoryChange = (value) => {
    this.setState({
      category: value,
    });
  };

  handleChange = (info) => {
    getBase64(info.file.originFileObj, (imageUrl) =>
      this.props.handlePhoto(info.file.originFileObj, imageUrl)
    );
  };

  rules = {
    name: [
      {
        required: true,
        message: "Name is required",
      },
    ],
    serial_number: [
      {
        required: true,
        message: "Serial number is required",
      },
    ],
    category: [
      {
        required: true,
        message: "Category is required",
      },
    ],
    id_type: [
      {
        required: true,
        message: "Device type is required",
      },
    ],
    id_environment: [
      {
        required: true,
        message: "Environment type is required",
      },
    ],
    id_state: [
      {
        required: true,
        message: "Device state is required",
      },
    ],
  };

  hasCoordinatesRequired = () => {
    return this.state.category === 1
      ? true
      : false;
  };

  componentDidUpdate(prevProps) {
    this.validateFields(prevProps);
  }

  handleInitialValueArray = (array) => {
    var response = [];

    array.map((element) => {
      response.push(element.id);
    })

    return response;
  };

  validateFields = (prevProps) => {
    if (prevProps.form != this.props.form) {
      let requiredFields = ["name", "serial_number", "id_state", "id_type", "id_environment", "category"];
      let values = this.props.form.getFieldsValue();

      let hasError = requiredFields.some((field) => {
        return values[field] == undefined;
      });

      if (!hasError) {
        this.props.handleDisabled(false);
      }
    }
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { iotDevice, previewImage } = this.props;

    var coordinatesRules = [
      {
        required: this.hasCoordinatesRequired(),
        message: "Coordinates are required",
      },
    ];

    return (
      <Form >
        <Row type="flex" gutter={8}>
          <Col xs={24} sm={12}>
            <FormItem label="Name">
              {getFieldDecorator("name", {
                initialValue: iotDevice.name,
                rules: this.rules.name,
              })(<Input placeholder="Device name" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={12}>
            <FormItem label="Serial Number">
              {getFieldDecorator("serial_number", {
                initialValue: iotDevice.serial_number,
                rules: this.rules.serial_number,
              })(<Input placeholder="Unique serial number" />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={this.hasCoordinatesRequired() ? 10 : 24}>
            <FormItem hasFeedback label="Specify if the device is static or moveable">
              {getFieldDecorator("category", {
                initialValue: iotDevice.category.id,
                rules: this.rules.category,
              })(
                <Select
                  style={{ width: "100%" }}
                  placeholder="Category"
                  onChange={this.handleCategoryChange}
                >
                  <Option value={1}>Receiver</Option>
                  <Option value={2}>Collector</Option>
                </Select>
              )}
            </FormItem>
          </Col>
          <Col
            style={{
              display: this.hasCoordinatesRequired() ? "block" : "none",
            }}
            xs={24}
            sm={12}
          >
            <Col span={12}>
              <FormItem hasFeedback label="Latitude">
                {getFieldDecorator("category_latitude", {
                  initialValue: iotDevice.category.position
                    ? iotDevice.category.position.latitude
                    : undefined,
                  rules: coordinatesRules,
                })(<StyledInputNumber placeholder="Latitude" />)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem hasFeedback label="Longitude">
                {getFieldDecorator("category_longitude", {
                  initialValue: iotDevice.category.position
                    ? iotDevice.category.position.longitude
                    : undefined,
                  rules: coordinatesRules,
                })(<StyledInputNumber placeholder="Longitude" />)}
              </FormItem>
            </Col>
          </Col>
          <Col span={24}>
            <FormItem label="Select all users that will be able to see and edit this device aside from you">
              {getFieldDecorator("users", {
                initialValue:
                  iotDevice.users.length > 0
                    ? this.handleInitialValueArray(iotDevice.users)
                    : [],
              })(
                <UserRemoteSelectContainer
                  placeholder="Select users from their emails registered on Wave Labs"
                />
              )}
            </FormItem>
          </Col>

          <Col xs={24} sm={8}>
            <FormItem label="Device Type">
              {getFieldDecorator("id_type", {
                initialValue: iotDevice.type.id,
                rules: this.rules.id_type,
              })(<IotDeviceTypeRemoteSelectContainer />)}
            </FormItem>
          </Col>

          <Col xs={24} sm={8}>
            <FormItem label="Environment Type">
              {getFieldDecorator("id_environment", {
                initialValue: iotDevice.environment.id,
                rules: this.rules.id_environment,
              })(<IotEnvironmentTypeRemoteSelectContainer />)}
            </FormItem>
          </Col>
          <Col xs={24} sm={8}>
            <FormItem label="State">
              {getFieldDecorator("id_state", {
                initialValue: iotDevice.state.id,
                rules: this.rules.id_state,
              })(<IotDeviceStateRemoteSelectContainer />)}
            </FormItem>
          </Col>

          <Col span={24}>
            <FormItem label="Select the gateways / receivers that will communicate with this device">
              {getFieldDecorator("gateways", {
                initialValue:
                  iotDevice.gateways.length > 0
                    ? this.handleInitialValueArray(iotDevice.gateways)
                    : undefined,
              })(
                <IotDeviceRemoteSelectContainer placeholder="Select gateways / receivers from the list" />
              )}
            </FormItem>
          </Col>

          <Col xs={24}>
            <FormItem style={{ width: "60%", margin: "auto" }}>
              <Upload
                customRequest={dummyRequest}
                onChange={this.handleChange}
                style={{ width: "100%", display: "block", margin: "auto" }}
                fileList={false}
              >
                {previewImage ? (
                  <StyledImage src={previewImage} />
                ) : (
                  <Button style={{ width: "100%" }}>
                    <UploadOutlined /> Upload photo
                  </Button>
                )}
              </Upload>
            </FormItem>
          </Col>

        </Row>
      </Form >
    );
  }
}

export default Form.create()(DeviceForm);
