import React, { Component } from "react";
import {
  deleteIotDevice,
  setIotDeviceEdit,
  updateIotDevice,
} from "redux-modules/iotDevice/actions";
import { connect } from "react-redux";
import {
  startCreating,
  startEditing,
} from "redux-modules/editCreateModal/actions";
import CardListContainer from "../../Common/CardListContainer";
import NoDataContainer from "../../../Common/NoDataContainer";

class IotDeviceListContainer extends Component {
  onUpdateClick = (iotDevice) => {
    this.props.setIotDeviceEdit(iotDevice);
    this.props.startEditing();
  };

  handleDuplicateClick = (record) => {
    this.props.setIotDeviceEdit(record);
    this.props.startCreating();
  };

  render() {
    return (
      <div>
        <NoDataContainer
          hasData={this.props.data.length > 0 && true}
          text="No devices found."
        >
          <CardListContainer
            data={this.props.data}
            handleUpdate={this.onUpdateClick}
            handleDuplicate={this.handleDuplicateClick}
            handleDelete={(iotDevice) =>
              this.props.deleteIotDevice(iotDevice.id)
            }
            defaultPath={this.props.deviceType + "/"}
          />
        </NoDataContainer>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteIotDevice: (id) => dispatch(deleteIotDevice(id)),
    startCreating: () => dispatch(startCreating()),
    startEditing: () => dispatch(startEditing()),
    setIotDeviceEdit: (data) => dispatch(setIotDeviceEdit(data)),
    updateIotDevice: (id, data) => dispatch(updateIotDevice(id, data)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.iotDevice.data,
    meta: state.iotDevice.meta,
    loading: state.iotDevice.loading,
    isAdmin: state.auth.isAdmin,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IotDeviceListContainer);
