import React from 'react'
import styled from "styled-components";
import { dimensions } from "helpers";

const Container = styled.div`
    text-align: center;
    width: 80%;
    margin: auto;
    display: block;

    @media (max-width: ${dimensions.md}) {
        width: 100%;
    }

    h2 {
        font-weight: bold;
    }

    p {
        color: #777;
    }
`;

function Information({ title, subtitle }) {
    return (
        <Container>
            <h2>{title}</h2>
            <p>{subtitle}</p>
        </Container>
    )
}

export default Information;
