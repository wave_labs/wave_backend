import React, { Component } from "react";
import { Layout, Menu } from "antd";
import styled from "styled-components";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { menuFromRoute } from "helpers";
import {
  WhaleReporter,
  DiveReporter,
  LitterReporter,
  Triton,
  Seamote,
  Poseidon,
  Intertagua,
  MegafaunaNet,
  MarineLitterNet,
  AcousticNet,
  BoundingBox,
  FreshwaterPin,
  Insect,
  Taxonomy,
  SoftCoral
} from "customIcons";
import Icon from "@ant-design/icons";
import {
  DatabaseOutlined,
  EnvironmentOutlined,
  FolderOpenOutlined,
  ShakeOutlined,
  FieldTimeOutlined,
  QqOutlined,
  FileAddOutlined,
  HomeOutlined,
  ApiOutlined,
  BookOutlined,
  SettingOutlined,
  DeploymentUnitOutlined,
  SolutionOutlined,
  TeamOutlined,
  InboxOutlined,
  MonitorOutlined,
  BarChartOutlined,
  PushpinOutlined,
  ImportOutlined,
} from "@ant-design/icons";
import { Diadema } from "../../customIcons";

const { Sider } = Layout;
const SubMenu = Menu.SubMenu;

const CloseIcon = styled(ImportOutlined)`
  position: absolute;
  top: 10px;
  right: 10px;
  color: white !important;
  font-size: 30px;

  @media (min-width: 500px) {
    display: none !important;
  }
`;
const HomepageLink = styled(Link)`
  width: 90%;
  display: block;
  margin-left: auto;
  margin-right: auto;
  padding-bottom: 5px;
  max-width: 100px;
  img {
    width: 100%;
  }
`;

class SideMenu extends Component {
  state = {
    collapsed: false,
    collapsedWidth: 0,
    sideMenuWidth: 200,
  };

  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  };

  handleCollapsedWidth = () => {
    this.setState({
      collapsedWidth: window.innerWidth > 500 ? 80 : 0,
      sideMenuWidth: window.innerWidth > 500 ? 200 : window.innerWidth,
    });
  };

  componentDidMount() {
    this.handleCollapsedWidth();
    window.addEventListener("resize", () => this.handleCollapsedWidth());
  }

  componentWillUnmount() {
    window.removeEventListener("resize", () => this.handleCollapsedWidth());
  }

  handleItemClick(to) {
    const { collapsedWidth, collapsed } = this.state;
    this.props.history.push(to);
    !collapsed && collapsedWidth == 0 && this.setState({ collapsed: true });
  }

  render() {
    const user = this.props.user;
    const { collapsedWidth, collapsed, sideMenuWidth } = this.state;

    const items = [
      {
        to: "/dashboard",
        icon: <HomeOutlined />,
        name: "Dashboard",
        content: [],
      },
      {
        to: "/dashboard/apps",
        icon: <ShakeOutlined />,
        name: "Apps",
        content: [
          {
            to: "/dashboard/apps/whaleReporter",
            icon: (
              <Icon component={WhaleReporter} style={{ stroke: "#ffffff" }} />
            ),
            name: "Whale Reporter",
          },
          {
            to: "/dashboard/apps/diveReporter",
            icon: (
              <Icon component={DiveReporter} style={{ stroke: "#ffffff" }} />
            ),
            name: "Dive Reporter",
          },
          {
            to: "/dashboard/apps/litterReporter",
            icon: (
              <Icon component={LitterReporter} style={{ fill: "#ffffff" }} />
            ),
            name: "Litter Reporter",
          },
        ],
      },
      {
        to: "/dashboard/iot",
        icon: <ApiOutlined />,
        name: "IoT",
        content: [
          {
            to: "/dashboard/iot/poseidon",
            icon: <Icon component={Poseidon} style={{ fill: "#ffffff" }} />,
            name: "Poseidon",
          },
          {
            to: "/dashboard/iot/seamote",
            icon: <Icon component={Seamote} style={{ fill: "#ffffff" }} />,
            name: "SeaMote",
          },
          {
            to: "/dashboard/iot/triton",
            icon: <Icon component={Triton} style={{ fill: "#ffffff" }} />,
            name: "Triton",
          },
          {
            to: "/dashboard/iot/intertagua",
            icon: <Icon component={Intertagua} style={{ fill: "#ffffff" }} />,
            name: "Intertagua",
          },
        ],
      },
      {
        to: "/dashboard/ai",
        icon: <DeploymentUnitOutlined />,
        name: "AI",
        content: [
          {
            to: "/dashboard/ai/1",
            icon: <Icon component={MegafaunaNet} style={{ fill: "#ffffff" }} />,
            name: "BiodiversityNet",
          },
          {
            to: "/dashboard/ai/2",
            icon: (
              <Icon component={MarineLitterNet} style={{ fill: "#ffffff" }} />
            ),
            name: "LitterNet",
          },
          {
            to: "/dashboard/ai/3",
            icon: <Icon component={AcousticNet} style={{ fill: "#ffffff" }} />,
            name: "AcousticNet",
          },
        ],
      },
      {
        to: "/dashboard/studies",
        icon: <BookOutlined />,
        name: "Studies",
        content: [
          {
            to: "/dashboard/studies/markers",
            icon: <EnvironmentOutlined />,
            name: "Markers",
          },
          {
            to: "/dashboard/studies/stopwatch",
            icon: <FieldTimeOutlined />,
            name: "Stopwatch",
          },
          {
            to: "/dashboard/studies/boundingBox",
            icon: <Icon component={BoundingBox} style={{ fill: "#ffffff" }} />,
            name: "Bounding Box",
          },
          {
            to: "/dashboard/studies/freshwaterPin",
            icon: (
              <Icon component={FreshwaterPin} style={{ fill: "#ffffff" }} />
            ),
            name: "Freshwater Pin",
          },
          {
            to: "/dashboard/studies/diadema",
            icon: <Icon component={Diadema} style={{ fill: "#ffffff" }} />,
            name: "Diadema",
          },
          {
            to: "/dashboard/studies/trout",
            icon: (
              <Icon component={FreshwaterPin} style={{ fill: "#ffffff" }} />
            ),
            name: "Trout",
          },
          {
            to: "/dashboard/studies/climarest",
            icon: (
              <Icon component={FreshwaterPin} style={{ fill: "#ffffff" }} />
            ),
            name: "Climarest",
          },
          {
            to: "/dashboard/studies/softCoral",
            icon: (
              <Icon component={SoftCoral} style={{ fill: "#ffffff" }} />
            ),
            name: "Soft Corals",
          },
        ],
      },
      {
        icon: <Icon component={Insect} style={{ stroke: "#ffffff" }} />,
        name: "Insect",
        content:
          user.roles && user.roles[0].name == "admin"
            ? [
                {
                  to: "/dashboard/insect/species",
                  icon: (
                    <Icon component={Taxonomy} style={{ stroke: "#ffffff" }} />
                  ),
                  name: "Nomenclature",
                },
                {
                  to: "/dashboard/insect/references",
                  icon: <FolderOpenOutlined />,
                  name: "References",
                },
                {
                  to: "/dashboard/insect/occurrences",
                  icon: <EnvironmentOutlined />,
                  name: "Occurrences",
                },
                {
                  to: "/dashboard/insect/projects",
                  icon: (
                    <Icon component={Insect} style={{ stroke: "#ffffff" }} />
                  ),
                  name: "Projects",
                },
              ]
            : [
                {
                  to: "/dashboard/insect/projects",
                  icon: (
                    <Icon component={Insect} style={{ stroke: "#ffffff" }} />
                  ),
                  name: "Projects",
                },
              ],
      },
      {
        to: "/dashboard/data",
        icon: <DatabaseOutlined />,
        name: "Data",
        content: [
          {
            to: "/dashboard/creatures",
            icon: <QqOutlined />,
            name: "Creatures",
          },
          {
            to: "/dashboard/divingSpot",
            icon: <PushpinOutlined />,
            name: "Diving Spots",
          },
          {
            to: "/dashboard/litter/categories",
            icon: <FolderOpenOutlined />,
            name: "Litter",
          },
        ],
      },
    ];

    return (
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={this.onCollapse}
        breakpoint="lg"
        collapsedWidth={collapsedWidth}
        width={sideMenuWidth}
        style={{
          overflowX: !collapsed && (collapsedWidth == 0 ? "hidden" : "auto"),
        }}
      >
        <HomepageLink to="/">
          <img src="/wave.png" />
        </HomepageLink>
        <CloseIcon onClick={() => this.setState({ collapsed: true })} />

        <Menu
          theme="dark"
          mode="inline"
          selectedKeys={[location.pathname]}
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={[menuFromRoute[location.pathname]]}
        >
          {items.map((item) =>
            item.content.length <= 0 ? (
              <Menu.Item
                key={item.to}
                onClick={() => this.handleItemClick(item.to)}
              >
                {item.icon} <span>{item.name}</span>
              </Menu.Item>
            ) : (
              <SubMenu
                key={item.to}
                title={
                  <p onClick={() => this.handleItemClick(item.to)}>
                    {item.icon} <span>{item.name}</span>
                  </p>
                }
              >
                {item.content.map((subitem) => (
                  <Menu.Item
                    key={subitem.to}
                    onClick={() => this.handleItemClick(subitem.to)}
                  >
                    {subitem.icon} <span>{subitem.name}</span>
                  </Menu.Item>
                ))}
              </SubMenu>
            )
          )}

          {user.roles && user.roles[0].name == "admin" && (
            <SubMenu
              key="admin"
              title={
                <span>
                  <SolutionOutlined />
                  <span>Admin</span>
                </span>
              }
            >
              <Menu.Item
                onClick={() => this.handleItemClick("/dashboard/users")}
                key="/dashboard/users"
              >
                <TeamOutlined />
                <span>Users</span>
              </Menu.Item>
              <Menu.Item
                onClick={() => this.handleItemClick("/dashboard/suggestions")}
                key="/dashboard/suggestions"
              >
                <InboxOutlined />
                <span>Suggestions</span>
              </Menu.Item>
              <Menu.Item
                onClick={() => this.handleItemClick("/dashboard/form")}
                key="/dashboard/form"
              >
                <MonitorOutlined />
                <span>Surveys</span>
              </Menu.Item>
              <Menu.Item
                onClick={() => this.handleItemClick("/dashboard/monitor")}
                key="/dashboard/monitor"
              >
                <BarChartOutlined />
                <span>Monitor</span>
              </Menu.Item>
              <Menu.Item
                onClick={() => this.handleItemClick("/dashboard/workplace")}
                key="/dashboard/workplace"
              >
                <FileAddOutlined />
                <span>Workplace</span>
              </Menu.Item>
            </SubMenu>
          )}
        </Menu>
      </Sider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
  };
};

export default withRouter(connect(mapStateToProps)(SideMenu));
