import React from "react";
import { connect } from "react-redux";
import { Select } from "antd";
import { fetchCopernicusProducts } from "redux-modules/copernicusProduct/actions";
import SelectSearch from "components/Dashboard/Common/SelectSearch";
import styled from "styled-components";

const StyledOption = styled.div`
  text-transform: capitalize;
`;

class CopernicusRemoteSelectContainer extends React.Component {
  componentDidMount = () => {
    this.props.fetchCopernicusProducts();
  };

  render() {
    const {
      data,
      loading,
      value,
      onChange,
      mode,
      placeholder = "Products",
    } = this.props;

    return (
      <SelectSearch
        value={value}
        onChange={onChange}
        onSearch={this.onSearch}
        loading={loading}
        placeholder={placeholder}
        mode={mode}
      >
        {data.map((record) => (
          <Select.Option value={record.id} key={record.id}>
            <StyledOption>{record.name}</StyledOption>
          </Select.Option>
        ))}
      </SelectSearch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCopernicusProducts: (filters) =>
      dispatch(fetchCopernicusProducts(filters)),
  };
};

const mapStateToProps = (state) => {
  return {
    data: state.copernicusProduct.data,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CopernicusRemoteSelectContainer);
