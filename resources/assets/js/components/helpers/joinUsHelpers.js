export const VacanciesContent = [
    {
        title: "Mobile App Developer - ARDITI-Porto Santo sem Lixo Marinho-2021-01  - Feb 26, 2021",
        link: "https://www.arditi.pt/concursos-arquive/bolsa-de-investigacao-para-licenciado-arditi-porto-santo-sem-lixo-marinho-2021-01.html",
    },
    {
        title: "Computer Engineer - ARDITI-CLEAN-ATLANTIC-2020-002 - Feb 28, 2020",
        link: "https://www.arditi.pt/index.php/concursos/bolsas-concursos?id=426",
    },
    {
        title: "Marine Sciences - ARDITI-CLEAN-ATLANTIC-2020-001 - Feb 26, 2020",
        link: "https://www.arditi.pt/index.php/concursos/bolsas-concursos?id=423",
    },
    {
        title: "Computer Engineer - RH82 - Dec 11, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/465400",
    },
    {
        title: "Computer Engineer - RH81 - Dec 11, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/465399",
    },
    {
        title: "Computer Engineer - RH80 - Dec 11, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/465397",
    },
    {
        title: "Computer Engineer - RH79 - Dec 11, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/465394",
    },
    {
        title: "Computer Engineer - RH78 - Dec 11, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/465393",
    },
    {
        title: "Marine Sciences - ARDITI-CLEAN-ATLANTIC-2019-001 - Dec 06, 2019",
        link: "https://www.arditi.pt/index.php/concursos/bolsas-concursos?id=412",
    },
    {
        title: "Marine Biologists - ARDITI-INTERTAGUA/2019/001-PDG - Dec 31, 2019",
        link: "http://www.eracareers.pt/opportunities/index.aspx?task=global&jobId=120118",
    },
    {
        title: "Backend Developer - ARDITI-LARSYS-2019-003 - Jul 04, 2019",
        link: "https://iti.larsys.pt/summer-internships-2019/",
    },
    {
        title: "Computer Engineer - RH66 - Nov 11, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/455101",
    },
    {
        title: "Computer Engineer - RH67 - Nov 11, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/455102",
    },
    {
        title: "Machine Learning - RH58 - Sep 13, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/437375",
    },
    {
        title: "Biotelemetry Developer - RH53 - Aug 21, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/430269",
    },
    {
        title: "Biotelemetry Developer - RH49 - Jul 10, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/418031",
    },
    {
        title: "Machine Learning - RH47 - Jul 09, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/415312",
    },
    {
        title: "Backend Developer - RH46 - Jul 04, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/414463",
    },
    {
        title: "Biotelemetry Designer - RH45 - May 28, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/404576",
    },
    {
        title: "Backend Developer - RH30 - Dec 11, 2018",
        link: "https://euraxess.ec.europa.eu/jobs/355942",
    },
    {
        title: "Interaction Design - RH65 - Dec 1, 2019",
        link: "https://euraxess.ec.europa.eu/jobs/455098",
    },
];

export const GalleryContent = [
    {
        title: "Cardboard Dome Outside",
        description: "100% reused material from the local scrapyard",
        image: "/images/gallery/1.png",
    },
    {
        title: "Cardboard Dome Inside",
        description: "Self-sustaining frameless structure with prototyped MDF support",
        image: "/images/gallery/2.png",
    },
    {
        title: "Acrylic Hologram OFF",
        description: "Acrylic Hologram OFF",
        image: "/images/gallery/3.png",
    },
    {
        title: "Acrylic Hologram ON",
        description: "Light scattering for bottom projection creating the illusion of the 3D specie",
        image: "/images/gallery/4.png",
    },
    {
        title: "Whale Projection Mapping",
        description: "Depicting Whales Fixing Carbon",
        image: "/images/gallery/24.png",
    },
    {
        title: "Dome 3D Join",
        description: "PLA Connector Prototype for Geodesic Structure",
        image: "/images/gallery/26.png",
    },
    {
        title: "Turtle Dome",
        description: "Geodesic Structure using Reusable Pallets",
        image: "/images/gallery/25.png",
    },
    {
        title: "Turtle Cross Reality",
        description: "Augmenting Geodesic Domes with XR",
        image: "/images/gallery/21.png",
    },
    {
        title: "Dolphin Dome",
        description: "Exploring Underwater Noise Pollution",
        image: "/images/gallery/22.png",
    },
    {
        title: "Seabird Dome",
        description: "Understanding the Impact of Oil Spills",
        image: "/images/gallery/34.png",
    },
    {
        title: "Seal Dome",
        description: "Using Lantern Interaction for Night Diving",
        image: "/images/gallery/23.png",
    },
    {
        title: "Pallets for Underwater Deployment",
        description: "In-situ Application of Geodesic Structures",
        image: "/images/gallery/35.jpg",
    },
    {
        title: "Geodesic Frame for Biodiversity Monitoring",
        description: "Deployment of Frames for Data Gathering",
        image: "/images/gallery/36.jpg",
    },
    {
        title: "In-situ Aquatic Deployment",
        description: "SCUBA Diving and Snorkelling for Affixing the Structure to Seabed",
        image: "/images/gallery/37.jpg",
    },
    {
        title: "In-situ Aquatic Deployment",
        description: "SCUBA Diving and Snorkelling for Affixing the Structure to Seabed",
        image: "/images/gallery/38.jpg",
    },
    {
        title: "Deep Breath",
        description: "Verifying the Geodesic Structure Position",
        image: "/images/gallery/39.jpg",
    },
    {
        title: "Transect Frame Deployment",
        description: "Affixing the Structure onto Existing Boulders",
        image: "/images/gallery/40.jpg",
    },
    {
        title: "Post-Deployment",
        description: "Wave Divers and Snorkelers",
        image: "/images/gallery/41.jpg",
    },
    {
        title: "Biodiversity Monitoring",
        description: "Reversed Pyramid for Hydrophone and Camera Support",
        image: "/images/gallery/42.jpg",
    },
    {
        title: "POSEIDON IoT In-situ",
        description: "Whale Listening apparatus for whale-watching sea-vessels",
        image: "/images/gallery/5.png",
    },
    {
        title: "POSEIDON App",
        description: "Graphical User Interface for Whale Listening for touristic sea-vessels",
        image: "/images/gallery/10.png",
    },
    {
        title: "POSEIDON Machine Learning",
        description: "Discrimination pipeline for clicks, moans, whistles",
        image: "/images/gallery/11.png",
    },
    {
        title: "LoRattle Controllers",
        description: "Usage of playful interaction with actuators and microcontroller boards for payload transmission and LoRa coverage mapping",
        image: "/images/gallery/6.png",
    },
    {
        title: "LoRattle In-vitro Interaction",
        description: "In-door study of LoRa propagation signal in diverse game mechanics",
        image: "/images/gallery/7.png",
    },
    {
        title: "LoRattle In-situ Interaction",
        description: "Exploration of outdoor setting for coverage propagation",
        image: "/images/gallery/9.png",
    },
    {
        title: "SeaMote App In-Situ",
        description: "Usage of Tangible User Interface for the remote USV operation",
        image: "/images/gallery/8.png",
    },
    {
        title: "SeaMote IoT In-Situ",
        description: "Navigation of the IoT device using LoRa for avoiding the obstacles",
        image: "/images/gallery/12.png",
    },
    {
        title: "SeaMote IoT In-Vitro",
        description: "USV containing the action camera and transmission antenna",
        image: "/images/gallery/15.png",
    },
    {
        title: "SeaMote IoT Underwater",
        description: "Footage from collected underwater media",
        image: "/images/gallery/16.png",
    },
    {
        title: "Tortogram - Flap Display Components",
        description: "Tangible User Interface for Exploring Biodiversity",
        image: "/images/gallery/18.png",
    },
    {
        title: "Tortogram - Flap Display Flaps",
        description: "Prototyped MDF Flaps depicting Marine Data",
        image: "/images/gallery/19.png",
    },
    {
        title: "Tortogram - Flap Display in Action",
        description: "Usage of MCU and REST API for Depicting the Marine Data",
        image: "/images/gallery/20.png",
    },
    {
        title: "Marine Megafauna Tag",
        description: "Epoxy Resin tests with and without Air Pockets",
        image: "/images/gallery/27.png",
    },
    {
        title: "Marine Megafauna Tag",
        description: "Embedded Power Autonomy and MCU",
        image: "/images/gallery/28.png",
    },
    {
        title: "Land Receiver Antenna",
        description: "Power Autonomous LoRa Receiver",
        image: "/images/gallery/29.png",
    },
    {
        title: "Land Receiver Antenna",
        description: "PoE for Lighthouse or Light Poles",
        image: "/images/gallery/30.png",
    },
    {
        title: "Land Receiver Antenna",
        description: "Used Components for Autonomous Operations",
        image: "/images/gallery/31.png",
    },
    {
        title: "Land Receiver Antennas",
        description: "MCUs for PVC Pole Deployments",
        image: "/images/gallery/32.png",
    },
    {
        title: "Land Receiver Antenna",
        description: "Deployment of Solar LoRa Gateways on Lighthouses",
        image: "/images/gallery/33.png",
    },
]; 