import { types } from "./types";
import api from "api/userOccupation";

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_USER_OCCUPATION_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});

export const createUserOccupation = (data) => ({
    type: types.CREATE_USER_OCCUPATION,
    payload: api.createUserOccupation(data),
    meta: { globalError: true }
});