import { types } from "./types";

const initialState = {
    data: [],
    UserOccupationSelector: [],
    meta: {},
    loading: false,
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_USER_OCCUPATION_SELECTOR}_PENDING`:
        case `${types.CREATE_USER_OCCUPATION}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.CREATE_USER_OCCUPATION}_REJECTED`:
        case `${types.FETCH_USER_OCCUPATION_SELECTOR}_REJECTED`:
            return {
                ...state,
                loading: false

            };

        case `${types.CREATE_USER_OCCUPATION}_FULFILLED`:
            return {
                ...state,
                loading: false,
                UserOccupationSelector: [action.payload.data.data, ...state.UserOccupationSelector],
            };

        case `${types.FETCH_USER_OCCUPATION_SELECTOR}_FULFILLED`:
            return {
                ...state,
                loading: false,
                UserOccupationSelector: action.payload.data.data,
            };

        default:
            return state;
    }
};
