import { types } from "./types";
import api from "api/activity";

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_ACTIVITY_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});