import { types } from "./types";
import { views } from "./types";

const initialState = {
	data: [], //sighting list
	meta: {}, //meta information about sightings and pagination
	loading: false,
	current: {
		userable: { user: {} },
		creature: {},
		beaufort_scale: {},
		vehicle: {},
		behaviour: {}
	},
	next: {},
	prior: {},
	view: views.TABLE_VIEW,
	sightingsByCoord: []
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_SIGHTING}_PENDING`:
		case `${types.FETCH_SIGHTINGS}_PENDING`:
		case `${types.FETCH_SIGHTING_NEXT_PRIOR}_PENDING`:
		case `${types.FETCH_SIGHTINGS_COORD}_PENDING`:
		case `${types.FETCH_SIGHTING}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_SIGHTING}_REJECTED`:
		case `${types.DELETE_SIGHTING}_REJECTED`:
		case `${types.FETCH_SIGHTING_NEXT_PRIOR}_REJECTED`:
		case `${types.FETCH_SIGHTINGS}_REJECTED`:
		case `${types.FETCH_SIGHTINGS_COORD}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_SIGHTINGS}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.FETCH_SIGHTING}_FULFILLED`:
			return {
				...state,
				loading: false,
				current: {
					...action.payload.data.data[0]
				},
			};

		case `${types.FETCH_SIGHTING_NEXT_PRIOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				prior: {
					...action.payload.data.previous
				},
				next: {
					...action.payload.data.next
				},
			};

		case `${types.DELETE_SIGHTING}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					sighting => sighting.id !== action.meta.id
				)
			};

		case `${types.CREATE_SIGHTING}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_SIGHTING}_FULFILLED`:
			return {
				...state,
				data: state.data.map((sighting, index) =>
					sighting.id === action.payload.data.data.id
						? action.payload.data.data
						: sighting
				)
			};

		case types.SET_CURRENT_SIGHTING:
			return {
				...state,
				loading: false,
				current: {
					...action.data
				}
			};

		case types.RESET_CURRENT_SIGHTING:
			return {
				...state,
				current: {
					userable: { user: {} },
					creature: {},
					beaufort_scale: {},
					vehicle: {},
					behaviour: {}
				}
			};

		case `${types.FETCH_SIGHTINGS_COORD}_FULFILLED`:
			return {
				...state,
				sightingsByCoord: action.payload.data.data
			};

		default:
			return state;
	}
};
