import { types } from "./types";
import api from "api/sighting"
import axios from "axios";
import { stringify } from "query-string";
import { download } from "helpers";

export const fetchSightings = (page = 1, filters = {}) => ({
    type: types.FETCH_SIGHTINGS,
    payload: api.fetchSightings(page, filters),
    meta: { globalError: true }
});

export const fetchSighting = id => ({
    type: types.FETCH_SIGHTING,
    payload: api.fetchSighting(id),
    meta: { globalError: true }
});

export const fetchSightingNextAndPrior = id => ({
    type: types.FETCH_SIGHTING_NEXT_PRIOR,
    payload: api.fetchSightingNextAndPrior(id),
    meta: { globalError: true }
});

export const deleteSighting = id => ({
    type: types.DELETE_SIGHTING,
    payload: api.deleteSighting(id),
    meta: { id, globalError: true }
});

export const createSighting = data => ({
    type: types.CREATE_SIGHTING,
    payload: api.createSighting(data),
    meta: { globalError: true }
});

export const updateSighting = (id, data) => ({
    type: types.UPDATE_SIGHTING,
    payload: api.updateSighting(id, data),
    meta: { globalError: true }
});

export const setCurrentSighting = data => ({
    type: types.SET_CURRENT_SIGHTING,
    data
});

export const resetCurrentSighting = () => ({
    type: types.RESET_CURRENT_SIGHTING
});

export const createSightingData = id => ({
    type: types.CREATE_SIGHTING_DATA,
    payload: api.createSightingData(id),
    meta: { globalError: true }
});

export const deleteSightingData = id => ({
    type: types.DELETE_SIGHTING_DATA,
    payload: api.deleteSightingData(id),
    meta: { id, globalError: true }
});

export const updateSightingData = (id, data) => ({
    type: types.UPDATE_SIGHTING_DATA,
    payload: api.updateSightingData(id, data),
    meta: { globalError: true }
});

export const setCurrentSightingData = data => ({
    type: types.SET_CURRENT_SIGHTING_DATA,
    data
});

export const resetCurrentSightingData = () => ({
    type: types.RESET_CURRENT_SIGHTING_DATA
});

export const setCurrentSightingDataList = data => ({
    type: types.SET_CURRENT_SIGHTING_DATA_LIST,
    data
});

export const resetCurrentSightingDataList = () => ({
    type: types.RESET_CURRENT_SIGHTING_DATA_LIST
});

export const setSightingView = view => ({
    type: types.SET_SIGHTING_VIEW,
    view
});

export const fetchSightingsCoord = filters => ({
    type: types.FETCH_SIGHTINGS_COORD,
    payload: api.fetchSightingsCoord(filters),
    meta: { globalError: true }
});

export const exportSightingCsv = (filters = {}) => ({
    type: types.EXPORT_SIGHTING_CSV,
    payload: axios({
        url: `${window.location.origin}/api/export/csv/sighting?${stringify(filters, {
            arrayFormat: "index"
        })}`,
        method: "GET",
        responseType: "blob",
    }).then(
        response => {
            download(response, 'whale-reporter.xlsx')
        },
        error => {
            return error.data;
        }
    ),
    meta: { globalError: true }
});

