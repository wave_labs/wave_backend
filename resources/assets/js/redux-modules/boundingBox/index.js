import { types } from "./types";

const initialState = {
	results: [],
	image: null,
	classes: [],
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_BOUNDING_BOX_IMAGES}_PENDING`:
		case `${types.FETCH_BOUNDING_BOX_CLASSES}_PENDING`:
		case `${types.CREATE_BOUNDING_BOX_RESULT}_PENDING`:
		case `${types.FETCH_BOUNDING_BOX_RESULTS}_PENDING`:
		case `${types.DELETE_BOUNDING_BOX_RESULT}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_BOUNDING_BOX_IMAGES}_REJECTED`:
		case `${types.FETCH_BOUNDING_BOX_CLASSES}_REJECTED`:
		case `${types.CREATE_BOUNDING_BOX_RESULT}_REJECTED`:
		case `${types.FETCH_BOUNDING_BOX_RESULTS}_REJECTED`:
		case `${types.DELETE_BOUNDING_BOX_RESULT}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.DELETE_BOUNDING_BOX_RESULT}_FULFILLED`:
			return {
				...state,
				loading: false,
				results: state.results.filter(
					element => element.id !== action.meta.id
				)
			};

		case `${types.FETCH_BOUNDING_BOX_RESULTS}_FULFILLED`:
			return {
				...state,
				loading: false,
				results: action.payload.data.data,
			};

		case `${types.FETCH_BOUNDING_BOX_IMAGES}_FULFILLED`:
			return {
				...state,
				loading: false,
				image: action.payload.data.data.image,
				classes: action.payload.data.data.classes,
			};

		case `${types.FETCH_BOUNDING_BOX_CLASSES}_FULFILLED`:
			return {
				...state,
				loading: false,
				classes: action.payload.data.data,
			};

		default:
			return state;
	}
};
