import { types } from "./types";
import api from "api/boundingBox";

export const fetchBoundingBoxImages = (filters = {}) => ({
    type: types.FETCH_BOUNDING_BOX_IMAGES,
    payload: api.fetchBoundingBoxImages(filters),
    meta: { globalError: true }
});

export const fetchBoundingBoxClasses = (filters = {}) => ({
    type: types.FETCH_BOUNDING_BOX_CLASSES,
    payload: api.fetchBoundingBoxClasses(filters),
    meta: { globalError: true }
});

export const createBoundingBoxResult = (data) => ({
    type: types.CREATE_BOUNDING_BOX_RESULT,
    payload: api.createBoundingBoxResult(data),
    meta: { globalError: true }
});

export const fetchBoundingBoxResults = (page, filters) => ({
    type: types.FETCH_BOUNDING_BOX_RESULTS,
    payload: api.fetchBoundingBoxResults(page, filters),
    meta: { globalError: true }
});

export const deleteBoundingBoxResult = (id) => ({
    type: types.DELETE_BOUNDING_BOX_RESULT,
    payload: api.deleteBoundingBoxResult(id),
    meta: { id, globalError: true }
});
