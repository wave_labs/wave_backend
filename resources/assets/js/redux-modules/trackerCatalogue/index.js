import { types } from "./types";

const initialState = {
	data: [],
	meta: {},
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_AI_DETECTIONS}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_PHOTO}_PENDING`:
		case `${types.FETCH_AI_DETECTIONS}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.DELETE_PHOTO}_REJECTED`:
		case `${types.FETCH_AI_DETECTIONS}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.CREATE_AI_DETECTION}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.DELETE_AI_DETECTION}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					element => element.id !== action.meta.id
				)
			};
		default:
			return state;
	}
};