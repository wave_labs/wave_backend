import { types } from "./types";
import api from "api/trackerCatalogue";

export const fetchAiDetections = (page = 1, filters = {}) => ({
    type: types.FETCH_AI_DETECTIONS,
    payload: api.fetchAiDetections(page, filters),
    meta: { globalError: true }
});

export const createAiDetection = data => ({
    type: types.CREATE_AI_DETECTION,
    payload: api.createAiDetection(data),
    meta: { globalError: true }
});

export const deleteAiDetection = id => ({
    type: types.DELETE_AI_DETECTION,
    payload: api.deleteAiDetection(id),
    meta: { id, globalError: true }
});