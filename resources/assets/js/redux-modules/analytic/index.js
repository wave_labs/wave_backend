import { types } from "./types";

const initialState = {
    loadingAnalyticSighting: false,
    analyticSighting: [],

    loadingAnalyticSightingCreature: false,
    analyticSightingCreature: [],


    loadingAnalyticLitter: false,
    analyticLitter: [],

    loadingAnalyticLitterCategory: false,
    analyticLitterCategory: [],
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_ANALYTIC_SIGHTING}_PENDING`:
            return {
                ...state,
                loadingAnalyticSighting: true
            };
        case `${types.FETCH_ANALYTIC_SIGHTING}_REJECTED`:
            return {
                ...state,
                loadingAnalyticSighting: false
            };
        case `${types.FETCH_ANALYTIC_SIGHTING}_FULFILLED`:
            return {
                ...state,
                loadingAnalyticSighting: false,
                analyticSighting: action.payload.data.data,
            };

        case `${types.FETCH_ANALYTIC_SIGHTING_CREATURE}_PENDING`:
            return {
                ...state,
                loadingAnalyticSightingCreature: true
            };
        case `${types.FETCH_ANALYTIC_SIGHTING_CREATURE}_REJECTED`:
            return {
                ...state,
                loadingAnalyticSightingCreature: false
            };
        case `${types.FETCH_ANALYTIC_SIGHTING_CREATURE}_FULFILLED`:
            return {
                ...state,
                loadingAnalyticSightingCreature: false,
                analyticSightingCreature: action.payload.data.data,
            };


        case `${types.FETCH_ANALYTIC_LITTER}_PENDING`:
            return {
                ...state,
                loadingAnalyticLitter: true
            };
        case `${types.FETCH_ANALYTIC_LITTER}_REJECTED`:
            return {
                ...state,
                loadingAnalyticLitter: false
            };
        case `${types.FETCH_ANALYTIC_LITTER}_FULFILLED`:
            return {
                ...state,
                loadingAnalyticLitter: false,
                analyticLitter: action.payload.data.data,
            };

        case `${types.FETCH_ANALYTIC_LITTER_CATEGORY}_PENDING`:
            return {
                ...state,
                loadingAnalyticLitterCategory: true
            };
        case `${types.FETCH_ANALYTIC_LITTER_CATEGORY}_REJECTED`:
            return {
                ...state,
                loadingAnalyticLitterCategory: false
            };
        case `${types.FETCH_ANALYTIC_LITTER_CATEGORY}_FULFILLED`:
            return {
                ...state,
                loadingAnalyticLitterCategory: false,
                analyticLitterCategory: action.payload.data.data,
            };


        default:
            return state;
    }
};
