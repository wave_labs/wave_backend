import { types } from "./types";
import api from "api/analytic";

export const fetchAnalyticSighting = (filters = {}) => ({
    type: types.FETCH_ANALYTIC_SIGHTING,
    payload: api.fetchAnalyticSighting(filters),
    meta: { globalError: true }
});

export const fetchAnalyticSightingCreature = (filters = {}) => ({
    type: types.FETCH_ANALYTIC_SIGHTING_CREATURE,
    payload: api.fetchAnalyticSightingCreature(filters),
    meta: { globalError: true }
});

export const fetchAnalyticLitter = (filters = {}) => ({
    type: types.FETCH_ANALYTIC_LITTER,
    payload: api.fetchAnalyticLitter(filters),
    meta: { globalError: true }
});

export const fetchAnalyticLitterCategory = (filters = {}) => ({
    type: types.FETCH_ANALYTIC_LITTER_CATEGORY,
    payload: api.fetchAnalyticLitterCategory(filters),
    meta: { globalError: true }
});




