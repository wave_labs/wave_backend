import { types } from "./types";
import api from "api/softCoral";
import axios from "axios";
import { stringify } from "query-string";
import { download } from "helpers";

export const fetchSoftCorals = (page = 1, filters = {}) => ({
    type: types.FETCH_SOFT_CORALS,
    payload: api.fetchSoftCorals(page, filters),
    meta: { globalError: true }
});

export const fetchSoftCoralCoordinates = (filters = {}) => ({
    type: types.FETCH_SOFT_CORAL_COORDINATES,
    payload: api.fetchSoftCoralCoordinates(filters),
    meta: { globalError: true }
});

export const fetchSoftCoral = id => ({
    type: types.FETCH_SOFT_CORAL,
    payload: api.fetchSoftCoral(id),
    meta: { id, globalError: true }
});

export const deleteSoftCoral = id => ({
    type: types.DELETE_SOFT_CORAL,
    payload: api.deleteSoftCoral(id),
    meta: { id, globalError: true }
});

export const createSoftCoral = data => ({
    type: types.CREATE_SOFT_CORAL,
    payload: api.createSoftCoral(data),
    meta: { globalError: true }
});

export const updateSoftCoral = (id, data) => ({
    type: types.UPDATE_SOFT_CORAL,
    payload: api.updateSoftCoral(id, data),
    meta: { globalError: true }
});

export const setCurrentMarker = data => ({
    type: types.SET_CURRENT_SOFT_CORAL,
    data
});

export const resetCurrentMarker = data => ({
    type: types.RESET_CURRENT_SOFT_CORAL
});

export const exportSoftCoral = (filters = {}) => ({
    type: types.EXPORT_SOFT_CORALS,
    payload: axios({
        url: `${window.location.origin}/api/export/csv/soft-coral?${stringify(filters, {
            arrayFormat: "index"
        })}`,
        method: "GET",
        responseType: "blob",
    }).then(
        response => {
            download(response, ' SoftCoral.xlsx')
        },
        error => {
            return error.data;
        }
    ),
    meta: { globalError: true }
});
