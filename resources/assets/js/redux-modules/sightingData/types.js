export const types = {
    FETCH_SIGHTING_DATA: 'FETCH_SIGHTING_DATA',
};

export const views = {
    MAP_VIEW: 'MAP_VIEW',
    TABLE_VIEW:  'TABLE_VIEW'
}