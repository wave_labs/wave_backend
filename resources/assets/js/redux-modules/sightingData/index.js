import { types } from "./types";
import { views } from "./types";

const initialState = {
	data: [], //sighting list
	meta: {}, //meta information about sightings and pagination
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_SIGHTING_DATA}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_SIGHTING_DATA}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_SIGHTING_DATA}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		default:
			return state;
	}
};
