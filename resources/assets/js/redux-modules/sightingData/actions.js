import { types } from "./types";
import api from "api/sighting"

export const fetchSightingData = (page = 1, filters = {}) => ({
    type: types.FETCH_SIGHTING_DATA,
    payload: api.fetchSightingData(page, filters),
    meta: { globalError: true }
});
