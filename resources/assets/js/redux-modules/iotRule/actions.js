import { types } from "./types";
import api from "api/iot/rule";

export const fetchSelector = (filters) => ({
    type: types.FETCH_IOT_RULE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});


