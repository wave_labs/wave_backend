import { types } from "./types";

const initialState = {
	data: [],
	selector: [],
	meta: {},
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_IOT_RULE_SELECTOR}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_IOT_RULE_SELECTOR}_REJECTED`:
			return {
				...state,
				loading: false,
			};

		case `${types.FETCH_IOT_RULE_SELECTOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				selector: action.payload.data.data,
			};

		default:
			return state;
	}
};
