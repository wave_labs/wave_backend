import { types } from "./types";
import { views } from "./types";

const initialState = {
	data: [], //IotDevice list
	IotDeviceSelector: [], //User list
	meta: {}, //meta information about IotDevices and pagination
	loading: false,
	current: {
		
	},
	currentGateway: {
		type: {},
		state: {},
		environment: {},
		category: {
			position: {
				latitude: 32,
				longitude: -16
			}
		},
		fields: [],
		users: [],
		gateways: [],
	},
	next: {},
	prior: {},
	editIotDevice: { //used to pass edit information to form
		type: {},
		state: {},
		environment: {},
		category: {},
		fields: [],
		users: [],
		gateways: [],
		sections: [],
	},
	view: views.TABLE_VIEW
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_IOT_DEVICE}_PENDING`:
		case `${types.FETCH_IOT_GATEWAY}_PENDING`:
		case `${types.FETCH_IOT_DEVICES}_PENDING`:
		case `${types.FETCH_IOT_DEVICE_SELECTOR}_PENDING`:
		case `${types.FETCH_IOT_DEVICE}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_IOT_DEVICES}_REJECTED`:
		case `${types.DELETE_IOT_DEVICE}_REJECTED`:
		case `${types.FETCH_IOT_DEVICE}_REJECTED`:
		case `${types.FETCH_IOT_GATEWAY}_REJECTED`:
		case `${types.FETCH_IOT_DEVICE_NEXT_PRIOR}_REJECTED`:
		case `${types.FETCH_IOT_DEVICE_SELECTOR}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_IOT_DEVICES}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.FETCH_IOT_DEVICE}_FULFILLED`:
			return {
				...state,
				loading: false,
				current: {
					...action.payload.data.data
				}
			};
		case `${types.FETCH_IOT_GATEWAY}_FULFILLED`:
			return {
				...state,
				loading: false,
				currentGateway: {
					...action.payload.data.data
				}
			};

		case `${types.FETCH_IOT_DEVICE_SELECTOR}_FULFILLED`:
			return {
				...state,
				IotDeviceSelector: action.payload.data.data,
			};

		case `${types.DELETE_IOT_DEVICE}_FULFILLED`:
			return {
				...state,
				data: state.data.filter(
					iotDevice => iotDevice.id !== action.meta.id
				)
			};

		case `${types.CREATE_IOT_DEVICE}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_IOT_DEVICE}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(iotDevice, index) =>
						iotDevice.id === action.payload.data.data.id
							? action.payload.data.data
							: iotDevice
				)
			};

		case types.SET_IOT_DEVICE_EDIT:
			return {
				...state,
				editIotDevice: action.data
			};

		case types.RESET_IOT_DEVICE_EDIT:
			return {
				...state,
				editIotDevice: {
					type: {},
					state: {},
					environment: {},
					category: {},
					fields: [],
					users: [],
					gateways: [],
				}
			};

		case types.SET_IOT_DEVICE_VIEW:
			return {
				...state,
				view: action.view
			};

		default:
			return state;
	}
};
