import { types } from "./types";
import api from "api/iotDevice";

export const fetchIotDevices = (filters = {}) => ({
    type: types.FETCH_IOT_DEVICES,
    payload: api.fetchIotDevices(filters),
    meta: { globalError: true }
});

export const fetchIotDevice = id => ({
    type: types.FETCH_IOT_DEVICE,
    payload: api.fetchIotDevice(id),
    meta: { globalError: true }
});


export const fetchIotGateway = id => ({
    type: types.FETCH_IOT_GATEWAY,
    payload: api.fetchIotGateway(id),
    meta: { globalError: true }
});

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_IOT_DEVICE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});

export const deleteIotDevice = id => ({
    type: types.DELETE_IOT_DEVICE,
    payload: api.deleteIotDevice(id),
    meta: { id, globalError: true }
});

export const fetchIotDeviceNextAndPrior = id => ({
    type: types.FETCH_IOT_DEVICE_NEXT_PRIOR,
    payload: api.fetchIotDeviceNextAndPrior(id),
    meta: { globalError: true }
});

export const createIotDevice = data => ({
    type: types.CREATE_IOT_DEVICE,
    payload: api.createIotDevice(data),
    meta: { globalError: true }
});

export const updateIotDevice = (id, data) => ({
    type: types.UPDATE_IOT_DEVICE,
    payload: api.updateIotDevice(id, data),
    meta: { globalError: true }
});

export const setIotDeviceEdit = data => ({
    type: types.SET_IOT_DEVICE_EDIT,
    data
});

export const resetIotDeviceEdit = data => ({
    type: types.RESET_IOT_DEVICE_EDIT
});


export const setIotDeviceView = view => ({
    type: types.SET_IOT_DEVICE_VIEW,
    view
});
