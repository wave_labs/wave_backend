import { types } from "./types";
import api from "api/coordinate"

export const fetchCenter = () => ({
    type: types.FETCH_CENTER,
    payload: api.fetchCenter(),
    meta: { globalError: true }
});

export const fetchIndex = filters => ({
    type: types.FETCH_INDEX,
    payload: api.fetchIndex(filters),
    meta: { globalError: true }
});
