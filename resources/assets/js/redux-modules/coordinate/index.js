import { types } from "./types";

const initialState = {
	data: [], //DivingSpot list
	litter: [],
	sighting: [],
	dive: [],
	center: [],
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_CENTER}_PENDING`:
		case `${types.FETCH_INDEX}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_CENTER}_REJECTED`:
		case `${types.FETCH_INDEX}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_CENTER}_FULFILLED`:
			return {
				...state,
				loading: false,
				center: action.payload.data,
			};

		case `${types.FETCH_INDEX}_FULFILLED`:
			return {
				...state,
				loading: false,
				litter: action.payload.data.data.litter,
				dive: action.payload.data.data.dive,
				sighting: action.payload.data.data.sighting,
			};


		default:
			return state;
	}
};
