import { types } from "./types";
import api from "api/form";

export const fetchForm = (page = 1, filters = {}) => ({
    type: types.FETCH_FORM,
    payload: api.fetchForm(page, filters),
    meta: { globalError: true }
});

export const deleteForm = id => ({
    type: types.DELETE_FORM,
    payload: api.deleteForm(id),
    meta: { id, globalError: true }
});

export const createForm = data => ({
    type: types.CREATE_FORM,
    payload: api.createForm(data),
    meta: { globalError: true }
});

export const updateForm = (id, data) => ({
    type: types.UPDATE_FORM,
    payload: api.updateForm(id, data),
    meta: { globalError: true }
});

export const setFormEdit = data => ({
    type: types.SET_FORM_EDIT,
    data
});

export const resetFormEdit = data => ({
    type: types.RESET_FORM_EDIT
});
