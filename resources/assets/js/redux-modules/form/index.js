import { types } from "./types";

const initialState = {
	data: [], //Form list
	meta: {}, //meta information about Form and pagination
	loading: false,
	editForm: { //used to pass edit information to form
		user: {}
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_FORM}_PENDING`:
		case `${types.FETCH_FORM}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_FORM}_REJECTED`:
		case `${types.DELETE_FORM}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_FORM}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_FORM}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					forms => forms.id !== action.meta.id
				)
			};

		case `${types.CREATE_FORM}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_FORM}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(forms, index) =>
						forms.id === action.payload.data.data.id
							? action.payload.data.data
							: forms
				)
			};

		case types.SET_FORM_EDIT:
			return {
				...state,
				editForm: action.data
			};

		case types.RESET_FORM_EDIT:
			return {
				...state,
				editForm: {
					user: {}
				}
			};

		default:
			return state;
	}
};
