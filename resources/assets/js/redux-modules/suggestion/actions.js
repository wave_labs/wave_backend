import { types } from "./types";
import api from "api/suggestion";

export const fetchSuggestions = (page = 1, filters = {}) => ({
    type: types.FETCH_SUGGESTIONS,
    payload: api.fetchSuggestions(page, filters),
    meta: { globalError: true }
});

export const deleteSuggestion = id => ({
    type: types.DELETE_SUGGESTION,
    payload: api.deleteSuggestion(id),
    meta: { id, globalError: true }
});

export const createSuggestion = data => ({
    type: types.CREATE_SUGGESTION,
    payload: api.createSuggestion(data),
    meta: { globalError: true }
});

export const updateSuggestion = (id, data) => ({
    type: types.UPDATE_SUGGESTION,
    payload: api.updateSuggestion(id, data),
    meta: { globalError: true }
});

export const setSuggestionEdit = data => ({
    type: types.SET_SUGGESTION_EDIT,
    data
});

export const resetSuggestionEdit = data => ({
    type: types.RESET_SUGGESTION_EDIT
});
