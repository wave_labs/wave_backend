import { types } from "./types";

const initialState = {
	data: [], //Suggestion list
	meta: {}, //meta information about Suggestions and pagination
	loading: false,
	editSuggestion: { //used to pass edit information to form
		userable: { user: {} },
		source: {},
		suggestionType: {}
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_SUGGESTION}_PENDING`:
		case `${types.FETCH_SUGGESTIONS}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_SUGGESTIONS}_REJECTED`:
		case `${types.DELETE_SUGGESTION}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_SUGGESTIONS}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_SUGGESTION}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					suggestion => suggestion.id !== action.meta.id
				)
			};

		case `${types.CREATE_SUGGESTION}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_SUGGESTION}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(suggestion, index) =>
						suggestion.id === action.payload.data.data.id
							? action.payload.data.data
							: suggestion
				)
			};

		case types.SET_SUGGESTION_EDIT:
			return {
				...state,
				editSuggestion: action.data
			};

		case types.RESET_SUGGESTION_EDIT:
			return {
				...state,
				editSuggestion: {
					userable: { user: {} },
					suggestionType: {},
					source: {},
				}
			};

		default:
			return state;
	}
};
