import { types } from "./types";

const initialState = {
	data: {
		userable: { certificates: [], user: {} },
		roles: [{}],
	},
	history: [],
	meta: {}, //meta information about Users and pagination
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_ME}_PENDING`:
		case `${types.FETCH_ME}_PENDING`:
		case `${types.FETCH_HISTORY}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_ME}_REJECTED`:
		case `${types.FETCH_HISTORY}_REJECTED`:
		case `${types.DELETE_ME}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_ME}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.FETCH_HISTORY}_FULFILLED`:
			return {
				...state,
				loading: false,
				history: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_ME}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(me => me.id !== action.meta.id)
			};

		default:
			return state;
	}
};
