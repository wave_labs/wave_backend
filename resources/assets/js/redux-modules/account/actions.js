import { types } from "./types";
import api from "api/account";

export const fetchMe = () => ({
    type: types.FETCH_ME,
    payload: api.fetchMe(),
    meta: { globalError: true }
});

export const fetchHistory = filters => ({
    type: types.FETCH_HISTORY,
    payload: api.fetchHistory(filters),
    meta: { globalError: true }
});

export const deleteMe = id => ({
    type: types.DELETE_ME,
    payload: api.deleteMe(id),
    meta: { id, globalError: true }
});

export const setMeEdit = data => ({
    type: types.SET_ME_EDIT,
    data
});

export const resetMeEdit = data => ({
    type: types.RESET_ME_EDIT
});

