export const types = {
    FETCH_ME: "FETCH_ME",
    FETCH_HISTORY: "FETCH_HISTORY",
    DELETE_ME: "DELETE_ME",
    UPDATE_ME: "UPDATE_ME",

    SET_ME_EDIT: "SET_ME_EDIT",
    RESET_ME_EDIT: "RESET_ME_EDIT",

    SET_USER_VIEW: 'SET_USER_VIEW',
};