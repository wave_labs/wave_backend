import { types } from "./types";

const initialState = {
  data: [], //
  meta: {}, //meta information about litter and pagination
  basicTaxonSelector: [],
  subTaxonSelector: [],
  infraTaxonSelector: [],
  superTaxonSelector: [],
  specificSelector: [],
  infraSpecificSelector: [],
  loading: false,
  current: {},
  selector: [],
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case `${types.FETCH_INSECT_SPECIES}_PENDING`:
    case `${types.FETCH_INSECT_SPECIE}_PENDING`:
    case `${types.UPDATE_INSECT_SPECIE}_PENDING`:
    case `${types.UPDATE_TAXON}_PENDING`:
    case `${types.CREATE_INSECT_SPECIE}_PENDING`:
    case `${types.DELETE_INSECT_SPECIE}_PENDING`:
    case `${types.SET_INSECT_SPECIE_CURRENT}_PENDING`:
    case `${types.FETCH_INSECT_KINGDOM_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_PHYLUM_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_CLASS_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_ORDER_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_SUB_ORDER_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_INFRA_ORDER_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_SUPER_FAMILY_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_FAMILY_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_SUB_FAMILY_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_TRIBE_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_GENUS_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_SUB_GENUS_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_SPECIFIC_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_INFRA_SPECIFIC_SELECTOR}_PENDING`:
    case `${types.INSECT_IMPORT_TAXON}_PENDING`:
      return {
        ...state,
        loading: true,
      };

    case `${types.FETCH_INSECT_SPECIES}_REJECTED`:
    case `${types.FETCH_INSECT_SPECIE}_REJECTED`:
    case `${types.UPDATE_INSECT_SPECIE}_REJECTED`:
    case `${types.DELETE_INSECT_SPECIE}_REJECTED`:
    case `${types.CREATE_INSECT_SPECIE}_REJECTED`:
    case `${types.FETCH_INSECT_KINGDOM_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_PHYLUM_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_CLASS_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_ORDER_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_SUB_ORDER_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_INFRA_ORDER_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_SUPER_FAMILY_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_FAMILY_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_SUB_FAMILY_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_TRIBE_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_GENUS_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_SUB_GENUS_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_SPECIFIC_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_INFRA_SPECIFIC_SELECTOR}_REJECTED`:
    case `${types.UPDATE_TAXON}_REJECTED`:
    case `${types.INSECT_IMPORT_TAXON}_REJECTED`:
      return {
        ...state,
        loading: false,
      };

    case `${types.FETCH_INSECT_SPECIES}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: action.payload.data.data,
        meta: action.payload.data.meta,
      };

    case `${types.FETCH_INSECT_SPECIE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        current: action.payload.data.data,
      };

    case `${types.CREATE_INSECT_SPECIE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: [action.payload.data.data, ...state.data],
      };

    case `${types.UPDATE_INSECT_SPECIE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: state.data.map((record, index) =>
          record.id === action.payload.data.data.id
            ? action.payload.data.data
            : record
        ),
      };
    case `${types.UPDATE_TAXON}_FULFILLED`:
      return {
        ...state,
        loading: false,
      };
    case `${types.DELETE_INSECT_SPECIE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: state.data.filter((record) => record.id !== action.meta.id),
      };

    case `${types.SET_INSECT_SPECIE_CURRENT}`:
      return {
        ...state,
        current: action.payload,
      };

    case `${types.FETCH_INSECT_KINGDOM_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        basicTaxonSelector: action.payload.data.data,
      };
    case `${types.FETCH_INSECT_PHYLUM_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        basicTaxonSelector: action.payload.data.data,
      };
    case `${types.FETCH_INSECT_CLASS_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        basicTaxonSelector: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_ORDER_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        basicTaxonSelector: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_SUB_ORDER_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        subTaxonSelector: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_INFRA_ORDER_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        infraTaxonSelector: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_SUPER_FAMILY_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        superTaxonSelector: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_FAMILY_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        basicTaxonSelector: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_SUB_FAMILY_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        subTaxonSelector: action.payload.data.data,
      };
    case `${types.FETCH_INSECT_TRIBE_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        infraTaxonSelector: action.payload.data.data,
      };
    case `${types.FETCH_INSECT_GENUS_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        basicTaxonSelector: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_SUB_GENUS_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        subTaxonSelector: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_SPECIFIC_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        specificSelector: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_INFRA_SPECIFIC_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        infraSpecificSelector: action.payload.data.data,
      };

    case `${types.INSECT_IMPORT_TAXON}_FULFILLED`:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
};
