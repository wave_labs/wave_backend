import { types } from "./types";
import api from "api/insect/species";

export const fetchInsectSpecies = (page, filters) => ({
  type: types.FETCH_INSECT_SPECIES,
  payload: api.fetchInsectSpecies(page, filters),
  meta: { globalError: true },
});

export const fetchInsectSpecie = (filters) => ({
  type: types.FETCH_INSECT_SPECIE,
  payload: api.fetchInsectSpecie(filters),
  meta: { globalError: true },
});

export const createInsectSpecie = (data) => ({
  type: types.CREATE_INSECT_SPECIE,
  payload: api.createInsectSpecie(data),
  meta: { globalError: true },
});

export const updateInsectSpecie = (id, data) => ({
  type: types.UPDATE_INSECT_SPECIE,
  payload: api.updateInsectSpecie(id, data),
  meta: { globalError: true },
});

export const updateTaxon = (taxonRank, id, data) => ({
  type: types.UPDATE_TAXON,
  payload: api.updateTaxon(taxonRank, id, data),
  meta: { globalError: true },
});

export const deleteInsectSpecie = (data) => ({
  type: types.DELETE_INSECT_SPECIE,
  payload: api.deleteInsectSpecie(data),
  meta: { globalError: true },
});

export const setInsectSpecieCurrent = (record = {}) => ({
  type: types.SET_INSECT_SPECIE_CURRENT,
  payload: record,
});

export const fetchInsectKingdomSelector = (filters) => ({
  type: types.FETCH_INSECT_KINGDOM_SELECTOR,
  payload: api.fetchInsectKingdomSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectPhylumSelector = (filters) => ({
  type: types.FETCH_INSECT_PHYLUM_SELECTOR,
  payload: api.fetchInsectPhylumSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectClassSelector = (filters) => ({
  type: types.FETCH_INSECT_CLASS_SELECTOR,
  payload: api.fetchInsectClassSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectOrderSelector = (filters) => ({
  type: types.FETCH_INSECT_ORDER_SELECTOR,
  payload: api.fetchInsectOrderSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectSubOrderSelector = (filters) => ({
  type: types.FETCH_INSECT_SUB_ORDER_SELECTOR,
  payload: api.fetchInsectSubOrderSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectInfraOrderSelector = (filters) => ({
  type: types.FETCH_INSECT_INFRA_ORDER_SELECTOR,
  payload: api.fetchInsectInfraOrderSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectSuperFamilySelector = (filters) => ({
  type: types.FETCH_INSECT_SUPER_FAMILY_SELECTOR,
  payload: api.fetchInsectSuperFamilySelector(filters),
  meta: { globalError: true },
});

export const fetchInsectFamilySelector = (filters) => ({
  type: types.FETCH_INSECT_FAMILY_SELECTOR,
  payload: api.fetchInsectFamilySelector(filters),
  meta: { globalError: true },
});

export const fetchInsectSubFamilySelector = (filters) => ({
  type: types.FETCH_INSECT_SUB_FAMILY_SELECTOR,
  payload: api.fetchInsectSubFamilySelector(filters),
  meta: { globalError: true },
});

export const fetchInsectTribeSelector = (filters) => ({
  type: types.FETCH_INSECT_TRIBE_SELECTOR,
  payload: api.fetchInsectTribeSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectGenusSelector = (filters) => ({
  type: types.FETCH_INSECT_GENUS_SELECTOR,
  payload: api.fetchInsectGenusSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectSubGenusSelector = (filters) => ({
  type: types.FETCH_INSECT_SUB_GENUS_SELECTOR,
  payload: api.fetchInsectSubGenusSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectSpecificSelector = (filters) => ({
  type: types.FETCH_INSECT_SPECIFIC_SELECTOR,
  payload: api.fetchInsectSpecificSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectInfraSpecificSelector = (filters) => ({
  type: types.FETCH_INSECT_INFRA_SPECIFIC_SELECTOR,
  payload: api.fetchInsectInfraSpecificSelector(filters),
  meta: { globalError: true },
});

export const insectImportTaxon = (data) => ({
  type: types.INSECT_IMPORT_TAXON,
  payload: api.insectImportTaxon(data),
  meta: { globalError: true },
});
