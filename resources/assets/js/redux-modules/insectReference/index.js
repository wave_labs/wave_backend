import { types } from "./types";
import { views } from "./types";

const initialState = {
  data: [], //
  selectorReferences: [], //
  selectorAuthors: [],
  meta: {}, //meta information about litter and pagination
  loading: false,
  current: {},
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case `${types.FETCH_INSECT_REFERENCES}_PENDING`:
    case `${types.FETCH_INSECT_REFERENCE}_PENDING`:
    case `${types.UPDATE_INSECT_REFERENCE}_PENDING`:
    case `${types.CREATE_INSECT_REFERENCE}_PENDING`:
    case `${types.FETCH_INSECT_REFERENCES_SELECTOR}_PENDING`:
    case `${types.DELETE_INSECT_REFERENCE}_PENDING`:
      return {
        ...state,
        loading: true,
      };

    case `${types.FETCH_INSECT_REFERENCES}_REJECTED`:
    case `${types.FETCH_INSECT_REFERENCE}_REJECTED`:
    case `${types.UPDATE_INSECT_REFERENCE}_REJECTED`:
    case `${types.CREATE_INSECT_REFERENCE}_REJECTED`:
    case `${types.FETCH_INSECT_REFERENCES_SELECTOR}_REJECTED`:
    case `${types.DELETE_INSECT_REFERENCE}_REJECTED`:
      return {
        ...state,
        loading: false,
      };

    case `${types.FETCH_INSECT_REFERENCES_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        selectorReferences: action.payload.data.data,
      };

    case `${types.FETCH_REFERENCE_AUTHORS}_FULFILLED`:
      return {
        ...state,
        loading: false,
        selectorAuthors: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_REFERENCES}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: action.payload.data.data,
        meta: action.payload.data.meta,
      };

    case `${types.FETCH_INSECT_REFERENCE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        current: action.payload.data.data,
        data: state.data.map((record, index) =>
          record.id === action.payload.data.data.id
            ? action.payload.data.data
            : record
        ),
      };

    case `${types.DELETE_INSECT_REFERENCE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: state.data.filter((record) => record.id !== action.meta.id),
      };

    case `${types.CREATE_INSECT_REFERENCE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: [action.payload.data.data, ...state.data],
      };

    case `${types.UPDATE_INSECT_REFERENCE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: state.data.map((record, index) =>
          record.id === action.payload.data.data.id
            ? action.payload.data.data
            : record
        ),
      };

    case `${types.SET_INSECT_REFERENCE_CURRENT}`:
      return {
        ...state,
        current: action.payload,
      };

    default:
      return state;
  }
};
