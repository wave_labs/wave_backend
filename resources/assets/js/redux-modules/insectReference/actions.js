import { types } from "./types";
import api from "api/insect/reference";

export const fetchInsectReferences = (page, filters) => ({
  type: types.FETCH_INSECT_REFERENCES,
  payload: api.fetchInsectReferences(page, filters),
  meta: { globalError: true },
});

export const fetchInsectReferencesSelector = (filters) => ({
  type: types.FETCH_INSECT_REFERENCES_SELECTOR,
  payload: api.fetchInsectReferencesSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectReference = (filters) => ({
  type: types.FETCH_INSECT_REFERENCE,
  payload: api.fetchInsectReference(filters),
  meta: { globalError: true },
});

export const fetchReferenceAuthorsSelector = (filters) => ({
  type: types.FETCH_REFERENCE_AUTHORS,
  payload: api.fetchReferenceAuthorsSelector(filters),
  meta: { globalError: true },
});

export const createInsectReference = (data) => ({
  type: types.CREATE_INSECT_REFERENCE,
  payload: api.createInsectReference(data),
  meta: { globalError: true },
});

export const updateInsectReference = (id, data) => ({
  type: types.UPDATE_INSECT_REFERENCE,
  payload: api.updateInsectReference(id, data),
  meta: { globalError: true },
});

export const deleteInsectReference = (id) => ({
  type: types.DELETE_INSECT_REFERENCE,
  payload: api.deleteInsectReference(id),
  meta: { id, globalError: true },
});

export const setInsectReferenceCurrent = (record = {}) => ({
  type: types.SET_INSECT_REFERENCE_CURRENT,
  payload: record,
});
