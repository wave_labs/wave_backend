import { types } from "./types";

const initialState = {
	data: [], //Iot device type list
	IotEnvironmentTypeSelector: [], //User list
	meta: {}, //meta information about Iot device types and pagination
	loading: false,
	editIotEnvironmentType: { //used to pass edit information to form
		type: {},
		defaultFields : []
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_IOT_ENVIRONMENT_TYPE_SELECTOR}_PENDING`:
			return {
				...state,
				loading: true
			};
		case `${types.FETCH_IOT_ENVIRONMENT_TYPE_SELECTOR}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_IOT_ENVIRONMENT_TYPE_SELECTOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				IotEnvironmentTypeSelector: action.payload.data.data,
			};

		default:
			return state;
	}
};
