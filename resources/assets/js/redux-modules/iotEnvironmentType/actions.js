import { types } from "./types";
import api from "api/iotEnvironmentType";


export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_IOT_ENVIRONMENT_TYPE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});

