import { types } from "./types";
import { views } from "./types";

const initialState = {
    data: [], //
    selector: [], //
    meta: {}, //meta information about litter and pagination
    loading: false,
    current: {},
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_INSECT_REPORTS}_PENDING`:
        case `${types.FETCH_INSECT_REPORT}_PENDING`:
        case `${types.UPDATE_INSECT_REPORT}_PENDING`:
        case `${types.CREATE_INSECT_REPORT}_PENDING`:
        case `${types.FETCH_INSECT_REPORTS_SELECTOR}_PENDING`:
        case `${types.DELETE_INSECT_REPORT}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_INSECT_REPORTS}_REJECTED`:
        case `${types.FETCH_INSECT_REPORT}_REJECTED`:
        case `${types.UPDATE_INSECT_REPORT}_REJECTED`:
        case `${types.CREATE_INSECT_REPORT}_REJECTED`:
        case `${types.FETCH_INSECT_REPORTS_SELECTOR}_REJECTED`:
        case `${types.DELETE_INSECT_REPORT}_REJECTED`:
            return {
                ...state,
                loading: false,
            };

        case `${types.FETCH_INSECT_REPORTS_SELECTOR}_FULFILLED`:
            return {
                ...state,
                loading: false,
                selector: action.payload.data.data,
            };

        case `${types.FETCH_INSECT_REPORTS}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: action.payload.data.data,
                meta: action.payload.data.meta,
            };

        case `${types.FETCH_INSECT_REPORT}_FULFILLED`:
            return {
                ...state,
                loading: false,
                current: action.payload.data.data,
            };

        case `${types.DELETE_INSECT_REPORT}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: state.data.filter(record => record.id !== action.meta.id)
            };

        case `${types.CREATE_INSECT_REPORT}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: [action.payload.data.data, ...state.data]
            };

        case `${types.UPDATE_INSECT_REPORT}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: state.data.map((record, index) =>
                    record.id === action.payload.data.data.id
                        ? action.payload.data.data
                        : record
                )
            };

        case `${types.SET_INSECT_REPORT_CURRENT}`:
            return {
                ...state,
                current: action.payload
            };

        default:
            return state;
    }
};
