import { types } from "./types";
import api from "api/insect/report";

export const fetchInsectReports = (page, filters) => ({
    type: types.FETCH_INSECT_REPORTS,
    payload: api.fetchInsectReports(page, filters),
    meta: { globalError: true }
});

export const fetchInsectReportsSelector = (filters) => ({
    type: types.FETCH_INSECT_REPORTS_SELECTOR,
    payload: api.fetchInsectReportsSelector(filters),
    meta: { globalError: true }
});

export const fetchInsectReport = (filters) => ({
    type: types.FETCH_INSECT_REPORT,
    payload: api.fetchInsectReport(filters),
    meta: { globalError: true }
});

export const createInsectReport = (data) => ({
    type: types.CREATE_INSECT_REPORT,
    payload: api.createInsectReport(data),
    meta: { globalError: true }
});

export const updateInsectReport = (id, data) => ({
    type: types.UPDATE_INSECT_REPORT,
    payload: api.updateInsectReport(id, data),
    meta: { globalError: true }
});

export const deleteInsectReport = (id) => ({
    type: types.DELETE_INSECT_REPORT,
    payload: api.deleteInsectReport(id),
    meta: { id, globalError: true }
});


export const setInsectReportCurrent = (record = {}) => ({
    type: types.SET_INSECT_REPORT_CURRENT,
    payload: record
});


