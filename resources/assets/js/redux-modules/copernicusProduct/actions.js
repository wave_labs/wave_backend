import { types } from "./types";
import api from "api/copernicusProduct"

export const fetchCopernicusProducts = () => ({
    type: types.FETCH_COPERNICUS_PRODUCTS,
    payload: api.fetchCopernicusProducts(),
    meta: { globalError: true }
});