import { types } from "./types";

const initialState = {
	data: [], //DivingSpot list
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_COPERNICUS_PRODUCTS}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_COPERNICUS_PRODUCTS}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_COPERNICUS_PRODUCTS}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
			};


		default:
			return state;
	}
};
