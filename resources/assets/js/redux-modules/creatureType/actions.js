import { types } from "./types";
import api from "api/creatureType";

export const fetchCreatureTypes = (page = 1, filters = {}) => ({
    type: types.FETCH_CREATURE_TYPES,
    payload: api.fetchCreatureTypes(page, filters),
    meta: { globalError: true }
});

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_CREATURE_TYPE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});

export const deleteCreatureType = id => ({
    type: types.DELETE_CREATURE_TYPE,
    payload: api.deleteCreatureType(id),
    meta: { id, globalError: true }
});

export const createCreatureType = data => ({
    type: types.CREATE_CREATURE_TYPE,
    payload: api.createCreatureType(data),
    meta: { globalError: true }
});

export const updateCreatureType = (id, data) => ({
    type: types.UPDATE_CREATURE_TYPE,
    payload: api.updateCreatureType(id, data),
    meta: { globalError: true }
});

export const setCreatureTypeEdit = data => ({
    type: types.SET_CREATURE_TYPE_EDIT,
    data
});

export const resetCreatureTypeEdit = data => ({
    type: types.RESET_CREATURE_TYPE_EDIT
});
