import { types } from "./types";

const initialState = {
	data: [], //CreatureType list
	CreatureTypeSelector: [],
	meta: {}, //meta information about CreatureTypes and pagination
	loading: false,
	editCreatureType: {
		//used to pass edit information to form
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_CREATURE_TYPE}_PENDING`:
		case `${types.FETCH_CREATURE_TYPE_SELECTOR}_PENDING`:
		case `${types.FETCH_CREATURE_TYPES}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_CREATURE_TYPES}_REJECTED`:
		case `${types.DELETE_CREATURE_TYPE}_REJECTED`:
		case `${types.FETCH_CREATURE_TYPE_SELECTOR}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_CREATURE_TYPES}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.FETCH_CREATURE_TYPE_SELECTOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				CreatureTypeSelector: action.payload.data.data,
			};

		case `${types.DELETE_CREATURE_TYPE}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					creatureType => creatureType.id !== action.meta.id
				)
			};

		case `${types.CREATE_CREATURE_TYPE}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_CREATURE_TYPE}_FULFILLED`:
			return {
				...state,
				data: state.data.map((creatureType, index) =>
					creatureType.id === action.payload.data.data.id
						? action.payload.data.data
						: creatureType
				)
			};

		case types.SET_CREATURE_TYPE_EDIT:
			return {
				...state,
				editCreatureType: action.data
			};

		case types.RESET_CREATURE_TYPE_EDIT:
			return {
				...state,
				editCreatureType: {
					user: {}
				}
			};

		default:
			return state;
	}
};
