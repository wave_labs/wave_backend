import { types } from "./types";
import api from "api/iot/feature";

export const fetchIotFeatures = (page, filters) => ({
    type: types.FETCH_IOT_FEATURES,
    payload: api.fetchIotFeatures(page, filters),
    meta: { globalError: true }
});

export const fetchIotFeature = (id) => ({
    type: types.FETCH_IOT_FEATURE,
    payload: api.fetchIotFeature(id),
    meta: { globalError: true }
});

export const fetchSelector = (filters) => ({
    type: types.FETCH_IOT_FEATURE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});


