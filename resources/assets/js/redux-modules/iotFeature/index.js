import { types } from "./types";

const initialState = {
	data: [],
	selector: [],
	current: {},
	meta: {},
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_IOT_FEATURES}_PENDING`:
		case `${types.FETCH_IOT_FEATURE}_PENDING`:
		case `${types.FETCH_IOT_FEATURE_SELECTOR}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_IOT_FEATURES}_REJECTED`:
		case `${types.FETCH_IOT_FEATURE_SELECTOR}_REJECTED`:
		case `${types.FETCH_IOT_FEATURE}_REJECTED`:
			return {
				...state,
				loading: false,
			};

		case `${types.FETCH_IOT_FEATURE}_REJECTED`:
			return {
				...state,
				loading: false,
				current: { ...action.payload.data.data }
			};

		case `${types.FETCH_IOT_FEATURE_SELECTOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				selector: action.payload.data.data,
			};
		case `${types.FETCH_IOT_FEATURES}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		default:
			return state;
	}
};
