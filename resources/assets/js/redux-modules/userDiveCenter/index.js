import { types } from "./types";

const initialState = {
	data: [], //userDiveCenter list
	meta: {}, //meta information about userDiveCenters and pagination
	loading: false,
	editUserDiveCenter: { //used to pass edit information to form
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_USER_DIVE_CENTER}_PENDING`:
		case `${types.FETCH_USER_DIVE_CENTERS}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_USER_DIVE_CENTERS}_REJECTED`:
		case `${types.DELETE_USER_DIVE_CENTER}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_USER_DIVE_CENTERS}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_USER_DIVE_CENTER}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					userDiveCenter => userDiveCenter.id !== action.meta.id
				)
			};

		case `${types.CREATE_USER_DIVE_CENTER}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_USER_DIVE_CENTER}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(userDiveCenter, index) =>
						userDiveCenter.id === action.payload.data.data.id
							? action.payload.data.data
							: userDiveCenter
				)
			};

		case types.SET_USER_DIVE_CENTER_EDIT:
			return {
				...state,
				editUserDiveCenter: action.data
			};

		case types.RESET_USER_DIVE_CENTER_EDIT:
			return {
				...state,
				editUserDiveCenter: {
					user: {}
				}
			};

		default:
			return state;
	}
};
