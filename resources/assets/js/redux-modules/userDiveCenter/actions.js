import { types } from "./types";
import api from "api/userDiveCenter";

export const fetchUserDiveCenters = (page = 1, filters = {}) => ({
    type: types.FETCH_USER_DIVE_CENTERS,
    payload: api.fetchUserDiveCenters(page, filters),
    meta: { globalError: true }
});

export const deleteUserDiveCenter = id => ({
    type: types.DELETE_USER_DIVE_CENTER,
    payload: api.deleteUserDiveCenter(id),
    meta: { id, globalError: true }
});

export const createUserDiveCenter = data => ({
    type: types.CREATE_USER_DIVE_CENTER,
    payload: api.createUserDiveCenter(data),
    meta: { globalError: true }
});

export const updateUserDiveCenter = (id, data) => ({
    type: types.UPDATE_USER_DIVE_CENTER,
    payload: api.updateUserDiveCenter(id, data),
    meta: { globalError: true }
});

export const setUserDiveCenterEdit = data => ({
    type: types.SET_USER_DIVE_CENTER_EDIT,
    data
});

export const resetUserDiveCenterEdit = data => ({
    type: types.RESET_USER_DIVE_CENTER_EDIT
});
