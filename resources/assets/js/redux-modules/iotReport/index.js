import { types } from "./types";
import { views } from "./types";

const initialState = {
	data: [], //IotReport list
	meta: {}, //meta information about iotReport and pagination
	loading: false,
	errors: false,
	current: {
		//currently open iotReport
		device: {},
		fields: [{}]
	},
	next: {},
	prior: {},
	editIotReport: {
		//used to pass edit information to form
		device: {},
		fields: [{}]
	},
	loadingExport: false,
	iotReportsByCoord: [],
	iotReportsByCoordForLineDraw: [],
	view: views.TABLE_VIEW,
	latestReport: {
		fields: []
	},
	hasReportChanged: null,
	fieldsTimeline: {
		labels: [],
		values: [],
	},
	customGraph: {
		values: undefined,
		labels: undefined,
	},
	loadingCustomGraph: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.GENERATE_CUSTOM_GRAPH}_PENDING`:
			return {
				...state,
				loadingCustomGraph: true
			};
		case `${types.GENERATE_CUSTOM_GRAPH}_REJECTED`:
			return {
				...state,
				loadingCustomGraph: false
			};

		case `${types.GENERATE_CUSTOM_GRAPH}_FULFILLED`:
			return {
				...state,
				customGraph: action.payload.data.data,
				loadingCustomGraph: false
			};

		case `${types.LATEST_IOT_REPORT}_FULFILLED`:
			return {
				...state,
				latestReport: action.payload.data.data,
				hasReportChanged: action.payload.data.data.id
			};



		case `${types.PUSH_TO_FIELDS_TIMELINE}`:
			return {
				...state,
				fieldsTimeline: {
					labels: [...state.fieldsTimeline.labels.filter((e, i) => i !== 0), ...action.meta.data.labels],
					values: action.meta.data.values
				},
			};

		case `${types.FETCH_FIELDS_TIMELINE}_FULFILLED`:
			return {
				...state,
				fieldsTimeline: action.payload.data.data
			};

		case `${types.EXPORT_IOT_REPORT_CSV}_PENDING`:
			return {
				...state,
				loadingExport: true
			};

		case `${types.DELETE_IOT_REPORT}_PENDING`:
		case `${types.FETCH_IOT_REPORTS_COORD}_PENDING`:
		case `${types.FETCH_IOT_REPORTS_COORD_FOR_LINE_DRAW}_PENDING`:
		case `${types.FETCH_IOT_REPORTS}_PENDING`:
		case `${types.FETCH_IOT_REPORT_NEXT_PRIOR}_PENDING`:
		case `${types.FETCH_SHOW_IOT_REPORT}_PENDING`:
		case `${types.FETCH_FIELDS_TIMELINE}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_IOT_REPORTS}_REJECTED`:
		case `${types.FETCH_SHOW_IOT_REPORT}_REJECTED`:
		case `${types.FETCH_IOT_REPORTS_COORD}_REJECTED`:
		case `${types.FETCH_IOT_REPORTS_COORD_FOR_LINE_DRAW}_REJECTED`:
		case `${types.FETCH_IOT_REPORT_NEXT_PRIOR}_REJECTED`:
		case `${types.DELETE_IOT_REPORT}_REJECTED`:
			return {
				...state,
				loading: false,
			};

		case `${types.EXPORT_IOT_REPORT_CSV}_REJECTED`:
		case `${types.EXPORT_IOT_REPORT_CSV}_FULFILLED`:
			return {
				...state,
				loadingExport: false
			};

		case `${types.FETCH_IOT_REPORTS}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.FETCH_SHOW_IOT_REPORT}_FULFILLED`:
			return {
				...state,
				loading: false,
				current: {
					...action.payload.data.data
				}
			};

		case `${types.FETCH_IOT_REPORT_NEXT_PRIOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				prior: {
					...action.payload.data.previous
				},
				next: {
					...action.payload.data.next
				},
			};

		case `${types.DELETE_IOT_REPORT}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(iotReport => iotReport.id !== action.meta.id)
			};

		case `${types.CREATE_IOT_REPORT}_REJECTED`: {
			return {
				...state,
				loading: false,
				errors: true
			}
		}

		case `${types.CREATE_IOT_REPORT}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_IOT_REPORT}_FULFILLED`:
			return {
				...state,
				data: state.data.map((iotReport, index) =>
					iotReport.id === action.payload.data.data.id
						? action.payload.data.data
						: iotReport
				)
			};

		case types.SET_CURRENT_IOT_REPORT:
			return {
				...state,
				loading: false,
				current: {
					...action.data
				}
			};

		case types.RESET_CURRENT_IOT_REPORT:
			return {
				...state,
				current: {
					userable: { user: {} },
					iotReport_sub_category: {},
					iotReport_category: {},
					photo: [{}]
				}
			};

		case types.SET_IOT_REPORT_EDIT:
			return {
				...state,
				editIotReport: action.data
			};

		case types.RESET_IOT_REPORT_EDIT:
			return {
				...state,
				editIotReport: {
					userable: { user: {} },
					iotReport_sub_category: {},
					iotReport_category: {},
					photo: [{}]
				}
			};

		case `${types.FETCH_IOT_REPORTS_COORD}_FULFILLED`:
			return {
				...state,
				iotReportsByCoord: action.payload.data.data
			};

		case `${types.FETCH_IOT_REPORTS_COORD_FOR_LINE_DRAW}_FULFILLED`:
			return {
				...state,
				iotReportsByCoordForLineDraw: action.payload.data
			};

		case types.SET_IOT_REPORT_VIEW:
			return {
				...state,
				view: action.view
			};

		default:
			return state;
	}
};
