import { types } from "./types";
import api from "api/iotReport";
import axios from "axios";
import { stringify } from "query-string";
import { download } from "helpers";

export const fetchIotReports = (page = 1, filters = {}) => ({
    type: types.FETCH_IOT_REPORTS,
    payload: api.fetchIotReports(page, filters),
    meta: { globalError: true }
});


export const latestIotReport = (filters = {}) => ({
    type: types.LATEST_IOT_REPORT,
    payload: api.latestIotReport(filters),
    meta: { globalError: true }
});

export const fetchFieldsTimeline = (filters = {}) => ({
    type: types.FETCH_FIELDS_TIMELINE,
    payload: api.fetchFieldsTimeline(filters),
    meta: { globalError: true }
});

export const pushToFieldsTimeline = (data) => ({
    type: types.PUSH_TO_FIELDS_TIMELINE,
    meta: { data, globalError: true }
});

export const generateCustomGraph = (filters = {}) => ({
    type: types.GENERATE_CUSTOM_GRAPH,
    payload: api.generateCustomGraph(filters),
    meta: { globalError: true }
});

export const fetchShowIotReport = id => ({
    type: types.FETCH_SHOW_IOT_REPORT,
    payload: api.fetchShowIotReport(id),
    meta: { globalError: true }
});

export const deleteIotReport = id => ({
    type: types.DELETE_IOT_REPORT,
    payload: api.deleteIotReport(id),
    meta: { id, globalError: true }
});

export const fetchIotReportNextAndPrior = id => ({
    type: types.FETCH_IOT_REPORT_NEXT_PRIOR,
    payload: api.fetchIotReportNextAndPrior(id),
    meta: { globalError: true }
});


export const createIotReport = data => ({
    type: types.CREATE_IOT_REPORT,
    payload: api.createIotReport(data),
    meta: { globalError: true }
});

export const updateIotReport = (id, data) => ({
    type: types.UPDATE_IOT_REPORT,
    payload: api.updateIotReport(id, data),
    meta: { globalError: true }
});

export const setIotReportEdit = data => ({
    type: types.SET_IOT_REPORT_EDIT,
    data
});

export const resetIotReportEdit = data => ({
    type: types.RESET_IOT_REPORT_EDIT
});

export const fetchIotReportsCoord = id => ({
    type: types.FETCH_IOT_REPORTS_COORD,
    payload: api.fetchIotReportsCoord(id),
    meta: { globalError: true }
});

export const fetchIotReportsCoordForLineDraw = id => ({
    type: types.FETCH_IOT_REPORTS_COORD_FOR_LINE_DRAW,
    payload: api.fetchIotReportsCoordForLineDraw(id),
    meta: { globalError: true }
});

export const setIotReportView = view => ({
    type: types.SET_IOT_REPORT_VIEW,
    view
});

export const setCurrentIotReport = data => ({
    type: types.SET_CURRENT_IOT_REPORT,
    data
});

export const resetCurrentIotReport = () => ({
    type: types.RESET_CURRENT_IOT_REPORT
});

export const exportIotReportCsv = (filters = {}) => ({
    type: types.EXPORT_IOT_REPORT_CSV,
    payload: axios({
        url: `${window.location.origin}/api/export/csv/iot-report?${stringify(filters, {
            arrayFormat: "index"
        })}`,
        method: "GET",
        responseType: "blob",
    }).then(
        response => {
            download(response, ' iotReport.xlsx')
        },
        error => {
            return error.data;
        }
    ),
    meta: { globalError: true }
});

