import { types } from "./types";
import { formatCenter } from "helpers";

const initialState = {
	enm: undefined,
	data: [],
	alerts: [],
	meta: {},
	hasFailed: false,
	ocurrenceData: [],
	variables: {
		bioclim: [],
		models: [],
		kfold: 5,
		resolution: null,
	},
	ocurrenceCenter: {
		lat: parseFloat(32.6),
		lng: parseFloat(-16.9),
	},
	loading: false,
	loadingPrediction: false,
	predictions: []
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.CREATE_OCURRENCE_DATA}_PENDING`:
		case `${types.UPLOAD_ENV_DATA}_PENDING`:
		case `${types.GET_COPERNICUS_DATA}_PENDING`:
		case `${types.GET_OCURRENCE_DATA_FROM_PIPELINE}_PENDING`:
		case `${types.FETCH_ENMS}_PENDING`:
		case `${types.DELETE_ENM}_PENDING`:
		case `${types.DELETE_OCURRENCE_DATA_DUPLICATES}_PENDING`:
		case `${types.QUERY_OCURRENCE_DATA}_PENDING`:
		case `${types.LIMIT_OCURRENCE_DATA}_PENDING`:
		case `${types.GET_ENV_DATA}_PENDING`:
		case `${types.DELETE_OCURRENCE_DATA_RECORD}_PENDING`:
		case `${types.CROP_OCURRENCE_DATA_GEOGRAPHIC_EXTENT}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.DELETE_ENM}_REJECTED`:
		case `${types.FETCH_ENMS}_REJECTED`:
			return {
				...state,
				loading: true
			};

		case `${types.DELETE_ENM}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					e => e.id !== action.meta.id
				)
			};

		case `${types.FETCH_ENMS}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.CREATE_PREDICTION}_PENDING`:
			return {
				...state,
				loadingPrediction: true
			};
		case `${types.UPLOAD_ENV_DATA}_REJECTED`:
			return {
				...state,
				loading: false,
				hasFailed: true,
				alerts: action.payload.response.status == 413 ? ["Directory payload is too large"] : ["Something unexpected happenned"],
			};

		case `${types.GET_COPERNICUS_DATA}_REJECTED`:
			return {
				...state,
				loading: false,
				hasFailed: true,
				alerts: action.payload.response.data.errors,
			};


		case `${types.QUERY_OCURRENCE_DATA}_REJECTED`:
			return {
				...state,
				loading: false,
				ocurrenceData: [],
				ocurrenceCenter: {
					lat: parseFloat(32.6),
					lng: parseFloat(-16.9),
				},
			};

		case `${types.CREATE_OCURRENCE_DATA}_REJECTED`:
		case `${types.GET_OCURRENCE_DATA_FROM_PIPELINE}_REJECTED`:
			return {
				...state,
				loading: false,
				alerts: action.payload.response.data.errors ? (action.payload.response.data.errors.length ? action.payload.response.data.errors : ["Something unexpected happened"]) : ["Something unexpected happened"],
			};
		case `${types.CREATE_PREDICTION}_REJECTED`:
			return {
				...state,
				loadingPrediction: false,
				alerts: action.payload.response.data.errors ? (action.payload.response.data.errors.length ? action.payload.response.data.errors : ["Something unexpected happened"]) : ["Something unexpected happened"],
				hasFailed: true,
			};

		case `${types.DELETE_OCURRENCE_DATA_DUPLICATES}_REJECTED`:
		case `${types.CROP_OCURRENCE_DATA_GEOGRAPHIC_EXTENT}_REJECTED`:
		case `${types.DELETE_OCURRENCE_DATA_RECORD}_REJECTED`:
		case `${types.LIMIT_OCURRENCE_DATA}_REJECTED`:
		case `${types.GET_ENV_DATA}_REJECTED`:
		case `${types.GET_ENV_DATA}_FULFILLED`:
			return {
				...state,
				loading: false
			};

		case `${types.CREATE_OCURRENCE_DATA}_FULFILLED`:
		case `${types.QUERY_OCURRENCE_DATA}_FULFILLED`:
		case `${types.GET_OCURRENCE_DATA_FROM_PIPELINE}_FULFILLED`:
			return {
				...state,
				loading: false,
				enm: action.payload.data.enm.id,
				ocurrenceData: action.payload.data.data,
				ocurrenceCenter: formatCenter(action.payload.data.center),
			};

		case `${types.DELETE_OCURRENCE_DATA_DUPLICATES}_FULFILLED`:
		case `${types.DELETE_OCURRENCE_DATA_RECORD}_FULFILLED`:
		case `${types.LIMIT_OCURRENCE_DATA}_FULFILLED`:
		case `${types.CROP_OCURRENCE_DATA_GEOGRAPHIC_EXTENT}_FULFILLED`:
			return {
				...state,
				loading: false,
				ocurrenceData: action.payload.data.data,
			};

		case `${types.SET_BIOCLIM_VARS}`:
			return {
				...state,
				variables: { bioclim: action.payload.bioclim, models: [], resolution: action.payload.resolution, kfold: 5 },
			};

		case `${types.SET_MODELS_VARS}`:
			return {
				...state,
				variables: { ...state.variables, ...action.payload },
			};

		case `${types.CREATE_PREDICTION}_FULFILLED`:
			return {
				...state,
				loadingPrediction: false,
				predictions: action.payload.data.data.predictions
			};

		case `${types.SET_ALERTS}`:
			return {
				...state,
				alerts: action.payload
			};

		case `${types.RESET_ALERTS}`:
			return {
				...state,
				alerts: [],
			};

		case `${types.RESET_ENM}`:
			return {
				...state,
				enm: undefined,
				hasFailed: false,
				ocurrenceData: [],
				variables: {
					bioclim: [],
					models: [],
					resolution: [],
					kfold: 5
				},
				ocurrenceCenter: {
					lat: parseFloat(32.6),
					lng: parseFloat(-16.9),
				},
				predictions: []
			};

		case `${types.SET_ENM}`:
			return {
				...state,
				enm: action.payload,
			};

		default:
			return state;
	}
};
