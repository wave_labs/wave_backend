import { types } from "./types";
import api from "api/enm";

export const createOcurrenceData = (data, filters = {}) => ({
    type: types.CREATE_OCURRENCE_DATA,
    payload: api.createOcurrenceData(data, filters),
    meta: { globalError: true }
});

export const getOcurrenceDataFromPipeline = (filters = {}) => ({
    type: types.GET_OCURRENCE_DATA_FROM_PIPELINE,
    payload: api.getOcurrenceDataFromPipeline(filters),
    meta: { globalError: true }
});

export const uploadEnvironmentalData = (data, id) => ({
    type: types.UPLOAD_ENV_DATA,
    payload: api.uploadEnvironmentalData(data, id),
    meta: { globalError: true }
});

export const getCopernicusData = (id, filters) => ({
    type: types.GET_COPERNICUS_DATA,
    payload: api.getCopernicusData(id, filters),
    meta: { globalError: true }
});

export const fetchEnms = (page = 1, filters = {}) => ({
    type: types.FETCH_ENMS,
    payload: api.fetchEnms(page, filters),
    meta: { globalError: true }
});

export const deleteEnm = id => ({
    type: types.DELETE_ENM,
    payload: api.deleteEnm(id),
    meta: { id, globalError: true }
});


export const queryOcurrenceData = (filters = {}) => ({
    type: types.QUERY_OCURRENCE_DATA,
    payload: api.queryOcurrenceData(filters),
    meta: { globalError: true }
});

export const deleteOcurrenceDataDuplicates = (id) => ({
    type: types.DELETE_OCURRENCE_DATA_DUPLICATES,
    payload: api.deleteOcurrenceDataDuplicates(id),
    meta: { globalError: true }
});


export const cropOcurrenceDataGeographicExtent = (id, data) => ({
    type: types.CROP_OCURRENCE_DATA_GEOGRAPHIC_EXTENT,
    payload: api.cropOcurrenceDataGeographicExtent(id, data),
    meta: { globalError: true }
});

export const deleteOcurrenceDataRecord = (id, record) => ({
    type: types.DELETE_OCURRENCE_DATA_RECORD,
    payload: api.deleteOcurrenceDataRecord(id, record),
    meta: { globalError: true }
});

export const limitOcurrenceData = (id, limit) => ({
    type: types.LIMIT_OCURRENCE_DATA,
    payload: api.limitOcurrenceData(id, limit),
    meta: { globalError: true }
});

export const getEnvData = (id) => ({
    type: types.GET_ENV_DATA,
    payload: api.getEnvData(id),
    meta: { globalError: true }
});

export const setBioclimVars = (array) => ({
    type: types.SET_BIOCLIM_VARS,
    payload: array,
    meta: { globalError: true }
});

export const setModelsVars = (array) => ({
    type: types.SET_MODELS_VARS,
    payload: array,
    meta: { globalError: true }
});

export const createPrediction = (id, data) => ({
    type: types.CREATE_PREDICTION,
    payload: api.createPrediction(id, data),
    meta: { globalError: true }
});

export const resetEnm = () => ({
    type: types.RESET_ENM,
});

export const setEnm = (id) => ({
    type: types.SET_ENM,
    payload: id,
});

export const setAlerts = (alerts) => ({
    type: types.SET_ALERTS,
    payload: alerts,
});
export const resetAlerts = () => ({
    type: types.RESET_ALERTS,
});





