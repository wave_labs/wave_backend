import { types } from "./types";

const initialState = {
	data: [],
	meta: {},
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_AI_DETECTION_IMAGES}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.FETCH_AI_DETECTION_IMAGES}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_AI_DETECTION_IMAGES}_REJECTED`:
			return {
				...state,
				loading: false
			};

		default:
			return state;
	}
};