import { types } from "./types";
import api from "api/ai/detectionImage";

export const fetchAiDetectionImages = (page = 1, filters = {}) => ({
    type: types.FETCH_AI_DETECTION_IMAGES,
    payload: api.fetchAiDetectionImages(page, filters),
    meta: { globalError: true }
});