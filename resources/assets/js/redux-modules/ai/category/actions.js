import { types } from "./types";
import api from "api/ai/category";

export const fetchAiCategories = (page, filters) => ({
    type: types.FETCH_AI_CATEGORIES,
    payload: api.fetchAiCategories(page, filters),
    meta: { globalError: true }
});

export const fetchAiCategory = (id) => ({
    type: types.FETCH_AI_CATEGORY,
    payload: api.fetchAiCategory(id),
    meta: { globalError: true }
});





