import { types } from "./types";

const initialState = {
    data: [],
    current: {
        models: [],
        labels: []
    },
    loading: false,
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_AI_CATEGORIES}_PENDING`:
        case `${types.FETCH_AI_CATEGORY}_PENDING`:
        case `${types.FETCH_AI_CATEGORIES}_REJECTED`:
        case `${types.FETCH_AI_CATEGORY}_REJECTED`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_AI_CATEGORIES}_FULFILLED`:
            return {
                ...state,
                data: action.payload.data,
                loading: false
            };

        case `${types.FETCH_AI_CATEGORY}_FULFILLED`:
            return {
                ...state,
                current: action.payload.data.data,
                loading: false
            };



        default:
            return state;
    }
};
