import { types } from "./types";
import api from "api/sightingBehaviour";

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_SIGHTING_BEHAVIOUR_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});