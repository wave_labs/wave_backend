import { types } from "./types";

const initialState = {
    data: [],
    SightingBehaviourSelector: [],
    meta: {},
    loading: false,
    editSightingBehaviour: {
        //used to pass edit information to form
    }
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_SIGHTING_BEHAVIOUR_SELECTOR}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_SIGHTING_BEHAVIOUR_SELECTOR}_REJECTED`:
            return {
                ...state,
                loading: false
            };

        case `${types.FETCH_SIGHTING_BEHAVIOUR_SELECTOR}_FULFILLED`:
            return {
                ...state,
                loading: false,
                SightingBehaviourSelector: action.payload.data.data,
            };

        default:
            return state;
    }
};
