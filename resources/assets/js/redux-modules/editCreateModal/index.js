import { editCreateModalTypes } from './types'

const initialState = {
	editing: false,
	creating: false,
}

export default (state = initialState, action = {}) => {
	switch(action.type) {
		case editCreateModalTypes.START_EDITING:
			return {
				editing: true,
				creating: false,
			}	

		case editCreateModalTypes.START_CREATING:
			return {
				editing: false,
				creating: true,
			}

		case editCreateModalTypes.RESET_MODAL:
			return {
				editing: false,
				creating: false,
			}	

		default: 
			return state;
	}
}