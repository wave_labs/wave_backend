import { editCreateModalTypes } from "./types";

export const startEditing = () => ({ type: editCreateModalTypes.START_EDITING });

export const startCreating = () => ({ type: editCreateModalTypes.START_CREATING });

export const resetModal = () => ({ type: editCreateModalTypes.RESET_MODAL });

