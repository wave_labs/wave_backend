export const editCreateModalTypes = {
    START_EDITING: 'START_EDITING',
    START_CREATING: 'START_CREATING',
    RESET_MODAL: 'RESET_MODAL',
};