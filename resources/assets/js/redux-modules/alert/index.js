import { alertTypes } from './types';

export default function alert(state = {}, action) {
  switch (action.type) {
    case alertTypes.SUCCESS:
      return {
        type: 'success',
        message: action.message.message,
        description: action.message.description
      };
    case alertTypes.ERROR:
      return {
        type: 'error',
        message: action.message.message,
        description: action.message.description
      };
    case alertTypes.CLEAR:
      return {  };
    default:
      return state
  }
}