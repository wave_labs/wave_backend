import { types } from "./types";
import api from "api/capsule";

export const fetchCapsules = (page = 1, filters = {}) => ({
    type: types.FETCH_CAPSULES,
    payload: api.fetchCapsules(page, filters),
    meta: { globalError: true }
});

export const deleteCapsule = id => ({
    type: types.DELETE_CAPSULE,
    payload: api.deleteCapsule(id),
    meta: { id, globalError: true }
});

export const createCapsule = data => ({
    type: types.CREATE_CAPSULE,
    payload: api.createCapsule(data),
    meta: { globalError: true }
});

export const updateCapsule = (id, data) => ({
    type: types.UPDATE_CAPSULE,
    payload: api.updateCapsule(id, data),
    meta: { globalError: true }
});

export const setCapsuleEdit = data => ({
    type: types.SET_CAPSULE_EDIT,
    data
});

export const resetCapsuleEdit = data => ({
    type: types.RESET_CAPSULE_EDIT
});
