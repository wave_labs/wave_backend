import { types } from "./types";

const initialState = {
	data: [], //Capsule list
	meta: {}, //meta information about Capsules and pagination
	loading: false,
	editCapsule: { //used to pass edit information to form
		user: {}
		
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_CAPSULE}_PENDING`:
		case `${types.FETCH_CAPSULES}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_CAPSULES}_REJECTED`:
		case `${types.DELETE_CAPSULE}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_CAPSULES}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_CAPSULE}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					capsule => capsule.id !== action.meta.id
				)
			};

		case `${types.CREATE_CAPSULE}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_CAPSULE}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(capsule, index) =>
						capsule.id === action.payload.data.data.id
							? action.payload.data.data
							: capsule
				)
			};

		case types.SET_CAPSULE_EDIT:
			return {
				...state,
				editCapsule: action.data
			};

		case types.RESET_CAPSULE_EDIT:
			return {
				...state,
				editCapsule: {
					user: {}
				}
			};

		default:
			return state;
	}
};
