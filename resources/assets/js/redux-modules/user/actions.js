import { types } from "./types";
import api from "api/user";

export const fetchUsers = (page = 1, filters = {}) => ({
    type: types.FETCH_USERS,
    payload: api.fetchUsers(page, filters),
    meta: { globalError: true },
});

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_USER_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true },
});

export const deleteUser = (id) => ({
    type: types.DELETE_USER,
    payload: api.deleteUser(id),
    meta: { id, globalError: true },
});

export const createUser = (data) => ({
    type: types.CREATE_USER,
    payload: api.createUser(data),
    meta: { globalError: true },
});

export const updateUser = (id, data) => ({
    type: types.UPDATE_USER,
    payload: api.updateUser(id, data),
    meta: { globalError: true },
});

export const activateUser = (id) => ({
    type: types.ACTIVATE_USER,
    payload: api.activateUser(id),
    meta: { id, globalError: true },
});

export const setUserEdit = (data) => ({
    type: types.SET_USER_EDIT,
    data,
});

export const resetUserEdit = (data) => ({
    type: types.RESET_USER_EDIT,
});

export const setUserView = (view) => ({
    type: types.SET_USER_VIEW,
    view,
});

export const sendRecoverPasswordEmail = (id) => ({
    type: types.SEND_RECOVER_PASSWORD_EMAIL,
    payload: api.sendRecoverPasswordEmail(id),
    meta: { id, globalError: true },
});
