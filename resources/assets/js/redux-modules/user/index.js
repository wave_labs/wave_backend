import { types } from "./types";
import { views } from "./types";

const initialState = {
	data: [], //User list
	UserSelector: [], //User list with no pagination
	meta: {}, //meta information about Users and pagination
	loading: false,
	editUser: {
		//used to pass edit information to form
	},
	view: views.USER_VIEW,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_USER}_PENDING`:
		case `${types.FETCH_USERS}_PENDING`:
		case `${types.FETCH_USER_SELECTOR}_PENDING`:
		case `${types.ACTIVATE_USER}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_USERS}_REJECTED`:
		case `${types.FETCH_USER_SELECTOR}_REJECTED`:
		case `${types.DELETE_USER}_REJECTED`:
		case `${types.ACTIVATE_USER}_REJECTED`:
			return {
				...state,
				loading: false
			};



		case `${types.FETCH_USERS}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.FETCH_USER_SELECTOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				UserSelector: action.payload.data.data,
			};

		case `${types.DELETE_USER}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					user => user.id !== action.meta.id
				)
			};

		case `${types.CREATE_USER}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_USER}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(user, index) =>
						user.id === action.payload.data.data.id
							? action.payload.data.data
							: user
				)
			};

		case `${types.ACTIVATE_USER}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.map(
					user =>
						user.id === action.meta.id
							? { ...user, active: !user.active }
							: user
				)
			};

		case types.SET_USER_EDIT:
			return {
				...state,
				editUser: action.data
			};

		case types.RESET_USER_EDIT:
			return {
				...state,
				editUser: {}
			};

		case types.SET_USER_VIEW:
			return {
				...state,
				view: action.view
			};

		default:
			return state;
	}
};
