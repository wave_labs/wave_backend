import { types } from "./types";

const initialState = {
  data: [], //Litter list
  meta: {}, //meta information about litter and pagination
  loading: false,
  current: [],
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case `${types.FETCH_CLIMARESTS}_PENDING`:
    case `${types.FETCH_CLIMAREST_AREAS}_PENDING`:
    case `${types.FETCH_CLIMAREST_AREA_STAKEHOLDERS}_PENDING`:
    case `${types.FETCH_CLIMAREST}_PENDING`:
    case `${types.CREATE_CLIMAREST}_PENDING`:
    case `${types.UPDATE_CLIMAREST}_PENDING`:
    case `${types.DELETE_CLIMAREST}_PENDING`:
      return {
        ...state,
        loading: true,
      };

    case `${types.FETCH_CLIMARESTS}_REJECTED`:
    case `${types.FETCH_CLIMAREST_AREAS}_REJECTED`:
    case `${types.FETCH_CLIMAREST_AREA_STAKEHOLDERS}_REJECTED`:
    case `${types.FETCH_CLIMAREST}_REJECTED`:
    case `${types.CREATE_CLIMAREST}_REJECTED`:
    case `${types.UPDATE_CLIMAREST}_REJECTED`:
    case `${types.DELETE_CLIMAREST}_REJECTED`:
      return {
        ...state,
        loading: false,
      };

    case `${types.FETCH_CLIMARESTS}_FULFILLED`:
    case `${types.FETCH_CLIMAREST_AREAS}_FULFILLED`:
    case `${types.FETCH_CLIMAREST_AREA_STAKEHOLDERS}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: action.payload.data.data,
        meta: action.payload.data.meta,
      };

    case `${types.FETCH_CLIMAREST}_FULFILLED`:
      return {
        ...state,
        loading: false,
        current: {
          ...action.payload.data.data,
        },
      };

    case `${types.DELETE_CLIMAREST}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: state.data.filter((record) => record.id !== action.meta.id),
      };

    case `${types.CREATE_CLIMAREST}_FULFILLED`:
      return {
        ...state,
        data: [action.payload.data.data, ...state.data],
      };

    case `${types.UPDATE_CLIMAREST}_FULFILLED`:
      return {
        ...state,
        data: state.data.map((record) =>
          record.id === action.payload.data.data.id
            ? action.payload.data.data
            : record
        ),
      };

    case types.SET_CURRENT_CLIMAREST:
      return {
        ...state,
        loading: false,
        current: {
          ...action.data,
        },
      };

    case types.EXPORT_CLIMARESTS:
      return {
        ...state,
      };

    case types.RESET_CURRENT_CLIMAREST:
      return {
        ...state,
        current: {},
      };

    default:
      return state;
  }
};
