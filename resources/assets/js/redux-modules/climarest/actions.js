import { types } from "./types";
import api from "api/climarest";
import axios from "axios";
import { stringify } from "query-string";
import { download } from "helpers";

export const fetchClimarests = (page = 1, filters = {}) => ({
    type: types.FETCH_CLIMARESTS,
    payload: api.fetchClimarests(page, filters),
    meta: { globalError: true }
});

export const fetchClimarest = id => ({
    type: types.FETCH_CLIMAREST,
    payload: api.fetchClimarest(id),
    meta: { id, globalError: true }
});

export const deleteClimarest = id => ({
    type: types.DELETE_CLIMAREST,
    payload: api.deleteClimarest(id),
    meta: { id, globalError: true }
});

export const createClimarest = data => ({
    type: types.CREATE_CLIMAREST,
    payload: api.createClimarest(data),
    meta: { globalError: true }
});

export const updateClimarest = (id, data) => ({
    type: types.UPDATE_CLIMAREST,
    payload: api.updateClimarest(id, data),
    meta: { globalError: true }
});

export const fetchClimarestAreas = (page = 1, filters = {}) => ({
    type: types.FETCH_CLIMAREST_AREAS,
    payload: api.fetchClimarestAreas(page, filters),
    meta: { globalError: true }
});

export const fetchClimarestAreaStakeholders = id => ({
    type: types.FETCH_CLIMAREST_AREA_STAKEHOLDERS,
    payload: api.fetchClimarestAreaStakeholders(id),
    meta: { id, globalError: true }
});

export const exportClimarest = (filters = {}) => ({
    type: types.EXPORT_CLIMARESTS,
    payload: axios({
        url: `${window.location.origin}/api/export/csv/climarest?${stringify(filters, {
            arrayFormat: "index"
        })}`,
        method: "GET",
        responseType: "blob",
    }).then(
        response => {
            download(response, 'climarestStakeholders.xlsx')
        },
        error => {
            return error.data;
        }
    ),
    meta: { globalError: true }
});


