import { types } from "./types";

const initialState = {
  data: [], //Dive list
  monthlyData: [],
  mostReportedData: [],
  speciesReportsData: [],
  meta: {}, //meta information about Dive and pagination
  loading: false,
  editDive: {
    //used to pass edit information to form
    diving_spot: {},
    userable: { user: {} },
    creatures: [(creature) => {}],
  },
  loadingExport: false,
  coordinates: [],
  loadingCoordinates: false,
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case `${types.EXPORT_DIVE_CSV}_PENDING`:
      return {
        ...state,
        loadingExport: true,
      };

    case `${types.FETCH_DIVE_COORDS}_PENDING`:
      return {
        ...state,
        loadingCoordinates: true,
      };

    case `${types.FETCH_DIVE_COORDS}_REJECTED`:
      return {
        ...state,
        loadingCoordinates: false,
        coordinates: [],
      };

    case `${types.FETCH_DIVE_COORDS}_FULFILLED`:
      return {
        ...state,
        loadingCoordinates: false,
        coordinates: action.payload.data.data,
      };

    case `${types.EXPORT_DIVE_CSV}_REJECTED`:
    case `${types.EXPORT_DIVE_CSV}_FULFILLED`:
      return {
        ...state,
        loadingExport: false,
      };

    case `${types.DELETE_DIVE}_PENDING`:
    case `${types.FETCH_DIVE}_PENDING`:
    case `${types.FETCH_SPECIES_REPORTS}_PENDING`:
    case `${types.FETCH_MONTHLY_DIVE}_PENDING`:
    case `${types.FETCH_MOST_REPORTED_SPECIES}_PENDING`:
    case `${types.FETCH_DIVE_CENTER}_PENDING`:
      return {
        ...state,
        loading: true,
      };

    case `${types.FETCH_DIVE}_REJECTED`:
    case `${types.FETCH_MONTHLY_DIVE}_REJECTED`:
    case `${types.FETCH_MOST_REPORTED_SPECIES}_REJECTED`:
    case `${types.FETCH_SPECIES_REPORTS}_REJECTED`:
    case `${types.FETCH_DIVE_CENTER}_REJECTED`:
    case `${types.DELETE_DIVE}_REJECTED`:
      return {
        ...state,
        loading: false,
      };

    case `${types.FETCH_DIVE}_FULFILLED`:
    case `${types.FETCH_DIVE_CENTER}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: action.payload.data.data,
        meta: action.payload.data.meta,
      };

    case `${types.FETCH_MONTHLY_DIVE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        monthlyData: action.payload.data,
      };
    case `${types.FETCH_MOST_REPORTED_SPECIES}_FULFILLED`:
      return {
        ...state,
        loading: false,
        mostReportedData: action.payload.data,
      };
    case `${types.FETCH_SPECIES_REPORTS}_FULFILLED`:
      return {
        ...state,
        loading: false,
        speciesReportsData: action.payload.data,
      };
    case `${types.DELETE_DIVE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: state.data.filter((dive) => dive.id !== action.meta.id),
      };

    case `${types.CREATE_DIVE}_FULFILLED`:
      return {
        ...state,
        data: [action.payload.data.data, ...state.data],
      };

    case `${types.UPDATE_DIVE}_FULFILLED`:
      return {
        ...state,
        data: state.data.map((dive, index) =>
          dive.id === action.payload.data.data.id
            ? action.payload.data.data
            : dive
        ),
      };

    case types.SET_DIVE_EDIT:
      return {
        ...state,
        editDive: action.data,
      };

    case types.RESET_DIVE_EDIT:
      return {
        ...state,
        editDive: {
          diving_spot: {},
          userable: { user: {} },
          creatures: { creature: {} },
        },
      };

    default:
      return state;
  }
};
