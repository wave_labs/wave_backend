export const types = {
  FETCH_DIVE: "FETCH_DIVE",
  DELETE_DIVE: "DELETE_DIVE",
  CREATE_DIVE: "CREATE_DIVE",
  UPDATE_DIVE: "UPDATE_DIVE",
  FETCH_DIVE_COORDS: "FETCH_DIVE_COORDS",

  FETCH_DIVE_CENTER: "FETCH_DIVE_CENTER",

  SET_DIVE_EDIT: "SET_DIVE_EDIT",
  RESET_DIVE_EDIT: "RESET_DIVE_EDIT",

  EXPORT_DIVE_CSV: "EXPORT_DIVE_CSV",

  FETCH_MONTHLY_DIVE: "FETCH_MONTHLY_DIVE",
  FETCH_MOST_REPORTED_SPECIES: "FETCH_MOST_REPORTED_SPECIES",
  FETCH_SPECIES_REPORTS: "FETCH_SPECIES_REPORTS",
};
