import { types } from "./types";
import api from "api/dive";
import axios from "axios";
import { stringify } from "query-string";
import { download } from "helpers";

export const fetchDives = (page = 1, filters = {}) => ({
  type: types.FETCH_DIVE,
  payload: api.fetchDives(page, filters),
  meta: { globalError: true },
});

export const deleteDive = (id) => ({
  type: types.DELETE_DIVE,
  payload: api.deleteDive(id),
  meta: { id, globalError: true },
});

export const createDive = (data) => ({
  type: types.CREATE_DIVE,
  payload: api.createDive(data),
  meta: { globalError: true },
});

export const updateDive = (id, data) => ({
  type: types.UPDATE_DIVE,
  payload: api.updateDive(id, data),
  meta: { globalError: true },
});

export const setDiveEdit = (data) => ({
  type: types.SET_DIVE_EDIT,
  data,
});

export const resetDiveEdit = (data) => ({
  type: types.RESET_DIVE_EDIT,
});

export const fetchDiveCoords = (filters) => ({
  type: types.FETCH_DIVE_COORDS,
  payload: api.fetchDiveCoords(filters),
  meta: { globalError: true },
});

export const fetchMonthlyDives = () => ({
  type: types.FETCH_MONTHLY_DIVE,
  payload: api.fetchMonthlyDives(),
  meta: { globalError: true },
});

export const fetchMostReportedSpecies = () => ({
  type: types.FETCH_MOST_REPORTED_SPECIES,
  payload: api.fetchMostReportedSpecies(),
  meta: { globalError: true },
});

export const fetchSpeciesReports = () => ({
  type: types.FETCH_SPECIES_REPORTS,
  payload: api.fetchSpeciesReports(),
  meta: { globalError: true },
});


export const exportDiveCsv = (filters = {}) => ({
  type: types.EXPORT_DIVE_CSV,
  payload: axios({
    url: `${window.location.origin}/api/export/csv/dive?${stringify(filters, {
      arrayFormat: "index",
    })}`,
    method: "GET",
    responseType: "blob",
  }).then(
    (response) => {
      download(response, "dive-reporter.xlsx");
    },
    (error) => {
      return error.data;
    }
  ),
  meta: { globalError: true },
});
