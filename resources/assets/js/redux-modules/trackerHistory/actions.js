import { types } from "./types";
import api from "api/trackerHistory";

export const specieHistory = () => ({
    type: types.SPECIE_HISTORY,
    payload: api.specieHistory(),
    meta: { globalError: true }
});

export const timeHistory = () => ({
    type: types.TIME_HISTORY,
    payload: api.timeHistory(),
    meta: { globalError: true }
});