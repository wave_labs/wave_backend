import { types } from "./types";

const initialState = {
	dataTime: [],
	dataSpecie: [],
	loadingTime: false,
	loadingSpecie: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.TIME_HISTORY}_FULFILLED`:
			return {
				...state,
				loadingTime: false,
				dataTime: action.payload.data
			};
		case `${types.TIME_HISTORY}_PENDING`:
			return {
				...state,
				loadingTime: true
			};
		case `${types.TIME_HISTORY}_REJECTED`:
			return {
				...state,
				loadingTime: false
			};


		case `${types.SPECIE_HISTORY}_FULFILLED`:
			return {
				...state,
				loadingSpecie: false,
				dataSpecie: action.payload.data
			};
		case `${types.SPECIE_HISTORY}_PENDING`:
			return {
				...state,
				loadingSpecie: true
			};
		case `${types.SPECIE_HISTORY}_REJECTED`:
			return {
				...state,
				loadingSpecie: false
			};
		default:
			return state;
	}
};