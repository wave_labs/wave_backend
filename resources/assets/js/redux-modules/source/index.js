import { types } from "./types";

const initialState = {
    data: [],
    SourceSelector: [],
    meta: {},
    loading: false,
    editSource: {
        //used to pass edit information to form
    }
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_SOURCE_SELECTOR}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_SOURCE_SELECTOR}_REJECTED`:
            return {
                ...state,
                loading: false
            };

        case `${types.FETCH_SOURCE_SELECTOR}_FULFILLED`:
            return {
                ...state,
                loading: false,
                SourceSelector: action.payload.data.data,
            };

        default:
            return state;
    }
};
