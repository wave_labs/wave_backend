import { types } from "./types";
import api from "api/source";

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_SOURCE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});