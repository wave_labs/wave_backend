import { types } from "./types";

const initialState = {
	data: [],
	LitterSubCategorySelector: [],
	meta: {},
	loading: false,
	editLitterSubCategory: { //used to pass edit information to form
		category: {}
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_LITTER_SUB_CATEGORY}_PENDING`:
		case `${types.FETCH_LITTER_SUB_CATEGORIES}_PENDING`:
		case `${types.FETCH_LITTER_SUB_CATEGORY_SELECTOR}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_LITTER_SUB_CATEGORIES}_REJECTED`:
		case `${types.DELETE_LITTER_SUB_CATEGORY}_REJECTED`:
		case `${types.FETCH_LITTER_SUB_CATEGORY_SELECTOR}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_LITTER_SUB_CATEGORIES}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.FETCH_LITTER_SUB_CATEGORY_SELECTOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				LitterSubCategorySelector: action.payload.data.data,
			};

		case `${types.DELETE_LITTER_SUB_CATEGORY}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					litterSubCategory => litterSubCategory.id !== action.meta.id
				)
			};

		case `${types.CREATE_LITTER_SUB_CATEGORY}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_LITTER_SUB_CATEGORY}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(litterSubCategory, index) =>
						litterSubCategory.id === action.payload.data.data.id
							? action.payload.data.data
							: litterSubCategory
				)
			};

		case types.SET_LITTER_SUB_CATEGORY_EDIT:
			return {
				...state,
				editLitterSubCategory: action.data
			};

		case types.RESET_LITTER_SUB_CATEGORY_EDIT:
			return {
				...state,
				editLitterSubCategory: {
					user: {},
					category: {}
				}
			};

		default:
			return state;
	}
};
