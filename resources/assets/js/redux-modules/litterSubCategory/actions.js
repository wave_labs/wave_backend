import { types } from "./types";
import api from "api/litterSubCategory";

export const fetchLitterSubCategories = (page = 1, filters = {}) => ({
    type: types.FETCH_LITTER_SUB_CATEGORIES,
    payload: api.fetchLitterSubCategories(page, filters),
    meta: { globalError: true }
});

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_LITTER_SUB_CATEGORY_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});

export const deleteLitterSubCategory = id => ({
    type: types.DELETE_LITTER_SUB_CATEGORY,
    payload: api.deleteLitterSubCategory(id),
    meta: { id, globalError: true }
});

export const createLitterSubCategory = data => ({
    type: types.CREATE_LITTER_SUB_CATEGORY,
    payload: api.createLitterSubCategory(data),
    meta: { globalError: true }
});

export const updateLitterSubCategory = (id, data) => ({
    type: types.UPDATE_LITTER_SUB_CATEGORY,
    payload: api.updateLitterSubCategory(id, data),
    meta: { globalError: true }
});

export const setLitterSubCategoryEdit = data => ({
    type: types.SET_LITTER_SUB_CATEGORY_EDIT,
    data
});

export const resetLitterSubCategoryEdit = data => ({
    type: types.RESET_LITTER_SUB_CATEGORY_EDIT
});
