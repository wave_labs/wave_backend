import { types } from "./types";

const initialState = {
	data: [], //Iot device type list
	IotDeviceTypeSelector: [], //User list
	meta: {}, //meta information about Iot device types and pagination
	loading: false,
	editIotDeviceType: { //used to pass edit information to form
		type: {},
		defaultFields : []
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_IOT_DEVICE_TYPE}_PENDING`:
		case `${types.FETCH_IOT_DEVICE_TYPES}_PENDING`:
		case `${types.FETCH_IOT_DEVICE_TYPE_SELECTOR}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_IOT_DEVICE_TYPES}_REJECTED`:
		case `${types.DELETE_IOT_DEVICE_TYPE}_REJECTED`:
		case `${types.FETCH_IOT_DEVICE_TYPE_SELECTOR}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_IOT_DEVICE_TYPES}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.FETCH_IOT_DEVICE_TYPE_SELECTOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				IotDeviceTypeSelector: action.payload.data.data,
			};

		case `${types.DELETE_IOT_DEVICE_TYPE}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					iotDeviceType => iotDeviceType.id !== action.meta.id
				)
			};

		case `${types.CREATE_IOT_DEVICE_TYPE}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_IOT_DEVICE_TYPE}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(iotDeviceType, index) =>
						iotDeviceType.id === action.payload.data.data.id
							? action.payload.data.data
							: iotDeviceType
				)
			};

		case types.SET_IOT_DEVICE_TYPE_EDIT:
			return {
				...state,
				editIotDeviceType: action.data
			};

		case types.RESET_IOT_DEVICE_TYPE_EDIT:
			return {
				...state,
				editIotDeviceType: {
					type: {},
					defaultFields : []
				}
			};

		default:
			return state;
	}
};
