import { types } from "./types";
import api from "api/iotDeviceType";

export const fetchIotDeviceTypes = (page = 1, filters = {}) => ({
    type: types.FETCH_IOT_DEVICE_TYPES,
    payload: api.fetchIotDeviceTypes(page, filters),
    meta: { globalError: true }
});

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_IOT_DEVICE_TYPE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});

export const deleteIotDeviceType = id => ({
    type: types.DELETE_IOT_DEVICE_TYPE,
    payload: api.deleteIotDeviceType(id),
    meta: { id, globalError: true }
});

export const createIotDeviceType = data => ({
    type: types.CREATE_IOT_DEVICE_TYPE,
    payload: api.createIotDeviceType(data),
    meta: { globalError: true }
});

export const updateIotDeviceType = (id, data) => ({
    type: types.UPDATE_IOT_DEVICE_TYPE,
    payload: api.updateIotDeviceType(id, data),
    meta: { globalError: true }
});

export const setIotDeviceTypeEdit = data => ({
    type: types.SET_IOT_DEVICE_TYPE_EDIT,
    data
});

export const resetIotDeviceTypeEdit = data => ({
    type: types.RESET_IOT_DEVICE_TYPE_EDIT
});
