export const types = {
    FETCH_IOT_DEVICE_TYPES: 'FETCH_IOT_DEVICE_TYPES',
    FETCH_IOT_DEVICE_TYPE_SELECTOR: 'FETCH_IOT_DEVICE_TYPE_SELECTOR',
    DELETE_IOT_DEVICE_TYPE: 'DELETE_IOT_DEVICE_TYPE',
    CREATE_IOT_DEVICE_TYPE: 'CREATE_IOT_DEVICE_TYPE',
    UPDATE_IOT_DEVICE_TYPE: 'UPDATE_IOT_DEVICE_TYPE',

    SET_IOT_DEVICE_TYPE_EDIT: 'SET_IOT_DEVICE_TYPE_EDIT',
    RESET_IOT_DEVICE_TYPE_EDIT: 'RESET_IOT_DEVICE_TYPE_EDIT',
};
