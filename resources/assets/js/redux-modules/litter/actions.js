import { types } from "./types";
import api from "api/litter";
import axios from "axios";
import { stringify } from "query-string";
import { download } from "helpers";

export const fetchLitter = (page = 1, filters = {}) => ({
    type: types.FETCH_LITTER,
    payload: api.fetchLitter(page, filters),
    meta: { globalError: true }
});

export const fetchShowLitter = id => ({
    type: types.FETCH_SHOW_LITTER,
    payload: api.fetchShowLitter(id),
    meta: { globalError: true }
});

export const deleteLitter = id => ({
    type: types.DELETE_LITTER,
    payload: api.deleteLitter(id),
    meta: { id, globalError: true }
});

export const fetchLitterNextAndPrior = id => ({
    type: types.FETCH_LITTER_NEXT_PRIOR,
    payload: api.fetchLitterNextAndPrior(id),
    meta: { globalError: true }
});


export const createLitter = data => ({
    type: types.CREATE_LITTER,
    payload: api.createLitter(data),
    meta: { globalError: true }
});

export const updateLitter = (id, data) => ({
    type: types.UPDATE_LITTER,
    payload: api.updateLitter(id, data),
    meta: { globalError: true }
});

export const setLitterEdit = data => ({
    type: types.SET_LITTER_EDIT,
    data
});

export const resetLitterEdit = data => ({
    type: types.RESET_LITTER_EDIT
});

export const fetchLittersCoord = filters => ({
    type: types.FETCH_LITTERS_COORD,
    payload: api.fetchLittersCoord(filters),
    meta: { globalError: true }
});

export const setLitterView = view => ({
    type: types.SET_LITTER_VIEW,
    view
});

export const setCurrentLitter = data => ({
    type: types.SET_CURRENT_LITTER,
    data
});

export const resetCurrentLitter = () => ({
    type: types.RESET_CURRENT_LITTER
});

export const exportLitter = (filters = {}) => ({
    type: types.EXPORT_LITTER,
    payload: axios({
        url: `${window.location.origin}/api/export/csv/litter?${stringify(filters, {
            arrayFormat: "index"
        })}`,
        method: "GET",
        responseType: "blob",
    }).then(
        response => {
            download(response, 'litter.xlsx')
        },
        error => {
            return error.data;
        }
    ),
    meta: { globalError: true }
});

export const incrementCounter = (filters) => ({
    type: types.INCREMENT_COUNTER,
    payload: api.incrementCounter(filters),
    meta: { globalError: true }
});
