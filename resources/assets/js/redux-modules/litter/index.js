import { types } from "./types";
import { views } from "./types";

const initialState = {
	data: [], //Litter list
	counter: 0,
	meta: {}, //meta information about litter and pagination
	loading: false,
	errors: false,
	loadingExport: false,
	current: {
		//currently open litter
		userable: { user: {} },
		litter_sub_category: {},
		litter_category: {},
		photo: []
	},
	next: {},
	prior: {},
	editLitter: {
		//used to pass edit information to form
		userable: { user: {} },
		litter_sub_category: {},
		litter_category: {},
		photo: []
	},
	littersByCoord: [],
	view: views.TABLE_VIEW
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_LITTER}_PENDING`:
		case `${types.FETCH_LITTERS_COORD}_PENDING`:
		case `${types.FETCH_LITTER}_PENDING`:
		case `${types.FETCH_LITTER_NEXT_PRIOR}_PENDING`:
		case `${types.FETCH_SHOW_LITTER}_PENDING`:
		case `${types.INCREMENT_COUNTER}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_LITTER}_REJECTED`:
		case `${types.FETCH_SHOW_LITTER}_REJECTED`:
		case `${types.FETCH_LITTERS_COORD}_REJECTED`:
		case `${types.FETCH_LITTER_NEXT_PRIOR}_REJECTED`:
		case `${types.DELETE_LITTER}_REJECTED`:
		case `${types.INCREMENT_COUNTER}_REJECTED`:
			return {
				...state,
				loading: false,
			};

		case `${types.INCREMENT_COUNTER}_FULFILLED`:
			return {
				...state,
				loading: false,
				counter: action.payload.data.counter,
			};

		case `${types.FETCH_LITTER}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.EXPORT_LITTER}_PENDING`:
			return {
				...state,
				loadingExport: true
			};

		case `${types.EXPORT_LITTER}_REJECTED`:
		case `${types.EXPORT_LITTER}_FULFILLED`:
			return {
				...state,
				loadingExport: false
			};

		case `${types.FETCH_SHOW_LITTER}_FULFILLED`:
			return {
				...state,
				loading: false,
				current: {
					...action.payload.data.data
				}
			};

		case `${types.FETCH_LITTER_NEXT_PRIOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				prior: {
					...action.payload.data.previous
				},
				next: {
					...action.payload.data.next
				},
			};

		case `${types.DELETE_LITTER}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(litter => litter.id !== action.meta.id)
			};

		case `${types.CREATE_LITTER}_REJECTED`: {
			return {
				...state,
				loading: false,
				errors: true
			}
		}

		case `${types.CREATE_LITTER}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_LITTER}_FULFILLED`:
			return {
				...state,
				data: state.data.map((litter, index) =>
					litter.id === action.payload.data.data.id
						? action.payload.data.data
						: litter
				)
			};

		case types.SET_CURRENT_LITTER:
			return {
				...state,
				loading: false,
				current: {
					...action.data
				}
			};

		case types.RESET_CURRENT_LITTER:
			return {
				...state,
				current: {
					userable: { user: {} },
					litter_sub_category: {},
					litter_category: {},
					photo: []
				}
			};

		case types.SET_LITTER_EDIT:
			return {
				...state,
				editLitter: action.data
			};

		case types.RESET_LITTER_EDIT:
			return {
				...state,
				editLitter: {
					userable: { user: {} },
					litter_sub_category: {},
					litter_category: {},
					photo: []
				}
			};

		case `${types.FETCH_LITTERS_COORD}_FULFILLED`:
			return {
				...state,
				littersByCoord: action.payload.data.data
			};

		case types.SET_LITTER_VIEW:
			return {
				...state,
				view: action.view
			};

		default:
			return state;
	}
};
