import { types } from "./types";
import api from "api/vehicle";

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_VEHICLE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});