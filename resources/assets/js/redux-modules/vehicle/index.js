import { types } from "./types";

const initialState = {
    data: [],
    VehicleSelector: [],
    meta: {},
    loading: false,
    editVehicle: {
        //used to pass edit information to form
    }
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_VEHICLE_SELECTOR}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_VEHICLE_SELECTOR}_REJECTED`:
            return {
                ...state,
                loading: false
            };

        case `${types.FETCH_VEHICLE_SELECTOR}_FULFILLED`:
            return {
                ...state,
                loading: false,
                VehicleSelector: action.payload.data.data,
            };

        default:
            return state;
    }
};
