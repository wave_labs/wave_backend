import { types } from "./types";

const initialState = {
    data: [],
    LitterCategorySelector: [],
    meta: {},
    loading: false,
    editLitterCategory: {
    }
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.DELETE_LITTER_CATEGORY}_PENDING`:
        case `${types.FETCH_LITTER_CATEGORIES}_PENDING`:
        case `${types.FETCH_LITTER_CATEGORY_SELECTOR}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_LITTER_CATEGORIES}_REJECTED`:
        case `${types.DELETE_LITTER_CATEGORY}_REJECTED`:
        case `${types.FETCH_LITTER_CATEGORY_SELECTOR}_REJECTED`:
            return {
                ...state,
                loading: false
            };

        case `${types.FETCH_LITTER_CATEGORIES}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: action.payload.data.data,
                meta: action.payload.data.meta
            };

        case `${types.FETCH_LITTER_CATEGORY_SELECTOR}_FULFILLED`:
            return {
                ...state,
                loading: false,
                LitterCategorySelector: action.payload.data.data,
            };

        case `${types.DELETE_LITTER_CATEGORY}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: state.data.filter(
                    litterCategory => litterCategory.id !== action.meta.id
                )
            };

        case `${types.CREATE_LITTER_CATEGORY}_FULFILLED`:
            return {
                ...state,
                data: [action.payload.data.data, ...state.data]
            };

        case `${types.UPDATE_LITTER_CATEGORY}_FULFILLED`:
            return {
                ...state,
                data: state.data.map(
                    (litterCategory, index) =>
                        litterCategory.id === action.payload.data.data.id
                            ? action.payload.data.data
                            : litterCategory
                )
            };

        case types.SET_LITTER_CATEGORY_EDIT:
            return {
                ...state,
                editLitterCategory: action.data
            };

        case types.RESET_LITTER_CATEGORY_EDIT:
            return {
                ...state,
                editLitterCategory: {
                    user: {}
                }
            };

        default:
            return state;
    }
};
