import { types } from "./types";
import api from "api/litterCategory";

export const fetchLitterCategories = (page = 1, filters = {}) => ({
    type: types.FETCH_LITTER_CATEGORIES,
    payload: api.fetchLitterCategories(page, filters),
    meta: { globalError: true }
});

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_LITTER_CATEGORY_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});

export const deleteLitterCategory = id => ({
    type: types.DELETE_LITTER_CATEGORY,
    payload: api.deleteLitterCategory(id),
    meta: { id, globalError: true }
});

export const createLitterCategory = data => ({
    type: types.CREATE_LITTER_CATEGORY,
    payload: api.createLitterCategory(data),
    meta: { globalError: true }
});

export const updateLitterCategory = (id, data) => ({
    type: types.UPDATE_LITTER_CATEGORY,
    payload: api.updateLitterCategory(id, data),
    meta: { globalError: true }
});

export const setLitterCategoryEdit = data => ({
    type: types.SET_LITTER_CATEGORY_EDIT,
    data
});

export const resetLitterCategoryEdit = data => ({
    type: types.RESET_LITTER_CATEGORY_EDIT
});
