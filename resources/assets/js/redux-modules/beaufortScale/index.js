import { types } from "./types";

const initialState = {
    data: [], //Beaufort Scale list
    BeaufortScaleSelector: [],
    meta: {}, //meta information about Beaufort Scale and pagination
    loading: false,
    editBeaufortScale: {
        //used to pass edit information to form
    }
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_BEAUFORT_SCALE_SELECTOR}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_BEAUFORT_SCALE_SELECTOR}_REJECTED`:
            return {
                ...state,
                loading: false
            };

        case `${types.FETCH_BEAUFORT_SCALE_SELECTOR}_FULFILLED`:
            return {
                ...state,
                loading: false,
                BeaufortScaleSelector: action.payload.data.data,
            };

        default:
            return state;
    }
};
