import { types } from "./types";
import api from "api/beaufortScale";

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_BEAUFORT_SCALE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});