import { types } from "./types";
import api from "api/substract";

export const fetchSelector = () => ({
    type: types.FETCH_SUBSTRACT_SELECTOR,
    payload: api.fetchSelector(),
    meta: { globalError: true }
});