import { types } from "./types";

const initialState = {
  data: [], //DivingSpot list
  selector: [],
  meta: {}, //meta information about DivingSpots and pagination
  loading: false,
  editDivingSpot: {
    //used to pass edit information to form
  },
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case `${types.FETCH_SUBSTRACT_SELECTOR}_PENDING`:
      return {
        ...state,
        loading: true,
      };

    case `${types.FETCH_SUBSTRACT_SELECTOR}_REJECTED`:
      return {
        ...state,
        loading: false,
      };
    case `${types.FETCH_SUBSTRACT_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        selector: action.payload.data.data,
      };

    default:
      return state;
  }
};
