import { types } from "./types";

const initialState = {
    data: [],
    loading: false,
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_GPS_TAG_SATELLITES}_PENDING`:
        case `${types.FETCH_GPS_GATEWAY_SATELLITES}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_GPS_GATEWAY_SATELLITES}_REJECTED`:
        case `${types.FETCH_GPS_TAG_SATELLITES}_REJECTED`:
            return {
                ...state,
                loading: false
            };

        case `${types.FETCH_GPS_TAG_SATELLITES}_FULFILLED`:
        case `${types.FETCH_GPS_GATEWAY_SATELLITES}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: action.payload.data,
            };
        case `${types.RESET_GPS_DATA}`:
            return {
                ...state,
                data: [],
            };

        default:
            return state;
    }
};
