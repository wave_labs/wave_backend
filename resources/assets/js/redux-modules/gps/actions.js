import { types } from "./types";
import api from "api/gps";

export const fetchGpsGatewaySatellites = (filters = {}) => ({
    type: types.FETCH_GPS_GATEWAY_SATELLITES,
    payload: api.fetchGpsGatewaySatellites(filters),
    meta: { globalError: true }
});

export const fetchGpsTagSatellites = (filters = {}) => ({
    type: types.FETCH_GPS_TAG_SATELLITES,
    payload: api.fetchGpsTagSatellites(filters),
    meta: { globalError: true }
});


export const resetGpsData = () => ({
    type: types.RESET_GPS_DATA
});





