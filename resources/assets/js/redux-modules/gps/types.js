export const types = {
    FETCH_GPS_GATEWAY_SATELLITES: 'FETCH_GPS_GATEWAY_SATELLITES',
    FETCH_GPS_TAG_SATELLITES: 'FETCH_GPS_TAG_SATELLITES',
    RESET_GPS_DATA: 'RESET_GPS_DATA'

};
