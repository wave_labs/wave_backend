import { types } from "./types";
import api from "api/creature";

export const fetchCreatures = (page = 1, filters = {}) => ({
    type: types.FETCH_CREATURES,
    payload: api.fetchCreatures(page, filters),
    meta: { globalError: true }
});

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_CREATURE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});

export const fetchCreatureSize = id => ({
    type: types.FETCH_CREATURE_SIZE,
    payload: api.fetchCreatureSize(id),
    meta: { id, globalError: true }
});

export const fetchCreatureActivities = (id, filters = {}) => ({
    type: types.FETCH_CREATURE_ACTIVITIES,
    payload: api.fetchCreatureActivities(id, filters),
    meta: { id, globalError: true }
});

export const deleteCreature = id => ({
    type: types.DELETE_CREATURE,
    payload: api.deleteCreature(id),
    meta: { id, globalError: true }
});

export const createCreature = data => ({
    type: types.CREATE_CREATURE,
    payload: api.createCreature(data),
    meta: { globalError: true }
});

export const updateCreature = (id, data) => ({
    type: types.UPDATE_CREATURE,
    payload: api.updateCreature(id, data),
    meta: { globalError: true }
});

export const setCreatureEdit = data => ({
    type: types.SET_CREATURE_EDIT,
    data
});

export const resetCreatureEdit = data => ({
    type: types.RESET_CREATURE_EDIT
});
