import { types } from "./types";

const initialState = {
	data: [], //Creature list
	CreatureSelector: [], //User list
	meta: {}, //meta information about Creatures and pagination
	loading: false,
	editCreature: { //used to pass edit information to form
		type: {}
	},
	dimensions: {}, //dimensions used in markers study
	activities: [] //dimensions used in markers study
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_CREATURE}_PENDING`:
		case `${types.FETCH_CREATURES}_PENDING`:
		case `${types.FETCH_CREATURE_SELECTOR}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_CREATURES}_REJECTED`:
		case `${types.DELETE_CREATURE}_REJECTED`:
		case `${types.FETCH_CREATURE_SELECTOR}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_CREATURES}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.FETCH_CREATURE_SELECTOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				CreatureSelector: action.payload.data.data,
			};

		case `${types.FETCH_CREATURE_SIZE}_FULFILLED`:
			return {
				...state,
				dimensions: action.payload.data[0],
			};

		case `${types.FETCH_CREATURE_ACTIVITIES}_FULFILLED`:
			return {
				...state,
				activities: action.payload.data.data,
			};

		case `${types.DELETE_CREATURE}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					creature => creature.id !== action.meta.id
				)
			};

		case `${types.CREATE_CREATURE}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_CREATURE}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(creature, index) =>
						creature.id === action.payload.data.data.id
							? action.payload.data.data
							: creature
				)
			};

		case types.SET_CREATURE_EDIT:
			return {
				...state,
				editCreature: action.data
			};

		case types.RESET_CREATURE_EDIT:
			return {
				...state,
				editCreature: {
					type: {}
				}
			};

		default:
			return state;
	}
};
