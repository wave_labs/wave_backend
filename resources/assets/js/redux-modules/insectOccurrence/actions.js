import { types } from "./types";
import api from "api/insect/occurrences";

export const fetchInsectSpeciesSelector = (filters) => ({
  type: types.FETCH_INSECT_SPECIES_SELECTOR,
  payload: api.fetchInsectSpeciesSelector(filters),
  meta: { globalError: true },
});

export const fetchInsectSubspeciesSelector = (filters) => ({
  type: types.FETCH_INSECT_SUBSPECIES_SELECTOR,
  payload: api.fetchInsectSubspeciesSelector(filters),
  meta: { globalError: true },
});

export const createInsectOccurrence = (data) => ({
  type: types.CREATE_INSECT_OCCURRENCE,
  payload: api.createInsectOccurrence(data),
  meta: { globalError: true },
});

export const setInsectOccurrenceCurrent = (record = {}) => ({
  type: types.SET_INSECT_OCCURRENCE_CURRENT,
  payload: record
});

export const fetchInsectOccurrences = (page, filters) => ({
  type: types.FETCH_INSECT_OCCURRENCES,
  payload: api.fetchInsectOccurrences(page, filters),
  meta: { globalError: true }
});

export const updateInsectOccurrence = (id, data) => ({
  type: types.UPDATE_INSECT_OCCURRENCE,
  payload: api.updateInsectOccurrence(id, data),
  meta: { globalError: true }
});

export const insectImportOccurrences = (data) => ({
  type: types.INSECT_IMPORT_OCCURRENCES,
  payload: api.insectImportOccurrences(data),
  meta: { globalError: true },
});