import { types } from "./types";

const initialState = {
  data: [], //
  meta: {}, //meta information about litter and pagination
  loading: false,
  current: {},
  selector: [],
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case `${types.FETCH_INSECT_SPECIES_SELECTOR}_PENDING`:
    case `${types.FETCH_INSECT_SUBSPECIES_SELECTOR}_PENDING`:
    case `${types.CREATE_INSECT_OCCURRENCE}_PENDING`:
    case `${types.FETCH_INSECT_OCCURRENCES}_PENDING`:
    case `${types.UPDATE_INSECT_OCCURRENCE}_PENDING`:
    case `${types.INSECT_IMPORT_OCCURRENCES}_PENDING`:
      return {
        ...state,
        loading: true,
      };
    case `${types.FETCH_INSECT_SPECIES_SELECTOR}_REJECTED`:
    case `${types.FETCH_INSECT_SUBSPECIES_SELECTOR}_REJECTED`:
    case `${types.CREATE_INSECT_OCCURRENCE}_REJECTED`:
    case `${types.FETCH_INSECT_OCCURRENCES}_REJECTED`:
    case `${types.UPDATE_INSECT_OCCURRENCE}_REJECTED`:
    case `${types.INSECT_IMPORT_OCCURRENCES}_REJECTED`:
      return {
        ...state,
        loading: false,
      };

    case `${types.FETCH_INSECT_SPECIES_SELECTOR}_FULFILLED`:
    case `${types.FETCH_INSECT_SUBSPECIES_SELECTOR}_FULFILLED`:
      return {
        ...state,
        loading: false,
        selector: action.payload.data.data,
      };
    case `${types.CREATE_INSECT_OCCURRENCE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: [action.payload.data.data, ...state.data],
      };

    case `${types.FETCH_INSECT_OCCURRENCES}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: action.payload.data.data,
        meta: action.payload.data.meta,
      };

    case `${types.UPDATE_INSECT_OCCURRENCE}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: state.data.map((record, index) =>
          record.id === action.payload.data.data.id
            ? action.payload.data.data
            : record
        ),
      };

    case `${types.SET_INSECT_OCCURRENCE_CURRENT}`:
      return {
        ...state,
        current: action.payload,
      };

    case `${types.INSECT_IMPORT_OCCURRENCES}_FULFILLED`:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
};
