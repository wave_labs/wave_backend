import { types } from "./types";

const initialState = {
	data: [], //Iot device type list
	IotDeviceStateSelector: [], //User list
	meta: {}, //meta information about Iot device types and pagination
	loading: false,
	editIotDeviceState: { //used to pass edit information to form
		type: {},
		defaultFields : []
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_IOT_DEVICE_STATE_SELECTOR}_PENDING`:
			return {
				...state,
				loading: true
			};
		case `${types.FETCH_IOT_DEVICE_STATE_SELECTOR}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_IOT_DEVICE_STATE_SELECTOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				IotDeviceStateSelector: action.payload.data.data,
			};

		default:
			return state;
	}
};
