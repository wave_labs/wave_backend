import { types } from "./types";
import api from "api/iotDeviceState";


export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_IOT_DEVICE_STATE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});

