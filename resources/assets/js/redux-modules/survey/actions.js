import { types } from "./types";
import api from "api/survey";

export const fetchSurveys = (page = 1, filters = {}) => ({
    type: types.FETCH_SURVEYS,
    payload: api.fetchSurveys(page, filters),
    meta: { globalError: true }
});

export const deleteSurvey = id => ({
    type: types.DELETE_SURVEY,
    payload: api.deleteSurvey(id),
    meta: { id, globalError: true }
});

export const createSurvey = data => ({
    type: types.CREATE_SURVEY,
    payload: api.createSurvey(data),
    meta: { globalError: true }
});

export const updateSurvey = (id, data) => ({
    type: types.UPDATE_SURVEY,
    payload: api.updateSurvey(id, data),
    meta: { globalError: true }
});

export const setSurveyEdit = data => ({
    type: types.SET_SURVEY_EDIT,
    data
});

export const resetSurveyEdit = data => ({
    type: types.RESET_SURVEY_EDIT
});
