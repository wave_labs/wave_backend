import { types } from "./types";

const initialState = {
	data: [], //Survey list
	meta: {}, //meta information about Surveys and pagination
	loading: false,
	editSurvey: {
		//used to pass edit information to form
		creature: {},
		userable: { user: {} },
		dive: { diving_spot: {} },
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_SURVEY}_PENDING`:
		case `${types.FETCH_SURVEYS}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_SURVEYS}_REJECTED`:
		case `${types.DELETE_SURVEY}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_SURVEYS}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_SURVEY}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(survey => survey.id !== action.meta.id)
			};

		case `${types.CREATE_SURVEY}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_SURVEY}_FULFILLED`:
			return {
				...state,
				data: state.data.map((survey, index) =>
					survey.id === action.payload.data.data.id
						? action.payload.data.data
						: survey
				)
			};

		case types.SET_SURVEY_EDIT:
			return {
				...state,
				editSurvey: action.data
			};

		case types.RESET_SURVEY_EDIT:
			return {
				...state,
				editSurvey: {
					creature: {},
					userable: { user: {} },
					dive: { diving_spot: {} },
				}
			};

		default:
			return state;
	}
};
