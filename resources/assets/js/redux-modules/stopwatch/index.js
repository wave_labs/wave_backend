import { types } from "./types";

const initialState = {
	results: [],
	currentResult: {},
	videos: [],
	classes: [],
	loading: false,
	meta: {}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_STOPWATCH_VIDEOS}_PENDING`:
		case `${types.FETCH_STOPWATCH_CLASSES}_PENDING`:
		case `${types.CREATE_STOPWATCH_RESULT}_PENDING`:
		case `${types.FETCH_STOPWATCH_RESULTS}_PENDING`:
		case `${types.FETCH_STOPWATCH_RESULT}_PENDING`:
		case `${types.DELETE_STOPWATCH_RESULT}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_STOPWATCH_VIDEOS}_REJECTED`:
		case `${types.FETCH_STOPWATCH_CLASSES}_REJECTED`:
		case `${types.CREATE_STOPWATCH_RESULT}_REJECTED`:
		case `${types.FETCH_STOPWATCH_RESULTS}_REJECTED`:
		case `${types.DELETE_STOPWATCH_RESULT}_REJECTED`:
		case `${types.FETCH_STOPWATCH_RESULT}_REJECTED`:
			return {
				...state,
				loading: false
			};
		case `${types.FETCH_STOPWATCH_RESULT}_FULFILLED`:
			return {
				...state,
				currentResult: action.payload.data.data
			};

		case `${types.FETCH_STOPWATCH_RESULTS}_FULFILLED`:
			return {
				...state,
				loading: false,
				results: action.payload.data.data,
				meta: action.payload.data.meta,
			};
		case types.SET_CURRENT_STOPWATCH_RESULT:
			return {
				...state,
				currentResult: {
					...action.record
				}
			};

		case `${types.FETCH_STOPWATCH_VIDEOS}_FULFILLED`:
			return {
				...state,
				loading: false,
				videos: action.payload.data.data,
			};

		case `${types.FETCH_STOPWATCH_CLASSES}_FULFILLED`:
			return {
				...state,
				loading: false,
				classes: action.payload.data.data,
			};

		case `${types.DELETE_STOPWATCH_RESULT}_FULFILLED`:
			return {
				...state,
				loading: false,
				results: state.results.filter(
					element => element.id !== action.meta.id
				)
			};

		default:
			return state;
	}
};
