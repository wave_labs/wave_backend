import { types } from "./types";
import api from "api/stopwatch";

export const fetchStopwatchVideos = (filters = {}) => ({
    type: types.FETCH_STOPWATCH_VIDEOS,
    payload: api.fetchStopwatchVideos(filters),
    meta: { globalError: true }
});

export const fetchNextStopwatchVideo = (video, filters) => ({
    type: types.FETCH_NEXT_STOPWATCH_VIDEO,
    payload: api.fetchNextStopwatchVideo(video, filters),
    meta: { globalError: true }
});

export const fetchStopwatchClasses = (filters = {}) => ({
    type: types.FETCH_STOPWATCH_CLASSES,
    payload: api.fetchStopwatchClasses(filters),
    meta: { globalError: true }
});

export const createStopwatchResult = (data) => ({
    type: types.CREATE_STOPWATCH_RESULT,
    payload: api.createStopwatchResult(data),
    meta: { globalError: true }
});

export const fetchStopwatchResults = (page = 1, filters = {}) => ({
    type: types.FETCH_STOPWATCH_RESULTS,
    payload: api.fetchStopwatchResults(page, filters),
    meta: { globalError: true }
});

export const fetchStopwatchResult = (id) => ({
    type: types.FETCH_STOPWATCH_RESULT,
    payload: api.fetchStopwatchResult(id),
    meta: { globalError: true }
});


export const setCurrentStopwatchResult = (record) => ({
    type: types.SET_CURRENT_STOPWATCH_RESULT,
    record
});


export const deleteStopwatchResult = (id) => ({
    type: types.DELETE_STOPWATCH_RESULT,
    payload: api.deleteStopwatchResult(id),
    meta: { id, globalError: true }
});
