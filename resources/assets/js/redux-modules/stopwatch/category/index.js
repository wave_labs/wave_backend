import { types } from "./types";

const initialState = {
	current: {},
	data: [],
	loading: false,
	meta: {}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_STOPWATCH_CATEGORIES}_PENDING`:
		case `${types.FETCH_STOPWATCH_CATEGORY}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_STOPWATCH_CATEGORIES}_REJECTED`:
		case `${types.FETCH_STOPWATCH_CATEGORY}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_STOPWATCH_CATEGORY}_FULFILLED`:
			return {
				...state,
				current: action.payload.data.data
			};

		case `${types.FETCH_STOPWATCH_CATEGORIES}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
			};
		default:
			return state;
	}
};
