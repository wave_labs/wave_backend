import { types } from "./types";
import api from "api/stopwatch/category";

export const fetchStopwatchCategories = (filters = {}) => ({
    type: types.FETCH_STOPWATCH_CATEGORIES,
    payload: api.fetchStopwatchCategories(filters),
    meta: { globalError: true }
});

export const fetchStopwatchCategory = (video) => ({
    type: types.FETCH_STOPWATCH_CATEGORY,
    payload: api.fetchStopwatchCategory(video),
    meta: { globalError: true }
});
