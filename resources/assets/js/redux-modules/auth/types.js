const registrationSuccessAlert = {
    message: "Registration done",
    description: "Please confirm your email!"
}

const registrationErrorAlert = {
    message: "Registration  Failed",
    description: "Something unexpected happened, please try again!"
}

const verifySuccessAlert = {
    message: "Verification  Done",
    description: "Your account has been verified!"
}

const verifyErrorAlert = {
    message: "Verification  Error",
    description: "Something unexpected happend, please try again!"
}

const verifyFailedAlert = {
    message: "Verification  Failed",
    description: "The token has expired! We will send you an email with a new token"
}

const recoverErrorAlert = {
    message: "Recovery  Error",
    description: "Something unexpected happend, please try again!"
}

const recoverSuccessAlert = {
    message: "Check your email",
    description: "A reset email has been sent, please check your email."
}

const resetSuccessAlert = {
    message: "Reset Success",
    description: "Your password has been successfully reset!"
}

const resetErrorAlert = {
    message: "Reset Error",
    description: "Something unexpected happend, please try again!"
}


const loginError = {
    message: "Reset Error",
    description: "Something unexpected happend, please try again!"
}


export const authTypes = {
    REGISTER_SUCCESS_ALERT: registrationSuccessAlert,
    REGISTER_ERROR_ALERT: registrationErrorAlert,

    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    LOGGED_USER: 'SET_USER',
    LOGIN: "LOGIN",
    ME: 'ME',
    UPDATE_ME: 'UPDATE_ME',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',
    LOGIN_FAILURE_ALERT: 'USERS_LOGIN_FAILURE',

    VERIFY_SUCCESS_ALERT: verifySuccessAlert,
    VERIFY_FAILED_ALERT: verifyFailedAlert,
    VERIFY_ERROR_ALERT: verifyErrorAlert,

    RECOVER_SUCCESS_ALERT: recoverSuccessAlert,
    RECOVER_ERROR_ALERT: recoverErrorAlert,

    RESET_SUCCESS_ALERT: resetSuccessAlert,
    RESET_ERROR_ALERT: resetErrorAlert,

    LOGOUT_SUCCESS: 'USERS_LOGOUT',
};