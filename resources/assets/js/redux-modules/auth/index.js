import { isEmpty } from 'underscore';
import { authTypes } from './types'

const initialState = {
  isAuthenticated: false,
  isAdmin: undefined,
  loading: false,
  user: {
    userable: { certificates: [], user: {} },
    roles: [{}],
    rating: {},
    occupation: {},
  },
  meta: {}, //meta information about Users and pagination
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case authTypes.LOGGED_USER:
      return {
        ...state,
        user: action.user,
        isAdmin: action.user.roles[0].name == "admin" && true
      }

    case `${authTypes.ME}_PENDING`:
    case `${authTypes.UPDATE_ME}_PENDING`:
      return {
        ...state,
        loading: true
      };

    case `${authTypes.ME}_REJECTED`:
    case `${authTypes.UPDATE_ME}_REJECTED`:
      return {
        ...state,
        loading: false
      };

    case `${authTypes.ME}_FULFILLED`:
    case `${authTypes.UPDATE_ME}_FULFILLED`:
      return {
        ...state,
        user: action.payload.data.data,
        isAdmin: action.payload.data.data.roles[0].name == "admin" && true,
        meta: action.payload.data.meta,
        loading: false
      }

    case authTypes.LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.token),
      }

    case authTypes.LOGIN_FAILURE:
    case authTypes.LOGOUT_SUCCESS:
      return {
        isAuthenticated: false,
        user: null,
        isAdmin: undefined,
      }

    default:
      return state;
  }
}