import { authTypes } from "./types";
import { alertActions } from "redux-modules/alert/actions";
import { history } from "routes";
import { getErrorMessages } from "helpers";
import axios from "axios";
import jwtDecode from "jwt-decode";
import api from "api/user";

export function register(user) {
    return dispatch => {
        return axios.post("api/register", user).then(
            data => {
                history.push("/login");
                dispatch(
                    alertActions.success({
                        message: "Thanks for registering!",
                        description: data.data.message
                    })
                );
                return data.data;
            },
            error => {
                dispatch(
                    alertActions.error({
                        message: "Registration failed",
                        description: getErrorMessages(error.response.data.errors)
                    })
                );

                return error.data;
            }
        );
    };
}

export function verify(token) {
    return dispatch => {
        history.push("/");
        return axios
            .get(`${window.location.origin}/api/user/verify/${token}`)
            .then(
                res => {
                    if (res.data.success) {
                        history.push("/login");
                        dispatch(
                            alertActions.success(authTypes.VERIFY_SUCCESS_ALERT)
                        );
                    } else {
                        dispatch(
                            alertActions.error(authTypes.VERIFY_FAILED_ALERT)
                        );
                    }
                },
                error => {
                    //TODO: SEND NEW EMAIL
                    dispatch(alertActions.error(authTypes.VERIFY_ERROR_ALERT));
                }
            );
    };
}

export function recoverPassword(email) {
    return dispatch => {
        return axios
            .post(`${window.location.origin}/api/password/recover`, email)
            .then(
                res => {
                    if (res.data.success) {
                        history.push("/");
                        dispatch(
                            alertActions.success(
                                authTypes.RECOVER_SUCCESS_ALERT
                            )
                        );
                    }

                    return res.data;
                },
                error => {
                    dispatch(alertActions.error(authTypes.RECOVER_ERROR_ALERT));

                    return error.data;
                }
            );
    };
}

export function resetPassword(data) {
    return dispatch => {
        return axios
            .post(`${window.location.origin}/api/password/reset`, data)
            .then(
                res => {
                    history.push("/login");
                    dispatch(
                        alertActions.success(authTypes.RESET_SUCCESS_ALERT)
                    );

                    return res.data;
                },
                error => {
                    dispatch(alertActions.error(authTypes.RESET_ERROR_ALERT));

                    return error.data;
                }
            );
    };
}

export function login(values) {
    return dispatch => {
        return axios.post("api/login", values).then(
            res => {
                const token = res.data.data.access_token;
                const user = res.data.user;
                localStorage.setItem("jwtToken", token);
                setAuthorizationToken(token);
                dispatch(loginUser(user));
                dispatch(loginSuccess(jwtDecode(token)));
                history.push("/dashboard");
                return res.data;

            },
            error => {
                dispatch(
                    alertActions.error({
                        message: "Login Failed",
                        description: error.response.data.message
                    })
                );

                throw "LoginError";
            }
        );
    };
}

export const me = () => {
    return (dispatch) => {
        const response = dispatch({
            type: authTypes.ME,
            payload: axios.get(`${window.location.origin}/api/me`),
            meta: { globalError: true }
        })
        response.then((res) => {
            dispatch(loginUser(res.value.data.data));
        })
    }
};

export const updateMe = (data) => {

    return (dispatch) => {
        const response = dispatch({
            type: authTypes.UPDATE_ME,
            payload: axios.put(`${window.location.origin}/api/me`, data),
            meta: { globalError: true }
        })
        response.then(
            data => {
                //
            },
            error => {

                dispatch(
                    alertActions.error({
                        message: "Update failed",
                        description: getErrorMessages(error.response.data.errors)
                    })
                );

                return error.data;
            }
        );
    }
};

export function logout() {
    return dispatch => {
        return axios
            .get("api/logout")
            .then(res => {
                resetToken(dispatch);
            })
            .catch(err => {
                throw err;
            });
    };
}

function resetToken(dispatch) {
    localStorage.removeItem("jwtToken");
    setAuthorizationToken(false);
    dispatch(logoutSuccess());
}

export function loginSuccess(token) {
    return { type: authTypes.LOGIN_SUCCESS, token };
}

export function loginUser(user) {
    return { type: authTypes.LOGGED_USER, user };
}

export function loginFailure(error) {
    return { type: authTypes.LOGIN_FAILURE, error };
}

export function logoutSuccess() {
    return {
        type: authTypes.LOGOUT_SUCCESS
    };
}

function removeAxiosDefaultToken() {
    delete axios.defaults.headers.common["Authorization"];
}

export function setAuthorizationToken(token, refresh = false) {
    if (token) {
        refresh && removeAxiosDefaultToken();
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        axios.defaults.headers.common['X-localization'] = `en`;

    } else {
        removeAxiosDefaultToken();
    }
}

export function refreshAuthorizationToken(token) {
    return dispatch => {
        return axios({
            url: `${window.location.origin}/api/refresh`,
            method: "get",
            headers: { Authorization: `Bearer ${token}` }
        })
            .then(res => {
                const token = res.data.data.access_token;
                localStorage.setItem("jwtToken", token);
                setAuthorizationToken(token, true);
                dispatch(loginSuccess(jwtDecode(token)));
            })
            .catch(err => {
                history.push("/");
                resetToken(dispatch);
            });
    };
}
