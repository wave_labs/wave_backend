import { types } from "./types";

const initialState = {
  data: [], //
  meta: {}, //meta information about litter and pagination
  loading: false,
  current: {},
  selector: [],
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case `${types.FETCH_INSECT_SINONIMIAS}_PENDING`:
    case `${types.FETCH_INSECT_SINONIMIA}_PENDING`:
    case `${types.UPDATE_INSECT_SINONIMIA}_PENDING`:
    case `${types.CREATE_INSECT_SINONIMIA}_PENDING`:
    case `${types.DELETE_INSECT_SINONIMIA}_PENDING`:
    case `${types.SET_INSECT_SINONIMIA_CURRENT}_PENDING`:
      return {
        ...state,
        loading: true,
      };

    case `${types.FETCH_INSECT_SINONIMIAS}_REJECTED`:
    case `${types.FETCH_INSECT_SINONIMIA}_REJECTED`:
    case `${types.UPDATE_INSECT_SINONIMIA}_REJECTED`:
    case `${types.DELETE_INSECT_SINONIMIA}_REJECTED`:
    case `${types.CREATE_INSECT_SINONIMIA}_REJECTED`:
      return {
        ...state,
        loading: false,
      };

    case `${types.FETCH_INSECT_SINONIMIAS}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: action.payload.data.data,
        meta: action.payload.data.meta,
      };

    case `${types.FETCH_INSECT_SINONIMIA}_FULFILLED`:
      return {
        ...state,
        loading: false,
        current: action.payload.data.data,
      };

    case `${types.CREATE_INSECT_SINONIMIA}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: [action.payload.data.data, ...state.data],
      };

    case `${types.UPDATE_INSECT_SINONIMIA}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: state.data.map((record, index) =>
          record.id === action.payload.data.data.id
            ? action.payload.data.data
            : record
        ),
      };
    case `${types.DELETE_INSECT_SINONIMIA}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: state.data.filter((record) => record.id !== action.meta.id),
      };

    case `${types.SET_INSECT_SINONIMIA_CURRENT}`:
      return {
        ...state,
        current: action.payload,
      };

    default:
      return state;
  }
};
