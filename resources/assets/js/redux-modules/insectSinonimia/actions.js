import { types } from "./types";
import api from "api/insect/sinonimia";

export const fetchInsectSinonimias = (page, filters) => ({
    type: types.FETCH_INSECT_SINONIMIAS,
    payload: api.fetchInsectSinonimias(page, filters),
    meta: { globalError: true }
});

export const fetchInsectSinonimia = (filters) => ({
    type: types.FETCH_INSECT_SINONIMIA,
    payload: api.fetchInsectSinonimia(filters),
    meta: { globalError: true }
});

export const createInsectSinonimia = (data) => ({
    type: types.CREATE_INSECT_SINONIMIA,
    payload: api.createInsectSinonimia(data),
    meta: { globalError: true }
});

export const updateInsectSinonimia = (id, data) => ({
    type: types.UPDATE_INSECT_SINONIMIA,
    payload: api.updateInsectSinonimia(id, data),
    meta: { globalError: true }
});

export const deleteInsectSinonimia = (id) => ({
    type: types.DELETE_INSECT_SINONIMIA,
    payload: api.deleteInsectSinonimia(id),
    meta: { id, globalError: true }
});

export const setInsectSinonimiaCurrent = (record = {}) => ({
    type: types.SET_INSECT_SINONIMIA_CURRENT,
    payload: record
});


