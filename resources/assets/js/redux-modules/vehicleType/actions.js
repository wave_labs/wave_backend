import { types } from "./types";
import api from "api/vehicleType";

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_VEHICLE_TYPE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});