import { types } from "./types";

const initialState = {
    data: [], //DivingSpot list
    DivingSpotSelector: [],
    meta: {}, //meta information about DivingSpots and pagination
    loading: false,
    editDivingSpot: {
        //used to pass edit information to form
    },
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.DELETE_DIVING_SPOT}_PENDING`:
        case `${types.FETCH_DIVING_SPOT_SELECTOR}_PENDING`:
        case `${types.FETCH_DIVING_SPOTS}_PENDING`:
        case `${types.ADD_TO_USER}_PENDING`:
        case `${types.REMOVE_FROM_USER}_PENDING`:
            return {
                ...state,
                loading: true,
            };

        case `${types.FETCH_DIVING_SPOTS}_REJECTED`:
        case `${types.FETCH_DIVING_SPOT_SELECTOR}_REJECTED`:
        case `${types.DELETE_DIVING_SPOT}_REJECTED`:
        case `${types.ADD_TO_USER}_REJECTED`:
        case `${types.ADD_TO_USER}_FULFILLED`:
        case `${types.REMOVE_FROM_USER}_REJECTED`:
        case `${types.REMOVE_FROM_USER}_FULFILLED`:
            return {
                ...state,
                loading: false,
            };

        case `${types.FETCH_DIVING_SPOTS}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: action.payload.data.data,
                meta: action.payload.data.meta,
            };

        case `${types.FETCH_DIVING_SPOT_SELECTOR}_FULFILLED`:
            return {
                ...state,
                loading: false,
                DivingSpotSelector: action.payload.data.data,
            };

        case `${types.DELETE_DIVING_SPOT}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: state.data.filter(
                    (divingSpot) => divingSpot.id !== action.meta.id
                ),
            };

        case `${types.CREATE_DIVING_SPOT}_FULFILLED`:
            return {
                ...state,
                data: [action.payload.data.data, ...state.data],
            };

        case `${types.UPDATE_DIVING_SPOT}_FULFILLED`:
            return {
                ...state,
                data: state.data.map((divingSpot, index) =>
                    divingSpot.id === action.payload.data.data.id
                        ? action.payload.data.data
                        : divingSpot
                ),
            };

        case types.SET_DIVING_SPOT_EDIT:
            return {
                ...state,
                editDivingSpot: action.data,
            };

        case types.RESET_DIVING_SPOT_EDIT:
            return {
                ...state,
                editDivingSpot: {
                    user: {},
                },
            };

        default:
            return state;
    }
};
