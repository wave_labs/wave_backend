import { types } from "./types";
import api from "api/divingSpot";

export const fetchDivingSpots = (page = 1, filters = {}) => ({
    type: types.FETCH_DIVING_SPOTS,
    payload: api.fetchDivingSpots(page, filters),
    meta: { globalError: true }
});

export const fetchSelector = (filters) => ({
    type: types.FETCH_DIVING_SPOT_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});

export const deleteDivingSpot = id => ({
    type: types.DELETE_DIVING_SPOT,
    payload: api.deleteDivingSpot(id),
    meta: { id, globalError: true }
});

export const createDivingSpot = data => ({
    type: types.CREATE_DIVING_SPOT,
    payload: api.createDivingSpot(data),
    meta: { globalError: true }
});

export const addToUser = data => ({
    type: types.ADD_TO_USER,
    payload: api.addToUser(data),
    meta: { globalError: true }
});

export const removeFromUser = data => ({
    type: types.REMOVE_FROM_USER,
    payload: api.removeFromUser(data),
    meta: { globalError: true }
});

export const updateDivingSpot = (id, data) => ({
    type: types.UPDATE_DIVING_SPOT,
    payload: api.updateDivingSpot(id, data),
    meta: { globalError: true }
});

export const setDivingSpotEdit = data => ({
    type: types.SET_DIVING_SPOT_EDIT,
    data
});

export const resetDivingSpotEdit = data => ({
    type: types.RESET_DIVING_SPOT_EDIT
});
