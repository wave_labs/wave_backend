import { types } from "./types";
import api from "api/marker";
import axios from "axios";
import { stringify } from "query-string";
import { download } from "helpers";

export const fetchMarkers = (page = 1, filters = {}) => ({
    type: types.FETCH_MARKERS,
    payload: api.fetchMarkers(page, filters),
    meta: { globalError: true }
});

export const fetchMarkerCoordinates = (page = 1, filters = {}) => ({
    type: types.FETCH_MARKERS_COORDINATES,
    payload: api.fetchMarkerCoordinates(page, filters),
    meta: { globalError: true }
});

export const fetchMarker = id => ({
    type: types.FETCH_MARKER,
    payload: api.fetchMarker(id),
    meta: { id, globalError: true }
});

export const deleteMarker = id => ({
    type: types.DELETE_MARKER,
    payload: api.deleteMarker(id),
    meta: { id, globalError: true }
});

export const createMarker = data => ({
    type: types.CREATE_MARKER,
    payload: api.createMarker(data),
    meta: { globalError: true }
});

export const updateMarker = (id, data) => ({
    type: types.UPDATE_MARKER,
    payload: api.updateMarker(id, data),
    meta: { globalError: true }
});

export const setCurrentMarker = data => ({
    type: types.SET_CURRENT_MARKER,
    data
});

export const resetCurrentMarker = data => ({
    type: types.RESET_CURRENT_MARKER
});

export const resetMarkerCoordinates = () => ({
    type: types.RESET_MARKER_COORDINATES
});

export const exportMarkers = (filters = {}) => ({
    type: types.EXPORT_MARKERS,
    payload: axios({
        url: `${window.location.origin}/api/export/csv/marker?${stringify(filters, {
            arrayFormat: "index"
        })}`,
        method: "GET",
        responseType: "blob",
    }).then(
        response => {
            download(response, ' marker.xlsx')
        },
        error => {
            return error.data;
        }
    ),
    meta: { globalError: true }
});
