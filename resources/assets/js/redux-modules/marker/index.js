import { types } from "./types";

const initialState = {
    data: [], //Litter list
    meta: {}, //meta information about litter and pagination
    loading: false,
    current: [],
    coordinates: [],
    loadingCoordinates: false,
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_MARKERS}_PENDING`:
        case `${types.FETCH_MARKER}_PENDING`:
        case `${types.CREATE_MARKER}_PENDING`:
        case `${types.UPDATE_MARKER}_PENDING`:
        case `${types.DELETE_MARKER}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_MARKERS}_REJECTED`:
        case `${types.FETCH_MARKER}_REJECTED`:
        case `${types.CREATE_MARKER}_REJECTED`:
        case `${types.UPDATE_MARKER}_REJECTED`:
        case `${types.DELETE_MARKER}_REJECTED`:
            return {
                ...state,
                loading: false,
            };

        case `${types.FETCH_MARKERS_COORDINATES}_PENDING`:
            return {
                ...state,
                loadingCoordinates: true
            };

        case `${types.DELETE_MARKER}_REJECTED`:
            return {
                ...state,
                loadingCoordinates: false,
            };

        case `${types.FETCH_MARKERS}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: action.payload.data.data,
                meta: action.payload.data.meta
            };

        case `${types.FETCH_MARKER}_FULFILLED`:
            return {
                ...state,
                loading: false,
                current: {
                    ...action.payload.data.data
                }
            };

        case `${types.DELETE_MARKER}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: state.data.filter(record => record.id !== action.meta.id)
            };

        case `${types.CREATE_MARKER}_FULFILLED`:
            return {
                ...state,
                data: [action.payload.data.data, ...state.data]
            };

        case `${types.UPDATE_MARKER}_FULFILLED`:
            return {
                ...state,
                data: state.data.map((record) =>
                    record.id === action.payload.data.data.id
                        ? action.payload.data.data
                        : record
                )
            };

        case `${types.FETCH_MARKERS_COORDINATES}_FULFILLED`:
            return {
                ...state,
                loadingCoordinates: false,
                coordinates: action.payload.data.data
            };

        case types.SET_CURRENT_MARKER:
            return {
                ...state,
                loading: false,
                current: {
                    ...action.data
                }
            };

        case types.EXPORT_MARKERS:
            return {
                ...state,
            };

        case types.RESET_CURRENT_MARKER:
            return {
                ...state,
                current: {}
            };

        case types.RESET_MARKER_COORDINATES:
            return {
                ...state,
                coordinates: {}
            };

        default:
            return state;
    }
};
