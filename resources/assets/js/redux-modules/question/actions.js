import { types } from "./types";
import api from "api/question";

export const fetchQuestion = (form, page = 1, filters = {}) => ({
    type: types.FETCH_QUESTION,
    payload: api.fetchQuestion(form, page, filters),
    meta: { globalError: true }
});

export const deleteQuestion = id => ({
    type: types.DELETE_QUESTION,
    payload: api.deleteQuestion(id),
    meta: { id, globalError: true }
});

export const createQuestion = data => ({
    type: types.CREATE_QUESTION,
    payload: api.createQuestion(data),
    meta: { globalError: true }
});

export const updateQuestion = (id, data) => ({
    type: types.UPDATE_QUESTION,
    payload: api.updateQuestion(id, data),
    meta: { globalError: true }
});

export const setQuestionEdit = data => ({
    type: types.SET_QUESTION_EDIT,
    data
});

export const resetQuestionEdit = data => ({
    type: types.RESET_QUESTION_EDIT
});
