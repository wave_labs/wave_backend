import { types } from "./types";

const initialState = {
	data: [], // Question list
	meta: {}, //meta information about  Question and pagination
	loading: false,
	editQuestion: { //used to pass edit information to form
		user: {}
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_QUESTION}_PENDING`:
		case `${types.FETCH_QUESTION}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_QUESTION}_REJECTED`:
		case `${types.DELETE_QUESTION}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_QUESTION}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_QUESTION}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					question => question.id !== action.meta.id
				)
			};

		case `${types.CREATE_QUESTION}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_QUESTION}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(question, index) =>
						question.id === action.payload.data.data.id
							? action.payload.data.data
							: question
				)
			};

		case types.SET_QUESTION_EDIT:
			return {
				...state,
				editQuestion: action.data
			};

		case types.RESET_QUESTION_EDIT:
			return {
				...state,
				editQuestion: {
					user: {}
				}
			};

		default:
			return state;
	}
};
