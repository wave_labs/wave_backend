import { types } from "./types";
import api from "api/suggestionType";

export const fetchSelector = (filters = {}) => ({
    type: types.FETCH_SUGGESTION_TYPE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});