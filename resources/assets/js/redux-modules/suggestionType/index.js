import { types } from "./types";

const initialState = {
    data: [],
    SuggestionTypeSelector: [],
    meta: {},
    loading: false,
    editSuggestionType: {
        //used to pass edit information to form
    }
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_SUGGESTION_TYPE_SELECTOR}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_SUGGESTION_TYPE_SELECTOR}_REJECTED`:
            return {
                ...state,
                loading: false
            };

        case `${types.FETCH_SUGGESTION_TYPE_SELECTOR}_FULFILLED`:
            return {
                ...state,
                loading: false,
                SuggestionTypeSelector: action.payload.data.data,
            };

        default:
            return state;
    }
};
