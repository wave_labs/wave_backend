import { types } from "./types";

const initialState = {
	data: [], //Feedback list
	meta: {}, //meta information about Feedback and pagination
	loading: false,
	editFeedback: { //used to pass edit information to form
		user: {}
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_FEEDBACK}_PENDING`:
		case `${types.FETCH_FEEDBACK}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_FEEDBACK}_REJECTED`:
		case `${types.DELETE_FEEDBACK}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_FEEDBACK}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_FEEDBACK}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					feedback => feedback.id !== action.meta.id
				)
			};

		case `${types.CREATE_FEEDBACK}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_FEEDBACK}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(feedback, index) =>
						feedback.id === action.payload.data.data.id
							? action.payload.data.data
							: feedback
				)
			};

		case types.SET_FEEDBACK_EDIT:
			return {
				...state,
				editFeedback: action.data
			};

		case types.RESET_FEEDBACK_EDIT:
			return {
				...state,
				editFeedback: {
					user: {}
				}
			};

		default:
			return state;
	}
};
