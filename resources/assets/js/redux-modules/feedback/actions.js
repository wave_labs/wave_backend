import { types } from "./types";
import api from "api/feedback";

export const fetchFeedback = (form, page = 1, filters = {}) => ({
    type: types.FETCH_FEEDBACK,
    payload: api.fetchFeedback(form, page, filters),
    meta: { globalError: true }
});

export const deleteFeedback = id => ({
    type: types.DELETE_FEEDBACK,
    payload: api.deleteFeedback(id),
    meta: { id, globalError: true }
});

export const createFeedback = data => ({
    type: types.CREATE_FEEDBACK,
    payload: api.createFeedback(data),
    meta: { globalError: true }
});

export const updateFeedback = (id, data) => ({
    type: types.UPDATE_FEEDBACK,
    payload: api.updateFeedback(id, data),
    meta: { globalError: true }
});

export const setFeedbackEdit = data => ({
    type: types.SET_FEEDBACK_EDIT,
    data
});

export const resetFeedbackEdit = data => ({
    type: types.RESET_FEEDBACK_EDIT
});
