import { types } from "./types";
import api from "api/insect/projects";

export const createInsectProject = (data) => ({
  type: types.CREATE_INSECT_PROJECT,
  payload: api.createInsectProject(data),
  meta: { globalError: true },
});

export const fetchInsectProjects = (filters) => ({
  type: types.FETCH_INSECT_PROJECTS,
  payload: api.fetchInsectProjects(filters),
  meta: { globalError: true },
});

export const fetchInsectProject = (id) => ({
  type: types.FETCH_INSECT_PROJECT,
  payload: api.fetchInsectProject(id),
  meta: { globalError: true },
});

export const addMemberToProject = (projectId, data) => ({
  type: types.ADD_MEMBER_INSECT_PROJECT,
  payload: api.addMemberToProject(projectId, data),
  meta: { globalError: true },
});

export const removeMemberFromProject = (userId, projectId) => ({
  type: types.REMOVE_MEMBER_INSECT_PROJECT,
  payload: api.removeMemberFromProject(userId, projectId),
  meta: { userId, globalError: true },
});

export const updateInsectProject = (id, data) => ({
  type: types.UPDATE_INSECT_PROJECT,
  payload: api.updateInsectProject(id, data),
  meta: { globalError: true },
});
