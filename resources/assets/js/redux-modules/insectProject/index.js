import { types } from "./types";

const initialState = {
  data: [], //
  projectSelector: [],
  meta: {}, //meta information about litter and pagination
  loading: false,
  current: {},
  /* selector: [], */
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case `${types.CREATE_INSECT_PROJECT}_PENDING`:
    case `${types.FETCH_INSECT_PROJECTS}_PENDING`:
    case `${types.FETCH_INSECT_PROJECT}_PENDING`:
    case `${types.ADD_MEMBER_INSECT_PROJECT}_PENDING`:
    case `${types.REMOVE_MEMBER_INSECT_PROJECT}_PENDING`:
    case `${types.UPDATE_INSECT_PROJECT}_PENDING`:
      return {
        ...state,
        loading: true,
      };

    case `${types.CREATE_INSECT_PROJECT}_REJECTED`:
    case `${types.FETCH_INSECT_PROJECTS}_REJECTED`:
    case `${types.FETCH_INSECT_PROJECT}_REJECTED`:
    case `${types.ADD_MEMBER_INSECT_PROJECT}_REJECTED`:
    case `${types.REMOVE_MEMBER_INSECT_PROJECT}_REJECTED`:
    case `${types.UPDATE_INSECT_PROJECT}_REJECTED`:
      return {
        ...state,
        loading: false,
      };

    case `${types.CREATE_INSECT_PROJECT}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: [action.payload.data.data, ...state.data],
      };

    case `${types.FETCH_INSECT_PROJECTS}_FULFILLED`:
      return {
        ...state,
        loading: false,
        data: action.payload.data.data,
      };

    case `${types.FETCH_INSECT_PROJECT}_FULFILLED`:
      return {
        ...state,
        loading: false,
        projectSelector: action.payload.data.data,
      };

    case `${types.ADD_MEMBER_INSECT_PROJECT}_FULFILLED`:
      var newTeam = [...state.projectSelector.team, action.payload.data];
      [state.projectSelector].map(function (item) {
        item["team"] = newTeam;
      });

      return {
        ...state,
        loading: false,
        projectSelector: state.projectSelector,
      };

    case `${types.REMOVE_MEMBER_INSECT_PROJECT}_FULFILLED`:
      var newTeam = state.projectSelector.team.filter(
        (record) => record.id !== action.meta.userId
      );
      [state.projectSelector].map(function (item) {
        item["team"] = newTeam;
      });

      return {
        ...state,
        loading: false,
        projectSelector: state.projectSelector,
      };
    case `${types.UPDATE_INSECT_PROJECT}_FULFILLED`:
      return {
        ...state,
        loading: false,
        projectSelector: action.payload.data.data,
      };

    default:
      return state;
  }
};
