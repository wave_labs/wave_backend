import { types } from "./types";
import api from "api/result";

export const fetchResult = (id, page = 1, filters = {}) => ({
    type: types.FETCH_RESULT,
    payload: api.fetchResult(id, page, filters),
    meta: { globalError: true }
});

export const deleteResult = id => ({
    type: types.DELETE_RESULT,
    payload: api.deleteResult(id),
    meta: { id, globalError: true }
});

export const createResult = data => ({
    type: types.CREATE_RESULT,
    payload: api.createResult(data),
    meta: { globalError: true }
});

export const updateResult = (id, data) => ({
    type: types.UPDATE_RESULT,
    payload: api.updateResult(id, data),
    meta: { globalError: true }
});

export const setResultEdit = data => ({
    type: types.SET_RESULT_EDIT,
    data
});

export const resetResultEdit = data => ({
    type: types.RESET_RESULT_EDIT
});
