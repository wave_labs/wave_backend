import { types } from "./types";

const initialState = {
	data: [], //Result list
	meta: {}, //meta information about Result and pagination
	loading: false,
	editResult: { //used to pass edit information to form
		user: {}
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_RESULT}_PENDING`:
		case `${types.FETCH_RESULT}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_RESULT}_REJECTED`:
		case `${types.DELETE_RESULT}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_RESULT}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_RESULT}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					result => result.id !== action.meta.id
				)
			};

		case `${types.CREATE_RESULT}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_RESULT}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(result, index) =>
						result.id === action.payload.data.data.id
							? action.payload.data.data
							: result
				)
			};

		case types.SET_RESULT_EDIT:
			return {
				...state,
				editResult: action.data
			};

		case types.RESET_RESULT_EDIT:
			return {
				...state,
				editResult: {
					user: {}
				}
			};

		default:
			return state;
	}
};
