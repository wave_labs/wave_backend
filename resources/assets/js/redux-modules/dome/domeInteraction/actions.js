import { types } from "./types";
import api from "api/dome/domeInteraction";


export const getDomeInteraction = (filters) => ({
    type: types.GET_DOME_INTERACTION,
    payload: api.getDomeInteraction(filters),
    meta: { globalError: true }
});


export const incrementDomeInteraction = (filters) => ({
    type: types.INCREMENT_DOME_INTERACTION,
    payload: api.incrementDomeInteraction(filters),
    meta: { globalError: true }
});
