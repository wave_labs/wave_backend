import { types } from "./types";

const initialState = {
	data: [],
	counter: 0,
	meta: {},
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.GET_DOME_INTERACTION}_PENDING`:
		case `${types.INCREMENT_DOME_INTERACTION}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.GET_DOME_INTERACTION}_REJECTED`:
		case `${types.INCREMENT_DOME_INTERACTION}_REJECTED`:
			return {
				...state,
				loading: false,
			};

		case `${types.GET_DOME_INTERACTION}_FULFILLED`:
			return {
				...state,
				loading: false,
				counter: action.payload.data.counter,
			};

		case `${types.INCREMENT_DOME_INTERACTION}_FULFILLED`:
			return {
				...state,
				loading: false,
				counter: action.payload.data.counter,
			};

		default:
			return state;
	}
};
