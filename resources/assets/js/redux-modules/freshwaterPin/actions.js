import { types } from "./types";
import api from "api/freshwaterPin";
import axios from "axios";
import { stringify } from "query-string";
import { download } from "helpers";

export const fetchFreshwaterPins = (page = 1, filters = {}) => ({
    type: types.FETCH_FRESHWATER_PINS,
    payload: api.fetchFreshwaterPins(page, filters),
    meta: { globalError: true }
});

export const fetchFreshwaterPinCoordinates = (filters = {}) => ({
    type: types.FETCH_FRESHWATER_PIN_COORDINATES,
    payload: api.fetchFreshwaterPinCoordinates(filters),
    meta: { globalError: true }
});

export const fetchFreshwaterPin = id => ({
    type: types.FETCH_FRESHWATER_PIN,
    payload: api.fetchFreshwaterPin(id),
    meta: { id, globalError: true }
});

export const deleteFreshwaterPin = id => ({
    type: types.DELETE_FRESHWATER_PIN,
    payload: api.deleteFreshwaterPin(id),
    meta: { id, globalError: true }
});

export const createFreshwaterPin = data => ({
    type: types.CREATE_FRESHWATER_PIN,
    payload: api.createFreshwaterPin(data),
    meta: { globalError: true }
});

export const updateFreshwaterPin = (id, data) => ({
    type: types.UPDATE_FRESHWATER_PIN,
    payload: api.updateFreshwaterPin(id, data),
    meta: { globalError: true }
});

export const setCurrentMarker = data => ({
    type: types.SET_CURRENT_FRESHWATER_PIN,
    data
});

export const resetCurrentMarker = data => ({
    type: types.RESET_CURRENT_FRESHWATER_PIN
});

export const exportFreshwaterPin = (filters = {}) => ({
    type: types.EXPORT_FRESHWATER_PINS,
    payload: axios({
        url: `${window.location.origin}/api/export/csv/freshwater-pin?${stringify(filters, {
            arrayFormat: "index"
        })}`,
        method: "GET",
        responseType: "blob",
    }).then(
        response => {
            download(response, ' freshwaterPin.xlsx')
        },
        error => {
            return error.data;
        }
    ),
    meta: { globalError: true }
});
