import { types } from "./types";

const initialState = {
    data: [], //Litter list
    meta: {}, //meta information about litter and pagination
    loading: false,
    current: [],
    coordinates: [],
    loadingCoordinates: false,
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_FRESHWATER_PINS}_PENDING`:
        case `${types.FETCH_FRESHWATER_PIN}_PENDING`:
        case `${types.CREATE_FRESHWATER_PIN}_PENDING`:
        case `${types.UPDATE_FRESHWATER_PIN}_PENDING`:
        case `${types.DELETE_FRESHWATER_PIN}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_FRESHWATER_PIN_COORDINATES}_PENDING`:
            return {
                ...state,
                loadingCoordinates: true
            };

        case `${types.FETCH_FRESHWATER_PINS}_REJECTED`:
        case `${types.FETCH_FRESHWATER_PIN}_REJECTED`:
        case `${types.CREATE_FRESHWATER_PIN}_REJECTED`:
        case `${types.UPDATE_FRESHWATER_PIN}_REJECTED`:
        case `${types.DELETE_FRESHWATER_PIN}_REJECTED`:
            return {
                ...state,
                loading: false,
            };

        case `${types.FETCH_FRESHWATER_PINS}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: action.payload.data.data,
                meta: action.payload.data.meta
            };

        case `${types.FETCH_FRESHWATER_PIN_COORDINATES}_FULFILLED`:
            return {
                ...state,
                loadingCoordinates: false,
                coordinates: action.payload.data.data
            };

        case `${types.FETCH_FRESHWATER_PIN}_FULFILLED`:
            return {
                ...state,
                loading: false,
                current: {
                    ...action.payload.data.data
                }
            };

        case `${types.DELETE_FRESHWATER_PIN}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: state.data.filter(record => record.id !== action.meta.id)
            };

        case `${types.CREATE_FRESHWATER_PIN}_FULFILLED`:
            return {
                ...state,
                data: [action.payload.data.data, ...state.data]
            };

        case `${types.UPDATE_FRESHWATER_PIN}_FULFILLED`:
            return {
                ...state,
                data: state.data.map((record) =>
                    record.id === action.payload.data.data.id
                        ? action.payload.data.data
                        : record
                )
            };



        case types.SET_CURRENT_FRESHWATER_PIN:
            return {
                ...state,
                loading: false,
                current: {
                    ...action.data
                }
            };

        case types.EXPORT_FRESHWATER_PINS:
            return {
                ...state,
            };

        case types.RESET_CURRENT_FRESHWATER_PIN:
            return {
                ...state,
                current: {}
            };

        default:
            return state;
    }
};
