import { types } from "./types";
import api from "api/userPerson";

export const fetchUserPerson = (page = 1, filters = {}) => ({
    type: types.FETCH_USER_PERSON,
    payload: api.fetchUserPerson(page, filters),
    meta: { globalError: true }
});

export const deleteUserPerson = id => ({
    type: types.DELETE_USER_PERSON,
    payload: api.deleteUserPerson(id),
    meta: { id, globalError: true }
});

export const createUserPerson = data => ({
    type: types.CREATE_USER_PERSON,
    payload: api.createUserPerson(data),
    meta: { globalError: true }
});

export const updateUserPerson = (id, data) => ({
    type: types.UPDATE_USER_PERSON,
    payload: api.updateUserPerson(id, data),
    meta: { globalError: true }
});

export const setUserPersonEdit = data => ({
    type: types.SET_USER_PERSON_EDIT,
    data
});

export const resetUserPersonEdit = data => ({
    type: types.RESET_USER_PERSON_EDIT
});
