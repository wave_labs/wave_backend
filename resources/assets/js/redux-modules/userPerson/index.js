import { types } from "./types";

const initialState = {
	data: [], //UserPerson list
	meta: {}, //meta information about UserPerson and pagination
	loading: false,
	editUserPerson: { //used to pass edit information to form
	}
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.DELETE_USER_PERSON}_PENDING`:
		case `${types.FETCH_USER_PERSON}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_USER_PERSON}_REJECTED`:
		case `${types.DELETE_USER_PERSON}_REJECTED`:
			return {
				...state,
				loading: false
			};

		case `${types.FETCH_USER_PERSON}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		case `${types.DELETE_USER_PERSON}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: state.data.filter(
					userPerson => userPerson.id !== action.meta.id
				)
			};

		case `${types.CREATE_USER_PERSON}_FULFILLED`:
			return {
				...state,
				data: [action.payload.data.data, ...state.data]
			};

		case `${types.UPDATE_USER_PERSON}_FULFILLED`:
			return {
				...state,
				data: state.data.map(
					(userPerson, index) =>
						userPerson.id === action.payload.data.data.id
							? action.payload.data.data
							: userPerson
				)
			};

		case types.SET_USER_PERSON_EDIT:
			return {
				...state,
				editUserPerson: action.data
			};

		case types.RESET_USER_PERSON_EDIT:
			return {
				...state,
				editUserPerson: {
					user: {}
				}
			};

		default:
			return state;
	}
};
