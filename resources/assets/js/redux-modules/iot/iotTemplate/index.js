import { types } from "./types";

const initialState = {
	data: [],
	selector: [],
	meta: {},
	loading: false,
};

export default (state = initialState, action = {}) => {
	switch (action.type) {
		case `${types.FETCH_IOT_TEMPLATES}_PENDING`:
		case `${types.FETCH_IOT_TEMPLATE_SELECTOR}_PENDING`:
			return {
				...state,
				loading: true
			};

		case `${types.FETCH_IOT_TEMPLATES}_REJECTED`:
		case `${types.FETCH_IOT_TEMPLATE_SELECTOR}_REJECTED`:
			return {
				...state,
				loading: false,
			};

		case `${types.FETCH_IOT_TEMPLATE_SELECTOR}_FULFILLED`:
			return {
				...state,
				loading: false,
				selector: action.payload.data.data,
			};
		case `${types.FETCH_IOT_TEMPLATES}_FULFILLED`:
			return {
				...state,
				loading: false,
				data: action.payload.data.data,
				meta: action.payload.data.meta
			};

		default:
			return state;
	}
};
