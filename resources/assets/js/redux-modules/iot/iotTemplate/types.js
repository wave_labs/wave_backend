export const types = {
    FETCH_IOT_TEMPLATES: 'FETCH_IOT_TEMPLATES',
    FETCH_IOT_TEMPLATE_SELECTOR: 'FETCH_IOT_TEMPLATE_SELECTOR',
};
