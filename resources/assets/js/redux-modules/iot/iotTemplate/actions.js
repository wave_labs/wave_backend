import { types } from "./types";
import api from "api/iot/template";

export const fetchIotTemplates = (page, filters) => ({
    type: types.FETCH_IOT_TEMPLATES,
    payload: api.fetchIotTemplates(page, filters),
    meta: { globalError: true }
});

export const fetchSelector = (filters) => ({
    type: types.FETCH_IOT_TEMPLATE_SELECTOR,
    payload: api.fetchSelector(filters),
    meta: { globalError: true }
});


