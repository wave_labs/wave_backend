import { types } from "./types";
import api from "api/trout";
import axios from "axios";
import { stringify } from "query-string";
import { download } from "helpers";

export const fetchTrouts = (page = 1, filters = {}) => ({
    type: types.FETCH_TROUTS,
    payload: api.fetchTrouts(page, filters),
    meta: { globalError: true }
});

export const fetchTroutCoordinates = (filters = {}) => ({
    type: types.FETCH_TROUT_COORDINATES,
    payload: api.fetchTroutCoordinates(filters),
    meta: { globalError: true }
});

export const fetchTrout = id => ({
    type: types.FETCH_TROUT,
    payload: api.fetchTrout(id),
    meta: { id, globalError: true }
});

export const deleteTrout = id => ({
    type: types.DELETE_TROUT,
    payload: api.deleteTrout(id),
    meta: { id, globalError: true }
});

export const createTrout = data => ({
    type: types.CREATE_TROUT,
    payload: api.createTrout(data),
    meta: { globalError: true }
});

export const updateTrout = (id, data) => ({
    type: types.UPDATE_TROUT,
    payload: api.updateTrout(id, data),
    meta: { globalError: true }
});

export const setCurrentMarker = data => ({
    type: types.SET_CURRENT_TROUT,
    data
});

export const resetCurrentMarker = data => ({
    type: types.RESET_CURRENT_TROUT
});

export const exportTrout = (filters = {}) => ({
    type: types.EXPORT_TROUTS,
    payload: axios({
        url: `${window.location.origin}/api/export/csv/trout?${stringify(filters, {
            arrayFormat: "index"
        })}`,
        method: "GET",
        responseType: "blob",
    }).then(
        response => {
            download(response, ' trout.xlsx')
        },
        error => {
            return error.data;
        }
    ),
    meta: { globalError: true }
});
