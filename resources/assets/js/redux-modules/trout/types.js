export const types = {
    FETCH_TROUTS: 'FETCH_TROUTS',
    FETCH_TROUT: 'FETCH_TROUT',
    CREATE_TROUT: 'CREATE_TROUT',
    UPDATE_TROUT: 'UPDATE_TROUT',
    DELETE_TROUT: 'DELETE_TROUT',
    EXPORT_TROUTS: 'EXPORT_TROUTS',
    FETCH_TROUT_COORDINATES: 'FETCH_TROUT_COORDINATES',

    SET_CURRENT_TROUT: 'SET_CURRENT_TROUT',
    RESET_CURRENT_TROUT: 'RESET_CURRENT_TROUT',

    RESET_TROUT_COORDINATES: 'RESET_TROUT_COORDINATES',
    FETCH_TROUTS_COORDINATES: 'FETCH_TROUTS_COORDINATES',
};
