import { types } from "./types";

const initialState = {
    data: [], //Litter list
    meta: {}, //meta information about litter and pagination
    loading: false,
    current: [],
    coordinates: [],
    loadingCoordinates: false,
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_TROUTS}_PENDING`:
        case `${types.FETCH_TROUT}_PENDING`:
        case `${types.CREATE_TROUT}_PENDING`:
        case `${types.UPDATE_TROUT}_PENDING`:
        case `${types.DELETE_TROUT}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_TROUT_COORDINATES}_PENDING`:
            return {
                ...state,
                loadingCoordinates: true
            };

        case `${types.FETCH_TROUTS}_REJECTED`:
        case `${types.FETCH_TROUT}_REJECTED`:
        case `${types.CREATE_TROUT}_REJECTED`:
        case `${types.UPDATE_TROUT}_REJECTED`:
        case `${types.DELETE_TROUT}_REJECTED`:
            return {
                ...state,
                loading: false,
            };

        case `${types.FETCH_TROUTS}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: action.payload.data.data,
                meta: action.payload.data.meta
            };

        case `${types.FETCH_TROUT_COORDINATES}_FULFILLED`:
            return {
                ...state,
                loadingCoordinates: false,
                coordinates: action.payload.data.data
            };

        case `${types.FETCH_TROUT}_FULFILLED`:
            return {
                ...state,
                loading: false,
                current: {
                    ...action.payload.data.data
                }
            };

        case `${types.DELETE_TROUT}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: state.data.filter(record => record.id !== action.meta.id)
            };

        case `${types.CREATE_TROUT}_FULFILLED`:
            return {
                ...state,
                data: [action.payload.data.data, ...state.data]
            };

        case `${types.UPDATE_TROUT}_FULFILLED`:
            return {
                ...state,
                data: state.data.map((record) =>
                    record.id === action.payload.data.data.id
                        ? action.payload.data.data
                        : record
                )
            };



        case types.SET_CURRENT_TROUT:
            return {
                ...state,
                loading: false,
                current: {
                    ...action.data
                }
            };

        case types.EXPORT_TROUTS:
            return {
                ...state,
            };

        case types.RESET_CURRENT_TROUT:
            return {
                ...state,
                current: {}
            };

        default:
            return state;
    }
};
