import { types } from "./types";

const initialState = {
    data: [],
    meta: {},
    loading: false,
    current: [],
    coordinates: [],
    loadingCoordinates: false,
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case `${types.FETCH_DIADEMAS}_PENDING`:
        case `${types.FETCH_DIADEMA}_PENDING`:
        case `${types.CREATE_DIADEMA}_PENDING`:
        case `${types.UPDATE_DIADEMA}_PENDING`:
        case `${types.DELETE_DIADEMA}_PENDING`:
            return {
                ...state,
                loading: true
            };

        case `${types.FETCH_DIADEMA_COORDINATES}_PENDING`:
            return {
                ...state,
                loadingCoordinates: true
            };

        case `${types.FETCH_DIADEMA_COORDINATES}_FULFILLED`:
            return {
                ...state,
                loadingCoordinates: false,
                coordinates: action.payload.data.data
            };

        case `${types.FETCH_DIADEMA_COORDINATES}_FULFILLED`:
            return {
                ...state,
                loadingCoordinates: false,
                coordinates: action.payload.data.data
            };

        case `${types.FETCH_DIADEMAS}_REJECTED`:
        case `${types.FETCH_DIADEMA}_REJECTED`:
        case `${types.CREATE_DIADEMA}_REJECTED`:
        case `${types.UPDATE_DIADEMA}_REJECTED`:
        case `${types.DELETE_DIADEMA}_REJECTED`:
            return {
                ...state,
                loading: false,
            };

        case `${types.FETCH_DIADEMAS}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: action.payload.data.data,
                meta: action.payload.data.meta
            };

        case `${types.FETCH_DIADEMA}_FULFILLED`:
            return {
                ...state,
                loading: false,
                current: {
                    ...action.payload.data.data
                }
            };

        case `${types.DELETE_DIADEMA}_FULFILLED`:
            return {
                ...state,
                loading: false,
                data: state.data.filter(record => record.id !== action.meta.id)
            };

        case `${types.CREATE_DIADEMA}_FULFILLED`:
            return {
                ...state,
                data: [action.payload.data.data, ...state.data]
            };

        case `${types.UPDATE_DIADEMA}_FULFILLED`:
            return {
                ...state,
                data: state.data.map((record) =>
                    record.id === action.payload.data.data.id
                        ? action.payload.data.data
                        : record
                )
            };



        case types.SET_CURRENT_DIADEMA:
            return {
                ...state,
                loading: false,
                current: {
                    ...action.data
                }
            };

        case types.EXPORT_DIADEMAS:
            return {
                ...state,
            };

        case types.RESET_CURRENT_DIADEMA:
            return {
                ...state,
                current: {}
            };

        default:
            return state;
    }
};
