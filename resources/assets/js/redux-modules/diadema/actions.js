import { types } from "./types";
import api from "api/diadema";
import axios from "axios";
import { stringify } from "query-string";
import { download } from "helpers";

export const fetchDiademas = (page = 1, filters = {}) => ({
    type: types.FETCH_DIADEMAS,
    payload: api.fetchDiademas(page, filters),
    meta: { globalError: true }
});

export const fetchDiademaCoordinates = (filters = {}) => ({
    type: types.FETCH_DIADEMA_COORDINATES,
    payload: api.fetchDiademaCoordinates(filters),
    meta: { globalError: true }
});

export const fetchDiadema = id => ({
    type: types.FETCH_DIADEMA,
    payload: api.fetchDiadema(id),
    meta: { id, globalError: true }
});

export const deleteDiadema = id => ({
    type: types.DELETE_DIADEMA,
    payload: api.deleteDiadema(id),
    meta: { id, globalError: true }
});

export const createDiadema = data => ({
    type: types.CREATE_DIADEMA,
    payload: api.createDiadema(data),
    meta: { globalError: true }
});

export const updateDiadema = (id, data) => ({
    type: types.UPDATE_DIADEMA,
    payload: api.updateDiadema(id, data),
    meta: { globalError: true }
});

export const setCurrentDiadema = data => ({
    type: types.SET_CURRENT_DIADEMA,
    data
});

export const resetCurrentDiadema = () => ({
    type: types.RESET_CURRENT_DIADEMA
});

export const exportDiadema = (filters = {}) => ({
    type: types.EXPORT_DIADEMAS,
    payload: axios({
        url: `${window.location.origin}/api/export/csv/diadema?${stringify(filters, {
            arrayFormat: "index"
        })}`,
        method: "GET",
        responseType: "blob",
    }).then(
        response => {
            download(response, ' diadema.xlsx')
        },
        error => {
            return error.data;
        }
    ),
    meta: { globalError: true }
});
