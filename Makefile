build:
	docker compose build --no-cache --force-rm
stop:
	docker compose stop
up:
	docker compose up
deps:
	docker compose exec php composer install
	docker compose run --rm node npm install