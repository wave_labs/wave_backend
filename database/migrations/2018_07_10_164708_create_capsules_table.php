<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapsulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capsules', function (Blueprint $table) {
            $table->increments('id');          
            $table->integer('user_id')->unsigned();
            $table->integer('temperature_c')->nullable();
            $table->integer('ram_mb');         
            $table->integer('disk_space_gb');
            $table->integer('volts');
            $table->integer('current');
            $table->integer('power')->nullable();
            $table->string('image_link')->nullable();
            $table->decimal('latitude', 10, 8); 
            $table->decimal('longitude', 10, 8);
            $table->text('description');
            $table->boolean('assigned')->default(false);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capsules');
    }
}
