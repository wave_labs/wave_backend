<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectOriginalNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_original_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('specific_id')->unsigned()->nullable();
            $table->integer('infra_specific_id')->unsigned()->nullable();
            $table->string('originalNameGenus')->nullable();
            $table->string('originalNameSubGenus')->nullable();
            $table->string('originalNameSpecificEpithet')->nullable();
            $table->string('originalNameInfraSpecificEpithet')->nullable();
            $table->date('originalNameDate')->nullable();
            $table->integer('originalNameReference_id')->unsigned()->nullable();
            $table->string('originalNameTaxonRank')->nullable();
            $table->string('originalNameTaxonRemarks')->nullable();
            $table->string('originalNameLanguage')->nullable();
            $table->string('originalNameRights')->nullable();
            $table->string('originalNameLicense')->nullable();
            $table->string('originalNameInstitutionID')->nullable();
            $table->string('originalNameInstitutionCode')->nullable();
            $table->string('originalNameRightSholder')->nullable();
            $table->string('originalNameAccessRights')->nullable();
            $table->string('originalNameSource')->nullable();
            $table->string('originalNameDatasetID')->nullable();
            $table->string('originalNameDatasetName')->nullable();

            $table->timestamps();

            $table->foreign('originalNameReference_id')->references('id')->on('insect_references')->onDelete('set null');

            $table->foreign('specific_id')->references('id')->on('insect_specifics')->onDelete('set null');
            $table->foreign('infra_specific_id')->references('id')->on('insect_infra_specifics')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_original_names');
    }
}
