<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dive_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dive_id')->unsigned();
            $table->string('url');
            $table->timestamps();

            $table->foreign('dive_id')->references('id')->on('dives')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dive__photos');
    }
}
