<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectTribesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_tribes', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('tribe');
            $table->integer('sub_family_id')->unsigned();
            $table->integer('reference_id')->unsigned()->nullable();
            $table->string('taxonRank')->nullable();
            $table->string('taxonRemarks')->nullable();
            $table->string('language')->nullable();
            $table->string('rights')->nullable();
            $table->string('license')->nullable();
            $table->string('institutionID')->nullable();
            $table->string('institutionCode')->nullable();
            $table->string('rightSholder')->nullable();
            $table->string('accessRights')->nullable();
            $table->string('source')->nullable();
            $table->string('datasetID')->nullable();    
            $table->string('datasetName')->nullable();
            $table->timestamps();

            $table->foreign('reference_id')->references('id')->on('insect_references')->onDelete('set null');

            $table->foreign('sub_family_id')->references('id')->on('insect_sub_families')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_tribes');
    }
}
