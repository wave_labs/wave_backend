<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectGenusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_genuses', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('genus');
            $table->integer('family_id')->unsigned(); //mandotory to have
            $table->integer('sub_family_id')->unsigned()->nullable(); //optional connection
            $table->integer('tribe_id')->unsigned()->nullable(); //optional connection
            $table->integer('reference_id')->unsigned()->nullable();
            $table->string('taxonRank')->nullable();
            $table->string('taxonRemarks')->nullable();
            $table->string('language')->nullable();
            $table->string('rights')->nullable();
            $table->string('license')->nullable();
            $table->string('institutionID')->nullable();
            $table->string('institutionCode')->nullable();
            $table->string('rightSholder')->nullable();
            $table->string('accessRights')->nullable();
            $table->string('source')->nullable();
            $table->string('datasetID')->nullable();
            $table->string('datasetName')->nullable();
            $table->timestamps();

            $table->foreign('reference_id')->references('id')->on('insect_references')->onDelete('set null');

            $table->foreign('family_id')->references('id')->on('insect_families')->onDelete('cascade');
            $table->foreign('sub_family_id')->references('id')->on('insect_sub_families')->onDelete('set null');
            $table->foreign('tribe_id')->references('id')->on('insect_tribes')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_genuses');
    }
}
