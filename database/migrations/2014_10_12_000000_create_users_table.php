<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userable_type');
            $table->integer('userable_id')->unsigned()->nullable();
            $table->integer('user_occupation_id')->unsigned()->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('social_id')->nullable();
            $table->string('social')->nullable();
            $table->text('note')->nullable();
            $table->boolean('active')->default(false);
            $table->string('token', 16)->unique();
            $table->rememberToken();
            $table->integer('points')->default(0);
            $table->integer('level')->default(1);
            $table->timestamps();

            $table->foreign('user_occupation_id')->references('id')->on('user_occupations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
