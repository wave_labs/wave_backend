<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStopwatchTimestampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stopwatch_timestamps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('class_id')->unsigned();
            $table->integer('result_id')->unsigned();
            $table->decimal('start_time', 5, 2);
            $table->decimal('end_time', 5, 2);
            $table->timestamps();

            $table->foreign('result_id')->references('id')->on('stopwatch_results')->onDelete('cascade');
            $table->foreign('class_id')->references('id')->on('stopwatch_classes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stopwatch_timestamps');
    }
}
