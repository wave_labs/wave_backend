<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectOccurrenceIdentificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_occurrence_identifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('occurrence_id')->unsigned();
            $table->integer('author_id')->unsigned()->nullable();
            $table->string('authorExperience')->nullable();
            $table->integer('species_id')->unsigned()->nullable();
            $table->integer('sub_species_id')->unsigned()->nullable();
            $table->date('dateIdentified')->nullable();
            $table->integer('creator_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('occurrence_id')->references('id')->on('insect_occurrences')->onDelete('cascade');
            $table->foreign('author_id')->references('id')->on('insect_authors')->onDelete('set null');
            $table->foreign('species_id')->references('id')->on('insect_specifics')->onDelete('set null');
            $table->foreign('sub_species_id')->references('id')->on('insect_infra_specifics')->onDelete('set null');
            $table->foreign('creator_id')->references('id')->on('wave.users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_occurrence_identifications');
    }
}
