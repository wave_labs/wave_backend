<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAiDetectionImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ai_detection_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ai_detection_id')->unsigned();
            $table->string('image');
            $table->timestamps();

            $table->foreign('ai_detection_id')->references('id')->on('ai_detections')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ai_detection_images');
    }
}
