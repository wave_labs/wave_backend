<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectOccurrencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_occurrences', function (Blueprint $table) {
            $table->increments('id');

            //event
            $table->string('basisOfRecord')->nullable();
            $table->string('dateType')->nullable();
            $table->date('eventDateStart')->nullable();
            $table->date('eventDateEnd')->nullable();
            $table->integer('endDayOfYear')->nullable();
            $table->string('verbatimEventDate')->nullable();
            $table->string('eventRemarks')->nullable();
            $table->string('occurrenceRemarks')->nullable();

            //specie and reference
            $table->integer('specific_id')->unsigned()->nullable();
            $table->integer('infra_specific_id')->unsigned()->nullable();
            $table->integer('reference_id')->unsigned()->nullable();

            //identification
            $table->date('dateIdentified')->nullable();
            $table->string('identificationRemarks')->nullable();
            $table->string('previousIdentifications')->nullable();

            //location/geography
            $table->string('decimalLatitude')->nullable();
            $table->string('decimalLongitude')->nullable();
            $table->string('geodeticDatum')->nullable();
            $table->string('coordinateUncertaintyInMeters')->nullable();
            $table->string('verbatimCoordinates')->nullable();
            $table->string('verbatimCoordinateSystem')->nullable();
            $table->string('georeferencedBy')->nullable();
            $table->date('georeferencedDate')->nullable();
            $table->string('georeferenceProtocol')->nullable();
            $table->string('georeferenceSources')->nullable();
            $table->string('georeferenceRemarks')->nullable();
            $table->string('georeferenceVerificationStatus')->nullable();
            $table->string('higherGeography')->nullable();
            $table->string('continent')->nullable();
            $table->string('islandGroup')->nullable();
            $table->string('island')->nullable();
            $table->string('biogeographicRealm')->nullable();
            $table->string('country')->nullable();
            $table->string('countryCode')->nullable();
            $table->string('region')->nullable();
            $table->string('province')->nullable();
            $table->string('county')->nullable();
            $table->string('geographicRemarks')->nullable();
            $table->string('locality')->nullable();
            $table->string('verbatimLocality')->nullable();
            $table->string('locationAccordingTo')->nullable();
            $table->string('localityRemarks')->nullable();
            $table->string('elevationMin')->nullable();
            $table->string('elevationMax')->nullable();
            $table->string('elevationMethod')->nullable();
            $table->string('elevationAccuracy')->nullable();
            $table->string('verbatimElevation')->nullable();
            $table->string('type')->nullable();
            $table->string('numbers')->nullable();
            $table->string('machineSpecifics')->nullable();
            $table->string('recordNumber')->nullable();
            $table->string('language')->nullable();
            $table->string('license')->nullable();
            $table->string('institutionId')->nullable();
            $table->string('institutionCode')->nullable();
            $table->string('collectionCode')->nullable();
            $table->string('catalogNumber')->nullable();
            $table->string('individualCount')->nullable();
            $table->string('maleCount')->nullable();
            $table->string('femaleCount')->nullable();
            $table->string('imatureCount')->nullable();
            $table->string('exuvias')->nullable();
            $table->string('organismQuantity')->nullable();
            $table->string('organismQuantityType')->nullable();
            $table->string('establishmentMeans')->nullable();
            $table->string('preparations')->nullable();
            $table->string('preparationType')->nullable();
            $table->string('preparedBy')->nullable();
            $table->string('preparationDate')->nullable();
            $table->string('preparationRemarks')->nullable();

            $table->string('habitat')->nullable();
            $table->string('habitatRemarks')->nullable();
            $table->string('microHabitats')->nullable();
            $table->string('biologicalAssociation')->nullable();
            $table->string('biologicalRemarks')->nullable();
            $table->string('samplingMethod')->nullable();
            $table->string('typeOfSampling')->nullable();
            $table->string('tripCode')->nullable();
            $table->string('dataAttributes')->nullable();
            $table->string('materialType')->nullable();
            $table->string('cataloguedBy')->nullable();
            $table->string('dataValidation')->nullable();
            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('identifiedBy')->unsigned()->nullable();
            $table->string('authorExperience')->nullable();
            $table->integer('last_edit_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('reference_id')->references('id')->on('insect_references')->onDelete('set null');
            $table->foreign('specific_id')->references('id')->on('insect_specifics')->onDelete('set null');
            $table->foreign('infra_specific_id')->references('id')->on('insect_infra_specifics')->onDelete('set null');
            $table->foreign('project_id')->references('id')->on('insect_projects')->onDelete('set null');
            $table->foreign('identifiedBy')->references('id')->on('insect_authors')->onDelete('set null');
            $table->foreign('last_edit_by')->references('id')->on('wave.users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_occurrences');
    }
}
