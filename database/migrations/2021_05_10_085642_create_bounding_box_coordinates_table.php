<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoundingBoxCoordinatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bounding_box_coordinates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('class_id')->unsigned();
            $table->integer('result_id')->unsigned();
            $table->integer('y_min');
            $table->integer('y_max');
            $table->integer('x_min');
            $table->integer('x_max');
            $table->timestamps();

            $table->foreign('result_id')->references('id')->on('bounding_box_results')->onDelete('cascade');
            $table->foreign('class_id')->references('id')->on('bounding_box_classes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bounding_box_coordinates');
    }
}
