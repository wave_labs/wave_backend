<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArDomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ar_domes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->decimal('latitude', 10, 7);
            $table->decimal('longitude', 10, 7);
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('dome_id')->unsigned();
            $table->string('score')->default(0);
            $table->decimal('timespent', 4, 1)->nullable();
            $table->boolean('has_dome')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('dome_id')->references('id')->on('domes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ar_domes');
    }
}
