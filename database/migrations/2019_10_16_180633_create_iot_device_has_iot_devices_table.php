<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIotDeviceHasIotDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_iot')->create('iot_device_has_iot_devices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iot_device_id')->unsigned();
            $table->integer('gateway_id')->unsigned();
            $table->timestamps();

            $table->foreign('iot_device_id')->references('id')->on('iot_device')->onDelete('cascade');
            $table->foreign('gateway_id')->references('id')->on('iot_device')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_iot')->dropIfExists('iot_device_has_iot_devices');
    }
}
