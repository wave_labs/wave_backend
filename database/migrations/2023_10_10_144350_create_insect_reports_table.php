<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('coordinates')->nullable();
            $table->integer('n_male')->nullable();
            $table->integer('n_female')->nullable();
            $table->integer('n_children')->nullable();
            $table->string('page')->nullable();
            $table->integer('zone_id')->unsigned();
            $table->integer('species_id')->unsigned();
            $table->integer('sinonimia_id')->unsigned()->nullable();
            $table->integer('reference_id')->unsigned();
            $table->string('collector')->default("unknown");
            $table->string('date_type')->nullable();
            $table->date('date')->nullable();
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->timestamps();

            //$table->foreign('user_id')->references('id')->on('wave.users')->onDelete('set null');
            $table->foreign('zone_id')->references('id')->on('insect_zones')->onDelete('cascade');
            $table->foreign('species_id')->references('id')->on('insect_species')->onDelete('cascade');
            $table->foreign('sinonimia_id')->references('id')->on('insect_sinonimias')->onDelete('set null');
            $table->foreign('reference_id')->references('id')->on('insect_references')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_insect')->dropIfExists('insect_reports');
    }
}
