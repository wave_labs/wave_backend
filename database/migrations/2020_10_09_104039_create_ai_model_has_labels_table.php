<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAiModelHasLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ai_model_has_labels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ai_label_id')->unsigned();
            $table->integer('ai_model_id')->unsigned();
            $table->timestamps();

            $table->foreign('ai_model_id')->references('id')->on('ai_models')->onDelete('cascade');
            $table->foreign('ai_label_id')->references('id')->on('ai_labels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ai_model_has_labels');
    }
}
