<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiveHasLittersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dive_has_litters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('litter_id')->unsigned();
            $table->integer('dive_id')->unsigned();
            $table->timestamps();

            $table->foreign('litter_id')->references('id')->on('litters')->onDelete('cascade');
            $table->foreign('dive_id')->references('id')->on('dives')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dive_has_litters');
    }
}
