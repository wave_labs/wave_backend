<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectSpeciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_species', function (Blueprint $table) { //valid name for insects
            $table->increments('id');
            $table->string('kingdom')->nullable();
            $table->string('phylum')->nullable();
            $table->string('class')->nullable();
            $table->string('order')->nullable();
            $table->string('subOrder')->nullable(); //
            $table->string('infraOrder')->nullable();
            $table->string('superFamily')->nullable(); //
            $table->string('family')->nullable();
            $table->string('subFamily')->nullable();
            $table->string('tribe')->nullable();

            $table->string('genus')->nullable();
            $table->string('subgenus')->nullable();
            $table->string('specificEpithet')->nullable();
            $table->string('infraSpecificEpithet')->nullable();
            $table->string('author')->nullable();
            $table->date('year')->nullable();
            $table->string('reference')->nullable();
            $table->string('taxonRank')->nullable();
            $table->string('taxonomicStatus')->nullable();
            $table->string('nomenclatureStatus')->nullable();
            $table->string('taxonRemarks')->nullable();
            $table->string('language')->nullable();
            $table->string('rights')->nullable();
            $table->string('license')->nullable();
            $table->string('institutionID')->nullable();
            $table->string('institutionCode')->nullable();
            $table->string('rightSholder')->nullable();
            $table->string('accessRights')->nullable();
            $table->string('source')->nullable();
            $table->string('datasetID')->nullable();
            $table->string('datasetName')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_insect')->dropIfExists('insect_species');
    }
}
