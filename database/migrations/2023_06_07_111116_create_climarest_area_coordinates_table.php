<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClimarestAreaCoordinatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('climarest_area_coordinates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('climarest_area_form_id')->unsigned();
            $table->decimal('latitude', 10, 7);
            $table->decimal('longitude', 10, 7);
            $table->text('label');
            $table->timestamps();

            $table->foreign('climarest_area_form_id')->references('id')->on('climarest_area_forms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('climarest_area_coordinates');
    }
}
