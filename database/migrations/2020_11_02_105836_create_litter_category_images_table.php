<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLitterCategoryImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('litter_category_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('litter_category_image_type_id')->unsigned();
            $table->integer('litter_category_id')->unsigned()->nullable();
            $table->string('url');
            $table->timestamps();

            $table->foreign('litter_category_image_type_id')->references('id')->on('litter_category_image_types')->onDelete('cascade');
            $table->foreign('litter_category_id')->references('id')->on('litter_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('litter_category_images');
    }
}
