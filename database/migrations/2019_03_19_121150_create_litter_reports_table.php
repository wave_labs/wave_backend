<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLitterReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('litter_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('litter_id')->unsigned();
            $table->string('reason');
            $table->timestamps();

            $table->foreign('litter_id')->references('id')->on('litters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('litter_reports');
    }
}
