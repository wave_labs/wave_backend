<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomePositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dome_positions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dome_id')->unsigned();
            $table->decimal('latitude', 8, 5)->nullable();
            $table->decimal('longitude', 8, 5)->nullable();
            $table->date('from')->useCurrent();
            $table->date('to')->nullable();
            $table->timestamps();

            $table->foreign('dome_id')->references('id')->on('domes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dome_positions');
    }
}
