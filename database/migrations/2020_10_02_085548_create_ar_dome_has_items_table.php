<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArDomeHasItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ar_dome_has_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ar_dome_id')->unsigned();
            $table->integer('dome_item_id')->unsigned();
            $table->timestamps();

            $table->foreign('ar_dome_id')->references('id')->on('ar_domes')->onDelete('cascade');
            $table->foreign('dome_item_id')->references('id')->on('dome_items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ar_dome_has_items');
    }
}
