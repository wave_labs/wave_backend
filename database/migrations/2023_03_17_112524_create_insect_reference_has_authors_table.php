<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectReferenceHasAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_reference_has_authors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id')->unsigned();
            $table->integer('reference_id')->unsigned();
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('insect_authors')->onDelete('cascade');
            $table->foreign('reference_id')->references('id')->on('insect_references')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_insect')->dropIfExists('insect_reference_has_authors');
    }
}
