<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creatures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id')->unsigned();
            $table->string('name');
            $table->string('name_scientific');
            $table->text('description', 400)->nullable();
            $table->text('curiosity', 400)->nullable();
            $table->enum('conserv', ['Extinct', 'Extinct in the Wild', 'Critical Endangered', 'Endangered', 'Vulnerable', 'Near Threatened', 'Least Concern', 'Data Deficient', 'Not Evaluated']);
            $table->text('size')->nullable();
            $table->string('group_size')->nullable();
            $table->string('calves')->nullable();
            $table->enum('nis_status', ['Native', 'Non-Indigenous Species']);
            $table->string('show_order')->nullable();
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('creature_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creatures');
    }
}
