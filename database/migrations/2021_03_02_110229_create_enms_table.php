<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('referenceId')->unique();
            $table->integer('creature_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('enm_ocurrence_data_source_id')->unsigned()->nullable();
            $table->integer('enm_environment_data_source_id')->unsigned()->nullable();
            $table->string('status')->default("pending");
            $table->string('environmentalDataPath')->nullable();
            $table->timestamps();

            $table->foreign('enm_ocurrence_data_source_id')->references('id')->on('enm_ocurrence_data_sources')->onDelete('set null');
            $table->foreign('enm_environment_data_source_id')->references('id')->on('enm_environment_data_sources')->onDelete('set null');
            $table->foreign('creature_id')->references('id')->on('creatures')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enms');
    }
}
