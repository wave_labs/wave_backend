<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnmHasModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enm_has_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enm_model_id')->unsigned();
            $table->integer('enm_id')->unsigned();
            $table->timestamps();

            $table->foreign('enm_model_id')->references('id')->on('enm_models')->onDelete('cascade');
            $table->foreign('enm_id')->references('id')->on('enms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enm_has_models');
    }
}
