<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLitterCategoryPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('litter_category_pivot', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('litter_id')->unsigned();
            $table->integer('litter_category_id')->unsigned();
            $table->timestamps();

            $table->foreign('litter_id')->references('id')->on('litters')->onDelete('cascade');
            $table->foreign('litter_category_id')->references('id')->on('litter_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('litter_category_pivot');
    }
}
