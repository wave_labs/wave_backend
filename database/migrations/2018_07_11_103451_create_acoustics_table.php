<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcousticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acoustics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sightings_data_id')->unsigned();
            $table->enum('acoustic_type', ['Click', 'Moan', 'Whistle']);            
            $table->integer('duration_s');
            $table->timestamps();

            $table->foreign('sightings_data_id')->references('id')->on('sightings_data')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acoustics');
    }
}
