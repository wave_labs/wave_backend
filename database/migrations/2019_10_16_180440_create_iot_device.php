<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIotDevice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_iot')->create('iot_device', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('serial_number',191)->unique();
            $table->integer('id_type')->unsigned();
            $table->integer('id_environment')->unsigned();
            $table->integer('id_state')->unsigned();
            $table->integer('template_id')->unsigned();
            $table->string('image')->default('storage/uploaded/photo/iot_device/placeholder.png');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_type')->references('id')->on('iot_device_type')->onDelete('cascade');
            $table->foreign('id_environment')->references('id')->on('iot_environment_type');
            $table->foreign('id_state')->references('id')->on('iot_device_state');
            $table->foreign('template_id')->references('id')->on('iot_templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_iot')->dropIfExists('iot_device');
    }
}
