<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoundingBoxImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bounding_box_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('filename');
            $table->integer('category_id')->unsigned();
            $table->integer('width');
            $table->integer('height');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('bounding_box_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bounding_box_images');
    }
}
