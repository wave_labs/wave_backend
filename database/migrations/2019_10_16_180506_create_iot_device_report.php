<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIotDeviceReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_iot')->create('iot_device_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_device')->unsigned();
            $table->timestamp('collected_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_device')->references('id')->on('iot_device')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_iot')->dropIfExists('iot_device_report');
    }
}
