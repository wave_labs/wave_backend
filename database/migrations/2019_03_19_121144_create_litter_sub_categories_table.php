<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLitterSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('litter_sub_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('litter_category_id')->unsigned();
            $table->text('name');
            $table->string('OSPAR_code')->nullable();
            $table->string('UNEP_code')->nullable();  
            $table->boolean('Core')->default(0);
            $table->boolean('Beach')->default(0);
            $table->boolean('Seafloor')->default(0);
            $table->boolean('Floating')->default(0);
            $table->boolean('Biota')->default(0);
            $table->boolean('Micro')->default(0);
            $table->timestamps();

            $table->foreign('litter_category_id')->references('id')->on('litter_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('litter_sub_categories');
    }
}
