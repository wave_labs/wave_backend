<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTroutCoordinatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trout_coordinates', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('latitude', 10, 7);
            $table->decimal('longitude', 10, 7);
            $table->integer('trout_id')->unsigned();
            $table->string('animal');
            $table->string('stream');
            $table->string('year');
            $table->string('location');
            $table->timestamps();

            $table->foreign('trout_id')->references('id')->on('trout')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trout_coordinates');
    }
}
