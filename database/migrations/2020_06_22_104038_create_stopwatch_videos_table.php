<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStopwatchVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stopwatch_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('video');
            $table->string('thumbnail');
            $table->integer('category_id')->unsigned();
            $table->boolean('detection')->default(false);
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('stopwatch_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stopwatch_videos');
    }
}
