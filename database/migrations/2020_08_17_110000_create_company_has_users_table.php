<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyHasUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_has_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_person_id')->unsigned();
            $table->integer('user_dive_center_id')->unsigned();
            $table->timestamps();


            $table->foreign('user_person_id')->references('id')->on('user_persons')->onDelete('cascade');
            $table->foreign('user_dive_center_id')->references('id')->on('user_dive_centers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_has_users');
    }
}
