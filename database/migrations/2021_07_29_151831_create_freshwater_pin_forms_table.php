<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreshwaterPinFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freshwater_pin_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->text('answer');
            $table->integer('study_question_id')->unsigned();
            $table->integer('freshwater_pin_id')->unsigned();
            $table->timestamps();

            $table->foreign('study_question_id')->references('id')->on('study_questions')->onDelete('cascade');
            $table->foreign('freshwater_pin_id')->references('id')->on('freshwater_pins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freshwater_pin_forms');
    }
}
