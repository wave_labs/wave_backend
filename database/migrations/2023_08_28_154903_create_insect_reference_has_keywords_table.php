<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectReferenceHasKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_reference_has_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reference_id')->unsigned();
            $table->integer('reference_keyword_id')->unsigned();
            $table->timestamps();

            $table->foreign('reference_id')->references('id')->on('insect_references')->onDelete('cascade');
            $table->foreign('reference_keyword_id')->references('id')->on('insect_reference_keywords')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_reference_has_keywords');
    }
}
