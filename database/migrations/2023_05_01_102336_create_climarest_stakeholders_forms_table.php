<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClimarestStakeholdersFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('climarest_stakeholders_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('climarest_id')->unsigned();
            $table->text('category');
            $table->text('type');
            $table->text('name');
            $table->text('role');
            $table->text('typeEngagement');
            $table->text('affect');
            $table->text('affectRestoration');
            $table->text('levelInterest');
            $table->text('levelInfluence');
            $table->text('levelTrust');
            $table->text('priority');
            $table->text('engage');
            $table->timestamps();

            $table->foreign('climarest_id')->references('id')->on('climarests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('climarest_stakeholders_forms');
    }
}
