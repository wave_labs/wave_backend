<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkerResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marker_results', function (Blueprint $table) {
            $table->increments('id');
            $table->text('answer');
            $table->integer('study_question_id')->unsigned();
            $table->integer('marker_id')->unsigned();
            $table->timestamps();

            $table->foreign('study_question_id')->references('id')->on('study_questions')->onDelete('cascade');
            $table->foreign('marker_id')->references('id')->on('markers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marker_results');
    }
}
