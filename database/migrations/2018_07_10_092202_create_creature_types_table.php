<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreatureTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creature_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('taxa_category');
            $table->string('image')->unique();
            $table->boolean('acoustic_data')->default(false);
            $table->boolean('acoustic_category')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creature_types');
    }
}
