<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSightingsEstimationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sightings_estimation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creature_id')->unsigned();
            $table->integer('sighting_data_id')->unsigned();
            $table->integer('confidence');
            $table->timestamps();

            $table->foreign('sighting_data_id')->references('id')->on('sightings_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sightings_estimation');
    }
}
