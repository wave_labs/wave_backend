<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreshwaterPinCoordinatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freshwater_pin_coordinates', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('latitude', 10, 7);
            $table->decimal('longitude', 10, 7);
            $table->integer('freshwater_pin_id')->unsigned();
            $table->string('timeframe');
            $table->string('image');
            $table->timestamps();

            $table->foreign('freshwater_pin_id')->references('id')->on('freshwater_pins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freshwater_pin_coordinates');
    }
}
