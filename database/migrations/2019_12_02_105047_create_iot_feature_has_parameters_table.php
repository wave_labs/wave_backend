<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIotFeatureHasParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_iot')->create('iot_feature_has_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parameter_id')->unsigned();
            $table->integer('feature_id')->unsigned();
            $table->timestamps();

            $table->foreign('parameter_id')->references('id')->on('iot_parameters')->onDelete('cascade');
            $table->foreign('feature_id')->references('id')->on('iot_features')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_iot')->dropIfExists('iot_feature_has_parameters');
    }
}
