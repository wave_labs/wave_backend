<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWhatsNewMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whats_new_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('whats_new_type_id')->unsigned();
            $table->text('message');
            $table->timestamps();

            $table->foreign('whats_new_type_id')->references('id')->on('whats_new_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whats_new_messages');
    }
}
