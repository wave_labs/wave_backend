<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIotFieldFactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_iot')->create('iot_field_facts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feature_id')->unsigned();
            $table->integer('field_id')->unsigned();
            $table->integer('section_id')->unsigned();
            $table->timestamps();

            $table->foreign('section_id')->references('id')->on('iot_sections')->onDelete("cascade");
            $table->foreign('feature_id')->references('id')->on('iot_features')->onDelete("cascade");
            $table->foreign('field_id')->references('id')->on('iot_device_fields')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_iot')->dropIfExists('iot_field_facts');
    }
}
