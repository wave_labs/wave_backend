<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_references', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('identifier')->nullable();
            $table->string('description')->nullable();
            $table->string('bibliographicCitation')->nullable();
            $table->string('editorList')->nullable();
            $table->date('date')->nullable();
            $table->string('journal')->nullable();
            $table->string('chapterTitle')->nullable();
            $table->string('editor')->nullable();
            $table->integer('volume')->nullable();
            $table->string('pages')->nullable();
            $table->integer('pageStart')->nullable();
            $table->integer('pageEnd')->nullable();
            $table->string('edition')->nullable();
            $table->string('publisher')->nullable();
            $table->string('language')->nullable();
            $table->string('subject')->nullable();
            $table->string('taxonRemarks')->nullable();
            $table->string('type')->nullable();
            $table->string('validated')->nullable();
            $table->string('datasetName')->nullable();
            $table->string('datasetID')->nullable();
            $table->string('modified')->nullable();
            $table->string('institutionID')->nullable();
            $table->string('collectionID')->nullable();
            $table->string('institutionCode')->nullable();
            $table->string('collectionCode')->nullable();
            $table->string('ownerInstitutionCode')->nullable();
            $table->string('issue')->nullable();

            $table->string('degree')->nullable();
            $table->string('university')->nullable();
            $table->string('conferenceTittle')->nullable();
            $table->date('accessDate')->nullable();

            $table->integer('project_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('insect_projects')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_insect')->dropIfExists('insect_references');
    }
}
