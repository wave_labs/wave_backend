<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrainwaveReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_brainwave')->create('brainwave_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file');
            $table->string('annotations')->default('file_all_subtypes.csv');
            $table->string('og_values')->default('og_values.csv');
            $table->integer('user_id')->unsigned()->nullable();
            $table->datetime('finished')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('wave.users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_brainwave')->dropIfExists('brainwave_reports');
    }
}
