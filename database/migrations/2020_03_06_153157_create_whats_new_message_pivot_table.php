<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWhatsNewMessagePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whats_new_message_pivot', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('whats_new_message_id')->unsigned()->index();
            $table->integer('whats_new_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('whats_new_message_id')->references('id')->on('whats_new_messages')->onDelete('cascade');
            $table->foreign('whats_new_id')->references('id')->on('whats_news')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whats_new_message_pivot');
    }
}
