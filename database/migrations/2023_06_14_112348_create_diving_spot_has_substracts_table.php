<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivingSpotHasSubstractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diving_spot_has_substracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('diving_spot_id')->unsigned();
            $table->integer('diving_spot_substract_id')->unsigned();
            $table->timestamps();

            $table->foreign('diving_spot_id')->references('id')->on('diving_spots')->onDelete('cascade');
            $table->foreign('diving_spot_substract_id')->references('id')->on('diving_spot_substracts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diving_spot_has_substracts');
    }
}
