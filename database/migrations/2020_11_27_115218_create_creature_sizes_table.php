<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreatureSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creature_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creature_id')->unsigned()->unique();
            $table->string('unit');
            $table->string('sm');
            $table->string('lg');
            $table->timestamps();

            $table->foreign('creature_id')->references('id')->on('creatures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creature_sizes');
    }
}
