<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClimarestAreaFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('climarest_area_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('climarest_id')->unsigned();
            $table->text('typeArea');
            $table->text('restorationArea');
            $table->text('distanceShore');
            $table->text('ownership');
            $table->text('adjacentOwnership');
            $table->text('marineRights');
            $table->text('resultRestrictions');
            $table->text('areaActivities');
            $table->text('activitiesRestrictions');
            $table->text('technologicSolutions');
            $table->text('natureSolutions');
            $table->text('climateVulnerability');
            $table->text('conflicts');
            $table->text('contreversialMethods');
            $table->text('name');
            $table->timestamps();

            $table->foreign('climarest_id')->references('id')->on('climarests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('climarest_area_forms');
    }
}
