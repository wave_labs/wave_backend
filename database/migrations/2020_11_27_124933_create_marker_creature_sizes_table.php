<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkerCreatureSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marker_creature_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marker_id')->unsigned()->unique();
            $table->string('sm');
            $table->string('md');
            $table->string('lg');
            $table->timestamps();

            $table->foreign('marker_id')->references('id')->on('markers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marker_creature_sizes');
    }
}
