<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desertas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('img_airport');
            $table->string('img_sky');
            $table->string('x_acc');
            $table->string('y_acc');
            $table->string('z_acc');
            $table->string('x_gyro');
            $table->string('y_gyro');
            $table->string('z_gyro');
            $table->string('temperature');
            $table->string('humidity');
            $table->string('timestamp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desertas');
    }
}
