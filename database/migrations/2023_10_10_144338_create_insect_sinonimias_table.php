<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectSinonimiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_sinonimias', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('specific_id')->unsigned()->nullable();
            $table->integer('infra_specific_id')->unsigned()->nullable();

            $table->string('genus')->nullable();
            $table->string('subgenus')->nullable();
            $table->string('specificEpithet')->nullable();
            $table->string('infraSpecificEpithet')->nullable();
            $table->integer('reference_id')->unsigned()->nullable(); //change to get the reference
            $table->string('taxonRank')->nullable();
            $table->string('taxonomicStatus')->nullable();
            $table->string('nomenclatureStatus')->nullable();
            $table->string('taxonRemarks')->nullable();
            $table->string('language')->nullable();
            $table->string('rights')->nullable();
            $table->string('license')->nullable();
            $table->string('institutionID')->nullable();
            $table->string('institutionCode')->nullable();
            $table->string('rightSholder')->nullable();
            $table->string('accessRights')->nullable();
            $table->string('source')->nullable();
            $table->string('datasetID')->nullable();
            $table->string('datasetName')->nullable();

            $table->timestamps();

            $table->foreign('specific_id')->references('id')->on('insect_specifics')->onDelete('cascade');
            $table->foreign('infra_specific_id')->references('id')->on('insect_infra_specifics')->onDelete('cascade');
            $table->foreign('reference_id')->references('id')->on('insect_references')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_sinonimias');
    }
}
