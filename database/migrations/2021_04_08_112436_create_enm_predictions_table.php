<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnmPredictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enm_predictions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enm_id')->unsigned();
            $table->integer('enm_model_prediction_id')->unsigned();
            $table->string('path');
            $table->timestamps();

            $table->foreign('enm_id')->references('id')->on('enms')->onDelete('cascade');
            $table->foreign('enm_model_prediction_id')->references('id')->on('enm_model_predictions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enm_predictions');
    }
}
