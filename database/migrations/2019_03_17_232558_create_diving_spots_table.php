<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivingSpotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diving_spots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('longitude');
            $table->string('latitude');
            $table->string('range')->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->text('substract')->nullable();
            $table->enum('protection', ['Marine Protected Area', 'Natural Reserve', 'None']);
            $table->boolean('validated')->default(false);
            $table->boolean('old')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diving_spots');
    }
}
