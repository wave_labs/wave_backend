<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGpsGatewaySatellitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_iot')->create('gps_gateway_satellites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('satID');
            $table->string('gpsWeek');
            $table->string('gpsTimeWeek_M7');
            $table->string('gpsTimeWeek_M41');
            $table->string('clockBias_M7');
            $table->string('clockBias_M30');
            $table->string('pseudorange');
            $table->string('gpsTime');
            $table->string('gpsSoftwareTime');
            $table->string('positionX');
            $table->string('positionY');
            $table->string('positionZ');
            $table->string('velocityX');
            $table->string('velocityY');
            $table->string('velocityZ');
            $table->integer('iot_report_id')->unsigned();
            $table->timestamps();

            $table->foreign('iot_report_id')->references('id')->on('iot_device_report')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gps_gateway_satellites');
    }
}
