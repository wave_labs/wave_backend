<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIotFieldFactHasParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_iot')->create('iot_field_fact_has_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parameter_id')->unsigned();
            $table->integer('field_fact_id')->unsigned();
            $table->string('value')->nullable();
            $table->timestamps();

            $table->foreign('parameter_id')->references('id')->on('iot_parameters')->onDelete('cascade');
            $table->foreign('field_fact_id')->references('id')->on('iot_field_facts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_iot')->dropIfExists('iot_field_fact_has_parameters');
    }
}
