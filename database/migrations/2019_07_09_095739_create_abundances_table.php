<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbundancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abundances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('creature_id')->unsigned();
            $table->enum('level_1', ['1', '0 - 1 m^2', '1 - 3', '1 - 5', '1 - 15']);
            $table->enum('level_2', ['2', '1 - 5 m^2', '4 - 6', '2 - 3', '6 - 10', '16 - 30']);
            $table->enum('level_3', ['3', '5 - 10 m^2', '7 - 10', '4 - 5', '11 - 15', '31 - 50']);
            $table->enum('level_4', ['> 3', '> 10 m^2', '> 10', '> 5', '> 15', '> 50']);
            $table->timestamps();

            $table->foreign('creature_id')->references('id')->on('creatures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abundances');
    }
}
