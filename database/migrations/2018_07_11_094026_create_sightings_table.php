<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSightingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sightings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('capsule_id')->unsigned()->nullable();
            $table->integer('creature_id')->unsigned()->nullable();
            $table->integer('beaufort_scale_id')->unsigned()->nullable();
            $table->integer('behaviour_id')->unsigned()->nullable();
            $table->integer('vehicle_id')->unsigned()->nullable();
            $table->dateTime('date');
            $table->decimal('latitude', 10, 8);
            $table->decimal('longitude', 10, 8);
            $table->boolean('is_group_mixed')->nullable();
            $table->integer('group_size')->nullable();
            $table->integer('time')->nullable();
            $table->enum('report_source', ['land', 'surface', 'aerial', 'underwater'])->nullable();
            $table->integer('calves')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('creature_id')->references('id')->on('creatures')->onDelete('set null');
            $table->foreign('capsule_id')->references('id')->on('capsules')->onDelete('set null');
            $table->foreign('beaufort_scale_id')->references('id')->on('beaufort_scale')->onDelete('set null');
            $table->foreign('behaviour_id')->references('id')->on('sighting_behaviours')->onDelete('set null');
            $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sightings');
    }
}
