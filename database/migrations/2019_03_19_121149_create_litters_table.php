<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLittersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('litters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->decimal('latitude', 10, 7); //100 points
            $table->decimal('longitude', 10, 7); //100 points
            $table->dateTime('date'); //100 points
            $table->enum('quantity', ['1-5', '5-10', '10-50', '>50'])->nullable();
            $table->enum('radius', ['<1', '1-5', '5-20', '>20'])->nullable();
            $table->enum('source', ['core', 'beach', 'seafloor', 'floating', 'micro', 'biota', 'dive']);
            $table->boolean('collected')->default(false);
            $table->boolean('multiple')->default(false);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('litters');
    }
}
