<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_persons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->char('gender', 1)->nullable();
            $table->string('country')->nullable();
            $table->date('b_day')->nullable();
            $table->timestamps();
            $table->enum('number_dives', ['0-10', '11-20', '21-30', '30+'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_persons');
    }
}
