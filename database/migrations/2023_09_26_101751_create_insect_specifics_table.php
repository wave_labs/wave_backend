<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectSpecificsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_specifics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('specific');
            $table->integer('genus_id')->unsigned();
            $table->integer('sub_genus_id')->unsigned()->nullable(); 
            $table->integer('reference_id')->unsigned()->nullable();
            $table->string('taxonRank')->nullable();
            $table->string('taxonRemarks')->nullable();
            $table->string('language')->nullable();
            $table->string('rights')->nullable();
            $table->string('license')->nullable();
            $table->string('institutionID')->nullable();
            $table->string('institutionCode')->nullable();
            $table->string('rightSholder')->nullable();
            $table->string('accessRights')->nullable();
            $table->string('source')->nullable();
            $table->string('datasetID')->nullable();
            $table->string('datasetName')->nullable();
            $table->timestamps();

            $table->foreign('reference_id')->references('id')->on('insect_references')->onDelete('set null');

            $table->foreign('genus_id')->references('id')->on('insect_genuses')->onDelete('cascade');
            $table->foreign('sub_genus_id')->references('id')->on('insect_sub_genuses')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_specifics');
    }
}
