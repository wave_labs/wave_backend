<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCopernicusHasParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('copernicus_has_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('copernicus_product_id')->unsigned();
            $table->integer('copernicus_parameter_id')->unsigned();
            $table->timestamps();

            $table->foreign('copernicus_product_id')->references('id')->on('copernicus_products')->onDelete('cascade');
            $table->foreign('copernicus_parameter_id')->references('id')->on('copernicus_parameters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copernicus_has_parameters');
    }
}
