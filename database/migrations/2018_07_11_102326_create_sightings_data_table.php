<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSightingsDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sightings_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sighting_id')->unsigned();
            $table->string('capsule_link')->nullable();
            $table->string('link')->nullable();
            $table->boolean('underwater')->default(true);
            $table->enum('type', ['Acoustic', 'Image', 'Video']);            
            $table->timestamps();

            $table->foreign('sighting_id')->references('id')->on('sightings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sightings_data');
    }
}
