<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrainwaveReportHasAnnotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_brainwave')->create('brainwave_report_has_annotations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_id')->unsigned();
            $table->integer('annotation_id')->unsigned();
            $table->timestamps();

            $table->foreign('report_id')->references('id')->on('brainwave_reports')->onDelete('cascade');
            $table->foreign('annotation_id')->references('id')->on('brainwave_annotations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_brainwave')->dropIfExists('brainwave_report_has_annotations');
    }
}
