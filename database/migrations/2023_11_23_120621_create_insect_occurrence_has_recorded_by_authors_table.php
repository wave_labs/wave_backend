<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsectOccurrenceHasRecordedByAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_insect')->create('insect_occurrence_has_recorded_by_authors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id')->unsigned()->nullable();
            $table->integer('occurrence_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('insect_authors')->onDelete('set null');
            $table->foreign('occurrence_id')->references('id')->on('insect_occurrences')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insect_occurrence_has_recorded_by_authors');
    }
}
