<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAiDetectionClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ai_detection_classifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ai_detection_image_id')->unsigned();
            $table->integer('ai_label_id')->unsigned()->nullable();
            $table->boolean('confirmation')->nullable();
            $table->decimal('accuracy', 5, 2);
            $table->decimal('xmin', 6, 2);
            $table->decimal('xmax', 6, 2);
            $table->decimal('ymin', 6, 2);
            $table->decimal('ymax', 6, 2);
            $table->timestamps();

            $table->foreign('ai_detection_image_id')->references('id')->on('ai_detection_images')->onDelete('cascade');
            $table->foreign('ai_label_id')->references('id')->on('ai_labels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ai_detection_classifications');
    }
}
