<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomeHasInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dome_has_interactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dome_id')->unsigned();
            $table->integer('dome_interaction_id')->unsigned();
            $table->integer('counter')->default(0);
            $table->timestamps();

            $table->foreign('dome_id')->references('id')->on('domes')->onDelete('cascade');
            $table->foreign('dome_interaction_id')->references('id')->on('dome_interactions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dome_has_interactions');
    }
}
