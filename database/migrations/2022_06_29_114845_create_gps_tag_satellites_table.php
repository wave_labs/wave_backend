<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGpsTagSatellitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_iot')->create('gps_tag_satellites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('satID');
            $table->string('pseudorange');
            $table->string('gpsWeek');
            $table->string('gpsTimeWeek');
            $table->integer('iot_report_id')->unsigned();
            $table->timestamps();

            $table->foreign('iot_report_id')->references('id')->on('iot_device_report')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gps_tag_satellites');
    }
}
