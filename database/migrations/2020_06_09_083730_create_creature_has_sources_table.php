<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreatureHasSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creature_has_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creature_id')->unsigned();
            $table->integer('source_id')->unsigned();
            $table->timestamps();

            $table->foreign('creature_id')->references('id')->on('creatures')->onDelete('cascade');
            $table->foreign('source_id')->references('id')->on('sources')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creature_has_sources');
    }
}
