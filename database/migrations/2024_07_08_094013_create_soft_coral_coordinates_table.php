<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftCoralCoordinatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_studies')->create('soft_coral_coordinates', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('latitude', 10, 7);
            $table->decimal('longitude', 10, 7);
            $table->integer('soft_coral_id')->unsigned();
            $table->string('year');
            $table->string('dimensions');
            $table->string('dimension_change');
            $table->string('depth');
            $table->timestamps();

            $table->foreign('soft_coral_id')->references('id')->on('soft_coral')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_studies')->dropIfExists('soft_coral_coordinates');
    }
}
