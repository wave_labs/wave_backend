<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSightingPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sighting_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sighting_id')->unsigned();
            $table->string('url');
            $table->timestamps();

            $table->foreign('sighting_id')->references('id')->on('sightings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sighting__photos');
    }
}
