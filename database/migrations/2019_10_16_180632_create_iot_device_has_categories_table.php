<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIotDeviceHasCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_iot')->create('iot_device_has_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iot_device_id')->unsigned();
            $table->integer('iot_device_category_id')->unsigned();
            $table->decimal('latitude', 7, 4)->nullable();
            $table->decimal('longitude', 7, 4)->nullable();
            $table->timestamps();

            $table->foreign('iot_device_id')->references('id')->on('iot_device')->onDelete('cascade');
            $table->foreign('iot_device_category_id')->references('id')->on('iot_device_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_iot')->dropIfExists('iot_device_has_categories');
    }
}
