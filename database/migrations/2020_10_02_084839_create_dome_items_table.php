<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomeItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dome_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dome_interaction_id')->unsigned();
            $table->string('item');
            $table->decimal('x', 3, 2);
            $table->decimal('y', 3, 2);
            $table->decimal('z', 3, 2);
            $table->timestamps();

            $table->foreign('dome_interaction_id')->references('id')->on('dome_has_interactions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dome_items');
    }
}
