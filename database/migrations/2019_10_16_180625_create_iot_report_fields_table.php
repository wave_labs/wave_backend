<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIotReportFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_iot')->create('iot_report_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('value', 191);
            $table->integer('report_id')->unsigned();
            $table->integer('device_field_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('report_id')->references('id')->on('iot_device_report')->onDelete('cascade');
            $table->foreign('device_field_id')->references('id')->on('iot_device_fields')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_iot')->dropIfExists('iot_report_fields');
    }
}
