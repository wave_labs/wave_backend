<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dives', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('diving_spot_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('max_depth');
            $table->integer('dive_time');
            $table->integer('number_diver');
            $table->dateTime('date');
            $table->timestamps();

            $table->foreign('diving_spot_id')->references('id')->on('diving_spots')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dives');
    }
}
