<?php

use App\Source;
use Illuminate\Database\Seeder;

class SourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Source::create([
            'name' => 'WHALE_REPORTER',
        ]);

        Source::create([
            'name' => 'DIVE_REPORTER',
        ]);

        Source::create([
            'name' => 'LITTER_REPORTER',
        ]);

        Source::create([
            'name' => 'WEB',
        ]);
    }
}
