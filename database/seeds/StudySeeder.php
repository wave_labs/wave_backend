<?php

use App\Study;
use Illuminate\Database\Seeder;

class StudySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Study::create([
            'title' => "Markers - Have you seen me lately?",
            'name' => "markers",
            'description' => "Evaluation of presence, abundance and distribution of biodiversity in the aquatic coast land of Arquipélago da Madeira",
            'image' => "/storage/images/logo/markers.svg",
        ]);

        Study::create([
            'title' => "Stopwatch",
            'name' => "stopwatch",
            'description' => "Web-based stopwatch of marine occurrences in various small videos, providing comparison metrics with AI generated previews.",
            'image' => "/storage/images/logo/stopwatch.svg",
        ]);

        Study::create([
            'title' => "Bounding Box",
            'name' => "bounding-box",
            'description' => "Web-based stopwatch of marine occurrences in various videos, providing comparison metrics with AI generated detections.",
            'image' => "/storage/images/logo/bounding-box.svg",
        ]);

        Study::create([
            'title' => "Freshwater Pin",
            'name' => "freshwater-pin",
            'description' => "Mapping species of the freshwater ecosystems at Madeira and Porto Santo islands, evaluating distributions across timeframes.",
            'image' => "/storage/images/logo/freshwater-pin.svg",
        ]);
    }
}
