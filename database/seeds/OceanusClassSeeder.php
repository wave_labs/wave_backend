<?php

use App\OceanusClass;
use Illuminate\Database\Seeder;

class OceanusClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OceanusClass::create([
            'name' => 'odontoceti',
        ]);

        OceanusClass::create([
            'name' => 'mysticeti',
        ]);

        OceanusClass::create([
            'name' => 'pinniped',
        ]);

        OceanusClass::create([
            'name' => 'bird',
        ]);

        OceanusClass::create([
            'name' => 'turtle',
        ]);

        OceanusClass::create([
            'name' => 'empty',
        ]);
    }
}
