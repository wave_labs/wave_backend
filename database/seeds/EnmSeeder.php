<?php

use App\EnmEnvironmentDataSource;
use App\EnmModel;
use App\EnmModelPrediction;
use App\EnmOcurrenceDataSource;
use Illuminate\Database\Seeder;

class EnmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'bioclim',
                ['Response Plot', 'Raster Object', 'ROC', 'TPR', 'Presence-Absence'],
                'bioclim.r'
            ],
            [
                'domain',
                ['Response Plot', 'Raster Object', 'ROC', 'TPR', 'Presence-Absence'],
                'domain.r'
            ],
            [
                'maxent',
                ['Response Plot', 'Raster Object', 'ROC', 'TPR', 'Presence-Absence'],
                'maxent.r'
            ],
        ];

        foreach ($items as $item) {
            $model = EnmModel::create([
                'model' => $item[0],
                'file' => $item[2],
            ]);

            foreach ($item[1] as $predictionType) {
                EnmModelPrediction::create([
                    'model_id' => $model->id,
                    'prediction_type' => $predictionType,
                    'code' => str_replace(' ', '', strtolower($predictionType))
                ]);
            }
        }

        $ocuData = [["name" => "GBIF"], ["name" => "wave"], ["name" => "upload"]];
        $envData = [["name" => "bioclim"], ["name" => "upload"], ["name" => "copernicus"]];

        foreach ($ocuData as $data) {
            EnmOcurrenceDataSource::create($data);
        }

        foreach ($envData as $data) {
            EnmEnvironmentDataSource::create($data);
        }
    }
}
