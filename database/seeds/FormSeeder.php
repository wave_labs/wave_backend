<?php

use Illuminate\Database\Seeder;
use App\Form;

class FormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Form::create([
            'form' => 'SUS',
            'desc' => 'The System Usability Scale (SUS) provides a “quick and dirty”, reliable tool for measuring the usability.   It consists of a 10 item questionnaire with five response options for respondents; from Strongly agree to Strongly disagree.  Originally created by John Brooke in 1986, it allows you to evaluate a wide variety of products and services, including hardware, software, mobile devices, websites and applications.'
        ]);

        /*

        Form::create([
            'form' => 'PRESENCE',
            'desc' => 'The authors of the scale state that presence is a normal awareness phenomenon that requires directed attention and is based in the interaction between sensory stimulation, environmental factors that encourage involvement and enable immersion, and internal tendencies to become involved. Focus of the PQ is to measure presence in virtual environments and games. In addition the immersive tendencies questionnaire (ITQ) was developed to measure differences in the tendencies of individuals to experience presence.'
        ]);

        Form::create([
            'form' => 'NASA-TLX',
            'desc' => 'The NASA Task Load Index (NASA-TLX) is a widely used, subjective, multidimensional assessment tool that rates perceived workload in order to assess a task, system, or team\'s effectiveness or other aspects of performance. It was developed by the Human Performance Group at NASA\'s Ames Research Center over a three-year development cycle that included more than 40 laboratory simulations. It has been cited in over 4,400 studies, highlighting the influence the NASA-TLX has had in human factors research. It has been used in a variety of domains, including aviation, healthcare and other complex socio-technical domains.'
        ]);

        Form::create([
            'form' => 'FSS',
            'desc' => 'The Flow Short Scale (FSS) has been developed to measure flow in its different components in everyday life. The FSS is a very economical instrument for this purpose, so the acitivity in question is only marginally interrupted. The 13 Items of the FSS are divided among the scales flow and anxiety. The items are answered on 7-point Likert scales.'
        ]);

        Form::create([
            'form' => 'IMI',
            'desc' => 'The Intrinsic Motivation Inventory (IMI) is a multidimensional measurement device intended to assess participants subjective experience related to a target activity in laboratory experiments. It has been used in several experiments related to intrinsic motivation and self-regulation. The instrument assesses participants interest/enjoyment, perceived competence, effort, value/usefulness, felt pressure and tension, and perceived choice while performing a given activity, thus yielding six subscale scores. Recently, a seventh subscale has been added to tap the experiences of relatedness..'
        ]);

        Form::create([
            'form' => 'SAM',
            'desc' => 'SAM is an emotion assessment tool that uses graphic scales, depicting cartoon characters expressing three emotion elements: pleasure, arousal and dominance. SAM has been used often in evaluations of advertisements, and increasingly also in evaluations of products. SAM is based on the PAD emotion model of Mehrabian. It is claimed that SAM is validated, although publications about the validation are not so clear on that; international comparison studies have been conducted; since it is a pictorial rather than a verbal scale this has advantages in certain situations; very quick to administer used in different settings, ranging from testing the appeal of advertisments and commercials to products, interactions, etc'
        ]);

        Form::create([
            'form' => 'STUDY',
            'desc' => 'Aglomeration of study questions directly connected with wave apps'
        ]);*/

        Form::create([
            'form' => 'NEP',
            'desc' => 'The New Ecological Paradigm scale is a measure of endorsement of a “pro-ecological” world view. It is used extensively in environmental education, outdoor recreation, and other realms where diff erences in behavior or attitudes are believed to be explained by underlying values, a world view, or a paradigm. The scale is constructed from individual responses to fifteen statements that measure agreement or disagreement.'
        ]);

        /*
        Form::create([
            'form' => 'PANAS-SF',
            'desc' => 'The PANAS-SF, comprises 10 items that were determined through the highest factor loadings on the exploratory factor analysis reported by Watson et al. (1988) in his original PANAS. Previous mood scales, such that of Bradburn, had low reliabilities and high correlations between subscales. Watson was able to address these concerns in his study of the original PANAS; however, his participants consisted mostly of student populations. The purpose of the PANAS-SF was not only to provide a shorter and more concise form of the PANAS, but to be able to apply the schedules to older clinical populations. Overall, it was reported that this modified model was consistent with Watson’s.'
        ]);*/

        Form::create([
            'form' => 'PANAS-X',
            'desc' => 'The PANAS-X assesses the specific, distinguishable affective emotional states that emerge from within the broader general dimensions of positive and negative emotional experience. In recent research, two broad, general factors—typically labeled Positive Affect (PA) and Negative Affect (NA)—have emerged reliably as the dominant dimensions of emotional experience. These factors have been identified in both intraand interindividual analyses, and they emerge consistently across diverse descriptor sets, time frames, response formats, languages, and cultures.'
        ]);


        Form::create([
            'form' => 'EVAL',
            'desc' => 'Enjoyment evaluation survey taken after dives.'
        ]);

        Form::create([
            'form' => 'IMI',
            'desc' => 'The Intrinsic Motivation Inventory (IMI) is a multidimensional measurement device intended to assess participants subjective experience related to a target activity in laboratory experiments. It has been used in several experiments related to intrinsic motivation and self-regulation. The instrument assesses participants interest/enjoyment, perceived competence, effort, value/usefulness, felt pressure and tension, and perceived choice while performing a given activity, thus yielding six subscale scores. Recently, a seventh subscale has been added to tap the experiences of relatedness..'
        ]);

    }
    
}
