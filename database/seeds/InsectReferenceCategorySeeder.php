<?php

use App\InsectReferenceCategories;
use Illuminate\Database\Seeder;

class InsectReferenceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'category' => 'Biology',
            ],
            [
                'category' => 'Distribution',
            ],
            [
                'category' => 'Nomenclature',
            ],
            [
                'category' => 'Food plants',
            ],
            [
                'category' => 'Habitat',
            ],
            [
                'category' => 'Description',
            ]
        ];

        foreach ($categories as $key => $category) {
            InsectReferenceCategories::create($category);
        }
    }
}
