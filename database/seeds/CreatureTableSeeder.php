<?php

use Illuminate\Database\Seeder;
use App\Creature;
use App\Source;

class CreatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // [creature][dive_center_app][whale_reporter_app]
        $items = [
            [
                [
                    'type_id' => '12',
                    'name' => 'Monk seal',
                    'name_scientific' => 'Monachus monachus',
                    'description' => 'Male have a black pelage and a white belly patch, female are brown or grey with a lighter belly',
                    'curiosity' => 'Monk Seal is the most endangered seal species in the world with less than 700 individuals in total and about 40 in Madeira. In the past, were hunted for their fur, oil, meat and for medicinal use. Monk Seal is considered as a species of community importance and its populations are legally protected.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 2.8 m length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native'
                ], [true], []
            ],
            [
                [
                    'type_id' => '2',
                    'name' => 'Seagrass',
                    'name_scientific' => 'Cymodocea nodosa',
                    'description' => 'Seagrass has light green or greyish-green leaves very narrow',
                    'curiosity' => 'Seagrass grows in sandy sediments and needs clear water for photosynthesis. Its meadows have high biological productivity and provide important habitat for many species including seahorses. It is threatened locally by mechanical damage from trawling and anchoring from boats and coastal development',
                    'conserv' => 'Vulnerable',
                    'size' => 'The leaves may be up to 40 cm long',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' =>
                    'Native'
                ], [true], []
            ],
            [
                [
                    'type_id' => '3',
                    'name' => 'Seahorse',
                    'name_scientific' => 'Hippocampus sp.',
                    'description' => 'Seahorse has a horse-shaped head, with a long snout and puckered mouth and a long prehensile tail',
                    'curiosity' => 'Seahorse is the only known animals in which the male carries the unborn offspring. Male pouch eggs until they hatch, and he will give birth to fully formed, miniature seahorses. Seahorses are used for traditional medicines, aquarium display, curiosities and good-luck charms when caught as by-catch.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Adult height 7- 13 cm',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [true], []
            ],
            [
                [
                    'type_id' => '3',
                    'name' => 'Dusky grouper',
                    'name_scientific' => 'Epinephelus marginatus',
                    'description' => 'Head and body dark with light irregular spots mostly set in vertical series.  Lighter ventrally',
                    'curiosity' => 'Heavily targeted by fisheries, resulted in a reduction of population and as a result it is listed as vulnerable species.Adults prefer rocky bottoms, are solitary and territorial. Juveniles live closer to shore in rocky tidal pools.  It change its sex from female to male with growth. ',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 150 cm length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [true], []
            ],
            [
                [
                    'type_id' => '3',
                    'name' => 'Red hogfish',
                    'name_scientific' => 'Bodianus scrofa',
                    'description' => 'Red head and body. Female: lighter ventrally with yellow horizontal line in the middle of the body',
                    'curiosity' => 'It has a limited distribution in the eastern Atlantic, with presence in less than ten locations. Its population is declining and is listed as vulnerable species. It shows marked haremic territorial system, typically one male to one to three females.  It changes its sex from female to male with growth.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 50 cm length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [true], []
            ],
            [
                [
                    'type_id' => '4',
                    'name' => 'Black coral',
                    'name_scientific' => 'Antipathella sp.',
                    'description' => 'Despite the name, bushes can be white, yellow, pink, grey, green or reddish brown',
                    'curiosity' => 'The horny skeleton, being hard and black through mineral intercalations, may used for manufacturing jewellery. The Greek used it to make amulets explaining the scientific name (anti pathes means against disease). Bushes of black coral are home of several animals.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 1.5 m high',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [true], []
            ],
            [
                [
                    'type_id' => '5',
                    'name' => 'Swimming crab',
                    'name_scientific' => 'Cronius ruber',
                    'description' => 'Different colors from orange to brown. It is a swimming crab with aggressive behavior',
                    'curiosity' => 'Characterized by slim claws with needle-like teeth, conferring a rapid action that in combination with paddle-like hind legs for swimming, make him an effective predator of fish and other soft-bodied fast-moving prey. Courtship ritual for coupling is common (through olfactory and tactile cues).',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 5 cm length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [true], []
            ],
            [
                [
                    'type_id' => '4',
                    'name' => 'Green reef coral',
                    'name_scientific' => 'Madracis asperula',
                    'description' => 'Green/brown encrusting colony formed by many small polyps. It may grow in branches or flat',
                    'curiosity' => 'The green/brown color is due to the presence of symbiotic algae called zooxanthellae that live in the coral tissue and help it with energy request through photosynthesis.  Without algae, the coral is white.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Branches up to 20 mm, polyps 2 mm length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [true], []
            ],
            [
                [
                    'type_id' => '8',
                    'name' => 'Harpoon weed',
                    'name_scientific' => 'Asparagopsis sp.',
                    'description' => 'Branches purple-red or branches and filamentous  brownish red',
                    'curiosity' => 'It is a red algae and presents two distinct life phases morphologically different.  It presents the thallus with tufts of all about the same length and spirally arranged. It showed a strong cytotoxicity against human cancer cell lines.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Branches up to 30 mm',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [true], []
            ],
            [
                [
                    'type_id' => '6',
                    'name' => 'Button tunicate',
                    'name_scientific' => 'Distaplia corolla',
                    'description' => 'Bright orange or purple colonies, forming lobes or heads developed in rosette and cluster',
                    'curiosity' => '8-20 individuals (zooids) form a rosette. Each zooid has its own mouth but share a common central exhalent opening. Several rosette form a cluster. The species was first described from Azores but it seems to be a Caribbean species, probably arrived by yachts crossing the Atlantic.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Zooid 2-4 mm long, rosette 1.5 cm diameter and cluster 40 cm diameter',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [true], []
            ],
            [
                [
                    'type_id' => '10',
                    'name' => 'Loggerhead turtle',
                    'name_scientific' => 'Caretta caretta',
                    'description' => 'Heart-shaped carapace that generally is reddish-brown hue with olive tones',
                    'curiosity' => 'It is the largest hard-shelled sea turtles. It nests on insular and mainland sandy beaches throughout the temperate and subtropical regions worldwide. It can undertake migrations traversing oceanic zones spanning hundreds to thousands of kilometres. It is protected by several treaties and laws. ',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 1 m length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [true], [true]
            ],
            [
                [
                    'type_id' => '4',
                    'name' => 'Fire Coral',
                    'name_scientific' => 'Millepora sp.',
                    'description' => 'Green-yellow colony with white tips and many growth forms, from cylindrical branches to flat slabs',
                    'curiosity' => 'Despite the name, it is not a true coral. The name fire coral comes from their ability to burn, in fact the polyps forming the colony are tiny but sting fiercely through cells called cnidocytes. They can inject a venom that causes a painful burning sensation, skin eruptions, blisters and scarring.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 45 cm high',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [true], []
            ],
            [
                [
                    'type_id' => '6',
                    'name' => 'Bushy encrusting anemone',
                    'name_scientific' => 'Antipathozoanthus sp.',
                    'description' => 'Polyps forming the colony have many tentacles with an intense yellow colour or slightly orange',
                    'curiosity' => 'To support the construction of its skeleton, it prefers slightly overhanging black coral. A colony of bushy encrusting anemone can be spread out on a square meter or more.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Single polyp up to 1.2 cm high and 0.6 cm diameter',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [true], []
            ],
            [
                [
                    'type_id' => '9',
                    'name' => 'Warty umbrella snail',
                    'name_scientific' => 'Umbraculum umbraculum',
                    'description' => 'Limpet-like shell and the big mantle is covered by numerous rounded papillae. Variable colour',
                    'curiosity' => 'This is a large, primitive species of slug with a round yellow body and its gills can be visible under the shell. The shell can be covered with encrusting plant and animal growths. As a means of defence, it produces a compound which tastes very disagreeable to fish.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 20 cm length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [true], []
            ],
            [
                [
                    'type_id' => '8',
                    'name' => 'Caulerpa seaweed',
                    'name_scientific' => 'Caulerpa webbiana',
                    'description' => 'Tiny green bristles of branched filaments, set in whorls around the central "stem"',
                    'curiosity' => ' Also known as bottlebrush green seaweed because its branches shape give the overall appearance of a stiff bottlebrush. It is a tropical species, common in Caribbean and Hawaii. Usually lives anchored in sediment or growing on coral reefs.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Single stem of 4-6 cm long',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [true], []
            ],
            [
                [
                    'type_id' => '9',
                    'name' => 'Octopus',
                    'name_scientific' => 'Octopus vulgaris',
                    'description' => 'Octopus has 8 arms that are lined with suckers, it has a big mantle and lack of internal shell',
                    'curiosity' => 'Octopus uses camouflage either to resemble other animals or to blend into their environment, changing its colour and its appearance by raising or lowering small protuberances all over the soft body. Usually the entrance of its den is full of leftovers of its food like mussel shells.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 25 cm in mantle length, up to 1 m in arms long',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [true], []
            ],
            [
                [
                    'type_id' => '3',
                    'name' => 'Gilthead seabream',
                    'name_scientific' => 'Sparus aurata',
                    'description' => 'Silvery grey body with a gold line between eyes and a dark patch at the beginning of the body',
                    'curiosity' => 'It is one of the most important commercially cultured species. It is used in ocean cage aquaculture in Madeira from 1997. It is a voracious opportunistic predators, capable of adapting their diet to the food available in its environment. It changes its sex from male to female with growth.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 70 cm length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [true], []
            ],
            [
                [
                    'type_id' => '8',
                    'name' => 'Leafy flat-blade algae',
                    'name_scientific' => 'Stypopodium sp.',
                    'description' => 'Golden brown or olive-green smooth blades with broad squared-off ends growing in a fan shape',
                    'curiosity' => 'It presents bands of different coloured material develop parallel to the growing edge including an iridescent green band that gives the seaweed an attractive zoned appearance. It is attached to rock and often forms dense beds. It has different compounds with defensive function.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 50 cm length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [true], []
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Atlantic Spotted Dolphin',
                    'name_scientific' => 'Stenella frontalis',
                    'description' => 'The Atlantic spotted dolphin is a dolphin found in the Gulf Stream of the North Atlantic Ocean. Older members of the species have a very distinctive spotted coloration all over their bodies.',
                    'curiosity' => 'Monk Seal is the most endangered seal species in the world with less than 700 individuals in total and about 40 in Madeira. In the past, were hunted for their fur, oil, meat and for medicinal use. Monk Seal is considered as a species of community importance and its populations are legally protected.',
                    'conserv' => 'Least Concern',
                    'size' => 'Up to 2.29 m length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '12',
                    'name' => 'Bearded Seal',
                    'name_scientific' => 'Erignathus barbatus',
                    'description' => 'Distinguishing features of this earless seal include square fore flippers and thick bristles on its muzzle. Adults are greyish-brown in colour, darker on the back. Occasionally the face and neck are reddish brown, seal pups are born with a greyish-brown natal fur with scattered patches of white on the back and head.',
                    'curiosity' => 'Primarily benthic, bearded seals feed on a variety of small prey found along the ocean floor, including clams, squid, and fish. Their whiskers serve as feelers in the soft bottom sediments. Adults tend not to dive very deep, favoring shallow coastal areas no more than 300 m deep. Pups up to one year old, however, will venture much deeper, diving as deep as 450 m',
                    'conserv' => 'Least Concern',
                    'size' => 'up to 2.7 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Beluga, White Whale',
                    'name_scientific' => 'Delphinapterus leucas',
                    'description' => 'The sudden tapering to the base of its neck gives it the appearance of shoulders, unique among cetaceans. The tailfin grows and becomes increasingly and ornately curved as the animal ages. The flippers are broad and short—making them almost square-shaped.',
                    'curiosity' => 'These cetaceans are highly sociable and they regularly form small groups, or pods, that may contain between two and 25 individuals, with an average of 10 members. Pods tend to be unstable, meaning individuals tend to move from pod to pod. Radio tracking has even shown belugas can start out in one pod and within a few days be hundreds of miles away from that pod.',
                    'conserv' => 'Least Concern',
                    'size' => 'Adult up to 5.5 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Bottlenose Dolphin',
                    'name_scientific' => 'Tursiops truncatus',
                    'description' => 'Their elongated upper and lower jaws form what is called a rostrum, or snout, which gives the animal its common name. The real, functional nose is the blowhole on top of its head; the nasal septum is visible when the blowhole is open.',
                    'curiosity' => 'Bottlenose dolphins can live for more than 40 years. Females typically live 5–10 years longer than males, with some females exceeding 60 years. This extreme age is rare and less than 2% of all Bottlenose dolphins will live longer than 60 years. Bottlenose dolphins can jump at a height of 6 metres up in the air; they use this to communicate with one another.',
                    'conserv' => 'Least Concern',
                    'size' => 'Up to 4 metres length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Bowhead Whale',
                    'name_scientific' => 'Balaena mysticetus',
                    'description' => 'The bowhead whale has a large, robust, dark-coloured body and a white chin/lower jaw. The whale has a massive triangular skull, which it uses to break through the Arctic ice to breathe.',
                    'curiosity' => 'They live entirely in fertile Arctic and sub-Arctic waters, unlike other whales that migrate to low latitude waters to feed or reproduce. The bowhead was also known as the Greenland right whale or Arctic whale. American whalemen called them the steeple-top, polar whale, or Russia or Russian whale. The bowhead has the largest mouth of any animal.',
                    'conserv' => 'Least Concern',
                    'size' => 'Up to 18 m length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Clymene Dolphin',
                    'name_scientific' => 'Stenella clymene',
                    'description' => 'The Clymene dolphin looks very similar to the spinner dolphin. At close quarters, it is possible to observe that the beak of the Clymene is slightly shorter than that of its relative. The dorsal fin is also less erect and triangular.',
                    'curiosity' => 'Clymene dolphins spend most of their lives in waters over 100 m in depth, but occasionally move into shallower, coastal regions. They feed on squid and small schooling fish., hunting either at night, or in mesopelagic waters where there is only limited light. Predators include cookie-cutter sharks, as evidenced by bite marks seen on a number of animals.',
                    'conserv' => 'Least Concern',
                    'size' => 'Up to 2 m length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Common dolphin',
                    'name_scientific' => 'Delphinus delphis',
                    'description' => ' The color pattern on the body is unusual. The back is dark and the belly is white, while on each side is an hourglass pattern colored light grey, yellow, or gold in front and dirty grey in back.',
                    'curiosity' => 'Common dolphins face a mixture of threats due to human influence. Moderate levels of metal pollutants, which are thought to negatively impact dolphin health, have been measured in some populations. Populations have been hunted off the coast of Peru for use as food and shark bait. In most other areas, the dolphins have not been hunted directly. ',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 2.5 m length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'False Killer Whale',
                    'name_scientific' => 'Pseudorca crassidens',
                    'description' => 'The false killer whale is black or dark gray, though slightly lighter on the underside. It has a slender body with an elongated, tapered head and 44 teeth. The dorsal fin is sickle-shaped, and its flippers are narrow, short, and pointed',
                    'curiosity' => 'The false killer whale (Pseudorca crassidens) is a large oceanic dolphin that inhabits oceans worldwide but mainly frequents tropical regions. It was first described in 1846 as a species of porpoise based on a skull, which was revised when the first carcasses were observed in 1861. The name "false killer whale" comes from the similar skull characteristics to the killer whale (Orcinus orca). ',
                    'conserv' => 'Near Threatened',
                    'size' => 'Up to 6 m length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Fin, Finback Whale',
                    'name_scientific' => 'Balaenoptera physalus',
                    'description' => 'The fin whale is usually distinguished by its tall spout, long back, prominent dorsal fin, and asymmetrical colouration. ',
                    'curiosity' => 'The fin whale (Balaenoptera physalus), also known as finback whale or common rorqual and formerly known as herring whale or razorback whale, is a marine mammal belonging to the parvorder of baleen whales. It is the second-largest species on Earth after the blue whale.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Up to 25.9 m length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Fraser´s Dolphin',
                    'name_scientific' => 'Distaplia corolla',
                    'description' => 'They have a stocky build, a small fin in relation to the size of the body, conspicuously small flippers. The dorsal fin and beak are also insubstantial. ',
                    'curiosity' => 'The dolphin is normally sighted in deep tropical waters; between 30°S and 20°N. The Eastern Pacific is the most reliable site for viewings. Groups of stranded dolphins have been found as far afield as France and Uruguay. However these are regarded as anomalous and possibly due to unusual oceanographic conditions, such as El Niño.  ',
                    'conserv' => 'Least Concern',
                    'size' => 'Growing up to 2.75 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Grampus, Risso´s Dolphin',
                    'name_scientific' => 'Grampus griseus',
                    'description' => 'Risso´s dolphin has a relatively large anterior body and dorsal fin, while the posterior tapers to a relatively narrow tail. The bulbous head has a vertical crease in front.',
                    'curiosity' => 'Risso´s dolphin (Grampus griseus) is the only species of dolphin in the genus Grampus. It is commonly known as the Monk dolphin among Taiwanese fishermen.',
                    'conserv' => 'Least Concern',
                    'size' => 'Up to 4.00 m length',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '12',
                    'name' => 'Harp Seal',
                    'name_scientific' => 'Pagophilus groenlandicus',
                    'description' => 'The mature harp seal has pure black eyes. It has a silver-gray fur covering its body, with black harp or wishbone-shaped markings dorsally.',
                    'curiosity' => 'Compared to other phocid seals, the harp seal dives from shallow to moderately deep depths. Dive depth varies with season, time of day and location. In the Greenland Sea sub-population, the average dive rate is around 8.3 dives per hour and dives range from a depth of less than 20 to over 500m.',
                    'conserv' => 'Least Concern',
                    'size' => 'reach a length up to 1.9 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Humpback Whale',
                    'name_scientific' => 'Megaptera novaeangliae',
                    'description' => 'Humpbacks can easily be identified by their stocky body, obvious hump, black dorsal coloring and elongated pectoral fins. The head and lower jaw are covered with knobs called tubercles, which are hair follicles and are characteristic of the species. ',
                    'curiosity' => 'Humpback whales are rorquals, members of the Balaenopteridae family that includes the blue, fin, Bryde´s, sei and minke whales. The rorquals are believed to have diverged from the other families of the suborder Mysticeti as long ago as the middle Miocene era.',
                    'conserv' => 'Least Concern',
                    'size' => 'Adults range in length from 12–16 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Orca, Killer Whale',
                    'name_scientific' => 'Orcinus orca',
                    'description' => 'A typical killer whale distinctively bears a black back, white chest and sides, and a white patch above and behind the eye. Calves are born with a yellowish or orange tint, which fades to white. It has a heavy and robust body with a large dorsal fin up to 1.8 m tall.',
                    'curiosity' => 'Killer whales are found in all oceans and most seas. Due to their enormous range, numbers, and density, relative distribution is difficult to estimate, but they clearly prefer higher latitudes and coastal areas over pelagic environments.',
                    'conserv' => 'Data Deficient ',
                    'size' => 'Range from 6 to 8 metres',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '12',
                    'name' => 'Leopard Seal',
                    'name_scientific' => 'Hydrurga leptonyx',
                    'description' => 'The leopard seal has a distinctively long and muscular body shape, when compared to other seals. This species of seal is known for its massive reptilian-like head and jaws that allow it to be one of the top predators in its environment.',
                    'curiosity' => 'Leopard seals are very vocal underwater during the austral summer. The male seals produce loud calls (153 to 177 dB re 1 μPa at 1 m) for many hours each day.',
                    'conserv' => 'Least Concern',
                    'size' => 'The overall length of this seal is 2.4–3.5 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Long-Finned Pilot Whale',
                    'name_scientific' => 'Globicephala melas',
                    'description' => 'Despite its common name, the long-finned pilot whale is actually a large species of dolphin. The same is true of orcas and several other small whales. It has a bulbous forehead and is black or dark grey in colour with light-grey or white markings on the throat and belly regions.',
                    'curiosity' => 'Pilot whales get their name from the original belief that there was a "pilot" or lead individual in their groups. The name for the genus, "Globicephala" is derived from a combination of the Latin words globus ("globe") and kephale ("head").',
                    'conserv' => 'Least Concern ',
                    'size' => ' Up to 6.7 meters',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Melon Headed Whale',
                    'name_scientific' => 'Peponocephala electra',
                    'description' => 'The melon-headed whale´s head is a rounded cone giving the animal its common name. The body is more or less uniformly light grey except for a dark grey face – sometimes called the "mask". The flippers are long and pointed. The dorsal fin is tall with a pointed tip – reminiscent of its relative the killer whale. ',
                    'curiosity' => 'Melon-headed whales are very social animals that live in large groups numbering between 100 and 1,000. They have been observed swimming close to each other and touching flippers. Within the large group, they usually swim in smaller groups of 10-14',
                    'conserv' => 'Least Concern',
                    'size' => 'Adult grows up to 3 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Minke Whale',
                    'name_scientific' => 'Balaenoptera acutorostrata',
                    'description' => 'The minke whale is a black/gray/purple color.',
                    'curiosity' => 'The minke whales are the second smallest baleen whale; only the pygmy right whale is smaller. Upon reaching sexual maturity (6–8 years of age), males measure an average of 6.9 m (23 ft) and females 8 m (26 ft) in length, respectively.',
                    'conserv' => 'Vulnerable',
                    'size' => 'Reported maximum lengths vary from 9.1 to 10.7 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Narwhal',
                    'name_scientific' => 'Monodon monoceros',
                    'description' => 'The pigmentation of narwhals is a mottled pattern, with blackish-brown markings over a white background. They are darkest when born and become whiter with age',
                    'curiosity' => 'The most conspicuous characteristic of the male narwhal is a single long tusk, a canine tooth that projects from the left side of the upper jaw, through the lip, and forms a left-handed helix spiral.',
                    'conserv' => 'Least Concern',
                    'size' => 'An average length of 4.1 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'North Atlantic right whale',
                    'name_scientific' => 'Eubalaena glacialis',
                    'description' => 'Like other right whales, the North Atlantic right whale, also known as the northern right whale or black right whale, is readily distinguished from other whales by the callosities on its head',
                    'curiosity' => 'Aside from mating activities performed by groups of single female and several males, so called SAG (Surface Active Group), North Atlantic right whales seem less active compared to subspecies in southern hemisphere.',
                    'conserv' => 'Endangered',
                    'size' => 'Adult North Atlantic right whales average 13–16 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Pantropical Spotted Dolphin',
                    'name_scientific' => 'Stenella attenuata',
                    'description' => 'The pantropical spotted dolphin varies significantly in size and coloration throughout its range. The most significant division is between coastal and pelagic varieties. The coastal form is larger and more spotted. ',
                    'curiosity' => 'The pantropical spotted dolphin (Stenella attenuata) is a species of dolphin found in all the world´s temperate and tropical oceans. The species was beginning to come under threat due to the killing of millions of individuals in tuna purse seines.',
                    'conserv' => 'Least Concern ',
                    'size' => 'Adults are about 2.5 m long',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '12',
                    'name' => 'Ross Seal',
                    'name_scientific' => 'Ommatophoca rossii',
                    'description' => 'The coat is colored dark-brown in the dorsal area and silvery-white beneath. At the onset of the Antarctic winter, the coat fades gradually to become light brown. ',
                    'curiosity' => 'The Ross seal is able to produce a variety of complex twittering and siren-like sounds that are performed on ice and underwater, where they carry for long distances.',
                    'conserv' => 'Least Concern ',
                    'size' => 'Reach a length of about 1.68–2.09 m ',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Rough-Toothed Dolphin',
                    'name_scientific' => 'Steno bredanensis',
                    'description' => 'As the common name for the species implies, the teeth are also distinctive, having a roughened surface formed by numerous narrow irregular ridges. ',
                    'curiosity' => 'Rough-toothed dolphins are typically social animals, although solitary individuals are also sighted. An average group has between ten and twenty members, but they can vary from as few as two to as many as ninety.',
                    'conserv' => 'Least Concern ',
                    'size' => 'Reach from 2.09 to 2.83 metres',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Short-Finned (Pacific) Pilot Whale',
                    'name_scientific' => 'Globicephala macrorhynchus',
                    'description' => 'The short-finned whale has a stocky body, a bulbous forehead, no prominent beak, long flippers sharply pointed at the tip, black or dark grey color, and the dorsal fin set forward on body. ',
                    'curiosity' => 'Short-finned pilot whales can be confused with their relatives the long-finned pilot whales, but there are various differences. As their names indicate, their flippers are shorter than those of the long-finned pilot whale, with a gentler curve on the edge. They have fewer teeth than the long-finned pilot whale, with 14 to 18 on each jaw.',
                    'conserv' => 'Least Concern ',
                    'size' => 'Reach up to 5.5 meters',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Southern Right Whale',
                    'name_scientific' => 'Eubalaena australis',
                    'description' => 'Its skin is very dark grey or black, occasionally with some white patches on the belly. The right whale´s callosities appear white due to large colonies of cyamids ',
                    'curiosity' => 'Right whales do not normally cross the warm equatorial waters to connect with the other species and (inter)breed: their thick layers of insulating blubber make it difficult for them to dissipate their internal body heat in tropical waters. ',
                    'conserv' => 'Least Concern ',
                    'size' => 'Reach up to 17.5–18 m',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '1',
                    'name' => 'Sperm Whale, Cachalot',
                    'name_scientific' => 'Physeter macrocephalus',
                    'description' => 'Atop the whales skull is positioned a large complex of organs filled with a liquid mixture of fats and waxes called spermaceti. The purpose of this complex is to generate powerful and focused clicking sounds, which the sperm whale uses for echolocation and communication.',
                    'curiosity' => 'Is the largest of the toothed whales and the largest toothed predator. It is the only living member of genus Physeter and one of three extant species in the sperm whale family, along with the pygmy sperm whale and dwarf sperm whale of the genus Kogia',
                    'conserv' => 'Least Concern ',
                    'size' => 'Reach up to 20.5 metres',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Spinner Dolphin',
                    'name_scientific' => 'Stenella longirostris',
                    'description' => 'The dorsal area is dark gray, the sides light gray, and the underside pale gray or white. Also, a dark band runs from the eye to the flipper, bordered above by a thin, light line.',
                    'curiosity' => 'The spinner dolphin lives in nearly all tropical and subtropical waters between 40°N and 40°S. The species primarily inhabits coastal waters, islands, or banks. However, in the eastern tropical Pacific, dolphins live far from shore. Spinner dolphins may use different habitats depending on the season.',
                    'conserv' => 'Least Concern ',
                    'size' => 'Adults are typically 129–235 cm long',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Striped Dolphin',
                    'name_scientific' => 'Stenella coeruleoalba',
                    'description' => 'Its colouring is very different and makes it relatively easy to notice at sea. The underside is blue, white, or pink. One or two black bands circle the eyes, and then run across the back, to the flipper. ',
                    'curiosity' => 'They may also mix with common dolphins. The striped dolphin is as capable as any dolphin at performing acrobatics — frequently breaching and jumping far above the surface of the water. ',
                    'conserv' => 'Least Concern ',
                    'size' => 'Adults are typically 2.6 m long',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '14',
                    'name' => 'Walrus',
                    'name_scientific' => 'Odobenus rosmarus',
                    'description' => 'The most prominent feature of the living species is its long tusks. These are elongated canines, which are present in both male and female walruses and can reach a length of 1 m  and weigh up to 5.4 kg ',
                    'curiosity' => 'The walrus is a mammal in the order Carnivora. It is the sole surviving member of the family Odobenidae, one of three lineages in the suborder Pinnipedia along with true seals (Phocidae) and eared seals (Otariidae). ',
                    'conserv' => 'Vulnerable',
                    'size' => 'Adults are typically 5 m long',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '12',
                    'name' => 'Weddell Seal',
                    'name_scientific' => 'Leptonychotes weddellii',
                    'description' => 'The Weddell seal grows a thin fur coat around its whole body except for small areas around the flippers. The colour and pattern of the coat varies, often fading to a duller colour as the seal ages.',
                    'curiosity' => 'Weddell seals are commonly found on fast ice, or ice fastened to land, and gather in small groups around cracks and holes within the ice. In the winter, they stay in the water to avoid blizzards, with only their heads poking through breathing holes in the ice.',
                    'conserv' => 'Least Concern',
                    'size' => 'Adults are typically  2.5–3.5 m long',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'White-beaked Dolphin',
                    'name_scientific' => 'Lagenorhynchus albirostris',
                    'description' => 'The white-beaked dolphin is a robust species of dolphin with a short beak. ',
                    'curiosity' => 'White-beaked dolphins have 25 to 28 teeth in each jaw, although the three teeth closest to the front of the mouth are often not visible, failing to erupt from the gums. They have up to 92 vertebrae, more than any other species of oceanic dolphin',
                    'conserv' => 'Least Concern',
                    'size' => 'Adults can reach 2.3 to 3.1 m',
                    'group_size' => '',
                    'calves' => ' Calves are 1.1 to 1.2 m',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '11',
                    'name' => 'Atlantic white-sided dolphin',
                    'name_scientific' => 'Lagenorhynchus acutus',
                    'description' => 'The key distinguishing feature is the white to pale yellow patch found behind the dorsal fin of the dolphin on each side. This colour variation is unique amongst the mixtures of white, greys and blues of other pelagic cetaceans.',
                    'curiosity' => ' Often traveling in large pods and display aerial behaviors as they travel. Despite being docile creatures, even known to interact with various species of cetecean in a nonviolent manner, most notably with the long-finned pilot whale ',
                    'conserv' => 'Least Concern',
                    'size' => 'Adults can reach 2.5 m',
                    'group_size' => '',
                    'calves' => ' Calves are 1.1 to 1.2 m',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '12',
                    'name' => 'Fur seal',
                    'name_scientific' => 'Arctocephalinae',
                    'description' => ' Fur seals are characterized by their external pinnae, dense underfur, vibrissae, and long, muscular limbs. They share with other otariids the ability to rotate their rear limbs forward, supporting their bodies and allowing them to ambulate on land.',
                    'curiosity' => ' Typically, fur seals gather during the summer in large rookeries at specific beaches or rocky outcrops to give birth and breed. All species are polygynous, meaning dominant males reproduce with more than one female.',
                    'conserv' => 'Least Concern',
                    'size' => 'Size ranges from about 1.5 m, 64 kg in the male Galapagos to 2.5 m, 180 kg in the adult male New Zealand fur seal.',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '12',
                    'name' => 'Sea lion',
                    'name_scientific' => 'Otariinae',
                    'description' => ' Sea lions are pinnipeds characterized by external ear flaps, long foreflippers, the ability to walk on all fours, short, thick hair, and a big chest and belly.',
                    'curiosity' => ' Sea lions consume large quantities of food at a time and are known to eat about 5–8% of their body weight at a single feeding. Sea lions can move around 30 km/h in water and at their fastest they can reach a speed of about 56 km/h.',
                    'conserv' => 'Least Concern',
                    'size' => 'A male California sea lion weighs on average about 300 kg and is about 2.4m long, while the female sea lion weighs 100 kg and is 1.8m long.',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '14',
                    'name' => 'Seagull',
                    'name_scientific' => 'Laridae',
                    'description' => 'They are generally uniform in shape, with heavy bodies, long wings, and moderately long necks. The tails of all but three species are rounded; the exceptions being Sabine\'s gull and swallow-tailed gulls, which have forked tails, and Ross\'s gull, which has a wedge-shaped tail.',
                    'curiosity' => ' Gulls are highly adaptable feeders that opportunistically take a wide range of prey. The food taken by gulls includes fish and marine and freshwater invertebrates, both alive and already dead, terrestrial arthropods and invertebrates such as insects and earthworms, rodents, eggs, carrion, offal, reptiles, amphibians, plant items such as seeds and fruit, human refuse, chips, and even other birds.',
                    'conserv' => 'Least Concern',
                    'size' => 'Gulls range in size from 120 grams and 29 centimetres to 1.75 kg and 76 cm.',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '3',
                    'name' => 'Spotfin burrfish',
                    'name_scientific' => 'Chilomycterus reticulatus',
                    'description' => 'Chilomycterus reticulatus has a rotund body, which can be inflated, with a wide, blunt head and large eyes. The nasal organ of adults sits in an open, pitted cup which in juveniles is a tentacle with two openings.',
                    'curiosity' => 'The teeth are fused into a parrot like beak with no frontal groove and the mouth is large.',
                    'conserv' => 'Least Concern',
                    'size' => 'They grow to a standard length of 50 cm but up to 75 cm has been recorded.',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Native',
                ], [], [true]
            ],
            [
                [
                    'type_id' => '3',
                    'name' => 'European eel',
                    'name_scientific' => 'Anguilla anguilla',
                    'description' => 'The European eel (Anguilla anguilla) is a species of eel, a snake-like, catadromous fish. Eels have been important sources of food both as adults (including jellied eels of East London) and as glass eels.',
                    'curiosity' => 'While the species\' lifespan in the wild has not been determined, captive specimens have lived over 80 years. A specimen known as \"the Brantevik Eel\" lived for 155 years in the well of a family home in Brantevik, a fishing village in southern Sweden.',
                    'conserv' => 'Critical Endangered',
                    'size' => 'They are normally around 60–80 cm (2.0–2.6 ft) and rarely reach more than 1 m (3 ft 3 in), but can reach a length of up to 1.5 m (4 ft 11 in) in exceptional cases.',
                    'group_size' => '',
                    'calves' => '',
                    'nis_status' => 'Non-Indigenous Species',
                ], [], []
            ],
        ];

        $PTitems = [
            [
                'name' => 'Foca monge',
                'description' => 'Machos possuem penugem preta com barriga branca, enquanto que as fêmeas são castanhas com barriga mais clara',
                'curiosity' => 'Foca monge é a foca mais em risco do mundo, com apenas 700 espécimes, dos quais 40 na Madeira. No passado, eram caçadas por pêlo, óleos, carne e medicamentos. A espécie possui importância comunitária e a sua população é protegida legalmente.',
                'size' => 'Até 2.8 m de comprimento',
            ],
            [
                'name' => 'Algas marinhas',
                'description' => 'Algas marinhas possuem folhas estreitas de cor verde clara ou verde acinzentado',
                'curiosity' => 'Algas marinhas cresce em áreas ariosas e necessita de água fresca para fotosíntese. Um prado de algas marinhas gera produtividade biológica e fornece habitat para diversas espécies, tal como cavalos marinhos. É ameaçada por pesca, âncoras de barcos e desenvolvimento costal',
                'size' => 'As folhas poderão alcançar 40 cm de comprimento',
            ],
            [
                'name' => 'Cavalo Marinho',
                'description' => 'Cavalo Marinho possui uma cabeça em formato de cavalo, com um focinho longo, boca enrugada e cauda preênsil',
                'curiosity' => 'Cavalo Marinho é o único animal conhecido que o macho carrega crias por nascer. Cavalos marinhos são utilizados para fins medicinais, decoração em aquários, curiosidades e símbolos de boa sorte quando avistados',
                'size' => 'Altura entre 7 e 13 cm',
            ],
            [
                'name' => 'Garoupa escura',
                'description' => 'Cabeça e corpo escuro com manchas claras irregulares, geralmente em séries verticais. Mais claro ventralmente',
                'curiosity' => 'Grande alvo de pesca, que resulta numa redução da sua população e consequentemente uma espécie de vulnerável. Adultos preferem fundos rochosos e são solitários e territoriais. Jovens vivem perto das costas rochosas. Modificam o seu sexo de fêmea para macho no crescimento',
                'size' => 'Até 150cm de comprimento',
            ],
            [
                'name' => 'Peixe porco vermelho',
                'description' => 'Corpo e cabeça vermelhos. Fêmea: mais claro ventralmente com linha horizontal amarela no meio do corpo',
                'curiosity' => 'Tem uma distribuição limitada no Atlântico Este, com presença em menos de dez localidades. A sua população está diminuindo e é listada como espécie vulnerável. Está marcado como um sistema territorial harêmico, tipicamente existe um macho para três fêmeas. Ele muda seu sexo de feminino para masculino com o crescimento.',
                'size' => 'Até 50 cm de comprimento',
            ],
            [
                'name' => 'Coral preto',
                'description' => 'Apesar do seu nome, poderá apresentar folhas brancas, amarelas, rosa, cinzentas, verdes or castanho avermelhado',
                'curiosity' => 'O esqueleto córneo, sendo duro e preto por intercalações minerais, pode ser usado para a fabricação de jóias. Os gregos o usavam para fazer amuletos explicando o nome científico (anti pathes significa contra doenças). Os arbustos de coral negro são o lar de vários animais.',
                'size' => 'Até 1.5 m de altura',
            ],
            [
                'name' => 'Caranguejo',
                'description' => 'Cores diferentes do laranja ao castanho. É um caranguejo nadador com comportamento agressivo',
                'curiosity' => 'Caracterizado por garras delgadas com dentes em forma de agulha, conferindo uma ação rápida que em combinação com patas traseiras em forma de remo para nadar, o tornam um predador eficaz de peixes e outras presas de corpo mole que se movem rapidamente. O ritual de namoro para acasalamento é comum (por meio de pistas olfativas e táteis).',
                'size' => 'Até 5cm de comprimento',
            ],
            [
                'name' => 'Coral de recife verde',
                'description' => 'Colônia incrustante verde / castanho formada por muitos pequenos pólipos. Pode crescer em galhos ou planos',
                'curiosity' => 'A cor verde / castanho se deve à presença de algas simbióticas chamadas zooxantelas que vivem no tecido coral e auxiliam na solicitação de energia por meio da fotossíntese. Sem algas, o coral é branco.',
                'size' => 'Ramos até 20 mm, pólipos de 2 mm de comprimento',
            ],
            [
                'name' => 'Alga vermelha',
                'description' => 'Ramos vermelho-púrpura ou ramificado e filamentoso vermelho acastanhado',
                'curiosity' => 'É uma alga vermelha e apresenta duas fases de vida distintas, morfologicamente diferentes. Apresenta o talo com tufos de quase o mesmo comprimento e dispostos em espiral. Ele mostrou uma forte citotoxicidade contra linhagens de células cancerígenas humanas.',
                'size' => 'Ramos até 30 mm',
            ],
            [
                'name' => 'Tunicado de botão',
                'description' => 'Colônias laranja ou roxas brilhantes, formando lóbulos ou cabeças desenvolvidas em roseta e cluster',
                'curiosity' => '8-20 indivíduos (zoóides) formam uma roseta. Cada zoóide tem sua própria boca, mas compartilha uma abertura exalante central comum. Várias rosetas formam um aglomerado. A espécie foi descrita pela primeira vez nos Açores, mas parece ser uma espécie caribenha, provavelmente chegada por iates que atravessam o Atlântico.',
                'size' => 'Zoóide de 2-4 mm de comprimento, roseta de 1,5 cm de diâmetro e cluster de 40 cm de diâmetro',
            ],
            [
                'name' => 'Tartaruga',
                'description' => 'Carapaça em forma de coração, geralmente de tonalidade marrom-avermelhada com tons de azeitona',
                'curiosity' => 'São as maiores tartarugas marinhas de carapaça dura. Nidifica em praias arenosas insulares e continentais em todas as regiões temperadas e subtropicais em todo o mundo. Ele pode realizar migrações que atravessam zonas oceânicas que abrangem centenas a milhares de quilômetros. É protegido por vários tratados e leis.',
                'size' => 'Até 1m de comprimento',
            ],
            [
                'name' => 'Coral de fogo',
                'description' => 'Colônia verde-amarela com pontas brancas e muitas formas de crescimento, de ramos cilíndricos a lajes planas',
                'curiosity' => 'Apesar do nome, não é um coral verdadeiro. O nome coral de fogo vem de sua capacidade de queimar; na verdade, os pólipos que formam a colônia são minúsculos, mas picam ferozmente através de células chamadas cnidócitos. Eles podem injetar um veneno que causa uma sensação dolorosa de queimação, erupções na pele, bolhas e cicatrizes.',
                'size' => 'Até 45 cm de altura',
            ],
            [
                'name' => 'Anêmona incrustante',
                'description' => 'Os pólipos que formam a colônia têm muitos tentáculos com uma cor amarela intensa ou ligeiramente laranja',
                'curiosity' => 'Para apoiar a construção de seu esqueleto, ele prefere um coral preto ligeiramente saliente. Uma colônia de anêmonas espesso e incrustante pode ser espalhada em um metro quadrado ou mais.',
                'size' => 'Pólipo único de até 1,2 cm de altura e 0,6 cm de diâmetro',
            ],
            [
                'name' => 'Caracol guarda chuva',
                'description' => 'A concha em forma de limpet e o grande manto são cobertos por numerosas papilas arredondadas. Cor variável',
                'curiosity' => 'Esta é uma espécie grande e primitiva de lesma com um corpo amarelo redondo e suas guelras podem ser visíveis sob a concha. A concha pode ser coberta com plantas incrustantes e crescimentos de animais. Como meio de defesa, produz um composto de sabor muito desagradável para os peixes.',
                'size' => 'Até 20 cm de altura',
            ],
            [
                'name' => 'Caulerpa',
                'description' => 'Minúsculos cerdas verdes de filamentos ramificados, inseridos em espirais em torno da "haste" central',
                'curiosity' => 'Também conhecida como alga marinha verde pincel para garrafas, porque a forma de seus ramos dá a aparência geral de uma escova para garrafas rígida. É uma espécie tropical, comum no Caribe e no Havaí. Normalmente vive ancorado em sedimentos ou crescendo em recifes de coral.',
                'size' => 'Única haste de 4-6 cm de comprimento',
            ],
            [
                'name' => 'Polvo',
                'description' => 'O polvo tem 8 braços forrados de ventosas, tem manto grande e falta a concha interna',
                'curiosity' => 'O polvo usa a camuflagem para se assemelhar a outros animais ou para se misturar ao seu ambiente, mudando sua cor e sua aparência ao elevar ou abaixar pequenas protuberâncias por todo o corpo mole. Normalmente, a entrada de sua toca está cheia de sobras de sua comida, como conchas de mexilhão.',
                'size' => 'Até 25 cm de comprimento de manto, até 1 m de braços de comprimento',
            ],
            [
                'name' => 'Dourada',
                'description' => 'Corpo cinza prateado com uma linha dourada entre os olhos e uma mancha escura no início do corpo',
                'curiosity' => 'É uma das espécies mais importantes comercialmente cultivadas. É utilizado na aquicultura em gaiola oceânica na Madeira desde 1997. É um predador oportunista voraz, capaz de adaptar a sua alimentação aos alimentos disponíveis no seu ambiente. Ele muda seu sexo de masculino para feminino com o crescimento.',
                'size' => 'Até 70 cm de comprimento',
            ],
            [
                'name' => 'Algas de lâmina plana',
                'description' => 'Lâminas lisas castanho-dourado ou verde-oliva com pontas largas quadradas crescendo em forma de leque',
                'curiosity' => 'Apresenta bandas de material de diversas cores desenvolvidas paralelamente à borda de crescimento incluindo uma banda verde iridescente que dá à alga uma atraente aparência zoneada. Ele está preso a rochas e frequentemente forma camadas densas. Possui diversos compostos com função defensiva.',
                'size' => 'Até 50 cm de comprimento',
            ],
            [
                'name' => 'Golfinho pintado do atlântico',
                'description' => 'O golfinho pintado do Atlântico é um golfinho encontrado na Corrente do Golfo do Oceano Atlântico Norte. Os membros mais velhos da espécie têm uma coloração manchada muito distinta em todo o corpo.',
                'curiosity' => 'A foca-monge é a espécie de foca mais ameaçada do mundo, com menos de 700 indivíduos no total e cerca de 40 na Madeira. No passado, eram caçados por sua pele, óleo, carne e para uso medicinal. A foca-monge é considerada uma espécie de importância comunitária e suas populações estão legalmente protegidas.',
                'size' => 'Até 2.29 m de comprimento',
            ],
            [
                'name' => 'Foca barbuda',
                'description' => 'As características distintivas deste selo sem orelhas incluem nadadeiras frontais quadradas e cerdas grossas em seu focinho. Os adultos são castanho-acinzentados, mais escuros no dorso. Ocasionalmente, o rosto e o pescoço são marrom-avermelhados, os filhotes de focas nascem com uma pelagem natal marrom-acinzentada com manchas brancas espalhadas nas costas e na cabeça.',
                'curiosity' => 'Focas bentônicas e barbadas se alimentam principalmente de uma variedade de pequenas presas encontradas ao longo do fundo do oceano, incluindo mariscos, lulas e peixes. Seus bigodes funcionam como antenas nos sedimentos do fundo macio. Os adultos tendem a não mergulhar muito fundo, preferindo áreas costeiras rasas com não mais de 300 m de profundidade. Filhotes de até um ano de idade, no entanto, se aventurarão muito mais fundo, mergulhando a uma profundidade de até 450 m',
                'size' => 'Até 2.7 m',
            ],
            [
                'name' => 'Baleia branca',
                'description' => 'O estreitamento súbito da base do pescoço dá-lhe a aparência de ombros, único entre os cetáceos. A cauda cresce e torna-se cada vez mais curvada com ornamentos à medida que o animal envelhece. As nadadeiras são largas e curtas - tornando-as quase quadradas.',
                'curiosity' => 'Estes cetáceos são altamente sociáveis ​​e regularmente formam pequenos grupos, ou vagens, que podem conter entre dois e 25 indivíduos, com uma média de 10 membros. Os vagens tendem a ser instáveis, o que significa que os indivíduos tendem a se mover de um vagem para outro. O rastreamento de rádio mostrou até mesmo que as belugas podem começar em um casulo e, em poucos dias, estar a centenas de quilômetros daquele casulo.',
                'size' => 'Adulto pode alcançar até 5.5 m',
            ],
            [
                'name' => 'Golfinho nariz de garrafa',
                'description' => 'Suas mandíbulas superiores e inferiores alongadas formam o que é chamado de rostro, ou focinho, que dá ao animal seu nome comum. O nariz real e funcional é a bolha de ar no topo de sua cabeça; o septo nasal é visível quando o orifício de respiração está aberto.',
                'curiosity' => 'Os golfinhos nariz-de-garrafa podem viver mais de 40 anos. As mulheres geralmente vivem de 5 a 10 anos a mais do que os homens, com algumas mulheres excedendo os 60 anos. Essa idade extrema é rara e menos de 2% de todos os golfinhos-nariz-de-garrafa viverão mais de 60 anos. Os golfinhos nariz-de-garrafa podem pular a uma altura de 6 metros de altura; eles usam isso para se comunicarem uns com os outros.',
                'size' => 'Até 4m de comprimento',
            ],
            [
                'name' => 'Baleia da groenlândia',
                'description' => 'Tem um corpo grande, robusto e de cor escura e um queixo / maxilar inferior brancos. A baleia tem um enorme crânio triangular, que usa para quebrar o gelo do Ártico para respirar.',
                'curiosity' => 'Eles vivem inteiramente em águas férteis do Ártico e subártico, ao contrário de outras baleias que migram para águas de baixa latitude para se alimentar ou se reproduzir. A cabeça de arco também era conhecida como baleia franca da Groenlândia ou baleia ártica. Os baleeiros americanos os chamavam de campanário, baleia polar ou Rússia ou baleia russa. A cabeça de arco tem a maior boca de qualquer animal.',
                'size' => 'Até 18m de comprimento',
            ],
            [
                'name' => 'Golfinho clímene',
                'description' => 'É muito parecido com o golfinho-rotador. De perto, é possível observar que o bico do Clymene é ligeiramente mais curto que o de seu parente. A barbatana dorsal também é menos ereta e triangular.',
                'curiosity' => 'Passam a maior parte de suas vidas em águas com mais de 100 m de profundidade, mas ocasionalmente se movem para regiões costeiras mais rasas. Alimentam-se de lulas e pequenos peixes de cardume, caçando à noite ou em águas mesopelágicas onde a luz é limitada. Predadores incluem tubarões cortadores de biscoitos, como evidenciado por marcas de mordidas vistas em vários animais.',
                'size' => 'Até 2m de comprimento',
            ],
            [
                'name' => 'Golfinho comum',
                'description' => 'O padrão de cores no corpo é incomum. O dorso é escuro e a barriga é branca, enquanto em cada lado há um padrão de ampulheta colorido em cinza claro, amarelo ou dourado na frente e cinza sujo atrás.',
                'curiosity' => 'Enfrentam uma mistura de ameaças devido à influência humana. Níveis moderados de poluentes metálicos, que parecem ter um impacto negativo na saúde dos golfinhos, foram medidos em algumas populações. Populações foram caçadas na costa do Peru para uso como alimento e isca para tubarões. Na maioria das outras áreas, os golfinhos não foram caçados diretamente.',
                'size' => 'Até 2.5m de comprimento',
            ],
            [
                'name' => 'Falsa orca',
                'description' => 'A falsa baleia assassina é preta ou cinza escuro, embora um pouco mais clara na parte inferior. Possui corpo esguio com cabeça alongada e afilada e 44 dentes. A barbatana dorsal tem forma de foice e as suas barbatanas são estreitas, curtas e pontiagudas',
                'curiosity' => 'Grande golfinho oceânico que habita oceanos em todo o mundo, mas frequenta principalmente regiões tropicais. Foi descrita pela primeira vez em 1846 como uma espécie de toninha baseada em um crânio, que foi revisada quando as primeiras carcaças foram observadas em 1861. O nome "falsa baleia assassina" vem das características do crânio semelhantes às da baleia assassina (Orcinus orca).',
                'size' => 'Até 6m de comprimento',
            ],
            [
                'name' => 'Baleia comum',
                'description' => 'A baleia-comum é geralmente caracterizada por seu bico alto, dorso longo, nadadeira dorsal proeminente e coloração assimétrica.',
                'curiosity' => 'Também conhecido como baleia finback ou rorqual comum e anteriormente conhecido como baleia arenque ou baleia razorback, é um mamífero marinho pertencente à parvorder das baleias de barbatanas. É a segunda maior espécie da Terra, depois da baleia azul.',
                'size' => 'Até 25.9m de comprimento',
            ],
            [
                'name' => 'Golfinho de fraser',
                'description' => 'Eles têm uma constituição robusta, uma barbatana pequena em relação ao tamanho do corpo, nadadeiras visivelmente pequenas. A barbatana dorsal e o bico também são insubstanciais.',
                'curiosity' => 'O golfinho é normalmente avistado em águas tropicais profundas; entre 30 ° S e 20 ° N. O Pacífico Oriental é o local mais confiável para visualizações. Grupos de golfinhos encalhados foram encontrados em lugares distantes como França e Uruguai. No entanto, eles são considerados anômalos e possivelmente devido a condições oceanográficas incomuns, como o El Niño.',
                'size' => 'Até 2.75m de comprimento',
            ],
            [
                'name' => 'Golfinho de risso',
                'description' => 'Corpo anterior relativamente grande e barbatana dorsal, enquanto o posterior diminui para uma cauda relativamente estreita. A cabeça bulbosa possui uma prega vertical na frente.',
                'curiosity' => 'É a única espécie de golfinho do gênero Grampus. É comumente conhecido como golfinho-monge entre os pescadores taiwaneses.',
                'size' => 'Até 4m de comprimento',
            ],
            [
                'name' => 'Foca de Harpa',
                'description' => 'A foca harpa madura tem olhos negros puros. Possui um pelo cinza prateado cobrindo seu corpo, com marcas em forma de harpa preta ou em forma de osso da sorte, dorsalmente.',
                'curiosity' => 'Comparada a outras focas focas, a foca harpa mergulha de profundidades rasas a moderadamente profundas. A profundidade do mergulho varia com a temporada, hora do dia e local. Na subpopulação do Mar da Groenlândia, a taxa média de mergulho é de cerca de 8,3 mergulhos por hora e os mergulhos variam de uma profundidade de menos de 20 a mais de 500m.',
                'size' => 'Até 1.9m de comprimento',
            ],
            [
                'name' => 'Baleia jubarte',
                'description' => 'As jubartes podem ser facilmente identificadas por seu corpo atarracado, corcunda óbvia, coloração dorsal preta e barbatanas peitorais alongadas. A cabeça e a mandíbula são cobertas por protuberâncias chamadas tubérculos, que são folículos capilares e são característicos da espécie. ',
                'curiosity' => 'As baleias jubarte são rorquais, membros da família Balaenopteridae que inclui as baleias azul, barbatana, Bryde, sei e minke. Acredita-se que os rorquals tenham divergido das outras famílias da subordem Mysticeti já na era do Mioceno médio.',
                'size' => 'Adultos alcançam um comprimento entre 12 e 16 m',
            ],
            [
                'name' => 'Orca assassina',
                'description' => 'Uma típica baleia assassina apresenta distintamente o dorso, o peito e os lados brancos e uma mancha branca acima e atrás do olho. Os bezerros nascem com uma tonalidade amarelada ou laranja, que desbota para o branco. Possui um corpo pesado e robusto com uma grande barbatana dorsal de até 1,8 m de altura.',
                'curiosity' => 'As baleias assassinas são encontradas em todos os oceanos e na maioria dos mares. Devido à sua enorme variedade, número e densidade, a distribuição relativa é difícil de estimar, mas eles claramente preferem latitudes mais altas e áreas costeiras a ambientes pelágicos.',
                'size' => 'Variam entre 6 e 8 metros',
            ],
            [
                'name' => 'Foca leopardo',
                'description' => 'A foca leopardo tem uma forma corporal distintamente longa e musculosa, quando comparada com outras focas. Esta espécie de foca é conhecida por sua enorme cabeça e mandíbulas semelhantes a répteis, que permitem que ela seja um dos principais predadores em seu ambiente.',
                'curiosity' => 'As focas leopardo são muito vocais debaixo d\'água durante o verão austral. As focas macho produzem chamadas altas (153 a 177 dB re 1 μPa a 1 m) por muitas horas todos os dias.',
                'size' => 'O comprimento total desta espécie é 2.4–3.5 m',
            ],
            [
                'name' => 'Baleia piloto de aleta longa',
                'description' => 'Apesar de seu nome comum, a baleia-piloto de nadadeira comprida é na verdade uma grande espécie de golfinho. O mesmo é verdade para orcas e várias outras pequenas baleias. Tem uma testa bulbosa e é de cor preta ou cinza escuro com manchas cinza claro ou brancas nas regiões da garganta e da barriga. ',
                'curiosity' => 'As baleias-piloto receberam seu nome da crença original de que havia um "piloto" ou indivíduo líder em seus grupos. O nome do gênero, "Globicephala" é derivado de uma combinação das palavras latinas globus ("globo") e kephale ("cabeça"). ',
                'size' => 'Até 6,7 metros',
            ],
            [
                'name' => 'Golfinho cabeça de melão',
                'description' => 'A cabeça da baleia com cabeça de melão é um cone arredondado que dá ao animal o seu nome comum. O corpo é mais ou menos uniformemente cinza claro, exceto por um rosto cinza escuro - às vezes chamado de "máscara". As nadadeiras são longas e pontudas. A barbatana dorsal é alta com uma ponta pontiaguda - uma reminiscência de sua parente, a baleia assassina. ',
                'curiosity' => 'As baleias com cabeça de melão são animais muito sociais que vivem em grandes grupos de 100 a 1.000. Eles foram observados nadando próximos um do outro e tocando as nadadeiras. Dentro do grupo grande, eles geralmente nadam em grupos menores de 10-14 ',
                'size' => 'Adulto cresce até 3 m',
            ],
            [
                'name' => 'Baleia minke',
                'description' => 'A baleia minke é preta / cinza / roxa.',
                'curiosity' => 'As baleias minke são a segunda menor baleia de barbatana; apenas a baleia franca pigmeu é menor. Ao atingir a maturidade sexual (6–8 anos de idade), os homens medem uma média de 6,9 ​​m (23 pés) e as mulheres 8 m (26 pés) de comprimento, respectivamente. ',
                'size' => 'Comprimentos máximos relatados variam entre 9,1 e 10,7 m',
            ],
            [
                'name' => 'Narval',
                'description' => 'A pigmentação dos narvais é um padrão mosqueado, com manchas marrom-pretas sobre um fundo branco. Eles são mais escuros quando nascem e tornam-se mais brancos com a idade ',
                'curiosity' => 'A característica mais notável do narval macho é uma única presa longa, um dente canino que se projeta do lado esquerdo da mandíbula superior, através do lábio, e forma uma espiral em hélice para a esquerda.',
                'size' => 'Um comprimento médio de 4,1 m',
            ],
            [
                'name' => 'Baleia franca do atlântico norte',
                'description' => 'Como outras baleias francas, a baleia franca do Atlântico Norte, também conhecida como baleia franca do norte ou baleia franca negra, é facilmente distinguida de outras baleias pelas calosidades em sua cabeça',
                'curiosity' => 'Além das atividades de acasalamento realizadas por grupos de fêmeas solteiras e vários machos, chamados SAG (Surface Active Group), as baleias francas do Atlântico Norte parecem menos ativas em comparação com as subespécies no hemisfério sul.',
                'size' => 'Baleias francas adultas do Atlântico Norte em média 13-16 m',
            ],
            [
                'name' => 'Golfinho pintado pantropical',
                'description' => 'Varia significativamente em tamanho e coloração em toda a sua gama. A divisão mais significativa é entre as variedades costeiras e pelágicas. A forma costeira é maior e mais manchada. ',
                'curiosity' => 'É uma espécie de golfinho encontrada em todos os oceanos temperados e tropicais do mundo. A espécie estava começando a ficar ameaçada devido à morte de milhões de indivíduos em redes de cerco com retenida de atum. ',
                'size' => 'Adultos têm cerca de 2,5 m de comprimento',
            ],
            [
                'name' => 'Foca de Ross',
                'description' => 'A pelagem é castanha-escura na zona dorsal e branca prateada na parte inferior. No início do inverno antártico, a pelagem desbota gradualmente para se tornar marrom claro. ',
                'curiosity' => 'Capaz de produzir uma variedade de sons complexos e semelhantes a sirenes que são executados no gelo e debaixo d\'água, onde são transportados por longas distâncias.',
                'size' => 'Alcance um comprimento de cerca de 1,68–2,09 m',
            ],
            [
                'name' => 'Golfinho de dentes rugosos',
                'description' => 'Como o nome comum da espécie indica, os dentes também são distintos, tendo uma superfície rugosa formada por numerosas cristas estreitas e irregulares. ',
                'curiosity' => 'Animais tipicamente sociais, embora indivíduos solitários também sejam avistados. Um grupo médio tem entre dez e vinte membros, mas eles podem variar de dois a noventa. ',
                'size' => 'Alcance de 2,09 a 2,83 metros',
            ],
            [
                'name' => 'Baleia piloto de aleta curta',
                'description' => 'Tem um corpo atarracado, uma testa bulbosa, nenhum bico proeminente, nadadeiras longas agudamente pontiagudas na ponta, cor preta ou cinza escuro, e a nadadeira dorsal inserida para a frente no corpo. ',
                'curiosity' => 'Pode ser confundida com seus parentes, as baleias-piloto de nadadeiras compridas, mas existem várias diferenças. Como seus nomes indicam, suas nadadeiras são mais curtas do que as da baleia-piloto de nadadeiras longas, com uma curva mais suave na borda. Eles têm menos dentes do que a baleia-piloto de nadadeiras compridas, com 14 a 18 em cada mandíbula. ',
                'size' => 'Alcance até 5,5 metros',
            ],
            [
                'name' => 'Baleia franca austral',
                'description' => 'Sua pele é cinza muito escura ou preta, ocasionalmente com algumas manchas brancas na barriga. As calosidades da baleia franca aparecem brancas devido a grandes colônias de ciamídeos ',
                'curiosity' => 'As baleias francas normalmente não cruzam as águas equatoriais quentes para se conectar com outras espécies e (inter) raças: suas camadas espessas de gordura isolante dificultam a dissipação do calor interno do corpo em águas tropicais. ',
                'size' => 'Alcance até 17,5-18 m',
            ],
            [
                'name' => 'Cachalote',
                'description' => 'No topo do crânio da baleia está posicionado um grande complexo de órgãos preenchido com uma mistura líquida de gorduras e ceras chamada espermacete. O objetivo deste complexo é gerar sons de clique poderosos e focados, que o cachalote usa para ecolocalização e comunicação. ',
                'curiosity' => 'É a maior das baleias dentadas e o maior predador dentado. É o único membro vivo do gênero Physeter e uma das três espécies existentes na família do cachalote, junto com o cachalote pigmeu e o cachalote anão do gênero Kogia ',
                'size' => 'Alcance até 20,5 metros',
            ],
            [
                'name' => 'Golfinho rotador',
                'description' => 'A área dorsal é cinza escuro, os lados cinza claro e a parte inferior cinza claro ou branco. Além disso, uma faixa escura vai do olho até a nadadeira, delimitada acima por uma linha fina e clara. ',
                'curiosity' => 'O golfinho rotador vive em quase todas as águas tropicais e subtropicais entre 40 ° N e 40 ° S. A espécie habita principalmente águas costeiras, ilhas ou bancos. No entanto, no Pacífico tropical oriental, os golfinhos vivem longe da costa. Os golfinhos-rotadores podem usar diferentes habitats, dependendo da estação. ',
                'size' => 'Os adultos têm normalmente 129-235 cm de comprimento',
            ],
            [
                'name' => 'Golfinho riscado',
                'description' => 'Sua coloração é muito diferente e torna-se relativamente fácil de notar no mar. A parte inferior é azul, branca ou rosa. Uma ou duas faixas pretas circundam os olhos e, em seguida, percorrem as costas, até a nadadeira. ',
                'curiosity' => 'Eles também podem se misturar com golfinhos comuns. O golfinho listrado é tão capaz quanto qualquer golfinho na realização de acrobacias - frequentemente rompendo e saltando muito acima da superfície da água. ',
                'size' => 'Os adultos têm normalmente 2,6 m de comprimento',
            ],
            [
                'name' => 'Morsa',
                'description' => 'A característica mais proeminente das espécies vivas são suas longas presas. Estes são caninos alongados, que estão presentes em morsas machos e fêmeas e podem atingir um comprimento de 1 m e pesar até 5,4 kg ',
                'curiosity' => 'A morsa é um mamífero da ordem Carnivora. É o único membro sobrevivente da família Odobenidae, uma das três linhagens na subordem Pinnipedia junto com focas verdadeiras (Phocidae) e focas orelhudas (Otariidae). ',
                'size' => 'Adultos têm tipicamente 5 m de comprimento',
            ],
            [
                'name' => 'Foca de weddell',
                'description' => 'A foca Weddell desenvolve um fino casaco de pele ao redor de todo o corpo, exceto em pequenas áreas ao redor das nadadeiras. A cor e o padrão da pelagem variam, muitas vezes desbotando para uma cor mais opaca à medida que o selo envelhece. ',
                'curiosity' => 'Focas de Weddell são comumente encontradas em gelo rápido, ou gelo preso à terra, e se reúnem em pequenos grupos ao redor de rachaduras e buracos no gelo. No inverno, eles ficam na água para evitar nevascas, apenas com a cabeça cutucando através dos orifícios de respiração no gelo. ',
                'size' => 'Adultos têm tipicamente 2,5–3,5 m de comprimento',
            ],
            [
                'name' => 'Golfinho de bico branco',
                'description' => 'O golfinho de bico branco é uma espécie robusta de golfinho com bico curto. ',
                'curiosity' => 'Golfinhos de bico branco têm de 25 a 28 dentes em cada mandíbula, embora os três dentes mais próximos à parte frontal da boca muitas vezes não sejam visíveis, não podendo irromper da gengiva. Eles têm até 92 vértebras, mais do que qualquer outra espécie de golfinho oceânico ',
                'size' => 'Adultos podem atingir 2,3 a 3,1 m',
            ],
            [
                'name' => 'Golfinho de laterais brancas do atlântico',
                'description' => 'A principal característica distintiva é a mancha branca a amarelo pálido encontrada atrás da barbatana dorsal do golfinho em cada lado. Esta variação de cor é única entre as misturas de branco, cinza e azul de outros cetáceos pelágicos. ',
                'curiosity' => 'Frequentemente viajando em grandes pods e exibindo comportamentos aéreos enquanto viajam. Apesar de serem criaturas dóceis, até mesmo conhecido por interagir com várias espécies de cetecos de forma não violenta, mais notavelmente com a baleia-piloto de nadadeiras compridas ',
                'size' => 'Adultos podem atingir 2,5 m',
            ],
            [
                'name' => 'Lobo marinho',
                'description' => 'Lobos-marinhos são caracterizados por seus pavilhões externos, subpêlo denso, vibrissas e membros longos e musculosos. Eles compartilham com outros otariídeos a capacidade de girar seus membros traseiros para a frente, apoiando seus corpos e permitindo-lhes deambular em terra. ',
                'curiosity' => 'Normalmente, as focas se reúnem durante o verão em grandes colônias em praias específicas ou afloramentos rochosos para dar à luz e procriar. Todas as espécies são políginas, o que significa que os machos dominantes se reproduzem com mais de uma fêmea. ',
                'size' => 'O tamanho varia de cerca de 1,5 m, 64 kg no macho Galápagos a 2,5 m, 180 kg no macho adulto da foca da Nova Zelândia.',
            ],
            [
                'name' => 'Leão marinho',
                'description' => 'Leões marinhos são pinípedes caracterizados por abas de orelha externas, patas dianteiras longas, a capacidade de andar de quatro, cabelo curto e espesso e um grande peito e barriga.',
                'curiosity' => 'Leões marinhos consomem grandes quantidades de comida por vez e são conhecidos por comer cerca de 5–8% de seu peso corporal em uma única alimentação. Os leões marinhos podem se mover cerca de 30 km / h na água e, no seu ritmo mais rápido, podem atingir uma velocidade de cerca de 56 km / h. ',
                'size' => 'Um leão-marinho macho da Califórnia pesa em média cerca de 300 kg e tem cerca de 2,4 m de comprimento, enquanto a leoa marinha fêmea pesa 100 kg e tem 1,8 m de comprimento.',
            ],
            [
                'name' => 'Gaivota',
                'description' => 'Eles são geralmente de forma uniforme, com corpos pesados, asas longas e pescoços moderadamente longos. As caudas de todas as espécies, exceto três, são arredondadas; as exceções são a gaivota de Sabine e as gaivotas com cauda de andorinha, que têm cauda bifurcada, e a gaivota de Ross, que tem cauda em forma de cunha. ',
                'curiosity' => 'Gaivotas são alimentadores altamente adaptáveis ​​que oportunisticamente tomam uma grande variedade de presas. Os alimentos ingeridos pelas gaivotas incluem peixes e invertebrados marinhos e de água doce, vivos e já mortos, artrópodes e invertebrados terrestres, como insetos e minhocas, roedores, ovos, carniça, vísceras, répteis, anfíbios, itens de plantas como sementes e frutas, humanos refugo, chips e até outros pássaros. ',
                'size' => 'Gaivotas variam em tamanho de 120 gramas e 29 centímetros a 1,75 kg e 76 cm.',
            ],
            [
                'name' => 'Burrfish manchado',
                'description' => 'Tem um corpo rotundo, que pode ser inflado, com uma cabeça larga e romba e olhos grandes. O órgão nasal dos adultos fica em uma xícara aberta, sem caroço, que nos jovens é um tentáculo com duas aberturas. ',
                'curiosity' => 'Os dentes são fundidos em um bico parecido com um papagaio sem ranhura frontal e a boca é grande.',
                'size' => 'Eles crescem até um comprimento padrão de 50 cm, mas foram registrados até 75 cm.',
            ],
            [
                'name' => 'Enguia-europeia',
                'description' => 'A enguia europeia (Anguilla anguilla) é uma espécie de enguia, um peixe catádromo semelhante a uma cobra. As enguias têm sido importantes fontes de alimento tanto na idade adulta (incluindo enguias gelatinosas do leste de Londres) quanto na forma de enguias de vidro. ',
                'curiosity' => 'Embora a expectativa de vida da espécie na natureza não tenha sido determinada, os espécimes em cativeiro viveram mais de 80 anos. Um espécime conhecido como \ "o Brantevik Eel \" viveu por 155 anos no poço de uma casa de família em Brantevik, uma vila de pescadores no sul da Suécia. ',
                'size' => 'Eles têm normalmente cerca de 60–80 cm (2,0–2,6 pés) e raramente alcançam mais de 1 m (3 pés 3 pol.), mas podem atingir um comprimento de até 1,5 m (4 pés 11 pol.) em casos excepcionais. ',
            ],
        ];

        foreach ($items as $key => $item) {
            $newCreature = Creature::create($item[0]);
            $newCreature
                ->setTranslation('name', 'en', $item[0]['name'])
                ->setTranslation('name', 'pt', $PTitems[$key]['name'])
                ->setTranslation('description', 'en', $item[0]['description'])
                ->setTranslation('description', 'pt', $PTitems[$key]['description'])
                ->setTranslation('curiosity', 'en', $item[0]['curiosity'])
                ->setTranslation('curiosity', 'pt', $PTitems[$key]['curiosity'])
                ->setTranslation('size', 'en', $item[0]['size'])
                ->setTranslation('size', 'pt', $PTitems[$key]['size'])
                ->save();
        }

        $diveReporter = Source::where('name', 'DIVE_REPORTER')->value('id');
        $whaleReporter = Source::where('name', 'WHALE_REPORTER')->value('id');

        foreach (Creature::all() as $key => $creature) {

            if (!empty($items[$key][1])) {
                $creature->sources()->attach($diveReporter);
            }
            if (!empty($items[$key][2])) {
                $creature->sources()->attach($whaleReporter);
            }
        }
    }
}
