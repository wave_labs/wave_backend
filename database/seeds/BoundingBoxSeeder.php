<?php

use App\BoundingBoxCategory;
use App\BoundingBoxClass;
use App\BoundingBoxImage;
use Illuminate\Database\Seeder;

class BoundingBoxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'storage/images/studies/bounding-box/';

        $images = [
            [
                'image' => $path . '4.jpg', 'width' => 1200, 'height' => 720,
                'category_id' => 2, 'filename' => '4'
            ],
            [
                'image' => $path . '5.jpg', 'width' => 1200, 'height' => 720,
                'category_id' => 2, 'filename' => '5'
            ],
            [
                'image' => $path . '6.jpg', 'width' => 1200, 'height' => 720,
                'category_id' => 2, 'filename' => '6'
            ],
            [
                'image' => $path . '7.jpg', 'width' => 1200, 'height' => 720,
                'category_id' => 2, 'filename' => '7'
            ],
            [
                'image' => $path . '8.jpg', 'width' => 1200, 'height' => 720,
                'category_id' => 2, 'filename' => '8'
            ],
            [
                'image' => $path . '9.jpg', 'width' => 1200, 'height' => 720,
                'category_id' => 2, 'filename' => '9'
            ],
        ];

        $classes = [
            ['name' => 'odontoceti', 'category_id' => 1, 'image' => 'storage/images/bounding-box/icons/odontoceti.png'],
            ['name' => 'mysticeti', 'category_id' => 1, 'image' => 'storage/images/bounding-box/icons/mysticeti.png'],
            ['name' => 'pinniped', 'category_id' => 1, 'image' => 'storage/images/bounding-box/icons/pinniped.png'],
            ['name' => 'bird', 'category_id' => 1, 'image' => 'storage/images/bounding-box/icons/bird.png'],
            ['name' => 'turtle', 'category_id' => 1, 'image' => 'storage/images/bounding-box/icons/turtle.png'],
            ['name' => 'plastic', 'category_id' => 2, 'image' => 'storage/images/bounding-box/icons/plastic.png'],
            ['name' => 'fishing_gear', 'category_id' => 2, 'image' => 'storage/images/bounding-box/icons/fishing_gear.png'],
            ['name' => 'paper', 'category_id' => 2, 'image' => 'storage/images/bounding-boxicons/paper.png'],
            ['name' => 'metal', 'category_id' => 2, 'image' => 'storage/images/bounding-box/icons/metal.png'],
            ['name' => 'other', 'category_id' => 2, 'image' => 'storage/images/bounding-box/icons/other.png'],
            ['name' => 'wood', 'category_id' => 2, 'image' => 'storage/images/bounding-boxicons/wood.png'],
            ['name' => 'rubber', 'category_id' => 2, 'image' => 'storage/images/bounding-box/icons/rubber.png'],
            ['name' => 'fabric', 'category_id' => 2, 'image' => 'storage/images/bounding-boxicons/fabric.png'],
        ];

        $categories = [
            ['name' => 'megafauna'],
            ['name' => 'marine litter'],
        ];

        foreach ($categories as $category) {
            BoundingBoxCategory::create($category);
        }

        foreach ($images as $image) {
            BoundingBoxImage::create($image);
        }

        foreach ($classes as $class) {
            BoundingBoxClass::create($class);
        }
    }
}
