<?php

use App\IotDevice;
use App\IotDeviceCategory;
use App\IotDeviceField;
use App\IotDeviceState;
use App\IotEnvironmentType;
use App\IotDeviceType;
use App\IotFeature;
use App\IotFeatureHasParameter;
use App\IotParameter;
use App\IotRule;
use App\IotSection;
use App\IotTemplate;
use Illuminate\Database\Seeder;

class IotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [['name' => 'latitude'], ['name' => 'longitude'], ['name' => 'single horizontal bar'], ['name' => 'timestamp'], ['name' => 'gauge'], ['name' => 'line chart', 'has_timeline' => true], ['name' => 'bar chart', 'has_timeline' => true], ['name' => 'compass chart']];
        foreach ($items as $item) {
            IotFeature::create($item);
        }

        //---------------------------------

        $items = ['latitude', 'longitude', 'timestamp'];
        foreach ($items as $item) {
            IotRule::create([
                'name' => $item,
            ]);
        }

        //---------------------------------

        $items = ['max', 'min', 'unit'];
        foreach ($items as $item) {
            IotParameter::create([
                'name' => $item,
            ]);
        }

        //---------------------------------

        IotFeatureHasParameter::create(['feature_id' => 5, 'parameter_id' => 1]);
        IotFeatureHasParameter::create(['feature_id' => 5, 'parameter_id' => 2]);
        IotFeatureHasParameter::create(['feature_id' => 5, 'parameter_id' => 3]);
        IotFeatureHasParameter::create(['feature_id' => 3, 'parameter_id' => 1]);
        IotFeatureHasParameter::create(['feature_id' => 3, 'parameter_id' => 2]);

        //---------------------------------
        $items = ['Active', 'Inactive'];
        foreach ($items as $item) {
            IotDeviceState::create([
                'name' => $item,
            ]);
        }

        //---------------------------------
        $items = ['receiver', 'collector'];
        foreach ($items as $item) {
            IotDeviceCategory::create([
                'name' => $item,
            ]);
        }

        //---------------------------------

        $iotDeviceTypePath = 'storage/images/logo/';
        $items =  [
            ['name' => 'poseidon', 'description' => '', 'icon' => $iotDeviceTypePath . 'poseidon.svg'],
            ['name' => 'seamote', 'description' => '', 'icon' => $iotDeviceTypePath . 'seamote.svg'],
            ['name' => 'triton', 'description' => '', 'icon' => $iotDeviceTypePath . 'triton.svg'],
            ['name' => 'oceanus', 'description' => '', 'icon' => $iotDeviceTypePath . 'oceanus.svg'],
        ];

        foreach ($items as $item) {
            IotDeviceType::create($item);
        }

        //---------------------------------
        $items =  [
            ['name' => 'Underwater', 'description' => 'Devices that collect data underwater'],
            ['name' => 'Surface', 'description' => 'Devices that collect data on the surface of the water'],
            ['name' => 'Aerial', 'description' => 'Devices that collect data from the air'],
        ];

        foreach ($items as $item) {
            IotEnvironmentType::create($item);
        }

        //---------------------------------
        $items =  [
            ['name' => 'Map', 'static' => true],
            ['name' => 'Section1'],
            ['name' => 'Section2'],
            ['name' => 'Section3'],
            ['name' => 'Section4'],
        ];

        foreach ($items as $item) {
            IotSection::create($item);
        }

        //---------------------------------
        $items =  [
            [['name' => 'Table View', 'image' => '/images/iot/template/1.webp'], []],
            [['name' => 'Minimalist Map', 'image' => '/images/iot/template/2.webp'], [1]],
            [['name' => 'One Section', 'image' => '/images/iot/template/3.webp'], [1, 2]],
            [['name' => 'Two Sections', 'image' => '/images/iot/template/4.webp'], [1, 2, 3]],
            [['name' => 'Three Sections', 'image' => '/images/iot/template/5.webp'], [1, 2, 3, 4]],
            [['name' => 'Four Sections', 'image' => '/images/iot/template/6.webp'], [1, 2, 3, 4, 5]],
        ];

        foreach ($items as $item) {
            $template = IotTemplate::create($item[0]);
            $template->sections()->sync($item[1]);
        }

        //---------------------------------

        $items = [
            ['name' => 'Tag 2', 'serial_number' => 'Tag 2', 'id_type' => 3, 'id_environment' => 1, 'id_state' => 1, 'template_id' => 1],
            ['name' => 'Tag 4', 'serial_number' => 'Tag 4', 'id_type' => 3, 'id_environment' => 1, 'id_state' => 1, 'template_id' => 2],
            ['name' => 'Tag 5', 'serial_number' => 'Tag 5', 'id_type' => 3, 'id_environment' => 1, 'id_state' => 1, 'template_id' => 1],
            ['name' => 'Tag 6', 'serial_number' => 'Tag 6', 'id_type' => 3, 'id_environment' => 1, 'id_state' => 1, 'template_id' => 2],
            ['name' => 'Tag 7', 'serial_number' => 'Tag 7', 'id_type' => 3, 'id_environment' => 1, 'id_state' => 1, 'template_id' => 1],
        ];

        foreach ($items as $item) {
            $iotDevice = IotDevice::create($item);
            $iotDevice->categories()->attach([2]);
        }

        //--------------------
        $items = [
            ['key' => 'lat', 'device_id' => 1],
            ['key' => 'lon', 'device_id' => 1],
            ['key' => 'lat', 'device_id' => 2],
            ['key' => 'lon', 'device_id' => 2],
            ['key' => 'lat', 'device_id' => 3],
            ['key' => 'lon', 'device_id' => 3],

            ['key' => 'lat', 'device_id' => 4],
            ['key' => 'lon', 'device_id' => 4],
            ['key' => 'lat', 'device_id' => 5],
            ['key' => 'lon', 'device_id' => 5],
        ];

        foreach ($items as $item) {
            IotDeviceField::create($item);
            //DB::table('iot_device_has_iot_field_type')->insert($item);
        }
    }
}
