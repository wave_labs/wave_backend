<?php

use Illuminate\Database\Seeder;
use App\SightingBehaviour;

class SightingBehaviourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SightingBehaviour::create([
            'behaviour' => 'Not sure',
        ]);
        
        SightingBehaviour::create([
            'behaviour' => 'Foraging',
        ]);

        SightingBehaviour::create([
            'behaviour' => 'Socializing',
        ]);

        SightingBehaviour::create([
            'behaviour' => 'Migrating',
        ]);

        SightingBehaviour::create([
            'behaviour' => 'Resting',
        ]);
    }
}
