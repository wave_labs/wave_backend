<?php

use Illuminate\Database\Seeder;
use App\Image;

class ImgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Image::create([
            'type' => 'sighting',
            'photo_number' => 1,
            'url' => 'image/sighting/1',
            'creature_id' => 19
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 2,
            'url' => 'image/sighting/2',
            'creature_id' => 20
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 3,
            'url' => 'image/sighting/3',
            'creature_id' => 21
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 4,
            'url' => 'image/sighting/4',
            'creature_id' => 22
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 5,
            'url' => 'image/sighting/5',
            'creature_id' => 23
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 6,
            'url' => 'image/sighting/6',
            'creature_id' => 24
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 7,
            'url' => 'image/sighting/7',
            'creature_id' => 25
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 8,
            'url' => 'image/sighting/8',
            'creature_id' => 26
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 9,
            'url' => 'image/sighting/9',
            'creature_id' => 27
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 10,
            'url' => 'image/sighting/10',
            'creature_id' => 28
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 11,
            'url' => 'image/sighting/11',
            'creature_id' => 29
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 12,
            'url' => 'image/sighting/12',
            'creature_id' => 30
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 13,
            'url' => 'image/sighting/13',
            'creature_id' => 31
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 14,
            'url' => 'image/sighting/14',
            'creature_id' => 32
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 15,
            'url' => 'image/sighting/15',
            'creature_id' => 33
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 16,
            'url' => 'image/sighting/16',
            'creature_id' => 34
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 17,
            'url' => 'image/sighting/17',
            'creature_id' => 35
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 18,
            'url' => 'image/sighting/18',
            'creature_id' => 36
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 19,
            'url' => 'image/sighting/19',
            'creature_id' => 37
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 20,
            'url' => 'image/sighting/20',
            'creature_id' => 38
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 21,
            'url' => 'image/sighting/21',
            'creature_id' => 39
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 22,
            'url' => 'image/sighting/22',
            'creature_id' => 40
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 23,
            'url' => 'image/sighting/23',
            'creature_id' => 41
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 24,
            'url' => 'image/sighting/24',
            'creature_id' => 42
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 25,
            'url' => 'image/sighting/25',
            'creature_id' => 43
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 26,
            'url' => 'image/sighting/26',
            'creature_id' => 44
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 27,
            'url' => 'image/sighting/27',
            'creature_id' => 45
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 28,
            'url' => 'image/sighting/28',
            'creature_id' => 46
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 29,
            'url' => 'image/sighting/29',
            'creature_id' => 47
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 30,
            'url' => 'image/sighting/30',
            'creature_id' => 48
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 31,
            'url' => 'image/sighting/31',
            'creature_id' => 49
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 32,
            'url' => 'image/sighting/32',
            'creature_id' => 50
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 33,
            'url' => 'image/sighting/33',
            'creature_id' => 51
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 34,
            'url' => 'image/sighting/34',
            'creature_id' => 52
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 35,
            'url' => 'image/sighting/35',
            'creature_id' => 11
        ]);

        Image::create([
            'type' => 'sighting',
            'photo_number' => 36,
            'url' => 'image/sighting/36',
            'creature_id' => 53
        ]);

        // DIVE IMAGES

        Image::create([
            'type' => 'dive',
            'photo_number' => 1,
            'url' => 'api/image/dive/1',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 2,
            'url' => 'api/image/dive/2',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 3,
            'url' => 'api/image/dive/3',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 4,
            'url' => 'api/image/dive/4',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 5,
            'url' => 'api/image/dive/5',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 6,
            'url' => 'api/image/dive/6',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 7,
            'url' => 'api/image/dive/7',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 8,
            'url' => 'api/image/dive/8',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 9,
            'url' => 'api/image/dive/9',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 10,
            'url' => 'api/image/dive/10',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 11,
            'url' => 'api/image/dive/11',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 12,
            'url' => 'api/image/dive/12',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 13,
            'url' => 'api/image/dive/13',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 14,
            'url' => 'api/image/dive/14',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 15,
            'url' => 'api/image/dive/15',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 16,
            'url' => 'api/image/dive/16',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 17,
            'url' => 'api/image/dive/17',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 18,
            'url' => 'api/image/dive/18',
        ]);

        Image::create([
            'type' => 'dive',
            'photo_number' => 19,
            'url' => 'api/image/dive/19',
        ]);

        Image::create([
            'type' => 'litter',
            'photo_number' => 1,
            'material' => 'Artificial polymer materials',
            'url' => 'api/image/litter/1',
        ]);

        Image::create([
            'type' => 'litter',
            'photo_number' => 2,
            'material' => 'Rubber',
            'url' => 'api/image/litter/2',
        ]);

        Image::create([
            'type' => 'litter',
            'photo_number' => 3,
            'material' => 'Cloth/textile',
            'url' => 'api/image/litter/3',
        ]);

        Image::create([
            'type' => 'litter',
            'photo_number' => 4,
            'material' => 'Paper/Cardboard',
            'url' => 'api/image/litter/4',
        ]);

        Image::create([
            'type' => 'litter',
            'photo_number' => 5,
            'material' => 'Processed/worked wood',
            'url' => 'api/image/litter/5',
        ]);

        Image::create([
            'type' => 'litter',
            'photo_number' => 6,
            'material' => 'Glass/ceramics',
            'url' => 'api/image/litter/6',
        ]);

        Image::create([
            'type' => 'litter',
            'photo_number' => 7,
            'material' => 'Chemicals',
            'url' => 'api/image/litter/7',
        ]);

        Image::create([
            'type' => 'litter',
            'photo_number' => 8,
            'material' => 'Food waste',
            'url' => 'api/image/litter/8',
        ]);

        Image::create([
            'type' => 'litter',
            'photo_number' => 9,
            'material' => 'Other',
            'url' => 'api/image/litter/9',
        ]);
    }
}
