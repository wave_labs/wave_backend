<?php

use App\Creature;
use App\CreatureSize;
use Illuminate\Database\Seeder;

class CreatureSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $creatures = Creature::all();

        foreach ($creatures as $creature) {
            CreatureSize::create([
                'sm' => '30',
                'lg' => '50',
                'unit' => 'cm',
                'creature_id' => $creature->id
            ]);

            $creature->activities()->attach([1, 2, 3]);
        }
    }
}
