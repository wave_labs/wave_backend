<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //studies seeds
        $this->call(ActivitySeeder::class);
        $this->call(StudySeeder::class);
        $this->call(StudyQuestionSeeder::class);


        $this->call(SourceSeeder::class);
        $this->call(UserOccupationSeeder::class);
        $this->call(VehicleTypeSeeder::class);
        $this->call(VehicleSeeder::class);
        $this->call(BeaufortScaleTableSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(CreatureTypeSeeder::class);
        $this->call(CreatureTableSeeder::class);
        $this->call(CreaturePhotoSeeder::class);
        $this->call(CreatureSizeSeeder::class);
        $this->call(DivingSpotSeeder::class);
        $this->call(ImgSeeder::class);
        $this->call(AbundanceTableSeeder::class);
        $this->call(FormSeeder::class);
        $this->call(QuestionSeeder::class);
        $this->call(AudioSeeder::class);
        $this->call(VideoSeeder::class);
        $this->call(SightingBehaviourSeeder::class);
        $this->call(WhatsNewTypeSeeder::class);
        $this->call(SuggestionTypeSeeder::class);
        $this->call(LitterCategorySeeder::class);
        $this->call(LitterSubCategorySeeder::class);

        $this->call(StopwatchSeeder::class);
        $this->call(BoundingBoxSeeder::class);

        $this->call(DomeSeeder::class);

        $this->call(LitterCategoryImageSeeder::class);

        //ai seeds
        $this->call(AiSeeder::class);


        //iot seeds

        $this->call(IotSeeder::class);
        $this->call(IotDeviceReportSeeder::class);
        $this->call(IotDeviceReportFieldSeeder::class);


        //enm seeds
        $this->call(EnmSeeder::class);
        $this->call(CopernicusSeeder::class);

        //insect seeds
        $this->call(InsectReferenceTagSeeder::class);
        $this->call(InsectReferenceKeywordSeeder::class);
        $this->call(InsectReferenceCategorySeeder::class);


        Model::reguard();
    }
}
