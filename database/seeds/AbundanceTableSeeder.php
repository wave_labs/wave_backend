<?php

use Illuminate\Database\Seeder;
use App\Abundance;

class AbundanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Abundance::create([
			'creature_id'=>'1',
			'level_1' => '1',
        	'level_2' => '2',
        	'level_3' => '3',
        	'level_4' => '> 3',
    	]);

    	Abundance::create([
			'creature_id'=>'2',
			'level_1' => '0 - 1 m^2',
        	'level_2' => '1 - 5 m^2',
        	'level_3' => '5 - 10 m^2',
        	'level_4' => '> 10 m^2',
		]);
		
		Abundance::create([
			'creature_id'=>'3',
			'level_1' => '1 - 3',
        	'level_2' => '4 - 6',
        	'level_3' => '7 - 10',
        	'level_4' => '> 10',
		]);
		
		Abundance::create([
			'creature_id'=>'4',
			'level_1' => '1 - 3',
        	'level_2' => '4 - 6',
        	'level_3' => '7 - 10',
        	'level_4' => '> 10',
		]);

		Abundance::create([
			'creature_id'=>'5',
			'level_1' => '1 - 3',
        	'level_2' => '4 - 6',
        	'level_3' => '7 - 10',
        	'level_4' => '> 10',
		]);

		Abundance::create([
			'creature_id'=>'6',
			'level_1' => '1 - 3',
        	'level_2' => '4 - 6',
        	'level_3' => '7 - 10',
        	'level_4' => '> 10',
		]);

		Abundance::create([
			'creature_id'=>'7',
			'level_1' => '1',
        	'level_2' => '2 - 3',
        	'level_3' => '4 - 5',
        	'level_4' => '> 5',
		]);

		Abundance::create([
			'creature_id'=>'8',
			'level_1' => '1 - 3',
        	'level_2' => '4 - 6',
        	'level_3' => '7 - 10',
        	'level_4' => '> 10',
		]);

		Abundance::create([
			'creature_id'=>'9',
			'level_1' => '1 - 15',
        	'level_2' => '16 - 30',
        	'level_3' => '31 - 50',
        	'level_4' => '> 50',
		]);

		Abundance::create([
			'creature_id'=>'10',
			'level_1' => '1 - 15',
        	'level_2' => '16 - 30',
        	'level_3' => '31 - 50',
        	'level_4' => '> 50',
		]);

		Abundance::create([
			'creature_id'=>'11',
			'level_1' => '1',
        	'level_2' => '2',
        	'level_3' => '3',
        	'level_4' => '> 3',
		]);
		
		Abundance::create([
			'creature_id'=>'12',
			'level_1' => '1',
        	'level_2' => '2',
        	'level_3' => '3',
        	'level_4' => '> 3',
    	]);
		
		Abundance::create([
			'creature_id'=>'13',
			'level_1' => '1',
        	'level_2' => '2 - 3',
        	'level_3' => '4 - 5',
        	'level_4' => '> 5',
		]);

		Abundance::create([
			'creature_id'=>'14',
			'level_1' => '1',
        	'level_2' => '2',
        	'level_3' => '3',
        	'level_4' => '> 3',
		]);
		
		Abundance::create([
			'creature_id'=>'15',
			'level_1' => '1 - 5',
        	'level_2' => '6 - 10',
        	'level_3' => '11 - 15',
        	'level_4' => '> 15',
		]);
		
		Abundance::create([
			'creature_id'=>'16',
			'level_1' => '1',
        	'level_2' => '2',
        	'level_3' => '3',
        	'level_4' => '> 3',
		]);

		Abundance::create([
			'creature_id'=>'17',
			'level_1' => '1 - 3',
        	'level_2' => '4 - 6',
        	'level_3' => '7 - 10',
        	'level_4' => '> 10',
		]);

		Abundance::create([
			'creature_id'=>'18',
			'level_1' => '1 - 15',
        	'level_2' => '16 - 30',
        	'level_3' => '31 - 50',
        	'level_4' => '> 50',
		]);
    }
}
