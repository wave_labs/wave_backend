<?php

use App\InsectReferenceTag;
use Illuminate\Database\Seeder;

class InsectReferenceTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            [
                'tag' => 'Collection objects',
            ],
            [
                'tag' => 'Taxon names',
            ],
            [
                'tag' => 'Synonym',
            ],
            [
                'tag' => 'Pylogenetic relationships',
            ],
            [
                'tag' => 'Description',
            ],
            [
                'tag' => 'Identification key',
            ],
            [
                'tag' => 'Illustrations',
            ],
            [
                'tag' => 'Diagnostic characters',
            ],
            [
                'tag' => 'Female genitalia',
            ],
            [
                'tag' => 'Inmatures',
            ],
            [
                'tag' => 'Host plants',
            ],
            [
                'tag' => 'Distributions',
            ],
            [
                'tag' => 'Acoustic calls',
            ],
            [
                'tag' => 'Behaviour',
            ],
            [
                'tag' => 'Cytogenetics',
            ],
            [
                'tag' => 'Parasitoids',
            ],
            [
                'tag' => 'Vector of',
            ]
        ];

        foreach ($tags as $key => $tag) {
            InsectReferenceTag::create($tag);
        }
    }
}
