<?php

use Illuminate\Database\Seeder;
use App\Question;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        //----------------------------------------------------------------------------------------------------
        //                                              PANAS-SF
        //----------------------------------------------------------------------------------------------------

        Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt interested over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,  	
        	'question' => 'Indicate the extent you have felt distressed over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt excited over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt upset over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt strong over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt guilty over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,       	
        	'question' => 'Indicate the extent you have felt scared over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,      	
        	'question' => 'Indicate the extent you have felt hostile over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,	
        	'question' => 'Indicate the extent you have felt enthusiastic over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt proud over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt irritable over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt alert over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt ashamed over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt inspired over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt nervous over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt determined over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt attentive over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt jittery over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt active over the past week',
            'scale' => 7,
    	]);

    	Question::create([
			'form_id' => 3,
        	'question' => 'Indicate the extent you have felt afraid over the past week',
            'scale' => 7,
    	]);*/

        //----------------------------------------------------------------------------------------------------
        //                                              SUS
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 1,
            'question' => 'I think I would like to use this app frequently',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 1,
            'question' => 'I found the app unnecessarily complex',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 1,
            'question' => 'I thought the app was easy to use',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 1,
            'question' => 'I think that I would need the support of a technical person to be able to use this app',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 1,
            'question' => 'I found the various functions in this app were well integrated',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 1,
            'question' => 'I thought there was too much inconsistency in this app',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 1,
            'question' => 'I would imagine that most people would learn to use this app very quickly',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 1,
            'question' => 'I found the app very cumbersome to use',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 1,
            'question' => 'I felt very confident using the app',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 1,
            'question' => 'I needed to learn a lot of things before I could get going with this app',
            'scale' => 5,
        ]);
        /*

        //----------------------------------------------------------------------------------------------------
        //                                              NASA-TLX
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 3,
            'question' => 'While using the app, how mentally demanding was the task?',
            'scale' => 21,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'While using the app, how physically demanding was the task?',
            'scale' => 21,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'While using the app, how hurried or rushed was the pace of the task?',
            'scale' => 21,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'While using the app, how successful were you in accomplishing what you were asked to do?',
            'scale' => 21,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'While using the app, how hard did you have to work to accomplish your level of performance?',
            'scale' => 21,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'While using the app, how insecure, discouraged, irritated, stressed, and annoyed wereyou?',
            'scale' => 21,
        ]);

        //----------------------------------------------------------------------------------------------------
        //                                              FSS
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I feel just the right amount of challenge',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, my thoughts/activities run fluidly and smoothly',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I do not notice time passing',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I have no difficulty concentrating.',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, my mind is completely clear',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I am totally absorbed in what I am doing',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, the right thoughts/movements occur of their own accord',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I know what I have to do each step of the way',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'scale' => 7,
            'question' => 'While using the app, I feel that I have everything under control',
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I am completely lost in thought',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'Compared to all other activities which I partake in, this one is easy',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'I think that my competence in this area is low',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'For me personally, the current demands are too low',
            'scale' => 7,
        ]);

        


        //----------------------------------------------------------------------------------------------------
        //                                              FSS
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I feel just the right amount of challenge',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, my thoughts/activities run fluidly and smoothly',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I do not notice time passing',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I have no difficulty concentrating.',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, my mind is completely clear',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I am totally absorbed in what I am doing',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, the right thoughts/movements occur of their own accord',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I know what I have to do each step of the way',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'scale' => 7,
            'question' => 'While using the app, I feel that I have everything under control',
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'While using the app, I am completely lost in thought',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'Compared to all other activities which I partake in, this one is easy',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'I think that my competence in this area is low',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 4,
            'question' => 'For me personally, the current demands are too low',
            'scale' => 7,
        ]);

        //----------------------------------------------------------------------------------------------------
        //                                              PRESENCE
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 2,
            'question' => 'How much were you able to control events?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How responsive was the app to actions that you initiated (or performed)?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How natural did your interactions with the app seem?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How much did the visual aspects of the app involve you?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How natural was the mechanism which controlled movement through the app?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How compelling was your sense of objects moving through app?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How much did your experiences in the app seem consistent with your real world experiences?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'Were you able to anticipate what would happen next in response to the actions that you performed?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How completely were you able to actively survey or search the app using vision?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How compelling was your sense of moving around inside the app?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How closely were you able to examine objects?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How well could you examine objects from multiple viewpoints?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How involved were you in the app experience?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How much delay did you experience between your actions and expected outcomes?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How quickly did you adjust to the app experience?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How proficient in moving and interacting with the app did you feel at the end of the experience?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How much did the visual display quality interfere or distract you from performing assigned tasks or required activities?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How much did the control devices interfere with the performance of assigned tasks or with other activities?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How well could you concentrate on the assigned tasks or required activities rather than on the mechanisms used to perform those tasks or activities?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How much did the auditory aspects of the environment involve you?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How well could you identify sounds?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How well could you localize sounds?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How well could you actively survey or search the app using touch?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'How well could you move or manipulate objects in the app?',
            'scale' => 7,
        ]);

        //----------------------------------------------------------------------------------------------------
        //                                              SAM
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 7,
            'question' => 'Pick a word which describes the best your experience',
            'scale' => 10,
        ]);

        Question::create([
            'form_id' => 7,
            'question' => 'How much did this app arouse you?',
            'scale' => 10,
        ]);

        Question::create([
            'form_id' => 7,
            'question' => 'How much did you find this app attractive?',
            'scale' => 10,
        ]);

        Question::create([
            'form_id' => 7,
            'question' => 'How much dominant you felt using this app?',
            'scale' => 10,
        ]);

        //----------------------------------------------------------------------------------------------------
        //                                              STUDY
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 8,
            'question' => 'Were you looking more to the actual whales and dolphins than the app?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How important to you was the sound during the whale watching activity?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How much you liked reporting the sightings?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How much you liked classifying the cetacean sounds?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How much you liked gathering the cetacean photos?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How much you would be willing to track the reported cetaceans from the shore?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How much you would be willing to have the future updates of your spotted cetacean in your phone, during your daily life routine?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How much you would be willing to adopt a digital version of the spotted cetacean?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'If you could, would you use your adopted cetacean as Tamagotchi?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'Please rate your previous experience with Tamagotchi',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'Please rate your previous experience as marine biologist',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'Please rate your previous experience as citizen scientist',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'Please rate your previous experience with green energy',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'During this experience, you used 100% solar energy, how much is this important to you?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How much you liked/would you like to manipulate the collected images? E.g. pinch, zoom, rotate',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How much you would like to know more about cetaceans\' vocal calls before the experience?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => '>While collecting an underwater photo, how much would you like to have an option to control the camera from the app to follow the cetacean?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How much you would be interested in having an option of collecting the near real-time underwater 360 video of cetaceans?',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 8,
            'question' => 'How much you would like to visit the place which offers the interactive/auugmented reality/virtual reality underwater 360 experience where you swim and dive with cetaceans?',
            'scale' => 7,
        ]);
        */

        //----------------------------------------------------------------------------------------------------
        //                                              NEP
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 2,
            'question' => 'We are approaching the limit of the number of people the Earth can support',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'Humans have the right to modify the natural environment to suit their needs',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'When humans interfere with nature it often produces disastrous consequences',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'Human ingenuity will insure that we do not make the Earth unlivable',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'Humans are seriously abusing the environment',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'The Earth has plenty of natural resources if we just learn how to develop them',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'Plants and animals have as much right as humans to exist',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'The balance of nature is strong enough to cope with the impacts of modern industrial nations',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'Despite our special abilities, humans are still subject to the laws of nature',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'The so-called “ecological crisis” facing humankind has been greatly exaggerated',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'The Earth is like a spaceship with very limited room and resources',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'Humans were meant to rule over the rest of nature',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'The balance of nature is very delicate and easily upset',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'Humans will eventually learn enough about how nature works to be able to control it',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 2,
            'question' => 'If things continue on their present course, we will soon experience a major ecological catastrophe',
            'scale' => 5,
        ]);


        //----------------------------------------------------------------------------------------------------
        //                                              PANAS-X
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt cheerful',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt disgusted',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt attentive',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt bashful',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt sluggish',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt daring',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt surprised',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt strong',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt scornful',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt relaxed',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt irritable',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt delighted',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt inspired',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt fearless',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt disgusted with self',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt sad',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt calm',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt afraid',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt tired',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt amazed',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt shaky',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt happy',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt timid',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt alone',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt alert',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt upset',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt angry',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt bold',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt blue',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt shy',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt active',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt guilty',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt joyful',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt nervous',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt lonely with self',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt sleepy',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt excited',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt hostile',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt proud',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt jittery',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt lively',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt ashamed',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt at ease',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt scared',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt drowsy',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt angry at self',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt enthusiastic',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt downhearted',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt sheepish',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt distressed',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt blameworthy',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt determined',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt frightened',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt astonished',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt interested with self',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt loathing',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt confident',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt energetic',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt concentrating',
            'scale' => 5,
        ]);

        Question::create([
            'form_id' => 3,
            'question' => 'Indicate the extent you have felt dissatisfied with self',
            'scale' => 5,
        ]);

        //----------------------------------------------------------------------------------------------------
        //                                                EVAL
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 4,
            'question' => 'Rate the overall experience of the dive',
            'scale' => 5,
        ]);

        //----------------------------------------------------------------------------------------------------
        //                                              IMI
        //----------------------------------------------------------------------------------------------------

        Question::create([
            'form_id' => 5,
            'question' => 'I enjoyed using this app very much',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'Using this app was fun to do',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I thought this was a boring app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'This app did not hold my attention at all',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I would describe using this app as very interesting',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I thought using this app was quite enjoyable',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'While I was using this app, I was thinking about how much I enjoyed it',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I think I am pretty good at using this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I think I did pretty well at using this app, compared to other persons',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'After working at using this app for awhile, I felt pretty competent',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I am satisfied with my performance at using this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I was pretty skilled at using this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'Using this app was an activity that I couldn’t do very well',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I put a lot of effort into this',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I didn’t try very hard to do well at using this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I tried very hard on using this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'It was important to me to do well at using this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I didn’t put much energy into using this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I did not feel nervous at all while using this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I felt very tense while using this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I was very relaxed in using this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I was anxious while working on this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I felt pressured while doing tasks in this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I believe I had some choice about doing this activity',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I felt like it was not my own choice to do this task',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I didn’t really have a choice about doing this task',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I felt like I had to do this',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I did this activity because I had no choice',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I did this activity because I wanted to',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I did this activity because I had to',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I believe this activity could be of some value to me',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I think that doing this activity is useful for marine biologists and tourists',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I think this is important to do because it can raise awareness about ocean and cetacean concerns',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I would be willing to do this again because it has some value to me',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I think doing this activity could help me to become a sea citizen scientist',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I believe doing this activity could be beneficial to me',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I think this is an important activity',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'While using the app, I felt really distant to the spotted cetaceans',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'While using the app, I really doubt that spotted cetaceans and I would ever be friends',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I felt like I could really trust this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I’d like a chance to interact with this app more often',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I’d really prefer not to interact with this app in the future',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'I don’t feel like I could really trust this app',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'While using the app, it is likely that the spotted cetaceans and I could become friends if we interacted a lot',
            'scale' => 7,
        ]);

        Question::create([
            'form_id' => 5,
            'question' => 'While using the app, I feel close to the spotted cetaceans',
            'scale' => 7,
        ]);
    }
}
