<?php

use App\Vehicle;
use Illuminate\Database\Seeder;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vehicle::create([
            'vehicle' => 'Austronesian ship',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Barges',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Canoe‎',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Catamaran',
            'vehicle_type_id' => 5
        ]);


        Vehicle::create([
            'vehicle' => 'Chesapeake Bay boat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Dinghie',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Electric boat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Ferry',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Fireboat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Fishing vessel',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'High-speed craft‎',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Houseboat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Hydrofoil',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Indigenous boat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Inflatable boat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Kayak',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Lifeboat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Military boat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Model boat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Riverboat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Rowing boat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Sailboat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Scow',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Ship’s boat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Sponge diving boat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Steamboat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Towboat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Traditional boat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Tugboat',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Water taxi',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Windglider‎',
            'vehicle_type_id' => 5
        ]);

        Vehicle::create([
            'vehicle' => 'Yachts',
            'vehicle_type_id' => 5
        ]);
    }
}
