<?php

use Illuminate\Database\Seeder;
use App\DivingSpot;
use App\DivingSpotSubstract;

class DivingSpotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boulder = DivingSpotSubstract::create([
            "name" => "boulders"
        ])["id"];

        $sand = DivingSpotSubstract::create([
            "name" => "sand"
        ])["id"];

        $wall = DivingSpotSubstract::create([
            "name" => "wall"
        ])["id"];

        $cave = DivingSpotSubstract::create([
            "name" => "cave"
        ])["id"];

        $wreck = DivingSpotSubstract::create([
            "name" => "wreck"
        ])["id"];


        $divingSpots = [
            [
                'latitude' => 32.640310,
                'longitude' => -16.834982,
                'range' => '26',
                'name' => 'Arena',
                'description' => '',
                'protection' => 'Marine Protected Area'
            ],
            [
                'latitude' => 32.640536,
                'longitude' => -16.832762,
                'range' => '30',
                'name' => 'Lavafinger',
                'description' => '',
                'protection' => 'Marine Protected Area'
            ],
            [
                'latitude' => 32.640737,
                'longitude' => -16.830952,
                'range' => '16',
                'name' => 'Ponta da Oliveira - Cave',
                'description' => '',
                'protection' => 'None'
            ],
            [
                'latitude' => 32.640240,
                'longitude' => -16.831157,
                'range' => '30',
                'name' => 'Ponta da Oliveira',
                'description' => '',
                'protection' => 'None'
            ],
            [
                'latitude' => 32.642936,
                'longitude' => -16.827994,
                'range' => '23',
                'name' => 'Canyon Reef',
                'description' => '',
                'protection' => 'None'
            ],
            [
                'latitude' => 32.642163,
                'longitude' => -16.828820,
                'range' => '24',
                'name' => 'Easy Track',
                'description' => '',
                'protection' => 'None'
            ],
            [
                'latitude' => 32.641565,
                'longitude' => -16.829237,
                'range' => '25',
                'name' => 'Eastern Rocks',
                'description' => '',
                'protection' => 'None'
            ],
            [
                'latitude' => 32.641222,
                'longitude' => -16.829382,
                'range' => '28',
                'name' => 'Morays Wall',
                'description' => '',
                'protection' => 'None'
            ],
            [
                'latitude' => 32.638972,
                'longitude' => -16.844806,
                'range' => '26',
                'name' => 'Sardine Plateau',
                'description' => '',
                'protection' => 'Marine Protected Area'
            ],
        ];

        $substracts = [
            [$boulder, $sand, $wall, $cave],
            [$boulder, $sand, $wall, $cave],
            [$boulder, $sand, $wall, $cave],
            [$boulder, $sand, $wall],
            [$boulder, $sand, $wall],
            [$boulder, $sand],
            [$boulder, $sand],
            [$boulder, $sand, $wall],
            [$boulder, $sand, $wall],
        ];

        foreach ($divingSpots as $key => $divingSpot) {
            $newDivingSpot = DivingSpot::create($divingSpot);
            $newDivingSpot->substracts()->sync($substracts[$key]);
        }
    }
}
