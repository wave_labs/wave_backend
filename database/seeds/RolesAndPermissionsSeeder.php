<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
  ///WE HAVE THE COMMAND PHP ARTISAN PERMISSIONS:CREATE FOR THIS

  
  /*
  CRUD for dive center tables only


  *scientists* CRUD for all (like admin) - is this ok?


  *tourist* CRUD for their reports only (litter, cetaceans), R all othe
    */

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $allPermissions = [
           'user-list',
           'user-create',
           'user-update',
           'user-delete',
           'sighting-list',
           'sighting-update',
           'sighting-create',
           'sighting-delete',
           'capsule-list',
           'capsule-create',
           'capsule-update',
           'capsule-delete', 
           'creature-list',
           'creature-create',
           'creature-update',
           'creature-delete',
           'diving-spot-list',
           'diving-spot-create',
           'diving-spot-update',
           'diving-spot-delete',
           'dive-list',
           'dive-create',
           'dive-update',
           'dive-delete',
           'litter-list',
           'litter-create',
           'litter-update',
           'litter-delete',
        ];

        $touristPermissions = [
           'sighting-list',
           'capsule-list',
           'creature-list',
           'litter-list',
        ];

        $diveCenterPermissions = [
           'diving-spot-list',
           'diving-spot-create',
           'diving-spot-update',
           'diving-spot-delete',
           'dive-list',
           'dive-create',
           'dive-update',
           'dive-delete',
        ];

        foreach ($allPermissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        $role = Role::create(['name' => 'admin'])->syncPermissions($allPermissions);
        $role = Role::create(['name' => 'scientist'])->syncPermissions($allPermissions);
        $role = Role::create(['name' => 'tourist'])->syncPermissions($touristPermissions);
        $role = Role::create(['name' => 'dive-center'])->syncPermissions($diveCenterPermissions);
    }
}
