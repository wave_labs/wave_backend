<?php

use App\StopwatchCategory;
use App\StopwatchClass;
use App\StopwatchVideo;
use Illuminate\Database\Seeder;

class StopwatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'storage/videos/stopwatch/';

        $videos = [
            [
                'video' => $path . 'bird1.webm',
                'thumbnail' => $path . 'thumbnail/bird1.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'bird2.webm',
                'thumbnail' => $path . 'thumbnail/bird2.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'bird3.webm',
                'thumbnail' => $path . 'thumbnail/bird3.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'mysticeti1.webm',
                'thumbnail' => $path . 'thumbnail/mysticeti1.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'mysticeti2.webm',
                'thumbnail' => $path . 'thumbnail/mysticeti2.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'mysticeti3.webm',
                'thumbnail' => $path . 'thumbnail/mysticeti3.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'pinniped1.webm',
                'thumbnail' => $path . 'thumbnail/pinniped1.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'pinniped2.webm',
                'thumbnail' => $path . 'thumbnail/pinniped2.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'pinniped3.webm',
                'thumbnail' => $path . 'thumbnail/pinniped3.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'turtle1.webm',
                'thumbnail' => $path . 'thumbnail/turtle1.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'turtle2.webm',
                'thumbnail' => $path . 'thumbnail/turtle2.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'turtle3.webm',
                'thumbnail' => $path . 'thumbnail/turtle3.jpg',
                'category_id' => 1,
            ],
            [
                'video' => $path . 'bali.webm',
                'thumbnail' => $path . 'thumbnail/bali.png',
                'category_id' => 2,
            ],
            [
                'video' => $path . 'manythings.webm',
                'thumbnail' => $path . 'thumbnail/manythings.png',
                'category_id' => 2,
            ],
        ];

        $classes = [
            ['name' => 'odontoceti', 'category_id' => 1, 'image' => 'storage/images/stopwatch/odontoceti.png'],
            ['name' => 'mysticeti', 'category_id' => 1, 'image' => 'storage/images/stopwatch/mysticeti.png'],
            ['name' => 'pinniped', 'category_id' => 1, 'image' => 'storage/images/stopwatch/pinniped.png'],
            ['name' => 'bird', 'category_id' => 1, 'image' => 'storage/images/stopwatch/bird.png'],
            ['name' => 'turtle', 'category_id' => 1, 'image' => 'storage/images/stopwatch/turtle.png'],
            ['name' => 'plastic', 'category_id' => 2, 'image' => 'storage/images/stopwatch/plastic.png'],
            ['name' => 'rubber', 'category_id' => 2, 'image' => 'storage/images/stopwatch/rubber.png'],
            ['name' => 'metal', 'category_id' => 2, 'image' => 'storage/images/stopwatch/metal.png'],
            ['name' => 'cloth/textile', 'category_id' => 2, 'image' => 'storage/images/stopwatch/cloth.png'],
        ];

        $categories = [
            ['name' => 'biodiversity'],
            ['name' => 'litter'],
        ];

        foreach ($categories as $category) {
            StopwatchCategory::create($category);
        }

        foreach ($videos as $video) {
            StopwatchVideo::create($video);
        }

        foreach ($classes as $class) {
            StopwatchClass::create($class);
        }
    }
}
