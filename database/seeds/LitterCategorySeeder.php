<?php

use App\LitterCategory;
use Illuminate\Database\Seeder;

class LitterCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Artificial polymer materials'],
            ['name' => 'Metal'],
            ['name' => 'Rubber'],
            ['name' => 'Cloth/textile'],
            ['name' => 'Paper/Cardboard'],
            ['name' => 'Processed/worked wood'],
            ['name' => 'Glass/ceramics'],
            ['name' => 'Chemicals'],
            ['name' => 'Food waste'],
            ['name' => 'Other'],
            ['name' => 'Fishing gear']

        ];

        $PTitems = [
            ['name' => 'Plásticos (polímeros artificiais)'],
            ['name' => 'Metal'],
            ['name' => 'Borracha'],
            ['name' => 'Tecidos/têxteis indiferenciados'],
            ['name' => 'Papel/cartão'],
            ['name' => 'Madeira processada'],
            ['name' => 'Vidro/ceramica'],
            ['name' => 'Químicos'],
            ['name' => 'Desperdicios de comida'],
            ['name' => 'Outros'],
            ['name' => 'Material de pesca']
        ];

        foreach ($items as $key => $item) {
            $newItem = new LitterCategory();
            $newItem
                ->setTranslation('name', 'en', $item['name'])
                ->setTranslation('name', 'pt', $PTitems[$key]['name'])
                ->save();
        }
    }
}
