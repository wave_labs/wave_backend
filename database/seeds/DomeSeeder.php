<?php

use App\Dome;
use App\DomeHasInteraction;
use App\DomeInteraction;
use App\DomeItem;
use App\DomePosition;
use Illuminate\Database\Seeder;

class DomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domes =  [
            [
                'name' => 'whale',
                'active' => true,
                'image' => "/images/domes/whale.png",
                'model' => "https://domes.wave-labs.org/html/whale.html",
                'description' => "Designed for the public beach and night projections, audience interacts with it using body gestures and integrated sonar modules. Interaction is based on the proximity of the person to the dome, where the more people are closer to the projection, the more carbon emission are being emitted. This interaction in further triggers the gathering of the greater whales, who are recently reported that can reduce the worldwide carbon emission by 5%"
            ],
            [
                'name' => 'dolphin',
                'active' => true,
                'image' => "/images/domes/dolphin.png",
                'model' => "https://domes.wave-labs.org/html/dolphin.html",
                'description' => "Dome floats at the sea surface, and is moored to the public beach with ropes and moored to the existing seabed connections. Is built on an existing water platform, allowing children, parents and visitor swimmers to explore it. At the each piece of frame, anadditional engraving tells that 5 tons of CO2 are emitted daily on Madeira, solely coming from the whale watching sea-vessels"
            ],
            [
                'name' => 'seal',
                'active' => true,
                'image' => "/images/domes/seal.png",
                'model' => "https://domes.wave-labs.org/html/seal.html",
                'description' => "Since certified Advanced OpenWater divers are usually in search for exploration of underwater shipwrecks, proposed installation resembles one of the endangered seals on Madeira archipelago, called Lobo Marinho. While their population is estimated to be solely appx. 20 species, such seals have been recently spotted by the divers to have in-water short naps, and is therefore the rationale for being anchored to the seabed using concrete weights"
            ],
            [
                'name' => 'turtle',
                'active' => true,
                'image' => "/images/domes/turtle.png",
                'model' => "https://domes.wave-labs.org/html/turtle.html",
                'description' => "Deployed at the protected turtle nesting beaches which are open to the visitors, the users scan the dome with their own mobile phone’s camera. The dome acts as a 2D marker to trigger a Cross Reality (XR) inside of the mobile application, depicting a game of 3D animated loggerhead turtle, combating marine litter"
            ],
            [
                'name' => 'seabird',
                'active' => true,
                'image' => "/images/domes/bird.png",
                'model' => "https://domes.wave-labs.org/html/bird.html",
                'description' => "Installed at the sea surface and acting as a lighthouse close to the harbor, it is deployed on root trajectories of whale-watching sea-vessels. Such vessels pass by the geodesic dome to observe the gathering of sea-birds, who are found rest at the surface. Tourists use their mobile phones to take the photos of gathered species and join the local solar powered router and WLAN to obtain the information about the latest ocean sensory input"
            ],
        ];

        $positions = [
            ['latitude' => 32.65945380034387,  'longitude' => -16.92537569208287, 'from' => '01-01-2021'],
            ['latitude' => 32.65941512922686, 'longitude' => -16.92531437665311, 'from' => '01-01-2021'],
            ['latitude' =>  32.65937208262384, 'longitude' => -16.92523978295275, 'from' => '01-01-2021'],
            ['latitude' => 32.65933587499486, 'longitude' => -16.9251694075496, 'from' => '01-01-2021'],
            ['latitude' =>  32.659282801596156, 'longitude' => -16.925085679660384, 'from' => '01-01-2021'],
        ];

        foreach ($domes as $key => $dome) {
            $newDome = Dome::create($dome);
            $positions[$key]['dome_id'] =  $newDome->id;
            DomePosition::create($positions[$key]);
        }

        // -------------------------------------------------------------------

        $interactions =  [
            ['name' => 'AR'],
            ['name' => 'VR'],
        ];

        foreach ($interactions as $interaction) {
            DomeInteraction::create($interaction);
        }

        // -------------------------------------------------------------------

        $domes = Dome::all();
        $interactions = DomeInteraction::all();

        foreach ($domes as $dome) {
            foreach ($interactions as $interaction) {
                $dome_interaction = DomeHasInteraction::create([
                    'dome_id' => $dome->id,
                    'dome_interaction_id' => $interaction->id
                ]);

                for ($i = 1; $i <= 10; $i++) {
                    $item = [
                        'dome_interaction_id' => $dome_interaction->id,
                        'item' => $i,
                        'x' => $i / 10,
                        'y' => $i / 10,
                        'z' => $i / 10
                    ];
                    DomeItem::create($item);
                }
            }
        }
    }
}
