<?php

use App\InsectReferenceKeyword;
use Illuminate\Database\Seeder;

class InsectReferenceKeywordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keywords = [
            [
                'keyword' => 'Source unprocessed biology',
            ],
            [
                'keyword' => 'Source being processed biology',
            ],
            [
                'keyword' => 'Source processed biology',
            ],
            [
                'keyword' => 'Source unprocessed distribution',
            ],
            [
                'keyword' => 'Source being processed distribution',
            ],
            [
                'keyword' => 'Source processed distribution',
            ],
            [
                'keyword' => 'Source unprocessed nomenclature',
            ],
            [
                'keyword' => 'Source being processed nomenclature',
            ],
            [
                'keyword' => 'Source processed nomenclature',
            ],
            [
                'keyword' => 'Source unprocessed food plants',
            ],
            [
                'keyword' => 'Source being processed food plants',
            ],
            [
                'keyword' => 'Source processed food plants',
            ],
            [
                'keyword' => 'Source unprocessed habitat',
            ],
            [
                'keyword' => 'Source being processed habitat',
            ],
            [
                'keyword' => 'Source processed habitat',
            ],
            [
                'keyword' => 'Source unprocessed description',
            ],
            [
                'keyword' => 'Source being processed description',
            ],
            [
                'keyword' => 'Source processed description',
            ],
            [
                'keyword' => 'Pages needed',
            ],
            [
                'keyword' => 'Request data from authors',
            ],
            [
                'keyword' => 'Search suplementary material',
            ]
        ];

        foreach ($keywords as $key => $keyword) {
            InsectReferenceKeyword::create($keyword);
        }
    }
}
