<?php

use Illuminate\Database\Seeder;
use App\LitterCategory;
use App\LitterSubCategory;

class LitterSubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = [];
        $litterCategories = LitterCategory::all();

        foreach ($litterCategories as $category) {
            $categories[$category->name] = $category->id;
        }

        /*$out = new \Symfony\Component\Console\Output\ConsoleOutput();
        $out->writeln($categories['Other']);*/

        $items = [
            [
                'name' => '4/6-pack yokes, six-pack rings',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '1',
                'UNEP_code' => 'PL05',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Bags',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'UNEP_code' => 'PL07',
                'Core' => true,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Shopping Bags incl. pieces',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '2',
                'UNEP_code' => 'PL07',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Small plastic bags, e.g. freezer bags incl. pieces',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '3',
                'UNEP_code' => 'PL07',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic bag collective role; what remains from rip-off plastic bags',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '112',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Bottles',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '4',
                'UNEP_code' => 'PL02',
                'Core' => true,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Bottles <= 0.5l',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '4',
                'UNEP_code' => 'PL02',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Bottles >0.5l',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '4',
                'UNEP_code' => 'PL02',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cleaner bottles & containers',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '5',
                'UNEP_code' => 'PL02',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Food containers incl. fast food containers',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '6',
                'UNEP_code' => 'PL06',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Beach use related cosmetic bottles and containers, e.g. Sunblocks',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '7',
                'UNEP_code' => 'PL02',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other cosmetics bottles & containers',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '7',
                'UNEP_code' => 'PL02',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other bottles & containers (drums)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '12',
                'UNEP_code' => 'PL02',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Engine oil bottles & containers <50 cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '8',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Engine oil bottles & containers >50 cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '9',
                'UNEP_code' => 'PL03',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Jerry cans (square plastic containers with handle)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '10',
                'UNEP_code' => 'PL03',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Injection gun containers',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '11',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Crates and containers / baskets',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '13',
                'UNEP_code' => 'PL13',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Car parts',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '14',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic caps and lids',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'UNEP_code' => 'PL01',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic caps/lids drinks',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '15',
                'UNEP_code' => 'PL01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic caps/lids chemicals, detergents (non-food)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '15',
                'UNEP_code' => 'PL01',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic caps/lids unidentified',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '15',
                'UNEP_code' => 'PL01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic rings from bottle caps/lids',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '15',
                'UNEP_code' => 'PL01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Tobacco pouches / plastic cigarette box packaging',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cigarette lighters',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '16',
                'UNEP_code' => 'PL10',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cigarette butts and filters',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '64',
                'UNEP_code' => 'PL11',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Pens and pen lids',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '17',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Combs/hair brushes/sunglasses',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '18',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Crisps packets/sweets wrappers',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '19',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Lolly sticks',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '19',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Toys and party poppers',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '20',
                'UNEP_code' => 'PL08',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cups and cup lids',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '21',
                'UNEP_code' => 'PL06',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cutlery and trays',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '22',
                'UNEP_code' => 'PL04',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Straws and stirrers',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '22',
                'UNEP_code' => 'PL04',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fertiliser/animal feed bags',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '23',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Mesh vegetable bags',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '24',
                'UNEP_code' => 'PL15',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cover / packaging',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],


            [
                'name' => 'Gloves',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'UNEP_code' => 'PL09',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Gloves (washing up)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '25',
                'UNEP_code' => 'PL09',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Gloves (industrial/professional rubber gloves)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '113',
                'UNEP_code' => 'RB03',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Crab/lobster pots and tops',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '26',
                'UNEP_code' => 'PL17',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Tags (fishing and industry)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '114',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Octopus pots',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '27',
                'UNEP_code' => 'PL17',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Mussels nets, Oyster nets',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '28',
                'UNEP_code' => 'PL15',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Oyster trays (round from oyster cultures)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '29',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic sheeting from mussel culture (Tahitians)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '30',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Synthetic rope',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Rope (diameter more than 1cm)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '31',
                'UNEP_code' => 'PL19',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'String and cord (diameter less than 1cm)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '32',
                'UNEP_code' => 'PL19',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fishing net',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'UNEP_code' => 'PL20',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Nets and pieces of net',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'UNEP_code' => 'PL20',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Nets and pieces of net < 50 cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '115',
                'UNEP_code' => 'PL20',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Nets and pieces of net > 50 cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '116',
                'UNEP_code' => 'PL20',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fishing line (entangled)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'UNEP_code' => 'PL18',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Tangled nets/cord',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '33',
                'UNEP_code' => 'PL20',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fish boxes - plastic',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '34',
                'UNEP_code' => 'PL17',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fish boxes - expanded polystyrene',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '34',
                'UNEP_code' => 'PL17',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fishing line/monofilament (angling)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '35',
                'UNEP_code' => 'PL18',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Light sticks (tubes with fluid) incl. packaging',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '36',
                'UNEP_code' => 'PL17',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other fishing related',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Floats for fishing nets',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '37',
                'UNEP_code' => 'PL14',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Buoys',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '37',
                'UNEP_code' => 'PL14',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fenders',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Buckets',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '38',
                'UNEP_code' => 'PL03',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Strapping bands',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '39',
                'UNEP_code' => 'PL21',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Sheets, industrial packaging, plastic sheeting',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '40',
                'UNEP_code' => 'PL16',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fibre glass/fragments',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '41',
                'UNEP_code' => 'PL22',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Hard hats/Helmets',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '42',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Shotgun cartridges',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '43',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Shoes/sandals',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '44',
                'UNEP_code' => 'CL01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Traffic cones',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Foam sponge',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '45',
                'UNEP_code' => 'FP01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Foam packaging/insulation/polyurethane',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic/polystyrene pieces 0 - 2.5 cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '117',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic/polystyrene pieces 2.5 cm > < 50cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '46',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic/polystyrene pieces > 50cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '47',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic pieces 0 - 2.5 cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic pieces 2.5 cm > < 50cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic pieces > 50 cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Polystyrene pieces 0 - 2.5 cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Polystyrene pieces 2.5 cm > < 50cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Polystyrene pieces > 50 cm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'CD, CD-box',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Salt packaging',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fin trees (from fins for scuba diving)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Masking tape',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Telephone (incl. parts)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic construction waste',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic flower pots',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Biomass holder from sewage treatment plants',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Bait containers/packaging',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cable ties',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Table cloth',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cotton bud sticks',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '98',
                'UNEP_code' => 'OT02',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Sanitary towels/panty liners/backing strips',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '99',
                'UNEP_code' => 'OT02',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Toilet fresheners',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '101',
                'UNEP_code' => 'OT02',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Diapers/nappies',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'UNEP_code' => 'OT02',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Syringes/needles',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '104',
                'UNEP_code' => 'PL12',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Medical/Pharmaceuticals containers/tubes',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '103',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Dog faeces bag',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '121',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Flip-flops',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'UNEP_code' => 'RB02',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Plastic fragments rounded <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'Plastic fragments subrounded <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'Plastic fragments subangular <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'Plastic fragments angular <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'cylindrical pellets <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'disks pellets <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'flat pellets <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'ovoid pellets <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'spheruloids pellets <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'Industrial pellets',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'UNEP_code' => 'PL23',
                'Core' => true,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Filament <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'Films <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'Foamed plastic <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'Granules <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'Styrofoam <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => true
            ],

            [
                'name' => 'Small industrial spheres (<5mm)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Sheet like user plastic (>1mm)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Threadlike user plastic (>1mm)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Foamed user plastic (>1mm)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Plastic fragments (>1mm)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Polyurethane granules <5mm',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other plastic/polystyrene items (identifiable)',
                'litter_category_id' => $categories['Artificial polymer materials'],
                'OSPAR_code' => '48',
                'UNEP_code' => 'PL24',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Balloons and balloon sticks',
                'litter_category_id' => $categories['Rubber'],
                'OSPAR_code' => '49',
                'UNEP_code' => 'RB01',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Balls',
                'litter_category_id' => $categories['Rubber'],
                'UNEP_code' => 'RB01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Rubber boots',
                'litter_category_id' => $categories['Rubber'],
                'OSPAR_code' => '50',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Tyres and belts',
                'litter_category_id' => $categories['Rubber'],
                'OSPAR_code' => '52',
                'UNEP_code' => 'RB04',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Inner-tubes and rubber sheet',
                'litter_category_id' => $categories['Rubber'],
                'UNEP_code' => 'RB05',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Wheels',
                'litter_category_id' => $categories['Rubber'],
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Rubber bands (small, for kitchen/household/post use)',
                'litter_category_id' => $categories['Rubber'],
                'UNEP_code' => 'RB06',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Bobbins (fishing)',
                'litter_category_id' => $categories['Rubber'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Condoms (incl. packaging)',
                'litter_category_id' => $categories['Rubber'],
                'OSPAR_code' => '97',
                'UNEP_code' => 'RB07',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other rubber pieces',
                'litter_category_id' => $categories['Rubber'],
                'OSPAR_code' => '53',
                'UNEP_code' => 'RB08',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Clothing (clothes, shoes)',
                'litter_category_id' => $categories['Cloth/textile'],
                'UNEP_code' => 'CL01',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Shoes',
                'litter_category_id' => $categories['Cloth/textile'],
                'UNEP_code' => 'CL01',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Clothing / rags (clothing, hats, towels)',
                'litter_category_id' => $categories['Cloth/textile'],
                'OSPAR_code' => '54',
                'UNEP_code' => 'CL01',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Shoes and sandals (e.g. Leather, cloth)',
                'litter_category_id' => $categories['Cloth/textile'],
                'OSPAR_code' => '57',
                'UNEP_code' => 'CL01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Backpacks & bags',
                'litter_category_id' => $categories['Cloth/textile'],
                'UNEP_code' => 'CL02',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Sacking (hessian)',
                'litter_category_id' => $categories['Cloth/textile'],
                'OSPAR_code' => '56',
                'UNEP_code' => 'CL03',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Carpet & Furnishing',
                'litter_category_id' => $categories['Cloth/textile'],
                'OSPAR_code' => '55',
                'UNEP_code' => 'CL05',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Rope, string and nets',
                'litter_category_id' => $categories['Cloth/textile'],
                'UNEP_code' => 'CL04',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Sails, canvas',
                'litter_category_id' => $categories['Cloth/textile'],
                'UNEP_code' => 'CL03',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Tampons and tampon applicators',
                'litter_category_id' => $categories['Cloth/textile'],
                'OSPAR_code' => '100',
                'UNEP_code' => 'OT02',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other textiles (incl. rags)',
                'litter_category_id' => $categories['Cloth/textile'],
                'OSPAR_code' => '59',
                'UNEP_code' => 'CL06',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Paper/Cardboard',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Paper bags',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'OSPAR_code' => '60',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cardboard (boxes & fragments)',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'OSPAR_code' => '61',
                'UNEP_code' => 'PC02',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Paper packaging',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'UNEP_code' => 'PC03',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cartons/Tetrapack Milk',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'OSPAR_code' => '118',
                'UNEP_code' => 'PC03',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cartons/Tetrapack (others)',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'OSPAR_code' => '62',
                'UNEP_code' => 'PC03',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cigarette packets',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'OSPAR_code' => '63',
                'UNEP_code' => 'PC03',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cups, food trays, food wrappers, drink containers',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'OSPAR_code' => '65',
                'UNEP_code' => 'PC03',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Newspapers & magazines',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'OSPAR_code' => '66',
                'UNEP_code' => 'PC01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Tubes for fireworks',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'UNEP_code' => 'PC04',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Paper fragments',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Paper',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Other paper items',
                'litter_category_id' => $categories['Paper/Cardboard'],
                'OSPAR_code' => '67',
                'UNEP_code' => 'PC05',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Corks',
                'litter_category_id' => $categories['Processed/worked wood'],
                'OSPAR_code' => '68',
                'UNEP_code' => 'WD01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Pallets',
                'litter_category_id' => $categories['Processed/worked wood'],
                'OSPAR_code' => '69',
                'UNEP_code' => 'WD04',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],


            [
                'name' => 'Processed timber',
                'litter_category_id' => $categories['Processed/worked wood'],
                'OSPAR_code' => '69',
                'UNEP_code' => 'WD04',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Crates',
                'litter_category_id' => $categories['Processed/worked wood'],
                'OSPAR_code' => '70',
                'UNEP_code' => 'WD04',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Crab/lobster pots',
                'litter_category_id' => $categories['Processed/worked wood'],
                'OSPAR_code' => '71',
                'UNEP_code' => 'WD02',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fish boxes',
                'litter_category_id' => $categories['Processed/worked wood'],
                'OSPAR_code' => '119',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Ice-cream sticks, chip forks, chopsticks,
        toothpicks',
                'litter_category_id' => $categories['Processed/worked wood'],
                'OSPAR_code' => '72',
                'UNEP_code' => 'WD03',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Paint brushes',
                'litter_category_id' => $categories['Processed/worked wood'],
                'OSPAR_code' => '89',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Matches & fireworks',
                'litter_category_id' => $categories['Processed/worked wood'],
                'UNEP_code' => 'WD06',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Wood boards',
                'litter_category_id' => $categories['Processed/worked wood'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Beams / Dunnage',
                'litter_category_id' => $categories['Processed/worked wood'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Wood (processed)',
                'litter_category_id' => $categories['Processed/worked wood'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other wood < 50 cm',
                'litter_category_id' => $categories['Processed/worked wood'],
                'OSPAR_code' => '74',
                'UNEP_code' => 'WD06',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other wood > 50 cm',
                'litter_category_id' => $categories['Processed/worked wood'],
                'OSPAR_code' => '75',
                'UNEP_code' => 'WD06',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other (specify)',
                'litter_category_id' => $categories['Processed/worked wood'],
                'UNEP_code' => 'WD06',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            // GLASS / CERAMICS

            [
                'name' => 'Aerosol/Spray cans industry',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '76',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cans (beverage)',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '78',
                'UNEP_code' => 'ME03',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cans (food)',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '82',
                'UNEP_code' => 'ME04',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Foil wrappers, aluminium foil',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '81',
                'UNEP_code' => 'ME06',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Bottle caps, lids & pull tabs',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '77',
                'UNEP_code' => 'ME02',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Disposable BBQs',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '120',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Appliances (refrigerators, washers, etc.)',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '79',
                'UNEP_code' => 'ME10',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Tableware (plates, cups & cutlery)',
                'litter_category_id' => $categories['Metal'],
                'UNEP_code' => 'ME01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fishing related (weights, sinkers, lures,
        hooks)',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '80',
                'UNEP_code' => 'ME07',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fish hook remains',
                'litter_category_id' => $categories['Metal'],
                'UNEP_code' => 'ME07',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Lobster/crab pots',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '87',
                'UNEP_code' => 'ME07',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Middle size containers',
                'litter_category_id' => $categories['Metal'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Industrial scrap',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '83',
                'UNEP_code' => 'ME10',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Drums, e.g. oil',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '84',
                'UNEP_code' => 'ME05',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other cans (< 4 L)',
                'litter_category_id' => $categories['Metal'],
                'UNEP_code' => 'ME04',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Gas bottles, drums & buckets ( > 4 L)',
                'litter_category_id' => $categories['Metal'],
                'UNEP_code' => 'ME05',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Paint tins',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '86',
                'UNEP_code' => 'ME05',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Wire, wire mesh, barbed wire',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '88',
                'UNEP_code' => 'ME09',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Barrels',
                'litter_category_id' => $categories['Metal'],
                'UNEP_code' => 'ME05',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Car parts / batteries',
                'litter_category_id' => $categories['Metal'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Cables',
                'litter_category_id' => $categories['Metal'],
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Household Batteries',
                'litter_category_id' => $categories['Metal'],
                'UNEP_code' => 'OT04',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Large metallic objects',
                'litter_category_id' => $categories['Metal'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other (metal)',
                'litter_category_id' => $categories['Metal'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => true,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other metal pieces < 50 cm',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '89',
                'UNEP_code' => 'ME10',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other metal pieces > 50 cm',
                'litter_category_id' => $categories['Metal'],
                'OSPAR_code' => '90',
                'UNEP_code' => 'ME10',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            // GLASS / CERAMICS

            [
                'name' => 'Bottles incl. pieces',
                'litter_category_id' => $categories['Glass/ceramics'],
                'OSPAR_code' => '91',
                'UNEP_code' => 'GC02',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Jars incl. pieces',
                'litter_category_id' => $categories['Glass/ceramics'],
                'UNEP_code' => 'GC02',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Light bulbs',
                'litter_category_id' => $categories['Glass/ceramics'],
                'OSPAR_code' => '92',
                'UNEP_code' => 'GC04',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Tableware (plates & cups)',
                'litter_category_id' => $categories['Glass/ceramics'],
                'UNEP_code' => 'GC03',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Construction material (brick, cement,
        pipes)',
                'litter_category_id' => $categories['Glass/ceramics'],
                'OSPAR_code' => '94',
                'UNEP_code' => 'GC01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Fluorescent light tubes',
                'litter_category_id' => $categories['Glass/ceramics'],
                'OSPAR_code' => '92',
                'UNEP_code' => 'GC05',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Glass buoys',
                'litter_category_id' => $categories['Glass/ceramics'],
                'UNEP_code' => 'GC06',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Octopus pots',
                'litter_category_id' => $categories['Glass/ceramics'],
                'OSPAR_code' => '95',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Glass or ceramic fragments >2.5cm',
                'litter_category_id' => $categories['Glass/ceramics'],
                'UNEP_code' => 'GC07',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Large glass objects (specify)',
                'litter_category_id' => $categories['Glass/ceramics'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Other glass items',
                'litter_category_id' => $categories['Glass/ceramics'],
                'OSPAR_code' => '96',
                'UNEP_code' => 'GC08',
                'Core' => true,
                'Beach' => true,
                'Seafloor' => true,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            // OTHER

            [
                'name' => 'Medical items',
                'litter_category_id' => $categories['Other'],
                'OSPAR_code' => '105',
                'UNEP_code' => 'OT05',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => false,
                'Micro' => false
            ],

            [
                'name' => 'Slack / Coal',
                'litter_category_id' => $categories['Other'],
                'OSPAR_code' => '181,109,110',
                'UNEP_code' => 'OT01',
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Paraffin/Wax',
                'litter_category_id' => $categories['Chemicals'],
                'OSPAR_code' => '181,109,110',
                'UNEP_code' => 'OT01',
                'Core' => false,
                'Beach' => true,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Oil/Tar',
                'litter_category_id' => $categories['Chemicals'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Food waste (galley waste)',
                'litter_category_id' => $categories['Food waste'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],

            [
                'name' => 'Various rubbish',
                'litter_category_id' => $categories['Other'],
                'Core' => false,
                'Beach' => false,
                'Seafloor' => false,
                'Floating' => false,
                'Biota' => true,
                'Micro' => false
            ],
        ];

        $PTitems = [
            [
                'name' => 'Embalanges múltlipas - 4/6, anéis de seis embalagens',
            ],

            [
                'name' => 'Sacos',
            ],

            [
                'name' => 'Sacos de compras',
            ],

            [
                'name' => 'Sacos de plástico pequenos, p.ex.: sacos para congelados',
            ],

            [
                'name' => 'Aglomerados de sacos de plástico e o que resta dos sacos plásticos rasgáveis',
            ],

            [
                'name' => 'Garrafas',
            ],

            [
                'name' => 'Garrafas <= 0.5L',
            ],

            [
                'name' => 'Garrafas > 0.5L',
            ],

            [
                'name' => 'Garrafas e contentores de produtos de limpeza',
            ],

            [
                'name' => 'Garrafas e contentores de alimentos incl. recipientes de fast food',
            ],

            [
                'name' => 'Garrafas e contentores de cosmeticos, p.ex.: loções solares, champô, gel de banho, desodorisante',
            ],

            [
                'name' => 'Outros frascos e recipientes de cosmética',
            ],

            [
                'name' => 'Outras garrafas e recipientes',
            ],

            [
                'name' => 'Garrafas, contentores, bidões de óleo de motores (<50cm)',
            ],

            [
                'name' => 'Bidões de óleo de motores (>50cm)',
            ],

            [
                'name' => 'Jerry cans (recipientes de plástico com pega)',
            ],

            [
                'name' => 'Cartuchos de Silicone',
            ],

            [
                'name' => 'Grades, contentores, caixotes',
            ],

            [
                'name' => 'Peças de carros',
            ],

            [
                'name' => 'Tampas, cápsulas de plástico',
            ],

            [
                'name' => 'Tampas de plástico de bebidas',
            ],

            [
                'name' => 'Tampas plásticas de químicos, detergentes',
            ],

            [
                'name' => 'Tampas de plásticas não identificadas',
            ],

            [
                'name' => 'Anéis de plástico de tampas de garrafas',
            ],

            [
                'name' => 'Bolsas de tabaco, embalagens de plástico para cigarros',
            ],

            [
                'name' => 'Isqueiros',
            ],

            [
                'name' => 'Beatas e filtros de cigarros',
            ],

            [
                'name' => 'Canetas, tampas de canetas',
            ],

            [
                'name' => 'Pentes, escovas de cabelo, óculos de sol',
            ],

            [
                'name' => 'Sacos de batatas fritas ou guloseimas',
            ],

            [
                'name' => 'Paus de chupa-chupas',
            ],

            [
                'name' => 'Brinquedos, artigos de festas',
            ],

            [
                'name' => 'Copos, tampas de copos',
            ],

            [
                'name' => 'Talheres, tabuleiros, copos',
            ],

            [
                'name' => 'Palhetas, agitadores',
            ],

            [
                'name' => 'Sacos de fertilizantes, sacos de comida para animais',
            ],

            [
                'name' => 'Sacos de rede para vegetais',
            ],

            [
                'name' => 'Cobertura de embalagem',
            ],


            [
                'name' => 'Luvas',
            ],

            [
                'name' => 'Luvas (uso doméstico)',
            ],

            [
                'name' => 'Luvas (de uso industrial ou profissional)',
            ],

            [
                'name' => 'Armadilhas para caranguejos ou lagostas',
            ],

            [
                'name' => 'Etiquetas de pesca ou indústria',
            ],

            [
                'name' => 'Armadilhas para polvos ou alcatruzes ',
            ],

            [
                'name' => 'Redes de mexilhões, redes para ostras',
            ],

            [
                'name' => 'Tabuleiros redondos para cultura de ostras',
            ],

            [
                'name' => 'Bandas de plástico para cultura de mexilhão (Tahitianas)',
            ],

            [
                'name' => 'Corda sintética',
            ],

            [
                'name' => 'Cordas (diâmetro >1cm)',
            ],

            [
                'name' => 'Corda ou cordel (diâmetro <1cm)',
            ],

            [
                'name' => 'Redes de pesca',
            ],

            [
                'name' => 'Redes e peças de redes de pesca',
            ],

            [
                'name' => 'Redes e pedaços de rede (<50cm)',
            ],

            [
                'name' => 'Redes e peças de redes (>50cm)',
            ],

            [
                'name' => 'Linha de pesca',
            ],

            [
                'name' => 'Emaranhado de redes e cordéis ',
            ],

            [
                'name' => 'Caixas de pesca de plástico',
            ],

            [
                'name' => 'Caixas de pesca de poliestireno expandido',
            ],

            [
                'name' => 'Linha de pesca (pesca com anzol)',
            ],

            [
                'name' => 'Tubos luminosos (tubos com líquido)',
            ],

            [
                'name' => 'Outros artigos de pesca',
            ],

            [
                'name' => 'Flutuadores para redes de pesca',
            ],

            [
                'name' => 'Bóias',
            ],

            [
                'name' => 'Pára-lamas',
            ],

            [
                'name' => 'Baldes',
            ],

            [
                'name' => 'Tiras ou bandas para empacotamento',
            ],

            [
                'name' => 'Embalagens industriais, tiras de plástico',
            ],

            [
                'name' => 'Fragmentos de fibra de vidro',
            ],

            [
                'name' => 'Capacetes de protecção',
            ],

            [
                'name' => 'Cartuchos de munições',
            ],

            [
                'name' => 'Sapatos, sandálias ',
            ],

            [
                'name' => 'Cones de trânsito',
            ],

            [
                'name' => 'Esponja de espuma',
            ],

            [
                'name' => 'Embalagem de espuma ou isolante em poliuretano',
            ],

            [
                'name' => 'Peças de plástico ou poliestireno de 0 a 2,5cm',
            ],

            [
                'name' => 'Peças de plástico ou poliestireno entre 2,5 e 50cm',
            ],

            [
                'name' => 'Peças de plástico ou poliestireno (>50cm)',
            ],

            [
                'name' => 'Peças de plástico de 0 a 2.5cm',
            ],

            [
                'name' => 'Peças de plástico entre 2.5 a 50cm',
            ],

            [
                'name' => 'Peças de plástico (>50cm)',
            ],

            [
                'name' => 'Peças de poliestireno entre 0 e 2.5 cm',
            ],

            [
                'name' => 'Peças de poliestireno entre 2.5 e 50cm',
            ],

            [
                'name' => 'Peças de poliestireno (>50cm)',
            ],

            [
                'name' => 'CD, CD-box',
            ],

            [
                'name' => 'Embalagem de sal',
            ],

            [
                'name' => 'Barbatanas de mergulho',
            ],

            [
                'name' => 'Fita adesiva',
            ],

            [
                'name' => 'Telefone (incl. peças)',
            ],

            [
                'name' => 'Resíduos de plástico da construção',
            ],

            [
                'name' => 'Vasos de flores de plástico',
            ],

            [
                'name' => 'Suporte de biomassa a partir de estações de tratamento de esgoto',
            ],

            [
                'name' => 'Recipientes, embalagens de iscos',
            ],

            [
                'name' => 'Abraçadeiras',
            ],

            [
                'name' => 'Toalha de mesa',
            ],

            [
                'name' => 'Cotonetes',
            ],

            [
                'name' => 'Toalhetes de limpeza, fraldas, pensos',
            ],

            [
                'name' => 'Ambientadores de sanitários',
            ],

            [
                'name' => 'Fraldas',
            ],

            [
                'name' => 'Seringas, agulhas',
            ],

            [
                'name' => 'Recipientes, tubos médico-farmacêuticos',
            ],

            [
                'name' => 'Saco de fezes de animais',
            ],

            [
                'name' => 'Sandálias de dedo',
            ],

            [
                'name' => 'Fragmentos de plástico arredondados (<5mm)',
            ],

            [
                'name' => 'Fragmentos de plástico sub-arredondados (<5mm)',
            ],

            [
                'name' => 'Fragmentos de plástico subangulares (<5mm)',
            ],

            [
                'name' => 'Fragmentos de plástico angulares (<5mm)',
            ],

            [
                'name' => 'Pellets cilindricas (<5mm)',
            ],

            [
                'name' => 'Pellets em forma de disco (<5mm)',
            ],

            [
                'name' => 'Pellets planas (<5mm)',
            ],

            [
                'name' => 'Pellets ovaloides (<5mm)',
            ],

            [
                'name' => 'Pellets esféricas (<5mm)',
            ],

            [
                'name' => 'Pellets industriais',
            ],

            [
                'name' => 'Filamentos (<5mm)',
            ],

            [
                'name' => 'Filme de parafina plástica (<5mm)',
            ],

            [
                'name' => 'Esferovite (<5mm)',
            ],

            [
                'name' => 'Grânulos (<5mm)',
            ],

            [
                'name' => 'Isopor - espuma de poliestireno (<5mm)',
            ],

            [
                'name' => 'Pequenas esferas industriais (<5mm)',
            ],

            [
                'name' => 'Folhas de plástico (>1mm)',
            ],

            [
                'name' => 'Fio de plástico (>1mm)',
            ],

            [
                'name' => 'Espuma de plástico (>1mm)',
            ],

            [
                'name' => 'Fragmentos de plástico (>1mm)',
            ],

            [
                'name' => 'Grânulos de poliuretano (<5mm)',
            ],

            [
                'name' => 'Outro plástico, itens de poliestireno (identificáveis)',
            ],

            [
                'name' => 'Balões e varas suporte dos balões',
            ],

            [
                'name' => 'Bolas',
            ],

            [
                'name' => 'Botas',
            ],

            [
                'name' => 'Pneus',
            ],

            [
                'name' => 'Câmaras de ar e folha de borracha',
            ],

            [
                'name' => 'Rodas',
            ],

            [
                'name' => 'Elásticos (uso doméstico)',
            ],

            [
                'name' => 'Bobinas (pesca)',
            ],

            [
                'name' => 'Preservativos (incl. embalagem)',
            ],

            [
                'name' => 'Outras peças de borracha',
            ],

            [
                'name' => 'Roupas, calçado',
            ],

            [
                'name' => 'Calçado',
            ],

            [
                'name' => 'Roupas ou trapos (roupas, chapéus, toalhas)',
            ],

            [
                'name' => 'Sapatos, sandálias (p.ex.: couro, tecido)',
            ],

            [
                'name' => 'Mochilas, bolsas',
            ],

            [
                'name' => 'Serapilheira',
            ],

            [
                'name' => 'Tapetes, mobílias',
            ],

            [
                'name' => 'Corda, fio e redes',
            ],

            [
                'name' => 'Velas, telas',
            ],

            [
                'name' => 'Tampões e aplicadores de tampões',
            ],

            [
                'name' => 'Outros têxteis (incl. trapos)',
            ],

            [
                'name' => 'Papel ou Cartão',
            ],

            [
                'name' => 'Sacos de papel',
            ],

            [
                'name' => 'Cartão (caixas ou fragmentos)',
            ],

            [
                'name' => 'Embalagem de papel',
            ],

            [
                'name' => 'Caixa de papelão ou Tetrapaks para leite',
            ],

            [
                'name' => 'Caixa de papelão ou Tetrapaks (outros)',
            ],

            [
                'name' => 'Pacotes de cigarros',
            ],

            [
                'name' => 'Copos, bandejas de comida, embalagens de comida, recipientes de bebida',
            ],

            [
                'name' => 'Jornais ou revistas',
            ],

            [
                'name' => 'Tubos de fogo de artifício',
            ],

            [
                'name' => 'Fragementos de papel',
            ],

            [
                'name' => 'Papel',
            ],

            [
                'name' => 'Outros artigos de papel',
            ],

            [
                'name' => 'Rolhas',
            ],

            [
                'name' => 'Paletes',
            ],

            [
                'name' => 'Madeira processada',
            ],

            [
                'name' => 'Grades ou caixotes',
            ],

            [
                'name' => 'Armadilhas para caranguejos ou lagostas',
            ],

            [
                'name' => 'Caixas de pesca',
            ],

            [
                'name' => 'Paus de gelados e chupa-chupas, garfos de batatas fritas, palitos de dentes',
            ],

            [
                'name' => 'Trinchas ou pinceis de pintura',
            ],

            [
                'name' => 'Cartuchos e suportes de fogo de artifício',
            ],

            [
                'name' => 'Tábuas de madeira',
            ],

            [
                'name' => 'Vigas ou barrotes',
            ],

            [
                'name' => 'Madeira (processada)',
            ],

            [
                'name' => 'Outras madeiras (<50cm)',
            ],

            [
                'name' => 'Outras madeiras (>50cm)',
            ],

            [
                'name' => 'Outros (específique)',
            ],

            [
                'name' => 'Aerosóis ou latas de spray industriais',
            ],

            [
                'name' => 'Latas de bebidas',
            ],

            [
                'name' => 'Latas de comida',
            ],

            [
                'name' => 'Invólucros de folha, folha de alumínio',
            ],

            [
                'name' => 'Tampas de garrafa, tampas e abas de puxar',
            ],

            [
                'name' => 'Grelhas de um só uso',
            ],

            [
                'name' => 'Dispositivos eléctricos (frigorífico, micro-ondas, etc.)',
            ],

            [
                'name' => 'Talheres (pratos, copos e talheres)',
            ],

            [
                'name' => 'Relacionado à pesca (pesos, chumbadas, iscos, anzóis)',
            ],

            [
                'name' => 'Restos de anzol',
            ],

            [
                'name' => 'Armadilhas para caranguejo ou lagosta',
            ],

            [
                'name' => 'Recipientes de tamanho médio',
            ],

            [
                'name' => 'Escórias industriais',
            ],

            [
                'name' => 'Bidões de óleo',
            ],

            [
                'name' => 'Outras latas (<4L)',
            ],

            [
                'name' => 'Garrafas de gás, tambores e baldes (>4L)',
            ],

            [
                'name' => 'Latas de tinta',
            ],

            [
                'name' => 'Arame, rolo de arame, arame farpado',
            ],

            [
                'name' => 'Barris',
            ],

            [
                'name' => 'Partes de carros ou baterias',
            ],

            [
                'name' => 'Cabos',
            ],

            [
                'name' => 'Baterias domésticas',
            ],

            [
                'name' => 'Objetos metálicos grandes',
            ],

            [
                'name' => 'Metal (outros)',
            ],

            [
                'name' => 'Outras peças de metal (<50cm)',
            ],

            [
                'name' => 'Outras peças de metal (>50cm)',
            ],

            [
                'name' => 'Garrafas incl. peças',
            ],

            [
                'name' => 'Frascos incl. peças',
            ],

            [
                'name' => 'Lâmpadas redondas ou tubulares',
            ],

            [
                'name' => 'Talheres (pratos e copos)',
            ],

            [
                'name' => 'Material de construção, p.ex.: azulejo, telha',
            ],

            [
                'name' => 'Tubos de luz fluorescente',
            ],

            [
                'name' => 'Bóias de vidro',
            ],

            [
                'name' => 'Alcatruzes para polvos',
            ],

            [
                'name' => 'Fragmentos de vidro ou cerâmica (>2.5cm)',
            ],
            [
                'name' => 'Objetos de vidro grandes (especifique)',
            ],
            [
                'name' => 'Outros objetos de vidro',
            ],
            [
                'name' => 'Artigos Médicos',
            ],

            [
                'name' => 'Carvão',
            ],

            [
                'name' => 'Parafina ou peças de cera',
            ],

            [
                'name' => 'Óleo ou Alcatrão',
            ],

            [
                'name' => 'Resíduos de alimentos (resíduos de cozinha)',
            ],

            [
                'name' => 'Diferentes tipos de lixo',

            ],
        ];


        foreach ($items as $key => $item) {
            $newItem = new LitterSubCategory($item);
            $newItem
                ->setTranslation('name', 'en', $item['name'])
                ->setTranslation('name', 'pt', $PTitems[$key]['name'])
                ->save();
        }
    }
}
