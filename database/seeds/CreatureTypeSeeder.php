<?php

use Illuminate\Database\Seeder;
use App\CreatureType;

class CreatureTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        CreatureType::create([
            'taxa_category' => 'Whale',
            'image' => '/images/creature-type/1',

        ]);
        CreatureType::create([
            'taxa_category' => 'Plant', //2
            'image' => '/images/creature-type/2',

        ]);

        CreatureType::create([
            'taxa_category' => 'Fish', //3
            'image' => '/images/creature-type/3',

        ]);

        CreatureType::create([
            'taxa_category' => 'Coral', //4
            'image' => '/images/creature-type/4',

        ]);

        CreatureType::create([
            'taxa_category' => 'Crab', //5
            'image' => '/images/creature-type/5',

        ]);

        CreatureType::create([
            'taxa_category' => 'Sessile Invertebrate', //6
            'image' => '/images/creature-type/6',

        ]);

        CreatureType::create([
            'taxa_category' => 'Reptile', //7
            'image' => '/images/creature-type/7',

        ]);

        CreatureType::create([
            'taxa_category' => 'Algae', //8
            'image' => '/images/creature-type/8',

        ]);

        CreatureType::create([
            'taxa_category' => 'Mollusc', //9
            'image' => '/images/creature-type/9',

        ]);

        CreatureType::create([
            'taxa_category' => 'Turtle', //10
            'image' => '/images/creature-type/10',

        ]);

        CreatureType::create([
            'taxa_category' => 'Dolphin', //11
            'image' => '/images/creature-type/11',

        ]);

        CreatureType::create([
            'taxa_category' => 'Seal', //12
            'image' => '/images/creature-type/12',

        ]);

        CreatureType::create([
            'taxa_category' => 'Bird', //13
            'image' => '/images/creature-type/13',

        ]);

        CreatureType::create([
            'taxa_category' => 'Other', //14
            'image' => '/images/creature-type/14',

        ]);
    }
}
