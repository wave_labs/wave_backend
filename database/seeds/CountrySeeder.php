<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Country::create([
			'code' => 'AF',
        	'name' => 'Afghanistan',
        	'folder' => 'afghanistan',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AL',
        	'name' => 'Albania',
        	'folder' => 'albania',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'DZ',
        	'name' => 'Algeria',
        	'folder' => 'algeria',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'DS',
        	'name' => 'American Samoa',
        	'folder' => 'american_samoa',
        	'continent' => 'Australia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AD',
        	'name' => 'Andorra',
        	'folder' => 'andorra',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AO',
        	'name' => 'Angola',
        	'folder' => 'angola',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AI',
        	'name' => 'Anguilla',
        	'folder' => 'anguilla',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AQ',
        	'name' => 'Antarctica',
        	'folder' => 'antarctica',
        	'continent' => 'Antarctica',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AG',
        	'name' => 'Antigua',
        	'folder' => 'antigua',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AR',
        	'name' => 'Argentina',
        	'folder' => 'argentina',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AM',
        	'name' => 'Armenia',
        	'folder' => 'armenia',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AW',
        	'name' => 'Aruba',
        	'folder' => 'aruba',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AU',
        	'name' => 'Australia',
        	'folder' => 'australia',
        	'continent' => 'Australia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AT',
        	'name' => 'Austria',
        	'folder' => 'austria',
        	'continent' => 'Europse',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'AZ',
        	'name' => 'Azerbaijan',
        	'folder' => 'azerbaijan',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BS',
        	'name' => 'The Bahamas',
        	'folder' => 'bahamas',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BH',
        	'name' => 'Bahrain',
        	'folder' => 'bahrain',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BD',
        	'name' => 'Bangladesh',
        	'folder' => 'bangladesh',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BB',
        	'name' => 'Barbados',
        	'folder' => 'barbados',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BY',
        	'name' => 'Belarus',
        	'folder' => 'belarus',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BE',
        	'name' => 'Belgium',
        	'folder' => 'belgium',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BZ',
        	'name' => 'Belize',
        	'folder' => 'Belize',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BJ',
        	'name' => 'Benin',
        	'folder' => 'benin',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BM',
        	'name' => 'Bermuda',
        	'folder' => 'bermuda',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BT',
        	'name' => 'Bhutan',
        	'folder' => 'bhutan',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BO',
        	'name' => 'Bolivia',
        	'folder' => 'bolivia',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BA',
        	'name' => 'Bosnia and Herzegovina',
        	'folder' => 'bosnia_and_herzegovina',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BW',
        	'name' => 'Botswana',
        	'folder' => 'botswana',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BV',
        	'name' => 'Bouvet Island',
        	'folder' => 'bouvet_island',
        	'continent' => 'Antarctica',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BR',
        	'name' => 'Brazil',
        	'folder' => 'brazil',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'IO',
        	'name' => 'British Indian Ocean Territory',
        	'folder' => 'british_indian_ocean_territory',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BN',
        	'name' => 'Brunei',
        	'folder' => 'brunei_darussalam',
        	'continent' => 'Asia',
            'active' => 1
    	]);
		
		Country::create([
			'code' => 'BG',
        	'name' => 'Bulgaria',
        	'folder' => 'bulgaria',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BF',
        	'name' => 'Burkina Faso',
        	'folder' => 'burkina_faso',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'BI',
        	'name' => 'Burundi',
        	'folder' => 'burundi',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'KH',
        	'name' => 'Cambodia',
        	'folder' => 'cambodia',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CM',
        	'name' => 'Cameroon',
        	'folder' => 'cameroon',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CA',
        	'name' => 'Canada',
        	'folder' => 'canada',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CV',
        	'name' => 'Cape Verde',
        	'folder' => 'cape_verde',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'KY',
        	'name' => 'Cayman Islands',
        	'folder' => 'cayman_islands',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CF',
        	'name' => 'Central African Republic',
        	'folder' => 'central_african_republic',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'TD',
        	'name' => 'Chad',
        	'folder' => 'chad',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CL',
        	'name' => 'Chile',
        	'folder' => 'chile',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CN',
        	'name' => 'China',
        	'folder' => 'china',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CX',
        	'name' => 'Christmas Island',
        	'folder' => 'christmas_island',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CC',
        	'name' => 'Cocos (Keeling) Islands',
        	'folder' => 'cocos_keeling_islands',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CO',
        	'name' => 'Colombia',
        	'folder' => 'colombia',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'KM',
        	'name' => 'Comoros',
        	'folder' => 'comoros',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CG',
        	'name' => 'Democratic Republic of the Congo',
        	'folder' => 'congo',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CK',
        	'name' => 'Cook Islands',
        	'folder' => 'cook_islands',
        	'continent' => 'Australia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CR',
        	'name' => 'Costa Rica',
        	'folder' => 'costa_rica',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'HR',
        	'name' => 'Croatia',
        	'folder' => 'croatia_hrvatska',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CU',
        	'name' => 'Cuba',
        	'folder' => 'cuba',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CY',
        	'name' => 'Cyprus',
        	'folder' => 'cyprus',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'CZ',
        	'name' => 'Czech Republic',
        	'folder' => 'czech_republic',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'DK',
        	'name' => 'Denmark',
        	'folder' => 'denmark',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'DJ',
        	'name' => 'Djibouti',
        	'folder' => 'djibouti',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'DM',
        	'name' => 'Dominica',
        	'folder' => 'dominica',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'DO',
        	'name' => 'Dominican Republic',
        	'folder' => 'dominican_republic',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'TP',
        	'name' => 'East Timor',
        	'folder' => 'east_timor',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'EC',
        	'name' => 'Ecuador',
        	'folder' => 'ecuador',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'EG',
        	'name' => 'Egypt',
        	'folder' => 'egypt',
        	'continent' => 'Africa',
            'active' => 1
    	]);

		Country::create([
			'code' => 'SV',
        	'name' => 'El Salvador',
        	'folder' => 'el_salvador',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GQ',
        	'name' => 'Equatorial Guinea',
        	'folder' => 'equatorial_guinea',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'ER',
        	'name' => 'Eritrea',
        	'folder' => 'eritrea',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'EE',
        	'name' => 'Estonia',
        	'folder' => 'estonia',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'ET',
        	'name' => 'Ethiopia',
        	'folder' => 'ethiopia',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'FK',
        	'name' => 'Falkland Islands',
        	'folder' => 'falkland_islands_malvinas',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'FO',
        	'name' => 'Faroe Islands',
        	'folder' => 'faroe_islands',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'FJ',
        	'name' => 'Fiji',
        	'folder' => 'fiji',
        	'continent' => 'Australia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'FI',
        	'name' => 'Finland',
        	'folder' => 'finland',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'FR',
        	'name' => 'France',
        	'folder' => 'france',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'FX',
        	'name' => 'France, Metropolitan',
        	'folder' => 'france_metropolitan',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GF',
        	'name' => 'France Guiana',
        	'folder' => 'french_guiana',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'PF',
        	'name' => 'France Polynesia',
        	'folder' => 'french_polynesia',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'TF',
        	'name' => 'France Southern Territories',
        	'folder' => 'french_southern_territories',
        	'continent' => 'Antarctica',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GA',
        	'name' => 'Gabon',
        	'folder' => 'gabon',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GM',
        	'name' => 'The Gambia',
        	'folder' => 'gambia',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GE',
        	'name' => 'Georgia',
        	'folder' => 'georgia',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'DE',
        	'name' => 'Germany',
        	'folder' => 'germany',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GH',
        	'name' => 'Ghana',
        	'folder' => 'ghana',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GI',
        	'name' => 'Gibraltar',
        	'folder' => 'gibraltar',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GK',
        	'name' => 'Guernsey',
        	'folder' => 'guernsey',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GR',
        	'name' => 'Greece',
        	'folder' => 'greece',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GL',
        	'name' => 'Greenland',
        	'folder' => 'greenland',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GD',
        	'name' => 'Grenada',
        	'folder' => 'grenada',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GP',
        	'name' => 'Guadeloupe',
        	'folder' => 'guadeloupe',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GU',
        	'name' => 'Guam',
        	'folder' => 'guam',
        	'continent' => 'Australia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GT',
        	'name' => 'Guatemala',
        	'folder' => 'guatemala',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GN',
        	'name' => 'Guinea',
        	'folder' => 'guinea',
        	'continent' => 'Australia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GW',
        	'name' => 'Guinea-Bissau',
        	'folder' => 'guinea-bissau',
        	'continent' => 'Africa',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'GY',
        	'name' => 'Guyana',
        	'folder' => 'guyana',
        	'continent' => 'South America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'HT',
        	'name' => 'Haiti',
        	'folder' => 'haiti',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'HM',
        	'name' => 'Heard Island and McDonald Islands',
        	'folder' => 'heard_and_mc_donald_islands',
        	'continent' => 'Australia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'HN',
        	'name' => 'Honduras',
        	'folder' => 'honduras',
        	'continent' => 'North America',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'HK',
        	'name' => 'Hong Kong',
        	'folder' => 'hong_kong',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'HU',
        	'name' => 'Hungary',
        	'folder' => 'hungary',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'IS',
        	'name' => 'Iceland',
        	'folder' => 'iceland',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'IN',
        	'name' => 'India',
        	'folder' => 'india',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'IM',
        	'name' => 'Isle of Man',
        	'folder' => 'isle_of_man',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'ID',
        	'name' => 'Indonesia',
        	'folder' => 'indonesia',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'IR',
        	'name' => 'Iran (Islamic Republic of)',
        	'folder' => 'iran_islamic_republic_of',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'IQ',
        	'name' => 'Iraq',
        	'folder' => 'iraq',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'IE',
        	'name' => 'Ireland',
        	'folder' => 'ireland',
        	'continent' => 'Europe',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'IL',
        	'name' => 'Israel',
        	'folder' => 'israel',
        	'continent' => 'Asia',
            'active' => 1
    	]);

    	Country::create([
			'code' => 'IT',
        	'name' => 'Italy',
        	'folder' => 'italy',
        	'continent' => 'Europe',
            'active' => 1
    	]);

		Country::create([
			'code' => 'CI',
	    	'name' => 'Ivory Coast',
	    	'folder' => 'ivory_coast',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'JE',
	    	'name' => 'Jersey',
	    	'folder' => 'jersey',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'JM',
	    	'name' => 'Jamaica',
	    	'folder' => 'jamaica',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'JP',
	    	'name' => 'Japan',
	    	'folder' => 'japan',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'JO',
	    	'name' => 'Jordan',
	    	'folder' => 'jordan',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'KZ',
	    	'name' => 'Kazakhstan',
	    	'folder' => 'kazakhstan',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'KE',
	    	'name' => 'Kenya',
	    	'folder' => 'kenya',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'KI',
	    	'name' => 'Kiribati',
	    	'folder' => 'kiribati',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'KP',
	    	'name' => 'North Korea',
	    	'folder' => 'korea_democratic_peoples_republic_of',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'KR',
	    	'name' => 'South Korea',
	    	'folder' => 'korea_republic_of',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'XK',
	    	'name' => 'Kosovo',
	    	'folder' => 'kosovo',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'KW',
	    	'name' => 'Kuwait',
	    	'folder' => 'kuwait',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'KG',
	    	'name' => 'Kyrgyzstan',
	    	'folder' => 'kyrgyzstan',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LA',
	    	'name' => 'Laos',
	    	'folder' => 'lao_peoples_democratic_republic',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LV',
	    	'name' => 'Latvia',
	    	'folder' => 'latvia',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LB',
	    	'name' => 'Lebanon',
	    	'folder' => 'lebanon',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LS',
	    	'name' => 'Lesotho',
	    	'folder' => 'lesotho',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LR',
	    	'name' => 'Liberia',
	    	'folder' => 'liberia',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LY',
	    	'name' => 'Libyan Arab Jamahiriya',
	    	'folder' => 'libyan_arab_jamahiriya',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LI',
	    	'name' => 'Liechtenstein',
	    	'folder' => 'liechtenstein',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LT',
	    	'name' => 'Lithuania',
	    	'folder' => 'lithuania',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LU',
	    	'name' => 'Luxembourg',
	    	'folder' => 'luxembourg',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MO',
	    	'name' => 'Macau',
	    	'folder' => 'macau',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MK',
	    	'name' => 'Macedonia',
	    	'folder' => 'macedonia',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MG',
	    	'name' => 'Madagascar',
	    	'folder' => 'madagascar',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MW',
	    	'name' => 'Malawi',
	    	'folder' => 'malawi',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MY',
	    	'name' => 'Malaysia',
	    	'folder' => 'malaysia',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MV',
	    	'name' => 'Maldives',
	    	'folder' => 'maldives',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'ML',
	    	'name' => 'Mali',
	    	'folder' => 'mali',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MT',
	    	'name' => 'Malta',
	    	'folder' => 'malta',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MH',
	    	'name' => 'Marshall Islands',
	    	'folder' => 'marshall_islands',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MQ',
	    	'name' => 'Martinique',
	    	'folder' => 'martinique',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MR',
	    	'name' => 'Mauritania',
	    	'folder' => 'mauritania',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MU',
	    	'name' => 'Mauritius',
	    	'folder' => 'mauritius',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TY',
	    	'name' => 'Mayotte',
	    	'folder' => 'mayotte',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MX',
	    	'name' => 'Mexico',
	    	'folder' => 'mexico',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'FM',
	    	'name' => 'Federated States of Micronesia',
	    	'folder' => 'micronesia_federated_states_of',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MD',
	    	'name' => 'Moldova',
	    	'folder' => 'moldova_republic_of',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MC',
	    	'name' => 'Monaco',
	    	'folder' => 'monaco',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MN',
	    	'name' => 'Mongolia',
	    	'folder' => 'mongolia',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'ME',
	    	'name' => 'Montenegro',
	    	'folder' => 'montenegro',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MS',
	    	'name' => 'Montserrat',
	    	'folder' => 'montserrat',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MA',
	    	'name' => 'Morocco',
	    	'folder' => 'morocco',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MZ',
	    	'name' => 'Mozambique',
	    	'folder' => 'mozambique',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MM',
	    	'name' => 'Myanmar',
	    	'folder' => 'myanmar',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NA',
	    	'name' => 'Namibia',
	    	'folder' => 'namibia',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NR',
	    	'name' => 'Nauru',
	    	'folder' => 'nauru',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NP',
	    	'name' => 'Nepal',
	    	'folder' => 'nepal',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NL',
	    	'name' => 'Netherlands',
	    	'folder' => 'netherlands',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'AN',
	    	'name' => 'Netherlands Antilles',
	    	'folder' => 'netherlands_antilles',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NC',
	    	'name' => 'New Caledonia',
	    	'folder' => 'new_caledonia',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NZ',
	    	'name' => 'New Zealand',
	    	'folder' => 'new_zealand',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NI',
	    	'name' => 'Nicaragua',
	    	'folder' => 'nicaragua',
	    	'continent' => 'South America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NE',
	    	'name' => 'Niger',
	    	'folder' => 'niger',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NG',
	    	'name' => 'Nigeria',
	    	'folder' => 'nigeria',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NU',
	    	'name' => 'Niue',
	    	'folder' => 'niue',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NF',
	    	'name' => 'Norfolk Island',
	    	'folder' => 'norfolk_island',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'MP',
	    	'name' => 'Northern Mariana Islands',
	    	'folder' => 'northern_mariana_islands',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'NO',
	    	'name' => 'Norway',
	    	'folder' => 'norway',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'OM',
	    	'name' => 'Oman',
	    	'folder' => 'oman',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PK',
	    	'name' => 'Pakistan',
	    	'folder' => 'pakistan',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PW',
	    	'name' => 'Palau',
	    	'folder' => 'palau',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PS',
	    	'name' => 'Palestine',
	    	'folder' => 'palestine',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PA',
	    	'name' => 'Panama',
	    	'folder' => 'panama',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PG',
	    	'name' => 'Papua New Guinea',
	    	'folder' => 'papua_new_guinea',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PY',
	    	'name' => 'Paraguay',
	    	'folder' => 'paraguay',
	    	'continent' => 'South America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PE',
	    	'name' => 'Peru',
	    	'folder' => 'peru',
	    	'continent' => 'South America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PH',
	    	'name' => 'Philippines',
	    	'folder' => 'philippines',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PN',
	    	'name' => 'Pitcairn',
	    	'folder' => 'pitcairn',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PL',
	    	'name' => 'Poland',
	    	'folder' => 'poland',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PT',
	    	'name' => 'Portugal',
	    	'folder' => 'portugal',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PR',
	    	'name' => 'Puerto Rico',
	    	'folder' => 'puerto_rico',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'QA',
	    	'name' => 'Qatar',
	    	'folder' => 'qatar',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'RE',
	    	'name' => 'Reunion',
	    	'folder' => 'reunion',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'RO',
	    	'name' => 'Romania',
	    	'folder' => 'romania',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'RU',
	    	'name' => 'Russia',
	    	'folder' => 'russian_federation',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'RW',
	    	'name' => 'Rwanda',
	    	'folder' => 'rwanda',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'KN',
	    	'name' => 'Saint Kitts and Nevis',
	    	'folder' => 'saint_kitts_and_nevis',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LC',
	    	'name' => 'St. Lucia',
	    	'folder' => 'saint_lucia',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'VC',
	    	'name' => 'Saint Vincent and the Grenadines',
	    	'folder' => 'saint_vincent_and_the_grenadines',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'WS',
	    	'name' => 'Samoa',
	    	'folder' => 'samoa',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SM',
	    	'name' => 'San Marino',
	    	'folder' => 'san_marino',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'ST',
	    	'name' => 'Sao Tome and Principe',
	    	'folder' => 'sao_tome_and_principe',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SA',
	    	'name' => 'Saudi Arabia',
	    	'folder' => 'saudi_arabia',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SN',
	    	'name' => 'Senegal',
	    	'folder' => 'senegal',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'RS',
	    	'name' => 'Serbia',
	    	'folder' => 'serbia',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SC',
	    	'name' => 'Seychelles',
	    	'folder' => 'seychelles',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SL',
	    	'name' => 'Sierra Leone',
	    	'folder' => 'sierra_leone',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SG',
	    	'name' => 'Singapore',
	    	'folder' => 'singapore',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SK',
	    	'name' => 'Slovakia',
	    	'folder' => 'slovakia',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SI',
	    	'name' => 'Slovenia',
	    	'folder' => 'slovenia',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SB',
	    	'name' => 'Solomon Islands',
	    	'folder' => 'solomon_islands',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SO',
	    	'name' => 'Somalia',
	    	'folder' => 'somalia',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'ZA',
	    	'name' => 'South Africa',
	    	'folder' => 'south_africa',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'GS',
	    	'name' => 'South Georgia and the South Sandwich Islands',
	    	'folder' => 'south_georgia_south_sandwich_islands',
	    	'continent' => 'Antarctica',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'ES',
	    	'name' => 'Spain',
	    	'folder' => 'spain',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'LK',
	    	'name' => 'Sri Lanka',
	    	'folder' => 'sri_lanka',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SH',
	    	'name' => 'Saint Helena',
	    	'folder' => 'st._helena',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'PM',
	    	'name' => 'St. Pierre and Miquelon',
	    	'folder' => 'st._pierre_and_miquelon',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SD',
	    	'name' => 'Sudan',
	    	'folder' => 'sudan',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SR',
	    	'name' => 'Suriname',
	    	'folder' => 'suriname',
	    	'continent' => 'South America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SJ',
	    	'name' => 'Svalbard and Jan Mayen',
	    	'folder' => 'svalbard_and_jan_mayen_islands',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SZ',
	    	'name' => 'Swaziland',
	    	'folder' => 'swaziland',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SE',
	    	'name' => 'Sweden',
	    	'folder' => 'sweden',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'CH',
	    	'name' => 'Switzerland',
	    	'folder' => 'switzerland',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'SY',
	    	'name' => 'Syrian Arab Republic',
	    	'folder' => 'syrian_arab_republic',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TW',
	    	'name' => 'Taiwan',
	    	'folder' => 'taiwan',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TJ',
	    	'name' => 'Tajikistan',
	    	'folder' => 'tajikistan',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TZ',
	    	'name' => 'Tanzania',
	    	'folder' => 'tanzania_united_republic_of',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TH',
	    	'name' => 'Thailand',
	    	'folder' => 'thailand',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TG',
	    	'name' => 'Togo',
	    	'folder' => 'togo',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TK',
	    	'name' => 'Tokelau',
	    	'folder' => 'tokelau',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TO',
	    	'name' => 'Tonga',
	    	'folder' => 'tonga',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TT',
	    	'name' => 'Trinidad and Tobago',
	    	'folder' => 'trinidad_and_tobago',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TN',
	    	'name' => 'Tunisia',
	    	'folder' => 'tunisia',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TR',
	    	'name' => 'Turkey',
	    	'folder' => 'turkey',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TM',
	    	'name' => 'Turkmenistan',
	    	'folder' => 'turkmenistan',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TC',
	    	'name' => 'Turks and Caicos Islands',
	    	'folder' => 'turks_and_caicos_islands',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'TV',
	    	'name' => 'Tuvalu',
	    	'folder' => 'tuvalu',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'UG',
	    	'name' => 'Uganda',
	    	'folder' => 'uganda',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'UA',
	    	'name' => 'Ukraine',
	    	'folder' => 'ukraine',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'AE',
	    	'name' => 'United Arab Emirates',
	    	'folder' => 'united_arab_emirates',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'GB',
	    	'name' => 'United Kingdom',
	    	'folder' => 'united_kingdom',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'US',
	    	'name' => 'United States',
	    	'folder' => 'united_states',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'UM',
	    	'name' => 'United States Minor Outlying Islands',
	    	'folder' => 'united_states_minor_outlying_islands',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'UY',
	    	'name' => 'Uruguay',
	    	'folder' => 'uruguay',
	    	'continent' => 'South America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'UZ',
	    	'name' => 'Uzbekistan',
	    	'folder' => 'uzbekistan',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'VU',
	    	'name' => 'Vanuatu',
	    	'folder' => 'vanuatu',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'VA',
	    	'name' => 'Vatican City',
	    	'folder' => 'vatican_city_state',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'VE',
	    	'name' => 'Venezuela',
	    	'folder' => 'venezuela',
	    	'continent' => 'South America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'VN',
	    	'name' => 'Vietnam',
	    	'folder' => 'vietnam',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'VG',
	    	'name' => 'Virgin Islands (British)',
	    	'folder' => 'virgin_islands_british',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'VI',
	    	'name' => 'Virgin Islands (U.S.)',
	    	'folder' => 'virgin_islands_u',
	    	'continent' => 'North America',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'WF',
	    	'name' => 'Wallis and Futuna',
	    	'folder' => 'wallis_and_futuna_islands',
	    	'continent' => 'Australia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'EH',
	    	'name' => 'Western Sahara',
	    	'folder' => 'western_sahara',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'YE',
	    	'name' => 'Yemen',
	    	'folder' => 'yemen',
	    	'continent' => 'Asia',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'YU',
	    	'name' => 'Yugoslavia',
	    	'folder' => 'yugoslavia',
	    	'continent' => 'Europe',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'ZR',
	    	'name' => 'Zaire',
	    	'folder' => 'zaire',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'ZM',
	    	'name' => 'Zambia',
	    	'folder' => 'zambia',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'ZW',
	    	'name' => 'Zimbabwe',
	    	'folder' => 'zimbabwe',
	    	'continent' => 'Africa',
	        'active' => 1
    	]);

    	Country::create([
			'code' => 'BL',
	    	'name' => 'Saint Barthelemy',
	    	'folder' => 'saint_barthelemy',
	    	'continent' => 'South America',
	        'active' => 1
    	]);
		
    }
}
