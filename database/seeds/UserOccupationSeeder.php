<?php

use Illuminate\Database\Seeder;
use App\UserOccupation;

class UserOccupationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserOccupation::create([
            'name' => 'researcher'
        ]);
        UserOccupation::create([
            'name' => 'tourist'
        ]);
        UserOccupation::create([
            'name' => 'marine biologist'
        ]);
    }
}
