<?php

use App\SuggestionType;
use Illuminate\Database\Seeder;

class SuggestionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SuggestionType::create([
            'name' => 'error',
        ]);

        SuggestionType::create([
            'name' => 'help',
        ]);

        SuggestionType::create([
            'name' => 'collaboration',
        ]);

        SuggestionType::create([
            'name' => 'suggestion',
        ]);

        SuggestionType::create([
            'name' => 'other',
        ]);
    }
}
