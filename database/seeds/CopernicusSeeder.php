<?php

use App\CopernicusParameter;
use App\CopernicusProduct;
use Illuminate\Database\Seeder;

class CopernicusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $parameters = [
            ['name' => 'temperature'],
            ['name' => 'salinity'],
            ['name' => 'sea surface height'],
            ['name' => 'velocity'],
            ['name' => 'mixed layer thickness'],
            ['name' => 'sea ice'],
            ['name' => 'wind'],
            ['name' => 'wave'],
            ['name' => 'primary producers'],
            ['name' => 'low trophic level'],
            ['name' => 'mid trophic level'],
            ['name' => 'oxygen'],
            ['name' => 'nutrients'],
            ['name' => 'carbonate system'],
            ['name' => 'reflectance'],
            ['name' => 'transparency'],
            ['name' => 'turbidity'],
        ];

        foreach ($parameters as $parameter) {
            CopernicusParameter::create($parameter);
        }

        $products = [
            [
                "object" => [
                    'name' => 'Global Ocean 1/12° Physics Analysis and Forecast updated Daily',
                    'path' => 'global-analysis-forecast-phy-001-024',
                    'identifier' => 'GLOBAL_ANALYSIS_FORECAST_PHY_001_024',
                    'domain' => 'Global Ocean'
                ],
                "parameters" => [1, 2, 3, 4, 5, 6]
            ],
            [
                "object" => [
                    'name' => 'Global Ocean Waves Analysis and Forecast',
                    'path' => 'global-analysis-forecast-wav-001-027',
                    'identifier' => 'GLOBAL_ANALYSIS_FORECAST_WAV_001_027',
                    'domain' => 'Global Ocean'
                ],
                "parameters" => [8]
            ],
            [
                "object" => [
                    'name' => 'Global Ocean Biogeochemistry Analysis and Forecast',
                    'path' => 'global-analysis-forecast-bio-001-028-daily',
                    'identifier' => 'GLOBAL_ANALYSIS_FORECAST_BIO_001_028',
                    'domain' => 'Global Ocean'
                ],
                "parameters" => [9, 12, 13, 14]
            ],
            [
                "object" => [
                    'name' => 'Atlantic-Iberian Biscay Irish- Ocean Biogeochemical Analysis and Forecast',
                    'path' => 'cmems_mod_ibi_bgc_anfc_0.027deg-3D_P1D-m',
                    'identifier' => 'IBI_ANALYSISFORECAST_BGC_005_004',
                    'domain' => 'Iberia-Biscay-Ireland Regional Seas'
                ],
                "parameters" => [9, 12, 13, 14, 17]
            ],
            [
                "object" => [
                    'name' => 'Atlantic-Iberian Biscay Irish- Ocean Physics Analysis and Forecast',
                    'path' => 'cmems_mod_ibi_phy_anfc_0.027deg-2D_PT1H-m',
                    'identifier' => 'IBI_ANALYSISFORECAST_PHY_005_001',
                    'domain' => 'Iberia-Biscay-Ireland Regional Seas'
                ],
                "parameters" => [1, 2, 3, 4, 5]
            ],
            [
                "object" => [
                    'name' => 'Atlantic-Iberian Biscay Irish- Ocean Wave Analysis and Forecast',
                    'path' => 'dataset-ibi-analysis-forecast-wav-005-005-hourly',
                    'identifier' => 'IBI_ANALYSIS_FORECAST_WAV_005_005',
                    'domain' => 'Iberia-Biscay-Ireland Regional Seas'
                ],
                "parameters" => [8]
            ],
            [
                "object" => [
                    'name' => 'European Sea Surface Chlorophyll Concentration from Multi Satellite observations',
                    'path' => 'dataset-oc-eur-chl-multi-l3-chl_1km_daily-rt-v02',
                    'identifier' => 'OCEANCOLOUR_EUR_CHL_L3_NRT_OBSERVATIONS_009_050',
                    'domain' => 'Mediterranean Sea'
                ],
                "parameters" => [8]
            ],
        ];

        foreach ($products as $product) {
            $nProduct = CopernicusProduct::create($product["object"]);

            $nProduct->parameters()->attach($product["parameters"]);
        }
    }
}
