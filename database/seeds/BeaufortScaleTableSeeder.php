<?php

use Illuminate\Database\Seeder;
use App\BeaufortScale;

class BeaufortScaleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BeaufortScale::create([
			'scale' => '0',
        	'desc' => 'Calm',
        	'sea_conditions' => 'Sea like a mirror',
        	'land_conditions' => 'Smoke rises vertically.',
    	]);

    	BeaufortScale::create([
			'scale' => '1',
        	'desc' => 'Light air',
        	'sea_conditions' => 'Ripples with appearance of scales are formed, without foam crests',
        	'land_conditions' => 'Direction shown by smoke drift but not by wind vanes.',
    	]);

    	BeaufortScale::create([
			'scale' => '2',
        	'desc' => 'Light breeze',
        	'sea_conditions' => 'Small wavelets still short but more pronounced; crests have a glassy appearance but do not break',
        	'land_conditions' => 'Wind felt on face; leaves rustle; wind vane moved by wind.',
    	]);

    	BeaufortScale::create([
			'scale' => '3',
        	'desc' => 'Gentle breeze',
        	'sea_conditions' => 'Large wavelets; crests begin to break; foam of glassy appearance; perhaps scattered white horses',
        	'land_conditions' => 'Leaves and small twigs in constant motion; light flags extended.',
    	]);

    	BeaufortScale::create([
			'scale' => '4',
        	'desc' => 'Moderate breeze',
        	'sea_conditions' => 'Small waves becoming longer; fairly frequent white horses',
        	'land_conditions' => 'Raises dust and loose paper; small branches moved.',
    	]);

    	BeaufortScale::create([
			'scale' => '5',
        	'desc' => 'Fresh breeze',
        	'sea_conditions' => 'Moderate waves taking a more pronounced long form; many white horses are formed; chance of some spray',
        	'land_conditions' => 'Small trees in leaf begin to sway; crested wavelets form on inland waters.',
    	]);

    	BeaufortScale::create([
			'scale' => '6',
        	'desc' => 'Strong breeze',
        	'sea_conditions' => 'Large waves begin to form; the white foam crests are more extensive everywhere; probably some spray',
        	'land_conditions' => 'Large branches in motion; whistling heard in telegraph wires; umbrellas used with difficulty.',
    	]);

    	BeaufortScale::create([
			'scale' => '7',
        	'desc' => 'High wind, moderate gale, near gale',
        	'sea_conditions' => 'Sea heaps up and white foam from breaking waves begins to be blown in streaks along the direction of the wind; spindrift begins to be seen',
        	'land_conditions' => 'Whole trees in motion; inconvenience felt when walking against the wind.',
    	]);

    	BeaufortScale::create([
			'scale' => '8',
        	'desc' => 'Gale, fresh gale',
        	'sea_conditions' => 'Moderately high waves of greater length; edges of crests break into spindrift; foam is blown in well-marked streaks along the direction of the wind',
        	'land_conditions' => 'Twigs break off trees; generally impedes progress.',
    	]);

    	BeaufortScale::create([
			'scale' => '9',
        	'desc' => 'Strong/severe gale',
        	'sea_conditions' => 'High waves; dense streaks of foam along the direction of the wind; sea begins to roll; spray affects visibility',
        	'land_conditions' => 'Slight structural damage (chimney pots and slates removed).',
    	]);

    	BeaufortScale::create([
			'scale' => '10',
        	'desc' => 'Storm, whole gale',
        	'sea_conditions' => 'Very high waves with long overhanging crests; resulting foam in great patches is blown in dense white streaks along the direction of the wind; on the whole the surface of the sea takes on a white appearance; rolling of the sea becomes heavy; visibility affected',
        	'land_conditions' => 'Seldom experienced inland; trees uprooted; considerable structural damage.',
    	]);

    	BeaufortScale::create([
			'scale' => '11',
        	'desc' => 'Violent storm',
        	'sea_conditions' => 'Exceptionally high waves; small- and medium-sized ships might be for a long time lost to view behind the waves; sea is covered with long white patches of foam; everywhere the edges of the wave crests are blown into foam; visibility affected',
        	'land_conditions' => 'Very rarely experienced; accompanied by widespread damage.',
    	]);

    	BeaufortScale::create([
			'scale' => '12',
        	'desc' => 'Hurricane force',
        	'sea_conditions' => 'The air is filled with foam and spray; sea is completely white with driving spray; visibility very seriously affected',
        	'land_conditions' => 'Devastation.',
    	]);

    }
}
