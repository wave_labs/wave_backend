<?php

use Illuminate\Database\Seeder;
use App\WhatsNewType;

class WhatsNewTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WhatsNewType::create([
            'type' => 'update',
        ]);

        WhatsNewType::create([
            'type' => 'event',
        ]);

        WhatsNewType::create([
            'type' => 'research',
        ]);
    }
}
