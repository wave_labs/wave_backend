<?php

use App\VehicleType;
use Illuminate\Database\Seeder;

class VehicleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VehicleType::create([
            'type' => 'Other',
        ]);

        VehicleType::create([
            'type' => 'USV',
        ]);

        VehicleType::create([
            'type' => 'ROV',
        ]);

        VehicleType::create([
            'type' => 'UAV',
        ]);
        
        VehicleType::create([
            'type' => 'Boat',
        ]);
    }
}
