<?php

use App\StudyQuestion;
use Illuminate\Database\Seeder;

class StudyQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $markers = [
            [
                'question' => 'Are you able to identify this fish?',
                'code' => 'knowledge',
            ],
            [
                'question' => 'If yes, may you please write his name?If yes, may you please write his name?',
                'code' => 'name_specie',
            ],
            [
                'question' => 'Do you remember when (month and year) you first saw this fish?',
                'code' => 'date',
            ],
            [
                'question' => 'In the last 10 years, how do you consider sightings and/or captures of this fish?',
                'code' => 'abundance',
            ],
            [
                'question' => 'Considering the last 5 years, do you think that other fish species have appeared in the archipelagos of Madeira?',
                'code' => 'other_species',
            ],
            [
                'question' => 'If yes, can you identify them and write their name(s)?',
                'code' => 'name_species',
            ],
            [
                'question' => 'One consequence of climate change is the increase of sea water temperature. Higher water temperatures may allow the arrival and establishment of marine species from other typically warmer regions. Do you think that this can be a threat for marine biodiversity?',
                'code' => 'consequences',
            ],
        ];

        foreach ($markers as $question) {
            $question['study_id'] = 1;

            StudyQuestion::create($question);
        }

        $freshwaterPins = [
            [
                'question' => 'Age',
                'code' => 'age',
            ],
            [
                'question' => 'Residence region',
                'code' => 'residence',
            ],
            [
                'question' => 'Have you ever seen this animal in Madeira or Porto Santo islands?',
                'code' => 'knowledge',
            ],
            [
                'question' => 'Write the name of the animal A',
                'code' => 'nameA',
            ],
            [
                'question' => 'Write the name of the animal B',
                'code' => 'nameB',
            ],
            [
                'question' => 'Which animal have you seen?',
                'code' => 'animal',
            ],
            [
                'question' => 'In what year?',
                'code' => 'year',
            ],
            [
                'question' => 'During which period of the year?',
                'code' => 'period_year',
            ],
            [
                'question' => 'During which period of the day?',
                'code' => 'period_day',
            ],
            [
                'question' => 'How many have you seen?',
                'code' => 'quantity',
            ],
            [
                'question' => 'In what stream?',
                'code' => 'stream',
            ],
            [
                'question' => 'What was the nearest location?',
                'code' => 'location',
            ],
            [
                'question' => 'In  which  council?',
                'code' => 'council',
            ],
            [
                'question' => 'Indicate which activity you were doing at the time',
                'code' => 'activity',
            ],
            [
                'question' => 'Observations',
                'code' => 'observations',
            ],
        ];

        foreach ($freshwaterPins as $question) {
            $question['study_id'] = 4;

            StudyQuestion::create($question);
        }
    }
}
