<?php


$items = [
    [
        [
            'type_id' => '3',
            'name' => 'Red hogfish',
            'name_scientific' => 'Bodianus scrofa',
            'description' => 'Red head and body. Female: lighter ventrally with yellow horizontal line in the middle of the body',
            'curiosity' => 'It has a limited distribution in the eastern Atlantic, with presence in less than ten locations. Its population is declining and is listed as vulnerable species. It shows marked haremic territorial system, typically one male to one to three females.  It changes its sex from female to male with growth.',
        ], [true], []
    ],
    [
        [
            'type_id' => '4',
            'name' => 'Black coral',
            'name_scientific' => 'Antipathella sp.',
            'description' => 'Despite the name, bushes can be white, yellow, pink, grey, green or reddish brown',
            'curiosity' => 'The horny skeleton, being hard and black through mineral intercalations, may used for manufacturing jewellery. The Greek used it to make amulets explaining the scientific name (anti pathes means against disease). Bushes of black coral are home of several animals.',
            'conserv' => 'Vulnerable',
            'size' => 'Up to 1.5 m high',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Native',
        ], [true], []
    ],
    [
        [
            'type_id' => '5',
            'name' => 'Swimming crab',
            'name_scientific' => 'Cronius ruber',
            'description' => 'Different colors from orange to brown. It is a swimming crab with aggressive behavior',
            'curiosity' => 'Characterized by slim claws with needle-like teeth, conferring a rapid action that in combination with paddle-like hind legs for swimming, make him an effective predator of fish and other soft-bodied fast-moving prey. Courtship ritual for coupling is common (through olfactory and tactile cues).',
            'conserv' => 'Vulnerable',
            'size' => 'Up to 5 cm length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [true], []
    ],
    [
        [
            'type_id' => '4',
            'name' => 'Green reef coral',
            'name_scientific' => 'Madracis asperula',
            'description' => 'Green/brown encrusting colony formed by many small polyps. It may grow in branches or flat',
            'curiosity' => 'The green/brown color is due to the presence of symbiotic algae called zooxanthellae that live in the coral tissue and help it with energy request through photosynthesis.  Without algae, the coral is white.',
            'conserv' => 'Vulnerable',
            'size' => 'Branches up to 20 mm, polyps 2 mm length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Native',
        ], [true], []
    ],
    [
        [
            'type_id' => '8',
            'name' => 'Harpoon weed',
            'name_scientific' => 'Asparagopsis sp.',
            'description' => 'Branches purple-red or branches and filamentous  brownish red',
            'curiosity' => 'It is a red algae and presents two distinct life phases morphologically different.  It presents the thallus with tufts of all about the same length and spirally arranged. It showed a strong cytotoxicity against human cancer cell lines.',
            'conserv' => 'Vulnerable',
            'size' => 'Branches up to 30 mm',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [true], []
    ],
    [
        [
            'type_id' => '6',
            'name' => 'Button tunicate',
            'name_scientific' => 'Distaplia corolla',
            'description' => 'Bright orange or purple colonies, forming lobes or heads developed in rosette and cluster',
            'curiosity' => '8-20 individuals (zooids) form a rosette. Each zooid has its own mouth but share a common central exhalent opening. Several rosette form a cluster. The species was first described from Azores but it seems to be a Caribbean species, probably arrived by yachts crossing the Atlantic.',
            'conserv' => 'Vulnerable',
            'size' => 'Zooid 2-4 mm long, rosette 1.5 cm diameter and cluster 40 cm diameter',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [true], []
    ],
    [
        [
            'type_id' => '10',
            'name' => 'Loggerhead turtle',
            'name_scientific' => 'Caretta caretta',
            'description' => 'Heart-shaped carapace that generally is reddish-brown hue with olive tones',
            'curiosity' => 'It is the largest hard-shelled sea turtles. It nests on insular and mainland sandy beaches throughout the temperate and subtropical regions worldwide. It can undertake migrations traversing oceanic zones spanning hundreds to thousands of kilometres. It is protected by several treaties and laws. ',
            'conserv' => 'Vulnerable',
            'size' => 'Up to 1 m length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Native',
        ], [true], [true]
    ],
    [
        [
            'type_id' => '4',
            'name' => 'Fire Coral',
            'name_scientific' => 'Millepora sp.',
            'description' => 'Green-yellow colony with white tips and many growth forms, from cylindrical branches to flat slabs',
            'curiosity' => 'Despite the name, it is not a true coral. The name fire coral comes from their ability to burn, in fact the polyps forming the colony are tiny but sting fiercely through cells called cnidocytes. They can inject a venom that causes a painful burning sensation, skin eruptions, blisters and scarring.',
            'conserv' => 'Vulnerable',
            'size' => 'Up to 45 cm high',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [true], []
    ],
    [
        [
            'type_id' => '6',
            'name' => 'Bushy encrusting anemone',
            'name_scientific' => 'Antipathozoanthus sp.',
            'description' => 'Polyps forming the colony have many tentacles with an intense yellow colour or slightly orange',
            'curiosity' => 'To support the construction of its skeleton, it prefers slightly overhanging black coral. A colony of bushy encrusting anemone can be spread out on a square meter or more.',
            'conserv' => 'Vulnerable',
            'size' => 'Single polyp up to 1.2 cm high and 0.6 cm diameter',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Native',
        ], [true], []
    ],
    [
        [
            'type_id' => '9',
            'name' => 'Warty umbrella snail',
            'name_scientific' => 'Umbraculum umbraculum',
            'description' => 'Limpet-like shell and the big mantle is covered by numerous rounded papillae. Variable colour',
            'curiosity' => 'This is a large, primitive species of slug with a round yellow body and its gills can be visible under the shell. The shell can be covered with encrusting plant and animal growths. As a means of defence, it produces a compound which tastes very disagreeable to fish.',
            'conserv' => 'Vulnerable',
            'size' => 'Up to 20 cm length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Native',
        ], [true], []
    ],
    [
        [
            'type_id' => '8',
            'name' => 'Caulerpa seaweed',
            'name_scientific' => 'Caulerpa webbiana',
            'description' => 'Tiny green bristles of branched filaments, set in whorls around the central "stem"',
            'curiosity' => ' Also known as bottlebrush green seaweed because its branches shape give the overall appearance of a stiff bottlebrush. It is a tropical species, common in Caribbean and Hawaii. Usually lives anchored in sediment or growing on coral reefs.',
            'conserv' => 'Vulnerable',
            'size' => 'Single stem of 4-6 cm long',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [true], []
    ],
    [
        [
            'type_id' => '9',
            'name' => 'Octopus',
            'name_scientific' => 'Octopus vulgaris',
            'description' => 'Octopus has 8 arms that are lined with suckers, it has a big mantle and lack of internal shell',
            'curiosity' => 'Octopus uses camouflage either to resemble other animals or to blend into their environment, changing its colour and its appearance by raising or lowering small protuberances all over the soft body. Usually the entrance of its den is full of leftovers of its food like mussel shells.',
            'conserv' => 'Vulnerable',
            'size' => 'Up to 25 cm in mantle length, up to 1 m in arms long',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Native',
        ], [true], []
    ],
    [
        [
            'type_id' => '3',
            'name' => 'Gilthead seabream',
            'name_scientific' => 'Sparus aurata',
            'description' => 'Silvery grey body with a gold line between eyes and a dark patch at the beginning of the body',
            'curiosity' => 'It is one of the most important commercially cultured species. It is used in ocean cage aquaculture in Madeira from 1997. It is a voracious opportunistic predators, capable of adapting their diet to the food available in its environment. It changes its sex from male to female with growth.',
            'conserv' => 'Vulnerable',
            'size' => 'Up to 70 cm length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [true], []
    ],
    [
        [
            'type_id' => '8',
            'name' => 'Leafy flat-blade algae',
            'name_scientific' => 'Stypopodium sp.',
            'description' => 'Golden brown or olive-green smooth blades with broad squared-off ends growing in a fan shape',
            'curiosity' => 'It presents bands of different coloured material develop parallel to the growing edge including an iridescent green band that gives the seaweed an attractive zoned appearance. It is attached to rock and often forms dense beds. It has different compounds with defensive function.',
            'conserv' => 'Vulnerable',
            'size' => 'Up to 50 cm length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Native',
        ], [true], []
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Atlantic Spotted Dolphin',
            'name_scientific' => 'Stenella frontalis',
            'description' => 'The Atlantic spotted dolphin is a dolphin found in the Gulf Stream of the North Atlantic Ocean. Older members of the species have a very distinctive spotted coloration all over their bodies.',
            'curiosity' => 'Monk Seal is the most endangered seal species in the world with less than 700 individuals in total and about 40 in Madeira. In the past, were hunted for their fur, oil, meat and for medicinal use. Monk Seal is considered as a species of community importance and its populations are legally protected.',
            'conserv' => 'Least Concern',
            'size' => 'Up to 2.29 m length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '12',
            'name' => 'Bearded Seal',
            'name_scientific' => 'Erignathus barbatus',
            'description' => 'Distinguishing features of this earless seal include square fore flippers and thick bristles on its muzzle. Adults are greyish-brown in colour, darker on the back. Occasionally the face and neck are reddish brown, seal pups are born with a greyish-brown natal fur with scattered patches of white on the back and head.',
            'curiosity' => 'Primarily benthic, bearded seals feed on a variety of small prey found along the ocean floor, including clams, squid, and fish. Their whiskers serve as feelers[16] in the soft bottom sediments. Adults tend not to dive very deep, favoring shallow coastal areas no more than 300 m deep. Pups up to one year old, however, will venture much deeper, diving as deep as 450 m',
            'conserv' => 'Least Concern',
            'size' => 'up to 2.7 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Native',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Beluga, White Whale',
            'name_scientific' => 'Delphinapterus leucas',
            'description' => 'The sudden tapering to the base of its neck gives it the appearance of shoulders, unique among cetaceans. The tailfin grows and becomes increasingly and ornately curved as the animal ages. The flippers are broad and short—making them almost square-shaped.',
            'curiosity' => 'These cetaceans are highly sociable and they regularly form small groups, or pods, that may contain between two and 25 individuals, with an average of 10 members. Pods tend to be unstable, meaning individuals tend to move from pod to pod. Radio tracking has even shown belugas can start out in one pod and within a few days be hundreds of miles away from that pod.',
            'conserv' => 'Least Concern',
            'size' => 'Adult up to 5.5 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Bottlenose Dolphin',
            'name_scientific' => 'Tursiops truncatus',
            'description' => 'Their elongated upper and lower jaws form what is called a rostrum, or snout, which gives the animal its common name.[39] The real, functional nose is the blowhole on top of its head; the nasal septum is visible when the blowhole is open.',
            'curiosity' => 'Bottlenose dolphins can live for more than 40 years. Females typically live 5–10 years longer than males, with some females exceeding 60 years. This extreme age is rare and less than 2% of all Bottlenose dolphins will live longer than 60 years. Bottlenose dolphins can jump at a height of 6 metres up in the air; they use this to communicate with one another.',
            'conserv' => 'Least Concern',
            'size' => 'Up to 4 metres length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Bowhead Whale',
            'name_scientific' => 'Balaena mysticetus',
            'description' => 'The bowhead whale has a large, robust, dark-coloured body and a white chin/lower jaw. The whale has a massive triangular skull, which it uses to break through the Arctic ice to breathe.',
            'curiosity' => 'They live entirely in fertile Arctic and sub-Arctic waters, unlike other whales that migrate to low latitude waters to feed or reproduce. The bowhead was also known as the Greenland right whale or Arctic whale. American whalemen called them the steeple-top, polar whale, or Russia or Russian whale. The bowhead has the largest mouth of any animal.',
            'conserv' => 'Least Concern',
            'size' => 'Up to 18 m length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Clymene Dolphin',
            'name_scientific' => 'Stenella clymene',
            'description' => 'The Clymene dolphin looks very similar to the spinner dolphin. At close quarters, it is possible to observe that the beak of the Clymene is slightly shorter than that of its relative. The dorsal fin is also less erect and triangular.',
            'curiosity' => 'Clymene dolphins spend most of their lives in waters over 100 m in depth, but occasionally move into shallower, coastal regions. They feed on squid and small schooling fish., hunting either at night, or in mesopelagic waters where there is only limited light. Predators include cookie-cutter sharks, as evidenced by bite marks seen on a number of animals.',
            'conserv' => 'Least Concern',
            'size' => 'Up to 2 m length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Common dolphin',
            'name_scientific' => 'Delphinus delphis',
            'description' => ' The color pattern on the body is unusual. The back is dark and the belly is white, while on each side is an hourglass pattern colored light grey, yellow, or gold in front and dirty grey in back.',
            'curiosity' => 'Common dolphins face a mixture of threats due to human influence. Moderate levels of metal pollutants, which are thought to negatively impact dolphin health,[10] have been measured in some populations. Populations have been hunted off the coast of Peru for use as food and shark bait. In most other areas, the dolphins have not been hunted directly. ',
            'conserv' => 'Vulnerable',
            'size' => 'Up to 2.5 m length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'False Killer Whale',
            'name_scientific' => 'Pseudorca crassidens',
            'description' => 'The false killer whale is black or dark gray, though slightly lighter on the underside. It has a slender body with an elongated, tapered head and 44 teeth. The dorsal fin is sickle-shaped, and its flippers are narrow, short, and pointed',
            'curiosity' => 'The false killer whale (Pseudorca crassidens) is a large oceanic dolphin that inhabits oceans worldwide but mainly frequents tropical regions. It was first described in 1846 as a species of porpoise based on a skull, which was revised when the first carcasses were observed in 1861. The name "false killer whale" comes from the similar skull characteristics to the killer whale (Orcinus orca). ',
            'conserv' => 'Near Threatened',
            'size' => 'Up to 6 m length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Fin, Finback Whale',
            'name_scientific' => 'Balaenoptera physalus',
            'description' => 'The fin whale is usually distinguished by its tall spout, long back, prominent dorsal fin, and asymmetrical colouration. ',
            'curiosity' => 'The fin whale (Balaenoptera physalus), also known as finback whale or common rorqual and formerly known as herring whale or razorback whale, is a marine mammal belonging to the parvorder of baleen whales. It is the second-largest species on Earth after the blue whale.',
            'conserv' => 'Vulnerable',
            'size' => 'Up to 25.9 m length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Fraser´s Dolphin',
            'name_scientific' => 'Distaplia corolla',
            'description' => 'They have a stocky build, a small fin in relation to the size of the body, conspicuously small flippers. The dorsal fin and beak are also insubstantial. ',
            'curiosity' => 'The dolphin is normally sighted in deep tropical waters; between 30°S and 20°N. The Eastern Pacific is the most reliable site for viewings. Groups of stranded dolphins have been found as far afield as France and Uruguay. However these are regarded as anomalous and possibly due to unusual oceanographic conditions, such as El Niño.  ',
            'conserv' => 'Least Concern',
            'size' => 'Growing up to 2.75 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Grampus, Risso´s Dolphin',
            'name_scientific' => 'Grampus griseus',
            'description' => 'Risso´s dolphin has a relatively large anterior body and dorsal fin, while the posterior tapers to a relatively narrow tail. The bulbous head has a vertical crease in front.',
            'curiosity' => 'Risso´s dolphin (Grampus griseus) is the only species of dolphin in the genus Grampus. It is commonly known as the Monk dolphin among Taiwanese fishermen.',
            'conserv' => 'Least Concern',
            'size' => 'Up to 4.00 m length',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '12',
            'name' => 'Harp Seal',
            'name_scientific' => 'Pagophilus groenlandicus',
            'description' => 'The mature harp seal has pure black eyes. It has a silver-gray fur covering its body, with black harp or wishbone-shaped markings dorsally.',
            'curiosity' => 'Compared to other phocid seals, the harp seal dives from shallow to moderately deep depths. Dive depth varies with season, time of day and location. In the Greenland Sea sub-population, the average dive rate is around 8.3 dives per hour and dives range from a depth of less than 20 to over 500m.',
            'conserv' => 'Least Concern',
            'size' => 'reach a length up to 1.9 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Humpback Whale',
            'name_scientific' => 'Megaptera novaeangliae',
            'description' => 'Humpbacks can easily be identified by their stocky body, obvious hump, black dorsal coloring and elongated pectoral fins. The head and lower jaw are covered with knobs called tubercles, which are hair follicles and are characteristic of the species. ',
            'curiosity' => 'Humpback whales are rorquals, members of the Balaenopteridae family that includes the blue, fin, Bryde´s, sei and minke whales. The rorquals are believed to have diverged from the other families of the suborder Mysticeti as long ago as the middle Miocene era.',
            'conserv' => 'Least Concern',
            'size' => 'Adults range in length from 12–16 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Orca, Killer Whale',
            'name_scientific' => 'Orcinus orca',
            'description' => 'A typical killer whale distinctively bears a black back, white chest and sides, and a white patch above and behind the eye. Calves are born with a yellowish or orange tint, which fades to white. It has a heavy and robust body with a large dorsal fin up to 1.8 m tall.',
            'curiosity' => 'Killer whales are found in all oceans and most seas. Due to their enormous range, numbers, and density, relative distribution is difficult to estimate, but they clearly prefer higher latitudes and coastal areas over pelagic environments.',
            'conserv' => 'Data Deficient ',
            'size' => 'Range from 6 to 8 metres',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '12',
            'name' => 'Leopard Seal',
            'name_scientific' => 'Hydrurga leptonyx',
            'description' => 'The leopard seal has a distinctively long and muscular body shape, when compared to other seals. This species of seal is known for its massive reptilian-like head and jaws that allow it to be one of the top predators in its environment.',
            'curiosity' => 'Leopard seals are very vocal underwater during the austral summer. The male seals produce loud calls (153 to 177 dB re 1 μPa at 1 m) for many hours each day.',
            'conserv' => 'Least Concern',
            'size' => 'The overall length of this seal is 2.4–3.5 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Long-Finned Pilot Whale',
            'name_scientific' => 'Globicephala melas',
            'description' => 'Despite its common name, the long-finned pilot whale is actually a large species of dolphin. The same is true of orcas and several other small whales. It has a bulbous forehead and is black or dark grey in colour with light-grey or white markings on the throat and belly regions.',
            'curiosity' => 'Pilot whales get their name from the original belief that there was a "pilot" or lead individual in their groups. The name for the genus, "Globicephala" is derived from a combination of the Latin words globus ("globe") and kephale ("head").',
            'conserv' => 'Least Concern ',
            'size' => ' Up to 6.7 meters',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Melon Headed Whale',
            'name_scientific' => 'Peponocephala electra',
            'description' => 'The melon-headed whale´s head is a rounded cone giving the animal its common name. The body is more or less uniformly light grey except for a dark grey face – sometimes called the "mask". The flippers are long and pointed. The dorsal fin is tall with a pointed tip – reminiscent of its relative the killer whale. ',
            'curiosity' => 'Melon-headed whales are very social animals that live in large groups numbering between 100 and 1,000. They have been observed swimming close to each other and touching flippers. Within the large group, they usually swim in smaller groups of 10-14',
            'conserv' => 'Least Concern',
            'size' => 'Adult grows up to 3 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Minke Whale',
            'name_scientific' => 'Balaenoptera acutorostrata',
            'description' => 'The minke whale is a black/gray/purple color.',
            'curiosity' => 'The minke whales are the second smallest baleen whale; only the pygmy right whale is smaller. Upon reaching sexual maturity (6–8 years of age), males measure an average of 6.9 m (23 ft) and females 8 m (26 ft) in length, respectively.',
            'conserv' => 'Vulnerable',
            'size' => 'Reported maximum lengths vary from 9.1 to 10.7 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Narwhal',
            'name_scientific' => 'Monodon monoceros',
            'description' => 'The pigmentation of narwhals is a mottled pattern, with blackish-brown markings over a white background. They are darkest when born and become whiter with age',
            'curiosity' => 'The most conspicuous characteristic of the male narwhal is a single long tusk, a canine tooth that projects from the left side of the upper jaw, through the lip, and forms a left-handed helix spiral.',
            'conserv' => 'Least Concern',
            'size' => 'An average length of 4.1 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'North Atlantic right whale',
            'name_scientific' => 'Eubalaena glacialis',
            'description' => 'Like other right whales, the North Atlantic right whale, also known as the northern right whale or black right whale, is readily distinguished from other whales by the callosities on its head',
            'curiosity' => 'Aside from mating activities performed by groups of single female and several males, so called SAG (Surface Active Group), North Atlantic right whales seem less active compared to subspecies in southern hemisphere.',
            'conserv' => 'Endangered',
            'size' => 'Adult North Atlantic right whales average 13–16 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Pantropical Spotted Dolphin',
            'name_scientific' => 'Stenella attenuata',
            'description' => 'The pantropical spotted dolphin varies significantly in size and coloration throughout its range. The most significant division is between coastal and pelagic varieties. The coastal form is larger and more spotted. ',
            'curiosity' => 'The pantropical spotted dolphin (Stenella attenuata) is a species of dolphin found in all the world´s temperate and tropical oceans. The species was beginning to come under threat due to the killing of millions of individuals in tuna purse seines.',
            'conserv' => 'Least Concern ',
            'size' => 'Adults are about 2.5 m long',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '12',
            'name' => 'Ross Seal',
            'name_scientific' => 'Ommatophoca rossii',
            'description' => 'The coat is colored dark-brown in the dorsal area and silvery-white beneath. At the onset of the Antarctic winter, the coat fades gradually to become light brown. ',
            'curiosity' => 'The Ross seal is able to produce a variety of complex twittering and siren-like sounds that are performed on ice and underwater, where they carry for long distances.',
            'conserv' => 'Least Concern ',
            'size' => 'Reach a length of about 1.68–2.09 m ',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Rough-Toothed Dolphin',
            'name_scientific' => 'Steno bredanensis',
            'description' => 'As the common name for the species implies, the teeth are also distinctive, having a roughened surface formed by numerous narrow irregular ridges. ',
            'curiosity' => 'Rough-toothed dolphins are typically social animals, although solitary individuals are also sighted. An average group has between ten and twenty members, but they can vary from as few as two to as many as ninety.',
            'conserv' => 'Least Concern ',
            'size' => 'Reach from 2.09 to 2.83 metres',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Short-Finned (Pacific) Pilot Whale',
            'name_scientific' => 'Globicephala macrorhynchus',
            'description' => 'The short-finned whale has a stocky body, a bulbous forehead, no prominent beak, long flippers sharply pointed at the tip, black or dark grey color, and the dorsal fin set forward on body. ',
            'curiosity' => 'Short-finned pilot whales can be confused with their relatives the long-finned pilot whales, but there are various differences. As their names indicate, their flippers are shorter than those of the long-finned pilot whale, with a gentler curve on the edge. They have fewer teeth than the long-finned pilot whale, with 14 to 18 on each jaw.',
            'conserv' => 'Least Concern ',
            'size' => 'Reach up to 5.5 meters',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Southern Right Whale',
            'name_scientific' => 'Eubalaena australis',
            'description' => ' Its skin is very dark grey or black, occasionally with some white patches on the belly. The right whale´s callosities appear white due to large colonies of cyamids ',
            'curiosity' => 'Right whales do not normally cross the warm equatorial waters to connect with the other species and (inter)breed: their thick layers of insulating blubber make it difficult for them to dissipate their internal body heat in tropical waters. ',
            'conserv' => 'Least Concern ',
            'size' => 'Reach up to 17.5–18 m',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '1',
            'name' => 'Sperm Whale, Cachalot',
            'name_scientific' => 'Physeter macrocephalus',
            'description' => 'Atop the whales skull is positioned a large complex of organs filled with a liquid mixture of fats and waxes called spermaceti. The purpose of this complex is to generate powerful and focused clicking sounds, which the sperm whale uses for echolocation and communication.',
            'curiosity' => 'Is the largest of the toothed whales and the largest toothed predator. It is the only living member of genus Physeter and one of three extant species in the sperm whale family, along with the pygmy sperm whale and dwarf sperm whale of the genus Kogia',
            'conserv' => 'Least Concern ',
            'size' => 'Reach up to 20.5 metres',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Spinner Dolphin',
            'name_scientific' => 'Stenella longirostris',
            'description' => 'The dorsal area is dark gray, the sides light gray, and the underside pale gray or white. Also, a dark band runs from the eye to the flipper, bordered above by a thin, light line.',
            'curiosity' => 'The spinner dolphin lives in nearly all tropical and subtropical waters between 40°N and 40°S. The species primarily inhabits coastal waters, islands, or banks. However, in the eastern tropical Pacific, dolphins live far from shore. Spinner dolphins may use different habitats depending on the season.',
            'conserv' => 'Least Concern ',
            'size' => 'Adults are typically 129–235 cm long',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Striped Dolphin',
            'name_scientific' => 'Stenella coeruleoalba',
            'description' => 'Its colouring is very different and makes it relatively easy to notice at sea. The underside is blue, white, or pink. One or two black bands circle the eyes, and then run across the back, to the flipper. ',
            'curiosity' => 'They may also mix with common dolphins. The striped dolphin is as capable as any dolphin at performing acrobatics — frequently breaching and jumping far above the surface of the water. ',
            'conserv' => 'Least Concern ',
            'size' => 'Adults are typically 2.6 m long',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '14',
            'name' => 'Walrus',
            'name_scientific' => 'Odobenus rosmarus',
            'description' => 'The most prominent feature of the living species is its long tusks. These are elongated canines, which are present in both male and female walruses and can reach a length of 1 m  and weigh up to 5.4 kg ',
            'curiosity' => 'The walrus is a mammal in the order Carnivora. It is the sole surviving member of the family Odobenidae, one of three lineages in the suborder Pinnipedia along with true seals (Phocidae) and eared seals (Otariidae). ',
            'conserv' => 'Vulnerable',
            'size' => 'Adults are typically 5 m long',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '12',
            'name' => 'Weddell Seal',
            'name_scientific' => 'Leptonychotes weddellii',
            'description' => 'The Weddell seal grows a thin fur coat around its whole body except for small areas around the flippers. The colour and pattern of the coat varies, often fading to a duller colour as the seal ages.',
            'curiosity' => 'Weddell seals are commonly found on fast ice, or ice fastened to land, and gather in small groups around cracks and holes within the ice. In the winter, they stay in the water to avoid blizzards, with only their heads poking through breathing holes in the ice.',
            'conserv' => 'Least Concern',
            'size' => 'Adults are typically  2.5–3.5 m long',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'White-beaked Dolphin',
            'name_scientific' => 'Lagenorhynchus albirostris',
            'description' => 'The white-beaked dolphin is a robust species of dolphin with a short beak. ',
            'curiosity' => 'White-beaked dolphins have 25 to 28 teeth in each jaw, although the three teeth closest to the front of the mouth are often not visible, failing to erupt from the gums. They have up to 92 vertebrae, more than any other species of oceanic dolphin',
            'conserv' => 'Least Concern',
            'size' => 'Adults can reach 2.3 to 3.1 m',
            'group_size' => '',
            'calves' => ' Calves are 1.1 to 1.2 m',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '11',
            'name' => 'Atlantic white-sided dolphin',
            'name_scientific' => 'Lagenorhynchus acutus',
            'description' => 'The key distinguishing feature is the white to pale yellow patch found behind the dorsal fin of the dolphin on each side. This colour variation is unique amongst the mixtures of white, greys and blues of other pelagic cetaceans.',
            'curiosity' => ' Often traveling in large pods and display aerial behaviors as they travel. Despite being docile creatures, even known to interact with various species of cetecean in a nonviolent manner, most notably with the long-finned pilot whale ',
            'conserv' => 'Least Concern',
            'size' => 'Adults can reach 2.5 m',
            'group_size' => '',
            'calves' => ' Calves are 1.1 to 1.2 m',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '12',
            'name' => 'Fur seal',
            'name_scientific' => 'Arctocephalinae',
            'description' => ' Fur seals are characterized by their external pinnae, dense underfur, vibrissae, and long, muscular limbs. They share with other otariids the ability to rotate their rear limbs forward, supporting their bodies and allowing them to ambulate on land.',
            'curiosity' => ' Typically, fur seals gather during the summer in large rookeries at specific beaches or rocky outcrops to give birth and breed. All species are polygynous, meaning dominant males reproduce with more than one female.',
            'conserv' => 'Least Concern',
            'size' => 'Size ranges from about 1.5 m, 64 kg in the male Galapagos to 2.5 m, 180 kg in the adult male New Zealand fur seal.',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '12',
            'name' => 'Sea lion',
            'name_scientific' => 'Otariinae',
            'description' => ' Sea lions are pinnipeds characterized by external ear flaps, long foreflippers, the ability to walk on all fours, short, thick hair, and a big chest and belly.',
            'curiosity' => ' Sea lions consume large quantities of food at a time and are known to eat about 5–8% of their body weight at a single feeding. Sea lions can move around 30 km/h in water and at their fastest they can reach a speed of about 56 km/h.',
            'conserv' => 'Least Concern',
            'size' => 'A male California sea lion weighs on average about 300 kg and is about 2.4m long, while the female sea lion weighs 100 kg and is 1.8m long.',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '14',
            'name' => 'Seagull',
            'name_scientific' => 'Laridae',
            'description' => 'They are generally uniform in shape, with heavy bodies, long wings, and moderately long necks. The tails of all but three species are rounded; the exceptions being Sabine\'s gull and swallow-tailed gulls, which have forked tails, and Ross\'s gull, which has a wedge-shaped tail.',
            'curiosity' => ' Gulls are highly adaptable feeders that opportunistically take a wide range of prey. The food taken by gulls includes fish and marine and freshwater invertebrates, both alive and already dead, terrestrial arthropods and invertebrates such as insects and earthworms, rodents, eggs, carrion, offal, reptiles, amphibians, plant items such as seeds and fruit, human refuse, chips, and even other birds.',
            'conserv' => 'Least Concern',
            'size' => 'Gulls range in size from 120 grams and 29 centimetres to 1.75 kg and 76 cm.',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Non-Indigenous Species',
        ], [], [true]
    ],
    [
        [
            'type_id' => '3',
            'name' => 'Spotfin burrfish',
            'name_scientific' => 'Chilomycterus reticulatus',
            'description' => 'Chilomycterus reticulatus has a rotund body, which can be inflated, with a wide, blunt head and large eyes. The nasal organ of adults sits in an open, pitted cup which in juveniles is a tentacle with two openings.',
            'curiosity' => 'The teeth are fused into a parrot like beak with no frontal groove and the mouth is large.',
            'conserv' => 'Least Concern',
            'size' => 'They grow to a standard length of 50 cm but up to 75 cm has been recorded.',
            'group_size' => '',
            'calves' => '',
            'nis_status' => 'Native',
        ], [], [true]
    ],
];
