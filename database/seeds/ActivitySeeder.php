<?php

use App\Activity;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = "/images/studies/activities/";
        $items = [
            ['name' => 'diving', 'image' => $path . "diving.svg", 'color' => "#1D2B48"],
            ['name' => 'fishing', 'image' => $path . "fishing.svg", 'color' => "#d40020"],
            ['name' => 'spearfishing', 'image' => $path . "spearfishing.svg", 'color' => "#4668f3"],
            ['name' => 'whale watching', 'image' => $path . "whale-watching.svg", 'color' => "#569788"],
            ['name' => 'drone piloting', 'image' => $path . "drone-piloting.svg", 'color' => "#3c4372"],
            ['name' => 'boardsports', 'image' => $path . "boardsports", 'color' => "#146C51"],
            ['name' => 'motorboats', 'image' => $path . "motorboats.svg", 'color' => "#217E3D"],
        ];

        $PTitems = [
            ['name' => 'mergulho'],
            ['name' => 'pesca'],
            ['name' => 'caça submarina'],
            ['name' => 'observação de baleia'],
            ['name' => 'drone'],
            ['name' => 'desportos aquáticos'],
            ['name' => 'barco a motor'],
        ];

        foreach ($items as $key => $item) {
            $newItem = new Activity($item);
            $newItem
                ->setTranslation('name', 'en', $item['name'])
                ->setTranslation('name', 'pt', $PTitems[$key]['name'])
                ->save();
        }
    }
}
