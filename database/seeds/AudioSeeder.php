<?php

use Illuminate\Database\Seeder;
use App\Audio;

class AudioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Audio::create([
            'type' => 'sighting',
            'file' => 1,
            'url' => 'audio/sighting/1',
            'creature_id' => 19
        ]);

        /*Audio::create([
			'type' => 'sighting',
        	'file' => 2,
        	'url' => 'audio/sighting/2',
            'creature_id' => 20
    	]);*/

        Audio::create([
            'type' => 'sighting',
            'file' => 3,
            'url' => 'audio/sighting/3',
            'creature_id' => 21
        ]);

        Audio::create([
            'type' => 'sighting',
            'file' => 4,
            'url' => 'audio/sighting/4',
            'creature_id' => 22
        ]);

        Audio::create([
            'type' => 'sighting',
            'file' => 5,
            'url' => 'audio/sighting/5',
            'creature_id' => 23
        ]);

        /*Audio::create([
			'type' => 'sighting',
        	'file' => 6,
        	'url' => 'audio/sighting/6',
            'creature_id' => 24
    	]);*/

        Audio::create([
            'type' => 'sighting',
            'file' => 7,
            'url' => 'audio/sighting/7',
            'creature_id' => 25
        ]);

        /*Audio::create([
			'type' => 'sighting',
        	'file' => 8,
        	'url' => 'audio/sighting/8',
            'creature_id' => 26
    	]);

    	Audio::create([
			'type' => 'sighting',
        	'file' => 9,
        	'url' => 'audio/sighting/9',
            'creature_id' => 27
    	]);

    	Audio::create([
			'type' => 'sighting',
        	'file' => 10,
        	'url' => 'audio/sighting/10',
            'creature_id' => 28
    	]);*/

        Audio::create([
            'type' => 'sighting',
            'file' => 11,
            'url' => 'audio/sighting/11',
            'creature_id' => 29
        ]);

        /*Audio::create([
			'type' => 'sighting',
        	'photo_number' => 12,
        	'url' => 'Audio/sighting/12',
            'creature_id' => 30
        ]);*/

        Audio::create([
            'type' => 'sighting',
            'file' => 13,
            'url' => 'audio/sighting/13',
            'creature_id' => 31
        ]);

        Audio::create([
            'type' => 'sighting',
            'file' => 14,
            'url' => 'audio/sighting/14',
            'creature_id' => 32
        ]);

        /*Audio::create([
			'type' => 'sighting',
        	'file' => 15,
        	'url' => 'audio/sighting/15',
            'creature_id' => 33
    	]);*/

        Audio::create([
            'type' => 'sighting',
            'file' => 16,
            'url' => 'audio/sighting/16',
            'creature_id' => 34
        ]);

        /*Audio::create([
			'type' => 'sighting',
        	'file' => 17,
        	'url' => 'audio/sighting/17',
            'creature_id' => 35
    	]);

    	Audio::create([
			'type' => 'sighting',
        	'file' => 18,
        	'url' => 'audio/sighting/18',
            'creature_id' => 36
        ]);
        
        Audio::create([
			'type' => 'sighting',
        	'file' => 19,
        	'url' => 'audio/sighting/19',
            'creature_id' => 37
        ]);

        Audio::create([
			'type' => 'sighting',
        	'file' => 20,
        	'url' => 'audio/sighting/20',
            'creature_id' => 38
        ]);
        
        Audio::create([
			'type' => 'sighting',
        	'file' => 21,
        	'url' => 'audio/sighting/21',
            'creature_id' => 39
        ]);
        
        Audio::create([
			'type' => 'sighting',
        	'file' => 22,
        	'url' => 'audio/sighting/22',
            'creature_id' => 40
        ]);
        
        Audio::create([
			'type' => 'sighting',
        	'file' => 23,
        	'url' => 'audio/sighting/23',
            'creature_id' => 41
        ]);
        
        Audio::create([
			'type' => 'sighting',
        	'file' => 24,
        	'url' => 'audio/sighting/24',
            'creature_id' => 42
        ]);
        
        Audio::create([
			'type' => 'sighting',
        	'file' => 25,
        	'url' => 'audio/sighting/25',
            'creature_id' => 43
        ]);*/

        Audio::create([
            'type' => 'sighting',
            'file' => 26,
            'url' => 'audio/sighting/26',
            'creature_id' => 44
        ]);

        Audio::create([
            'type' => 'sighting',
            'file' => 27,
            'url' => 'audio/sighting/27',
            'creature_id' => 45
        ]);

        /*Audio::create([
			'type' => 'sighting',
        	'file' => 28,
        	'url' => 'audio/sighting/28',
            'creature_id' => 46
        ]);
        
        Audio::create([
			'type' => 'sighting',
        	'file' => 29,
        	'url' => 'audio/sighting/29',
            'creature_id' => 47
        ]);
        
        Audio::create([
			'type' => 'sighting',
        	'file' => 30,
        	'url' => 'audio/sighting/30',
            'creature_id' => 48
        ]);
        
        Audio::create([
			'type' => 'sighting',
        	'file' => 31,
        	'url' => 'audio/sighting/31',
            'creature_id' => 49
        ]);
        
        Audio::create([
			'type' => 'sighting',
        	'file' => 32,
        	'url' => 'audio/sighting/32',
            'creature_id' => 50
    	]);*/
    }
}
