<?php

use App\AiCategory;
use App\AiModel;
use App\AiLabel;
use Illuminate\Database\Seeder;

class AiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = [
            ['name' => 'BiodiversityNet', "image" => "biodiversitynet.svg", "description" => "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolor nam iusto vitae veniam ullam eius impedit expedita, magnam modi et quis tenetur at accusantium, tempora quia commodi ducimus suscipit placeat."],
            ['name' => 'LitterNet', "image" => "litternet.svg", "description" => "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolor nam iusto vitae veniam ullam eius impedit expedita, magnam modi et quis tenetur at accusantium, tempora quia commodi ducimus suscipit placeat."],
            ['name' => 'AcousticNet', "image" => "acousticnet.svg", "description" => "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolor nam iusto vitae veniam ullam eius impedit expedita, magnam modi et quis tenetur at accusantium, tempora quia commodi ducimus suscipit placeat."],
        ];
        $models = [
            ['name' => 'OceanusNet_25k', 'path' => 'OceanusNet_25k', 'ai_category_id' => 1],
            ['name' => 'OceanusNet_50k', 'path' => 'OceanusNet_50k', 'ai_category_id' => 1],
            ['name' => 'OceanusNet_75k', 'path' => 'OceanusNet_75k', 'ai_category_id' => 1],
            ['name' => 'OceanusNet_125k', 'path' => 'OceanusNet_125k', 'ai_category_id' => 1],
            ['name' => 'OceanusNet_150k', 'path' => 'OceanusNet_150k', 'ai_category_id' => 1],
            ['name' => 'OceanusNet_175k', 'path' => 'OceanusNet_175k', 'ai_category_id' => 1],

            ['name' => 'OceanusNet_quantized_10k', 'path' => 'OceanusNet_quantized_10k', 'ai_category_id' => 1],
            ['name' => 'OceanusNet_quantized_20k', 'path' => 'OceanusNet_quantized_20k', 'ai_category_id' => 1],
            ['name' => 'OceanusNet_quantized_30k', 'path' => 'OceanusNet_quantized_30k', 'ai_category_id' => 1],
            ['name' => 'OceanusNet_quantized_40k', 'path' => 'OceanusNet_quantized_40k', 'ai_category_id' => 1],
            ['name' => 'OceanusNet_quantized_50k', 'path' => 'OceanusNet_quantized_50k', 'ai_category_id' => 1],
            ['name' => 'OceanusNet_quantized_60k', 'path' => 'OceanusNet_quantized_60k', 'ai_category_id' => 1],

            ['name' => 'jamstec-RIC_50k_tf_c_v1', 'path' => 'jamstec-RIC_50k_tf_c_v1', 'ai_category_id' => 2],
            ['name' => 'jamstec-litter_50k_tf_c_v1', 'path' => 'jamstec-litter_50k_tf_c_v1', 'ai_category_id' => 2],
            ['name' => 'jamstec-plastics_50k_tf_bL_v1', 'path' => 'jamstec-plastics_50k_tf_bL_v1', 'ai_category_id' => 2],
            ['name' => 'jamstec-plastics_50k_tf_bR_v1', 'path' => 'jamstec-plastics_50k_tf_bR_v1', 'ai_category_id' => 2],
            ['name' => 'jamstec-plastics_50k_tf_c_v1', 'path' => 'jamstec-plastics_50k_tf_c_v1', 'ai_category_id' => 2],
            ['name' => 'jamstec-plastics_50k_tf_c_v3', 'path' => 'jamstec-plastics_50k_tf_c_v3', 'ai_category_id' => 2],
            ['name' => 'jamstec-plastics_50k_tf_cbL_v3', 'path' => 'jamstec-plastics_50k_tf_cbL_v3', 'ai_category_id' => 2],
            ['name' => 'jamstec-plastics_50k_tf_cbLbR_v3', 'path' => 'jamstec-plastics_50k_tf_cbLbR_v3', 'ai_category_id' => 2],
            ['name' => 'jamstec-plastics_50k_tf_cbLbRn_v3', 'path' => 'jamstec-plastics_50k_tf_cbLbRn_v3', 'ai_category_id' => 2],
            ['name' => 'jamstec-plastics_50k_tf_n_v1', 'path' => 'jamstec-plastics_50k_tf_n_v1', 'ai_category_id' => 2],
        ];
        $litter = [
            'Floating Fishing Gear', 'Tetra Pak', 'Other Containers', 'Plastic Bags', 'Drink Bottles Green', 'Drink Bottles Transparent', 'PET', 'HDPE', 'PP', 'PVC', 'LDPE', 'plastic', 'RIC6 foodservice packaging, instrument panels, thermal insulation in walls and roofs', 'RIC4 6-pack rings, milk carton', 'RIC5 PP Respiratory masks filters, sanitary products, diapers, yarns and textiles, sulture prolene', 'RIC2 - shampoo bottles, grocery bags, detergent bottles', 'fishing_gear', 'PS', 'Drink Bottles Large', 'Cleaner Bottles Containers', 'Other Floating Debris', 'RIC3_vinyl gloves, plastic cards(bank, ids, loyalty), pipes, inflatable products', 'RIC1 soft drinks plastic bottles, clamshell', 'fabric', 'rubber', 'wood', 'metal', 'other', 'paper',
        ];
        $biodiversity = [
            'mysticeti', 'odontoceti', 'pinniped', 'bird', 'turtle',
        ];
        $RIC = [
            'PET', 'HDPE', 'PP', 'PS', 'PVC', 'LDPE',
        ];

        foreach ($categories as $category) {
            AiCategory::create($category);
        }

        foreach ($models as $model) {
            AiModel::create($model);
        }

        foreach ($biodiversity as $newLabel) {
            for ($i = 1; $i <= 12; $i++) {
                $label = AiLabel::createOrGet($newLabel);


                $label->models()->attach($i);
            }
        }

        foreach ($RIC as $newLabel) {
            $label = AiLabel::createOrGet($newLabel);
            $label->models()->attach(13);
        }

        foreach ($litter as $newLabel) {
            $label = AiLabel::createOrGet($newLabel);
            $label->models()->attach(14);
        }

        for ($i = 15; $i <= 22; $i++) {
            $label = AiLabel::createOrGet("plastic");
            $label->models()->attach($i);
        }
    }
}
