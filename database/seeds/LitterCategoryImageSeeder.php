<?php

use App\LitterCategoryImage;
use App\LitterCategoryImageType;
use Illuminate\Database\Seeder;

class LitterCategoryImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LitterCategoryImageType::create([
            'name' => 'ios'
        ]);
        LitterCategoryImageType::create([
            'name' => 'android-self'
        ]);
        LitterCategoryImageType::create([
            'name' => 'android-others'
        ]);
        LitterCategoryImageType::create([
            'name' => 'android-self-picked'
        ]);
        LitterCategoryImageType::create([
            'name' => 'android-others-picked'
        ]);
        LitterCategoryImageType::create([
            'name' => 'menu'
        ]);

        $path = '/images/litter-reporter/';
        $ext = '.png';

        $imgs = [
            ['url' => $path . 'menu/1' . $ext, 'litter_category_id' => 1, 'litter_category_image_type_id' => 6],
            ['url' => $path . 'menu/2' . $ext, 'litter_category_id' => 2, 'litter_category_image_type_id' => 6],
            ['url' => $path . 'menu/3' . $ext, 'litter_category_id' => 3, 'litter_category_image_type_id' => 6],
            ['url' => $path . 'menu/4' . $ext, 'litter_category_id' => 4, 'litter_category_image_type_id' => 6],
            ['url' => $path . 'menu/5' . $ext, 'litter_category_id' => 5, 'litter_category_image_type_id' => 6],
            ['url' => $path . 'menu/6' . $ext, 'litter_category_id' => 6, 'litter_category_image_type_id' => 6],
            ['url' => $path . 'menu/7' . $ext, 'litter_category_id' => 7, 'litter_category_image_type_id' => 6],
            ['url' => $path . 'menu/8' . $ext, 'litter_category_id' => 8, 'litter_category_image_type_id' => 6],
            ['url' => $path . 'menu/9' . $ext, 'litter_category_id' => 9, 'litter_category_image_type_id' => 6],
            ['url' => $path . 'menu/10' . $ext, 'litter_category_id' => 10, 'litter_category_image_type_id' => 6],
            ['url' => $path . 'menu/11' . $ext, 'litter_category_id' => 11, 'litter_category_image_type_id' => 6],

            ['url' => $path . 'ios/1' . $ext, 'litter_category_id' => 1, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/2' . $ext, 'litter_category_id' => 2, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/3' . $ext, 'litter_category_id' => 3, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/4' . $ext, 'litter_category_id' => 4, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/5' . $ext, 'litter_category_id' => 5, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/6' . $ext, 'litter_category_id' => 6, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/7' . $ext, 'litter_category_id' => 7, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/8' . $ext, 'litter_category_id' => 8, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/9' . $ext, 'litter_category_id' => 9, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/10' . $ext, 'litter_category_id' => 10, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/11' . $ext, 'litter_category_id' => 11, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/multiple' . $ext, 'litter_category_image_type_id' => 1],
            ['url' => $path . 'ios/unknown' . $ext, 'litter_category_image_type_id' => 1],

            ['url' => $path . 'android/other/1' . $ext, 'litter_category_id' => 1, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/2' . $ext, 'litter_category_id' => 2, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/3' . $ext, 'litter_category_id' => 3, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/4' . $ext, 'litter_category_id' => 4, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/5' . $ext, 'litter_category_id' => 5, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/6' . $ext, 'litter_category_id' => 6, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/7' . $ext, 'litter_category_id' => 7, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/8' . $ext, 'litter_category_id' => 8, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/9' . $ext, 'litter_category_id' => 9, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/10' . $ext, 'litter_category_id' => 10, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/11' . $ext, 'litter_category_id' => 11, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/multiple' . $ext, 'litter_category_image_type_id' => 3],
            ['url' => $path . 'android/other/unknown' . $ext, 'litter_category_image_type_id' => 3],

            ['url' => $path . 'android/other/1p' . $ext, 'litter_category_id' => 1, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/2p' . $ext, 'litter_category_id' => 2, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/3p' . $ext, 'litter_category_id' => 3, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/4p' . $ext, 'litter_category_id' => 4, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/5p' . $ext, 'litter_category_id' => 5, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/6p' . $ext, 'litter_category_id' => 6, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/7p' . $ext, 'litter_category_id' => 7, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/8p' . $ext, 'litter_category_id' => 8, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/9p' . $ext, 'litter_category_id' => 9, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/10p' . $ext, 'litter_category_id' => 10, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/11p' . $ext, 'litter_category_id' => 11, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/multiplep' . $ext, 'litter_category_image_type_id' => 5],
            ['url' => $path . 'android/other/unknownp' . $ext, 'litter_category_image_type_id' => 5],

            ['url' => $path . 'android/self/1' . $ext, 'litter_category_id' => 1, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/2' . $ext, 'litter_category_id' => 2, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/3' . $ext, 'litter_category_id' => 3, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/4' . $ext, 'litter_category_id' => 4, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/5' . $ext, 'litter_category_id' => 5, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/6' . $ext, 'litter_category_id' => 6, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/7' . $ext, 'litter_category_id' => 7, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/8' . $ext, 'litter_category_id' => 8, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/9' . $ext, 'litter_category_id' => 9, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/10' . $ext, 'litter_category_id' => 10, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/11' . $ext, 'litter_category_id' => 11, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/multiple' . $ext, 'litter_category_image_type_id' => 2],
            ['url' => $path . 'android/self/unknown' . $ext, 'litter_category_image_type_id' => 2],

            ['url' => $path . 'android/self/1p' . $ext, 'litter_category_id' => 1, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/2p' . $ext, 'litter_category_id' => 2, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/3p' . $ext, 'litter_category_id' => 3, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/4p' . $ext, 'litter_category_id' => 4, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/5p' . $ext, 'litter_category_id' => 5, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/6p' . $ext, 'litter_category_id' => 6, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/7p' . $ext, 'litter_category_id' => 7, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/8p' . $ext, 'litter_category_id' => 8, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/9p' . $ext, 'litter_category_id' => 9, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/10p' . $ext, 'litter_category_id' => 10, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/11p' . $ext, 'litter_category_id' => 11, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/multiplep' . $ext, 'litter_category_image_type_id' => 4],
            ['url' => $path . 'android/self/unknownp' . $ext, 'litter_category_image_type_id' => 4],
        ];

        foreach ($imgs as $img) {
            LitterCategoryImage::create($img);
        }
    }
}
