<?php

use Illuminate\Database\Seeder;
use App\Video;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Video::create([
            'type' => 'sighting',
            'file' => 1,
            'url' => 'video/sighting/1',
            'creature_id' => 19
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 2,
            'url' => 'video/sighting/2',
            'creature_id' => 20
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 3,
            'url' => 'video/sighting/3',
            'creature_id' => 21
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 4,
            'url' => 'video/sighting/4',
            'creature_id' => 22
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 5,
            'url' => 'video/sighting/5',
            'creature_id' => 23
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 6,
            'url' => 'video/sighting/6',
            'creature_id' => 24
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 7,
            'url' => 'video/sighting/7',
            'creature_id' => 25
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 8,
            'url' => 'video/sighting/8',
            'creature_id' => 26
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 9,
            'url' => 'video/sighting/9',
            'creature_id' => 27
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 10,
            'url' => 'video/sighting/10',
            'creature_id' => 28
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 11,
            'url' => 'video/sighting/11',
            'creature_id' => 29
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 12,
            'url' => 'Video/sighting/12',
            'creature_id' => 30
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 13,
            'url' => 'video/sighting/13',
            'creature_id' => 31
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 14,
            'url' => 'video/sighting/14',
            'creature_id' => 32
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 15,
            'url' => 'video/sighting/15',
            'creature_id' => 33
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 16,
            'url' => 'video/sighting/16',
            'creature_id' => 34
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 17,
            'url' => 'video/sighting/17',
            'creature_id' => 35
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 18,
            'url' => 'video/sighting/18',
            'creature_id' => 36
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 19,
            'url' => 'video/sighting/19',
            'creature_id' => 37
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 20,
            'url' => 'video/sighting/20',
            'creature_id' => 38
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 21,
            'url' => 'video/sighting/21',
            'creature_id' => 39
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 22,
            'url' => 'video/sighting/22',
            'creature_id' => 40
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 23,
            'url' => 'video/sighting/23',
            'creature_id' => 41
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 24,
            'url' => 'video/sighting/24',
            'creature_id' => 42
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 25,
            'url' => 'video/sighting/25',
            'creature_id' => 43
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 26,
            'url' => 'video/sighting/26',
            'creature_id' => 44
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 27,
            'url' => 'video/sighting/27',
            'creature_id' => 45
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 28,
            'url' => 'video/sighting/28',
            'creature_id' => 46
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 29,
            'url' => 'video/sighting/29',
            'creature_id' => 47
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 30,
            'url' => 'video/sighting/30',
            'creature_id' => 48
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 31,
            'url' => 'video/sighting/31',
            'creature_id' => 49
        ]);

        Video::create([
            'type' => 'sighting',
            'file' => 32,
            'url' => 'video/sighting/32',
            'creature_id' => 50
        ]);
    }
}
