<?php

use App\CreaturePhoto;
use Illuminate\Database\Seeder;

class CreaturePhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* CreaturePhoto::create([
            'link' => 'image/creature/1',
            'creature_id' => 1,
        ]); */

        /* CreaturePhoto::create([
            'creature_id' => 2,
            'link' => 'image/creature/2',
        ]); */

        /* CreaturePhoto::create([
            'creature_id' => 3,
            'link' => 'image/creature/3',
        ]); */

        /* CreaturePhoto::create([
            'creature_id' => 4,
            'link' => 'image/creature/4',
        ]); */

        CreaturePhoto::create([
            'creature_id' => 5,
            'link' => 'image/creature/5',
        ]);

        CreaturePhoto::create([
            'creature_id' => 6,
            'link' => 'image/creature/6',
        ]);

        CreaturePhoto::create([
            'creature_id' => 7,
            'link' => 'image/creature/7',
        ]);

        CreaturePhoto::create([
            'creature_id' => 8,
            'link' => 'image/creature/8',
        ]);

        /* CreaturePhoto::create([
            'creature_id' => 9,
            'link' => 'image/creature/9',
        ]); */

        /* CreaturePhoto::create([
            'creature_id' => 10,
            'link' => 'image/creature/10',
        ]); */

        /* CreaturePhoto::create([
            'creature_id' => 11,
            'link' => 'image/creature/11',
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/11b',
            'creature_id' => 11
        ]); */

        /* CreaturePhoto::create([
            'creature_id' => 12,
            'link' => 'image/creature/12',
        ]); */

        /* CreaturePhoto::create([
            'creature_id' => 13,
            'link' => 'image/creature/13',
        ]); */

        CreaturePhoto::create([
            'creature_id' => 14,
            'link' => 'image/creature/14',
        ]);

        CreaturePhoto::create([
            'creature_id' => 15,
            'link' => 'image/creature/15',
        ]);

        /* CreaturePhoto::create([
            'creature_id' => 16,
            'link' => 'image/creature/16',
        ]); */

        /* CreaturePhoto::create([
            'creature_id' => 17,
            'link' => 'image/creature/17',
        ]); */

        /* CreaturePhoto::create([
            'creature_id' => 18,
            'link' => 'image/creature/18',
        ]); */

        /* CreaturePhoto::create([
            'link' => 'image/creature/19b',
            'creature_id' => 19
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/20b',
            'creature_id' => 20
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/21b',
            'creature_id' => 21
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/22b',
            'creature_id' => 22
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/23b',
            'creature_id' => 23
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/24b',
            'creature_id' => 24
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/25b',
            'creature_id' => 25
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/26b',
            'creature_id' => 26
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/27b',
            'creature_id' => 27
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/28b',
            'creature_id' => 28
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/29b',
            'creature_id' => 29
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/30b',
            'creature_id' => 30
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/31b',
            'creature_id' => 31
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/32b',
            'creature_id' => 32
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/33b',
            'creature_id' => 33
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/34b',
            'creature_id' => 34
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/35b',
            'creature_id' => 35
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/36b',
            'creature_id' => 36
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/37b',
            'creature_id' => 37
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/38b',
            'creature_id' => 38
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/39b',
            'creature_id' => 39
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/40b',
            'creature_id' => 40
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/41b',
            'creature_id' => 41
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/42b',
            'creature_id' => 42
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/43b',
            'creature_id' => 43
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/44b',
            'creature_id' => 44
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/45b',
            'creature_id' => 45
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/46b',
            'creature_id' => 46
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/47b',
            'creature_id' => 47
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/48b',
            'creature_id' => 48
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/49b',
            'creature_id' => 49
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/50b',
            'creature_id' => 50
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/51b',
            'creature_id' => 51
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/52b',
            'creature_id' => 52
        ]);

        CreaturePhoto::create([
            'link' => 'image/creature/53b',
            'creature_id' => 53
        ]);

        CreaturePhoto::create([
            'creature_id' => 54,
            'link' => 'image/creature/54b',
        ]);

        CreaturePhoto::create([
            'creature_id' => 55,
            'link' => 'image/creature/55',
        ]);

        CreaturePhoto::create([
            'creature_id' => 55,
            'link' => 'image/creature/55b',
        ]);

        //dive photos
        CreaturePhoto::create([
            'creature_id' => 2,
            'link' => 'image/dive/2',
        ]);

        CreaturePhoto::create([
            'creature_id' => 9,
            'link' => 'image/dive/9',
        ]);

        CreaturePhoto::create([
            'creature_id' => 18,
            'link' => 'image/dive/18',
        ]);

        CreaturePhoto::create([
            'creature_id' => 56,
            'link' => 'image/dive/56',
        ]);

        CreaturePhoto::create([
            'creature_id' => 57,
            'link' => 'image/dive/57_a',
        ]);

        CreaturePhoto::create([
            'creature_id' => 57,
            'link' => 'image/dive/57_b',
        ]);

        CreaturePhoto::create([
            'creature_id' => 57,
            'link' => 'image/dive/57_c',
        ]);

        CreaturePhoto::create([
            'creature_id' => 57,
            'link' => 'image/dive/57_d',
        ]);

        CreaturePhoto::create([
            'creature_id' => 12,
            'link' => 'image/dive/12',
        ]);

        CreaturePhoto::create([
            'creature_id' => 58,
            'link' => 'image/dive/58',
        ]);

        CreaturePhoto::create([
            'creature_id' => 10,
            'link' => 'image/dive/10_a',
        ]);

        CreaturePhoto::create([
            'creature_id' => 10,
            'link' => 'image/dive/10_b',
        ]);

        CreaturePhoto::create([
            'creature_id' => 16,
            'link' => 'image/dive/16',
        ]);

        CreaturePhoto::create([
            'creature_id' => 59,
            'link' => 'image/dive/59',
        ]);

        CreaturePhoto::create([
            'creature_id' => 13,
            'link' => 'image/dive/13',
        ]);

        CreaturePhoto::create([
            'creature_id' => 60,
            'link' => 'image/dive/60',
        ]);

        CreaturePhoto::create([
            'creature_id' => 61,
            'link' => 'image/dive/61',
        ]);

        CreaturePhoto::create([
            'creature_id' => 62,
            'link' => 'image/dive/62',
        ]);

        CreaturePhoto::create([
            'creature_id' => 63,
            'link' => 'image/dive/63',
        ]);

        CreaturePhoto::create([
            'creature_id' => 64,
            'link' => 'image/dive/64',
        ]);

        CreaturePhoto::create([
            'creature_id' => 17,
            'link' => 'image/dive/17',
        ]);

        CreaturePhoto::create([
            'creature_id' => 65,
            'link' => 'image/dive/65',
        ]);

        CreaturePhoto::create([
            'creature_id' => 66,
            'link' => 'image/dive/66_a',
        ]);

        CreaturePhoto::create([
            'creature_id' => 66,
            'link' => 'image/dive/66_b',
        ]);

        CreaturePhoto::create([
            'creature_id' => 4,
            'link' => 'image/dive/4',
        ]);

        CreaturePhoto::create([
            'creature_id' => 3,
            'link' => 'image/dive/3',
        ]);
 
        CreaturePhoto::create([
            'creature_id' => 67,
            'link' => 'image/dive/67',
        ]);

        CreaturePhoto::create([
            'creature_id' => 11,
            'link' => 'image/dive/11',
        ]);

        CreaturePhoto::create([
            'creature_id' => 1,
            'link' => 'image/dive/1',
        ]); */

    }
}
