Most HTTP requests return a UserResource which has the following shape:
 			
 			{         								
	            'id' => int
	            'first_name' => string
	            'last_name' => string 
	            'email' => string
	            'phone' => string(16)
	            'gender' => char(1)
	            'country' => string
	            'b_day' => date
	            'note' => string
	            'active' => bool \\default = false
	            'is_verified' => bool
	            'roles' =>  array of strings \\only if the user has any roles 
	        }  

Some HTTP requests can have filters applied to them by adding them as parameters of the HTTP request link those are implemented in the UserFilters.php file and are the following:

 * search: 
 	* type: string
 	* values: any
 	* searches on: 

			 		[															
			 			first_name, 
			 			last_name,
			 			email,
			 			phone,
			 			gender,
			 			country,
			 			b_day,
			 			note 
			 		]
 * order:
 	* type: [field, order]
 	* field: 
 		* type: string
 		* values: 

 					[															
 						id,
 					]
		* order: 'ascend' or 'descend'
 * active: 		
 	* type: int 
 	* values: 0 or 1 
 
##Get All Users paginated by 20:
* **GET /api/user filters can be applied**:
    
    INPUT: None

	OUTPUT:
		
		{                                                                                                 
            //Attributes used for pagination                    
            ...
            'data':  //array with the last 20 user
                [
                    UserResource1,
					UserResource2
                    ...
                ]
        }

##Get User:

* **GET /api/user/{user_id}, filters can be applied**:
    
    INPUT: None

	OUTPUT:	
		UserResource


##Create User:


* **POST api/user**:
    
    INPUT:

	        {                                                                        
	           	'first_name' => string - required
	            'last_name' => string - required
	            'email' => string - required
	            'phone' => string(16) - required
	            'gender' => char(1) - required
	            'country' => string - required
	            'b_day' => date - required
	            'note' => string - required
	            'is_verified' => bool
	            'roles' =>  array of strings  \\only if the user has any roles 
	        }      

	OUTPUT:
		UserResource

##Update User:


* **PUT api/user/{user_id}**:
    
    INPUT:

	        {                                                                        
	          	'first_name' => string - optional
	            'last_name' => string - optional
	            'email' => string - optional
	            'phone' => string(16) - optional
	            'gender' => char(1) - optional
	            'country' => string - optional
	            'b_day' => date - optional
	            'note' => string - optional
	            'is_verified' => bool
	            'roles' =>  array of strings  \\only if the user has any roles 
	        }      

	OUTPUT:
		UserResource

##Delete User:


* **DELETE /api/user/{user_id}**:
    
    INPUT: None


	OUTPUT: Null, HTTP 204

