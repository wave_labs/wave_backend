Most HTTP requests return a FeedbackResource which has the following shape:
 			
 			{         								
	            'id' => int
            	'user' =>  [
                    id => int
                    first_name => string 
                    last_name => string
                ]
            	'question_id' =>  id
            	'rating' =>  int
            	'created_at' => string
            	'updated_at' => string
	        }  

Some HTTP requests can have filters applied to them by adding them as parameters of the HTTP request link those are implemented in the FeedbackFilters.php file and are the following:

 * search: 
    * type: string
    * values: any
    * searches on: 

                    [                                                           
                        rating, 
                        user.first_name,
                        user.last_name,
                        question.text,
                        question.type,
                        question.section 
                    ]

 
##Get All Feedbacks paginated by 20:
* **GET /api/feedback**:
    
    INPUT: None

	OUTPUT:
		
		{                                                                                                 
            //Attributes used for pagination                    
            ...
            'data':  //array with the last 20 creature
                [
                    FeedbackResource1,
					FeedbackResource2
                    ...
                ]
        }

##Get Feedback:

* **GET /api/feedback/{feedback_id},**:
    
    INPUT: None

	OUTPUT:	
		FeedbackResource


##Create Feedback:


* **POST api/feedback**:
    
    INPUT:

	        {                                                                        
                'user_id' => int - required
                'question_id' =>  id - required
                'rating' =>  int - required
	        }      

	OUTPUT:
		FeedbackResource

##Update Feedback:


* **PUT api/feedback/{feedback_id}**:
    
    INPUT:

	        {                                                                        
                'user_id' => int - optional
                'question_id' =>  id - optional
                'rating' =>  int - optional
	        }      

	OUTPUT:
		FeedbackResource

##Delete Feedback:


* **DELETE /api/feedback/{feedback_id}**:
    
    INPUT: None


	OUTPUT: Null, HTTP 204

