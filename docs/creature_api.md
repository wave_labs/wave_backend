Most HTTP requests return a CreatureResource which has the following shape:
 			
 			{         								
	            'id' => int
	            'type' => [  \\creature type
	              'id' => int 
	              'name' => string
	            ], 
	            'name' => string
	            'photo' => string \\photo link
	            'info' =>  string
	            'created_at' => string 
	            'updated_at' => string 
	        }  

Some HTTP requests can have filters applied to them by adding them as parameters of the HTTP request link those are implemented in the CreatureFilters.php file and are the following:

 * search: 
 	* type: string
 	* values: any
 	* searches on: 

			 		[															
			 			name, 
			 			info, 
			 		]
 * order:
 	* type: [field, order]
 	* field: 
 		* type: string
 		* values: 

 					[															
 						id,
 					]
		* order: 'ascend' or 'descend'

 
##Get All Creatures paginated by 20:
* **GET /api/creature filters can be applied**:
    
    INPUT: None

	OUTPUT:
		
		{                                                                                                 
            //Attributes used for pagination                    
            ...
            'data':  //array with the last 20 creature
                [
                    CreatureResource1,
					CreatureResource2
                    ...
                ]
        }

##Get Creature:

* **GET /api/creature/{creature_id}, filters can be applied**:
    
    INPUT: None

	OUTPUT:	
		CreatureResource


##Create Creature:


* **POST api/creature**:
    
    INPUT:

	        {                                                                        
	          "file" => int - required
		      "type_id" => int - required \\creature type id              
		      "name" => string - required 
		      "info" => string - required 
	        }      

	OUTPUT:
		CreatureResource

##Update Creature:


* **PUT api/creature/{creature_id}**:
    
    INPUT:

	        {                                                                        
	          "file" => int - required
		      "type_id" => int - required \\creature type id              
		      "name" => string - required 
		      "info" => string - required 
	        }      

	OUTPUT:
		CreatureResource

##Delete Creature:


* **DELETE /api/creature/{creature_id}**:
    
    INPUT: None


	OUTPUT: Null, HTTP 204

