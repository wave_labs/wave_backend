Get all sighting images:

    GET /api/image/sighting:

    INPUT: None

    OUTPUT:

    {
        'status':
            [
                success
            ],
        'data':
            [
                {
                    'photo_number' => int
                    'url' => string
                    'creature_id => int
                }
                ...
            ]
    }

Get all dive images:

    GET /api/image/dive:

    INPUT: None

    OUTPUT:

    {
        'status':
            [
                success
            ],
        'images':
            [
                {
                    'photo_number' => int
                    'url' => string
                }
                ...
            ]
    }

Get all litter images:

    GET /api/image/litter:

    INPUT: None

    OUTPUT:

    {
        'status':
            [
                success
            ],
        'images':
            [
                {
                    'photo_number' => int
                    'url' => string
                    'material => string
                }
                ...
            ]
    }

Get sighting image:

    GET /api/image/sighting/{index},:

    INPUT: index

    OUTPUT: Image

Get litter image:

    GET /api/image/litter/{index},:

    INPUT: index

    OUTPUT: Image

Get dive image:

    GET /api/image/dive/{index}:

    INPUT: index

    OUTPUT: Image
