Get Version:

    GET api/database/version/{type}:

    INPUT: 'litter' or 'dive' or 'sighting'

    OUTPUT:
	SUCESS:
		{
			'success' => true,
			'version' => int
		}, HTTP 200

	FAILURE:
		{
			'success' => false,
			'message' => 'Invalid parameter {type}'
		}, HTTP 400


Create version:

    GET api/database/create/{type}:

    INPUT: 'litter' or 'dive' or 'sighting'

    OUTPUT:
	SUCESS:
		redirect(/)->with('success', true)

	FAILURE:
		{
			'success' => false,
			'message' => 'Invalid parameter {type}'
		}, HTTP 400


Download version:

    GET api/database/download/{type}:

    INPUT: 'litter' or 'dive' or 'sighting'

    OUTPUT:
	SUCESS:
		Response::download(sqlite_file)

	FAILURE:
		{
			'success' => false,
			'message' => 'Invalid parameter {type}'
		}, HTTP 400


