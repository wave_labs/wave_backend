Most HTTP requests return a SightingDataResource which has the following shape:
 			
 			{         								
	            'id' => int
	            'sighting_id' => int
	            'link' => string \\ where the data is located in the website
	            'capsule_link' => string,
	            'underwater' => bool,
	            'type' => enum(['Acoustic', 'Image', 'Video']),
	            'acoustic' => AcousticResource if type == 'Acoustic' 
	            'created_at' => string 
	            'updated_at' => string              
	        }  

Some HTTP requests can have filters applied to them by adding them as parameters of the HTTP request link those are implemented in the SightingDataFilters.php file and are the following:

 * underwater: 		
 	* type: int
 	* values: 0 or 1 
 * type:
 	* type: array
 	* values: ['Acoustic', &\\|| 'Image', &\\|| 'Video'] 
 * order:
 	* type: [field, order]
 	* field: 
 		* type: string
 		* values: 

 					[															
 						id 
 						sighting\_id 
 					]
		* order: 'ascend' or 'descend'

 
##Get All Sighting Data paginated by 20:
* **GET /api/sighting-data filters can be applied**:
    
    INPUT: None

	OUTPUT:
		
		{                                                                                                 
            //Attributes used for pagination                    
            ...
            'data':  //array with the last 20 sighting data
                [
                    SightingDataResource1,
					SightingDataResource2
                    ...
                ]
        }

##Get Sighting:

* **GET /api/sighting-data/{sighting_data_id}, filters can be applied**:
    
    INPUT: None

	OUTPUT:	
		SightingResource


##Create Sighting Data:


* **POST api/sighting-data**:
    
    INPUT:

	        {                                                                        
	          "sighting_id" => int - required
		      "underwater" => boolean - required              
		      "download" => boolean - optional //used to know if its a file upload or if the file should be downloaded             
		      "capsule_link" => string - required if download field is true //link where the file to be download is located
		      "file" => file - required if download field is false
		      "type" => enum('Acoustic', 'Image', 'Video') - required
		      "acoustic_type" => enum('Click', 'Moan', 'Whistle') - required if type == "Acoustic"
	        }      

	OUTPUT:
		SightingDataResource

##Update Sighting Data:


* **PUT api/sighting-data/{sighting_data_id}**:
    
    INPUT:

	        {                                                                        
	          "sighting_id" => int - optional
		      "underwater" => boolean - optional              
		      "download" => boolean - optional //used to know if its a file upload or if the file should be downloaded             
		      "capsule_link" => string - optional if download field is true //link where the file to be download is located
		      "file" => file - optional if download field is false
		      "type" => enum('Acoustic', 'Image', 'Video') - optional
		      "acoustic_type" => enum('Click', 'Moan', 'Whistle') - optional if type == "Acoustic"
	        }      

	OUTPUT:
		SightingDataResource

##Delete Sighting-Data:


* **DELETE /api/sighting-data/{sighting_data_id}**:
    
    INPUT: None


	OUTPUT: Null, HTTP 204

