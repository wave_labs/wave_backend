Most HTTP requests return a CreatureTypeResource which has the following shape:
 			
 			{         								
	            'id' => int
            	'name' => question string
            	'mammal' =>  boolean
            	'acoustic' =>  boolean
            	'created_at' => string
            	'updated_at' => string
	        }  

Some HTTP requests can have filters applied to them by adding them as parameters of the HTTP request link those are implemented in the CreatureTypeFilters.php file and are the following:

 * search: 
    * type: string
    * values: any
    * searches on: 

                    [                                                           
                        name, 
                    ]
 * mammal:
    * type: int
    * values: 0 or 1 

 * acoustic:
    * type: int
    * values: 0 or 1
 

 
##Get All Creatures paginated by 20:
* **GET /api/creature-type**:
    
    INPUT: None

	OUTPUT:
		
		{                                                                                                 
            //Attributes used for pagination                    
            ...
            'data':  //array with the last 20 creature
                [
                    CreatureTypeResource1,
					CreatureTypeResource2
                    ...
                ]
        }

##Get Creature:

* **GET /api/creature-type/{creature_type_id},**:
    
    INPUT: None

	OUTPUT:	
		CreatureTypeResource


##Create Creature:


* **POST api/creature-type**:
    
    INPUT:

	        {                                                                        
            	'name' =>  string - required
            	'mammal' =>  boolean - required
            	'acoustic' =>  boolean - required
	        }      

	OUTPUT:
		CreatureTypeResource

##Update Creature:


* **PUT api/creature-type/{creature_type_id}**:
    
    INPUT:

	        {                                                                        
	         	'name' =>  string - optional
            	'mammal' =>  boolean - optional
            	'acoustic' =>  boolean - optional
	        }      

	OUTPUT:
		CreatureTypeResource

##Delete Creature:


* **DELETE /api/creature-type/{creature_type_id}**:
    
    INPUT: None


	OUTPUT: Null, HTTP 204

