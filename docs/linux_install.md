Pre-requirements:
	node -v = 13.1
	npm -v = 6.1.
	php -v = 7.2

git clone https://bitbucket.org/wave_lab/backend.git

composer install (Return sugestions and carbon 1 is deprecated)

composer update (Return In ProviderRepository.php line 208:
	Class 'Cerbero\QueryFilters\QueryFiltersServiceProvider' not found )
	Solution: config/app.php change that line to Cerbero\QueryFilters\Providers\QueryFiltersServiceProvider
re run: composer update

(create .env file)
php artisan key:generate
php artisan jwt:secret

npm install
npm audit fix
npm run watch

create database mol into phpmyadmin
	php artisan migrate
	php artisan serve

Create a admin user:
	php artisan permissions:create
	php artisan admin:create
	php artisan serve

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

GIT STUFF:
	Creating a branch:
		git checkout -b NAME
	Push/Pull branch:
		git pull origin NAME
		git add....
		git commit -m "msg"
		git push origin NAME
	Update required branch with the new master:
		git checkout master
		git fetch -p origin
		git merge origin/master
		git checkout NAME
		git merge master
		git push origin NAME
