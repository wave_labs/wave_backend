Most HTTP requests return a LitterSubCategoryResource which has the following shape:

        {
            'id' => int
            'name' => string
            'materials' => string
            'OSPAR_code' => string
            'UNEP_code' => string
            'Core' =>  boolean
            'Beach' =>  boolean
            'Seafloor' =>  boolean
            'Floating' =>  boolean
            'Biota' =>  boolean
            'Micro' =>  boolean
            'created_at' => string
            'updated_at' => string
        }

Get All Litter Sub Categories paginated by 10:

    GET /api/litter-type:

    INPUT: Request [?search="filter"&page="page number"]

    OUTPUT:
    {
    //Attributes used for pagination
    ...
    'data':  //array with the last 10 Litter Type
        [
    	LitterSubCategoryResource1,
    	LitterSubCategoryResource2
    	...
        ]
    }

Get All Litter Sub Categories:

    GET /api/litter-type/selector:

    INPUT: Request [?search="filter"&page="page number"]

    OUTPUT:
    {
    'data':
        [
    	LitterSubCategoryResource1,
    	LitterSubCategoryResource2
    	...
        ]
    }

Get All Litter Sub Categories for mobile:

    GET /api/litter-type/mobile:

    INPUT: Request [?search="filter"&page="page number"]

    OUTPUT:
    {
    //Attributes used for pagination
    ...
    'data':
        [
    	'material' => [
    		LitterSubCategoryResource1,
    		LitterSubCategoryResource2
    		...
    	],
    	'material' => [
    		LitterSubCategoryResource1,
    		LitterSubCategoryResource2
    		...
    	],
    	...

        ]
    }

Get Litter:

    GET /api/litter-type/{litter_type_id},:

    INPUT: None

    OUTPUT: LitterSubCategoryResource

Create Litter:

    POST api/litter-type:

    INPUT:

        {
            'name' => string - required
            'materials' => string - required
            'OSPAR_code' => string
            'UNEP_code' => string
            'Core' =>  boolean - required
            'Beach' =>  boolean - required
            'Seafloor' =>  boolean - required
            'Floating' =>  boolean - required
            'Biota' =>  boolean - required
            'Micro' =>  boolean - required
        }

    OUTPUT: LitterSubCategoryResource

Update Litter:

    PUT api/litter-type/{litter_type_id}:

    INPUT:

        {
            'name' => string - opcional
            'materials' => string - opcional
            'OSPAR_code' => string - opcional
            'UNEP_code' => string - opcional
            'Core' =>  boolean - opcional
            'Beach' =>  boolean - opcional
            'Seafloor' =>  boolean - opcional
            'Floating' =>  boolean - opcional
            'Biota' =>  boolean - opcional
            'Micro' =>  boolean - opcional
        }

    OUTPUT: CreatureTypeResource

Delete Litter:

    DELETE /api/litter-type/{litter_type_id}:

    INPUT: None

    OUTPUT: Null, HTTP 204
