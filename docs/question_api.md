Most HTTP requests return a QuestionResource which has the following shape:
 			
 			{         								
	            'id' => int
            	'text' =>  string
            	'type' =>  string
            	'section' =>  string
            	'created_at' => string
            	'updated_at' => string
	        }  

Some HTTP requests can have filters applied to them by adding them as parameters of the HTTP request link those are implemented in the QuestionFilters.php file and are the following:

 * search: 
    * type: string
    * values: any
    * searches on: 

                    [                                                           
                        text, 
                        type, 
                        section 
                    ]

 
##Get All Questions paginated by 20:
* **GET /api/question**:
    
    INPUT: None

	OUTPUT:
		
		{                                                                                                 
            //Attributes used for pagination                    
            ...
            'data':  //array with the last 20 creature
                [
                    QuestionResource1,
					QuestionResource2
                    ...
                ]
        }

##Get Question:

* **GET /api/question/{question_id},**:
    
    INPUT: None

	OUTPUT:	
		QuestionResource


##Create Question:


* **POST api/question**:
    
    INPUT:

	        {                                                                        
                'text' =>  string - required
                'type' =>  string - required
            	'section' =>  string - required
	        }      

	OUTPUT:
		QuestionResource

##Update Question:


* **PUT api/question/{question_id}**:
    
    INPUT:

	        {                                                                        
                'text' =>  string - optional
                'type' =>  string - optional
                'section' =>  string - optional
	        }      

	OUTPUT:
		QuestionResource

##Delete Question:


* **DELETE /api/question/{question_id}**:
    
    INPUT: None


	OUTPUT: Null, HTTP 204

