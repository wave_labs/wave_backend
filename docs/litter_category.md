Most HTTP requests return a Litter Category Resource which has the following shape:

    {
    	'id' => int
    	'name' => string
    	'sub_category' => [
    		'id' => int
    		'name' => string
    		'OSPAR_code' => string
    		'UNEP_code' => string
    	]
    }

Constraints

    {
    	NULL
    }

Get All Litter Categories paginated by 10:

    GET /api/litter-category:

    INPUT: Request [?search="filter"&page="page"&search=$string]

    OUTPUT:

    {
        //Attributes used for pagination
        ...
        'data':  //array with the last 10 Litter
            [
                LitterCategoryResource1,
                LitterCategoryResource2
                ...
            ]
    }

Get Litter Category:

    GET /api/litter-category/{litter_id},:

    INPUT: None

    OUTPUT: LitterCategoryResource

Create Litter:

    POST api/litter-category:

    INPUT:

        {
    		'name' => string - required
        }

    OUTPUT: LitterCategoryResource

Update Litter:

    PUT api/litter-category/{litter_id}:

    INPUT:

        {
    	    'name' => string - required
        }

    OUTPUT: LitterCategoryResource

Delete Litter:

    DELETE /api/litter-category/{litter_id}:

    INPUT: None

    OUTPUT: 'status' => 'success', HTTP 200
