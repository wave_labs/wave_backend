Every route here is in the AuthController.php file unless stated otherwise.

**POST /api/register**:
    Registers a user. Sends an email through ConfirmEmailNotification.php

    INPUT: 

        {
            'email' => string - required
            'password' => string - required 
            'userable_type' => 'UserPerson' or 'UserDiveCenter' - required
            'name' => string -required 
            if type_name = 'UserDiveCenter'
                'latitute' => float - required
                'longitude' => float - required
          
        }

	OUTPUT:
		SUCCESS:
    		{                                                                                          
                'success'=> true, 
                'message'=> 'Thanks for signing up! Please check your email to complete your registration.'
            }
        FAILURE:
            {                                                                                          
                'success'=> false, 
                'message'=> message depends on which field it fails
            }


**POST /api/login**:

    INPUT: 

        {
            'email' => string - required
            'password' => string - required 
        }

    OUTPUT:
        SUCCESS:
            {
                'success' => true, 
                'user'=> [   
                    'id' => $this->id,
                        'userable' => [
                            if type_name = 'UserPerson'
                              'name' => $this->name
                              'gender' => $this->gender
                              'country' => $this->country
                              'b_day' => $this->b_day
                              'created_at' => $this->created_at
                              'updated_at' => $this->updated_at
                              'number_dives' => $this->number_dives

                            elif type_name = 'UserDiveCenter'
                              'name' => $this->name
                              'latitude' => $this->latitude
                              'longitude' => $this->longitude
                              'address' => $this->address
                              'created_at' => $this->created_at
                              'updated_at' => $this->updated_at
                            ],
                    'email' => $this->email,
                    'note' => $this->note,
                    'active' => $this->active,
                    'level' => $this->level,
                    'points' => $this->points,
                    'created_at' => $this->created_at,
                    'is_verified' => $this->is_verified,
                    'updated_at' => $this->updated_at',
                'data'=> [ 
                    'access_token' => $token,
                    'token_type' => 'bearer'
                ]
            }
        FAILURE:
            Wrong Credentials, HTTP 404:  
                {                                                                                          
                    'success' => false, 
                    'error' => 'We cant find an account with this credentials. Please make sure you entered the right information and you have verified your email address.'
                }
            Email not verified, HTTP 401:
                {                                                                                          
                    'success' => false, 
                    'error' => 'The email is not verified.'
                }
            Email not active, HTTP 401:
                {                                                                                          
                    'success' => false, 
                    'error' => 'You must wait for our admin to accept your account.'
                }
            Something unexpected happened, HTTP 500:  
                {                                                                                          
                    'success' => false, 
                    'error' => 'Failed to login, please try again.''
                }

**GET /api/logout**:

    INPUT: None 

    OUTPUT:
        SUCCESS:
            {
                'success' => true, 
                'user'=> auth()->user(), 
                'data'=> [ 
                    'access_token' => $token,
                    'token_type' => 'bearer'
                ]
            }
        FAILURE:
            Wrong Credentials, HTTP 404:  
                {                                                                                          
                    'success' => false, 
                    'error' => 'We cant find an account with this credentials. Please make sure you entered the right information and you have verified your email address.'
                }
            Email not verified, HTTP 401:
                {                                                                                          
                    'success' => false, 
                    'error' => 'The email is not verified.'
                }
            Email not active, HTTP 401:
                {                                                                                          
                    'success' => false, 
                    'error' => 'You must wait for our admin to accept your account.'
                }
            Something unexpected happened, HTTP 500:  
                {                                                                                          
                    'success' => false, 
                    'error' => 'Failed to login, please try again.''
                }


**POST api/password/recover**:

    INPUT: 

        {
            'email' => string - required
        }

    OUTPUT:
        SUCCESS:
            {
                'success' => true, 
                'data'=> [
                     'message'=> 'A reset email has been sent! Please check your email.'
                    ]
            }
        FAILURE:
            Email not found:  
                {                                                                                          
                    'success' => false, 
                    'error' => [
                        'email'=> 'Your email address was not found.'
                    ]
                }
            Something unexpected happened, HTTP 401:  
                {                                                                                          
                    'success' => false, 
                    'error' => exception message
                }

**POST api/activate/{user_id}**:
    Activates a user. Sends an email through UserActivationNotification.php

    INPUT: None

    OUTPUT:
        SUCCESS:
            {
                'success' => true, 
                'data'=> [
                     'message'=> 'User successfully activated'
                    ]
            }

**GET api/user/verify/{verification_code}**:
    Verifies user.

    INPUT: None

    OUTPUT:
        SUCCESS:
            {
                'success' => true, 
                'message'=> 'You have successfully verified your email address'
            }

            Account already verified:
                {
                    'success' => true, 
                    'message'=> 'Account already verified'
                }    

        FAILURE:
            {
                'success' => false, 
                'error'=> 'Verification code is invalid.'
            }


**GET api/refresh**:
    Refresh JWT token

    INPUT: None

    OUTPUT:
        SUCCESS:
            {
                'success' => true, 
                'user'=> auth()->user(), 
                'data'=> [ 
                    'access_token' => $token,
                    'token_type' => 'bearer'
                ]
            }

**GET api/me**:

    INPUT: None

    OUTPUT:
        SUCCESS:
            {
                'user'=> auth()->user(), 
            }

**GET api/password/reset/{token}**:
**POST api/password/reset**:
    Default Laravel
