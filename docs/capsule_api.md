Most HTTP requests return a CapsuleResource which has the following shape:
 			
 			{         								
		        'id' => int 
	            'user' => [
	             'id' => int
	             'first_name' => string
	             'last_name' => string
	            ]
	            'temperature_c' => int \\can be null
	            'ram_mb' => int
	            'disk_space_gb' => int 
	            'volts' => int
	            'current' => int
	            'power' => int \\can be null
	            'image_link' => int \\can be null
	            'latitude' => decimal(10,8)
	            'longitude' => decimal(10,8)
	            'assigned' => boolean or int[0,1] \\default false
	            'description' => string
	            'created_at' => string
	            'updated_at' => string       
	        }  

Some HTTP requests can have filters applied to them by adding them as parameters of the HTTP request link those are implemented in the CapsuleFilters.php file and are the following:

 * assigned: 		
 	* type: int
 	* values: 0 or 1 
 * search: 
 	* type: string
 	* values: any
 	* searches on: 

			 		[															
			 			description, 
			 			user.first_name, 
			 			user.last_name, 
			 		]
 * order:
 	* type: [field, order]
 	* field: 
 		* type: string
 		* values: 

 					[															
 						id,
 						temperature_c,
 						ram_mb,
 						disk_space_gb,
 						volts,
 						current,
 						power
 					]
		* order: 'ascend' or 'descend'

 
##Get All Capsules paginated by 20:
* **GET /api/capsule filters can be applied**:
    
    INPUT: None

	OUTPUT:
		
		{                                                                                                 
            //Attributes used for pagination                    
            ...
            'data':  //array with the last 20 capsule
                [
                    CapsuleResource1,
					CapsuleResource2
                    ...
                ]
        }

##Get Capsule:

* **GET /api/capsule/{capsule_id}, filters can be applied**:
    
    INPUT: None

	OUTPUT:	
		CapsuleResource


##Create Capsule:


* **POST api/capsule**:
    
    INPUT:

	        {                                                                        
	          "user_id" => int - required
		      "current" => int - required              
		      "disk_space_gb" => int - required 
		      "ram_mb" => int - required 
		      "volts" => int - required 
		      "latitude" => float - required 
		      "longitude" => float - required 
		      "description" => string - required 
		      "assigned" => boolean or int[0,1] - optional, default is false  
	        }      

	OUTPUT:
		CapsuleResource

##Update Capsule:


* **PUT api/capsule/{capsule_id}**:
    
    INPUT:

	        {                                                                        
	          "user_id" => int - optional
		      "current" => int - optional              
		      "disk_space_gb" => int - optional 
		      "ram_mb" => int - optional 
		      "volts" => int - optional 
		      "latitude" => float - optional 
		      "longitude" => float - optional 
		      "description" => string - optional 
		      "assigned" => boolean or int[0,1] - optional, default is false  
	        }      

	OUTPUT:
		CapsuleResource

##Delete Capsule:


* **DELETE /api/capsule/{capsule_id}**:
    
    INPUT: None


	OUTPUT: Null, HTTP 204

