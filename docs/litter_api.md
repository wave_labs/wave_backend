Most HTTP requests return a LitterResource which has the following shape:

    {
    	'id' => int
    	'latitude' => decimal
    	'longitude' => decimal
    	'date' => date
    	'quantity' => string
    	'radius' => string
    	'collected' => boolean
    	'source' => string
    	'userable' => [
    		'id' => int
    		'type_name' => string
    		'user' => UserPersonResource
    	]
    	'litter_sub_category' => [
    		'id' => int
    		'name' => string
    		'category' => string
    		'OSPAR_code' => string
    		'UNEP_code' => string
    	]
    	'litter_category' => [
    		'id' => int
    		'name' => string
    	]
    	'created_at' => string
    	'updated_at' => string
    }

Constraints

    {
    	'source' => core, beach, seafloor, floating, micro, biota
    	'quantity' => 1-5, 5-10, 10-50, >50
    	'radius' => <1, 1-5, 5-20, >20
    	'litter_sub_category' => array of ids
    	'litter_category' => array of ids
    	'photo' => jpeg, png, bmp, gif, svg, or webp
    }

Get All Litter paginated by 10:

    GET /api/litter:

    INPUT: Request [?search="filter"&page="page"&core&beach&seafloor&floating&micro&biota]

    OUTPUT:

    {
        //Attributes used for pagination
        ...
        'data':  //array with the last 10 Litter
            [
                LitterResource1,
                LitterResource2
                ...
            ]
    }

Get All Litter coordinates:

    GET /api/litter/coordinates:

    INPUT: Request [?search="filter"&page="page number"]

    OUTPUT:

    {
        'data':
            [
    			'id' => int
    			'name' => string
    			'date' => string
    			'latitude' => decimal
    			'longitude' => decimal
            ]
    }

Get Litter:

    GET /api/litter/{litter_id},:

    INPUT: None

    OUTPUT: LitterResource

Create Litter:

    POST api/litter:

    INPUT:

        {
    		'user_id' => int - required
    		'photo' => image - required
    		'latitude' => decimal - required
    		'longitude' => decimal - required
    		'date' => date - required
    		'collected' => boolean - nullable
    		'litter_sub_category' => array - required-if:source=beach
    		'litter_category' => array - required-if:source=beach
    		'quantity' => string - required-if:source=beach
    		'radius' => string - required-if:source=beach
        }

    OUTPUT: LitterResource

Update Litter:

    PUT api/litter/{litter_id}:

    INPUT:

        {
    	'user_id' => int - required
    	'latitude' => decimal - required
    	'longitude' => decimal - required
    	'date' => date - required
    	'collected' => boolean - nullable
    	'quantity' => string - required-if:source=beach
    	'radius' => string - required-if:source=beach
        }

    OUTPUT: LitterResource

Delete Litter:

    DELETE /api/litter/{litter_id}:

    INPUT: None

    OUTPUT: 'status' => 'success', HTTP 200
