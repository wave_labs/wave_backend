Most HTTP requests return a SightingResource which has the following shape:
 			
 			{         																							
				'id' => int,
				'user' => [
				 'id' => int,
				 'user_type' => string,
				 'user_type_id' => int,
				],
				'creature' => [
					'id' => 1
					'name' => string,
					'dive_center_app' =>  boolean,
					'whale_reporter_app' =>  boolean,
					'type_id' => [
							'id' => 1
							'taxa_category' => string,
							'acoustic_data' => string,
							'acoustic_category' => string,
							'created_at' => string,
	            					'updated_at' => string,
							'deleted_at' => string,
					]
				], 
				'capsule_id' => int,
	            'beaufort_scale_id' => int,
	            'date' => date,
	            'latitude' => decimal(10,8),
	            'longitude' => decimal(10,8),
		    'time' => int,
		    'report_source' => string //to be enum in future
	            'is_group_mixed' => boolean or int,
	            'group_size' => int,
		    'date' => date,
	            'behaviour' => string,
	            'calves' => int,
	            'vehicle_id' => int,
	            'notes' => string,
	            'created_at' => string,
	            'updated_at' => string,
	        }   

Some HTTP requests can have filters applied to them by adding them as parameters of the HTTP request link those are implemented in the SightingFilters.php file and are the following:

 * is\_group\_mixed: 		
 	* type: int
 	* values: 0 or 1 
 * creatures:
 	* type: int
 	* values: any, creature id 
 * date:
 	* type: string-string  //sightings between this dates
 	* values: any of this type YYYY-MM-DD HH:MM:SS
 * search: 
 	* type: string
 	* values: any
 	* searches on: 

			 		[															
			 			date, 
			 			notes, 
			 			behaviour, 
			 			vehicle_id, 
			 			user.first_name, 
			 			user.last_name, 
			 			creature.name
			 		]
 * order:
 	* type: [field, order]
 	* field: 
 		* type: string
 		* values: 

 					[															
 						id 
 						capsule\_id 
 						beaufort\_scale 
 						date 
 						group\_size 
 						calves
 					]
		* order: 'ascend' or 'descend'


##Get All Sightings paginated by 20:
* **GET /api/sighting filters can be applied**:
    
    INPUT: None

	OUTPUT:
		
		{                                                                                                 
            //Attributes used for pagination                    
            ...
            'data':  //array with the last 20 sightings
                [
                    SightingResource1,
					SightingResource2
                    ...
                ]
        }
		

##Get All Sightings by coordinates:
* **GET /api/sighting/coordinates, filters can be applied**:
    
    INPUT: None

	OUTPUT:
	
	Returns all sightings in a SightingCoordResource Collection which has the following shape: 
		
		{																
			...
			'data':
				[
					{																				
			            'id' => int,
			            'creature' => [ 
			              'id' => int,
			              'name' => string,
			            ], 
			            'date' => string,
			            'latitude' => decimal(10,8),
			            'longitude' => decimal(10,8),
			            'image' => string - optional //link of where the image is 
			        }
			    ]
    	}


##Get Sighting:


* **GET /api/sighting/{sighting_id}, filters can be applied**:
    
    INPUT: None

	OUTPUT:	
		SightingResource



##Create Sighting:


* **POST /api/sighting**:
    
    INPUT:

	        {                                                                                      
		      "user_id" => int - required  
		      "capsule_id" => int - required
		      "creature_id" => int - required
		      "beaufort_scale_id" => int - required
		      "latitude" => decimal(10,8) - optional
		      "longitude" => decimal(10,8) - optional 
		      "is_group_mixed" => boolean - optional
		      "group_size" => int - optional
		      "behaviour_id" => int - optional
		      "time" => int - optional
		      "report_source" => string - optional
		      "calves" => int  - optional
		      "vehicle_id" => int - optional
		      "notes" => text - optional
	        }      

	OUTPUT:
		SightingResource


##Update Sighting:


* **PUT /api/sighting/{sighting_id}**:
    
    INPUT:

	        {                                                                                      
		      'user_id' => int - optional  
		      'creature_id' => int - optional
		      'beaufort_scale_id' => int - optional
		      'latitude' => decimal(10,8) - optional
		      'longitude' => decimal(10,8) - optional 
		      'is_group_mixed' => boolean - optional
		      'group_size' => int - optional
		      'time' => int - optional
		      'report_source' => string - optional
		      'behaviour' => string - optional
		      'calves' => int  - optional
		      'vehicle_id' => int - optional
		      'notes' => text - optional
	        }      

	OUTPUT:
		SightingResource


##Delete Sighting:


* **DELETE /api/sighting/{sighting_id}**:
    
    INPUT: None


	OUTPUT: Null, HTTP 204

